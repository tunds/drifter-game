﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

	// Position variables
	Transform playerPosition;
	NavMeshAgent navigation;

	PlayerHealth playerHealth;
	EnemyHealth enemyHealth;

	void Awake () {

		playerPosition = GameObject.FindGameObjectWithTag ("Player").transform;
		playerHealth = playerPosition.GetComponent <PlayerHealth> ();

		enemyHealth = GetComponent <EnemyHealth> ();
		navigation = GetComponent<NavMeshAgent> ();

	}

	void Update () {
	

		if(enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
			// Move the enemy on the nav mesh based on the players position
			navigation.SetDestination (playerPosition.position);
		else 
			// Stop the enemy from moving
			navigation.enabled = false;
	}
}
