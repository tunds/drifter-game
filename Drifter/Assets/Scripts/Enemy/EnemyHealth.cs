﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour {

	float time;

	bool isDead;
	bool isSinking;

	public int startHealth = 100;
	public int currentHealth;
	public float sinkSpeed = 2.5f;

	public Slider healthSlider;

	public AudioClip deathAudio;
	AudioSource audio;

	CapsuleCollider[] capCollider;
	EnemyAttack enemyAtk;

	Animator anim;
	int healthHash = Animator.StringToHash("enemyDead");


	void Awake () {

		audio = GetComponent<AudioSource>();
		enemyAtk = GetComponent<EnemyAttack> ();
		anim = GetComponent<Animator>();
		capCollider = GetComponents<CapsuleCollider> ();

		currentHealth = startHealth;
	
	}

	void Update () {

		if(isDead){

			// Create a timer until you remove the dead enemy
			time += Time.deltaTime;

			if(time > 4f){
				
				if(isSinking)
				{
					// Move the enemy down
					 transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
				}
			}
		}
	}

	// Enemy damage
	public void HitDamage (int amount)
	{

		if(isDead)
			return;
		
		currentHealth -= amount;
		healthSlider.value = currentHealth;

		if(currentHealth <= 0)
		{
			if(!audio.isPlaying){
				audio.PlayOneShot (deathAudio, 0.4f);
			}
			Death ();
		}
	}

	void Death ()
	{

		enemyAtk.enabled = false;

		// Find and disable the Nav Mesh Agent.
		GetComponent <NavMeshAgent> ().enabled = false;

		// Find the rigidbody component and make it kinematic so we can sink the enemy
		GetComponent <Rigidbody> ().isKinematic = true;

		// Start the death animation
		anim.SetTrigger (healthHash);

		// Set the dead and sinking variables
		isDead = true;
		isSinking = true;

		// Turn the collider into a trigger so shots can pass through it.
		capCollider[0].isTrigger = true;
		capCollider[1].isTrigger = true;

		// After 2 seconds destory the enemy.
		 Destroy (gameObject, 10f);
	}

}
