﻿using UnityEngine;
using System.Collections;

// TODO: Set up the animations for the enemy

public class EnemyAttack : MonoBehaviour {

	public float timeBetweenAttacks = 2f;
	public int attackDamage = 10;

	float timer;
	int atkHash = Animator.StringToHash("atk");

	bool plyrInRange;

	Animator anim;
	AudioSource audio;
	public AudioClip eatingAudio;

	PlayerHealth playerHealth;
	EnemyHealth enemyHealth;
	PlayerEffects playerEffects;

	void Awake () {

		// Get & assign the other scripts
		audio = GetComponent<AudioSource>();
		anim = GetComponent<Animator>();
		enemyHealth = GetComponent<EnemyHealth> ();

		playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
		playerEffects = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerEffects>();

	}
	

	void Update () {

		// Increment timer & see if the enemy is allowed to attack
		timer += Time.deltaTime;

		if (timer >= timeBetweenAttacks && plyrInRange) {

			if(!audio.isPlaying){
				audio.PlayOneShot (eatingAudio, 0.4f);	
			}

			Attack ();
		}
	}

	void Attack(){

		// Reset the timer to attack & make player take damage
		timer = 0;
		if(playerHealth.currentHealth > 0)
			playerHealth.TookDamage (attackDamage);
	}
		
	void OnTriggerEnter(Collider other){

		// The player is in the range
		if (other.CompareTag ("Player")) {

			if(enemyHealth.currentHealth > 0){

				// Show effect & animations
				playerEffects.showHitEffect ();
				anim.SetTrigger (atkHash);
				plyrInRange = true;
			}

		}

	}

	void OnTriggerExit(Collider other){

		// The player isn't in the range & disable the hit effect
		if (other.CompareTag ("Player")) {
			if (enemyHealth.currentHealth > 0) {

				playerEffects.hitEffect.SetActive (false);
				plyrInRange = false;

			}
		}
	}
		
}
