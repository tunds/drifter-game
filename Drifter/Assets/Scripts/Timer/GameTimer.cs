﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class GameTimer : MonoBehaviour {

	// Help from http://answers.unity3d.com/questions/259482/format-string-minutesseconds.html

	public DateTime startTime;
	public TimeSpan elapsedTime;

	float time;

	PlayerHealth playerHealth;

	GlobalObjectManager persistentData;

	void Start () {

		playerHealth = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerHealth> ();
		persistentData = GameObject.FindGameObjectWithTag ("PersistentData").GetComponent<GlobalObjectManager> ();

		// Stop timer from resetting
		if (persistentData.isLoaded){
			startTime = DateTime.Now;
			persistentData.isLoaded = false;
		}

	}

	void Update () {

		// Only display the time if the user isn't dead
		if(!persistentData.isDead){
			
			elapsedTime = DateTime.Now - startTime;
				
			string displayTime = String.Format("{0:00" + "}:{1:00}:{2:00}", elapsedTime.Hours, elapsedTime.Minutes, elapsedTime.Seconds);
			GetComponent<Text>().text = displayTime;

		} else {
			
			time += Time.deltaTime;

			if(time > 3f){

				persistentData.SaveData ();
				SceneManager.LoadScene ("Game Over");
			}

		}

	}
}
