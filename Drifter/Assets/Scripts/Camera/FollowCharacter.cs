﻿using UnityEngine;
using System.Collections;

public class FollowCharacter : MonoBehaviour {

	// Referenced from https://unity3d.com/learn/tutorials/projects/survival-shooter/camera-setup?playlist=17144

	public Transform target;           
	public float smoothing = 5f;        

	Vector3 offset;                    

	void Start ()
	{
		// Get the offset between the camera and player
		offset = transform.position - target.position;
	}

	void FixedUpdate ()
	{
		// Create a position for the camera
		Vector3 targetCamPos = target.position + offset;

		// Smoothly interpolate between the camera's current position and it's target position.
		transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);
	}
}
