﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerPickups : MonoBehaviour {

	public AudioClip destroyAudio;

	public Slider healthSlider;
	public Slider specialSlider;

	public GameObject weapon;
	private GameObject oldWeapon;
	public GameObject destroyedEffect;

	AudioSource audio;

	MeshRenderer weaponRenderer;
	MeshRenderer oldWeaponRenderer;

	public int numHits = 30;

	void Start(){
		audio = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter (Collider hit)
	{

		switch(hit.gameObject.tag){

		case "Health":

			healthSlider.value += 20;
			RemoveObject (hit);
			break;
		case "Special":
			
			specialSlider.value += 20;
			RemoveObject (hit);
			break;
		case "Pickup_bat":
			
			destroyedEffect.SetActive (false);
			AssignWeapon ("weapon_bat", hit);
			break;
		case "Pickup_lumber":
			
			destroyedEffect.SetActive (false);
			AssignWeapon ("weapon_lumber", hit);
			break;
		case "Pickup_pipe":
			
			destroyedEffect.SetActive (false);
			AssignWeapon ("weapon_pipe", hit);
			break;
			
		}

	}

	// Remove object add delday for sound to play
	void RemoveObject(Collider hit){
		
		hit.gameObject.transform.position = new Vector3 (0, -5, 0);
		hit.gameObject.GetComponentInChildren<MeshRenderer> ().enabled = false;
		Destroy (hit.gameObject, 2f);

	}
	// Remove object add delday for sound to play
	void AssignWeapon(string tag, Collider hit){

		hit.gameObject.transform.position = new Vector3 (0, -5, 0);
		hit.gameObject.GetComponentInChildren<MeshRenderer> ().enabled = false;
		Destroy (hit.gameObject, 2f);

		ToggleWeapon (tag);

	}

	// Display destroyed particle, play audio, assign empty weapon and reset number of hits
	public void DestroyWeapon(){

		destroyedEffect.SetActive (true);
		ParticleSystem particles = destroyedEffect.GetComponent<ParticleSystem> ();

		if(!particles.isPlaying)
			audio.PlayOneShot (destroyAudio, 1f);
			particles.Play ();
			numHits = 3;

		ToggleWeapon ("weapon_empty");
		numHits = 30;
	}

	// Get the old weapon turn it off and enable the new weapon
	void ToggleWeapon(string tag){

		oldWeapon = weapon;
		oldWeaponRenderer = oldWeapon.GetComponent<MeshRenderer> ();
		oldWeaponRenderer.enabled = false;

		weapon = GameObject.FindGameObjectWithTag (tag);
		weaponRenderer = weapon.GetComponent<MeshRenderer> ();
		weaponRenderer.enabled = true;
	}

}
