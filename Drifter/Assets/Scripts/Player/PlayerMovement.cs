﻿using CnControls;
using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public float speed = 4;
	private float playerRotation;
	private Vector3 movementDirection;

	Vector3 movement;
	Animation playerAnimation;
	CharacterController playerCharacterController;
	Quaternion rotation;

	PlayerEffects playerEffects;
	PlayerPickups playerPickups;

	bool isAttacking;

	void Awake ()
	{
		playerAnimation = GetComponent<Animation> ();
		playerCharacterController = GetComponent<CharacterController> (); 

		playerEffects = GetComponent<PlayerEffects> ();
		playerPickups = GetComponent<PlayerPickups> ();

	}
		
	void FixedUpdate ()
	{

		if (!Input.GetKeyDown (KeyCode.A) || !CnInputManager.GetButtonDown("Attack") ) 
			Move ();
	}

	void Move ()
	{

		// If the char is standing still
		if (CnInputManager.GetAxisRaw ("Horizontal") == 0 && CnInputManager.GetAxisRaw ("Vertical") == 0) {

			// If they're holding a weapon
			if (playerPickups.weapon.tag != "weapon_empty") {

				// Neither of these animations are playing then play the effect
				if (!playerAnimation.IsPlaying ("Attack_weapon") && !playerAnimation.IsPlaying ("Idle_weapon") && !playerAnimation.IsPlaying ("Skill_B")) 
					playerEffects.showIdleEffect ("Idle_weapon");
				
			} else {

				if (!playerAnimation.IsPlaying ("Attack1") && !playerAnimation.IsPlaying ("Idle") && !playerAnimation.IsPlaying ("Skill_B"))
					playerEffects.showIdleEffect ("Idle");
			}

			// Or else if they're moving
		} else {
			
			if (playerPickups.weapon.tag != "weapon_empty") {

				if (!playerAnimation.IsPlaying ("Attack_weapon") && !playerAnimation.IsPlaying ("Walk_weapon") && !playerAnimation.IsPlaying ("Skill_B")) 
					playerEffects.showWalkingEffect ("Walk_weapon");
				

			} else {

				if (!playerAnimation.IsPlaying ("Attack1") && !playerAnimation.IsPlaying ("Walk") && !playerAnimation.IsPlaying ("Skill_B")) 
					playerEffects.showWalkingEffect ("Walk");

			}

		}


		float axisCombined = CnInputManager.GetAxis ("Vertical") / CnInputManager.GetAxis ("Horizontal");

		// Get the input of the user
		Vector3 direction = new Vector3 (CnInputManager.GetAxis ("Horizontal") + 0.001f, 0, CnInputManager.GetAxis ("Vertical"));

		/* https://community.unity.com/t5/Scripting/Simple-Fix-For-quot-Look-Rotation-Viewing-Vector-Is-Zero-quot/td-p/2610783
		 * Fix the rotation 
		 */
		Quaternion newRotation = Quaternion.LookRotation(direction);

		if(axisCombined >= 0.1f || axisCombined <= 0.1f)
		{

			transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, 20 * Time.deltaTime);

		}

		// Move the character
		playerCharacterController.Move ((direction * speed) * Time.deltaTime);
	}

}
