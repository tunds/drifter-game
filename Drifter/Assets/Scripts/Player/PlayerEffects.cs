﻿using UnityEngine;
using System.Collections;

public class PlayerEffects : MonoBehaviour {

	public GameObject hitEffect;
	public GameObject stunEffect;
	public GameObject normalAttackEffect;
	public GameObject specialAttackEffect;

	Animation playerAnimation;

	void Awake () {
	
		hitEffect.SetActive (false);
		stunEffect.SetActive (false);
		normalAttackEffect.SetActive (false);
		specialAttackEffect.SetActive (false);

		playerAnimation = GetComponent<Animation> ();
	}

	public void showIdleEffect(string typeOfIdle){

		if(!playerAnimation.IsPlaying(typeOfIdle))
			playerAnimation.Play (typeOfIdle, PlayMode.StopAll);

	}

	public void showWalkingEffect(string typeOfWalk){

		if(!playerAnimation.IsPlaying(typeOfWalk))
			playerAnimation.Play (typeOfWalk, PlayMode.StopAll);

	}
		
	public void showStunEffect(){

		stunEffect.SetActive (true);
		transform.Rotate(new Vector3(0f,180f,0f));
		playerAnimation.Play("Stun");
	}

	public void showHitEffect(){
		hitEffect.SetActive (true);
	}

	public void showNormalAttackEffect(string typeOfAttack){

		normalAttackEffect.SetActive (true);
		playerAnimation.Play (typeOfAttack, PlayMode.StopAll);

	}

	public void showSpecialAttackEffect(string typeOfAttack){

		specialAttackEffect.SetActive (true);
		playerAnimation.Play (typeOfAttack, PlayMode.StopAll);

	}
}
