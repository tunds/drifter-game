﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

	public int startHealth = 100;
	public int currentHealth;
	public Slider healthSldr;
	public AudioClip audioClip;
	public AudioClip deathAudioClip;

	AudioSource audio;

	PlayerMovement playerMovement;
	PlayerEffects playerEffects;

	GlobalObjectManager persistentData;

	bool hasDied;

	void Start () {

		audio = GetComponent<AudioSource> ();

		playerMovement = GetComponent<PlayerMovement> ();
		playerEffects = GetComponent<PlayerEffects> ();
		persistentData = GameObject.FindGameObjectWithTag ("PersistentData").GetComponent<GlobalObjectManager> ();

		// Don't reset health between levels
		if (persistentData.isLoaded) {
			
			// Set the health at the start of the game
			currentHealth = startHealth;
			healthSldr.value = currentHealth;
		}

	}
		
	public void TookDamage(int amount){

		if (!audio.isPlaying) {
			audio.PlayOneShot (audioClip, 1f);
		}

		currentHealth -= amount;
		healthSldr.value = currentHealth;

		// Check to see if the user should die
		if(currentHealth <= 0 && !hasDied){

			audio.PlayOneShot (deathAudioClip, 1f);
			Die ();
		}
	}

	void Die(){

		// Set that the user is now dead
		hasDied = true;
		persistentData.isDead = true;

		// Stop the movement
		playerMovement.enabled = false;

		playerEffects.showStunEffect ();

	}
}
