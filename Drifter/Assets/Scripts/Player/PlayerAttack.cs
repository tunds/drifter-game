﻿using CnControls;
using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class PlayerAttack : MonoBehaviour {

	AudioSource audio;
	public AudioClip specialClip;
	public AudioClip normalClip;

	public Slider specialAtkSlider;

	public int damagePerHit = 20;
	public float timeBetweenAttacks = 1f;

	float timer;
	float atkEffectTimer; 
	int hitableMask;

	bool isAttacking = false;

	Ray shootRay; 
	RaycastHit shootHit; 

	PlayerEffects playerEffects;
	PlayerPickups playerPickups;

	void Awake () {

		audio = GetComponent<AudioSource> ();

		hitableMask = LayerMask.GetMask ("Hitable");
		playerEffects = GetComponent<PlayerEffects> ();
		playerPickups = GetComponent<PlayerPickups> ();
	
	}
	

	void Update () {

		// Test raycast
		// Debug.DrawRay(transform.position, transform.forward, Color.green);


		timer += Time.deltaTime;

		if (playerPickups.numHits <= 0)
			playerPickups.DestroyWeapon ();
			
		// When the user attacks
		if ((Input.GetKeyDown (KeyCode.A) && timer >= timeBetweenAttacks) || (CnInputManager.GetButtonDown("Attack") && timer >= timeBetweenAttacks)) {

			// Play sound and set attacking variable
			isAttacking = true;
			PlayAtkSound (normalClip);

			// Show the effect and change the damage depending on the weapon
			if (playerPickups.weapon.tag != "weapon_empty") {
				damagePerHit = 50;
				playerEffects.showNormalAttackEffect ("Attack_weapon");
			} else {
				damagePerHit = 20;
				playerEffects.showNormalAttackEffect ("Attack1");
			}
				
			Attack ();

		} else if ((Input.GetKeyDown (KeyCode.S) && timer >= timeBetweenAttacks && specialAtkSlider.value > 0) || (CnInputManager.GetButtonDown("SpecialAttack") && timer >= timeBetweenAttacks && specialAtkSlider.value > 0)) {

			isAttacking = true;
			PlayAtkSound (specialClip);

			damagePerHit = 100;
			playerEffects.showSpecialAttackEffect ("Skill_B");
			specialAtkSlider.value -= 20;
			Attack ();

		}

		// If the user is attack
		if (isAttacking){

			atkEffectTimer += Time.deltaTime;

			// Reset the attack effect
			if(atkEffectTimer > 1f){

				atkEffectTimer = 0;
 			    isAttacking = false;
				playerEffects.normalAttackEffect.SetActive (false);
				playerEffects.specialAttackEffect.SetActive (false);
				
			}
			
		}
	
	}

	// Function to play attack sound
	void PlayAtkSound(AudioClip clip){
		audio.PlayOneShot (clip, 0.5f);
	}

	void Attack ()
	{
		// Decrease the number of times you can use the weapon
		if (playerPickups.weapon.tag != "weapon_empty") {
			playerPickups.numHits -= 1;
		}

		timer = 0f;

		// Set position and direction of the raycast to match the characters
		shootRay.origin = transform.position;
		shootRay.direction = transform.forward;

		// If it hits the hitable layer
		if(Physics.Raycast (shootRay, out shootHit, 100f, hitableMask))
		{
			// Check the range
			if(shootHit.distance < 4f){

				// Get the enemy script component on the enemy and take off the damage
				EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
				if(enemyHealth != null)
				{
					enemyHealth.HitDamage (damagePerHit);
				}
			}
				
		}
	}
}

