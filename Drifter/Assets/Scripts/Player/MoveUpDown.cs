﻿using UnityEngine;
using System.Collections;

public class MoveUpDown : MonoBehaviour {

	// Help and script from taken from http://answers.unity3d.com/questions/658759/c-how-do-i-animate-my-cloud-to-go-up-and-down.html

	public bool Move = true;    ///gives you control in inspector to trigger it or not
	public Vector3 MoveVector = Vector3.up; //unity already supplies us with a readonly vector representing up and we are just chaching that into MoveVector
	public float MoveRange = 2.0f; //change this to increase/decrease the distance between the highest and lowest points of the bounce
	public float MoveSpeed = 0.5f; //change this to make it faster or slower


	Vector3 startPosition; //used to cache the start position of the transform
	void Start()
	{ 
		startPosition = transform.position;
	}

	void Update()
	{
		if(Move) //bool is checked
			//See if you can work out whats going on here, for your own enjoyment
			transform.position = startPosition + MoveVector * (MoveRange * Mathf.Sin(Time.timeSinceLevelLoad * MoveSpeed));

	}
}
