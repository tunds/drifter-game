﻿using UnityEngine;
using System.Collections;

public class PickUpRotate : MonoBehaviour {

	public float xRotation;
	public float yRotation;
	public float zRotation;

	void FixedUpdate () {
	
		transform.Rotate(new Vector3(xRotation, yRotation, zRotation) * Time.deltaTime);
	}
}
