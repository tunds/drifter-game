﻿using UnityEngine;
using System.Collections;

public class PickupEffects : MonoBehaviour {

	AudioSource audio;

	void Start(){
		audio = GetComponent<AudioSource> ();

	}

	void OnTriggerEnter (Collider hit)
	{
		if(hit.CompareTag("Player")){

			  audio.PlayOneShot (audio.clip, .4f);
		}

	}
}
