﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class GlobalObjectManager : MonoBehaviour {

	private int currentHealth;
	private int special;
	private int numHits;

	public DateTime start;
	public TimeSpan elapsed;

	public bool isLoaded = true;
	public bool isDead = false;

	public float startAudioPosition = 0f;
	public float volume = 1f;

	AudioListener listener;

	// https://www.sitepoint.com/saving-data-between-scenes-in-unity/
	public static GlobalObjectManager Instance;

	void Awake ()   
	{
		// Only create single instance of this class
		if (Instance == null)
		{
			DontDestroyOnLoad(gameObject);
			Instance = this;
		}
		else if (Instance != this)
		{
			Destroy (gameObject);
		}
	}

	void OnLevelWasLoaded(){

		// Listener for general game audio
		listener = Camera.main.GetComponent<AudioListener>();

		// If either one of these components exits then assign the varibles in the other scripts
		if (GameObject.FindGameObjectWithTag ("Player") != null && GameObject.FindGameObjectWithTag ("Timer") != null){
			
			GameObject player = GameObject.FindGameObjectWithTag ("Player");
			GameObject timer = GameObject.FindGameObjectWithTag ("Timer");

	     	player.GetComponent<PlayerHealth> ().currentHealth = GlobalObjectManager.Instance.currentHealth;
			player.GetComponent<PlayerHealth> ().healthSldr.value = GlobalObjectManager.Instance.currentHealth;
			player.GetComponent<PlayerAttack> ().specialAtkSlider.value = (float)GlobalObjectManager.Instance.special;
			player.GetComponent<PlayerPickups> ().numHits = GlobalObjectManager.Instance.numHits;

			timer.GetComponent<GameTimer> ().startTime = GlobalObjectManager.Instance.start;
			timer.GetComponent<GameTimer> ().elapsedTime = GlobalObjectManager.Instance.elapsed;

		}


	}
		
	// Persist the data betweenn scenes
	public void SaveData()
	{
		GameObject player = GameObject.FindGameObjectWithTag ("Player");
		GameObject timer = GameObject.FindGameObjectWithTag ("Timer");

		GlobalObjectManager.Instance.currentHealth = player.GetComponent<PlayerHealth> ().currentHealth;
		GlobalObjectManager.Instance.special = (int)player.GetComponent<PlayerAttack> ().specialAtkSlider.value;
		GlobalObjectManager.Instance.numHits = player.GetComponent<PlayerPickups> ().numHits;

		GlobalObjectManager.Instance.start = timer.GetComponent<GameTimer> ().startTime;
		GlobalObjectManager.Instance.elapsed = timer.GetComponent<GameTimer> ().elapsedTime;

	}
}
