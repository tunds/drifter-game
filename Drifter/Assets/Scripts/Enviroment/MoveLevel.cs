﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MoveLevel : MonoBehaviour {

	public string levelName;
	bool goToLevel = false;
	float timer;

	GlobalObjectManager persistentData;
	AudioSource audio;

	void Start(){
		audio = GetComponent<AudioSource> ();
	}

	void Update(){

		if(goToLevel){

			timer += Time.deltaTime;

			if(timer > 1.1f){

				persistentData = GameObject.FindGameObjectWithTag ("PersistentData").GetComponent<GlobalObjectManager> ();
				persistentData.SaveData ();

				SceneManager.LoadScene (levelName);
			}

		}
	}

	// When the user moves level save the data and load the scene passed into function
	void OnTriggerEnter(Collider collider){
		
		if (collider.CompareTag("Player")){
			audio.PlayOneShot (audio.clip, 1f);
			goToLevel = true;
		}
	}
}
