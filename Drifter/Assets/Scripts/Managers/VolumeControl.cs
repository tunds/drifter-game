﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VolumeControl : MonoBehaviour {

	GlobalObjectManager persistentData;
	public Slider volumeSldr;

	void Start(){
		
		persistentData = GameObject.FindGameObjectWithTag ("PersistentData").GetComponent<GlobalObjectManager> ();
		volumeSldr.value = persistentData.volume;

	}
		
	public void changeVolume(){

		persistentData.volume = GameObject.Find ("Volume").GetComponent<Slider> ().value;
		AudioListener.volume = persistentData.volume;
	}
}
