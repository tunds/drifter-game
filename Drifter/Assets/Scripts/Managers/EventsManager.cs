﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using SQLiter;
using PoqXert.MessageBox;
using UnityEngine.SceneManagement;
using SQLiteDatabase;
using System.Collections.Generic;

public class EventsManager : MonoBehaviour {

	SQLite data;
	//GameObject db;
	GlobalObjectManager persistentData;
	InputField input;
	Text time;
	string name = "";
	SQLiteDB db = SQLiteDB.Instance;

	void Awake(){

		time = GameObject.Find ("Saved Time").GetComponent<Text>();
		persistentData = GameObject.FindGameObjectWithTag ("PersistentData").GetComponent<GlobalObjectManager> ();
		input = GameObject.Find ("InputField").GetComponent<InputField> ();
		time.text = String.Format ("{0:00" + "}:{1:00}:{2:00}", persistentData.elapsed.Hours, persistentData.elapsed.Minutes, persistentData.elapsed.Seconds);

		// Reset that the scene has been loaded and the user is dead
		persistentData.isLoaded = true;
		persistentData.isDead = false;
	}

	void OnApplicationQuit(){
		Debug.Log ("Left scene");
	}

	public void setName(){
		name = input.text;
	}


	public void saveTime(){

		if (name != "") {

			Button btn = GameObject.Find ("Save").GetComponent<Button>();
			btn.interactable = false;

			List<SQLiteDB.DB_DataPair> dataPairList = new List<SQLiteDB.DB_DataPair>();
			SQLiteDB.DB_DataPair data = new SQLiteDB.DB_DataPair();

			data.fieldName = "Name";
			data.value = name;
			dataPairList.Add(data);


			data.fieldName = "SurvivalTime";
			data.value = persistentData.elapsed.Ticks.ToString();
			dataPairList.Add(data);

			// insert into Users - first row
			int i = db.Insert("Scores",dataPairList);

			if (i > 0)
			{
				Debug.Log("Record Inserted!");
			}

			// Create instance of db script and destroy it after saving data
//			data = db.AddComponent (typeof(SQLite)) as SQLite;
//			data.InsertPlayer (name, persistentData.elapsed.Ticks);
//			Destroy (data);

		} else {

			MsgBox.Show(0,"You must enter a name in order to save a score",closeMessageBox,true,"OK","","");

		}
			
	}

	public void closeMessageBox(int id, DialogResult btn){
		MsgBox.Close ();
	}

	public void replayGame(){

		SceneManager.LoadScene ("Main");
	}

	public void homeGame(){

		// Reset the position of the audio since the clips are different
		SceneManager.LoadScene ("Home");
	}
}
