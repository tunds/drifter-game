﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyManager : MonoBehaviour {

	public Slider healthSldr;       
	public GameObject item;               
	public float spawnTime = 2f;            
	public Transform[] spawnPoints;

	void Start ()
	{
		// Call this function every spawntime also uses spawntime as a delay
		InvokeRepeating ("SpawnItem", spawnTime, spawnTime);
	}


	void SpawnItem ()
	{

		if(healthSldr.value <= 0f)
			return;

		// Get a random spawn point and create an object with the rotation and position of it
		int spawnPointIndex = Random.Range (0, spawnPoints.Length);
		Instantiate (item, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
	}
}
