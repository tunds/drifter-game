﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

	GlobalObjectManager persistentData;

	AudioSource gameBGSound;

	void Awake(){

		persistentData = GameObject.FindGameObjectWithTag ("PersistentData").GetComponent<GlobalObjectManager> ();
		gameBGSound = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<AudioSource> ();

		// Check if it's playing
		if(!gameBGSound.isPlaying){

			// Reset and play from the persisted data position with a small delay
			gameBGSound.Stop ();
			gameBGSound.timeSamples = (int)persistentData.startAudioPosition;
			gameBGSound.PlayDelayed (0.05f);

		}

	}

	void Update(){
		
		persistentData.startAudioPosition = gameBGSound.timeSamples;

	}

	public void goGame(){
		SceneManager.LoadScene ("Main");
	}

	public void goSettings(){
		SceneManager.LoadScene ("Settings");
	}

	public void goLeaderboards(){
		SceneManager.LoadScene ("Leaderboard");
	}

	public void goInstructionsAims(){
		SceneManager.LoadScene ("InstructionAims");
	}

	public void goInstructionsPickups(){
		SceneManager.LoadScene ("InstructionPickups");
	}

	public void goInstructionsMovements(){
		SceneManager.LoadScene ("InstructionsMovements");
	}

	public void goHome(){
		SceneManager.LoadScene ("Home");
	}
}
