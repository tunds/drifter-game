﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ToggleMobileControls : MonoBehaviour {

	public GameObject joystick;
	public GameObject attackButton;
	public GameObject specialAttackButton;

	public Slider specialSldr;

	bool isDevice = false;
	public Color disabledColor = new Color(1f,1F, 1F, 0.5F);
	public Color enabledColor = new Color(1f, 1f, 1f, 1f);

	// Use this for initialization
	void Start () {

		if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer){
			isDevice = true;
		}

		if (!isDevice){
			joystick.SetActive (false);
			attackButton.SetActive (false);
			specialAttackButton.SetActive (false);
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (specialSldr.value == 0) {
			specialAttackButton.GetComponent<Image> ().color = disabledColor;
		} else {
			specialAttackButton.GetComponent<Image> ().color = enabledColor;
		}

	}
}
