﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PickUpManager: MonoBehaviour {

	public Slider healthSldr;       
	public GameObject item;               
	public float spawnTime = 2f;            
	public Transform[] spawnPoints;

	void Start ()
	{
		// Call this function every spawntime also uses spawntime as a delay
		InvokeRepeating ("SpawnItem", spawnTime, spawnTime);
	}


	void SpawnItem ()
	{

		// If the item already exists in the scene do nothing
		if (GameObject.FindWithTag (item.tag))
			return;

		// If the user is dead do nothing
		if(healthSldr.value <= 0f)
			return;

		// Get a random spawn point and create an object with the rotation and position of it
		int spawnPointIndex = Random.Range (0, spawnPoints.Length);
		Instantiate (item, spawnPoints[spawnPointIndex].position, item.transform.rotation);
	}
} 
