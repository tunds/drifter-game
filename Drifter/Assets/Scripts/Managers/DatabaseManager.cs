﻿using UnityEngine;
using System.Collections;
using SQLiteDatabase;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class DatabaseManager : MonoBehaviour {

	SQLiteDB db = SQLiteDB.Instance;

	public Text[] names; 
	public Text[] scores;

	// Use this for initialization
	void Start () {
		// set database location (directory)
		db.DBLocation = Application.persistentDataPath;
		Debug.Log ("Database Directory : "+db.DBLocation);
		CreateDB ();
		GetScoresData ();
	}

	// Events
	void OnEnable()
	{
		SQLiteEventListener.onError += OnError;
	}

	void OnDisable()
	{
		SQLiteEventListener.onError -= OnError;
	}

	void OnError(string err)
	{
		Debug.Log (err);
	}

	// create database and table
	void CreateDB()
	{

		// use default database from StreamingAssets folder
		if (db.Exists) {
			db.ConnectToDefaultDatabase ("ScoresDatabase.db",false);
		} else {
			db.ConnectToDefaultDatabase ("ScoresDatabase.db",true);
		}

		if(!db.IsTableExists("Scores")){
			CreateTable ();
		}
	}

	bool CreateTable()
	{
		// database created successfuly then create table
		if (db.Exists) {
			// Create table :: schema
			// you can create table when you want
			DBSchema schema = new DBSchema ("Scores");
			schema.AddField ("Name", SQLiteDB.DB_DataType.DB_VARCHAR, 100, false, false, false);
			schema.AddField ("SurvivalTime", SQLiteDB.DB_DataType.DB_INT, 100, false, false, false);
			return db.CreateTable (schema);
		}

		return false;
	}

	void AddNewScore(string name, int time)
	{
		List<SQLiteDB.DB_DataPair> dataPairList = new List<SQLiteDB.DB_DataPair>();
		SQLiteDB.DB_DataPair data = new SQLiteDB.DB_DataPair();
		// Insert first row
		// first field
		data.fieldName = "Name";
		data.value = name;
		dataPairList.Add(data);

		// second field
		data.fieldName = "SurvivalTime";
		data.value = time.ToString();
		dataPairList.Add(data);

		// insert into Users - first row
		int i = db.Insert("Scores",dataPairList);

		if (i > 0)
		{
			Debug.Log("Record Inserted!");

		}

	}
	void GetScoresData(){

		if (scores != null && names != null) {

			DBReader reader = db.Select("Select * from Scores ORDER BY SurvivalTime DESC LIMIT 3");
			int i = 0;
			while (reader != null && reader.Read () && i < 3) {
				
				names[i].text = reader.GetStringValue("Name");

				TimeSpan time = TimeSpan.FromTicks(reader.GetIntValue("SurvivalTime"));
				scores[i].text = "Time: " + String.Format("{0:00" + "}:{1:00}:{2:00}", time.Hours, time.Minutes, time.Seconds);

				i++;
			}
		}


	}

	// use this to avoid any lock on database, otherwise restart editor or application after each run
	void OnApplicationQuit()
	{
		// release all resource using by database.
		db.Dispose ();
	}
}
