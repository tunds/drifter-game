﻿using UnityEngine;
using System.Collections;

public class player : MonoBehaviour {
	[System.Serializable]
	public class  AniClip {
		public AnimationClip idle;
		public AnimationClip walk;
		public AnimationClip attack1;
		public AnimationClip attack2;
		public AnimationClip skillA;
		public AnimationClip skillB;
		public AnimationClip skillC;
		public AnimationClip stun;		
		public AnimationClip hit;

		public AnimationClip idle_weapon;
		public AnimationClip walk_weapon;
		public AnimationClip attack_weapon;
		public AnimationClip hit_weapon;

	}
	

	public AniClip ani;
	public Animation  playerAni ;

	int weaponId;
	bool weaponVisible=false;
	public GameObject weapon1;
	public GameObject weapon2;
	public GameObject weapon3;

	public GameObject FXattack1;
	public GameObject FXattack2;
	public GameObject FXattack_weapon;
	public GameObject FXskillA;
	public GameObject FXskillB;
	public GameObject FXskillC;
	public GameObject FXsturn;
	public GameObject FXhit;



	void Start () {
		disableFX ();
	}

	void OnGUI(){
		if (GUI.Button (new Rect (10, 10, 150, 30), "Idle")) {
			disableFX ();
			playerAni.CrossFade ( ani.idle.name, 0.1f );
			weaponVisible=false;
		} else if (GUI.Button (new Rect (10, 50, 150, 30), "Attack1")) {
			disableFX ();
			playerAni.CrossFade ( ani.attack1.name, 0.1f );
			FXattack1.SetActive (true);	
			weaponVisible=false;
		} else if (GUI.Button (new Rect (10, 80, 150, 30), "Attack2")) {
			disableFX ();
			playerAni.CrossFade ( ani.attack2.name, 0.1f );
			FXattack2.SetActive (true);
			weaponVisible=false;
		} else if (GUI.Button (new Rect (10, 120, 150, 30), "Skill A")) {
			disableFX ();
			playerAni.CrossFade ( ani.skillA.name, 0.1f );
			FXskillA.SetActive (true);
			weaponVisible=false;
		} else if (GUI.Button (new Rect (10, 150, 150, 30), "Skill B")) {
			disableFX ();
			playerAni.CrossFade ( ani.skillB.name, 0.1f );
			FXskillB.SetActive (true);		
			weaponVisible=false;
		} else if (GUI.Button (new Rect (10, 180, 150, 30), "Skill C")) {
			disableFX ();
			playerAni.CrossFade ( ani.skillC.name, 0.1f );
			FXskillC.SetActive (true);
			weaponVisible=false;

		} else if (GUI.Button (new Rect (10, 220, 150, 30), "Stun")) {
			disableFX ();
			playerAni.CrossFade ( ani.stun.name, 0.1f );
			FXsturn.SetActive (true);		
			weaponVisible=false;
		} else if (GUI.Button (new Rect (10, 250, 150, 30), "Walk")) {
			disableFX ();
			playerAni.CrossFade ( ani.walk.name, 0.1f );
			weaponVisible=false;
		} else if (GUI.Button (new Rect (10, 280, 150, 30), "Hit")) {
			disableFX ();
			playerAni.CrossFade ( ani.hit.name, 0.1f );
			FXhit.SetActive (true);
			weaponVisible=false;

		}else if (GUI.Button (new Rect (10, 320, 150, 30), "Idle(weapon)")) {
			disableFX ();			
			weaponVisible = true;
			weaponSel();
			playerAni.CrossFade ( ani.idle_weapon.name, 0.1f );		
		}else if (GUI.Button (new Rect (10, 350, 150, 30), "Walk(weapon)")) {
			disableFX ();			
			weaponVisible = true;
			weaponSel();
			playerAni.CrossFade ( ani.walk_weapon.name, 0.1f );
		}else if (GUI.Button (new Rect (10, 380, 150, 30), "Attack(weapon)")) {
			disableFX ();			
			weaponVisible = true;
			weaponSel();
			playerAni.CrossFade ( ani.attack_weapon.name, 0.1f );
			FXattack_weapon.SetActive (true);
		}else if (GUI.Button (new Rect (10, 410, 150, 30), "Hit(weapon)")) {
			disableFX ();			
			weaponVisible = true;
			weaponSel();
			playerAni.CrossFade ( ani.hit_weapon.name, 0.1f );
			FXhit.SetActive (true);
		}


		if (GUI.Button (new Rect (160, 320, 80, 30), "weapon1")) {
								weaponId = 0;
								weaponSel ();
		} else if (GUI.Button (new Rect (160, 350, 80, 30), "weapon2")) {
								weaponId = 1;	
								weaponSel ();
		} else if (GUI.Button (new Rect (160, 380, 80, 30), "weapon3")) {
								weaponId = 2;
								weaponSel ();		
						}






	}

	void weaponSel(){
		if (weaponVisible == true) {
						switch (weaponId) {
			
						case 0:			
								weapon1.SetActive (true);
								weapon2.SetActive (false);
								weapon3.SetActive (false);		
								break;
			
						case 1:
								weapon1.SetActive (false);
								weapon2.SetActive (true);
								weapon3.SetActive (false);				
								break;
			
						case 2:
								weapon1.SetActive (false);
								weapon2.SetActive (false);
								weapon3.SetActive (true);	
								break;
						}
				}
		

		
		
	}
	
	void disableFX(){
		weapon1.SetActive (false);
		weapon2.SetActive (false);
		weapon3.SetActive (false);
		FXattack1.SetActive (false);
		FXattack2.SetActive (false);
		FXsturn.SetActive (false);
		FXattack_weapon.SetActive (false);
		FXskillA.SetActive (false);
		FXskillB.SetActive (false);
		FXskillC.SetActive (false);
		FXhit.SetActive (false);

		}


}
