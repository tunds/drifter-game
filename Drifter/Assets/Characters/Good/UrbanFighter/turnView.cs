﻿using UnityEngine;
using System.Collections;

public class turnView : MonoBehaviour {


	public GameObject chr_skin1;
	public GameObject chr_skin2;

	void Start() {
		chr_skin1.SetActive(true);
		chr_skin2.SetActive(false);
		}

void Update() {
		if (Input.GetKey(KeyCode.Space)) 
			transform.Rotate(Vector3.up*1.0f);
		}


	void OnGUI(){
		if (GUI.Button (new Rect (400, 10, 150, 30), "Character Skin A")) {
			chr_skin1.SetActive(true);
			chr_skin2.SetActive(false);
		} else if (GUI.Button (new Rect (400, 50, 150, 30), "Character Skin B")) {
			chr_skin1.SetActive(false);
			chr_skin2.SetActive(true);
		}	
	}

}
