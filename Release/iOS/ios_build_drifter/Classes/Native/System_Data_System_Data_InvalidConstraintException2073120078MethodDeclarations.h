﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.InvalidConstraintException
struct InvalidConstraintException_t2073120078;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void System.Data.InvalidConstraintException::.ctor()
extern "C"  void InvalidConstraintException__ctor_m3369294070 (InvalidConstraintException_t2073120078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.InvalidConstraintException::.ctor(System.String)
extern "C"  void InvalidConstraintException__ctor_m2229077452 (InvalidConstraintException_t2073120078 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.InvalidConstraintException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void InvalidConstraintException__ctor_m2261908791 (InvalidConstraintException_t2073120078 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
