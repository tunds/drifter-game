﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalObjectManager
struct GlobalObjectManager_t849077355;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalObjectManager::.ctor()
extern "C"  void GlobalObjectManager__ctor_m2975135584 (GlobalObjectManager_t849077355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalObjectManager::Awake()
extern "C"  void GlobalObjectManager_Awake_m3212740803 (GlobalObjectManager_t849077355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalObjectManager::OnLevelWasLoaded()
extern "C"  void GlobalObjectManager_OnLevelWasLoaded_m3732057101 (GlobalObjectManager_t849077355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalObjectManager::SaveData()
extern "C"  void GlobalObjectManager_SaveData_m3896486251 (GlobalObjectManager_t849077355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
