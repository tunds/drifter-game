﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLiter.LoomManager/LoomDispatcher
struct LoomDispatcher_t1654289480;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void SQLiter.LoomManager/LoomDispatcher::.ctor()
extern "C"  void LoomDispatcher__ctor_m1655812988 (LoomDispatcher_t1654289480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.LoomManager/LoomDispatcher::QueueOnMainThread(System.Action)
extern "C"  void LoomDispatcher_QueueOnMainThread_m4120137776 (LoomDispatcher_t1654289480 * __this, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.LoomManager/LoomDispatcher::Update()
extern "C"  void LoomDispatcher_Update_m1517457169 (LoomDispatcher_t1654289480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
