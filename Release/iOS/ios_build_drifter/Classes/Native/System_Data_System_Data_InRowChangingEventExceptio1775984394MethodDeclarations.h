﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.InRowChangingEventException
struct InRowChangingEventException_t1775984394;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void System.Data.InRowChangingEventException::.ctor()
extern "C"  void InRowChangingEventException__ctor_m2928129310 (InRowChangingEventException_t1775984394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.InRowChangingEventException::.ctor(System.String)
extern "C"  void InRowChangingEventException__ctor_m2193992356 (InRowChangingEventException_t1775984394 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.InRowChangingEventException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void InRowChangingEventException__ctor_m1854590303 (InRowChangingEventException_t1775984394 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
