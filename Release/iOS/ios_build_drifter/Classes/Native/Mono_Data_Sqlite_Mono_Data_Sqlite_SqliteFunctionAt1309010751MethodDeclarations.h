﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteFunctionAttribute
struct SqliteFunctionAttribute_t1309010751;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_FunctionType1981583289.h"

// System.String Mono.Data.Sqlite.SqliteFunctionAttribute::get_Name()
extern "C"  String_t* SqliteFunctionAttribute_get_Name_m683501400 (SqliteFunctionAttribute_t1309010751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteFunctionAttribute::get_Arguments()
extern "C"  int32_t SqliteFunctionAttribute_get_Arguments_m1773307826 (SqliteFunctionAttribute_t1309010751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.FunctionType Mono.Data.Sqlite.SqliteFunctionAttribute::get_FuncType()
extern "C"  int32_t SqliteFunctionAttribute_get_FuncType_m4059995904 (SqliteFunctionAttribute_t1309010751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
