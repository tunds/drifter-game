﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioListener
struct AudioListener_t1735598807;
// GlobalObjectManager
struct GlobalObjectManager_t849077355;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalObjectManager
struct  GlobalObjectManager_t849077355  : public MonoBehaviour_t3012272455
{
public:
	// System.Int32 GlobalObjectManager::currentHealth
	int32_t ___currentHealth_2;
	// System.Int32 GlobalObjectManager::special
	int32_t ___special_3;
	// System.Int32 GlobalObjectManager::numHits
	int32_t ___numHits_4;
	// System.DateTime GlobalObjectManager::start
	DateTime_t339033936  ___start_5;
	// System.TimeSpan GlobalObjectManager::elapsed
	TimeSpan_t763862892  ___elapsed_6;
	// System.Boolean GlobalObjectManager::isLoaded
	bool ___isLoaded_7;
	// System.Boolean GlobalObjectManager::isDead
	bool ___isDead_8;
	// System.Single GlobalObjectManager::startAudioPosition
	float ___startAudioPosition_9;
	// System.Single GlobalObjectManager::volume
	float ___volume_10;
	// UnityEngine.AudioListener GlobalObjectManager::listener
	AudioListener_t1735598807 * ___listener_11;

public:
	inline static int32_t get_offset_of_currentHealth_2() { return static_cast<int32_t>(offsetof(GlobalObjectManager_t849077355, ___currentHealth_2)); }
	inline int32_t get_currentHealth_2() const { return ___currentHealth_2; }
	inline int32_t* get_address_of_currentHealth_2() { return &___currentHealth_2; }
	inline void set_currentHealth_2(int32_t value)
	{
		___currentHealth_2 = value;
	}

	inline static int32_t get_offset_of_special_3() { return static_cast<int32_t>(offsetof(GlobalObjectManager_t849077355, ___special_3)); }
	inline int32_t get_special_3() const { return ___special_3; }
	inline int32_t* get_address_of_special_3() { return &___special_3; }
	inline void set_special_3(int32_t value)
	{
		___special_3 = value;
	}

	inline static int32_t get_offset_of_numHits_4() { return static_cast<int32_t>(offsetof(GlobalObjectManager_t849077355, ___numHits_4)); }
	inline int32_t get_numHits_4() const { return ___numHits_4; }
	inline int32_t* get_address_of_numHits_4() { return &___numHits_4; }
	inline void set_numHits_4(int32_t value)
	{
		___numHits_4 = value;
	}

	inline static int32_t get_offset_of_start_5() { return static_cast<int32_t>(offsetof(GlobalObjectManager_t849077355, ___start_5)); }
	inline DateTime_t339033936  get_start_5() const { return ___start_5; }
	inline DateTime_t339033936 * get_address_of_start_5() { return &___start_5; }
	inline void set_start_5(DateTime_t339033936  value)
	{
		___start_5 = value;
	}

	inline static int32_t get_offset_of_elapsed_6() { return static_cast<int32_t>(offsetof(GlobalObjectManager_t849077355, ___elapsed_6)); }
	inline TimeSpan_t763862892  get_elapsed_6() const { return ___elapsed_6; }
	inline TimeSpan_t763862892 * get_address_of_elapsed_6() { return &___elapsed_6; }
	inline void set_elapsed_6(TimeSpan_t763862892  value)
	{
		___elapsed_6 = value;
	}

	inline static int32_t get_offset_of_isLoaded_7() { return static_cast<int32_t>(offsetof(GlobalObjectManager_t849077355, ___isLoaded_7)); }
	inline bool get_isLoaded_7() const { return ___isLoaded_7; }
	inline bool* get_address_of_isLoaded_7() { return &___isLoaded_7; }
	inline void set_isLoaded_7(bool value)
	{
		___isLoaded_7 = value;
	}

	inline static int32_t get_offset_of_isDead_8() { return static_cast<int32_t>(offsetof(GlobalObjectManager_t849077355, ___isDead_8)); }
	inline bool get_isDead_8() const { return ___isDead_8; }
	inline bool* get_address_of_isDead_8() { return &___isDead_8; }
	inline void set_isDead_8(bool value)
	{
		___isDead_8 = value;
	}

	inline static int32_t get_offset_of_startAudioPosition_9() { return static_cast<int32_t>(offsetof(GlobalObjectManager_t849077355, ___startAudioPosition_9)); }
	inline float get_startAudioPosition_9() const { return ___startAudioPosition_9; }
	inline float* get_address_of_startAudioPosition_9() { return &___startAudioPosition_9; }
	inline void set_startAudioPosition_9(float value)
	{
		___startAudioPosition_9 = value;
	}

	inline static int32_t get_offset_of_volume_10() { return static_cast<int32_t>(offsetof(GlobalObjectManager_t849077355, ___volume_10)); }
	inline float get_volume_10() const { return ___volume_10; }
	inline float* get_address_of_volume_10() { return &___volume_10; }
	inline void set_volume_10(float value)
	{
		___volume_10 = value;
	}

	inline static int32_t get_offset_of_listener_11() { return static_cast<int32_t>(offsetof(GlobalObjectManager_t849077355, ___listener_11)); }
	inline AudioListener_t1735598807 * get_listener_11() const { return ___listener_11; }
	inline AudioListener_t1735598807 ** get_address_of_listener_11() { return &___listener_11; }
	inline void set_listener_11(AudioListener_t1735598807 * value)
	{
		___listener_11 = value;
		Il2CppCodeGenWriteBarrier(&___listener_11, value);
	}
};

struct GlobalObjectManager_t849077355_StaticFields
{
public:
	// GlobalObjectManager GlobalObjectManager::Instance
	GlobalObjectManager_t849077355 * ___Instance_12;

public:
	inline static int32_t get_offset_of_Instance_12() { return static_cast<int32_t>(offsetof(GlobalObjectManager_t849077355_StaticFields, ___Instance_12)); }
	inline GlobalObjectManager_t849077355 * get_Instance_12() const { return ___Instance_12; }
	inline GlobalObjectManager_t849077355 ** get_address_of_Instance_12() { return &___Instance_12; }
	inline void set_Instance_12(GlobalObjectManager_t849077355 * value)
	{
		___Instance_12 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
