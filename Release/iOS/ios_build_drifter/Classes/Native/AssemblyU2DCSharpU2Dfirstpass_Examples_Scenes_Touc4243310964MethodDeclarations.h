﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Examples.Scenes.TouchpadCamera.RotationConstraint
struct RotationConstraint_t4243310964;

#include "codegen/il2cpp-codegen.h"

// System.Void Examples.Scenes.TouchpadCamera.RotationConstraint::.ctor()
extern "C"  void RotationConstraint__ctor_m1279824105 (RotationConstraint_t4243310964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Examples.Scenes.TouchpadCamera.RotationConstraint::Awake()
extern "C"  void RotationConstraint_Awake_m1517429324 (RotationConstraint_t4243310964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Examples.Scenes.TouchpadCamera.RotationConstraint::LateUpdate()
extern "C"  void RotationConstraint_LateUpdate_m3774866186 (RotationConstraint_t4243310964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
