﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.TableAdapterSchemaInfo
struct TableAdapterSchemaInfo_t1131857475;
// System.Data.Common.DbProviderFactory
struct DbProviderFactory_t2435213707;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_Common_DbProviderFactory2435213707.h"

// System.Void System.Data.TableAdapterSchemaInfo::.ctor(System.Data.Common.DbProviderFactory)
extern "C"  void TableAdapterSchemaInfo__ctor_m2798241732 (TableAdapterSchemaInfo_t1131857475 * __this, DbProviderFactory_t2435213707 * ___provider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
