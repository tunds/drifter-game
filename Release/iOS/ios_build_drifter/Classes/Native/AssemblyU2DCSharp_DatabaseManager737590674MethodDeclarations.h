﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DatabaseManager
struct DatabaseManager_t737590674;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void DatabaseManager::.ctor()
extern "C"  void DatabaseManager__ctor_m1163406873 (DatabaseManager_t737590674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseManager::Start()
extern "C"  void DatabaseManager_Start_m110544665 (DatabaseManager_t737590674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseManager::OnEnable()
extern "C"  void DatabaseManager_OnEnable_m739653325 (DatabaseManager_t737590674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseManager::OnDisable()
extern "C"  void DatabaseManager_OnDisable_m1895353856 (DatabaseManager_t737590674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseManager::OnError(System.String)
extern "C"  void DatabaseManager_OnError_m532773922 (DatabaseManager_t737590674 * __this, String_t* ___err0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseManager::CreateDB()
extern "C"  void DatabaseManager_CreateDB_m1991173893 (DatabaseManager_t737590674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DatabaseManager::CreateTable()
extern "C"  bool DatabaseManager_CreateTable_m2952491549 (DatabaseManager_t737590674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseManager::AddNewScore(System.String,System.Int32)
extern "C"  void DatabaseManager_AddNewScore_m941688607 (DatabaseManager_t737590674 * __this, String_t* ___name0, int32_t ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseManager::GetScoresData()
extern "C"  void DatabaseManager_GetScoresData_m1846108248 (DatabaseManager_t737590674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseManager::OnApplicationQuit()
extern "C"  void DatabaseManager_OnApplicationQuit_m3294051863 (DatabaseManager_t737590674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
