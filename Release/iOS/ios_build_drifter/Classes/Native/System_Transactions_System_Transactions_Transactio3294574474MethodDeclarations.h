﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Transactions.TransactionInformation
struct TransactionInformation_t3294574474;

#include "codegen/il2cpp-codegen.h"
#include "System_Transactions_System_Transactions_Transactio1562036300.h"

// System.Void System.Transactions.TransactionInformation::.ctor()
extern "C"  void TransactionInformation__ctor_m49122873 (TransactionInformation_t3294574474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Transactions.TransactionStatus System.Transactions.TransactionInformation::get_Status()
extern "C"  int32_t TransactionInformation_get_Status_m4243117603 (TransactionInformation_t3294574474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.TransactionInformation::set_Status(System.Transactions.TransactionStatus)
extern "C"  void TransactionInformation_set_Status_m2073488130 (TransactionInformation_t3294574474 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
