﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.RowUpdatingEventArgs
struct RowUpdatingEventArgs_t2786481031;
// System.Data.IDbCommand
struct IDbCommand_t2345198679;
// System.Exception
struct Exception_t1967233988;
// System.Data.DataRow
struct DataRow_t3654701923;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "System_Data_System_Data_StatementType1752600284.h"
#include "System_Data_System_Data_UpdateStatus4065415150.h"

// System.Data.IDbCommand System.Data.Common.RowUpdatingEventArgs::get_Command()
extern "C"  Il2CppObject * RowUpdatingEventArgs_get_Command_m2327234181 (RowUpdatingEventArgs_t2786481031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.RowUpdatingEventArgs::set_Command(System.Data.IDbCommand)
extern "C"  void RowUpdatingEventArgs_set_Command_m2843037566 (RowUpdatingEventArgs_t2786481031 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.RowUpdatingEventArgs::set_Errors(System.Exception)
extern "C"  void RowUpdatingEventArgs_set_Errors_m3280467969 (RowUpdatingEventArgs_t2786481031 * __this, Exception_t1967233988 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataRow System.Data.Common.RowUpdatingEventArgs::get_Row()
extern "C"  DataRow_t3654701923 * RowUpdatingEventArgs_get_Row_m3287031026 (RowUpdatingEventArgs_t2786481031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.StatementType System.Data.Common.RowUpdatingEventArgs::get_StatementType()
extern "C"  int32_t RowUpdatingEventArgs_get_StatementType_m3205496648 (RowUpdatingEventArgs_t2786481031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.RowUpdatingEventArgs::set_Status(System.Data.UpdateStatus)
extern "C"  void RowUpdatingEventArgs_set_Status_m2534092568 (RowUpdatingEventArgs_t2786481031 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
