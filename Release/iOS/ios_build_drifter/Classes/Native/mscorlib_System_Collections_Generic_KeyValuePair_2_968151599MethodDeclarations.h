﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24054610746MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3983702470(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t968151599 *, int64_t, AggregateData_t2045614569 *, const MethodInfo*))KeyValuePair_2__ctor_m606127727_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::get_Key()
#define KeyValuePair_2_get_Key_m1942940514(__this, method) ((  int64_t (*) (KeyValuePair_2_t968151599 *, const MethodInfo*))KeyValuePair_2_get_Key_m1653476505_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2029870243(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t968151599 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Key_m3195669082_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::get_Value()
#define KeyValuePair_2_get_Value_m2637963663(__this, method) ((  AggregateData_t2045614569 * (*) (KeyValuePair_2_t968151599 *, const MethodInfo*))KeyValuePair_2_get_Value_m2983326873_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3225659171(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t968151599 *, AggregateData_t2045614569 *, const MethodInfo*))KeyValuePair_2_set_Value_m2008326490_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::ToString()
#define KeyValuePair_2_ToString_m3261866655(__this, method) ((  String_t* (*) (KeyValuePair_2_t968151599 *, const MethodInfo*))KeyValuePair_2_ToString_m2061720648_gshared)(__this, method)
