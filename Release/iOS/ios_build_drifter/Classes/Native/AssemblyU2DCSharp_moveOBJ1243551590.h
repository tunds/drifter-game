﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// moveOBJ
struct  moveOBJ_t1243551590  : public MonoBehaviour_t3012272455
{
public:
	// System.Single moveOBJ::Xtansform
	float ___Xtansform_2;
	// System.Single moveOBJ::Ytansform
	float ___Ytansform_3;
	// System.Single moveOBJ::Ztansform
	float ___Ztansform_4;

public:
	inline static int32_t get_offset_of_Xtansform_2() { return static_cast<int32_t>(offsetof(moveOBJ_t1243551590, ___Xtansform_2)); }
	inline float get_Xtansform_2() const { return ___Xtansform_2; }
	inline float* get_address_of_Xtansform_2() { return &___Xtansform_2; }
	inline void set_Xtansform_2(float value)
	{
		___Xtansform_2 = value;
	}

	inline static int32_t get_offset_of_Ytansform_3() { return static_cast<int32_t>(offsetof(moveOBJ_t1243551590, ___Ytansform_3)); }
	inline float get_Ytansform_3() const { return ___Ytansform_3; }
	inline float* get_address_of_Ytansform_3() { return &___Ytansform_3; }
	inline void set_Ytansform_3(float value)
	{
		___Ytansform_3 = value;
	}

	inline static int32_t get_offset_of_Ztansform_4() { return static_cast<int32_t>(offsetof(moveOBJ_t1243551590, ___Ztansform_4)); }
	inline float get_Ztansform_4() const { return ___Ztansform_4; }
	inline float* get_address_of_Ztansform_4() { return &___Ztansform_4; }
	inline void set_Ztansform_4(float value)
	{
		___Ztansform_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
