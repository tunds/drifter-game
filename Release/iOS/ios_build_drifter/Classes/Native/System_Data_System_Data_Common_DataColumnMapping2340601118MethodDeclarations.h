﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DataColumnMapping
struct DataColumnMapping_t2340601118;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Data.DataColumn
struct DataColumn_t3354469747;
// System.Data.DataTable
struct DataTable_t2176726999;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "System_Data_System_Data_DataTable2176726999.h"
#include "mscorlib_System_Type2779229935.h"
#include "System_Data_System_Data_MissingSchemaAction2534824272.h"

// System.Void System.Data.Common.DataColumnMapping::.ctor(System.String,System.String)
extern "C"  void DataColumnMapping__ctor_m565899704 (DataColumnMapping_t2340601118 * __this, String_t* ___sourceColumn0, String_t* ___dataSetColumn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Data.Common.DataColumnMapping::System.ICloneable.Clone()
extern "C"  Il2CppObject * DataColumnMapping_System_ICloneable_Clone_m742775467 (DataColumnMapping_t2340601118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DataColumnMapping::get_DataSetColumn()
extern "C"  String_t* DataColumnMapping_get_DataSetColumn_m808905414 (DataColumnMapping_t2340601118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DataColumnMapping::get_SourceColumn()
extern "C"  String_t* DataColumnMapping_get_SourceColumn_m4221159867 (DataColumnMapping_t2340601118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataColumn System.Data.Common.DataColumnMapping::GetDataColumnBySchemaAction(System.Data.DataTable,System.Type,System.Data.MissingSchemaAction)
extern "C"  DataColumn_t3354469747 * DataColumnMapping_GetDataColumnBySchemaAction_m623919904 (DataColumnMapping_t2340601118 * __this, DataTable_t2176726999 * ___dataTable0, Type_t * ___dataType1, int32_t ___schemaAction2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DataColumnMapping::ToString()
extern "C"  String_t* DataColumnMapping_ToString_m2887016973 (DataColumnMapping_t2340601118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
