﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t3875263730;

#include "mscorlib_System_MarshalByRefObject2055500882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DbParameter
struct  DbParameter_t3306161371  : public MarshalByRefObject_t2055500882
{
public:

public:
};

struct DbParameter_t3306161371_StaticFields
{
public:
	// System.Collections.Hashtable System.Data.Common.DbParameter::dbTypeMapping
	Hashtable_t3875263730 * ___dbTypeMapping_1;

public:
	inline static int32_t get_offset_of_dbTypeMapping_1() { return static_cast<int32_t>(offsetof(DbParameter_t3306161371_StaticFields, ___dbTypeMapping_1)); }
	inline Hashtable_t3875263730 * get_dbTypeMapping_1() const { return ___dbTypeMapping_1; }
	inline Hashtable_t3875263730 ** get_address_of_dbTypeMapping_1() { return &___dbTypeMapping_1; }
	inline void set_dbTypeMapping_1(Hashtable_t3875263730 * value)
	{
		___dbTypeMapping_1 = value;
		Il2CppCodeGenWriteBarrier(&___dbTypeMapping_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
