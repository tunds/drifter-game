﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>
struct ListValues_t1500937207;
// System.Collections.Generic.SortedList`2<System.Object,System.Object>
struct SortedList_2_t2672481741;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Array
struct Il2CppArray;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Array2840145358.h"

// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C"  void ListValues__ctor_m919525363_gshared (ListValues_t1500937207 * __this, SortedList_2_t2672481741 * ___host0, const MethodInfo* method);
#define ListValues__ctor_m919525363(__this, ___host0, method) ((  void (*) (ListValues_t1500937207 *, SortedList_2_t2672481741 *, const MethodInfo*))ListValues__ctor_m919525363_gshared)(__this, ___host0, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ListValues_System_Collections_IEnumerable_GetEnumerator_m39657388_gshared (ListValues_t1500937207 * __this, const MethodInfo* method);
#define ListValues_System_Collections_IEnumerable_GetEnumerator_m39657388(__this, method) ((  Il2CppObject * (*) (ListValues_t1500937207 *, const MethodInfo*))ListValues_System_Collections_IEnumerable_GetEnumerator_m39657388_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::Add(TValue)
extern "C"  void ListValues_Add_m2253379861_gshared (ListValues_t1500937207 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListValues_Add_m2253379861(__this, ___item0, method) ((  void (*) (ListValues_t1500937207 *, Il2CppObject *, const MethodInfo*))ListValues_Add_m2253379861_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::Remove(TValue)
extern "C"  bool ListValues_Remove_m2146480130_gshared (ListValues_t1500937207 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ListValues_Remove_m2146480130(__this, ___value0, method) ((  bool (*) (ListValues_t1500937207 *, Il2CppObject *, const MethodInfo*))ListValues_Remove_m2146480130_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::Clear()
extern "C"  void ListValues_Clear_m1298110558_gshared (ListValues_t1500937207 * __this, const MethodInfo* method);
#define ListValues_Clear_m1298110558(__this, method) ((  void (*) (ListValues_t1500937207 *, const MethodInfo*))ListValues_Clear_m1298110558_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ListValues_CopyTo_m798388821_gshared (ListValues_t1500937207 * __this, ObjectU5BU5D_t11523773* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ListValues_CopyTo_m798388821(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ListValues_t1500937207 *, ObjectU5BU5D_t11523773*, int32_t, const MethodInfo*))ListValues_CopyTo_m798388821_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::Contains(TValue)
extern "C"  bool ListValues_Contains_m227488797_gshared (ListValues_t1500937207 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListValues_Contains_m227488797(__this, ___item0, method) ((  bool (*) (ListValues_t1500937207 *, Il2CppObject *, const MethodInfo*))ListValues_Contains_m227488797_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::IndexOf(TValue)
extern "C"  int32_t ListValues_IndexOf_m1140322029_gshared (ListValues_t1500937207 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListValues_IndexOf_m1140322029(__this, ___item0, method) ((  int32_t (*) (ListValues_t1500937207 *, Il2CppObject *, const MethodInfo*))ListValues_IndexOf_m1140322029_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::Insert(System.Int32,TValue)
extern "C"  void ListValues_Insert_m4217134862_gshared (ListValues_t1500937207 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define ListValues_Insert_m4217134862(__this, ___index0, ___item1, method) ((  void (*) (ListValues_t1500937207 *, int32_t, Il2CppObject *, const MethodInfo*))ListValues_Insert_m4217134862_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::RemoveAt(System.Int32)
extern "C"  void ListValues_RemoveAt_m2585773593_gshared (ListValues_t1500937207 * __this, int32_t ___index0, const MethodInfo* method);
#define ListValues_RemoveAt_m2585773593(__this, ___index0, method) ((  void (*) (ListValues_t1500937207 *, int32_t, const MethodInfo*))ListValues_RemoveAt_m2585773593_gshared)(__this, ___index0, method)
// TValue System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ListValues_get_Item_m2884331164_gshared (ListValues_t1500937207 * __this, int32_t ___index0, const MethodInfo* method);
#define ListValues_get_Item_m2884331164(__this, ___index0, method) ((  Il2CppObject * (*) (ListValues_t1500937207 *, int32_t, const MethodInfo*))ListValues_get_Item_m2884331164_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::set_Item(System.Int32,TValue)
extern "C"  void ListValues_set_Item_m503733591_gshared (ListValues_t1500937207 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ListValues_set_Item_m503733591(__this, ___index0, ___value1, method) ((  void (*) (ListValues_t1500937207 *, int32_t, Il2CppObject *, const MethodInfo*))ListValues_set_Item_m503733591_gshared)(__this, ___index0, ___value1, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ListValues_GetEnumerator_m3936758998_gshared (ListValues_t1500937207 * __this, const MethodInfo* method);
#define ListValues_GetEnumerator_m3936758998(__this, method) ((  Il2CppObject* (*) (ListValues_t1500937207 *, const MethodInfo*))ListValues_GetEnumerator_m3936758998_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::get_Count()
extern "C"  int32_t ListValues_get_Count_m3270312679_gshared (ListValues_t1500937207 * __this, const MethodInfo* method);
#define ListValues_get_Count_m3270312679(__this, method) ((  int32_t (*) (ListValues_t1500937207 *, const MethodInfo*))ListValues_get_Count_m3270312679_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::get_IsSynchronized()
extern "C"  bool ListValues_get_IsSynchronized_m3759581570_gshared (ListValues_t1500937207 * __this, const MethodInfo* method);
#define ListValues_get_IsSynchronized_m3759581570(__this, method) ((  bool (*) (ListValues_t1500937207 *, const MethodInfo*))ListValues_get_IsSynchronized_m3759581570_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::get_IsReadOnly()
extern "C"  bool ListValues_get_IsReadOnly_m2797631952_gshared (ListValues_t1500937207 * __this, const MethodInfo* method);
#define ListValues_get_IsReadOnly_m2797631952(__this, method) ((  bool (*) (ListValues_t1500937207 *, const MethodInfo*))ListValues_get_IsReadOnly_m2797631952_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::get_SyncRoot()
extern "C"  Il2CppObject * ListValues_get_SyncRoot_m2903454338_gshared (ListValues_t1500937207 * __this, const MethodInfo* method);
#define ListValues_get_SyncRoot_m2903454338(__this, method) ((  Il2CppObject * (*) (ListValues_t1500937207 *, const MethodInfo*))ListValues_get_SyncRoot_m2903454338_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::CopyTo(System.Array,System.Int32)
extern "C"  void ListValues_CopyTo_m1783765840_gshared (ListValues_t1500937207 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ListValues_CopyTo_m1783765840(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ListValues_t1500937207 *, Il2CppArray *, int32_t, const MethodInfo*))ListValues_CopyTo_m1783765840_gshared)(__this, ___array0, ___arrayIndex1, method)
