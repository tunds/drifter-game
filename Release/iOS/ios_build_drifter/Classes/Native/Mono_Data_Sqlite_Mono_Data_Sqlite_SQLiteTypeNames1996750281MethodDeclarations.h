﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteTypeNames1996750281.h"
#include "mscorlib_System_String968488902.h"
#include "System_Data_System_Data_DbType2586775211.h"

// System.Void Mono.Data.Sqlite.SQLiteTypeNames::.ctor(System.String,System.Data.DbType)
extern "C"  void SQLiteTypeNames__ctor_m3452802464 (SQLiteTypeNames_t1996750281 * __this, String_t* ___newtypeName0, int32_t ___newdataType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct SQLiteTypeNames_t1996750281;
struct SQLiteTypeNames_t1996750281_marshaled_pinvoke;

extern "C" void SQLiteTypeNames_t1996750281_marshal_pinvoke(const SQLiteTypeNames_t1996750281& unmarshaled, SQLiteTypeNames_t1996750281_marshaled_pinvoke& marshaled);
extern "C" void SQLiteTypeNames_t1996750281_marshal_pinvoke_back(const SQLiteTypeNames_t1996750281_marshaled_pinvoke& marshaled, SQLiteTypeNames_t1996750281& unmarshaled);
extern "C" void SQLiteTypeNames_t1996750281_marshal_pinvoke_cleanup(SQLiteTypeNames_t1996750281_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SQLiteTypeNames_t1996750281;
struct SQLiteTypeNames_t1996750281_marshaled_com;

extern "C" void SQLiteTypeNames_t1996750281_marshal_com(const SQLiteTypeNames_t1996750281& unmarshaled, SQLiteTypeNames_t1996750281_marshaled_com& marshaled);
extern "C" void SQLiteTypeNames_t1996750281_marshal_com_back(const SQLiteTypeNames_t1996750281_marshaled_com& marshaled, SQLiteTypeNames_t1996750281& unmarshaled);
extern "C" void SQLiteTypeNames_t1996750281_marshal_com_cleanup(SQLiteTypeNames_t1996750281_marshaled_com& marshaled);
