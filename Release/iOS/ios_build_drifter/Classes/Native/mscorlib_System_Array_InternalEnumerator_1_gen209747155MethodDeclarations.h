﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen209747155.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_System_ComponentModel_ListSortDirection302256652.h"

// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.ListSortDirection>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3698811141_gshared (InternalEnumerator_1_t209747155 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3698811141(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t209747155 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3698811141_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.ListSortDirection>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1322935419_gshared (InternalEnumerator_1_t209747155 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1322935419(__this, method) ((  void (*) (InternalEnumerator_1_t209747155 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1322935419_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.ListSortDirection>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3149864881_gshared (InternalEnumerator_1_t209747155 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3149864881(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t209747155 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3149864881_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.ListSortDirection>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m511999260_gshared (InternalEnumerator_1_t209747155 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m511999260(__this, method) ((  void (*) (InternalEnumerator_1_t209747155 *, const MethodInfo*))InternalEnumerator_1_Dispose_m511999260_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.ListSortDirection>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3733422507_gshared (InternalEnumerator_1_t209747155 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3733422507(__this, method) ((  bool (*) (InternalEnumerator_1_t209747155 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3733422507_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.ComponentModel.ListSortDirection>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2604177518_gshared (InternalEnumerator_1_t209747155 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2604177518(__this, method) ((  int32_t (*) (InternalEnumerator_1_t209747155 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2604177518_gshared)(__this, method)
