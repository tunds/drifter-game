﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t4006040370;

#include "UnityEngine_UnityEngine_ScriptableObject184905905.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PoqXert.MessageBox.MSGBoxStyle
struct  MSGBoxStyle_t1923386386  : public ScriptableObject_t184905905
{
public:
	// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::_captionColor
	Color_t1588175760  ____captionColor_2;
	// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::_backgroundColor
	Color_t1588175760  ____backgroundColor_3;
	// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::_btnYesColor
	Color_t1588175760  ____btnYesColor_4;
	// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::_btnNoColor
	Color_t1588175760  ____btnNoColor_5;
	// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::_btnCancelColor
	Color_t1588175760  ____btnCancelColor_6;
	// UnityEngine.Sprite PoqXert.MessageBox.MSGBoxStyle::_icon
	Sprite_t4006040370 * ____icon_7;

public:
	inline static int32_t get_offset_of__captionColor_2() { return static_cast<int32_t>(offsetof(MSGBoxStyle_t1923386386, ____captionColor_2)); }
	inline Color_t1588175760  get__captionColor_2() const { return ____captionColor_2; }
	inline Color_t1588175760 * get_address_of__captionColor_2() { return &____captionColor_2; }
	inline void set__captionColor_2(Color_t1588175760  value)
	{
		____captionColor_2 = value;
	}

	inline static int32_t get_offset_of__backgroundColor_3() { return static_cast<int32_t>(offsetof(MSGBoxStyle_t1923386386, ____backgroundColor_3)); }
	inline Color_t1588175760  get__backgroundColor_3() const { return ____backgroundColor_3; }
	inline Color_t1588175760 * get_address_of__backgroundColor_3() { return &____backgroundColor_3; }
	inline void set__backgroundColor_3(Color_t1588175760  value)
	{
		____backgroundColor_3 = value;
	}

	inline static int32_t get_offset_of__btnYesColor_4() { return static_cast<int32_t>(offsetof(MSGBoxStyle_t1923386386, ____btnYesColor_4)); }
	inline Color_t1588175760  get__btnYesColor_4() const { return ____btnYesColor_4; }
	inline Color_t1588175760 * get_address_of__btnYesColor_4() { return &____btnYesColor_4; }
	inline void set__btnYesColor_4(Color_t1588175760  value)
	{
		____btnYesColor_4 = value;
	}

	inline static int32_t get_offset_of__btnNoColor_5() { return static_cast<int32_t>(offsetof(MSGBoxStyle_t1923386386, ____btnNoColor_5)); }
	inline Color_t1588175760  get__btnNoColor_5() const { return ____btnNoColor_5; }
	inline Color_t1588175760 * get_address_of__btnNoColor_5() { return &____btnNoColor_5; }
	inline void set__btnNoColor_5(Color_t1588175760  value)
	{
		____btnNoColor_5 = value;
	}

	inline static int32_t get_offset_of__btnCancelColor_6() { return static_cast<int32_t>(offsetof(MSGBoxStyle_t1923386386, ____btnCancelColor_6)); }
	inline Color_t1588175760  get__btnCancelColor_6() const { return ____btnCancelColor_6; }
	inline Color_t1588175760 * get_address_of__btnCancelColor_6() { return &____btnCancelColor_6; }
	inline void set__btnCancelColor_6(Color_t1588175760  value)
	{
		____btnCancelColor_6 = value;
	}

	inline static int32_t get_offset_of__icon_7() { return static_cast<int32_t>(offsetof(MSGBoxStyle_t1923386386, ____icon_7)); }
	inline Sprite_t4006040370 * get__icon_7() const { return ____icon_7; }
	inline Sprite_t4006040370 ** get_address_of__icon_7() { return &____icon_7; }
	inline void set__icon_7(Sprite_t4006040370 * value)
	{
		____icon_7 = value;
		Il2CppCodeGenWriteBarrier(&____icon_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
