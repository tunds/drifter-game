﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveLevel
struct MoveLevel_t3054455315;
// UnityEngine.Collider
struct Collider_t955670625;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"

// System.Void MoveLevel::.ctor()
extern "C"  void MoveLevel__ctor_m2215801016 (MoveLevel_t3054455315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveLevel::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void MoveLevel_OnTriggerEnter_m972740032 (MoveLevel_t3054455315 * __this, Collider_t955670625 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
