﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct DefaultComparer_t186827088;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K848873357.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m3961920087_gshared (DefaultComparer_t186827088 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3961920087(__this, method) ((  void (*) (DefaultComparer_t186827088 *, const MethodInfo*))DefaultComparer__ctor_m3961920087_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m525824188_gshared (DefaultComparer_t186827088 * __this, KeyInfo_t848873357  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m525824188(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t186827088 *, KeyInfo_t848873357 , const MethodInfo*))DefaultComparer_GetHashCode_m525824188_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1282235628_gshared (DefaultComparer_t186827088 * __this, KeyInfo_t848873357  ___x0, KeyInfo_t848873357  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1282235628(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t186827088 *, KeyInfo_t848873357 , KeyInfo_t848873357 , const MethodInfo*))DefaultComparer_Equals_m1282235628_gshared)(__this, ___x0, ___y1, method)
