﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// turnSpaceBar
struct turnSpaceBar_t2231649482;

#include "codegen/il2cpp-codegen.h"

// System.Void turnSpaceBar::.ctor()
extern "C"  void turnSpaceBar__ctor_m2873323793 (turnSpaceBar_t2231649482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void turnSpaceBar::Update()
extern "C"  void turnSpaceBar_Update_m605586460 (turnSpaceBar_t2231649482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
