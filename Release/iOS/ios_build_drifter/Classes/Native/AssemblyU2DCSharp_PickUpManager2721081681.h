﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Slider
struct Slider_t1468074762;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3681339876;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickUpManager
struct  PickUpManager_t2721081681  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.UI.Slider PickUpManager::healthSldr
	Slider_t1468074762 * ___healthSldr_2;
	// UnityEngine.GameObject PickUpManager::item
	GameObject_t4012695102 * ___item_3;
	// System.Single PickUpManager::spawnTime
	float ___spawnTime_4;
	// UnityEngine.Transform[] PickUpManager::spawnPoints
	TransformU5BU5D_t3681339876* ___spawnPoints_5;

public:
	inline static int32_t get_offset_of_healthSldr_2() { return static_cast<int32_t>(offsetof(PickUpManager_t2721081681, ___healthSldr_2)); }
	inline Slider_t1468074762 * get_healthSldr_2() const { return ___healthSldr_2; }
	inline Slider_t1468074762 ** get_address_of_healthSldr_2() { return &___healthSldr_2; }
	inline void set_healthSldr_2(Slider_t1468074762 * value)
	{
		___healthSldr_2 = value;
		Il2CppCodeGenWriteBarrier(&___healthSldr_2, value);
	}

	inline static int32_t get_offset_of_item_3() { return static_cast<int32_t>(offsetof(PickUpManager_t2721081681, ___item_3)); }
	inline GameObject_t4012695102 * get_item_3() const { return ___item_3; }
	inline GameObject_t4012695102 ** get_address_of_item_3() { return &___item_3; }
	inline void set_item_3(GameObject_t4012695102 * value)
	{
		___item_3 = value;
		Il2CppCodeGenWriteBarrier(&___item_3, value);
	}

	inline static int32_t get_offset_of_spawnTime_4() { return static_cast<int32_t>(offsetof(PickUpManager_t2721081681, ___spawnTime_4)); }
	inline float get_spawnTime_4() const { return ___spawnTime_4; }
	inline float* get_address_of_spawnTime_4() { return &___spawnTime_4; }
	inline void set_spawnTime_4(float value)
	{
		___spawnTime_4 = value;
	}

	inline static int32_t get_offset_of_spawnPoints_5() { return static_cast<int32_t>(offsetof(PickUpManager_t2721081681, ___spawnPoints_5)); }
	inline TransformU5BU5D_t3681339876* get_spawnPoints_5() const { return ___spawnPoints_5; }
	inline TransformU5BU5D_t3681339876** get_address_of_spawnPoints_5() { return &___spawnPoints_5; }
	inline void set_spawnPoints_5(TransformU5BU5D_t3681339876* value)
	{
		___spawnPoints_5 = value;
		Il2CppCodeGenWriteBarrier(&___spawnPoints_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
