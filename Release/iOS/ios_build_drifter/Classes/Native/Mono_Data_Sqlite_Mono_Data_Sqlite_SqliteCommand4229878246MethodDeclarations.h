﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteCommand
struct SqliteCommand_t4229878246;
// System.String
struct String_t;
// Mono.Data.Sqlite.SqliteConnection
struct SqliteConnection_t3853176977;
// Mono.Data.Sqlite.SqliteTransaction
struct SqliteTransaction_t3177548665;
// Mono.Data.Sqlite.SqliteStatement
struct SqliteStatement_t3906494218;
// System.Data.Common.DbParameter
struct DbParameter_t3306161371;
// Mono.Data.Sqlite.SqliteParameter
struct SqliteParameter_t3651135812;
// System.Data.Common.DbConnection
struct DbConnection_t462757452;
// Mono.Data.Sqlite.SqliteParameterCollection
struct SqliteParameterCollection_t534758338;
// System.Data.Common.DbParameterCollection
struct DbParameterCollection_t3381130713;
// System.Data.Common.DbTransaction
struct DbTransaction_t4162579344;
// System.Data.Common.DbDataReader
struct DbDataReader_t2472406139;
// Mono.Data.Sqlite.SqliteDataReader
struct SqliteDataReader_t1567858368;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection3853176977.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteCommand4229878246.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteTransactio3177548665.h"
#include "System_Data_System_Data_CommandType753495736.h"
#include "System_Data_System_Data_Common_DbConnection462757452.h"
#include "System_Data_System_Data_Common_DbTransaction4162579344.h"
#include "System_Data_System_Data_CommandBehavior326612048.h"
#include "System_Data_System_Data_UpdateRowSource1763489119.h"

// System.Void Mono.Data.Sqlite.SqliteCommand::.ctor()
extern "C"  void SqliteCommand__ctor_m686868988 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::.ctor(System.String,Mono.Data.Sqlite.SqliteConnection)
extern "C"  void SqliteCommand__ctor_m2030531647 (SqliteCommand_t4229878246 * __this, String_t* ___commandText0, SqliteConnection_t3853176977 * ___connection1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::.ctor(Mono.Data.Sqlite.SqliteConnection)
extern "C"  void SqliteCommand__ctor_m1261266051 (SqliteCommand_t4229878246 * __this, SqliteConnection_t3853176977 * ___connection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::.ctor(Mono.Data.Sqlite.SqliteCommand)
extern "C"  void SqliteCommand__ctor_m1281750470 (SqliteCommand_t4229878246 * __this, SqliteCommand_t4229878246 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::.ctor(System.String,Mono.Data.Sqlite.SqliteConnection,Mono.Data.Sqlite.SqliteTransaction)
extern "C"  void SqliteCommand__ctor_m1269067774 (SqliteCommand_t4229878246 * __this, String_t* ___commandText0, SqliteConnection_t3853176977 * ___connection1, SqliteTransaction_t3177548665 * ___transaction2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::Dispose(System.Boolean)
extern "C"  void SqliteCommand_Dispose_m751256816 (SqliteCommand_t4229878246 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::ClearCommands()
extern "C"  void SqliteCommand_ClearCommands_m2365351119 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteStatement Mono.Data.Sqlite.SqliteCommand::BuildNextCommand()
extern "C"  SqliteStatement_t3906494218 * SqliteCommand_BuildNextCommand_m1837423747 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteStatement Mono.Data.Sqlite.SqliteCommand::GetStatement(System.Int32)
extern "C"  SqliteStatement_t3906494218 * SqliteCommand_GetStatement_m3941582915 (SqliteCommand_t4229878246 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteCommand::get_CommandText()
extern "C"  String_t* SqliteCommand_get_CommandText_m1956326982 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::set_CommandText(System.String)
extern "C"  void SqliteCommand_set_CommandText_m3554762061 (SqliteCommand_t4229878246 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteCommand::get_CommandTimeout()
extern "C"  int32_t SqliteCommand_get_CommandTimeout_m533504387 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::set_CommandTimeout(System.Int32)
extern "C"  void SqliteCommand_set_CommandTimeout_m2184844460 (SqliteCommand_t4229878246 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::set_CommandType(System.Data.CommandType)
extern "C"  void SqliteCommand_set_CommandType_m257754864 (SqliteCommand_t4229878246 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameter Mono.Data.Sqlite.SqliteCommand::CreateDbParameter()
extern "C"  DbParameter_t3306161371 * SqliteCommand_CreateDbParameter_m3495392171 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteParameter Mono.Data.Sqlite.SqliteCommand::CreateParameter()
extern "C"  SqliteParameter_t3651135812 * SqliteCommand_CreateParameter_m1338769424 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteConnection Mono.Data.Sqlite.SqliteCommand::get_Connection()
extern "C"  SqliteConnection_t3853176977 * SqliteCommand_get_Connection_m3706866959 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::set_Connection(Mono.Data.Sqlite.SqliteConnection)
extern "C"  void SqliteCommand_set_Connection_m1669684284 (SqliteCommand_t4229878246 * __this, SqliteConnection_t3853176977 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbConnection Mono.Data.Sqlite.SqliteCommand::get_DbConnection()
extern "C"  DbConnection_t462757452 * SqliteCommand_get_DbConnection_m2143828262 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::set_DbConnection(System.Data.Common.DbConnection)
extern "C"  void SqliteCommand_set_DbConnection_m3147942679 (SqliteCommand_t4229878246 * __this, DbConnection_t462757452 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteParameterCollection Mono.Data.Sqlite.SqliteCommand::get_Parameters()
extern "C"  SqliteParameterCollection_t534758338 * SqliteCommand_get_Parameters_m1135154484 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameterCollection Mono.Data.Sqlite.SqliteCommand::get_DbParameterCollection()
extern "C"  DbParameterCollection_t3381130713 * SqliteCommand_get_DbParameterCollection_m494314586 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteTransaction Mono.Data.Sqlite.SqliteCommand::get_Transaction()
extern "C"  SqliteTransaction_t3177548665 * SqliteCommand_get_Transaction_m18016173 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::set_Transaction(Mono.Data.Sqlite.SqliteTransaction)
extern "C"  void SqliteCommand_set_Transaction_m4189438194 (SqliteCommand_t4229878246 * __this, SqliteTransaction_t3177548665 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbTransaction Mono.Data.Sqlite.SqliteCommand::get_DbTransaction()
extern "C"  DbTransaction_t4162579344 * SqliteCommand_get_DbTransaction_m1203222152 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::set_DbTransaction(System.Data.Common.DbTransaction)
extern "C"  void SqliteCommand_set_DbTransaction_m1113483771 (SqliteCommand_t4229878246 * __this, DbTransaction_t4162579344 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::InitializeForReader()
extern "C"  void SqliteCommand_InitializeForReader_m2154519958 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbDataReader Mono.Data.Sqlite.SqliteCommand::ExecuteDbDataReader(System.Data.CommandBehavior)
extern "C"  DbDataReader_t2472406139 * SqliteCommand_ExecuteDbDataReader_m397199146 (SqliteCommand_t4229878246 * __this, int32_t ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteDataReader Mono.Data.Sqlite.SqliteCommand::ExecuteReader(System.Data.CommandBehavior)
extern "C"  SqliteDataReader_t1567858368 * SqliteCommand_ExecuteReader_m1005760793 (SqliteCommand_t4229878246 * __this, int32_t ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteDataReader Mono.Data.Sqlite.SqliteCommand::ExecuteReader()
extern "C"  SqliteDataReader_t1567858368 * SqliteCommand_ExecuteReader_m585431169 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::ClearDataReader()
extern "C"  void SqliteCommand_ClearDataReader_m3847512212 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteCommand::ExecuteNonQuery()
extern "C"  int32_t SqliteCommand_ExecuteNonQuery_m3380954062 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::Prepare()
extern "C"  void SqliteCommand_Prepare_m1587082273 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.UpdateRowSource Mono.Data.Sqlite.SqliteCommand::get_UpdatedRowSource()
extern "C"  int32_t SqliteCommand_get_UpdatedRowSource_m2063921815 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::set_UpdatedRowSource(System.Data.UpdateRowSource)
extern "C"  void SqliteCommand_set_UpdatedRowSource_m78551372 (SqliteCommand_t4229878246 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteCommand::get_DesignTimeVisible()
extern "C"  bool SqliteCommand_get_DesignTimeVisible_m3261981686 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommand::set_DesignTimeVisible(System.Boolean)
extern "C"  void SqliteCommand_set_DesignTimeVisible_m243266235 (SqliteCommand_t4229878246 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteCommand::Clone()
extern "C"  Il2CppObject * SqliteCommand_Clone_m1340367138 (SqliteCommand_t4229878246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
