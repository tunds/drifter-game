﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.EncodingInfo
struct EncodingInfo_t2013923205;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Boolean System.Text.EncodingInfo::Equals(System.Object)
extern "C"  bool EncodingInfo_Equals_m1504812574 (EncodingInfo_t2013923205 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.EncodingInfo::GetHashCode()
extern "C"  int32_t EncodingInfo_GetHashCode_m3965017986 (EncodingInfo_t2013923205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
