﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// SQLiteDatabase.DBReader
struct DBReader_t4220400158;
// Mono.Data.Sqlite.SqliteDataReader
struct SqliteDataReader_t1567858368;
// System.String
struct String_t;
// SQLiteDatabase.DBSchema
struct DBSchema_t4247391996;
// System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct List_1_t282322642;
// SQLiteDatabase.SQLiteDB
struct SQLiteDB_t557922535;
// System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct List_1_t722451806;
// SQLiteDatabase.SQLiteEventListener/OnError
struct OnError_t314785609;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "LocalDB_U3CModuleU3E86524790.h"
#include "LocalDB_U3CModuleU3E86524790MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_DBReader4220400158.h"
#include "LocalDB_SQLiteDatabase_DBReader4220400158MethodDeclarations.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteDataReader1567858368.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3271763293MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Int162847414729.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3271763293.h"
#include "System_Data_System_Data_Common_DbDataReader2472406139MethodDeclarations.h"
#include "System_Data_System_Data_Common_DbDataReader2472406139.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Boolean211005341.h"
#include "LocalDB_SQLiteDatabase_SQLiteEventListener_OnError314785609MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_SQLiteEventListener1751327711.h"
#include "LocalDB_SQLiteDatabase_SQLiteEventListener1751327711MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_SQLiteEventListener_OnError314785609.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "mscorlib_System_GC2776609905MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_DBSchema4247391996.h"
#include "LocalDB_SQLiteDatabase_DBSchema4247391996MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen282322642MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen282322642.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataType4220602565.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Field3780330969.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB557922535.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB557922535MethodDeclarations.h"
#include "mscorlib_System_IO_Path2029632748MethodDeclarations.h"
#include "mscorlib_System_IO_File2029342275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW1522972100MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW1522972100.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1574985880.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection3853176977MethodDeclarations.h"
#include "mscorlib_System_IO_FileStream1527309539.h"
#include "mscorlib_System_IO_Stream219029575MethodDeclarations.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection3853176977.h"
#include "System_Data_System_Data_Common_DbConnection462757452MethodDeclarations.h"
#include "System_Data_System_Data_Common_DbConnection462757452.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteCommand4229878246.h"
#include "System_Data_System_Data_Common_DbCommand2323745021MethodDeclarations.h"
#include "System_Data_System_Data_Common_DbCommand2323745021.h"
#include "System_System_ComponentModel_Component553679750MethodDeclarations.h"
#include "System_System_ComponentModel_Component553679750.h"
#include "mscorlib_System_Collections_Generic_List_1_gen722451806.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataPair4220460133.h"
#include "mscorlib_System_Collections_Generic_List_1_gen722451806MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_ConditionPair1745523828.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Condition1501727354.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteCommand4229878246MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Condition1501727354MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_ConditionPair1745523828MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataPair4220460133MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataType4220602565MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Field3780330969MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SQLiteDatabase.DBReader::.ctor(Mono.Data.Sqlite.SqliteDataReader)
extern Il2CppClass* List_1_t3271763293_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2474804324_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3653606545_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m898618037_MethodInfo_var;
extern const uint32_t DBReader__ctor_m102303496_MetadataUsageId;
extern "C"  void DBReader__ctor_m102303496 (DBReader_t4220400158 * __this, SqliteDataReader_t1567858368 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DBReader__ctor_m102303496_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t2474804324 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		__this->set_rowCounter_1((-1));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		List_1_t3271763293 * L_0 = (List_1_t3271763293 *)il2cpp_codegen_object_new(List_1_t3271763293_il2cpp_TypeInfo_var);
		List_1__ctor_m3653606545(L_0, /*hidden argument*/List_1__ctor_m3653606545_MethodInfo_var);
		__this->set_dataTable_0(L_0);
		goto IL_005a;
	}

IL_001d:
	{
		Dictionary_2_t2474804324 * L_1 = (Dictionary_2_t2474804324 *)il2cpp_codegen_object_new(Dictionary_2_t2474804324_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m898618037(L_1, /*hidden argument*/Dictionary_2__ctor_m898618037_MethodInfo_var);
		V_0 = L_1;
		V_1 = 0;
		goto IL_0042;
	}

IL_002a:
	{
		Dictionary_2_t2474804324 * L_2 = V_0;
		SqliteDataReader_t1567858368 * L_3 = ___reader0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(37 /* System.String System.Data.Common.DbDataReader::GetName(System.Int32) */, L_3, L_4);
		SqliteDataReader_t1567858368 * L_6 = ___reader0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		Il2CppObject * L_8 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(40 /* System.Object System.Data.Common.DbDataReader::GetValue(System.Int32) */, L_6, L_7);
		NullCheck(L_2);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Add(!0,!1) */, L_2, L_5, L_8);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_10 = V_1;
		SqliteDataReader_t1567858368 * L_11 = ___reader0;
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(22 /* System.Int32 System.Data.Common.DbDataReader::get_FieldCount() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_002a;
		}
	}
	{
		List_1_t3271763293 * L_13 = __this->get_dataTable_0();
		Dictionary_2_t2474804324 * L_14 = V_0;
		NullCheck(L_13);
		VirtActionInvoker1< Dictionary_2_t2474804324 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::Add(!0) */, L_13, L_14);
	}

IL_005a:
	{
		SqliteDataReader_t1567858368 * L_15 = ___reader0;
		NullCheck(L_15);
		bool L_16 = VirtFuncInvoker0< bool >::Invoke(44 /* System.Boolean System.Data.Common.DbDataReader::Read() */, L_15);
		if (L_16)
		{
			goto IL_001d;
		}
	}
	{
		SqliteDataReader_t1567858368 * L_17 = ___reader0;
		if (!L_17)
		{
			goto IL_0071;
		}
	}
	{
		SqliteDataReader_t1567858368 * L_18 = ___reader0;
		NullCheck(L_18);
		VirtActionInvoker0::Invoke(28 /* System.Void System.Data.Common.DbDataReader::Close() */, L_18);
	}

IL_0071:
	{
		return;
	}
}
// System.Boolean SQLiteDatabase.DBReader::Read()
extern "C"  bool DBReader_Read_m2141930664 (DBReader_t4220400158 * __this, const MethodInfo* method)
{
	{
		int16_t L_0 = __this->get_rowCounter_1();
		__this->set_rowCounter_1((((int16_t)((int16_t)((int32_t)((int32_t)L_0+(int32_t)1))))));
		int16_t L_1 = __this->get_rowCounter_1();
		List_1_t3271763293 * L_2 = __this->get_dataTable_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Count() */, L_2);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0027;
		}
	}
	{
		return (bool)1;
	}

IL_0027:
	{
		DBReader_Dispose_m400197349(__this, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.String SQLiteDatabase.DBReader::GetStringValue(System.String)
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral913339090;
extern const uint32_t DBReader_GetStringValue_m1373122399_MetadataUsageId;
extern "C"  String_t* DBReader_GetStringValue_m1373122399 (DBReader_t4220400158 * __this, String_t* ___fieldName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DBReader_GetStringValue_m1373122399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int16_t L_0 = __this->get_rowCounter_1();
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0045;
		}
	}
	{
		List_1_t3271763293 * L_1 = __this->get_dataTable_0();
		if (!L_1)
		{
			goto IL_0045;
		}
	}
	{
		List_1_t3271763293 * L_2 = __this->get_dataTable_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Count() */, L_2);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0045;
		}
	}
	{
		List_1_t3271763293 * L_4 = __this->get_dataTable_0();
		int16_t L_5 = __this->get_rowCounter_1();
		NullCheck(L_4);
		Dictionary_2_t2474804324 * L_6 = VirtFuncInvoker1< Dictionary_2_t2474804324 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Item(System.Int32) */, L_4, L_5);
		String_t* L_7 = ___fieldName0;
		NullCheck(L_6);
		Il2CppObject * L_8 = VirtFuncInvoker1< Il2CppObject *, String_t* >::Invoke(28 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_6, L_7);
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		return L_9;
	}

IL_0045:
	{
		OnError_t314785609 * L_10 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_10)
		{
			goto IL_005e;
		}
	}
	{
		OnError_t314785609 * L_11 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_11);
		OnError_Invoke_m3917243719(L_11, _stringLiteral913339090, /*hidden argument*/NULL);
	}

IL_005e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_12;
	}
}
// System.Int32 SQLiteDatabase.DBReader::GetIntValue(System.String)
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral913339090;
extern const uint32_t DBReader_GetIntValue_m2828894496_MetadataUsageId;
extern "C"  int32_t DBReader_GetIntValue_m2828894496 (DBReader_t4220400158 * __this, String_t* ___fieldName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DBReader_GetIntValue_m2828894496_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int16_t L_0 = __this->get_rowCounter_1();
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		List_1_t3271763293 * L_1 = __this->get_dataTable_0();
		if (!L_1)
		{
			goto IL_0059;
		}
	}
	{
		List_1_t3271763293 * L_2 = __this->get_dataTable_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Count() */, L_2);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		V_0 = 0;
		List_1_t3271763293 * L_4 = __this->get_dataTable_0();
		int16_t L_5 = __this->get_rowCounter_1();
		NullCheck(L_4);
		Dictionary_2_t2474804324 * L_6 = VirtFuncInvoker1< Dictionary_2_t2474804324 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Item(System.Int32) */, L_4, L_5);
		String_t* L_7 = ___fieldName0;
		NullCheck(L_6);
		Il2CppObject * L_8 = VirtFuncInvoker1< Il2CppObject *, String_t* >::Invoke(28 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_6, L_7);
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		bool L_10 = Int32_TryParse_m695344220(NULL /*static, unused*/, L_9, (&V_0), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_11 = V_0;
		return L_11;
	}

IL_0054:
	{
		goto IL_0072;
	}

IL_0059:
	{
		OnError_t314785609 * L_12 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_12)
		{
			goto IL_0072;
		}
	}
	{
		OnError_t314785609 * L_13 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_13);
		OnError_Invoke_m3917243719(L_13, _stringLiteral913339090, /*hidden argument*/NULL);
	}

IL_0072:
	{
		return 0;
	}
}
// System.Void SQLiteDatabase.DBReader::Dispose()
extern const MethodInfo* List_1_TrimExcess_m2298140554_MethodInfo_var;
extern const uint32_t DBReader_Dispose_m400197349_MetadataUsageId;
extern "C"  void DBReader_Dispose_m400197349 (DBReader_t4220400158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DBReader_Dispose_m400197349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3271763293 * L_0 = __this->get_dataTable_0();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::Clear() */, L_0);
		List_1_t3271763293 * L_1 = __this->get_dataTable_0();
		NullCheck(L_1);
		List_1_TrimExcess_m2298140554(L_1, /*hidden argument*/List_1_TrimExcess_m2298140554_MethodInfo_var);
		__this->set_dataTable_0((List_1_t3271763293 *)NULL);
		GC_Collect_m1459080321(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SQLiteDatabase.DBSchema::.ctor(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t282322642_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3759833351_MethodInfo_var;
extern const uint32_t DBSchema__ctor_m2925071800_MetadataUsageId;
extern "C"  void DBSchema__ctor_m2925071800 (DBSchema_t4247391996 * __this, String_t* ___tableName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DBSchema__ctor_m2925071800_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__name_0(L_0);
		List_1_t282322642 * L_1 = (List_1_t282322642 *)il2cpp_codegen_object_new(List_1_t282322642_il2cpp_TypeInfo_var);
		List_1__ctor_m3759833351(L_1, /*hidden argument*/List_1__ctor_m3759833351_MethodInfo_var);
		__this->set_fieldList_1(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___tableName0;
		NullCheck(L_2);
		String_t* L_3 = String_Trim_m1030489823(L_2, /*hidden argument*/NULL);
		__this->set__name_0(L_3);
		return;
	}
}
// System.String SQLiteDatabase.DBSchema::get_TableName()
extern "C"  String_t* DBSchema_get_TableName_m230189915 (DBSchema_t4247391996 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__name_0();
		return L_0;
	}
}
// System.Void SQLiteDatabase.DBSchema::AddField(System.String,SQLiteDatabase.SQLiteDB/DB_DataType,System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern Il2CppClass* DB_Field_t3780330969_il2cpp_TypeInfo_var;
extern const uint32_t DBSchema_AddField_m776412250_MetadataUsageId;
extern "C"  void DBSchema_AddField_m776412250 (DBSchema_t4247391996 * __this, String_t* ___fieldName0, int32_t ___fieldType1, int32_t ___size2, bool ___isNotNull3, bool ___isPrimaryKey4, bool ___isUnique5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DBSchema_AddField_m776412250_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DB_Field_t3780330969  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (DB_Field_t3780330969_il2cpp_TypeInfo_var, (&V_0));
		String_t* L_0 = ___fieldName0;
		(&V_0)->set_name_0(L_0);
		int32_t L_1 = ___fieldType1;
		(&V_0)->set_type_1(L_1);
		bool L_2 = ___isNotNull3;
		(&V_0)->set_isNotNull_4(L_2);
		bool L_3 = ___isPrimaryKey4;
		(&V_0)->set_isPrimaryKey_3(L_3);
		bool L_4 = ___isUnique5;
		(&V_0)->set_isUnique_5(L_4);
		int32_t L_5 = ___size2;
		(&V_0)->set_size_2(L_5);
		List_1_t282322642 * L_6 = __this->get_fieldList_1();
		DB_Field_t3780330969  L_7 = V_0;
		NullCheck(L_6);
		VirtActionInvoker1< DB_Field_t3780330969  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::Add(!0) */, L_6, L_7);
		return;
	}
}
// System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field> SQLiteDatabase.DBSchema::GetTableFields()
extern "C"  List_1_t282322642 * DBSchema_GetTableFields_m2566209761 (DBSchema_t4247391996 * __this, const MethodInfo* method)
{
	{
		List_1_t282322642 * L_0 = __this->get_fieldList_1();
		return L_0;
	}
}
// System.Void SQLiteDatabase.SQLiteDB::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SQLiteDB__ctor_m463384575_MetadataUsageId;
extern "C"  void SQLiteDB__ctor_m463384575 (SQLiteDB_t557922535 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB__ctor_m463384575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__dbName_1(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__dbPath_2(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SQLiteDatabase.SQLiteDB::get_Exists()
extern Il2CppClass* Path_t2029632748_il2cpp_TypeInfo_var;
extern const uint32_t SQLiteDB_get_Exists_m3278641920_MetadataUsageId;
extern "C"  bool SQLiteDB_get_Exists_m3278641920 (SQLiteDB_t557922535 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_get_Exists_m3278641920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get__dbPath_2();
		String_t* L_1 = __this->get__dbName_1();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t2029632748_il2cpp_TypeInfo_var);
		String_t* L_2 = Path_Combine_m4122812896(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		bool L_3 = File_Exists_m1326262381(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// SQLiteDatabase.SQLiteDB SQLiteDatabase.SQLiteDB::get_Instance()
extern Il2CppClass* SQLiteDB_t557922535_il2cpp_TypeInfo_var;
extern const uint32_t SQLiteDB_get_Instance_m2094062401_MetadataUsageId;
extern "C"  SQLiteDB_t557922535 * SQLiteDB_get_Instance_m2094062401 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_get_Instance_m2094062401_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SQLiteDB_t557922535 * L_0 = ((SQLiteDB_t557922535_StaticFields*)SQLiteDB_t557922535_il2cpp_TypeInfo_var->static_fields)->get__instance_0();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		SQLiteDB_t557922535 * L_1 = (SQLiteDB_t557922535 *)il2cpp_codegen_object_new(SQLiteDB_t557922535_il2cpp_TypeInfo_var);
		SQLiteDB__ctor_m463384575(L_1, /*hidden argument*/NULL);
		((SQLiteDB_t557922535_StaticFields*)SQLiteDB_t557922535_il2cpp_TypeInfo_var->static_fields)->set__instance_0(L_1);
	}

IL_0014:
	{
		SQLiteDB_t557922535 * L_2 = ((SQLiteDB_t557922535_StaticFields*)SQLiteDB_t557922535_il2cpp_TypeInfo_var->static_fields)->get__instance_0();
		return L_2;
	}
}
// System.String SQLiteDatabase.SQLiteDB::get_DBLocation()
extern "C"  String_t* SQLiteDB_get_DBLocation_m603624094 (SQLiteDB_t557922535 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__dbPath_2();
		return L_0;
	}
}
// System.Void SQLiteDatabase.SQLiteDB::set_DBLocation(System.String)
extern "C"  void SQLiteDB_set_DBLocation_m3743704717 (SQLiteDB_t557922535 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__dbPath_2(L_0);
		return;
	}
}
// System.Boolean SQLiteDatabase.SQLiteDB::ConnectToDefaultDatabase(System.String,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t2029632748_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2467078719;
extern Il2CppCodeGenString* _stringLiteral86477086;
extern Il2CppCodeGenString* _stringLiteral1734141857;
extern Il2CppCodeGenString* _stringLiteral3953900535;
extern const uint32_t SQLiteDB_ConnectToDefaultDatabase_m3847386093_MetadataUsageId;
extern "C"  bool SQLiteDB_ConnectToDefaultDatabase_m3847386093 (SQLiteDB_t557922535 * __this, String_t* ___dbName0, bool ___loadFresh1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_ConnectToDefaultDatabase_m3847386093_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	WWW_t1522972100 * V_2 = NULL;
	bool V_3 = false;
	Exception_t1967233988 * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = __this->get__dbPath_2();
		NullCheck(L_0);
		String_t* L_1 = String_Trim_m1030489823(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_3 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__dbPath_2(L_3);
	}

IL_0020:
	{
		bool L_4 = ___loadFresh1;
		if (!L_4)
		{
			goto IL_0110;
		}
	}

IL_0026:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_5 = Application_get_streamingAssetsPath_m1181082379(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_6 = ___dbName0;
			IL2CPP_RUNTIME_CLASS_INIT(Path_t2029632748_il2cpp_TypeInfo_var);
			String_t* L_7 = Path_Combine_m4122812896(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
			V_0 = L_7;
			String_t* L_8 = __this->get__dbPath_2();
			String_t* L_9 = ___dbName0;
			String_t* L_10 = Path_Combine_m4122812896(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
			V_1 = L_10;
			int32_t L_11 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)11)))))
			{
				goto IL_00c5;
			}
		}

IL_004b:
		{
			String_t* L_12 = Application_get_dataPath_m2519694320(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_13 = ___dbName0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_14 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral2467078719, L_12, _stringLiteral86477086, L_13, /*hidden argument*/NULL);
			V_0 = L_14;
			String_t* L_15 = V_0;
			WWW_t1522972100 * L_16 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
			WWW__ctor_m1985874080(L_16, L_15, /*hidden argument*/NULL);
			V_2 = L_16;
			goto IL_006d;
		}

IL_006d:
		{
			WWW_t1522972100 * L_17 = V_2;
			NullCheck(L_17);
			bool L_18 = WWW_get_isDone_m634060017(L_17, /*hidden argument*/NULL);
			if (!L_18)
			{
				goto IL_006d;
			}
		}

IL_0078:
		{
			WWW_t1522972100 * L_19 = V_2;
			NullCheck(L_19);
			bool L_20 = WWW_get_isDone_m634060017(L_19, /*hidden argument*/NULL);
			if (L_20)
			{
				goto IL_00a3;
			}
		}

IL_0083:
		{
			OnError_t314785609 * L_21 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			if (!L_21)
			{
				goto IL_009c;
			}
		}

IL_008d:
		{
			OnError_t314785609 * L_22 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			NullCheck(L_22);
			OnError_Invoke_m3917243719(L_22, _stringLiteral1734141857, /*hidden argument*/NULL);
		}

IL_009c:
		{
			V_3 = (bool)0;
			goto IL_0119;
		}

IL_00a3:
		{
			String_t* L_23 = V_1;
			bool L_24 = File_Exists_m1326262381(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
			if (!L_24)
			{
				goto IL_00b4;
			}
		}

IL_00ae:
		{
			String_t* L_25 = V_1;
			File_Delete_m760984832(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		}

IL_00b4:
		{
			String_t* L_26 = V_1;
			WWW_t1522972100 * L_27 = V_2;
			NullCheck(L_27);
			ByteU5BU5D_t58506160* L_28 = WWW_get_bytes_m2080623436(L_27, /*hidden argument*/NULL);
			File_WriteAllBytes_m2419938065(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
			goto IL_00dd;
		}

IL_00c5:
		{
			String_t* L_29 = V_1;
			bool L_30 = File_Exists_m1326262381(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
			if (!L_30)
			{
				goto IL_00d6;
			}
		}

IL_00d0:
		{
			String_t* L_31 = V_1;
			File_Delete_m760984832(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		}

IL_00d6:
		{
			String_t* L_32 = V_0;
			String_t* L_33 = V_1;
			File_Copy_m4182716978(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		}

IL_00dd:
		{
			goto IL_0110;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00e2;
		throw e;
	}

CATCH_00e2:
	{ // begin catch(System.Exception)
		{
			V_4 = ((Exception_t1967233988 *)__exception_local);
			OnError_t314785609 * L_34 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			if (!L_34)
			{
				goto IL_0109;
			}
		}

IL_00ee:
		{
			OnError_t314785609 * L_35 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			Exception_t1967233988 * L_36 = V_4;
			NullCheck(L_36);
			String_t* L_37 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_36);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_38 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3953900535, L_37, /*hidden argument*/NULL);
			NullCheck(L_35);
			OnError_Invoke_m3917243719(L_35, L_38, /*hidden argument*/NULL);
		}

IL_0109:
		{
			V_3 = (bool)0;
			goto IL_0119;
		}
	} // end catch (depth: 1)

IL_0110:
	{
		String_t* L_39 = ___dbName0;
		bool L_40 = SQLiteDB_CreateDatabase_m2289450989(__this, L_39, (bool)0, /*hidden argument*/NULL);
		return L_40;
	}

IL_0119:
	{
		bool L_41 = V_3;
		return L_41;
	}
}
// System.Boolean SQLiteDatabase.SQLiteDB::CreateDatabase(System.String,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t2029632748_il2cpp_TypeInfo_var;
extern Il2CppClass* SqliteConnection_t3853176977_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral320314682;
extern Il2CppCodeGenString* _stringLiteral73824365;
extern Il2CppCodeGenString* _stringLiteral3097081105;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t SQLiteDB_CreateDatabase_m2289450989_MetadataUsageId;
extern "C"  bool SQLiteDB_CreateDatabase_m2289450989 (SQLiteDB_t557922535 * __this, String_t* ___dbName0, bool ___isOverWrite1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_CreateDatabase_m2289450989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	FileStream_t1527309539 * V_2 = NULL;
	Exception_t1967233988 * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___dbName0;
		NullCheck(L_0);
		String_t* L_1 = String_Trim_m1030489823(L_0, /*hidden argument*/NULL);
		__this->set__dbName_1(L_1);
		bool L_2 = ___isOverWrite1;
		__this->set__isOverWrite_3(L_2);
		V_0 = (bool)0;
		String_t* L_3 = __this->get__dbName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		OnError_t314785609 * L_5 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		OnError_t314785609 * L_6 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_6);
		OnError_Invoke_m3917243719(L_6, _stringLiteral320314682, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return (bool)0;
	}

IL_0040:
	{
		String_t* L_7 = __this->get__dbPath_2();
		String_t* L_8 = __this->get__dbName_1();
		IL2CPP_RUNTIME_CLASS_INIT(Path_t2029632748_il2cpp_TypeInfo_var);
		String_t* L_9 = Path_Combine_m4122812896(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0052:
	try
	{ // begin try (depth: 1)
		{
			bool L_10 = __this->get__isOverWrite_3();
			if (!L_10)
			{
				goto IL_006e;
			}
		}

IL_005d:
		{
			String_t* L_11 = V_1;
			bool L_12 = File_Exists_m1326262381(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
			if (!L_12)
			{
				goto IL_006e;
			}
		}

IL_0068:
		{
			String_t* L_13 = V_1;
			File_Delete_m760984832(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		}

IL_006e:
		{
			String_t* L_14 = V_1;
			bool L_15 = File_Exists_m1326262381(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
			if (L_15)
			{
				goto IL_0086;
			}
		}

IL_0079:
		{
			String_t* L_16 = V_1;
			FileStream_t1527309539 * L_17 = File_Create_m3497726217(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
			V_2 = L_17;
			FileStream_t1527309539 * L_18 = V_2;
			NullCheck(L_18);
			VirtActionInvoker0::Invoke(14 /* System.Void System.IO.Stream::Close() */, L_18);
		}

IL_0086:
		{
			String_t* L_19 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_20 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral73824365, L_19, /*hidden argument*/NULL);
			SqliteConnection_t3853176977 * L_21 = (SqliteConnection_t3853176977 *)il2cpp_codegen_object_new(SqliteConnection_t3853176977_il2cpp_TypeInfo_var);
			SqliteConnection__ctor_m1210514465(L_21, L_20, /*hidden argument*/NULL);
			__this->set_connection_4(L_21);
			V_0 = (bool)1;
			goto IL_00fd;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00a3;
		throw e;
	}

CATCH_00a3:
	{ // begin catch(System.Exception)
		{
			V_3 = ((Exception_t1967233988 *)__exception_local);
			OnError_t314785609 * L_22 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			if (!L_22)
			{
				goto IL_00f6;
			}
		}

IL_00ae:
		{
			OnError_t314785609 * L_23 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
			ObjectU5BU5D_t11523773* L_24 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)6));
			NullCheck(L_24);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
			ArrayElementTypeCheck (L_24, _stringLiteral3097081105);
			(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3097081105);
			ObjectU5BU5D_t11523773* L_25 = L_24;
			Exception_t1967233988 * L_26 = V_3;
			NullCheck(L_26);
			String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_26);
			NullCheck(L_25);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 1);
			ArrayElementTypeCheck (L_25, L_27);
			(L_25)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_27);
			ObjectU5BU5D_t11523773* L_28 = L_25;
			NullCheck(L_28);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 2);
			ArrayElementTypeCheck (L_28, _stringLiteral10);
			(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral10);
			ObjectU5BU5D_t11523773* L_29 = L_28;
			Exception_t1967233988 * L_30 = V_3;
			NullCheck(L_30);
			Exception_t1967233988 * L_31 = VirtFuncInvoker0< Exception_t1967233988 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_30);
			NullCheck(L_29);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 3);
			ArrayElementTypeCheck (L_29, L_31);
			(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_31);
			ObjectU5BU5D_t11523773* L_32 = L_29;
			NullCheck(L_32);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 4);
			ArrayElementTypeCheck (L_32, _stringLiteral10);
			(L_32)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral10);
			ObjectU5BU5D_t11523773* L_33 = L_32;
			Exception_t1967233988 * L_34 = V_3;
			NullCheck(L_34);
			String_t* L_35 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_34);
			NullCheck(L_33);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 5);
			ArrayElementTypeCheck (L_33, L_35);
			(L_33)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_35);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_36 = String_Concat_m3016520001(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
			NullCheck(L_23);
			OnError_Invoke_m3917243719(L_23, L_36, /*hidden argument*/NULL);
		}

IL_00f6:
		{
			V_0 = (bool)0;
			goto IL_00fd;
		}
	} // end catch (depth: 1)

IL_00fd:
	{
		bool L_37 = V_0;
		return L_37;
	}
}
// System.Boolean SQLiteDatabase.SQLiteDB::CreateTable(SQLiteDatabase.DBSchema)
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3679522742;
extern Il2CppCodeGenString* _stringLiteral2497456690;
extern Il2CppCodeGenString* _stringLiteral1624098666;
extern Il2CppCodeGenString* _stringLiteral1768767382;
extern Il2CppCodeGenString* _stringLiteral3893179053;
extern Il2CppCodeGenString* _stringLiteral2330800566;
extern Il2CppCodeGenString* _stringLiteral40;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral1303;
extern Il2CppCodeGenString* _stringLiteral2330375892;
extern Il2CppCodeGenString* _stringLiteral2507767761;
extern Il2CppCodeGenString* _stringLiteral1149335411;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral824662974;
extern const uint32_t SQLiteDB_CreateTable_m3808471283_MetadataUsageId;
extern "C"  bool SQLiteDB_CreateTable_m3808471283 (SQLiteDB_t557922535 * __this, DBSchema_t4247391996 * ___schema0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_CreateTable_m3808471283_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	DB_Field_t3780330969  V_5;
	memset(&V_5, 0, sizeof(V_5));
	DB_Field_t3780330969  V_6;
	memset(&V_6, 0, sizeof(V_6));
	DB_Field_t3780330969  V_7;
	memset(&V_7, 0, sizeof(V_7));
	DB_Field_t3780330969  V_8;
	memset(&V_8, 0, sizeof(V_8));
	DB_Field_t3780330969  V_9;
	memset(&V_9, 0, sizeof(V_9));
	DB_Field_t3780330969  V_10;
	memset(&V_10, 0, sizeof(V_10));
	DB_Field_t3780330969  V_11;
	memset(&V_11, 0, sizeof(V_11));
	DB_Field_t3780330969  V_12;
	memset(&V_12, 0, sizeof(V_12));
	DB_Field_t3780330969  V_13;
	memset(&V_13, 0, sizeof(V_13));
	DB_Field_t3780330969  V_14;
	memset(&V_14, 0, sizeof(V_14));
	DB_Field_t3780330969  V_15;
	memset(&V_15, 0, sizeof(V_15));
	DB_Field_t3780330969  V_16;
	memset(&V_16, 0, sizeof(V_16));
	DB_Field_t3780330969  V_17;
	memset(&V_17, 0, sizeof(V_17));
	DB_Field_t3780330969  V_18;
	memset(&V_18, 0, sizeof(V_18));
	DB_Field_t3780330969  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Exception_t1967233988 * V_20 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = SQLiteDB_get_Exists_m3278641920(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		OnError_t314785609 * L_1 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		OnError_t314785609 * L_2 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_2);
		OnError_Invoke_m3917243719(L_2, _stringLiteral3679522742, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return (bool)0;
	}

IL_0026:
	{
		DBSchema_t4247391996 * L_3 = ___schema0;
		NullCheck(L_3);
		String_t* L_4 = DBSchema_get_TableName_m230189915(L_3, /*hidden argument*/NULL);
		bool L_5 = SQLiteDB_IsTableExists_m4049229627(__this, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0062;
		}
	}
	{
		OnError_t314785609 * L_6 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_6)
		{
			goto IL_0060;
		}
	}
	{
		OnError_t314785609 * L_7 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		DBSchema_t4247391996 * L_8 = ___schema0;
		NullCheck(L_8);
		String_t* L_9 = DBSchema_get_TableName_m230189915(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2497456690, L_9, _stringLiteral1624098666, /*hidden argument*/NULL);
		NullCheck(L_7);
		OnError_Invoke_m3917243719(L_7, L_10, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return (bool)0;
	}

IL_0062:
	{
		DBSchema_t4247391996 * L_11 = ___schema0;
		NullCheck(L_11);
		String_t* L_12 = DBSchema_get_TableName_m230189915(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_008d;
		}
	}
	{
		OnError_t314785609 * L_14 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_14)
		{
			goto IL_008b;
		}
	}
	{
		OnError_t314785609 * L_15 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_15);
		OnError_Invoke_m3917243719(L_15, _stringLiteral1768767382, /*hidden argument*/NULL);
	}

IL_008b:
	{
		return (bool)0;
	}

IL_008d:
	{
		DBSchema_t4247391996 * L_16 = ___schema0;
		if (!L_16)
		{
			goto IL_00a3;
		}
	}
	{
		DBSchema_t4247391996 * L_17 = ___schema0;
		NullCheck(L_17);
		List_1_t282322642 * L_18 = DBSchema_GetTableFields_m2566209761(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Count() */, L_18);
		if (L_19)
		{
			goto IL_00be;
		}
	}

IL_00a3:
	{
		OnError_t314785609 * L_20 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_20)
		{
			goto IL_00bc;
		}
	}
	{
		OnError_t314785609 * L_21 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_21);
		OnError_Invoke_m3917243719(L_21, _stringLiteral3893179053, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		return (bool)0;
	}

IL_00be:
	{
		V_0 = (bool)0;
	}

IL_00c0:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				V_1 = 0;
				DBSchema_t4247391996 * L_22 = ___schema0;
				NullCheck(L_22);
				String_t* L_23 = DBSchema_get_TableName_m230189915(L_22, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_24 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2330800566, L_23, _stringLiteral40, /*hidden argument*/NULL);
				V_2 = L_24;
				V_3 = 0;
				goto IL_0348;
			}

IL_00df:
			{
				int32_t L_25 = V_3;
				DBSchema_t4247391996 * L_26 = ___schema0;
				NullCheck(L_26);
				List_1_t282322642 * L_27 = DBSchema_GetTableFields_m2566209761(L_26, /*hidden argument*/NULL);
				NullCheck(L_27);
				int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Count() */, L_27);
				if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)((int32_t)L_28-(int32_t)1))))))
				{
					goto IL_0235;
				}
			}

IL_00f2:
			{
				String_t* L_29 = V_2;
				V_4 = L_29;
				ObjectU5BU5D_t11523773* L_30 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)7));
				String_t* L_31 = V_4;
				NullCheck(L_30);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
				ArrayElementTypeCheck (L_30, L_31);
				(L_30)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_31);
				ObjectU5BU5D_t11523773* L_32 = L_30;
				DBSchema_t4247391996 * L_33 = ___schema0;
				NullCheck(L_33);
				List_1_t282322642 * L_34 = DBSchema_GetTableFields_m2566209761(L_33, /*hidden argument*/NULL);
				int32_t L_35 = V_3;
				NullCheck(L_34);
				DB_Field_t3780330969  L_36 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_34, L_35);
				V_5 = L_36;
				String_t* L_37 = (&V_5)->get_name_0();
				NullCheck(L_32);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 1);
				ArrayElementTypeCheck (L_32, L_37);
				(L_32)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_37);
				ObjectU5BU5D_t11523773* L_38 = L_32;
				NullCheck(L_38);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 2);
				ArrayElementTypeCheck (L_38, _stringLiteral32);
				(L_38)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral32);
				ObjectU5BU5D_t11523773* L_39 = L_38;
				DBSchema_t4247391996 * L_40 = ___schema0;
				NullCheck(L_40);
				List_1_t282322642 * L_41 = DBSchema_GetTableFields_m2566209761(L_40, /*hidden argument*/NULL);
				int32_t L_42 = V_3;
				NullCheck(L_41);
				DB_Field_t3780330969  L_43 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_41, L_42);
				V_6 = L_43;
				int32_t L_44 = (&V_6)->get_type_1();
				String_t* L_45 = SQLiteDB_GetDataType_m3557671473(__this, L_44, /*hidden argument*/NULL);
				NullCheck(L_39);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 3);
				ArrayElementTypeCheck (L_39, L_45);
				(L_39)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_45);
				ObjectU5BU5D_t11523773* L_46 = L_39;
				NullCheck(L_46);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 4);
				ArrayElementTypeCheck (L_46, _stringLiteral40);
				(L_46)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral40);
				ObjectU5BU5D_t11523773* L_47 = L_46;
				DBSchema_t4247391996 * L_48 = ___schema0;
				NullCheck(L_48);
				List_1_t282322642 * L_49 = DBSchema_GetTableFields_m2566209761(L_48, /*hidden argument*/NULL);
				int32_t L_50 = V_3;
				NullCheck(L_49);
				DB_Field_t3780330969  L_51 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_49, L_50);
				V_7 = L_51;
				int32_t L_52 = (&V_7)->get_size_2();
				int32_t L_53 = L_52;
				Il2CppObject * L_54 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_53);
				NullCheck(L_47);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_47, 5);
				ArrayElementTypeCheck (L_47, L_54);
				(L_47)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_54);
				ObjectU5BU5D_t11523773* L_55 = L_47;
				NullCheck(L_55);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 6);
				ArrayElementTypeCheck (L_55, _stringLiteral1303);
				(L_55)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral1303);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_56 = String_Concat_m3016520001(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
				V_2 = L_56;
				DBSchema_t4247391996 * L_57 = ___schema0;
				NullCheck(L_57);
				List_1_t282322642 * L_58 = DBSchema_GetTableFields_m2566209761(L_57, /*hidden argument*/NULL);
				int32_t L_59 = V_3;
				NullCheck(L_58);
				DB_Field_t3780330969  L_60 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_58, L_59);
				V_8 = L_60;
				bool L_61 = (&V_8)->get_isNotNull_4();
				if (!L_61)
				{
					goto IL_0197;
				}
			}

IL_018b:
			{
				String_t* L_62 = V_2;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_63 = String_Concat_m138640077(NULL /*static, unused*/, L_62, _stringLiteral2330375892, /*hidden argument*/NULL);
				V_2 = L_63;
			}

IL_0197:
			{
				DBSchema_t4247391996 * L_64 = ___schema0;
				NullCheck(L_64);
				List_1_t282322642 * L_65 = DBSchema_GetTableFields_m2566209761(L_64, /*hidden argument*/NULL);
				int32_t L_66 = V_3;
				NullCheck(L_65);
				DB_Field_t3780330969  L_67 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_65, L_66);
				V_9 = L_67;
				bool L_68 = (&V_9)->get_isUnique_5();
				if (L_68)
				{
					goto IL_01cb;
				}
			}

IL_01b1:
			{
				DBSchema_t4247391996 * L_69 = ___schema0;
				NullCheck(L_69);
				List_1_t282322642 * L_70 = DBSchema_GetTableFields_m2566209761(L_69, /*hidden argument*/NULL);
				int32_t L_71 = V_3;
				NullCheck(L_70);
				DB_Field_t3780330969  L_72 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_70, L_71);
				V_10 = L_72;
				bool L_73 = (&V_10)->get_isPrimaryKey_3();
				if (!L_73)
				{
					goto IL_01d7;
				}
			}

IL_01cb:
			{
				String_t* L_74 = V_2;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_75 = String_Concat_m138640077(NULL /*static, unused*/, L_74, _stringLiteral2507767761, /*hidden argument*/NULL);
				V_2 = L_75;
			}

IL_01d7:
			{
				DBSchema_t4247391996 * L_76 = ___schema0;
				NullCheck(L_76);
				List_1_t282322642 * L_77 = DBSchema_GetTableFields_m2566209761(L_76, /*hidden argument*/NULL);
				int32_t L_78 = V_3;
				NullCheck(L_77);
				DB_Field_t3780330969  L_79 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_77, L_78);
				V_11 = L_79;
				bool L_80 = (&V_11)->get_isPrimaryKey_3();
				if (!L_80)
				{
					goto IL_01f5;
				}
			}

IL_01f1:
			{
				int32_t L_81 = V_3;
				V_1 = ((int32_t)((int32_t)L_81+(int32_t)1));
			}

IL_01f5:
			{
				int32_t L_82 = V_1;
				if ((((int32_t)L_82) <= ((int32_t)0)))
				{
					goto IL_0224;
				}
			}

IL_01fc:
			{
				String_t* L_83 = V_2;
				DBSchema_t4247391996 * L_84 = ___schema0;
				NullCheck(L_84);
				List_1_t282322642 * L_85 = DBSchema_GetTableFields_m2566209761(L_84, /*hidden argument*/NULL);
				int32_t L_86 = V_1;
				NullCheck(L_85);
				DB_Field_t3780330969  L_87 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_85, ((int32_t)((int32_t)L_86-(int32_t)1)));
				V_12 = L_87;
				String_t* L_88 = (&V_12)->get_name_0();
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_89 = String_Concat_m2933632197(NULL /*static, unused*/, L_83, _stringLiteral1149335411, L_88, _stringLiteral41, /*hidden argument*/NULL);
				V_2 = L_89;
			}

IL_0224:
			{
				String_t* L_90 = V_2;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_91 = String_Concat_m138640077(NULL /*static, unused*/, L_90, _stringLiteral41, /*hidden argument*/NULL);
				V_2 = L_91;
				goto IL_0344;
			}

IL_0235:
			{
				String_t* L_92 = V_2;
				V_4 = L_92;
				ObjectU5BU5D_t11523773* L_93 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)7));
				String_t* L_94 = V_4;
				NullCheck(L_93);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_93, 0);
				ArrayElementTypeCheck (L_93, L_94);
				(L_93)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_94);
				ObjectU5BU5D_t11523773* L_95 = L_93;
				DBSchema_t4247391996 * L_96 = ___schema0;
				NullCheck(L_96);
				List_1_t282322642 * L_97 = DBSchema_GetTableFields_m2566209761(L_96, /*hidden argument*/NULL);
				int32_t L_98 = V_3;
				NullCheck(L_97);
				DB_Field_t3780330969  L_99 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_97, L_98);
				V_13 = L_99;
				String_t* L_100 = (&V_13)->get_name_0();
				NullCheck(L_95);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_95, 1);
				ArrayElementTypeCheck (L_95, L_100);
				(L_95)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_100);
				ObjectU5BU5D_t11523773* L_101 = L_95;
				NullCheck(L_101);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_101, 2);
				ArrayElementTypeCheck (L_101, _stringLiteral32);
				(L_101)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral32);
				ObjectU5BU5D_t11523773* L_102 = L_101;
				DBSchema_t4247391996 * L_103 = ___schema0;
				NullCheck(L_103);
				List_1_t282322642 * L_104 = DBSchema_GetTableFields_m2566209761(L_103, /*hidden argument*/NULL);
				int32_t L_105 = V_3;
				NullCheck(L_104);
				DB_Field_t3780330969  L_106 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_104, L_105);
				V_14 = L_106;
				int32_t L_107 = (&V_14)->get_type_1();
				String_t* L_108 = SQLiteDB_GetDataType_m3557671473(__this, L_107, /*hidden argument*/NULL);
				NullCheck(L_102);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_102, 3);
				ArrayElementTypeCheck (L_102, L_108);
				(L_102)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_108);
				ObjectU5BU5D_t11523773* L_109 = L_102;
				NullCheck(L_109);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_109, 4);
				ArrayElementTypeCheck (L_109, _stringLiteral40);
				(L_109)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral40);
				ObjectU5BU5D_t11523773* L_110 = L_109;
				DBSchema_t4247391996 * L_111 = ___schema0;
				NullCheck(L_111);
				List_1_t282322642 * L_112 = DBSchema_GetTableFields_m2566209761(L_111, /*hidden argument*/NULL);
				int32_t L_113 = V_3;
				NullCheck(L_112);
				DB_Field_t3780330969  L_114 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_112, L_113);
				V_15 = L_114;
				int32_t L_115 = (&V_15)->get_size_2();
				int32_t L_116 = L_115;
				Il2CppObject * L_117 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_116);
				NullCheck(L_110);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_110, 5);
				ArrayElementTypeCheck (L_110, L_117);
				(L_110)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_117);
				ObjectU5BU5D_t11523773* L_118 = L_110;
				NullCheck(L_118);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_118, 6);
				ArrayElementTypeCheck (L_118, _stringLiteral1303);
				(L_118)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral1303);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_119 = String_Concat_m3016520001(NULL /*static, unused*/, L_118, /*hidden argument*/NULL);
				V_2 = L_119;
				DBSchema_t4247391996 * L_120 = ___schema0;
				NullCheck(L_120);
				List_1_t282322642 * L_121 = DBSchema_GetTableFields_m2566209761(L_120, /*hidden argument*/NULL);
				int32_t L_122 = V_3;
				NullCheck(L_121);
				DB_Field_t3780330969  L_123 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_121, L_122);
				V_16 = L_123;
				bool L_124 = (&V_16)->get_isNotNull_4();
				if (!L_124)
				{
					goto IL_02da;
				}
			}

IL_02ce:
			{
				String_t* L_125 = V_2;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_126 = String_Concat_m138640077(NULL /*static, unused*/, L_125, _stringLiteral2330375892, /*hidden argument*/NULL);
				V_2 = L_126;
			}

IL_02da:
			{
				DBSchema_t4247391996 * L_127 = ___schema0;
				NullCheck(L_127);
				List_1_t282322642 * L_128 = DBSchema_GetTableFields_m2566209761(L_127, /*hidden argument*/NULL);
				int32_t L_129 = V_3;
				NullCheck(L_128);
				DB_Field_t3780330969  L_130 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_128, L_129);
				V_17 = L_130;
				bool L_131 = (&V_17)->get_isUnique_5();
				if (L_131)
				{
					goto IL_030e;
				}
			}

IL_02f4:
			{
				DBSchema_t4247391996 * L_132 = ___schema0;
				NullCheck(L_132);
				List_1_t282322642 * L_133 = DBSchema_GetTableFields_m2566209761(L_132, /*hidden argument*/NULL);
				int32_t L_134 = V_3;
				NullCheck(L_133);
				DB_Field_t3780330969  L_135 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_133, L_134);
				V_18 = L_135;
				bool L_136 = (&V_18)->get_isPrimaryKey_3();
				if (!L_136)
				{
					goto IL_031a;
				}
			}

IL_030e:
			{
				String_t* L_137 = V_2;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_138 = String_Concat_m138640077(NULL /*static, unused*/, L_137, _stringLiteral2507767761, /*hidden argument*/NULL);
				V_2 = L_138;
			}

IL_031a:
			{
				DBSchema_t4247391996 * L_139 = ___schema0;
				NullCheck(L_139);
				List_1_t282322642 * L_140 = DBSchema_GetTableFields_m2566209761(L_139, /*hidden argument*/NULL);
				int32_t L_141 = V_3;
				NullCheck(L_140);
				DB_Field_t3780330969  L_142 = VirtFuncInvoker1< DB_Field_t3780330969 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32) */, L_140, L_141);
				V_19 = L_142;
				bool L_143 = (&V_19)->get_isPrimaryKey_3();
				if (!L_143)
				{
					goto IL_0338;
				}
			}

IL_0334:
			{
				int32_t L_144 = V_3;
				V_1 = ((int32_t)((int32_t)L_144+(int32_t)1));
			}

IL_0338:
			{
				String_t* L_145 = V_2;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_146 = String_Concat_m138640077(NULL /*static, unused*/, L_145, _stringLiteral44, /*hidden argument*/NULL);
				V_2 = L_146;
			}

IL_0344:
			{
				int32_t L_147 = V_3;
				V_3 = ((int32_t)((int32_t)L_147+(int32_t)1));
			}

IL_0348:
			{
				int32_t L_148 = V_3;
				DBSchema_t4247391996 * L_149 = ___schema0;
				NullCheck(L_149);
				List_1_t282322642 * L_150 = DBSchema_GetTableFields_m2566209761(L_149, /*hidden argument*/NULL);
				NullCheck(L_150);
				int32_t L_151 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Count() */, L_150);
				if ((((int32_t)L_148) < ((int32_t)L_151)))
				{
					goto IL_00df;
				}
			}

IL_0359:
			{
				SqliteConnection_t3853176977 * L_152 = __this->get_connection_4();
				NullCheck(L_152);
				VirtActionInvoker0::Invoke(26 /* System.Void System.Data.Common.DbConnection::Open() */, L_152);
				SqliteConnection_t3853176977 * L_153 = __this->get_connection_4();
				NullCheck(L_153);
				SqliteCommand_t4229878246 * L_154 = SqliteConnection_CreateCommand_m110869411(L_153, /*hidden argument*/NULL);
				__this->set_command_6(L_154);
				SqliteCommand_t4229878246 * L_155 = __this->get_command_6();
				String_t* L_156 = V_2;
				NullCheck(L_155);
				VirtActionInvoker1< String_t* >::Invoke(18 /* System.Void System.Data.Common.DbCommand::set_CommandText(System.String) */, L_155, L_156);
				SqliteCommand_t4229878246 * L_157 = __this->get_command_6();
				NullCheck(L_157);
				VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 System.Data.Common.DbCommand::ExecuteNonQuery() */, L_157);
				V_0 = (bool)1;
				IL2CPP_LEAVE(0x3D9, FINALLY_03c2);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0394;
			throw e;
		}

CATCH_0394:
		{ // begin catch(System.Exception)
			{
				V_20 = ((Exception_t1967233988 *)__exception_local);
				OnError_t314785609 * L_158 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				if (!L_158)
				{
					goto IL_03bb;
				}
			}

IL_03a0:
			{
				OnError_t314785609 * L_159 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				Exception_t1967233988 * L_160 = V_20;
				NullCheck(L_160);
				String_t* L_161 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_160);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_162 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral824662974, L_161, /*hidden argument*/NULL);
				NullCheck(L_159);
				OnError_Invoke_m3917243719(L_159, L_162, /*hidden argument*/NULL);
			}

IL_03bb:
			{
				V_0 = (bool)0;
				IL2CPP_LEAVE(0x3D9, FINALLY_03c2);
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_03c2;
	}

FINALLY_03c2:
	{ // begin finally (depth: 1)
		SqliteCommand_t4229878246 * L_163 = __this->get_command_6();
		NullCheck(L_163);
		VirtActionInvoker0::Invoke(6 /* System.Void System.ComponentModel.Component::Dispose() */, L_163);
		SqliteConnection_t3853176977 * L_164 = __this->get_connection_4();
		NullCheck(L_164);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Data.Common.DbConnection::Close() */, L_164);
		IL2CPP_END_FINALLY(962)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(962)
	{
		IL2CPP_JUMP_TBL(0x3D9, IL_03d9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_03d9:
	{
		bool L_165 = V_0;
		return L_165;
	}
}
// System.Boolean SQLiteDatabase.SQLiteDB::DeleteTable(System.String)
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3679522742;
extern Il2CppCodeGenString* _stringLiteral167887376;
extern Il2CppCodeGenString* _stringLiteral377003107;
extern Il2CppCodeGenString* _stringLiteral2144084333;
extern const uint32_t SQLiteDB_DeleteTable_m3319765176_MetadataUsageId;
extern "C"  bool SQLiteDB_DeleteTable_m3319765176 (SQLiteDB_t557922535 * __this, String_t* ___tableName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_DeleteTable_m3319765176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	Exception_t1967233988 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = SQLiteDB_get_Exists_m3278641920(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		OnError_t314785609 * L_1 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		OnError_t314785609 * L_2 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_2);
		OnError_Invoke_m3917243719(L_2, _stringLiteral3679522742, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return (bool)0;
	}

IL_0026:
	{
		String_t* L_3 = ___tableName0;
		bool L_4 = SQLiteDB_IsTableExists_m4049229627(__this, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_004d;
		}
	}
	{
		OnError_t314785609 * L_5 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		OnError_t314785609 * L_6 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_6);
		OnError_Invoke_m3917243719(L_6, _stringLiteral167887376, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return (bool)0;
	}

IL_004d:
	{
		V_0 = (bool)0;
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			String_t* L_7 = ___tableName0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_8 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral377003107, L_7, /*hidden argument*/NULL);
			V_1 = L_8;
			SqliteConnection_t3853176977 * L_9 = __this->get_connection_4();
			NullCheck(L_9);
			VirtActionInvoker0::Invoke(26 /* System.Void System.Data.Common.DbConnection::Open() */, L_9);
			SqliteConnection_t3853176977 * L_10 = __this->get_connection_4();
			NullCheck(L_10);
			SqliteCommand_t4229878246 * L_11 = SqliteConnection_CreateCommand_m110869411(L_10, /*hidden argument*/NULL);
			__this->set_command_6(L_11);
			SqliteCommand_t4229878246 * L_12 = __this->get_command_6();
			String_t* L_13 = V_1;
			NullCheck(L_12);
			VirtActionInvoker1< String_t* >::Invoke(18 /* System.Void System.Data.Common.DbCommand::set_CommandText(System.String) */, L_12, L_13);
			SqliteCommand_t4229878246 * L_14 = __this->get_command_6();
			NullCheck(L_14);
			VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 System.Data.Common.DbCommand::ExecuteNonQuery() */, L_14);
			V_0 = (bool)1;
			IL2CPP_LEAVE(0xD9, FINALLY_00c2);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0096;
			throw e;
		}

CATCH_0096:
		{ // begin catch(System.Exception)
			{
				V_2 = ((Exception_t1967233988 *)__exception_local);
				OnError_t314785609 * L_15 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				if (!L_15)
				{
					goto IL_00bb;
				}
			}

IL_00a1:
			{
				OnError_t314785609 * L_16 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				Exception_t1967233988 * L_17 = V_2;
				NullCheck(L_17);
				String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_17);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_19 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2144084333, L_18, /*hidden argument*/NULL);
				NullCheck(L_16);
				OnError_Invoke_m3917243719(L_16, L_19, /*hidden argument*/NULL);
			}

IL_00bb:
			{
				V_0 = (bool)0;
				IL2CPP_LEAVE(0xD9, FINALLY_00c2);
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00c2;
	}

FINALLY_00c2:
	{ // begin finally (depth: 1)
		SqliteCommand_t4229878246 * L_20 = __this->get_command_6();
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(6 /* System.Void System.ComponentModel.Component::Dispose() */, L_20);
		SqliteConnection_t3853176977 * L_21 = __this->get_connection_4();
		NullCheck(L_21);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Data.Common.DbConnection::Close() */, L_21);
		IL2CPP_END_FINALLY(194)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(194)
	{
		IL2CPP_JUMP_TBL(0xD9, IL_00d9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00d9:
	{
		bool L_22 = V_0;
		return L_22;
	}
}
// System.Int32 SQLiteDatabase.SQLiteDB::ClearTable(System.String)
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3679522742;
extern Il2CppCodeGenString* _stringLiteral167887376;
extern Il2CppCodeGenString* _stringLiteral2061912833;
extern Il2CppCodeGenString* _stringLiteral540174111;
extern const uint32_t SQLiteDB_ClearTable_m4150127616_MetadataUsageId;
extern "C"  int32_t SQLiteDB_ClearTable_m4150127616 (SQLiteDB_t557922535 * __this, String_t* ___tableName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_ClearTable_m4150127616_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	Exception_t1967233988 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (-1);
		bool L_0 = SQLiteDB_get_Exists_m3278641920(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0028;
		}
	}
	{
		OnError_t314785609 * L_1 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		OnError_t314785609 * L_2 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_2);
		OnError_Invoke_m3917243719(L_2, _stringLiteral3679522742, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return (-1);
	}

IL_0028:
	{
		String_t* L_3 = ___tableName0;
		bool L_4 = SQLiteDB_IsTableExists_m4049229627(__this, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_004f;
		}
	}
	{
		OnError_t314785609 * L_5 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		OnError_t314785609 * L_6 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_6);
		OnError_Invoke_m3917243719(L_6, _stringLiteral167887376, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return (-1);
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			String_t* L_7 = ___tableName0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_8 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2061912833, L_7, /*hidden argument*/NULL);
			V_1 = L_8;
			SqliteConnection_t3853176977 * L_9 = __this->get_connection_4();
			NullCheck(L_9);
			VirtActionInvoker0::Invoke(26 /* System.Void System.Data.Common.DbConnection::Open() */, L_9);
			SqliteConnection_t3853176977 * L_10 = __this->get_connection_4();
			NullCheck(L_10);
			SqliteCommand_t4229878246 * L_11 = SqliteConnection_CreateCommand_m110869411(L_10, /*hidden argument*/NULL);
			__this->set_command_6(L_11);
			SqliteCommand_t4229878246 * L_12 = __this->get_command_6();
			String_t* L_13 = V_1;
			NullCheck(L_12);
			VirtActionInvoker1< String_t* >::Invoke(18 /* System.Void System.Data.Common.DbCommand::set_CommandText(System.String) */, L_12, L_13);
			SqliteCommand_t4229878246 * L_14 = __this->get_command_6();
			NullCheck(L_14);
			int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 System.Data.Common.DbCommand::ExecuteNonQuery() */, L_14);
			V_0 = L_15;
			IL2CPP_LEAVE(0xD5, FINALLY_00be);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0094;
			throw e;
		}

CATCH_0094:
		{ // begin catch(System.Exception)
			{
				V_2 = ((Exception_t1967233988 *)__exception_local);
				OnError_t314785609 * L_16 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				if (!L_16)
				{
					goto IL_00b9;
				}
			}

IL_009f:
			{
				OnError_t314785609 * L_17 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				Exception_t1967233988 * L_18 = V_2;
				NullCheck(L_18);
				String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_18);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_20 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral540174111, L_19, /*hidden argument*/NULL);
				NullCheck(L_17);
				OnError_Invoke_m3917243719(L_17, L_20, /*hidden argument*/NULL);
			}

IL_00b9:
			{
				IL2CPP_LEAVE(0xD5, FINALLY_00be);
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00be;
	}

FINALLY_00be:
	{ // begin finally (depth: 1)
		SqliteCommand_t4229878246 * L_21 = __this->get_command_6();
		NullCheck(L_21);
		VirtActionInvoker0::Invoke(6 /* System.Void System.ComponentModel.Component::Dispose() */, L_21);
		SqliteConnection_t3853176977 * L_22 = __this->get_connection_4();
		NullCheck(L_22);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Data.Common.DbConnection::Close() */, L_22);
		IL2CPP_END_FINALLY(190)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(190)
	{
		IL2CPP_JUMP_TBL(0xD5, IL_00d5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00d5:
	{
		int32_t L_23 = V_0;
		return L_23;
	}
}
// System.Boolean SQLiteDatabase.SQLiteDB::DeleteDatabase()
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t2029632748_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3679522742;
extern Il2CppCodeGenString* _stringLiteral2904602100;
extern const uint32_t SQLiteDB_DeleteDatabase_m2858673313_MetadataUsageId;
extern "C"  bool SQLiteDB_DeleteDatabase_m2858673313 (SQLiteDB_t557922535 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_DeleteDatabase_m2858673313_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	Exception_t1967233988 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = SQLiteDB_get_Exists_m3278641920(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		OnError_t314785609 * L_1 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		OnError_t314785609 * L_2 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_2);
		OnError_Invoke_m3917243719(L_2, _stringLiteral3679522742, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return (bool)0;
	}

IL_0026:
	{
		V_0 = (bool)0;
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				String_t* L_3 = __this->get__dbPath_2();
				String_t* L_4 = __this->get__dbName_1();
				IL2CPP_RUNTIME_CLASS_INIT(Path_t2029632748_il2cpp_TypeInfo_var);
				String_t* L_5 = Path_Combine_m4122812896(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
				V_1 = L_5;
				String_t* L_6 = V_1;
				bool L_7 = File_Exists_m1326262381(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
				if (!L_7)
				{
					goto IL_0058;
				}
			}

IL_0045:
			{
				SQLiteDB_Dispose_m2829228156(__this, /*hidden argument*/NULL);
				String_t* L_8 = V_1;
				File_Delete_m760984832(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
				V_0 = (bool)1;
				goto IL_0073;
			}

IL_0058:
			{
				OnError_t314785609 * L_9 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				if (!L_9)
				{
					goto IL_0071;
				}
			}

IL_0062:
			{
				OnError_t314785609 * L_10 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				NullCheck(L_10);
				OnError_Invoke_m3917243719(L_10, _stringLiteral3679522742, /*hidden argument*/NULL);
			}

IL_0071:
			{
				V_0 = (bool)0;
			}

IL_0073:
			{
				IL2CPP_LEAVE(0xA5, FINALLY_00a4);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0078;
			throw e;
		}

CATCH_0078:
		{ // begin catch(System.Exception)
			{
				V_2 = ((Exception_t1967233988 *)__exception_local);
				OnError_t314785609 * L_11 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				if (!L_11)
				{
					goto IL_009d;
				}
			}

IL_0083:
			{
				OnError_t314785609 * L_12 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				Exception_t1967233988 * L_13 = V_2;
				NullCheck(L_13);
				String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_13);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_15 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2904602100, L_14, /*hidden argument*/NULL);
				NullCheck(L_12);
				OnError_Invoke_m3917243719(L_12, L_15, /*hidden argument*/NULL);
			}

IL_009d:
			{
				V_0 = (bool)0;
				IL2CPP_LEAVE(0xA5, FINALLY_00a4);
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00a4;
	}

FINALLY_00a4:
	{ // begin finally (depth: 1)
		IL2CPP_END_FINALLY(164)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(164)
	{
		IL2CPP_JUMP_TBL(0xA5, IL_00a5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a5:
	{
		bool L_16 = V_0;
		return L_16;
	}
}
// System.Int32 SQLiteDatabase.SQLiteDB::Insert(System.String,System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>)
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3679522742;
extern Il2CppCodeGenString* _stringLiteral167887363;
extern Il2CppCodeGenString* _stringLiteral3578695065;
extern Il2CppCodeGenString* _stringLiteral40;
extern Il2CppCodeGenString* _stringLiteral880853441;
extern Il2CppCodeGenString* _stringLiteral1303;
extern Il2CppCodeGenString* _stringLiteral1250;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral38882;
extern Il2CppCodeGenString* _stringLiteral830106893;
extern const uint32_t SQLiteDB_Insert_m4013404553_MetadataUsageId;
extern "C"  int32_t SQLiteDB_Insert_m4013404553 (SQLiteDB_t557922535 * __this, String_t* ___tableName0, List_1_t722451806 * ___dataList1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_Insert_m4013404553_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	DB_DataPair_t4220460133  V_4;
	memset(&V_4, 0, sizeof(V_4));
	String_t* V_5 = NULL;
	Exception_t1967233988 * V_6 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = SQLiteDB_get_Exists_m3278641920(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		OnError_t314785609 * L_1 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		OnError_t314785609 * L_2 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_2);
		OnError_Invoke_m3917243719(L_2, _stringLiteral3679522742, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return (-1);
	}

IL_0026:
	{
		String_t* L_3 = ___tableName0;
		bool L_4 = SQLiteDB_IsTableExists_m4049229627(__this, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_004d;
		}
	}
	{
		OnError_t314785609 * L_5 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		OnError_t314785609 * L_6 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_6);
		OnError_Invoke_m3917243719(L_6, _stringLiteral167887363, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return (-1);
	}

IL_004d:
	{
		V_0 = (-1);
	}

IL_004f:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				String_t* L_7 = ___tableName0;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_8 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3578695065, L_7, _stringLiteral40, /*hidden argument*/NULL);
				V_1 = L_8;
				V_2 = _stringLiteral880853441;
				V_3 = 0;
				goto IL_00d9;
			}

IL_006d:
			{
				List_1_t722451806 * L_9 = ___dataList1;
				int32_t L_10 = V_3;
				NullCheck(L_9);
				DB_DataPair_t4220460133  L_11 = VirtFuncInvoker1< DB_DataPair_t4220460133 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Item(System.Int32) */, L_9, L_10);
				V_4 = L_11;
				int32_t L_12 = V_3;
				List_1_t722451806 * L_13 = ___dataList1;
				NullCheck(L_13);
				int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Count() */, L_13);
				if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)((int32_t)L_14-(int32_t)1))))))
				{
					goto IL_00af;
				}
			}

IL_0084:
			{
				String_t* L_15 = V_1;
				String_t* L_16 = (&V_4)->get_fieldName_0();
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_17 = String_Concat_m1825781833(NULL /*static, unused*/, L_15, L_16, _stringLiteral1303, /*hidden argument*/NULL);
				V_1 = L_17;
				String_t* L_18 = V_2;
				String_t* L_19 = (&V_4)->get_value_1();
				String_t* L_20 = String_Concat_m1825781833(NULL /*static, unused*/, L_18, L_19, _stringLiteral1250, /*hidden argument*/NULL);
				V_2 = L_20;
				goto IL_00d5;
			}

IL_00af:
			{
				String_t* L_21 = V_1;
				String_t* L_22 = (&V_4)->get_fieldName_0();
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_23 = String_Concat_m1825781833(NULL /*static, unused*/, L_21, L_22, _stringLiteral44, /*hidden argument*/NULL);
				V_1 = L_23;
				String_t* L_24 = V_2;
				String_t* L_25 = (&V_4)->get_value_1();
				String_t* L_26 = String_Concat_m1825781833(NULL /*static, unused*/, L_24, L_25, _stringLiteral38882, /*hidden argument*/NULL);
				V_2 = L_26;
			}

IL_00d5:
			{
				int32_t L_27 = V_3;
				V_3 = ((int32_t)((int32_t)L_27+(int32_t)1));
			}

IL_00d9:
			{
				int32_t L_28 = V_3;
				List_1_t722451806 * L_29 = ___dataList1;
				NullCheck(L_29);
				int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Count() */, L_29);
				if ((((int32_t)L_28) < ((int32_t)L_30)))
				{
					goto IL_006d;
				}
			}

IL_00e5:
			{
				String_t* L_31 = V_1;
				String_t* L_32 = V_2;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_33 = String_Concat_m138640077(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
				V_5 = L_33;
				SqliteConnection_t3853176977 * L_34 = __this->get_connection_4();
				NullCheck(L_34);
				VirtActionInvoker0::Invoke(26 /* System.Void System.Data.Common.DbConnection::Open() */, L_34);
				SqliteConnection_t3853176977 * L_35 = __this->get_connection_4();
				NullCheck(L_35);
				SqliteCommand_t4229878246 * L_36 = SqliteConnection_CreateCommand_m110869411(L_35, /*hidden argument*/NULL);
				__this->set_command_6(L_36);
				SqliteCommand_t4229878246 * L_37 = __this->get_command_6();
				String_t* L_38 = V_5;
				NullCheck(L_37);
				VirtActionInvoker1< String_t* >::Invoke(18 /* System.Void System.Data.Common.DbCommand::set_CommandText(System.String) */, L_37, L_38);
				SqliteCommand_t4229878246 * L_39 = __this->get_command_6();
				NullCheck(L_39);
				int32_t L_40 = VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 System.Data.Common.DbCommand::ExecuteNonQuery() */, L_39);
				V_0 = L_40;
				IL2CPP_LEAVE(0x16B, FINALLY_0154);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0128;
			throw e;
		}

CATCH_0128:
		{ // begin catch(System.Exception)
			{
				V_6 = ((Exception_t1967233988 *)__exception_local);
				OnError_t314785609 * L_41 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				if (!L_41)
				{
					goto IL_014f;
				}
			}

IL_0134:
			{
				OnError_t314785609 * L_42 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				Exception_t1967233988 * L_43 = V_6;
				NullCheck(L_43);
				String_t* L_44 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_43);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_45 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral830106893, L_44, /*hidden argument*/NULL);
				NullCheck(L_42);
				OnError_Invoke_m3917243719(L_42, L_45, /*hidden argument*/NULL);
			}

IL_014f:
			{
				IL2CPP_LEAVE(0x16B, FINALLY_0154);
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0154;
	}

FINALLY_0154:
	{ // begin finally (depth: 1)
		SqliteCommand_t4229878246 * L_46 = __this->get_command_6();
		NullCheck(L_46);
		VirtActionInvoker0::Invoke(6 /* System.Void System.ComponentModel.Component::Dispose() */, L_46);
		SqliteConnection_t3853176977 * L_47 = __this->get_connection_4();
		NullCheck(L_47);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Data.Common.DbConnection::Close() */, L_47);
		IL2CPP_END_FINALLY(340)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(340)
	{
		IL2CPP_JUMP_TBL(0x16B, IL_016b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_016b:
	{
		int32_t L_48 = V_0;
		return L_48;
	}
}
// System.Int32 SQLiteDatabase.SQLiteDB::Update(System.String,System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>,SQLiteDatabase.SQLiteDB/DB_ConditionPair)
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3679522742;
extern Il2CppCodeGenString* _stringLiteral167887363;
extern Il2CppCodeGenString* _stringLiteral2505246425;
extern Il2CppCodeGenString* _stringLiteral4060536951;
extern Il2CppCodeGenString* _stringLiteral33079326;
extern Il2CppCodeGenString* _stringLiteral1930;
extern Il2CppCodeGenString* _stringLiteral1091509810;
extern Il2CppCodeGenString* _stringLiteral39;
extern Il2CppCodeGenString* _stringLiteral1253;
extern Il2CppCodeGenString* _stringLiteral1610915613;
extern const uint32_t SQLiteDB_Update_m4162022779_MetadataUsageId;
extern "C"  int32_t SQLiteDB_Update_m4162022779 (SQLiteDB_t557922535 * __this, String_t* ___tableName0, List_1_t722451806 * ___dataList1, DB_ConditionPair_t1745523828  ___condition2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_Update_m4162022779_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	DB_DataPair_t4220460133  V_4;
	memset(&V_4, 0, sizeof(V_4));
	DB_DataPair_t4220460133  V_5;
	memset(&V_5, 0, sizeof(V_5));
	DB_DataPair_t4220460133  V_6;
	memset(&V_6, 0, sizeof(V_6));
	DB_DataPair_t4220460133  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Exception_t1967233988 * V_8 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = SQLiteDB_get_Exists_m3278641920(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		OnError_t314785609 * L_1 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		OnError_t314785609 * L_2 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_2);
		OnError_Invoke_m3917243719(L_2, _stringLiteral3679522742, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return (-1);
	}

IL_0026:
	{
		String_t* L_3 = ___tableName0;
		bool L_4 = SQLiteDB_IsTableExists_m4049229627(__this, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_004d;
		}
	}
	{
		OnError_t314785609 * L_5 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		OnError_t314785609 * L_6 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_6);
		OnError_Invoke_m3917243719(L_6, _stringLiteral167887363, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return (-1);
	}

IL_004d:
	{
		String_t* L_7 = (&___condition2)->get_fieldName_0();
		NullCheck(L_7);
		String_t* L_8 = String_Trim_m1030489823(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_10 = (&___condition2)->get_value_1();
		NullCheck(L_10);
		String_t* L_11 = String_Trim_m1030489823(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0094;
		}
	}

IL_0079:
	{
		OnError_t314785609 * L_13 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_13)
		{
			goto IL_0092;
		}
	}
	{
		OnError_t314785609 * L_14 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_14);
		OnError_Invoke_m3917243719(L_14, _stringLiteral2505246425, /*hidden argument*/NULL);
	}

IL_0092:
	{
		return (-1);
	}

IL_0094:
	{
		V_0 = (-1);
	}

IL_0096:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				String_t* L_15 = ___tableName0;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_16 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral4060536951, L_15, _stringLiteral33079326, /*hidden argument*/NULL);
				V_1 = L_16;
				V_2 = 0;
				goto IL_018b;
			}

IL_00ae:
			{
				int32_t L_17 = V_2;
				List_1_t722451806 * L_18 = ___dataList1;
				NullCheck(L_18);
				int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Count() */, L_18);
				if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)((int32_t)L_19-(int32_t)1))))))
				{
					goto IL_013f;
				}
			}

IL_00bc:
			{
				String_t* L_20 = V_1;
				V_3 = L_20;
				StringU5BU5D_t2956870243* L_21 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
				String_t* L_22 = V_3;
				NullCheck(L_21);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 0);
				ArrayElementTypeCheck (L_21, L_22);
				(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_22);
				StringU5BU5D_t2956870243* L_23 = L_21;
				List_1_t722451806 * L_24 = ___dataList1;
				int32_t L_25 = V_2;
				NullCheck(L_24);
				DB_DataPair_t4220460133  L_26 = VirtFuncInvoker1< DB_DataPair_t4220460133 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Item(System.Int32) */, L_24, L_25);
				V_4 = L_26;
				String_t* L_27 = (&V_4)->get_fieldName_0();
				NullCheck(L_23);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 1);
				ArrayElementTypeCheck (L_23, L_27);
				(L_23)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_27);
				StringU5BU5D_t2956870243* L_28 = L_23;
				NullCheck(L_28);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 2);
				ArrayElementTypeCheck (L_28, _stringLiteral1930);
				(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1930);
				StringU5BU5D_t2956870243* L_29 = L_28;
				List_1_t722451806 * L_30 = ___dataList1;
				int32_t L_31 = V_2;
				NullCheck(L_30);
				DB_DataPair_t4220460133  L_32 = VirtFuncInvoker1< DB_DataPair_t4220460133 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Item(System.Int32) */, L_30, L_31);
				V_5 = L_32;
				String_t* L_33 = (&V_5)->get_value_1();
				NullCheck(L_29);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 3);
				ArrayElementTypeCheck (L_29, L_33);
				(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_33);
				StringU5BU5D_t2956870243* L_34 = L_29;
				NullCheck(L_34);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 4);
				ArrayElementTypeCheck (L_34, _stringLiteral1091509810);
				(L_34)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1091509810);
				StringU5BU5D_t2956870243* L_35 = L_34;
				String_t* L_36 = (&___condition2)->get_fieldName_0();
				NullCheck(L_35);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 5);
				ArrayElementTypeCheck (L_35, L_36);
				(L_35)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_36);
				StringU5BU5D_t2956870243* L_37 = L_35;
				int32_t L_38 = (&___condition2)->get_condition_2();
				String_t* L_39 = SQLiteDB_GetConditionSymbol_m1038213543(__this, L_38, /*hidden argument*/NULL);
				NullCheck(L_37);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 6);
				ArrayElementTypeCheck (L_37, L_39);
				(L_37)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)L_39);
				StringU5BU5D_t2956870243* L_40 = L_37;
				NullCheck(L_40);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 7);
				ArrayElementTypeCheck (L_40, _stringLiteral39);
				(L_40)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral39);
				StringU5BU5D_t2956870243* L_41 = L_40;
				String_t* L_42 = (&___condition2)->get_value_1();
				NullCheck(L_41);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 8);
				ArrayElementTypeCheck (L_41, L_42);
				(L_41)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)L_42);
				StringU5BU5D_t2956870243* L_43 = L_41;
				NullCheck(L_43);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)9));
				ArrayElementTypeCheck (L_43, _stringLiteral39);
				(L_43)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral39);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_44 = String_Concat_m21867311(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
				V_1 = L_44;
				goto IL_0187;
			}

IL_013f:
			{
				String_t* L_45 = V_1;
				V_3 = L_45;
				StringU5BU5D_t2956870243* L_46 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
				String_t* L_47 = V_3;
				NullCheck(L_46);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 0);
				ArrayElementTypeCheck (L_46, L_47);
				(L_46)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_47);
				StringU5BU5D_t2956870243* L_48 = L_46;
				List_1_t722451806 * L_49 = ___dataList1;
				int32_t L_50 = V_2;
				NullCheck(L_49);
				DB_DataPair_t4220460133  L_51 = VirtFuncInvoker1< DB_DataPair_t4220460133 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Item(System.Int32) */, L_49, L_50);
				V_6 = L_51;
				String_t* L_52 = (&V_6)->get_fieldName_0();
				NullCheck(L_48);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 1);
				ArrayElementTypeCheck (L_48, L_52);
				(L_48)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_52);
				StringU5BU5D_t2956870243* L_53 = L_48;
				NullCheck(L_53);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 2);
				ArrayElementTypeCheck (L_53, _stringLiteral1930);
				(L_53)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1930);
				StringU5BU5D_t2956870243* L_54 = L_53;
				List_1_t722451806 * L_55 = ___dataList1;
				int32_t L_56 = V_2;
				NullCheck(L_55);
				DB_DataPair_t4220460133  L_57 = VirtFuncInvoker1< DB_DataPair_t4220460133 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Item(System.Int32) */, L_55, L_56);
				V_7 = L_57;
				String_t* L_58 = (&V_7)->get_value_1();
				NullCheck(L_54);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 3);
				ArrayElementTypeCheck (L_54, L_58);
				(L_54)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_58);
				StringU5BU5D_t2956870243* L_59 = L_54;
				NullCheck(L_59);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_59, 4);
				ArrayElementTypeCheck (L_59, _stringLiteral1253);
				(L_59)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1253);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_60 = String_Concat_m21867311(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
				V_1 = L_60;
			}

IL_0187:
			{
				int32_t L_61 = V_2;
				V_2 = ((int32_t)((int32_t)L_61+(int32_t)1));
			}

IL_018b:
			{
				int32_t L_62 = V_2;
				List_1_t722451806 * L_63 = ___dataList1;
				NullCheck(L_63);
				int32_t L_64 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Count() */, L_63);
				if ((((int32_t)L_62) < ((int32_t)L_64)))
				{
					goto IL_00ae;
				}
			}

IL_0197:
			{
				SqliteConnection_t3853176977 * L_65 = __this->get_connection_4();
				NullCheck(L_65);
				VirtActionInvoker0::Invoke(26 /* System.Void System.Data.Common.DbConnection::Open() */, L_65);
				SqliteConnection_t3853176977 * L_66 = __this->get_connection_4();
				NullCheck(L_66);
				SqliteCommand_t4229878246 * L_67 = SqliteConnection_CreateCommand_m110869411(L_66, /*hidden argument*/NULL);
				__this->set_command_6(L_67);
				SqliteCommand_t4229878246 * L_68 = __this->get_command_6();
				String_t* L_69 = V_1;
				NullCheck(L_68);
				VirtActionInvoker1< String_t* >::Invoke(18 /* System.Void System.Data.Common.DbCommand::set_CommandText(System.String) */, L_68, L_69);
				SqliteCommand_t4229878246 * L_70 = __this->get_command_6();
				NullCheck(L_70);
				int32_t L_71 = VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 System.Data.Common.DbCommand::ExecuteNonQuery() */, L_70);
				V_0 = L_71;
				IL2CPP_LEAVE(0x213, FINALLY_01fc);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_01d0;
			throw e;
		}

CATCH_01d0:
		{ // begin catch(System.Exception)
			{
				V_8 = ((Exception_t1967233988 *)__exception_local);
				OnError_t314785609 * L_72 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				if (!L_72)
				{
					goto IL_01f7;
				}
			}

IL_01dc:
			{
				OnError_t314785609 * L_73 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				Exception_t1967233988 * L_74 = V_8;
				NullCheck(L_74);
				String_t* L_75 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_74);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_76 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1610915613, L_75, /*hidden argument*/NULL);
				NullCheck(L_73);
				OnError_Invoke_m3917243719(L_73, L_76, /*hidden argument*/NULL);
			}

IL_01f7:
			{
				IL2CPP_LEAVE(0x213, FINALLY_01fc);
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01fc;
	}

FINALLY_01fc:
	{ // begin finally (depth: 1)
		SqliteCommand_t4229878246 * L_77 = __this->get_command_6();
		NullCheck(L_77);
		VirtActionInvoker0::Invoke(6 /* System.Void System.ComponentModel.Component::Dispose() */, L_77);
		SqliteConnection_t3853176977 * L_78 = __this->get_connection_4();
		NullCheck(L_78);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Data.Common.DbConnection::Close() */, L_78);
		IL2CPP_END_FINALLY(508)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(508)
	{
		IL2CPP_JUMP_TBL(0x213, IL_0213)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0213:
	{
		int32_t L_79 = V_0;
		return L_79;
	}
}
// System.Int32 SQLiteDatabase.SQLiteDB::DeleteRow(System.String,SQLiteDatabase.SQLiteDB/DB_ConditionPair)
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3679522742;
extern Il2CppCodeGenString* _stringLiteral167887363;
extern Il2CppCodeGenString* _stringLiteral2505246425;
extern Il2CppCodeGenString* _stringLiteral2061912833;
extern Il2CppCodeGenString* _stringLiteral1841383481;
extern Il2CppCodeGenString* _stringLiteral39;
extern Il2CppCodeGenString* _stringLiteral2029867321;
extern const uint32_t SQLiteDB_DeleteRow_m1399588258_MetadataUsageId;
extern "C"  int32_t SQLiteDB_DeleteRow_m1399588258 (SQLiteDB_t557922535 * __this, String_t* ___tableName0, DB_ConditionPair_t1745523828  ___condition1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_DeleteRow_m1399588258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	Exception_t1967233988 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = SQLiteDB_get_Exists_m3278641920(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		OnError_t314785609 * L_1 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		OnError_t314785609 * L_2 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_2);
		OnError_Invoke_m3917243719(L_2, _stringLiteral3679522742, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return (-1);
	}

IL_0026:
	{
		String_t* L_3 = ___tableName0;
		bool L_4 = SQLiteDB_IsTableExists_m4049229627(__this, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_004d;
		}
	}
	{
		OnError_t314785609 * L_5 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		OnError_t314785609 * L_6 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_6);
		OnError_Invoke_m3917243719(L_6, _stringLiteral167887363, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return (-1);
	}

IL_004d:
	{
		String_t* L_7 = (&___condition1)->get_fieldName_0();
		NullCheck(L_7);
		String_t* L_8 = String_Trim_m1030489823(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_10 = (&___condition1)->get_value_1();
		NullCheck(L_10);
		String_t* L_11 = String_Trim_m1030489823(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0094;
		}
	}

IL_0079:
	{
		OnError_t314785609 * L_13 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_13)
		{
			goto IL_0092;
		}
	}
	{
		OnError_t314785609 * L_14 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_14);
		OnError_Invoke_m3917243719(L_14, _stringLiteral2505246425, /*hidden argument*/NULL);
	}

IL_0092:
	{
		return (-1);
	}

IL_0094:
	{
		V_0 = (-1);
	}

IL_0096:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			StringU5BU5D_t2956870243* L_15 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)8));
			NullCheck(L_15);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
			ArrayElementTypeCheck (L_15, _stringLiteral2061912833);
			(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2061912833);
			StringU5BU5D_t2956870243* L_16 = L_15;
			String_t* L_17 = ___tableName0;
			NullCheck(L_16);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
			ArrayElementTypeCheck (L_16, L_17);
			(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_17);
			StringU5BU5D_t2956870243* L_18 = L_16;
			NullCheck(L_18);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 2);
			ArrayElementTypeCheck (L_18, _stringLiteral1841383481);
			(L_18)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1841383481);
			StringU5BU5D_t2956870243* L_19 = L_18;
			String_t* L_20 = (&___condition1)->get_fieldName_0();
			NullCheck(L_19);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
			ArrayElementTypeCheck (L_19, L_20);
			(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_20);
			StringU5BU5D_t2956870243* L_21 = L_19;
			int32_t L_22 = (&___condition1)->get_condition_2();
			String_t* L_23 = SQLiteDB_GetConditionSymbol_m1038213543(__this, L_22, /*hidden argument*/NULL);
			NullCheck(L_21);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 4);
			ArrayElementTypeCheck (L_21, L_23);
			(L_21)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_23);
			StringU5BU5D_t2956870243* L_24 = L_21;
			NullCheck(L_24);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 5);
			ArrayElementTypeCheck (L_24, _stringLiteral39);
			(L_24)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral39);
			StringU5BU5D_t2956870243* L_25 = L_24;
			String_t* L_26 = (&___condition1)->get_value_1();
			NullCheck(L_25);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 6);
			ArrayElementTypeCheck (L_25, L_26);
			(L_25)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)L_26);
			StringU5BU5D_t2956870243* L_27 = L_25;
			NullCheck(L_27);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 7);
			ArrayElementTypeCheck (L_27, _stringLiteral39);
			(L_27)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral39);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_28 = String_Concat_m21867311(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
			V_1 = L_28;
			SqliteConnection_t3853176977 * L_29 = __this->get_connection_4();
			NullCheck(L_29);
			VirtActionInvoker0::Invoke(26 /* System.Void System.Data.Common.DbConnection::Open() */, L_29);
			SqliteConnection_t3853176977 * L_30 = __this->get_connection_4();
			NullCheck(L_30);
			SqliteCommand_t4229878246 * L_31 = SqliteConnection_CreateCommand_m110869411(L_30, /*hidden argument*/NULL);
			__this->set_command_6(L_31);
			SqliteCommand_t4229878246 * L_32 = __this->get_command_6();
			String_t* L_33 = V_1;
			NullCheck(L_32);
			VirtActionInvoker1< String_t* >::Invoke(18 /* System.Void System.Data.Common.DbCommand::set_CommandText(System.String) */, L_32, L_33);
			SqliteCommand_t4229878246 * L_34 = __this->get_command_6();
			NullCheck(L_34);
			int32_t L_35 = VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 System.Data.Common.DbCommand::ExecuteNonQuery() */, L_34);
			V_0 = L_35;
			IL2CPP_LEAVE(0x164, FINALLY_014d);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0123;
			throw e;
		}

CATCH_0123:
		{ // begin catch(System.Exception)
			{
				V_2 = ((Exception_t1967233988 *)__exception_local);
				OnError_t314785609 * L_36 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				if (!L_36)
				{
					goto IL_0148;
				}
			}

IL_012e:
			{
				OnError_t314785609 * L_37 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				Exception_t1967233988 * L_38 = V_2;
				NullCheck(L_38);
				String_t* L_39 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_38);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_40 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2029867321, L_39, /*hidden argument*/NULL);
				NullCheck(L_37);
				OnError_Invoke_m3917243719(L_37, L_40, /*hidden argument*/NULL);
			}

IL_0148:
			{
				IL2CPP_LEAVE(0x164, FINALLY_014d);
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_014d;
	}

FINALLY_014d:
	{ // begin finally (depth: 1)
		SqliteCommand_t4229878246 * L_41 = __this->get_command_6();
		NullCheck(L_41);
		VirtActionInvoker0::Invoke(6 /* System.Void System.ComponentModel.Component::Dispose() */, L_41);
		SqliteConnection_t3853176977 * L_42 = __this->get_connection_4();
		NullCheck(L_42);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Data.Common.DbConnection::Close() */, L_42);
		IL2CPP_END_FINALLY(333)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(333)
	{
		IL2CPP_JUMP_TBL(0x164, IL_0164)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0164:
	{
		int32_t L_43 = V_0;
		return L_43;
	}
}
// System.String SQLiteDatabase.SQLiteDB::GetConditionSymbol(SQLiteDatabase.SQLiteDB/DB_Condition)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral60;
extern Il2CppCodeGenString* _stringLiteral62;
extern Il2CppCodeGenString* _stringLiteral61;
extern const uint32_t SQLiteDB_GetConditionSymbol_m1038213543_MetadataUsageId;
extern "C"  String_t* SQLiteDB_GetConditionSymbol_m1038213543 (SQLiteDB_t557922535 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_GetConditionSymbol_m1038213543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		int32_t L_1 = ___id0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = ___id0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = ___id0;
		if ((((int32_t)L_3) == ((int32_t)3)))
		{
			goto IL_0036;
		}
	}
	{
		goto IL_0041;
	}

IL_0020:
	{
		V_0 = _stringLiteral60;
		goto IL_0041;
	}

IL_002b:
	{
		V_0 = _stringLiteral62;
		goto IL_0041;
	}

IL_0036:
	{
		V_0 = _stringLiteral61;
		goto IL_0041;
	}

IL_0041:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.String SQLiteDatabase.SQLiteDB::GetDataType(SQLiteDatabase.SQLiteDB/DB_DataType)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral72655;
extern Il2CppCodeGenString* _stringLiteral2456310801;
extern Il2CppCodeGenString* _stringLiteral2571565;
extern Il2CppCodeGenString* _stringLiteral954596061;
extern const uint32_t SQLiteDB_GetDataType_m3557671473_MetadataUsageId;
extern "C"  String_t* SQLiteDB_GetDataType_m3557671473 (SQLiteDB_t557922535 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_GetDataType_m3557671473_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		int32_t L_1 = ___id0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0023;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_002e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_0039;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_0044;
		}
	}
	{
		goto IL_004f;
	}

IL_0023:
	{
		V_0 = _stringLiteral72655;
		goto IL_004f;
	}

IL_002e:
	{
		V_0 = _stringLiteral2456310801;
		goto IL_004f;
	}

IL_0039:
	{
		V_0 = _stringLiteral2571565;
		goto IL_004f;
	}

IL_0044:
	{
		V_0 = _stringLiteral954596061;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// SQLiteDatabase.DBReader SQLiteDatabase.SQLiteDB::Select(System.String)
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppClass* DBReader_t4220400158_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3679522742;
extern Il2CppCodeGenString* _stringLiteral2715305214;
extern const uint32_t SQLiteDB_Select_m2209825900_MetadataUsageId;
extern "C"  DBReader_t4220400158 * SQLiteDB_Select_m2209825900 (SQLiteDB_t557922535 * __this, String_t* ___query0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_Select_m2209825900_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DBReader_t4220400158 * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = SQLiteDB_get_Exists_m3278641920(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		OnError_t314785609 * L_1 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		OnError_t314785609 * L_2 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_2);
		OnError_Invoke_m3917243719(L_2, _stringLiteral3679522742, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return (DBReader_t4220400158 *)NULL;
	}

IL_0026:
	{
		V_0 = (DBReader_t4220400158 *)NULL;
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			SqliteConnection_t3853176977 * L_3 = __this->get_connection_4();
			NullCheck(L_3);
			VirtActionInvoker0::Invoke(26 /* System.Void System.Data.Common.DbConnection::Open() */, L_3);
			SqliteConnection_t3853176977 * L_4 = __this->get_connection_4();
			NullCheck(L_4);
			SqliteCommand_t4229878246 * L_5 = SqliteConnection_CreateCommand_m110869411(L_4, /*hidden argument*/NULL);
			__this->set_command_6(L_5);
			SqliteCommand_t4229878246 * L_6 = __this->get_command_6();
			String_t* L_7 = ___query0;
			NullCheck(L_6);
			VirtActionInvoker1< String_t* >::Invoke(18 /* System.Void System.Data.Common.DbCommand::set_CommandText(System.String) */, L_6, L_7);
			SqliteCommand_t4229878246 * L_8 = __this->get_command_6();
			NullCheck(L_8);
			SqliteDataReader_t1567858368 * L_9 = SqliteCommand_ExecuteReader_m585431169(L_8, /*hidden argument*/NULL);
			__this->set_reader_5(L_9);
			SqliteDataReader_t1567858368 * L_10 = __this->get_reader_5();
			DBReader_t4220400158 * L_11 = (DBReader_t4220400158 *)il2cpp_codegen_object_new(DBReader_t4220400158_il2cpp_TypeInfo_var);
			DBReader__ctor_m102303496(L_11, L_10, /*hidden argument*/NULL);
			V_0 = L_11;
			IL2CPP_LEAVE(0xB3, FINALLY_009c);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0072;
			throw e;
		}

CATCH_0072:
		{ // begin catch(System.Exception)
			{
				V_1 = ((Exception_t1967233988 *)__exception_local);
				OnError_t314785609 * L_12 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				if (!L_12)
				{
					goto IL_0097;
				}
			}

IL_007d:
			{
				OnError_t314785609 * L_13 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
				Exception_t1967233988 * L_14 = V_1;
				NullCheck(L_14);
				String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_14);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_16 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2715305214, L_15, /*hidden argument*/NULL);
				NullCheck(L_13);
				OnError_Invoke_m3917243719(L_13, L_16, /*hidden argument*/NULL);
			}

IL_0097:
			{
				IL2CPP_LEAVE(0xB3, FINALLY_009c);
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_009c;
	}

FINALLY_009c:
	{ // begin finally (depth: 1)
		SqliteCommand_t4229878246 * L_17 = __this->get_command_6();
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(6 /* System.Void System.ComponentModel.Component::Dispose() */, L_17);
		SqliteConnection_t3853176977 * L_18 = __this->get_connection_4();
		NullCheck(L_18);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Data.Common.DbConnection::Close() */, L_18);
		IL2CPP_END_FINALLY(156)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(156)
	{
		IL2CPP_JUMP_TBL(0xB3, IL_00b3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b3:
	{
		DBReader_t4220400158 * L_19 = V_0;
		return L_19;
	}
}
// SQLiteDatabase.DBReader SQLiteDatabase.SQLiteDB::GetAllData(System.String)
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral167887363;
extern Il2CppCodeGenString* _stringLiteral457293596;
extern const uint32_t SQLiteDB_GetAllData_m261559315_MetadataUsageId;
extern "C"  DBReader_t4220400158 * SQLiteDB_GetAllData_m261559315 (SQLiteDB_t557922535 * __this, String_t* ___tableName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_GetAllData_m261559315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___tableName0;
		bool L_1 = SQLiteDB_IsTableExists_m4049229627(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		OnError_t314785609 * L_2 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		OnError_t314785609 * L_3 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		NullCheck(L_3);
		OnError_Invoke_m3917243719(L_3, _stringLiteral167887363, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return (DBReader_t4220400158 *)NULL;
	}

IL_0027:
	{
		String_t* L_4 = ___tableName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral457293596, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = V_0;
		DBReader_t4220400158 * L_7 = SQLiteDB_Select_m2209825900(__this, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Boolean SQLiteDatabase.SQLiteDB::IsTableExists(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral415794840;
extern Il2CppCodeGenString* _stringLiteral39;
extern const uint32_t SQLiteDB_IsTableExists_m4049229627_MetadataUsageId;
extern "C"  bool SQLiteDB_IsTableExists_m4049229627 (SQLiteDB_t557922535 * __this, String_t* ___tableName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_IsTableExists_m4049229627_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)0;
		String_t* L_0 = ___tableName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral415794840, L_0, _stringLiteral39, /*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			SqliteConnection_t3853176977 * L_2 = __this->get_connection_4();
			NullCheck(L_2);
			VirtActionInvoker0::Invoke(26 /* System.Void System.Data.Common.DbConnection::Open() */, L_2);
			SqliteConnection_t3853176977 * L_3 = __this->get_connection_4();
			NullCheck(L_3);
			SqliteCommand_t4229878246 * L_4 = SqliteConnection_CreateCommand_m110869411(L_3, /*hidden argument*/NULL);
			__this->set_command_6(L_4);
			SqliteCommand_t4229878246 * L_5 = __this->get_command_6();
			String_t* L_6 = V_1;
			NullCheck(L_5);
			VirtActionInvoker1< String_t* >::Invoke(18 /* System.Void System.Data.Common.DbCommand::set_CommandText(System.String) */, L_5, L_6);
			SqliteCommand_t4229878246 * L_7 = __this->get_command_6();
			NullCheck(L_7);
			SqliteDataReader_t1567858368 * L_8 = SqliteCommand_ExecuteReader_m585431169(L_7, /*hidden argument*/NULL);
			__this->set_reader_5(L_8);
			SqliteDataReader_t1567858368 * L_9 = __this->get_reader_5();
			NullCheck(L_9);
			bool L_10 = VirtFuncInvoker0< bool >::Invoke(23 /* System.Boolean System.Data.Common.DbDataReader::get_HasRows() */, L_9);
			V_0 = L_10;
			SqliteDataReader_t1567858368 * L_11 = __this->get_reader_5();
			NullCheck(L_11);
			VirtActionInvoker0::Invoke(28 /* System.Void System.Data.Common.DbDataReader::Close() */, L_11);
			IL2CPP_LEAVE(0x87, FINALLY_0070);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0068;
			throw e;
		}

CATCH_0068:
		{ // begin catch(System.Object)
			V_0 = (bool)0;
			IL2CPP_LEAVE(0x87, FINALLY_0070);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0070;
	}

FINALLY_0070:
	{ // begin finally (depth: 1)
		SqliteCommand_t4229878246 * L_12 = __this->get_command_6();
		NullCheck(L_12);
		VirtActionInvoker0::Invoke(6 /* System.Void System.ComponentModel.Component::Dispose() */, L_12);
		SqliteConnection_t3853176977 * L_13 = __this->get_connection_4();
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(20 /* System.Void System.Data.Common.DbConnection::Close() */, L_13);
		IL2CPP_END_FINALLY(112)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(112)
	{
		IL2CPP_JUMP_TBL(0x87, IL_0087)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0087:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
// System.Void SQLiteDatabase.SQLiteDB::Dispose()
extern Il2CppClass* SQLiteDB_t557922535_il2cpp_TypeInfo_var;
extern const uint32_t SQLiteDB_Dispose_m2829228156_MetadataUsageId;
extern "C"  void SQLiteDB_Dispose_m2829228156 (SQLiteDB_t557922535 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteDB_Dispose_m2829228156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SqliteConnection_t3853176977 * L_0 = __this->get_connection_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		SqliteConnection_t3853176977 * L_1 = __this->get_connection_4();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(6 /* System.Void System.ComponentModel.Component::Dispose() */, L_1);
	}

IL_0016:
	{
		SqliteCommand_t4229878246 * L_2 = __this->get_command_6();
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		SqliteCommand_t4229878246 * L_3 = __this->get_command_6();
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(6 /* System.Void System.ComponentModel.Component::Dispose() */, L_3);
	}

IL_002c:
	{
		__this->set_connection_4((SqliteConnection_t3853176977 *)NULL);
		__this->set_reader_5((SqliteDataReader_t1567858368 *)NULL);
		__this->set_command_6((SqliteCommand_t4229878246 *)NULL);
		((SQLiteDB_t557922535_StaticFields*)SQLiteDB_t557922535_il2cpp_TypeInfo_var->static_fields)->set__instance_0((SQLiteDB_t557922535 *)NULL);
		GC_Collect_m1459080321(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: SQLiteDatabase.SQLiteDB/DB_ConditionPair
extern "C" void DB_ConditionPair_t1745523828_marshal_pinvoke(const DB_ConditionPair_t1745523828& unmarshaled, DB_ConditionPair_t1745523828_marshaled_pinvoke& marshaled)
{
	marshaled.___fieldName_0 = il2cpp_codegen_marshal_string(unmarshaled.get_fieldName_0());
	marshaled.___value_1 = il2cpp_codegen_marshal_string(unmarshaled.get_value_1());
	marshaled.___condition_2 = unmarshaled.get_condition_2();
}
extern "C" void DB_ConditionPair_t1745523828_marshal_pinvoke_back(const DB_ConditionPair_t1745523828_marshaled_pinvoke& marshaled, DB_ConditionPair_t1745523828& unmarshaled)
{
	unmarshaled.set_fieldName_0(il2cpp_codegen_marshal_string_result(marshaled.___fieldName_0));
	unmarshaled.set_value_1(il2cpp_codegen_marshal_string_result(marshaled.___value_1));
	int32_t unmarshaled_condition_temp = 0;
	unmarshaled_condition_temp = marshaled.___condition_2;
	unmarshaled.set_condition_2(unmarshaled_condition_temp);
}
// Conversion method for clean up from marshalling of: SQLiteDatabase.SQLiteDB/DB_ConditionPair
extern "C" void DB_ConditionPair_t1745523828_marshal_pinvoke_cleanup(DB_ConditionPair_t1745523828_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___fieldName_0);
	marshaled.___fieldName_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___value_1);
	marshaled.___value_1 = NULL;
}
// Conversion methods for marshalling of: SQLiteDatabase.SQLiteDB/DB_ConditionPair
extern "C" void DB_ConditionPair_t1745523828_marshal_com(const DB_ConditionPair_t1745523828& unmarshaled, DB_ConditionPair_t1745523828_marshaled_com& marshaled)
{
	marshaled.___fieldName_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_fieldName_0());
	marshaled.___value_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_value_1());
	marshaled.___condition_2 = unmarshaled.get_condition_2();
}
extern "C" void DB_ConditionPair_t1745523828_marshal_com_back(const DB_ConditionPair_t1745523828_marshaled_com& marshaled, DB_ConditionPair_t1745523828& unmarshaled)
{
	unmarshaled.set_fieldName_0(il2cpp_codegen_marshal_bstring_result(marshaled.___fieldName_0));
	unmarshaled.set_value_1(il2cpp_codegen_marshal_bstring_result(marshaled.___value_1));
	int32_t unmarshaled_condition_temp = 0;
	unmarshaled_condition_temp = marshaled.___condition_2;
	unmarshaled.set_condition_2(unmarshaled_condition_temp);
}
// Conversion method for clean up from marshalling of: SQLiteDatabase.SQLiteDB/DB_ConditionPair
extern "C" void DB_ConditionPair_t1745523828_marshal_com_cleanup(DB_ConditionPair_t1745523828_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___fieldName_0);
	marshaled.___fieldName_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___value_1);
	marshaled.___value_1 = NULL;
}
// Conversion methods for marshalling of: SQLiteDatabase.SQLiteDB/DB_DataPair
extern "C" void DB_DataPair_t4220460133_marshal_pinvoke(const DB_DataPair_t4220460133& unmarshaled, DB_DataPair_t4220460133_marshaled_pinvoke& marshaled)
{
	marshaled.___fieldName_0 = il2cpp_codegen_marshal_string(unmarshaled.get_fieldName_0());
	marshaled.___value_1 = il2cpp_codegen_marshal_string(unmarshaled.get_value_1());
}
extern "C" void DB_DataPair_t4220460133_marshal_pinvoke_back(const DB_DataPair_t4220460133_marshaled_pinvoke& marshaled, DB_DataPair_t4220460133& unmarshaled)
{
	unmarshaled.set_fieldName_0(il2cpp_codegen_marshal_string_result(marshaled.___fieldName_0));
	unmarshaled.set_value_1(il2cpp_codegen_marshal_string_result(marshaled.___value_1));
}
// Conversion method for clean up from marshalling of: SQLiteDatabase.SQLiteDB/DB_DataPair
extern "C" void DB_DataPair_t4220460133_marshal_pinvoke_cleanup(DB_DataPair_t4220460133_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___fieldName_0);
	marshaled.___fieldName_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___value_1);
	marshaled.___value_1 = NULL;
}
// Conversion methods for marshalling of: SQLiteDatabase.SQLiteDB/DB_DataPair
extern "C" void DB_DataPair_t4220460133_marshal_com(const DB_DataPair_t4220460133& unmarshaled, DB_DataPair_t4220460133_marshaled_com& marshaled)
{
	marshaled.___fieldName_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_fieldName_0());
	marshaled.___value_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_value_1());
}
extern "C" void DB_DataPair_t4220460133_marshal_com_back(const DB_DataPair_t4220460133_marshaled_com& marshaled, DB_DataPair_t4220460133& unmarshaled)
{
	unmarshaled.set_fieldName_0(il2cpp_codegen_marshal_bstring_result(marshaled.___fieldName_0));
	unmarshaled.set_value_1(il2cpp_codegen_marshal_bstring_result(marshaled.___value_1));
}
// Conversion method for clean up from marshalling of: SQLiteDatabase.SQLiteDB/DB_DataPair
extern "C" void DB_DataPair_t4220460133_marshal_com_cleanup(DB_DataPair_t4220460133_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___fieldName_0);
	marshaled.___fieldName_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___value_1);
	marshaled.___value_1 = NULL;
}
// Conversion methods for marshalling of: SQLiteDatabase.SQLiteDB/DB_Field
extern "C" void DB_Field_t3780330969_marshal_pinvoke(const DB_Field_t3780330969& unmarshaled, DB_Field_t3780330969_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	marshaled.___type_1 = unmarshaled.get_type_1();
	marshaled.___size_2 = unmarshaled.get_size_2();
	marshaled.___isPrimaryKey_3 = unmarshaled.get_isPrimaryKey_3();
	marshaled.___isNotNull_4 = unmarshaled.get_isNotNull_4();
	marshaled.___isUnique_5 = unmarshaled.get_isUnique_5();
}
extern "C" void DB_Field_t3780330969_marshal_pinvoke_back(const DB_Field_t3780330969_marshaled_pinvoke& marshaled, DB_Field_t3780330969& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	int32_t unmarshaled_type_temp = 0;
	unmarshaled_type_temp = marshaled.___type_1;
	unmarshaled.set_type_1(unmarshaled_type_temp);
	int32_t unmarshaled_size_temp = 0;
	unmarshaled_size_temp = marshaled.___size_2;
	unmarshaled.set_size_2(unmarshaled_size_temp);
	bool unmarshaled_isPrimaryKey_temp = false;
	unmarshaled_isPrimaryKey_temp = marshaled.___isPrimaryKey_3;
	unmarshaled.set_isPrimaryKey_3(unmarshaled_isPrimaryKey_temp);
	bool unmarshaled_isNotNull_temp = false;
	unmarshaled_isNotNull_temp = marshaled.___isNotNull_4;
	unmarshaled.set_isNotNull_4(unmarshaled_isNotNull_temp);
	bool unmarshaled_isUnique_temp = false;
	unmarshaled_isUnique_temp = marshaled.___isUnique_5;
	unmarshaled.set_isUnique_5(unmarshaled_isUnique_temp);
}
// Conversion method for clean up from marshalling of: SQLiteDatabase.SQLiteDB/DB_Field
extern "C" void DB_Field_t3780330969_marshal_pinvoke_cleanup(DB_Field_t3780330969_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// Conversion methods for marshalling of: SQLiteDatabase.SQLiteDB/DB_Field
extern "C" void DB_Field_t3780330969_marshal_com(const DB_Field_t3780330969& unmarshaled, DB_Field_t3780330969_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	marshaled.___type_1 = unmarshaled.get_type_1();
	marshaled.___size_2 = unmarshaled.get_size_2();
	marshaled.___isPrimaryKey_3 = unmarshaled.get_isPrimaryKey_3();
	marshaled.___isNotNull_4 = unmarshaled.get_isNotNull_4();
	marshaled.___isUnique_5 = unmarshaled.get_isUnique_5();
}
extern "C" void DB_Field_t3780330969_marshal_com_back(const DB_Field_t3780330969_marshaled_com& marshaled, DB_Field_t3780330969& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	int32_t unmarshaled_type_temp = 0;
	unmarshaled_type_temp = marshaled.___type_1;
	unmarshaled.set_type_1(unmarshaled_type_temp);
	int32_t unmarshaled_size_temp = 0;
	unmarshaled_size_temp = marshaled.___size_2;
	unmarshaled.set_size_2(unmarshaled_size_temp);
	bool unmarshaled_isPrimaryKey_temp = false;
	unmarshaled_isPrimaryKey_temp = marshaled.___isPrimaryKey_3;
	unmarshaled.set_isPrimaryKey_3(unmarshaled_isPrimaryKey_temp);
	bool unmarshaled_isNotNull_temp = false;
	unmarshaled_isNotNull_temp = marshaled.___isNotNull_4;
	unmarshaled.set_isNotNull_4(unmarshaled_isNotNull_temp);
	bool unmarshaled_isUnique_temp = false;
	unmarshaled_isUnique_temp = marshaled.___isUnique_5;
	unmarshaled.set_isUnique_5(unmarshaled_isUnique_temp);
}
// Conversion method for clean up from marshalling of: SQLiteDatabase.SQLiteDB/DB_Field
extern "C" void DB_Field_t3780330969_marshal_com_cleanup(DB_Field_t3780330969_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// System.Void SQLiteDatabase.SQLiteEventListener::add_onError(SQLiteDatabase.SQLiteEventListener/OnError)
extern "C"  void SQLiteEventListener_add_onError_m1384741999 (Il2CppObject * __this /* static, unused */, OnError_t314785609 * ___value0, const MethodInfo* method)
{
	{
		OnError_t314785609 * L_0 = ___value0;
		SQLiteEventListener_AddHandler_onError_m1563276007(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SQLiteDatabase.SQLiteEventListener::remove_onError(SQLiteDatabase.SQLiteEventListener/OnError)
extern "C"  void SQLiteEventListener_remove_onError_m1527352418 (Il2CppObject * __this /* static, unused */, OnError_t314785609 * ___value0, const MethodInfo* method)
{
	{
		OnError_t314785609 * L_0 = ___value0;
		SQLiteEventListener_RemoveHandler_onError_m1626034004(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SQLiteDatabase.SQLiteEventListener::AddHandler_onError(SQLiteDatabase.SQLiteEventListener/OnError)
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppClass* OnError_t314785609_il2cpp_TypeInfo_var;
extern const uint32_t SQLiteEventListener_AddHandler_onError_m1563276007_MetadataUsageId;
extern "C"  void SQLiteEventListener_AddHandler_onError_m1563276007 (Il2CppObject * __this /* static, unused */, OnError_t314785609 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteEventListener_AddHandler_onError_m1563276007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnError_t314785609 * L_0 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		OnError_t314785609 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->set_onErrorInvoker_0(((OnError_t314785609 *)CastclassSealed(L_2, OnError_t314785609_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SQLiteDatabase.SQLiteEventListener::RemoveHandler_onError(SQLiteDatabase.SQLiteEventListener/OnError)
extern Il2CppClass* SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var;
extern Il2CppClass* OnError_t314785609_il2cpp_TypeInfo_var;
extern const uint32_t SQLiteEventListener_RemoveHandler_onError_m1626034004_MetadataUsageId;
extern "C"  void SQLiteEventListener_RemoveHandler_onError_m1626034004 (Il2CppObject * __this /* static, unused */, OnError_t314785609 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SQLiteEventListener_RemoveHandler_onError_m1626034004_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnError_t314785609 * L_0 = ((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->get_onErrorInvoker_0();
		OnError_t314785609 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((SQLiteEventListener_t1751327711_StaticFields*)SQLiteEventListener_t1751327711_il2cpp_TypeInfo_var->static_fields)->set_onErrorInvoker_0(((OnError_t314785609 *)CastclassSealed(L_2, OnError_t314785609_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void SQLiteDatabase.SQLiteEventListener/OnError::.ctor(System.Object,System.IntPtr)
extern "C"  void OnError__ctor_m1086636545 (OnError_t314785609 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void SQLiteDatabase.SQLiteEventListener/OnError::Invoke(System.String)
extern "C"  void OnError_Invoke_m3917243719 (OnError_t314785609 * __this, String_t* ___err0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnError_Invoke_m3917243719((OnError_t314785609 *)__this->get_prev_9(),___err0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___err0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___err0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___err0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___err0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___err0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnError_t314785609(Il2CppObject* delegate, String_t* ___err0)
{
	typedef void (STDCALL *native_function_ptr_type)(char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___err0' to native representation
	char* ____err0_marshaled = NULL;
	____err0_marshaled = il2cpp_codegen_marshal_string(___err0);

	// Native function invocation
	_il2cpp_pinvoke_func(____err0_marshaled);

	// Marshaling cleanup of parameter '___err0' native representation
	il2cpp_codegen_marshal_free(____err0_marshaled);
	____err0_marshaled = NULL;

}
// System.IAsyncResult SQLiteDatabase.SQLiteEventListener/OnError::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnError_BeginInvoke_m1468012748 (OnError_t314785609 * __this, String_t* ___err0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___err0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void SQLiteDatabase.SQLiteEventListener/OnError::EndInvoke(System.IAsyncResult)
extern "C"  void OnError_EndInvoke_m2953010321 (OnError_t314785609 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
