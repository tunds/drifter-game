﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>
struct ListKeys_t780934445;
// System.Collections.Generic.SortedList`2<System.Object,System.Object>
struct SortedList_2_t2672481741;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Array
struct Il2CppArray;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Array2840145358.h"

// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C"  void ListKeys__ctor_m1415898181_gshared (ListKeys_t780934445 * __this, SortedList_2_t2672481741 * ___host0, const MethodInfo* method);
#define ListKeys__ctor_m1415898181(__this, ___host0, method) ((  void (*) (ListKeys_t780934445 *, SortedList_2_t2672481741 *, const MethodInfo*))ListKeys__ctor_m1415898181_gshared)(__this, ___host0, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ListKeys_System_Collections_IEnumerable_GetEnumerator_m2517214106_gshared (ListKeys_t780934445 * __this, const MethodInfo* method);
#define ListKeys_System_Collections_IEnumerable_GetEnumerator_m2517214106(__this, method) ((  Il2CppObject * (*) (ListKeys_t780934445 *, const MethodInfo*))ListKeys_System_Collections_IEnumerable_GetEnumerator_m2517214106_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::Add(TKey)
extern "C"  void ListKeys_Add_m4109433337_gshared (ListKeys_t780934445 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListKeys_Add_m4109433337(__this, ___item0, method) ((  void (*) (ListKeys_t780934445 *, Il2CppObject *, const MethodInfo*))ListKeys_Add_m4109433337_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::Remove(TKey)
extern "C"  bool ListKeys_Remove_m713037762_gshared (ListKeys_t780934445 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ListKeys_Remove_m713037762(__this, ___key0, method) ((  bool (*) (ListKeys_t780934445 *, Il2CppObject *, const MethodInfo*))ListKeys_Remove_m713037762_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::Clear()
extern "C"  void ListKeys_Clear_m4075768240_gshared (ListKeys_t780934445 * __this, const MethodInfo* method);
#define ListKeys_Clear_m4075768240(__this, method) ((  void (*) (ListKeys_t780934445 *, const MethodInfo*))ListKeys_Clear_m4075768240_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void ListKeys_CopyTo_m807474325_gshared (ListKeys_t780934445 * __this, ObjectU5BU5D_t11523773* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ListKeys_CopyTo_m807474325(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ListKeys_t780934445 *, ObjectU5BU5D_t11523773*, int32_t, const MethodInfo*))ListKeys_CopyTo_m807474325_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::Contains(TKey)
extern "C"  bool ListKeys_Contains_m1776517917_gshared (ListKeys_t780934445 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListKeys_Contains_m1776517917(__this, ___item0, method) ((  bool (*) (ListKeys_t780934445 *, Il2CppObject *, const MethodInfo*))ListKeys_Contains_m1776517917_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::IndexOf(TKey)
extern "C"  int32_t ListKeys_IndexOf_m3432855761_gshared (ListKeys_t780934445 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ListKeys_IndexOf_m3432855761(__this, ___item0, method) ((  int32_t (*) (ListKeys_t780934445 *, Il2CppObject *, const MethodInfo*))ListKeys_IndexOf_m3432855761_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::Insert(System.Int32,TKey)
extern "C"  void ListKeys_Insert_m3333998130_gshared (ListKeys_t780934445 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define ListKeys_Insert_m3333998130(__this, ___index0, ___item1, method) ((  void (*) (ListKeys_t780934445 *, int32_t, Il2CppObject *, const MethodInfo*))ListKeys_Insert_m3333998130_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::RemoveAt(System.Int32)
extern "C"  void ListKeys_RemoveAt_m1607534215_gshared (ListKeys_t780934445 * __this, int32_t ___index0, const MethodInfo* method);
#define ListKeys_RemoveAt_m1607534215(__this, ___index0, method) ((  void (*) (ListKeys_t780934445 *, int32_t, const MethodInfo*))ListKeys_RemoveAt_m1607534215_gshared)(__this, ___index0, method)
// TKey System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ListKeys_get_Item_m2612728440_gshared (ListKeys_t780934445 * __this, int32_t ___index0, const MethodInfo* method);
#define ListKeys_get_Item_m2612728440(__this, ___index0, method) ((  Il2CppObject * (*) (ListKeys_t780934445 *, int32_t, const MethodInfo*))ListKeys_get_Item_m2612728440_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::set_Item(System.Int32,TKey)
extern "C"  void ListKeys_set_Item_m2416105019_gshared (ListKeys_t780934445 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ListKeys_set_Item_m2416105019(__this, ___index0, ___value1, method) ((  void (*) (ListKeys_t780934445 *, int32_t, Il2CppObject *, const MethodInfo*))ListKeys_set_Item_m2416105019_gshared)(__this, ___index0, ___value1, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ListKeys_GetEnumerator_m3660290966_gshared (ListKeys_t780934445 * __this, const MethodInfo* method);
#define ListKeys_GetEnumerator_m3660290966(__this, method) ((  Il2CppObject* (*) (ListKeys_t780934445 *, const MethodInfo*))ListKeys_GetEnumerator_m3660290966_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::get_Count()
extern "C"  int32_t ListKeys_get_Count_m2270037817_gshared (ListKeys_t780934445 * __this, const MethodInfo* method);
#define ListKeys_get_Count_m2270037817(__this, method) ((  int32_t (*) (ListKeys_t780934445 *, const MethodInfo*))ListKeys_get_Count_m2270037817_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::get_IsSynchronized()
extern "C"  bool ListKeys_get_IsSynchronized_m2689495024_gshared (ListKeys_t780934445 * __this, const MethodInfo* method);
#define ListKeys_get_IsSynchronized_m2689495024(__this, method) ((  bool (*) (ListKeys_t780934445 *, const MethodInfo*))ListKeys_get_IsSynchronized_m2689495024_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::get_IsReadOnly()
extern "C"  bool ListKeys_get_IsReadOnly_m717793086_gshared (ListKeys_t780934445 * __this, const MethodInfo* method);
#define ListKeys_get_IsReadOnly_m717793086(__this, method) ((  bool (*) (ListKeys_t780934445 *, const MethodInfo*))ListKeys_get_IsReadOnly_m717793086_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::get_SyncRoot()
extern "C"  Il2CppObject * ListKeys_get_SyncRoot_m916185008_gshared (ListKeys_t780934445 * __this, const MethodInfo* method);
#define ListKeys_get_SyncRoot_m916185008(__this, method) ((  Il2CppObject * (*) (ListKeys_t780934445 *, const MethodInfo*))ListKeys_get_SyncRoot_m916185008_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Object,System.Object>::CopyTo(System.Array,System.Int32)
extern "C"  void ListKeys_CopyTo_m2477648418_gshared (ListKeys_t780934445 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ListKeys_CopyTo_m2477648418(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ListKeys_t780934445 *, Il2CppArray *, int32_t, const MethodInfo*))ListKeys_CopyTo_m2477648418_gshared)(__this, ___array0, ___arrayIndex1, method)
