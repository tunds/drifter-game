﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Predicate`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct Predicate_1_t56327571;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Field3780330969.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Predicate`1<SQLiteDatabase.SQLiteDB/DB_Field>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m256243108_gshared (Predicate_1_t56327571 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Predicate_1__ctor_m256243108(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t56327571 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m256243108_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<SQLiteDatabase.SQLiteDB/DB_Field>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3033941726_gshared (Predicate_1_t56327571 * __this, DB_Field_t3780330969  ___obj0, const MethodInfo* method);
#define Predicate_1_Invoke_m3033941726(__this, ___obj0, method) ((  bool (*) (Predicate_1_t56327571 *, DB_Field_t3780330969 , const MethodInfo*))Predicate_1_Invoke_m3033941726_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<SQLiteDatabase.SQLiteDB/DB_Field>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3518409197_gshared (Predicate_1_t56327571 * __this, DB_Field_t3780330969  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m3518409197(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t56327571 *, DB_Field_t3780330969 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m3518409197_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<SQLiteDatabase.SQLiteDB/DB_Field>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1153274806_gshared (Predicate_1_t56327571 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Predicate_1_EndInvoke_m1153274806(__this, ___result0, method) ((  bool (*) (Predicate_1_t56327571 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m1153274806_gshared)(__this, ___result0, method)
