﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Transactions_System_Transactions_Transactio3644312060.h"
#include "System_Transactions_System_Transactions_IsolationL2549522802.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Transactions.TransactionOptions::.ctor(System.Transactions.IsolationLevel,System.TimeSpan)
extern "C"  void TransactionOptions__ctor_m379171565 (TransactionOptions_t3644312060 * __this, int32_t ___level0, TimeSpan_t763862892  ___timeout1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.TransactionOptions::Equals(System.Object)
extern "C"  bool TransactionOptions_Equals_m1860415864 (TransactionOptions_t3644312060 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Transactions.TransactionOptions::GetHashCode()
extern "C"  int32_t TransactionOptions_GetHashCode_m578159260 (TransactionOptions_t3644312060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.TransactionOptions::op_Equality(System.Transactions.TransactionOptions,System.Transactions.TransactionOptions)
extern "C"  bool TransactionOptions_op_Equality_m4085403305 (Il2CppObject * __this /* static, unused */, TransactionOptions_t3644312060  ___o10, TransactionOptions_t3644312060  ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct TransactionOptions_t3644312060;
struct TransactionOptions_t3644312060_marshaled_pinvoke;

extern "C" void TransactionOptions_t3644312060_marshal_pinvoke(const TransactionOptions_t3644312060& unmarshaled, TransactionOptions_t3644312060_marshaled_pinvoke& marshaled);
extern "C" void TransactionOptions_t3644312060_marshal_pinvoke_back(const TransactionOptions_t3644312060_marshaled_pinvoke& marshaled, TransactionOptions_t3644312060& unmarshaled);
extern "C" void TransactionOptions_t3644312060_marshal_pinvoke_cleanup(TransactionOptions_t3644312060_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TransactionOptions_t3644312060;
struct TransactionOptions_t3644312060_marshaled_com;

extern "C" void TransactionOptions_t3644312060_marshal_com(const TransactionOptions_t3644312060& unmarshaled, TransactionOptions_t3644312060_marshaled_com& marshaled);
extern "C" void TransactionOptions_t3644312060_marshal_com_back(const TransactionOptions_t3644312060_marshaled_com& marshaled, TransactionOptions_t3644312060& unmarshaled);
extern "C" void TransactionOptions_t3644312060_marshal_com_cleanup(TransactionOptions_t3644312060_marshaled_com& marshaled);
