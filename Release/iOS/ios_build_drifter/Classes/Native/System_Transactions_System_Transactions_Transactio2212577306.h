﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Transactions_System_Transactions_Transactio3768828717.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Transactions.TransactionAbortedException
struct  TransactionAbortedException_t2212577306  : public TransactionException_t3768828717
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
