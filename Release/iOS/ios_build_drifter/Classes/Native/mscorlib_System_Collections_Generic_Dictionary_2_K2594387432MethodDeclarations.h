﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>
struct KeyCollection_t2594387432;
// System.Collections.Generic.Dictionary`2<System.Int64,System.Object>
struct Dictionary_2_t271112152;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t35554034;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Int64[]
struct Int64U5BU5D_t753178071;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key38140093.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m876018024_gshared (KeyCollection_t2594387432 * __this, Dictionary_2_t271112152 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m876018024(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2594387432 *, Dictionary_2_t271112152 *, const MethodInfo*))KeyCollection__ctor_m876018024_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1950490350_gshared (KeyCollection_t2594387432 * __this, int64_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1950490350(__this, ___item0, method) ((  void (*) (KeyCollection_t2594387432 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1950490350_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m650061797_gshared (KeyCollection_t2594387432 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m650061797(__this, method) ((  void (*) (KeyCollection_t2594387432 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m650061797_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3575032736_gshared (KeyCollection_t2594387432 * __this, int64_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3575032736(__this, ___item0, method) ((  bool (*) (KeyCollection_t2594387432 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3575032736_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m643400965_gshared (KeyCollection_t2594387432 * __this, int64_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m643400965(__this, ___item0, method) ((  bool (*) (KeyCollection_t2594387432 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m643400965_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3408536631_gshared (KeyCollection_t2594387432 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3408536631(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2594387432 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3408536631_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m299064535_gshared (KeyCollection_t2594387432 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m299064535(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2594387432 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m299064535_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2072724262_gshared (KeyCollection_t2594387432 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2072724262(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2594387432 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2072724262_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2513681153_gshared (KeyCollection_t2594387432 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2513681153(__this, method) ((  bool (*) (KeyCollection_t2594387432 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2513681153_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1569874163_gshared (KeyCollection_t2594387432 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1569874163(__this, method) ((  bool (*) (KeyCollection_t2594387432 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1569874163_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m497659493_gshared (KeyCollection_t2594387432 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m497659493(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2594387432 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m497659493_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3810787357_gshared (KeyCollection_t2594387432 * __this, Int64U5BU5D_t753178071* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3810787357(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2594387432 *, Int64U5BU5D_t753178071*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3810787357_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::GetEnumerator()
extern "C"  Enumerator_t38140093  KeyCollection_GetEnumerator_m1000129002_gshared (KeyCollection_t2594387432 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1000129002(__this, method) ((  Enumerator_t38140093  (*) (KeyCollection_t2594387432 *, const MethodInfo*))KeyCollection_GetEnumerator_m1000129002_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m556640685_gshared (KeyCollection_t2594387432 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m556640685(__this, method) ((  int32_t (*) (KeyCollection_t2594387432 *, const MethodInfo*))KeyCollection_get_Count_m556640685_gshared)(__this, method)
