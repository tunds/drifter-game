﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AlphaButtonClickMask
struct AlphaButtonClickMask_t1323679844;
// UnityEngine.Camera
struct Camera_t3533968274;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Camera3533968274.h"

// System.Void AlphaButtonClickMask::.ctor()
extern "C"  void AlphaButtonClickMask__ctor_m3827939179 (AlphaButtonClickMask_t1323679844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlphaButtonClickMask::Start()
extern "C"  void AlphaButtonClickMask_Start_m2775076971 (AlphaButtonClickMask_t1323679844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AlphaButtonClickMask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  bool AlphaButtonClickMask_IsRaycastLocationValid_m3312612013 (AlphaButtonClickMask_t1323679844 * __this, Vector2_t3525329788  ___sp0, Camera_t3533968274 * ___eventCamera1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
