﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLiter.LoomManager/NullLoom
struct NullLoom_t2453031144;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void SQLiter.LoomManager/NullLoom::.ctor()
extern "C"  void NullLoom__ctor_m2268010396 (NullLoom_t2453031144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.LoomManager/NullLoom::QueueOnMainThread(System.Action)
extern "C"  void NullLoom_QueueOnMainThread_m123326480 (NullLoom_t2453031144 * __this, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
