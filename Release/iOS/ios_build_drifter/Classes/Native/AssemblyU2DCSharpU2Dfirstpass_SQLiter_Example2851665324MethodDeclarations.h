﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLiter.Example
struct Example_t2851665324;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SQLiter.Example::.ctor()
extern "C"  void Example__ctor_m128259925 (Example_t2851665324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::.cctor()
extern "C"  void Example__cctor_m3493961464 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::Awake()
extern "C"  void Example_Awake_m365865144 (Example_t2851665324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::Start()
extern "C"  void Example_Start_m3370365013 (Example_t2851665324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::Test()
extern "C"  void Example_Test_m1509506913 (Example_t2851665324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::OnDestroy()
extern "C"  void Example_OnDestroy_m2244032974 (Example_t2851665324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::RunAsyncInit()
extern "C"  void Example_RunAsyncInit_m740795504 (Example_t2851665324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::SQLiteInit()
extern "C"  void Example_SQLiteInit_m1861901579 (Example_t2851665324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::InsertWord(System.String,System.String)
extern "C"  void Example_InsertWord_m766538508 (Example_t2851665324 * __this, String_t* ___name0, String_t* ___definition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::GetAllWords()
extern "C"  void Example_GetAllWords_m270115441 (Example_t2851665324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLiter.Example::GetDefinitation(System.String)
extern "C"  String_t* Example_GetDefinitation_m1353344950 (Example_t2851665324 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLiter.Example::QueryString(System.String,System.String)
extern "C"  String_t* Example_QueryString_m2079837903 (Example_t2851665324 * __this, String_t* ___column0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::SetValue(System.String,System.Int32,System.String)
extern "C"  void Example_SetValue_m1418526031 (Example_t2851665324 * __this, String_t* ___column0, int32_t ___value1, String_t* ___wordKey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::DeleteWord(System.String)
extern "C"  void Example_DeleteWord_m848501406 (Example_t2851665324 * __this, String_t* ___wordKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::ExecuteNonQuery(System.String)
extern "C"  void Example_ExecuteNonQuery_m3230977375 (Example_t2851665324 * __this, String_t* ___commandText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::SQLiteClose()
extern "C"  void Example_SQLiteClose_m802630975 (Example_t2851665324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::<Test>m__0()
extern "C"  void Example_U3CTestU3Em__0_m1343061954 (Example_t2851665324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.Example::<RunAsyncInit>m__1()
extern "C"  void Example_U3CRunAsyncInitU3Em__1_m3194064852 (Example_t2851665324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
