﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteEnlistment
struct SQLiteEnlistment_t56717400;
// Mono.Data.Sqlite.SqliteConnection
struct SqliteConnection_t3853176977;
// System.Transactions.Transaction
struct Transaction_t3175846586;
// System.Transactions.Enlistment
struct Enlistment_t3082063553;
// System.Transactions.PreparingEnlistment
struct PreparingEnlistment_t3015517605;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection3853176977.h"
#include "System_Transactions_System_Transactions_Transactio3175846586.h"
#include "System_Transactions_System_Transactions_Enlistment3082063553.h"
#include "System_Transactions_System_Transactions_PreparingE3015517605.h"

// System.Void Mono.Data.Sqlite.SQLiteEnlistment::.ctor(Mono.Data.Sqlite.SqliteConnection,System.Transactions.Transaction)
extern "C"  void SQLiteEnlistment__ctor_m1757632461 (SQLiteEnlistment_t56717400 * __this, SqliteConnection_t3853176977 * ___cnn0, Transaction_t3175846586 * ___scope1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteEnlistment::Cleanup(Mono.Data.Sqlite.SqliteConnection)
extern "C"  void SQLiteEnlistment_Cleanup_m4242007267 (SQLiteEnlistment_t56717400 * __this, SqliteConnection_t3853176977 * ___cnn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteEnlistment::Commit(System.Transactions.Enlistment)
extern "C"  void SQLiteEnlistment_Commit_m3290083906 (SQLiteEnlistment_t56717400 * __this, Enlistment_t3082063553 * ___enlistment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteEnlistment::Prepare(System.Transactions.PreparingEnlistment)
extern "C"  void SQLiteEnlistment_Prepare_m3987375812 (SQLiteEnlistment_t56717400 * __this, PreparingEnlistment_t3015517605 * ___preparingEnlistment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteEnlistment::Rollback(System.Transactions.Enlistment)
extern "C"  void SQLiteEnlistment_Rollback_m2740002223 (SQLiteEnlistment_t56717400 * __this, Enlistment_t3082063553 * ___enlistment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
