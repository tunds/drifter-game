﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "System_Data_System_Data_Common_DbParameter3306161371.h"
#include "System_Data_System_Data_DbType2586775211.h"
#include "System_Data_System_Data_ParameterDirection608485289.h"
#include "System_Data_System_Data_DataRowVersion2975473339.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqliteClient.SqliteParameter
struct  SqliteParameter_t2628453389  : public DbParameter_t3306161371
{
public:
	// System.String Mono.Data.SqliteClient.SqliteParameter::name
	String_t* ___name_2;
	// System.Data.DbType Mono.Data.SqliteClient.SqliteParameter::type
	int32_t ___type_3;
	// System.Data.DbType Mono.Data.SqliteClient.SqliteParameter::originalType
	int32_t ___originalType_4;
	// System.Boolean Mono.Data.SqliteClient.SqliteParameter::typeSet
	bool ___typeSet_5;
	// System.String Mono.Data.SqliteClient.SqliteParameter::source_column
	String_t* ___source_column_6;
	// System.Data.ParameterDirection Mono.Data.SqliteClient.SqliteParameter::direction
	int32_t ___direction_7;
	// System.Data.DataRowVersion Mono.Data.SqliteClient.SqliteParameter::row_version
	int32_t ___row_version_8;
	// System.Object Mono.Data.SqliteClient.SqliteParameter::param_value
	Il2CppObject * ___param_value_9;
	// System.Int32 Mono.Data.SqliteClient.SqliteParameter::size
	int32_t ___size_10;
	// System.Boolean Mono.Data.SqliteClient.SqliteParameter::isNullable
	bool ___isNullable_11;
	// System.Boolean Mono.Data.SqliteClient.SqliteParameter::sourceColumnNullMapping
	bool ___sourceColumnNullMapping_12;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(SqliteParameter_t2628453389, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(SqliteParameter_t2628453389, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_originalType_4() { return static_cast<int32_t>(offsetof(SqliteParameter_t2628453389, ___originalType_4)); }
	inline int32_t get_originalType_4() const { return ___originalType_4; }
	inline int32_t* get_address_of_originalType_4() { return &___originalType_4; }
	inline void set_originalType_4(int32_t value)
	{
		___originalType_4 = value;
	}

	inline static int32_t get_offset_of_typeSet_5() { return static_cast<int32_t>(offsetof(SqliteParameter_t2628453389, ___typeSet_5)); }
	inline bool get_typeSet_5() const { return ___typeSet_5; }
	inline bool* get_address_of_typeSet_5() { return &___typeSet_5; }
	inline void set_typeSet_5(bool value)
	{
		___typeSet_5 = value;
	}

	inline static int32_t get_offset_of_source_column_6() { return static_cast<int32_t>(offsetof(SqliteParameter_t2628453389, ___source_column_6)); }
	inline String_t* get_source_column_6() const { return ___source_column_6; }
	inline String_t** get_address_of_source_column_6() { return &___source_column_6; }
	inline void set_source_column_6(String_t* value)
	{
		___source_column_6 = value;
		Il2CppCodeGenWriteBarrier(&___source_column_6, value);
	}

	inline static int32_t get_offset_of_direction_7() { return static_cast<int32_t>(offsetof(SqliteParameter_t2628453389, ___direction_7)); }
	inline int32_t get_direction_7() const { return ___direction_7; }
	inline int32_t* get_address_of_direction_7() { return &___direction_7; }
	inline void set_direction_7(int32_t value)
	{
		___direction_7 = value;
	}

	inline static int32_t get_offset_of_row_version_8() { return static_cast<int32_t>(offsetof(SqliteParameter_t2628453389, ___row_version_8)); }
	inline int32_t get_row_version_8() const { return ___row_version_8; }
	inline int32_t* get_address_of_row_version_8() { return &___row_version_8; }
	inline void set_row_version_8(int32_t value)
	{
		___row_version_8 = value;
	}

	inline static int32_t get_offset_of_param_value_9() { return static_cast<int32_t>(offsetof(SqliteParameter_t2628453389, ___param_value_9)); }
	inline Il2CppObject * get_param_value_9() const { return ___param_value_9; }
	inline Il2CppObject ** get_address_of_param_value_9() { return &___param_value_9; }
	inline void set_param_value_9(Il2CppObject * value)
	{
		___param_value_9 = value;
		Il2CppCodeGenWriteBarrier(&___param_value_9, value);
	}

	inline static int32_t get_offset_of_size_10() { return static_cast<int32_t>(offsetof(SqliteParameter_t2628453389, ___size_10)); }
	inline int32_t get_size_10() const { return ___size_10; }
	inline int32_t* get_address_of_size_10() { return &___size_10; }
	inline void set_size_10(int32_t value)
	{
		___size_10 = value;
	}

	inline static int32_t get_offset_of_isNullable_11() { return static_cast<int32_t>(offsetof(SqliteParameter_t2628453389, ___isNullable_11)); }
	inline bool get_isNullable_11() const { return ___isNullable_11; }
	inline bool* get_address_of_isNullable_11() { return &___isNullable_11; }
	inline void set_isNullable_11(bool value)
	{
		___isNullable_11 = value;
	}

	inline static int32_t get_offset_of_sourceColumnNullMapping_12() { return static_cast<int32_t>(offsetof(SqliteParameter_t2628453389, ___sourceColumnNullMapping_12)); }
	inline bool get_sourceColumnNullMapping_12() const { return ___sourceColumnNullMapping_12; }
	inline bool* get_address_of_sourceColumnNullMapping_12() { return &___sourceColumnNullMapping_12; }
	inline void set_sourceColumnNullMapping_12(bool value)
	{
		___sourceColumnNullMapping_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
