﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<System.WeakReference>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m3143338627(__this, ___q0, method) ((  void (*) (Enumerator_t1076658417 *, Queue_1_t3902003996 *, const MethodInfo*))Enumerator__ctor_m3846579967_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.WeakReference>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1804448382(__this, method) ((  void (*) (Enumerator_t1076658417 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2396412922_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<System.WeakReference>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m498339754(__this, method) ((  Il2CppObject * (*) (Enumerator_t1076658417 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m280731440_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.WeakReference>::Dispose()
#define Enumerator_Dispose_m4156784697(__this, method) ((  void (*) (Enumerator_t1076658417 *, const MethodInfo*))Enumerator_Dispose_m3069945149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<System.WeakReference>::MoveNext()
#define Enumerator_MoveNext_m3259853142(__this, method) ((  bool (*) (Enumerator_t1076658417 *, const MethodInfo*))Enumerator_MoveNext_m262168254_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<System.WeakReference>::get_Current()
#define Enumerator_get_Current_m991287145(__this, method) ((  WeakReference_t2193916456 * (*) (Enumerator_t1076658417 *, const MethodInfo*))Enumerator_get_Current_m3381813839_gshared)(__this, method)
