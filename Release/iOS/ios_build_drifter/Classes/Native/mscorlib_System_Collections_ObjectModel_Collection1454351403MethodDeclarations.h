﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct Collection_1_t1454351403;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// SQLiteDatabase.SQLiteDB/DB_Field[]
struct DB_FieldU5BU5D_t1717179044;
// System.Collections.Generic.IEnumerator`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct IEnumerator_1_t968470121;
// System.Collections.Generic.IList`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct IList_1_t1651855987;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Field3780330969.h"

// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::.ctor()
extern "C"  void Collection_1__ctor_m311580542_gshared (Collection_1_t1454351403 * __this, const MethodInfo* method);
#define Collection_1__ctor_m311580542(__this, method) ((  void (*) (Collection_1_t1454351403 *, const MethodInfo*))Collection_1__ctor_m311580542_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m786179961_gshared (Collection_1_t1454351403 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m786179961(__this, method) ((  bool (*) (Collection_1_t1454351403 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m786179961_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m411568006_gshared (Collection_1_t1454351403 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m411568006(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1454351403 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m411568006_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m373633173_gshared (Collection_1_t1454351403 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m373633173(__this, method) ((  Il2CppObject * (*) (Collection_1_t1454351403 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m373633173_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1371002440_gshared (Collection_1_t1454351403 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1371002440(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1454351403 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1371002440_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1932569976_gshared (Collection_1_t1454351403 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1932569976(__this, ___value0, method) ((  bool (*) (Collection_1_t1454351403 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1932569976_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m420718752_gshared (Collection_1_t1454351403 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m420718752(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1454351403 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m420718752_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m750641427_gshared (Collection_1_t1454351403 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m750641427(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1454351403 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m750641427_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m736003381_gshared (Collection_1_t1454351403 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m736003381(__this, ___value0, method) ((  void (*) (Collection_1_t1454351403 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m736003381_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3445495524_gshared (Collection_1_t1454351403 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3445495524(__this, method) ((  bool (*) (Collection_1_t1454351403 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3445495524_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2758581910_gshared (Collection_1_t1454351403 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2758581910(__this, method) ((  Il2CppObject * (*) (Collection_1_t1454351403 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2758581910_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2563102439_gshared (Collection_1_t1454351403 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2563102439(__this, method) ((  bool (*) (Collection_1_t1454351403 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2563102439_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1506520882_gshared (Collection_1_t1454351403 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1506520882(__this, method) ((  bool (*) (Collection_1_t1454351403 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1506520882_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2068005533_gshared (Collection_1_t1454351403 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2068005533(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1454351403 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2068005533_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m425858090_gshared (Collection_1_t1454351403 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m425858090(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1454351403 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m425858090_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::Add(T)
extern "C"  void Collection_1_Add_m3879518529_gshared (Collection_1_t1454351403 * __this, DB_Field_t3780330969  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3879518529(__this, ___item0, method) ((  void (*) (Collection_1_t1454351403 *, DB_Field_t3780330969 , const MethodInfo*))Collection_1_Add_m3879518529_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::Clear()
extern "C"  void Collection_1_Clear_m2012681129_gshared (Collection_1_t1454351403 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2012681129(__this, method) ((  void (*) (Collection_1_t1454351403 *, const MethodInfo*))Collection_1_Clear_m2012681129_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3461050105_gshared (Collection_1_t1454351403 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3461050105(__this, method) ((  void (*) (Collection_1_t1454351403 *, const MethodInfo*))Collection_1_ClearItems_m3461050105_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::Contains(T)
extern "C"  bool Collection_1_Contains_m4233667739_gshared (Collection_1_t1454351403 * __this, DB_Field_t3780330969  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m4233667739(__this, ___item0, method) ((  bool (*) (Collection_1_t1454351403 *, DB_Field_t3780330969 , const MethodInfo*))Collection_1_Contains_m4233667739_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2304195313_gshared (Collection_1_t1454351403 * __this, DB_FieldU5BU5D_t1717179044* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2304195313(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1454351403 *, DB_FieldU5BU5D_t1717179044*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2304195313_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2469712114_gshared (Collection_1_t1454351403 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2469712114(__this, method) ((  Il2CppObject* (*) (Collection_1_t1454351403 *, const MethodInfo*))Collection_1_GetEnumerator_m2469712114_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m598999933_gshared (Collection_1_t1454351403 * __this, DB_Field_t3780330969  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m598999933(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1454351403 *, DB_Field_t3780330969 , const MethodInfo*))Collection_1_IndexOf_m598999933_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3099154856_gshared (Collection_1_t1454351403 * __this, int32_t ___index0, DB_Field_t3780330969  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3099154856(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1454351403 *, int32_t, DB_Field_t3780330969 , const MethodInfo*))Collection_1_Insert_m3099154856_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m89879515_gshared (Collection_1_t1454351403 * __this, int32_t ___index0, DB_Field_t3780330969  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m89879515(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1454351403 *, int32_t, DB_Field_t3780330969 , const MethodInfo*))Collection_1_InsertItem_m89879515_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::Remove(T)
extern "C"  bool Collection_1_Remove_m3507736406_gshared (Collection_1_t1454351403 * __this, DB_Field_t3780330969  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3507736406(__this, ___item0, method) ((  bool (*) (Collection_1_t1454351403 *, DB_Field_t3780330969 , const MethodInfo*))Collection_1_Remove_m3507736406_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m973007726_gshared (Collection_1_t1454351403 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m973007726(__this, ___index0, method) ((  void (*) (Collection_1_t1454351403 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m973007726_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1657179150_gshared (Collection_1_t1454351403 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1657179150(__this, ___index0, method) ((  void (*) (Collection_1_t1454351403 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1657179150_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1206436254_gshared (Collection_1_t1454351403 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1206436254(__this, method) ((  int32_t (*) (Collection_1_t1454351403 *, const MethodInfo*))Collection_1_get_Count_m1206436254_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32)
extern "C"  DB_Field_t3780330969  Collection_1_get_Item_m842385876_gshared (Collection_1_t1454351403 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m842385876(__this, ___index0, method) ((  DB_Field_t3780330969  (*) (Collection_1_t1454351403 *, int32_t, const MethodInfo*))Collection_1_get_Item_m842385876_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3685077119_gshared (Collection_1_t1454351403 * __this, int32_t ___index0, DB_Field_t3780330969  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3685077119(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1454351403 *, int32_t, DB_Field_t3780330969 , const MethodInfo*))Collection_1_set_Item_m3685077119_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2901109786_gshared (Collection_1_t1454351403 * __this, int32_t ___index0, DB_Field_t3780330969  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2901109786(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1454351403 *, int32_t, DB_Field_t3780330969 , const MethodInfo*))Collection_1_SetItem_m2901109786_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2170332209_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2170332209(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2170332209_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::ConvertItem(System.Object)
extern "C"  DB_Field_t3780330969  Collection_1_ConvertItem_m2131652275_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2131652275(__this /* static, unused */, ___item0, method) ((  DB_Field_t3780330969  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2131652275_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1248747825_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1248747825(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1248747825_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m218499059_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m218499059(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m218499059_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_Field>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1884781452_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1884781452(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1884781452_gshared)(__this /* static, unused */, ___list0, method)
