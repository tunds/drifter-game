﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickUpManager
struct PickUpManager_t2721081681;

#include "codegen/il2cpp-codegen.h"

// System.Void PickUpManager::.ctor()
extern "C"  void PickUpManager__ctor_m623698746 (PickUpManager_t2721081681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickUpManager::Start()
extern "C"  void PickUpManager_Start_m3865803834 (PickUpManager_t2721081681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickUpManager::SpawnItem()
extern "C"  void PickUpManager_SpawnItem_m3304709766 (PickUpManager_t2721081681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
