﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_Generic_Comparer_1_gen1303533693.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1/DefaultComparer<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct  DefaultComparer_t186827089  : public Comparer_1_t1303533693
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
