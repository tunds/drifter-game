﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Action>
struct List_1_t1234482916;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLiter.LoomManager/LoomDispatcher
struct  LoomDispatcher_t1654289480  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Action> SQLiter.LoomManager/LoomDispatcher::actions
	List_1_t1234482916 * ___actions_0;

public:
	inline static int32_t get_offset_of_actions_0() { return static_cast<int32_t>(offsetof(LoomDispatcher_t1654289480, ___actions_0)); }
	inline List_1_t1234482916 * get_actions_0() const { return ___actions_0; }
	inline List_1_t1234482916 ** get_address_of_actions_0() { return &___actions_0; }
	inline void set_actions_0(List_1_t1234482916 * value)
	{
		___actions_0 = value;
		Il2CppCodeGenWriteBarrier(&___actions_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
