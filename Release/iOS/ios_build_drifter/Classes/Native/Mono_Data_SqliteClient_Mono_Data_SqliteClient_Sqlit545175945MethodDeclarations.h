﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.SqliteClient.SqliteDataReader
struct SqliteDataReader_t545175945;
// Mono.Data.SqliteClient.SqliteCommand
struct SqliteCommand_t3207195823;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Data.DataTable
struct DataTable_t2176726999;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli3207195823.h"
#include "mscorlib_System_IntPtr676692020.h"

// System.Void Mono.Data.SqliteClient.SqliteDataReader::.ctor(Mono.Data.SqliteClient.SqliteCommand,System.IntPtr,System.Int32)
extern "C"  void SqliteDataReader__ctor_m3186976856 (SqliteDataReader_t545175945 * __this, SqliteCommand_t3207195823 * ___cmd0, IntPtr_t ___pVm1, int32_t ___version2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.SqliteDataReader::get_FieldCount()
extern "C"  int32_t SqliteDataReader_get_FieldCount_m4201696990 (SqliteDataReader_t545175945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.SqliteClient.SqliteDataReader::get_Item(System.Int32)
extern "C"  Il2CppObject * SqliteDataReader_get_Item_m1103853130 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteDataReader::get_IsClosed()
extern "C"  bool SqliteDataReader_get_IsClosed_m3649268921 (SqliteDataReader_t545175945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.SqliteDataReader::get_RecordsAffected()
extern "C"  int32_t SqliteDataReader_get_RecordsAffected_m2985366639 (SqliteDataReader_t545175945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteDataReader::ReadpVm(System.IntPtr,System.Int32,Mono.Data.SqliteClient.SqliteCommand)
extern "C"  void SqliteDataReader_ReadpVm_m1436201761 (SqliteDataReader_t545175945 * __this, IntPtr_t ___pVm0, int32_t ___version1, SqliteCommand_t3207195823 * ___cmd2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteDataReader::ReadingDone()
extern "C"  void SqliteDataReader_ReadingDone_m2348083870 (SqliteDataReader_t545175945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteDataReader::Close()
extern "C"  void SqliteDataReader_Close_m351493416 (SqliteDataReader_t545175945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteDataReader::Dispose(System.Boolean)
extern "C"  void SqliteDataReader_Dispose_m2110605062 (SqliteDataReader_t545175945 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Mono.Data.SqliteClient.SqliteDataReader::GetEnumerator()
extern "C"  Il2CppObject * SqliteDataReader_GetEnumerator_m1957115766 (SqliteDataReader_t545175945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.SqliteClient.SqliteDataReader::GetSchemaTable()
extern "C"  DataTable_t2176726999 * SqliteDataReader_GetSchemaTable_m1411085877 (SqliteDataReader_t545175945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteDataReader::NextResult()
extern "C"  bool SqliteDataReader_NextResult_m2337084842 (SqliteDataReader_t545175945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteDataReader::Read()
extern "C"  bool SqliteDataReader_Read_m1001217936 (SqliteDataReader_t545175945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteDataReader::GetBoolean(System.Int32)
extern "C"  bool SqliteDataReader_GetBoolean_m3554466685 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.SqliteClient.SqliteDataReader::GetDataTypeName(System.Int32)
extern "C"  String_t* SqliteDataReader_GetDataTypeName_m1418635401 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Mono.Data.SqliteClient.SqliteDataReader::GetFieldType(System.Int32)
extern "C"  Type_t * SqliteDataReader_GetFieldType_m4068207367 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 Mono.Data.SqliteClient.SqliteDataReader::GetInt16(System.Int32)
extern "C"  int16_t SqliteDataReader_GetInt16_m2270111253 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.SqliteDataReader::GetInt32(System.Int32)
extern "C"  int32_t SqliteDataReader_GetInt32_m2154195209 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.Data.SqliteClient.SqliteDataReader::GetInt64(System.Int32)
extern "C"  int64_t SqliteDataReader_GetInt64_m3001048967 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.SqliteClient.SqliteDataReader::GetName(System.Int32)
extern "C"  String_t* SqliteDataReader_GetName_m956931941 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.SqliteClient.SqliteDataReader::GetString(System.Int32)
extern "C"  String_t* SqliteDataReader_GetString_m1538306155 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.SqliteClient.SqliteDataReader::GetValue(System.Int32)
extern "C"  Il2CppObject * SqliteDataReader_GetValue_m1756856681 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.SqliteDataReader::GetValues(System.Object[])
extern "C"  int32_t SqliteDataReader_GetValues_m3144340824 (SqliteDataReader_t545175945 * __this, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteDataReader::IsDBNull(System.Int32)
extern "C"  bool SqliteDataReader_IsDBNull_m3635478490 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteDataReader::get_HasRows()
extern "C"  bool SqliteDataReader_get_HasRows_m634013074 (SqliteDataReader_t545175945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.SqliteDataReader::get_VisibleFieldCount()
extern "C"  int32_t SqliteDataReader_get_VisibleFieldCount_m3269162400 (SqliteDataReader_t545175945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
