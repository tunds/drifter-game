﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteCollation
struct SQLiteCollation_t3373936932;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void Mono.Data.Sqlite.SQLiteCollation::.ctor(System.Object,System.IntPtr)
extern "C"  void SQLiteCollation__ctor_m4271600356 (SQLiteCollation_t3373936932 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SQLiteCollation::Invoke(System.IntPtr,System.Int32,System.IntPtr,System.Int32,System.IntPtr)
extern "C"  int32_t SQLiteCollation_Invoke_m645071514 (SQLiteCollation_t3373936932 * __this, IntPtr_t ___puser0, int32_t ___len11, IntPtr_t ___pv12, int32_t ___len23, IntPtr_t ___pv24, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" int32_t pinvoke_delegate_wrapper_SQLiteCollation_t3373936932(Il2CppObject* delegate, IntPtr_t ___puser0, int32_t ___len11, IntPtr_t ___pv12, int32_t ___len23, IntPtr_t ___pv24);
// System.IAsyncResult Mono.Data.Sqlite.SQLiteCollation::BeginInvoke(System.IntPtr,System.Int32,System.IntPtr,System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SQLiteCollation_BeginInvoke_m2078320599 (SQLiteCollation_t3373936932 * __this, IntPtr_t ___puser0, int32_t ___len11, IntPtr_t ___pv12, int32_t ___len23, IntPtr_t ___pv24, AsyncCallback_t1363551830 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SQLiteCollation::EndInvoke(System.IAsyncResult)
extern "C"  int32_t SQLiteCollation_EndInvoke_m1187646224 (SQLiteCollation_t3373936932 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
