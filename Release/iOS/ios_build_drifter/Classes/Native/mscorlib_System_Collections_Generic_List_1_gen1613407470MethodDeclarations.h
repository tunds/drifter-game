﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct List_1_t1613407470;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerable_1_t3688602857;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t2299554949;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct ICollection_1_t1282279887;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct ReadOnlyCollection_1_t3979593849;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t3778309272;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct Predicate_1_t1387412399;
// System.Collections.Generic.IComparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IComparer_1_t3516155910;
// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct Comparison_1_t3520123377;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_816448501.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3994157758.h"

// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor()
extern "C"  void List_1__ctor_m3498238276_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1__ctor_m3498238276(__this, method) ((  void (*) (List_1_t1613407470 *, const MethodInfo*))List_1__ctor_m3498238276_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m200042683_gshared (List_1_t1613407470 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m200042683(__this, ___collection0, method) ((  void (*) (List_1_t1613407470 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m200042683_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3079983253_gshared (List_1_t1613407470 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3079983253(__this, ___capacity0, method) ((  void (*) (List_1_t1613407470 *, int32_t, const MethodInfo*))List_1__ctor_m3079983253_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.cctor()
extern "C"  void List_1__cctor_m589107945_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m589107945(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m589107945_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m309634454_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m309634454(__this, method) ((  Il2CppObject* (*) (List_1_t1613407470 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m309634454_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2304213120_gshared (List_1_t1613407470 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2304213120(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1613407470 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2304213120_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m420756347_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m420756347(__this, method) ((  Il2CppObject * (*) (List_1_t1613407470 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m420756347_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4173029718_gshared (List_1_t1613407470 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m4173029718(__this, ___item0, method) ((  int32_t (*) (List_1_t1613407470 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m4173029718_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3162513974_gshared (List_1_t1613407470 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3162513974(__this, ___item0, method) ((  bool (*) (List_1_t1613407470 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3162513974_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m773782702_gshared (List_1_t1613407470 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m773782702(__this, ___item0, method) ((  int32_t (*) (List_1_t1613407470 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m773782702_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1997616089_gshared (List_1_t1613407470 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1997616089(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1613407470 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1997616089_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m117319855_gshared (List_1_t1613407470 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m117319855(__this, ___item0, method) ((  void (*) (List_1_t1613407470 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m117319855_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2217166519_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2217166519(__this, method) ((  bool (*) (List_1_t1613407470 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2217166519_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m46170470_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m46170470(__this, method) ((  bool (*) (List_1_t1613407470 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m46170470_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m948244690_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m948244690(__this, method) ((  Il2CppObject * (*) (List_1_t1613407470 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m948244690_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m486748709_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m486748709(__this, method) ((  bool (*) (List_1_t1613407470 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m486748709_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m4210488372_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m4210488372(__this, method) ((  bool (*) (List_1_t1613407470 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m4210488372_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1795691993_gshared (List_1_t1613407470 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1795691993(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1613407470 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1795691993_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m472632688_gshared (List_1_t1613407470 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m472632688(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1613407470 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m472632688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Add(T)
extern "C"  void List_1_Add_m3151029947_gshared (List_1_t1613407470 * __this, KeyValuePair_2_t816448501  ___item0, const MethodInfo* method);
#define List_1_Add_m3151029947(__this, ___item0, method) ((  void (*) (List_1_t1613407470 *, KeyValuePair_2_t816448501 , const MethodInfo*))List_1_Add_m3151029947_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m4291848566_gshared (List_1_t1613407470 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m4291848566(__this, ___newCount0, method) ((  void (*) (List_1_t1613407470 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4291848566_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1783457908_gshared (List_1_t1613407470 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1783457908(__this, ___collection0, method) ((  void (*) (List_1_t1613407470 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1783457908_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1044430132_gshared (List_1_t1613407470 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1044430132(__this, ___enumerable0, method) ((  void (*) (List_1_t1613407470 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1044430132_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2107883747_gshared (List_1_t1613407470 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2107883747(__this, ___collection0, method) ((  void (*) (List_1_t1613407470 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2107883747_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3979593849 * List_1_AsReadOnly_m182613990_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m182613990(__this, method) ((  ReadOnlyCollection_1_t3979593849 * (*) (List_1_t1613407470 *, const MethodInfo*))List_1_AsReadOnly_m182613990_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Clear()
extern "C"  void List_1_Clear_m904371567_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_Clear_m904371567(__this, method) ((  void (*) (List_1_t1613407470 *, const MethodInfo*))List_1_Clear_m904371567_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Contains(T)
extern "C"  bool List_1_Contains_m529086749_gshared (List_1_t1613407470 * __this, KeyValuePair_2_t816448501  ___item0, const MethodInfo* method);
#define List_1_Contains_m529086749(__this, ___item0, method) ((  bool (*) (List_1_t1613407470 *, KeyValuePair_2_t816448501 , const MethodInfo*))List_1_Contains_m529086749_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m3558684268_gshared (List_1_t1613407470 * __this, KeyValuePair_2U5BU5D_t3778309272* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m3558684268(__this, ___array0, method) ((  void (*) (List_1_t1613407470 *, KeyValuePair_2U5BU5D_t3778309272*, const MethodInfo*))List_1_CopyTo_m3558684268_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2218702315_gshared (List_1_t1613407470 * __this, KeyValuePair_2U5BU5D_t3778309272* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2218702315(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1613407470 *, KeyValuePair_2U5BU5D_t3778309272*, int32_t, const MethodInfo*))List_1_CopyTo_m2218702315_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Find(System.Predicate`1<T>)
extern "C"  KeyValuePair_2_t816448501  List_1_Find_m3702644509_gshared (List_1_t1613407470 * __this, Predicate_1_t1387412399 * ___match0, const MethodInfo* method);
#define List_1_Find_m3702644509(__this, ___match0, method) ((  KeyValuePair_2_t816448501  (*) (List_1_t1613407470 *, Predicate_1_t1387412399 *, const MethodInfo*))List_1_Find_m3702644509_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m4230603096_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1387412399 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m4230603096(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1387412399 *, const MethodInfo*))List_1_CheckMatch_m4230603096_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3529043069_gshared (List_1_t1613407470 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1387412399 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3529043069(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1613407470 *, int32_t, int32_t, Predicate_1_t1387412399 *, const MethodInfo*))List_1_GetIndex_m3529043069_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::GetEnumerator()
extern "C"  Enumerator_t3994157758  List_1_GetEnumerator_m2038290394_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2038290394(__this, method) ((  Enumerator_t3994157758  (*) (List_1_t1613407470 *, const MethodInfo*))List_1_GetEnumerator_m2038290394_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m752166575_gshared (List_1_t1613407470 * __this, KeyValuePair_2_t816448501  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m752166575(__this, ___item0, method) ((  int32_t (*) (List_1_t1613407470 *, KeyValuePair_2_t816448501 , const MethodInfo*))List_1_IndexOf_m752166575_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1340305730_gshared (List_1_t1613407470 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1340305730(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1613407470 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1340305730_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1965069499_gshared (List_1_t1613407470 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1965069499(__this, ___index0, method) ((  void (*) (List_1_t1613407470 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1965069499_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3814148898_gshared (List_1_t1613407470 * __this, int32_t ___index0, KeyValuePair_2_t816448501  ___item1, const MethodInfo* method);
#define List_1_Insert_m3814148898(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1613407470 *, int32_t, KeyValuePair_2_t816448501 , const MethodInfo*))List_1_Insert_m3814148898_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m737766935_gshared (List_1_t1613407470 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m737766935(__this, ___collection0, method) ((  void (*) (List_1_t1613407470 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m737766935_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Remove(T)
extern "C"  bool List_1_Remove_m822320216_gshared (List_1_t1613407470 * __this, KeyValuePair_2_t816448501  ___item0, const MethodInfo* method);
#define List_1_Remove_m822320216(__this, ___item0, method) ((  bool (*) (List_1_t1613407470 *, KeyValuePair_2_t816448501 , const MethodInfo*))List_1_Remove_m822320216_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m14102834_gshared (List_1_t1613407470 * __this, Predicate_1_t1387412399 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m14102834(__this, ___match0, method) ((  int32_t (*) (List_1_t1613407470 *, Predicate_1_t1387412399 *, const MethodInfo*))List_1_RemoveAll_m14102834_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1688001768_gshared (List_1_t1613407470 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1688001768(__this, ___index0, method) ((  void (*) (List_1_t1613407470 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1688001768_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Reverse()
extern "C"  void List_1_Reverse_m3180369028_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_Reverse_m3180369028(__this, method) ((  void (*) (List_1_t1613407470 *, const MethodInfo*))List_1_Reverse_m3180369028_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Sort()
extern "C"  void List_1_Sort_m3677002142_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_Sort_m3677002142(__this, method) ((  void (*) (List_1_t1613407470 *, const MethodInfo*))List_1_Sort_m3677002142_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3639711238_gshared (List_1_t1613407470 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3639711238(__this, ___comparer0, method) ((  void (*) (List_1_t1613407470 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3639711238_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3630463857_gshared (List_1_t1613407470 * __this, Comparison_1_t3520123377 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3630463857(__this, ___comparison0, method) ((  void (*) (List_1_t1613407470 *, Comparison_1_t3520123377 *, const MethodInfo*))List_1_Sort_m3630463857_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t3778309272* List_1_ToArray_m2260537475_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_ToArray_m2260537475(__this, method) ((  KeyValuePair_2U5BU5D_t3778309272* (*) (List_1_t1613407470 *, const MethodInfo*))List_1_ToArray_m2260537475_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2069596855_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2069596855(__this, method) ((  void (*) (List_1_t1613407470 *, const MethodInfo*))List_1_TrimExcess_m2069596855_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3147050847_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3147050847(__this, method) ((  int32_t (*) (List_1_t1613407470 *, const MethodInfo*))List_1_get_Capacity_m3147050847_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1502037384_gshared (List_1_t1613407470 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1502037384(__this, ___value0, method) ((  void (*) (List_1_t1613407470 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1502037384_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Count()
extern "C"  int32_t List_1_get_Count_m1659634860_gshared (List_1_t1613407470 * __this, const MethodInfo* method);
#define List_1_get_Count_m1659634860(__this, method) ((  int32_t (*) (List_1_t1613407470 *, const MethodInfo*))List_1_get_Count_m1659634860_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t816448501  List_1_get_Item_m1236721004_gshared (List_1_t1613407470 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1236721004(__this, ___index0, method) ((  KeyValuePair_2_t816448501  (*) (List_1_t1613407470 *, int32_t, const MethodInfo*))List_1_get_Item_m1236721004_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3599584121_gshared (List_1_t1613407470 * __this, int32_t ___index0, KeyValuePair_2_t816448501  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3599584121(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1613407470 *, int32_t, KeyValuePair_2_t816448501 , const MethodInfo*))List_1_set_Item_m3599584121_gshared)(__this, ___index0, ___value1, method)
