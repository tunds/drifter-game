﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t792326996;
// UnityEngine.AudioSource
struct AudioSource_t3628549054;
// UnityEngine.AudioClip
struct AudioClip_t3714538611;
// PlayerHealth
struct PlayerHealth_t3877793981;
// EnemyHealth
struct EnemyHealth_t1417584612;
// PlayerEffects
struct PlayerEffects_t1618007745;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyAttack
struct  EnemyAttack_t1231588304  : public MonoBehaviour_t3012272455
{
public:
	// System.Single EnemyAttack::timeBetweenAttacks
	float ___timeBetweenAttacks_2;
	// System.Int32 EnemyAttack::attackDamage
	int32_t ___attackDamage_3;
	// System.Single EnemyAttack::timer
	float ___timer_4;
	// System.Int32 EnemyAttack::atkHash
	int32_t ___atkHash_5;
	// System.Boolean EnemyAttack::plyrInRange
	bool ___plyrInRange_6;
	// UnityEngine.Animator EnemyAttack::anim
	Animator_t792326996 * ___anim_7;
	// UnityEngine.AudioSource EnemyAttack::audio
	AudioSource_t3628549054 * ___audio_8;
	// UnityEngine.AudioClip EnemyAttack::eatingAudio
	AudioClip_t3714538611 * ___eatingAudio_9;
	// PlayerHealth EnemyAttack::playerHealth
	PlayerHealth_t3877793981 * ___playerHealth_10;
	// EnemyHealth EnemyAttack::enemyHealth
	EnemyHealth_t1417584612 * ___enemyHealth_11;
	// PlayerEffects EnemyAttack::playerEffects
	PlayerEffects_t1618007745 * ___playerEffects_12;

public:
	inline static int32_t get_offset_of_timeBetweenAttacks_2() { return static_cast<int32_t>(offsetof(EnemyAttack_t1231588304, ___timeBetweenAttacks_2)); }
	inline float get_timeBetweenAttacks_2() const { return ___timeBetweenAttacks_2; }
	inline float* get_address_of_timeBetweenAttacks_2() { return &___timeBetweenAttacks_2; }
	inline void set_timeBetweenAttacks_2(float value)
	{
		___timeBetweenAttacks_2 = value;
	}

	inline static int32_t get_offset_of_attackDamage_3() { return static_cast<int32_t>(offsetof(EnemyAttack_t1231588304, ___attackDamage_3)); }
	inline int32_t get_attackDamage_3() const { return ___attackDamage_3; }
	inline int32_t* get_address_of_attackDamage_3() { return &___attackDamage_3; }
	inline void set_attackDamage_3(int32_t value)
	{
		___attackDamage_3 = value;
	}

	inline static int32_t get_offset_of_timer_4() { return static_cast<int32_t>(offsetof(EnemyAttack_t1231588304, ___timer_4)); }
	inline float get_timer_4() const { return ___timer_4; }
	inline float* get_address_of_timer_4() { return &___timer_4; }
	inline void set_timer_4(float value)
	{
		___timer_4 = value;
	}

	inline static int32_t get_offset_of_atkHash_5() { return static_cast<int32_t>(offsetof(EnemyAttack_t1231588304, ___atkHash_5)); }
	inline int32_t get_atkHash_5() const { return ___atkHash_5; }
	inline int32_t* get_address_of_atkHash_5() { return &___atkHash_5; }
	inline void set_atkHash_5(int32_t value)
	{
		___atkHash_5 = value;
	}

	inline static int32_t get_offset_of_plyrInRange_6() { return static_cast<int32_t>(offsetof(EnemyAttack_t1231588304, ___plyrInRange_6)); }
	inline bool get_plyrInRange_6() const { return ___plyrInRange_6; }
	inline bool* get_address_of_plyrInRange_6() { return &___plyrInRange_6; }
	inline void set_plyrInRange_6(bool value)
	{
		___plyrInRange_6 = value;
	}

	inline static int32_t get_offset_of_anim_7() { return static_cast<int32_t>(offsetof(EnemyAttack_t1231588304, ___anim_7)); }
	inline Animator_t792326996 * get_anim_7() const { return ___anim_7; }
	inline Animator_t792326996 ** get_address_of_anim_7() { return &___anim_7; }
	inline void set_anim_7(Animator_t792326996 * value)
	{
		___anim_7 = value;
		Il2CppCodeGenWriteBarrier(&___anim_7, value);
	}

	inline static int32_t get_offset_of_audio_8() { return static_cast<int32_t>(offsetof(EnemyAttack_t1231588304, ___audio_8)); }
	inline AudioSource_t3628549054 * get_audio_8() const { return ___audio_8; }
	inline AudioSource_t3628549054 ** get_address_of_audio_8() { return &___audio_8; }
	inline void set_audio_8(AudioSource_t3628549054 * value)
	{
		___audio_8 = value;
		Il2CppCodeGenWriteBarrier(&___audio_8, value);
	}

	inline static int32_t get_offset_of_eatingAudio_9() { return static_cast<int32_t>(offsetof(EnemyAttack_t1231588304, ___eatingAudio_9)); }
	inline AudioClip_t3714538611 * get_eatingAudio_9() const { return ___eatingAudio_9; }
	inline AudioClip_t3714538611 ** get_address_of_eatingAudio_9() { return &___eatingAudio_9; }
	inline void set_eatingAudio_9(AudioClip_t3714538611 * value)
	{
		___eatingAudio_9 = value;
		Il2CppCodeGenWriteBarrier(&___eatingAudio_9, value);
	}

	inline static int32_t get_offset_of_playerHealth_10() { return static_cast<int32_t>(offsetof(EnemyAttack_t1231588304, ___playerHealth_10)); }
	inline PlayerHealth_t3877793981 * get_playerHealth_10() const { return ___playerHealth_10; }
	inline PlayerHealth_t3877793981 ** get_address_of_playerHealth_10() { return &___playerHealth_10; }
	inline void set_playerHealth_10(PlayerHealth_t3877793981 * value)
	{
		___playerHealth_10 = value;
		Il2CppCodeGenWriteBarrier(&___playerHealth_10, value);
	}

	inline static int32_t get_offset_of_enemyHealth_11() { return static_cast<int32_t>(offsetof(EnemyAttack_t1231588304, ___enemyHealth_11)); }
	inline EnemyHealth_t1417584612 * get_enemyHealth_11() const { return ___enemyHealth_11; }
	inline EnemyHealth_t1417584612 ** get_address_of_enemyHealth_11() { return &___enemyHealth_11; }
	inline void set_enemyHealth_11(EnemyHealth_t1417584612 * value)
	{
		___enemyHealth_11 = value;
		Il2CppCodeGenWriteBarrier(&___enemyHealth_11, value);
	}

	inline static int32_t get_offset_of_playerEffects_12() { return static_cast<int32_t>(offsetof(EnemyAttack_t1231588304, ___playerEffects_12)); }
	inline PlayerEffects_t1618007745 * get_playerEffects_12() const { return ___playerEffects_12; }
	inline PlayerEffects_t1618007745 ** get_address_of_playerEffects_12() { return &___playerEffects_12; }
	inline void set_playerEffects_12(PlayerEffects_t1618007745 * value)
	{
		___playerEffects_12 = value;
		Il2CppCodeGenWriteBarrier(&___playerEffects_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
