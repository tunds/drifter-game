﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbParameter
struct DbParameter_t3306161371;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Data.Common.DbParameter::.ctor()
extern "C"  void DbParameter__ctor_m2022249449 (DbParameter_t3306161371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbParameter::System.Data.IDbDataParameter.set_Precision(System.Byte)
extern "C"  void DbParameter_System_Data_IDbDataParameter_set_Precision_m2596664928 (DbParameter_t3306161371 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbParameter::System.Data.IDbDataParameter.set_Scale(System.Byte)
extern "C"  void DbParameter_System_Data_IDbDataParameter_set_Scale_m3831076628 (DbParameter_t3306161371 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbParameter::set_FrameworkDbType(System.Object)
extern "C"  void DbParameter_set_FrameworkDbType_m3180540596 (DbParameter_t3306161371 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
