﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbProviderFactory
struct DbProviderFactory_t2435213707;
// System.NotImplementedException
struct NotImplementedException_t1091014741;
// System.Data.Common.DbCommand
struct DbCommand_t2323745021;
// System.Data.Common.DbConnection
struct DbConnection_t462757452;
// System.Data.Common.DbDataAdapter
struct DbDataAdapter_t3684585719;
// System.Data.Common.DbParameter
struct DbParameter_t3306161371;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Data.Common.DbProviderFactory::.ctor()
extern "C"  void DbProviderFactory__ctor_m2548214905 (DbProviderFactory_t2435213707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.NotImplementedException System.Data.Common.DbProviderFactory::CreateNotImplementedException()
extern "C"  NotImplementedException_t1091014741 * DbProviderFactory_CreateNotImplementedException_m1064057353 (DbProviderFactory_t2435213707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbProviderFactory::CreateCommand()
extern "C"  DbCommand_t2323745021 * DbProviderFactory_CreateCommand_m1585380610 (DbProviderFactory_t2435213707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbConnection System.Data.Common.DbProviderFactory::CreateConnection()
extern "C"  DbConnection_t462757452 * DbProviderFactory_CreateConnection_m1027815742 (DbProviderFactory_t2435213707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbDataAdapter System.Data.Common.DbProviderFactory::CreateDataAdapter()
extern "C"  DbDataAdapter_t3684585719 * DbProviderFactory_CreateDataAdapter_m1818676982 (DbProviderFactory_t2435213707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameter System.Data.Common.DbProviderFactory::CreateParameter()
extern "C"  DbParameter_t3306161371 * DbProviderFactory_CreateParameter_m264439230 (DbProviderFactory_t2435213707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
