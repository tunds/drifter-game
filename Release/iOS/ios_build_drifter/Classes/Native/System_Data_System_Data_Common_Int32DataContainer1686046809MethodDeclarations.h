﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.Int32DataContainer
struct Int32DataContainer_t1686046809;
// System.Object
struct Il2CppObject;
// System.Data.ISafeDataRecord
struct ISafeDataRecord_t3927591524;
// System.Data.Common.DataContainer
struct DataContainer_t1942492167;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Data_System_Data_Common_DataContainer1942492167.h"

// System.Void System.Data.Common.Int32DataContainer::.ctor()
extern "C"  void Int32DataContainer__ctor_m2043614633 (Int32DataContainer_t1686046809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Data.Common.Int32DataContainer::GetValue(System.Int32)
extern "C"  Il2CppObject * Int32DataContainer_GetValue_m1384212338 (Int32DataContainer_t1686046809 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.Int32DataContainer::ZeroOut(System.Int32)
extern "C"  void Int32DataContainer_ZeroOut_m209213726 (Int32DataContainer_t1686046809 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.Int32DataContainer::SetValue(System.Int32,System.Object)
extern "C"  void Int32DataContainer_SetValue_m1954767305 (Int32DataContainer_t1686046809 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.Int32DataContainer::SetValueFromSafeDataRecord(System.Int32,System.Data.ISafeDataRecord,System.Int32)
extern "C"  void Int32DataContainer_SetValueFromSafeDataRecord_m2686372906 (Int32DataContainer_t1686046809 * __this, int32_t ___index0, Il2CppObject * ___record1, int32_t ___field2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.Int32DataContainer::DoCopyValue(System.Data.Common.DataContainer,System.Int32,System.Int32)
extern "C"  void Int32DataContainer_DoCopyValue_m3416098431 (Int32DataContainer_t1686046809 * __this, DataContainer_t1942492167 * ___from0, int32_t ___from_index1, int32_t ___to_index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.Int32DataContainer::DoCompareValues(System.Int32,System.Int32)
extern "C"  int32_t Int32DataContainer_DoCompareValues_m2628550463 (Int32DataContainer_t1686046809 * __this, int32_t ___index10, int32_t ___index21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.Int32DataContainer::Resize(System.Int32)
extern "C"  void Int32DataContainer_Resize_m3734122112 (Int32DataContainer_t1686046809 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Data.Common.Int32DataContainer::GetInt64(System.Int32)
extern "C"  int64_t Int32DataContainer_GetInt64_m567728126 (Int32DataContainer_t1686046809 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
