﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3540781296MethodDeclarations.h"

// System.Void System.Comparison`1<Mono.Data.Sqlite.SqliteFunction>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m574277796(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t1945925255 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m487232819_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<Mono.Data.Sqlite.SqliteFunction>::Invoke(T,T)
#define Comparison_1_Invoke_m659021852(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t1945925255 *, SqliteFunction_t3537217675 *, SqliteFunction_t3537217675 *, const MethodInfo*))Comparison_1_Invoke_m1888033133_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<Mono.Data.Sqlite.SqliteFunction>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m1352177557(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t1945925255 *, SqliteFunction_t3537217675 *, SqliteFunction_t3537217675 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3177996774_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<Mono.Data.Sqlite.SqliteFunction>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m1321648848(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t1945925255 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m651541983_gshared)(__this, ___result0, method)
