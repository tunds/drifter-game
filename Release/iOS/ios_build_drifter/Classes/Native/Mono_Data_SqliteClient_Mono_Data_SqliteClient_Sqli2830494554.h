﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t180559927;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "System_Data_System_Data_Common_DbConnection462757452.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_Data_System_Data_ConnectionState270608870.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqliteClient.SqliteConnection
struct  SqliteConnection_t2830494554  : public DbConnection_t462757452
{
public:
	// System.String Mono.Data.SqliteClient.SqliteConnection::conn_str
	String_t* ___conn_str_6;
	// System.String Mono.Data.SqliteClient.SqliteConnection::db_file
	String_t* ___db_file_7;
	// System.Int32 Mono.Data.SqliteClient.SqliteConnection::db_mode
	int32_t ___db_mode_8;
	// System.Int32 Mono.Data.SqliteClient.SqliteConnection::db_version
	int32_t ___db_version_9;
	// System.IntPtr Mono.Data.SqliteClient.SqliteConnection::sqlite_handle
	IntPtr_t ___sqlite_handle_10;
	// System.Data.ConnectionState Mono.Data.SqliteClient.SqliteConnection::state
	int32_t ___state_11;
	// System.Text.Encoding Mono.Data.SqliteClient.SqliteConnection::encoding
	Encoding_t180559927 * ___encoding_12;
	// System.Int32 Mono.Data.SqliteClient.SqliteConnection::busy_timeout
	int32_t ___busy_timeout_13;
	// System.Boolean Mono.Data.SqliteClient.SqliteConnection::disposed
	bool ___disposed_14;

public:
	inline static int32_t get_offset_of_conn_str_6() { return static_cast<int32_t>(offsetof(SqliteConnection_t2830494554, ___conn_str_6)); }
	inline String_t* get_conn_str_6() const { return ___conn_str_6; }
	inline String_t** get_address_of_conn_str_6() { return &___conn_str_6; }
	inline void set_conn_str_6(String_t* value)
	{
		___conn_str_6 = value;
		Il2CppCodeGenWriteBarrier(&___conn_str_6, value);
	}

	inline static int32_t get_offset_of_db_file_7() { return static_cast<int32_t>(offsetof(SqliteConnection_t2830494554, ___db_file_7)); }
	inline String_t* get_db_file_7() const { return ___db_file_7; }
	inline String_t** get_address_of_db_file_7() { return &___db_file_7; }
	inline void set_db_file_7(String_t* value)
	{
		___db_file_7 = value;
		Il2CppCodeGenWriteBarrier(&___db_file_7, value);
	}

	inline static int32_t get_offset_of_db_mode_8() { return static_cast<int32_t>(offsetof(SqliteConnection_t2830494554, ___db_mode_8)); }
	inline int32_t get_db_mode_8() const { return ___db_mode_8; }
	inline int32_t* get_address_of_db_mode_8() { return &___db_mode_8; }
	inline void set_db_mode_8(int32_t value)
	{
		___db_mode_8 = value;
	}

	inline static int32_t get_offset_of_db_version_9() { return static_cast<int32_t>(offsetof(SqliteConnection_t2830494554, ___db_version_9)); }
	inline int32_t get_db_version_9() const { return ___db_version_9; }
	inline int32_t* get_address_of_db_version_9() { return &___db_version_9; }
	inline void set_db_version_9(int32_t value)
	{
		___db_version_9 = value;
	}

	inline static int32_t get_offset_of_sqlite_handle_10() { return static_cast<int32_t>(offsetof(SqliteConnection_t2830494554, ___sqlite_handle_10)); }
	inline IntPtr_t get_sqlite_handle_10() const { return ___sqlite_handle_10; }
	inline IntPtr_t* get_address_of_sqlite_handle_10() { return &___sqlite_handle_10; }
	inline void set_sqlite_handle_10(IntPtr_t value)
	{
		___sqlite_handle_10 = value;
	}

	inline static int32_t get_offset_of_state_11() { return static_cast<int32_t>(offsetof(SqliteConnection_t2830494554, ___state_11)); }
	inline int32_t get_state_11() const { return ___state_11; }
	inline int32_t* get_address_of_state_11() { return &___state_11; }
	inline void set_state_11(int32_t value)
	{
		___state_11 = value;
	}

	inline static int32_t get_offset_of_encoding_12() { return static_cast<int32_t>(offsetof(SqliteConnection_t2830494554, ___encoding_12)); }
	inline Encoding_t180559927 * get_encoding_12() const { return ___encoding_12; }
	inline Encoding_t180559927 ** get_address_of_encoding_12() { return &___encoding_12; }
	inline void set_encoding_12(Encoding_t180559927 * value)
	{
		___encoding_12 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_12, value);
	}

	inline static int32_t get_offset_of_busy_timeout_13() { return static_cast<int32_t>(offsetof(SqliteConnection_t2830494554, ___busy_timeout_13)); }
	inline int32_t get_busy_timeout_13() const { return ___busy_timeout_13; }
	inline int32_t* get_address_of_busy_timeout_13() { return &___busy_timeout_13; }
	inline void set_busy_timeout_13(int32_t value)
	{
		___busy_timeout_13 = value;
	}

	inline static int32_t get_offset_of_disposed_14() { return static_cast<int32_t>(offsetof(SqliteConnection_t2830494554, ___disposed_14)); }
	inline bool get_disposed_14() const { return ___disposed_14; }
	inline bool* get_address_of_disposed_14() { return &___disposed_14; }
	inline void set_disposed_14(bool value)
	{
		___disposed_14 = value;
	}
};

struct SqliteConnection_t2830494554_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Data.SqliteClient.SqliteConnection::<>f__switch$map0
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map0_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_15() { return static_cast<int32_t>(offsetof(SqliteConnection_t2830494554_StaticFields, ___U3CU3Ef__switchU24map0_15)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map0_15() const { return ___U3CU3Ef__switchU24map0_15; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map0_15() { return &___U3CU3Ef__switchU24map0_15; }
	inline void set_U3CU3Ef__switchU24map0_15(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map0_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
