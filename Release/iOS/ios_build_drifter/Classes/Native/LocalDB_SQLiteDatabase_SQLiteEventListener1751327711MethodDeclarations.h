﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLiteDatabase.SQLiteEventListener/OnError
struct OnError_t314785609;

#include "codegen/il2cpp-codegen.h"
#include "LocalDB_SQLiteDatabase_SQLiteEventListener_OnError314785609.h"

// System.Void SQLiteDatabase.SQLiteEventListener::add_onError(SQLiteDatabase.SQLiteEventListener/OnError)
extern "C"  void SQLiteEventListener_add_onError_m1384741999 (Il2CppObject * __this /* static, unused */, OnError_t314785609 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiteDatabase.SQLiteEventListener::remove_onError(SQLiteDatabase.SQLiteEventListener/OnError)
extern "C"  void SQLiteEventListener_remove_onError_m1527352418 (Il2CppObject * __this /* static, unused */, OnError_t314785609 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiteDatabase.SQLiteEventListener::AddHandler_onError(SQLiteDatabase.SQLiteEventListener/OnError)
extern "C"  void SQLiteEventListener_AddHandler_onError_m1563276007 (Il2CppObject * __this /* static, unused */, OnError_t314785609 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiteDatabase.SQLiteEventListener::RemoveHandler_onError(SQLiteDatabase.SQLiteEventListener/OnError)
extern "C"  void SQLiteEventListener_RemoveHandler_onError_m1626034004 (Il2CppObject * __this /* static, unused */, OnError_t314785609 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
