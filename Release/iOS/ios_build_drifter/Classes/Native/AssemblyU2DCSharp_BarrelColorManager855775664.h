﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Renderer
struct Renderer_t1092684080;
// UnityEngine.ReflectionProbe
struct ReflectionProbe_t936962674;
// UnityEngine.Material
struct Material_t1886596500;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "AssemblyU2DCSharp_BarrelColorManager_ReflectMode2261419488.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarrelColorManager
struct  BarrelColorManager_t855775664  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Renderer BarrelColorManager::BodyRenderer
	Renderer_t1092684080 * ___BodyRenderer_2;
	// UnityEngine.Color BarrelColorManager::BodyColor
	Color_t1588175760  ___BodyColor_3;
	// BarrelColorManager/ReflectMode BarrelColorManager::ReflectionMode
	int32_t ___ReflectionMode_4;
	// UnityEngine.ReflectionProbe BarrelColorManager::ReflectProbe
	ReflectionProbe_t936962674 * ___ReflectProbe_5;
	// UnityEngine.Material BarrelColorManager::ReferenceMaterial
	Material_t1886596500 * ___ReferenceMaterial_6;
	// UnityEngine.Material BarrelColorManager::RuntimeMaterial
	Material_t1886596500 * ___RuntimeMaterial_7;

public:
	inline static int32_t get_offset_of_BodyRenderer_2() { return static_cast<int32_t>(offsetof(BarrelColorManager_t855775664, ___BodyRenderer_2)); }
	inline Renderer_t1092684080 * get_BodyRenderer_2() const { return ___BodyRenderer_2; }
	inline Renderer_t1092684080 ** get_address_of_BodyRenderer_2() { return &___BodyRenderer_2; }
	inline void set_BodyRenderer_2(Renderer_t1092684080 * value)
	{
		___BodyRenderer_2 = value;
		Il2CppCodeGenWriteBarrier(&___BodyRenderer_2, value);
	}

	inline static int32_t get_offset_of_BodyColor_3() { return static_cast<int32_t>(offsetof(BarrelColorManager_t855775664, ___BodyColor_3)); }
	inline Color_t1588175760  get_BodyColor_3() const { return ___BodyColor_3; }
	inline Color_t1588175760 * get_address_of_BodyColor_3() { return &___BodyColor_3; }
	inline void set_BodyColor_3(Color_t1588175760  value)
	{
		___BodyColor_3 = value;
	}

	inline static int32_t get_offset_of_ReflectionMode_4() { return static_cast<int32_t>(offsetof(BarrelColorManager_t855775664, ___ReflectionMode_4)); }
	inline int32_t get_ReflectionMode_4() const { return ___ReflectionMode_4; }
	inline int32_t* get_address_of_ReflectionMode_4() { return &___ReflectionMode_4; }
	inline void set_ReflectionMode_4(int32_t value)
	{
		___ReflectionMode_4 = value;
	}

	inline static int32_t get_offset_of_ReflectProbe_5() { return static_cast<int32_t>(offsetof(BarrelColorManager_t855775664, ___ReflectProbe_5)); }
	inline ReflectionProbe_t936962674 * get_ReflectProbe_5() const { return ___ReflectProbe_5; }
	inline ReflectionProbe_t936962674 ** get_address_of_ReflectProbe_5() { return &___ReflectProbe_5; }
	inline void set_ReflectProbe_5(ReflectionProbe_t936962674 * value)
	{
		___ReflectProbe_5 = value;
		Il2CppCodeGenWriteBarrier(&___ReflectProbe_5, value);
	}

	inline static int32_t get_offset_of_ReferenceMaterial_6() { return static_cast<int32_t>(offsetof(BarrelColorManager_t855775664, ___ReferenceMaterial_6)); }
	inline Material_t1886596500 * get_ReferenceMaterial_6() const { return ___ReferenceMaterial_6; }
	inline Material_t1886596500 ** get_address_of_ReferenceMaterial_6() { return &___ReferenceMaterial_6; }
	inline void set_ReferenceMaterial_6(Material_t1886596500 * value)
	{
		___ReferenceMaterial_6 = value;
		Il2CppCodeGenWriteBarrier(&___ReferenceMaterial_6, value);
	}

	inline static int32_t get_offset_of_RuntimeMaterial_7() { return static_cast<int32_t>(offsetof(BarrelColorManager_t855775664, ___RuntimeMaterial_7)); }
	inline Material_t1886596500 * get_RuntimeMaterial_7() const { return ___RuntimeMaterial_7; }
	inline Material_t1886596500 ** get_address_of_RuntimeMaterial_7() { return &___RuntimeMaterial_7; }
	inline void set_RuntimeMaterial_7(Material_t1886596500 * value)
	{
		___RuntimeMaterial_7 = value;
		Il2CppCodeGenWriteBarrier(&___RuntimeMaterial_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
