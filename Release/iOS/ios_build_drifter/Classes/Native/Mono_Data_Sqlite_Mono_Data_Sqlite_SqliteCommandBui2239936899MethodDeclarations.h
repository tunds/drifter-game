﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteCommandBuilder
struct SqliteCommandBuilder_t2239936899;
// Mono.Data.Sqlite.SqliteDataAdapter
struct SqliteDataAdapter_t2699555040;
// System.Data.Common.DbParameter
struct DbParameter_t3306161371;
// System.Data.DataRow
struct DataRow_t3654701923;
// System.String
struct String_t;
// System.Data.Common.DbDataAdapter
struct DbDataAdapter_t3684585719;
// System.Object
struct Il2CppObject;
// System.Data.Common.RowUpdatingEventArgs
struct RowUpdatingEventArgs_t2786481031;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteDataAdapte2699555040.h"
#include "System_Data_System_Data_Common_DbParameter3306161371.h"
#include "System_Data_System_Data_DataRow3654701923.h"
#include "System_Data_System_Data_StatementType1752600284.h"
#include "mscorlib_System_String968488902.h"
#include "System_Data_System_Data_Common_DbDataAdapter3684585719.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Data_System_Data_Common_RowUpdatingEventArg2786481031.h"

// System.Void Mono.Data.Sqlite.SqliteCommandBuilder::.ctor()
extern "C"  void SqliteCommandBuilder__ctor_m1634471887 (SqliteCommandBuilder_t2239936899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommandBuilder::.ctor(Mono.Data.Sqlite.SqliteDataAdapter)
extern "C"  void SqliteCommandBuilder__ctor_m858966303 (SqliteCommandBuilder_t2239936899 * __this, SqliteDataAdapter_t2699555040 * ___adp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommandBuilder::ApplyParameterInfo(System.Data.Common.DbParameter,System.Data.DataRow,System.Data.StatementType,System.Boolean)
extern "C"  void SqliteCommandBuilder_ApplyParameterInfo_m410024963 (SqliteCommandBuilder_t2239936899 * __this, DbParameter_t3306161371 * ___parameter0, DataRow_t3654701923 * ___row1, int32_t ___statementType2, bool ___whereClause3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteCommandBuilder::GetParameterName(System.String)
extern "C"  String_t* SqliteCommandBuilder_GetParameterName_m1821440850 (SqliteCommandBuilder_t2239936899 * __this, String_t* ___parameterName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteCommandBuilder::GetParameterName(System.Int32)
extern "C"  String_t* SqliteCommandBuilder_GetParameterName_m3923349249 (SqliteCommandBuilder_t2239936899 * __this, int32_t ___parameterOrdinal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommandBuilder::SetRowUpdatingHandler(System.Data.Common.DbDataAdapter)
extern "C"  void SqliteCommandBuilder_SetRowUpdatingHandler_m2424672272 (SqliteCommandBuilder_t2239936899 * __this, DbDataAdapter_t3684585719 * ___adapter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommandBuilder::RowUpdatingEventHandler(System.Object,System.Data.Common.RowUpdatingEventArgs)
extern "C"  void SqliteCommandBuilder_RowUpdatingEventHandler_m3597908462 (SqliteCommandBuilder_t2239936899 * __this, Il2CppObject * ___sender0, RowUpdatingEventArgs_t2786481031 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommandBuilder::set_DataAdapter(Mono.Data.Sqlite.SqliteDataAdapter)
extern "C"  void SqliteCommandBuilder_set_DataAdapter_m3806469029 (SqliteCommandBuilder_t2239936899 * __this, SqliteDataAdapter_t2699555040 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteCommandBuilder::get_QuotePrefix()
extern "C"  String_t* SqliteCommandBuilder_get_QuotePrefix_m1987703893 (SqliteCommandBuilder_t2239936899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommandBuilder::set_QuotePrefix(System.String)
extern "C"  void SqliteCommandBuilder_set_QuotePrefix_m409835076 (SqliteCommandBuilder_t2239936899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteCommandBuilder::get_QuoteSuffix()
extern "C"  String_t* SqliteCommandBuilder_get_QuoteSuffix_m1317340500 (SqliteCommandBuilder_t2239936899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteCommandBuilder::set_QuoteSuffix(System.String)
extern "C"  void SqliteCommandBuilder_set_QuoteSuffix_m3267910565 (SqliteCommandBuilder_t2239936899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteCommandBuilder::QuoteIdentifier(System.String)
extern "C"  String_t* SqliteCommandBuilder_QuoteIdentifier_m2140663117 (SqliteCommandBuilder_t2239936899 * __this, String_t* ___unquotedIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteCommandBuilder::UnquoteIdentifier(System.String)
extern "C"  String_t* SqliteCommandBuilder_UnquoteIdentifier_m537872870 (SqliteCommandBuilder_t2239936899 * __this, String_t* ___quotedIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
