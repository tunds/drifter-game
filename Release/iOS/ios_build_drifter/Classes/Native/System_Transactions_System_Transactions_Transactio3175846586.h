﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Transactions.Transaction
struct Transaction_t3175846586;
// System.Transactions.TransactionInformation
struct TransactionInformation_t3294574474;
// System.Collections.ArrayList
struct ArrayList_t2121638921;
// System.Collections.Generic.List`1<System.Transactions.IEnlistmentNotification>
struct List_1_t2549268686;
// System.Collections.Generic.List`1<System.Transactions.ISinglePhaseNotification>
struct List_1_t2960894218;
// System.Transactions.Transaction/AsyncCommit
struct AsyncCommit_t802550451;
// System.Transactions.TransactionScope
struct TransactionScope_t3316380210;
// System.Exception
struct Exception_t1967233988;
// System.Transactions.TransactionCompletedEventHandler
struct TransactionCompletedEventHandler_t1858411801;

#include "mscorlib_System_Object837106420.h"
#include "System_Transactions_System_Transactions_IsolationL2549522802.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Transactions.Transaction
struct  Transaction_t3175846586  : public Il2CppObject
{
public:
	// System.Transactions.IsolationLevel System.Transactions.Transaction::level
	int32_t ___level_1;
	// System.Transactions.TransactionInformation System.Transactions.Transaction::info
	TransactionInformation_t3294574474 * ___info_2;
	// System.Collections.ArrayList System.Transactions.Transaction::dependents
	ArrayList_t2121638921 * ___dependents_3;
	// System.Collections.Generic.List`1<System.Transactions.IEnlistmentNotification> System.Transactions.Transaction::volatiles
	List_1_t2549268686 * ___volatiles_4;
	// System.Collections.Generic.List`1<System.Transactions.ISinglePhaseNotification> System.Transactions.Transaction::durables
	List_1_t2960894218 * ___durables_5;
	// System.Transactions.Transaction/AsyncCommit System.Transactions.Transaction::asyncCommit
	AsyncCommit_t802550451 * ___asyncCommit_6;
	// System.Boolean System.Transactions.Transaction::committing
	bool ___committing_7;
	// System.Boolean System.Transactions.Transaction::committed
	bool ___committed_8;
	// System.Boolean System.Transactions.Transaction::aborted
	bool ___aborted_9;
	// System.Transactions.TransactionScope System.Transactions.Transaction::scope
	TransactionScope_t3316380210 * ___scope_10;
	// System.Exception System.Transactions.Transaction::innerException
	Exception_t1967233988 * ___innerException_11;
	// System.Transactions.TransactionCompletedEventHandler System.Transactions.Transaction::TransactionCompleted
	TransactionCompletedEventHandler_t1858411801 * ___TransactionCompleted_12;

public:
	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(Transaction_t3175846586, ___level_1)); }
	inline int32_t get_level_1() const { return ___level_1; }
	inline int32_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int32_t value)
	{
		___level_1 = value;
	}

	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(Transaction_t3175846586, ___info_2)); }
	inline TransactionInformation_t3294574474 * get_info_2() const { return ___info_2; }
	inline TransactionInformation_t3294574474 ** get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(TransactionInformation_t3294574474 * value)
	{
		___info_2 = value;
		Il2CppCodeGenWriteBarrier(&___info_2, value);
	}

	inline static int32_t get_offset_of_dependents_3() { return static_cast<int32_t>(offsetof(Transaction_t3175846586, ___dependents_3)); }
	inline ArrayList_t2121638921 * get_dependents_3() const { return ___dependents_3; }
	inline ArrayList_t2121638921 ** get_address_of_dependents_3() { return &___dependents_3; }
	inline void set_dependents_3(ArrayList_t2121638921 * value)
	{
		___dependents_3 = value;
		Il2CppCodeGenWriteBarrier(&___dependents_3, value);
	}

	inline static int32_t get_offset_of_volatiles_4() { return static_cast<int32_t>(offsetof(Transaction_t3175846586, ___volatiles_4)); }
	inline List_1_t2549268686 * get_volatiles_4() const { return ___volatiles_4; }
	inline List_1_t2549268686 ** get_address_of_volatiles_4() { return &___volatiles_4; }
	inline void set_volatiles_4(List_1_t2549268686 * value)
	{
		___volatiles_4 = value;
		Il2CppCodeGenWriteBarrier(&___volatiles_4, value);
	}

	inline static int32_t get_offset_of_durables_5() { return static_cast<int32_t>(offsetof(Transaction_t3175846586, ___durables_5)); }
	inline List_1_t2960894218 * get_durables_5() const { return ___durables_5; }
	inline List_1_t2960894218 ** get_address_of_durables_5() { return &___durables_5; }
	inline void set_durables_5(List_1_t2960894218 * value)
	{
		___durables_5 = value;
		Il2CppCodeGenWriteBarrier(&___durables_5, value);
	}

	inline static int32_t get_offset_of_asyncCommit_6() { return static_cast<int32_t>(offsetof(Transaction_t3175846586, ___asyncCommit_6)); }
	inline AsyncCommit_t802550451 * get_asyncCommit_6() const { return ___asyncCommit_6; }
	inline AsyncCommit_t802550451 ** get_address_of_asyncCommit_6() { return &___asyncCommit_6; }
	inline void set_asyncCommit_6(AsyncCommit_t802550451 * value)
	{
		___asyncCommit_6 = value;
		Il2CppCodeGenWriteBarrier(&___asyncCommit_6, value);
	}

	inline static int32_t get_offset_of_committing_7() { return static_cast<int32_t>(offsetof(Transaction_t3175846586, ___committing_7)); }
	inline bool get_committing_7() const { return ___committing_7; }
	inline bool* get_address_of_committing_7() { return &___committing_7; }
	inline void set_committing_7(bool value)
	{
		___committing_7 = value;
	}

	inline static int32_t get_offset_of_committed_8() { return static_cast<int32_t>(offsetof(Transaction_t3175846586, ___committed_8)); }
	inline bool get_committed_8() const { return ___committed_8; }
	inline bool* get_address_of_committed_8() { return &___committed_8; }
	inline void set_committed_8(bool value)
	{
		___committed_8 = value;
	}

	inline static int32_t get_offset_of_aborted_9() { return static_cast<int32_t>(offsetof(Transaction_t3175846586, ___aborted_9)); }
	inline bool get_aborted_9() const { return ___aborted_9; }
	inline bool* get_address_of_aborted_9() { return &___aborted_9; }
	inline void set_aborted_9(bool value)
	{
		___aborted_9 = value;
	}

	inline static int32_t get_offset_of_scope_10() { return static_cast<int32_t>(offsetof(Transaction_t3175846586, ___scope_10)); }
	inline TransactionScope_t3316380210 * get_scope_10() const { return ___scope_10; }
	inline TransactionScope_t3316380210 ** get_address_of_scope_10() { return &___scope_10; }
	inline void set_scope_10(TransactionScope_t3316380210 * value)
	{
		___scope_10 = value;
		Il2CppCodeGenWriteBarrier(&___scope_10, value);
	}

	inline static int32_t get_offset_of_innerException_11() { return static_cast<int32_t>(offsetof(Transaction_t3175846586, ___innerException_11)); }
	inline Exception_t1967233988 * get_innerException_11() const { return ___innerException_11; }
	inline Exception_t1967233988 ** get_address_of_innerException_11() { return &___innerException_11; }
	inline void set_innerException_11(Exception_t1967233988 * value)
	{
		___innerException_11 = value;
		Il2CppCodeGenWriteBarrier(&___innerException_11, value);
	}

	inline static int32_t get_offset_of_TransactionCompleted_12() { return static_cast<int32_t>(offsetof(Transaction_t3175846586, ___TransactionCompleted_12)); }
	inline TransactionCompletedEventHandler_t1858411801 * get_TransactionCompleted_12() const { return ___TransactionCompleted_12; }
	inline TransactionCompletedEventHandler_t1858411801 ** get_address_of_TransactionCompleted_12() { return &___TransactionCompleted_12; }
	inline void set_TransactionCompleted_12(TransactionCompletedEventHandler_t1858411801 * value)
	{
		___TransactionCompleted_12 = value;
		Il2CppCodeGenWriteBarrier(&___TransactionCompleted_12, value);
	}
};

struct Transaction_t3175846586_ThreadStaticFields
{
public:
	// System.Transactions.Transaction System.Transactions.Transaction::ambient
	Transaction_t3175846586 * ___ambient_0;

public:
	inline static int32_t get_offset_of_ambient_0() { return static_cast<int32_t>(offsetof(Transaction_t3175846586_ThreadStaticFields, ___ambient_0)); }
	inline Transaction_t3175846586 * get_ambient_0() const { return ___ambient_0; }
	inline Transaction_t3175846586 ** get_address_of_ambient_0() { return &___ambient_0; }
	inline void set_ambient_0(Transaction_t3175846586 * value)
	{
		___ambient_0 = value;
		Il2CppCodeGenWriteBarrier(&___ambient_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
