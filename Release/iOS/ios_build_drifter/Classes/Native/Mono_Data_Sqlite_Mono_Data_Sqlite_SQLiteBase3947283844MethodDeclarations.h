﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteBase
struct SQLiteBase_t3947283844;
// System.String
struct String_t;
// Mono.Data.Sqlite.SqliteConnectionHandle
struct SqliteConnectionHandle_t2345657177;
// Mono.Data.Sqlite.SqliteStatementHandle
struct SqliteStatementHandle_t1861022482;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteDateFormat4009100393.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection2345657177.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteStatementH1861022482.h"

// System.Void Mono.Data.Sqlite.SQLiteBase::.ctor(Mono.Data.Sqlite.SQLiteDateFormats)
extern "C"  void SQLiteBase__ctor_m544998933 (SQLiteBase_t3947283844 * __this, int32_t ___fmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteBase::.cctor()
extern "C"  void SQLiteBase__cctor_m2706846335 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteBase::Dispose(System.Boolean)
extern "C"  void SQLiteBase_Dispose_m1055772130 (SQLiteBase_t3947283844 * __this, bool ___bDisposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteBase::Dispose()
extern "C"  void SQLiteBase_Dispose_m4266221483 (SQLiteBase_t3947283844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLiteBase::SQLiteLastError(Mono.Data.Sqlite.SqliteConnectionHandle)
extern "C"  String_t* SQLiteBase_SQLiteLastError_m1316342210 (Il2CppObject * __this /* static, unused */, SqliteConnectionHandle_t2345657177 * ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteBase::FinalizeStatement(Mono.Data.Sqlite.SqliteStatementHandle)
extern "C"  void SQLiteBase_FinalizeStatement_m2816333211 (Il2CppObject * __this /* static, unused */, SqliteStatementHandle_t1861022482 * ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteBase::CloseConnection(Mono.Data.Sqlite.SqliteConnectionHandle)
extern "C"  void SQLiteBase_CloseConnection_m2510788405 (Il2CppObject * __this /* static, unused */, SqliteConnectionHandle_t2345657177 * ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteBase::ResetConnection(Mono.Data.Sqlite.SqliteConnectionHandle)
extern "C"  void SQLiteBase_ResetConnection_m3849416318 (Il2CppObject * __this /* static, unused */, SqliteConnectionHandle_t2345657177 * ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
