﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.MissingPrimaryKeyException
struct MissingPrimaryKeyException_t3389106783;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void System.Data.MissingPrimaryKeyException::.ctor()
extern "C"  void MissingPrimaryKeyException__ctor_m2673614597 (MissingPrimaryKeyException_t3389106783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.MissingPrimaryKeyException::.ctor(System.String)
extern "C"  void MissingPrimaryKeyException__ctor_m1363464221 (MissingPrimaryKeyException_t3389106783 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.MissingPrimaryKeyException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void MissingPrimaryKeyException__ctor_m2146447046 (MissingPrimaryKeyException_t3389106783 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
