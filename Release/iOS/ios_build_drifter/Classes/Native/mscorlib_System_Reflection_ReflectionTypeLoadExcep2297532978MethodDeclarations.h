﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.ReflectionTypeLoadException
struct ReflectionTypeLoadException_t2297532978;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Type[]
struct TypeU5BU5D_t3431720054;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void System.Reflection.ReflectionTypeLoadException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void ReflectionTypeLoadException__ctor_m2363984437 (ReflectionTypeLoadException_t2297532978 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___sc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.ReflectionTypeLoadException::get_Types()
extern "C"  TypeU5BU5D_t3431720054* ReflectionTypeLoadException_get_Types_m214586650 (ReflectionTypeLoadException_t2297532978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ReflectionTypeLoadException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void ReflectionTypeLoadException_GetObjectData_m3425291410 (ReflectionTypeLoadException_t2297532978 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
