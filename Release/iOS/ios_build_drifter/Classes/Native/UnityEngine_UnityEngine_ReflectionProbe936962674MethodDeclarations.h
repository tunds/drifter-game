﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ReflectionProbe
struct ReflectionProbe_t936962674;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeM1822783935.h"

// System.Void UnityEngine.ReflectionProbe::set_mode(UnityEngine.Rendering.ReflectionProbeMode)
extern "C"  void ReflectionProbe_set_mode_m2850534931 (ReflectionProbe_t936962674 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
