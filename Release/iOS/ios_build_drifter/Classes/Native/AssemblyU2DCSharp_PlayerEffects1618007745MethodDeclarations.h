﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerEffects
struct PlayerEffects_t1618007745;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayerEffects::.ctor()
extern "C"  void PlayerEffects__ctor_m2486735818 (PlayerEffects_t1618007745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEffects::Awake()
extern "C"  void PlayerEffects_Awake_m2724341037 (PlayerEffects_t1618007745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEffects::showIdleEffect(System.String)
extern "C"  void PlayerEffects_showIdleEffect_m3987524294 (PlayerEffects_t1618007745 * __this, String_t* ___typeOfIdle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEffects::showWalkingEffect(System.String)
extern "C"  void PlayerEffects_showWalkingEffect_m3667812717 (PlayerEffects_t1618007745 * __this, String_t* ___typeOfWalk0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEffects::showStunEffect()
extern "C"  void PlayerEffects_showStunEffect_m2027784354 (PlayerEffects_t1618007745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEffects::showHitEffect()
extern "C"  void PlayerEffects_showHitEffect_m751940815 (PlayerEffects_t1618007745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEffects::showNormalAttackEffect(System.String)
extern "C"  void PlayerEffects_showNormalAttackEffect_m3980383595 (PlayerEffects_t1618007745 * __this, String_t* ___typeOfAttack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEffects::showSpecialAttackEffect(System.String)
extern "C"  void PlayerEffects_showSpecialAttackEffect_m2043161349 (PlayerEffects_t1618007745 * __this, String_t* ___typeOfAttack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
