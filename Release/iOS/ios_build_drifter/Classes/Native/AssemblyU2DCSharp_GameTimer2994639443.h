﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayerHealth
struct PlayerHealth_t3877793981;
// GlobalObjectManager
struct GlobalObjectManager_t849077355;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameTimer
struct  GameTimer_t2994639443  : public MonoBehaviour_t3012272455
{
public:
	// System.DateTime GameTimer::startTime
	DateTime_t339033936  ___startTime_2;
	// System.TimeSpan GameTimer::elapsedTime
	TimeSpan_t763862892  ___elapsedTime_3;
	// System.Single GameTimer::time
	float ___time_4;
	// PlayerHealth GameTimer::playerHealth
	PlayerHealth_t3877793981 * ___playerHealth_5;
	// GlobalObjectManager GameTimer::persistentData
	GlobalObjectManager_t849077355 * ___persistentData_6;

public:
	inline static int32_t get_offset_of_startTime_2() { return static_cast<int32_t>(offsetof(GameTimer_t2994639443, ___startTime_2)); }
	inline DateTime_t339033936  get_startTime_2() const { return ___startTime_2; }
	inline DateTime_t339033936 * get_address_of_startTime_2() { return &___startTime_2; }
	inline void set_startTime_2(DateTime_t339033936  value)
	{
		___startTime_2 = value;
	}

	inline static int32_t get_offset_of_elapsedTime_3() { return static_cast<int32_t>(offsetof(GameTimer_t2994639443, ___elapsedTime_3)); }
	inline TimeSpan_t763862892  get_elapsedTime_3() const { return ___elapsedTime_3; }
	inline TimeSpan_t763862892 * get_address_of_elapsedTime_3() { return &___elapsedTime_3; }
	inline void set_elapsedTime_3(TimeSpan_t763862892  value)
	{
		___elapsedTime_3 = value;
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(GameTimer_t2994639443, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}

	inline static int32_t get_offset_of_playerHealth_5() { return static_cast<int32_t>(offsetof(GameTimer_t2994639443, ___playerHealth_5)); }
	inline PlayerHealth_t3877793981 * get_playerHealth_5() const { return ___playerHealth_5; }
	inline PlayerHealth_t3877793981 ** get_address_of_playerHealth_5() { return &___playerHealth_5; }
	inline void set_playerHealth_5(PlayerHealth_t3877793981 * value)
	{
		___playerHealth_5 = value;
		Il2CppCodeGenWriteBarrier(&___playerHealth_5, value);
	}

	inline static int32_t get_offset_of_persistentData_6() { return static_cast<int32_t>(offsetof(GameTimer_t2994639443, ___persistentData_6)); }
	inline GlobalObjectManager_t849077355 * get_persistentData_6() const { return ___persistentData_6; }
	inline GlobalObjectManager_t849077355 ** get_address_of_persistentData_6() { return &___persistentData_6; }
	inline void set_persistentData_6(GlobalObjectManager_t849077355 * value)
	{
		___persistentData_6 = value;
		Il2CppCodeGenWriteBarrier(&___persistentData_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
