﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Data.StateChangeEventHandler
struct StateChangeEventHandler_t2980893476;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "System_System_ComponentModel_Component553679750.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.DbConnection
struct  DbConnection_t462757452  : public Component_t553679750
{
public:
	// System.Data.StateChangeEventHandler System.Data.Common.DbConnection::StateChange
	StateChangeEventHandler_t2980893476 * ___StateChange_4;

public:
	inline static int32_t get_offset_of_StateChange_4() { return static_cast<int32_t>(offsetof(DbConnection_t462757452, ___StateChange_4)); }
	inline StateChangeEventHandler_t2980893476 * get_StateChange_4() const { return ___StateChange_4; }
	inline StateChangeEventHandler_t2980893476 ** get_address_of_StateChange_4() { return &___StateChange_4; }
	inline void set_StateChange_4(StateChangeEventHandler_t2980893476 * value)
	{
		___StateChange_4 = value;
		Il2CppCodeGenWriteBarrier(&___StateChange_4, value);
	}
};

struct DbConnection_t462757452_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Data.Common.DbConnection::<>f__switch$map1
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map1_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_5() { return static_cast<int32_t>(offsetof(DbConnection_t462757452_StaticFields, ___U3CU3Ef__switchU24map1_5)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map1_5() const { return ___U3CU3Ef__switchU24map1_5; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map1_5() { return &___U3CU3Ef__switchU24map1_5; }
	inline void set_U3CU3Ef__switchU24map1_5(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
