﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Data.DataRow[]
struct DataRowU5BU5D_t1036778418;
// System.IO.TextWriter
struct TextWriter_t1689927879;
// Mono.Data.SqlExpressions.yydebug.yyDebug
struct yyDebug_t3576773820;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Object
struct Il2CppObject;
// System.Int16[]
struct Int16U5BU5D_t3675865332;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.Parser
struct  Parser_t3728723425  : public Il2CppObject
{
public:
	// System.Boolean Mono.Data.SqlExpressions.Parser::cacheAggregationResults
	bool ___cacheAggregationResults_0;
	// System.Data.DataRow[] Mono.Data.SqlExpressions.Parser::aggregationRows
	DataRowU5BU5D_t1036778418* ___aggregationRows_1;
	// System.IO.TextWriter Mono.Data.SqlExpressions.Parser::ErrorOutput
	TextWriter_t1689927879 * ___ErrorOutput_3;
	// System.Int32 Mono.Data.SqlExpressions.Parser::eof_token
	int32_t ___eof_token_4;
	// Mono.Data.SqlExpressions.yydebug.yyDebug Mono.Data.SqlExpressions.Parser::debug
	Il2CppObject * ___debug_5;
	// System.Int32 Mono.Data.SqlExpressions.Parser::yyExpectingState
	int32_t ___yyExpectingState_7;
	// System.Int32 Mono.Data.SqlExpressions.Parser::yyMax
	int32_t ___yyMax_8;
	// System.Boolean Mono.Data.SqlExpressions.Parser::use_global_stacks
	bool ___use_global_stacks_11;
	// System.Object[] Mono.Data.SqlExpressions.Parser::yyVals
	ObjectU5BU5D_t11523773* ___yyVals_12;
	// System.Object Mono.Data.SqlExpressions.Parser::yyVal
	Il2CppObject * ___yyVal_13;
	// System.Int32 Mono.Data.SqlExpressions.Parser::yyToken
	int32_t ___yyToken_14;
	// System.Int32 Mono.Data.SqlExpressions.Parser::yyTop
	int32_t ___yyTop_15;

public:
	inline static int32_t get_offset_of_cacheAggregationResults_0() { return static_cast<int32_t>(offsetof(Parser_t3728723425, ___cacheAggregationResults_0)); }
	inline bool get_cacheAggregationResults_0() const { return ___cacheAggregationResults_0; }
	inline bool* get_address_of_cacheAggregationResults_0() { return &___cacheAggregationResults_0; }
	inline void set_cacheAggregationResults_0(bool value)
	{
		___cacheAggregationResults_0 = value;
	}

	inline static int32_t get_offset_of_aggregationRows_1() { return static_cast<int32_t>(offsetof(Parser_t3728723425, ___aggregationRows_1)); }
	inline DataRowU5BU5D_t1036778418* get_aggregationRows_1() const { return ___aggregationRows_1; }
	inline DataRowU5BU5D_t1036778418** get_address_of_aggregationRows_1() { return &___aggregationRows_1; }
	inline void set_aggregationRows_1(DataRowU5BU5D_t1036778418* value)
	{
		___aggregationRows_1 = value;
		Il2CppCodeGenWriteBarrier(&___aggregationRows_1, value);
	}

	inline static int32_t get_offset_of_ErrorOutput_3() { return static_cast<int32_t>(offsetof(Parser_t3728723425, ___ErrorOutput_3)); }
	inline TextWriter_t1689927879 * get_ErrorOutput_3() const { return ___ErrorOutput_3; }
	inline TextWriter_t1689927879 ** get_address_of_ErrorOutput_3() { return &___ErrorOutput_3; }
	inline void set_ErrorOutput_3(TextWriter_t1689927879 * value)
	{
		___ErrorOutput_3 = value;
		Il2CppCodeGenWriteBarrier(&___ErrorOutput_3, value);
	}

	inline static int32_t get_offset_of_eof_token_4() { return static_cast<int32_t>(offsetof(Parser_t3728723425, ___eof_token_4)); }
	inline int32_t get_eof_token_4() const { return ___eof_token_4; }
	inline int32_t* get_address_of_eof_token_4() { return &___eof_token_4; }
	inline void set_eof_token_4(int32_t value)
	{
		___eof_token_4 = value;
	}

	inline static int32_t get_offset_of_debug_5() { return static_cast<int32_t>(offsetof(Parser_t3728723425, ___debug_5)); }
	inline Il2CppObject * get_debug_5() const { return ___debug_5; }
	inline Il2CppObject ** get_address_of_debug_5() { return &___debug_5; }
	inline void set_debug_5(Il2CppObject * value)
	{
		___debug_5 = value;
		Il2CppCodeGenWriteBarrier(&___debug_5, value);
	}

	inline static int32_t get_offset_of_yyExpectingState_7() { return static_cast<int32_t>(offsetof(Parser_t3728723425, ___yyExpectingState_7)); }
	inline int32_t get_yyExpectingState_7() const { return ___yyExpectingState_7; }
	inline int32_t* get_address_of_yyExpectingState_7() { return &___yyExpectingState_7; }
	inline void set_yyExpectingState_7(int32_t value)
	{
		___yyExpectingState_7 = value;
	}

	inline static int32_t get_offset_of_yyMax_8() { return static_cast<int32_t>(offsetof(Parser_t3728723425, ___yyMax_8)); }
	inline int32_t get_yyMax_8() const { return ___yyMax_8; }
	inline int32_t* get_address_of_yyMax_8() { return &___yyMax_8; }
	inline void set_yyMax_8(int32_t value)
	{
		___yyMax_8 = value;
	}

	inline static int32_t get_offset_of_use_global_stacks_11() { return static_cast<int32_t>(offsetof(Parser_t3728723425, ___use_global_stacks_11)); }
	inline bool get_use_global_stacks_11() const { return ___use_global_stacks_11; }
	inline bool* get_address_of_use_global_stacks_11() { return &___use_global_stacks_11; }
	inline void set_use_global_stacks_11(bool value)
	{
		___use_global_stacks_11 = value;
	}

	inline static int32_t get_offset_of_yyVals_12() { return static_cast<int32_t>(offsetof(Parser_t3728723425, ___yyVals_12)); }
	inline ObjectU5BU5D_t11523773* get_yyVals_12() const { return ___yyVals_12; }
	inline ObjectU5BU5D_t11523773** get_address_of_yyVals_12() { return &___yyVals_12; }
	inline void set_yyVals_12(ObjectU5BU5D_t11523773* value)
	{
		___yyVals_12 = value;
		Il2CppCodeGenWriteBarrier(&___yyVals_12, value);
	}

	inline static int32_t get_offset_of_yyVal_13() { return static_cast<int32_t>(offsetof(Parser_t3728723425, ___yyVal_13)); }
	inline Il2CppObject * get_yyVal_13() const { return ___yyVal_13; }
	inline Il2CppObject ** get_address_of_yyVal_13() { return &___yyVal_13; }
	inline void set_yyVal_13(Il2CppObject * value)
	{
		___yyVal_13 = value;
		Il2CppCodeGenWriteBarrier(&___yyVal_13, value);
	}

	inline static int32_t get_offset_of_yyToken_14() { return static_cast<int32_t>(offsetof(Parser_t3728723425, ___yyToken_14)); }
	inline int32_t get_yyToken_14() const { return ___yyToken_14; }
	inline int32_t* get_address_of_yyToken_14() { return &___yyToken_14; }
	inline void set_yyToken_14(int32_t value)
	{
		___yyToken_14 = value;
	}

	inline static int32_t get_offset_of_yyTop_15() { return static_cast<int32_t>(offsetof(Parser_t3728723425, ___yyTop_15)); }
	inline int32_t get_yyTop_15() const { return ___yyTop_15; }
	inline int32_t* get_address_of_yyTop_15() { return &___yyTop_15; }
	inline void set_yyTop_15(int32_t value)
	{
		___yyTop_15 = value;
	}
};

struct Parser_t3728723425_StaticFields
{
public:
	// System.Int32 Mono.Data.SqlExpressions.Parser::yacc_verbose_flag
	int32_t ___yacc_verbose_flag_2;
	// System.String[] Mono.Data.SqlExpressions.Parser::yyNames
	StringU5BU5D_t2956870243* ___yyNames_6;
	// System.Int32[] Mono.Data.SqlExpressions.Parser::global_yyStates
	Int32U5BU5D_t1809983122* ___global_yyStates_9;
	// System.Object[] Mono.Data.SqlExpressions.Parser::global_yyVals
	ObjectU5BU5D_t11523773* ___global_yyVals_10;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyLhs
	Int16U5BU5D_t3675865332* ___yyLhs_16;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyLen
	Int16U5BU5D_t3675865332* ___yyLen_17;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyDefRed
	Int16U5BU5D_t3675865332* ___yyDefRed_18;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyDgoto
	Int16U5BU5D_t3675865332* ___yyDgoto_19;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yySindex
	Int16U5BU5D_t3675865332* ___yySindex_20;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyRindex
	Int16U5BU5D_t3675865332* ___yyRindex_21;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyGindex
	Int16U5BU5D_t3675865332* ___yyGindex_22;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyTable
	Int16U5BU5D_t3675865332* ___yyTable_23;
	// System.Int16[] Mono.Data.SqlExpressions.Parser::yyCheck
	Int16U5BU5D_t3675865332* ___yyCheck_24;

public:
	inline static int32_t get_offset_of_yacc_verbose_flag_2() { return static_cast<int32_t>(offsetof(Parser_t3728723425_StaticFields, ___yacc_verbose_flag_2)); }
	inline int32_t get_yacc_verbose_flag_2() const { return ___yacc_verbose_flag_2; }
	inline int32_t* get_address_of_yacc_verbose_flag_2() { return &___yacc_verbose_flag_2; }
	inline void set_yacc_verbose_flag_2(int32_t value)
	{
		___yacc_verbose_flag_2 = value;
	}

	inline static int32_t get_offset_of_yyNames_6() { return static_cast<int32_t>(offsetof(Parser_t3728723425_StaticFields, ___yyNames_6)); }
	inline StringU5BU5D_t2956870243* get_yyNames_6() const { return ___yyNames_6; }
	inline StringU5BU5D_t2956870243** get_address_of_yyNames_6() { return &___yyNames_6; }
	inline void set_yyNames_6(StringU5BU5D_t2956870243* value)
	{
		___yyNames_6 = value;
		Il2CppCodeGenWriteBarrier(&___yyNames_6, value);
	}

	inline static int32_t get_offset_of_global_yyStates_9() { return static_cast<int32_t>(offsetof(Parser_t3728723425_StaticFields, ___global_yyStates_9)); }
	inline Int32U5BU5D_t1809983122* get_global_yyStates_9() const { return ___global_yyStates_9; }
	inline Int32U5BU5D_t1809983122** get_address_of_global_yyStates_9() { return &___global_yyStates_9; }
	inline void set_global_yyStates_9(Int32U5BU5D_t1809983122* value)
	{
		___global_yyStates_9 = value;
		Il2CppCodeGenWriteBarrier(&___global_yyStates_9, value);
	}

	inline static int32_t get_offset_of_global_yyVals_10() { return static_cast<int32_t>(offsetof(Parser_t3728723425_StaticFields, ___global_yyVals_10)); }
	inline ObjectU5BU5D_t11523773* get_global_yyVals_10() const { return ___global_yyVals_10; }
	inline ObjectU5BU5D_t11523773** get_address_of_global_yyVals_10() { return &___global_yyVals_10; }
	inline void set_global_yyVals_10(ObjectU5BU5D_t11523773* value)
	{
		___global_yyVals_10 = value;
		Il2CppCodeGenWriteBarrier(&___global_yyVals_10, value);
	}

	inline static int32_t get_offset_of_yyLhs_16() { return static_cast<int32_t>(offsetof(Parser_t3728723425_StaticFields, ___yyLhs_16)); }
	inline Int16U5BU5D_t3675865332* get_yyLhs_16() const { return ___yyLhs_16; }
	inline Int16U5BU5D_t3675865332** get_address_of_yyLhs_16() { return &___yyLhs_16; }
	inline void set_yyLhs_16(Int16U5BU5D_t3675865332* value)
	{
		___yyLhs_16 = value;
		Il2CppCodeGenWriteBarrier(&___yyLhs_16, value);
	}

	inline static int32_t get_offset_of_yyLen_17() { return static_cast<int32_t>(offsetof(Parser_t3728723425_StaticFields, ___yyLen_17)); }
	inline Int16U5BU5D_t3675865332* get_yyLen_17() const { return ___yyLen_17; }
	inline Int16U5BU5D_t3675865332** get_address_of_yyLen_17() { return &___yyLen_17; }
	inline void set_yyLen_17(Int16U5BU5D_t3675865332* value)
	{
		___yyLen_17 = value;
		Il2CppCodeGenWriteBarrier(&___yyLen_17, value);
	}

	inline static int32_t get_offset_of_yyDefRed_18() { return static_cast<int32_t>(offsetof(Parser_t3728723425_StaticFields, ___yyDefRed_18)); }
	inline Int16U5BU5D_t3675865332* get_yyDefRed_18() const { return ___yyDefRed_18; }
	inline Int16U5BU5D_t3675865332** get_address_of_yyDefRed_18() { return &___yyDefRed_18; }
	inline void set_yyDefRed_18(Int16U5BU5D_t3675865332* value)
	{
		___yyDefRed_18 = value;
		Il2CppCodeGenWriteBarrier(&___yyDefRed_18, value);
	}

	inline static int32_t get_offset_of_yyDgoto_19() { return static_cast<int32_t>(offsetof(Parser_t3728723425_StaticFields, ___yyDgoto_19)); }
	inline Int16U5BU5D_t3675865332* get_yyDgoto_19() const { return ___yyDgoto_19; }
	inline Int16U5BU5D_t3675865332** get_address_of_yyDgoto_19() { return &___yyDgoto_19; }
	inline void set_yyDgoto_19(Int16U5BU5D_t3675865332* value)
	{
		___yyDgoto_19 = value;
		Il2CppCodeGenWriteBarrier(&___yyDgoto_19, value);
	}

	inline static int32_t get_offset_of_yySindex_20() { return static_cast<int32_t>(offsetof(Parser_t3728723425_StaticFields, ___yySindex_20)); }
	inline Int16U5BU5D_t3675865332* get_yySindex_20() const { return ___yySindex_20; }
	inline Int16U5BU5D_t3675865332** get_address_of_yySindex_20() { return &___yySindex_20; }
	inline void set_yySindex_20(Int16U5BU5D_t3675865332* value)
	{
		___yySindex_20 = value;
		Il2CppCodeGenWriteBarrier(&___yySindex_20, value);
	}

	inline static int32_t get_offset_of_yyRindex_21() { return static_cast<int32_t>(offsetof(Parser_t3728723425_StaticFields, ___yyRindex_21)); }
	inline Int16U5BU5D_t3675865332* get_yyRindex_21() const { return ___yyRindex_21; }
	inline Int16U5BU5D_t3675865332** get_address_of_yyRindex_21() { return &___yyRindex_21; }
	inline void set_yyRindex_21(Int16U5BU5D_t3675865332* value)
	{
		___yyRindex_21 = value;
		Il2CppCodeGenWriteBarrier(&___yyRindex_21, value);
	}

	inline static int32_t get_offset_of_yyGindex_22() { return static_cast<int32_t>(offsetof(Parser_t3728723425_StaticFields, ___yyGindex_22)); }
	inline Int16U5BU5D_t3675865332* get_yyGindex_22() const { return ___yyGindex_22; }
	inline Int16U5BU5D_t3675865332** get_address_of_yyGindex_22() { return &___yyGindex_22; }
	inline void set_yyGindex_22(Int16U5BU5D_t3675865332* value)
	{
		___yyGindex_22 = value;
		Il2CppCodeGenWriteBarrier(&___yyGindex_22, value);
	}

	inline static int32_t get_offset_of_yyTable_23() { return static_cast<int32_t>(offsetof(Parser_t3728723425_StaticFields, ___yyTable_23)); }
	inline Int16U5BU5D_t3675865332* get_yyTable_23() const { return ___yyTable_23; }
	inline Int16U5BU5D_t3675865332** get_address_of_yyTable_23() { return &___yyTable_23; }
	inline void set_yyTable_23(Int16U5BU5D_t3675865332* value)
	{
		___yyTable_23 = value;
		Il2CppCodeGenWriteBarrier(&___yyTable_23, value);
	}

	inline static int32_t get_offset_of_yyCheck_24() { return static_cast<int32_t>(offsetof(Parser_t3728723425_StaticFields, ___yyCheck_24)); }
	inline Int16U5BU5D_t3675865332* get_yyCheck_24() const { return ___yyCheck_24; }
	inline Int16U5BU5D_t3675865332** get_address_of_yyCheck_24() { return &___yyCheck_24; }
	inline void set_yyCheck_24(Int16U5BU5D_t3675865332* value)
	{
		___yyCheck_24 = value;
		Il2CppCodeGenWriteBarrier(&___yyCheck_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
