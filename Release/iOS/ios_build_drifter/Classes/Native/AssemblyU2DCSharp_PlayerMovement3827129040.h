﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animation
struct Animation_t350396337;
// UnityEngine.CharacterController
struct CharacterController_t2029520850;
// PlayerEffects
struct PlayerEffects_t1618007745;
// PlayerPickups
struct PlayerPickups_t2873926454;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMovement
struct  PlayerMovement_t3827129040  : public MonoBehaviour_t3012272455
{
public:
	// System.Single PlayerMovement::speed
	float ___speed_2;
	// System.Single PlayerMovement::playerRotation
	float ___playerRotation_3;
	// UnityEngine.Vector3 PlayerMovement::movementDirection
	Vector3_t3525329789  ___movementDirection_4;
	// UnityEngine.Vector3 PlayerMovement::movement
	Vector3_t3525329789  ___movement_5;
	// UnityEngine.Animation PlayerMovement::playerAnimation
	Animation_t350396337 * ___playerAnimation_6;
	// UnityEngine.CharacterController PlayerMovement::playerCharacterController
	CharacterController_t2029520850 * ___playerCharacterController_7;
	// UnityEngine.Quaternion PlayerMovement::rotation
	Quaternion_t1891715979  ___rotation_8;
	// PlayerEffects PlayerMovement::playerEffects
	PlayerEffects_t1618007745 * ___playerEffects_9;
	// PlayerPickups PlayerMovement::playerPickups
	PlayerPickups_t2873926454 * ___playerPickups_10;
	// System.Boolean PlayerMovement::isAttacking
	bool ___isAttacking_11;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(PlayerMovement_t3827129040, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_playerRotation_3() { return static_cast<int32_t>(offsetof(PlayerMovement_t3827129040, ___playerRotation_3)); }
	inline float get_playerRotation_3() const { return ___playerRotation_3; }
	inline float* get_address_of_playerRotation_3() { return &___playerRotation_3; }
	inline void set_playerRotation_3(float value)
	{
		___playerRotation_3 = value;
	}

	inline static int32_t get_offset_of_movementDirection_4() { return static_cast<int32_t>(offsetof(PlayerMovement_t3827129040, ___movementDirection_4)); }
	inline Vector3_t3525329789  get_movementDirection_4() const { return ___movementDirection_4; }
	inline Vector3_t3525329789 * get_address_of_movementDirection_4() { return &___movementDirection_4; }
	inline void set_movementDirection_4(Vector3_t3525329789  value)
	{
		___movementDirection_4 = value;
	}

	inline static int32_t get_offset_of_movement_5() { return static_cast<int32_t>(offsetof(PlayerMovement_t3827129040, ___movement_5)); }
	inline Vector3_t3525329789  get_movement_5() const { return ___movement_5; }
	inline Vector3_t3525329789 * get_address_of_movement_5() { return &___movement_5; }
	inline void set_movement_5(Vector3_t3525329789  value)
	{
		___movement_5 = value;
	}

	inline static int32_t get_offset_of_playerAnimation_6() { return static_cast<int32_t>(offsetof(PlayerMovement_t3827129040, ___playerAnimation_6)); }
	inline Animation_t350396337 * get_playerAnimation_6() const { return ___playerAnimation_6; }
	inline Animation_t350396337 ** get_address_of_playerAnimation_6() { return &___playerAnimation_6; }
	inline void set_playerAnimation_6(Animation_t350396337 * value)
	{
		___playerAnimation_6 = value;
		Il2CppCodeGenWriteBarrier(&___playerAnimation_6, value);
	}

	inline static int32_t get_offset_of_playerCharacterController_7() { return static_cast<int32_t>(offsetof(PlayerMovement_t3827129040, ___playerCharacterController_7)); }
	inline CharacterController_t2029520850 * get_playerCharacterController_7() const { return ___playerCharacterController_7; }
	inline CharacterController_t2029520850 ** get_address_of_playerCharacterController_7() { return &___playerCharacterController_7; }
	inline void set_playerCharacterController_7(CharacterController_t2029520850 * value)
	{
		___playerCharacterController_7 = value;
		Il2CppCodeGenWriteBarrier(&___playerCharacterController_7, value);
	}

	inline static int32_t get_offset_of_rotation_8() { return static_cast<int32_t>(offsetof(PlayerMovement_t3827129040, ___rotation_8)); }
	inline Quaternion_t1891715979  get_rotation_8() const { return ___rotation_8; }
	inline Quaternion_t1891715979 * get_address_of_rotation_8() { return &___rotation_8; }
	inline void set_rotation_8(Quaternion_t1891715979  value)
	{
		___rotation_8 = value;
	}

	inline static int32_t get_offset_of_playerEffects_9() { return static_cast<int32_t>(offsetof(PlayerMovement_t3827129040, ___playerEffects_9)); }
	inline PlayerEffects_t1618007745 * get_playerEffects_9() const { return ___playerEffects_9; }
	inline PlayerEffects_t1618007745 ** get_address_of_playerEffects_9() { return &___playerEffects_9; }
	inline void set_playerEffects_9(PlayerEffects_t1618007745 * value)
	{
		___playerEffects_9 = value;
		Il2CppCodeGenWriteBarrier(&___playerEffects_9, value);
	}

	inline static int32_t get_offset_of_playerPickups_10() { return static_cast<int32_t>(offsetof(PlayerMovement_t3827129040, ___playerPickups_10)); }
	inline PlayerPickups_t2873926454 * get_playerPickups_10() const { return ___playerPickups_10; }
	inline PlayerPickups_t2873926454 ** get_address_of_playerPickups_10() { return &___playerPickups_10; }
	inline void set_playerPickups_10(PlayerPickups_t2873926454 * value)
	{
		___playerPickups_10 = value;
		Il2CppCodeGenWriteBarrier(&___playerPickups_10, value);
	}

	inline static int32_t get_offset_of_isAttacking_11() { return static_cast<int32_t>(offsetof(PlayerMovement_t3827129040, ___isAttacking_11)); }
	inline bool get_isAttacking_11() const { return ___isAttacking_11; }
	inline bool* get_address_of_isAttacking_11() { return &___isAttacking_11; }
	inline void set_isAttacking_11(bool value)
	{
		___isAttacking_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
