﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.Int16DataContainer
struct Int16DataContainer_t943675987;
// System.Object
struct Il2CppObject;
// System.Data.ISafeDataRecord
struct ISafeDataRecord_t3927591524;
// System.Data.Common.DataContainer
struct DataContainer_t1942492167;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Data_System_Data_Common_DataContainer1942492167.h"

// System.Void System.Data.Common.Int16DataContainer::.ctor()
extern "C"  void Int16DataContainer__ctor_m2861718767 (Int16DataContainer_t943675987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Data.Common.Int16DataContainer::GetValue(System.Int32)
extern "C"  Il2CppObject * Int16DataContainer_GetValue_m1170860908 (Int16DataContainer_t943675987 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.Int16DataContainer::ZeroOut(System.Int32)
extern "C"  void Int16DataContainer_ZeroOut_m2557636068 (Int16DataContainer_t943675987 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.Int16DataContainer::SetValue(System.Int32,System.Object)
extern "C"  void Int16DataContainer_SetValue_m292579395 (Int16DataContainer_t943675987 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.Int16DataContainer::SetValueFromSafeDataRecord(System.Int32,System.Data.ISafeDataRecord,System.Int32)
extern "C"  void Int16DataContainer_SetValueFromSafeDataRecord_m3499951088 (Int16DataContainer_t943675987 * __this, int32_t ___index0, Il2CppObject * ___record1, int32_t ___field2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.Int16DataContainer::DoCopyValue(System.Data.Common.DataContainer,System.Int32,System.Int32)
extern "C"  void Int16DataContainer_DoCopyValue_m3238239685 (Int16DataContainer_t943675987 * __this, DataContainer_t1942492167 * ___from0, int32_t ___from_index1, int32_t ___to_index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.Int16DataContainer::DoCompareValues(System.Int32,System.Int32)
extern "C"  int32_t Int16DataContainer_DoCompareValues_m2935308857 (Int16DataContainer_t943675987 * __this, int32_t ___index10, int32_t ___index21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.Int16DataContainer::Resize(System.Int32)
extern "C"  void Int16DataContainer_Resize_m2285857018 (Int16DataContainer_t943675987 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Data.Common.Int16DataContainer::GetInt64(System.Int32)
extern "C"  int64_t Int16DataContainer_GetInt64_m354376696 (Int16DataContainer_t943675987 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
