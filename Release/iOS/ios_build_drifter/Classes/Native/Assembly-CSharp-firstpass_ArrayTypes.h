﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// CnControls.VirtualAxis
struct VirtualAxis_t3870720551;
// CnControls.VirtualButton
struct VirtualButton_t3983641336;
// CnControls.DpadAxis
struct DpadAxis_t639718155;
// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
struct AxisTouchButton_t1467159680;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct VirtualAxis_t675652908;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
struct VirtualButton_t788573693;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
struct ReplacementDefinition_t3635855589;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct Entry_t67115092;

#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_VirtualAx3870720551.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_VirtualBu3983641336.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_DpadAxis639718155.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1467159680.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C675652908.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C788573693.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3635855589.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Ut67115090.h"

#pragma once
// CnControls.VirtualAxis[]
struct VirtualAxisU5BU5D_t982779230  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) VirtualAxis_t3870720551 * m_Items[1];

public:
	inline VirtualAxis_t3870720551 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VirtualAxis_t3870720551 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VirtualAxis_t3870720551 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CnControls.VirtualButton[]
struct VirtualButtonU5BU5D_t3494040105  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) VirtualButton_t3983641336 * m_Items[1];

public:
	inline VirtualButton_t3983641336 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VirtualButton_t3983641336 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VirtualButton_t3983641336 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CnControls.DpadAxis[]
struct DpadAxisU5BU5D_t30212202  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DpadAxis_t639718155 * m_Items[1];

public:
	inline DpadAxis_t639718155 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DpadAxis_t639718155 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DpadAxis_t639718155 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityStandardAssets.CrossPlatformInput.AxisTouchButton[]
struct AxisTouchButtonU5BU5D_t2808842625  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) AxisTouchButton_t1467159680 * m_Items[1];

public:
	inline AxisTouchButton_t1467159680 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AxisTouchButton_t1467159680 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AxisTouchButton_t1467159680 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis[]
struct VirtualAxisU5BU5D_t2735875237  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) VirtualAxis_t675652908 * m_Items[1];

public:
	inline VirtualAxis_t675652908 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VirtualAxis_t675652908 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VirtualAxis_t675652908 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton[]
struct VirtualButtonU5BU5D_t952168816  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) VirtualButton_t788573693 * m_Items[1];

public:
	inline VirtualButton_t788573693 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VirtualButton_t788573693 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VirtualButton_t788573693 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[]
struct ReplacementDefinitionU5BU5D_t1980362216  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ReplacementDefinition_t3635855589 * m_Items[1];

public:
	inline ReplacementDefinition_t3635855589 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ReplacementDefinition_t3635855589 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ReplacementDefinition_t3635855589 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityStandardAssets.Utility.TimedObjectActivator/Entry[]
struct EntryU5BU5D_t864858664  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Entry_t67115092 * m_Items[1];

public:
	inline Entry_t67115092 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Entry_t67115092 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Entry_t67115092 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
