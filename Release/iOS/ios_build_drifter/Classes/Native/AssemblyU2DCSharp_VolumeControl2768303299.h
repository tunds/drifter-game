﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalObjectManager
struct GlobalObjectManager_t849077355;
// UnityEngine.UI.Slider
struct Slider_t1468074762;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VolumeControl
struct  VolumeControl_t2768303299  : public MonoBehaviour_t3012272455
{
public:
	// GlobalObjectManager VolumeControl::persistentData
	GlobalObjectManager_t849077355 * ___persistentData_2;
	// UnityEngine.UI.Slider VolumeControl::volumeSldr
	Slider_t1468074762 * ___volumeSldr_3;

public:
	inline static int32_t get_offset_of_persistentData_2() { return static_cast<int32_t>(offsetof(VolumeControl_t2768303299, ___persistentData_2)); }
	inline GlobalObjectManager_t849077355 * get_persistentData_2() const { return ___persistentData_2; }
	inline GlobalObjectManager_t849077355 ** get_address_of_persistentData_2() { return &___persistentData_2; }
	inline void set_persistentData_2(GlobalObjectManager_t849077355 * value)
	{
		___persistentData_2 = value;
		Il2CppCodeGenWriteBarrier(&___persistentData_2, value);
	}

	inline static int32_t get_offset_of_volumeSldr_3() { return static_cast<int32_t>(offsetof(VolumeControl_t2768303299, ___volumeSldr_3)); }
	inline Slider_t1468074762 * get_volumeSldr_3() const { return ___volumeSldr_3; }
	inline Slider_t1468074762 ** get_address_of_volumeSldr_3() { return &___volumeSldr_3; }
	inline void set_volumeSldr_3(Slider_t1468074762 * value)
	{
		___volumeSldr_3 = value;
		Il2CppCodeGenWriteBarrier(&___volumeSldr_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
