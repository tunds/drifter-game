﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct List_1_t1645832326;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4026582614.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K848873357.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3974369406_gshared (Enumerator_t4026582614 * __this, List_1_t1645832326 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3974369406(__this, ___l0, method) ((  void (*) (Enumerator_t4026582614 *, List_1_t1645832326 *, const MethodInfo*))Enumerator__ctor_m3974369406_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4073073940_gshared (Enumerator_t4026582614 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4073073940(__this, method) ((  void (*) (Enumerator_t4026582614 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4073073940_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m397134346_gshared (Enumerator_t4026582614 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m397134346(__this, method) ((  Il2CppObject * (*) (Enumerator_t4026582614 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m397134346_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m632692195_gshared (Enumerator_t4026582614 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m632692195(__this, method) ((  void (*) (Enumerator_t4026582614 *, const MethodInfo*))Enumerator_Dispose_m632692195_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1762800540_gshared (Enumerator_t4026582614 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1762800540(__this, method) ((  void (*) (Enumerator_t4026582614 *, const MethodInfo*))Enumerator_VerifyState_m1762800540_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2201601732_gshared (Enumerator_t4026582614 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2201601732(__this, method) ((  bool (*) (Enumerator_t4026582614 *, const MethodInfo*))Enumerator_MoveNext_m2201601732_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Current()
extern "C"  KeyInfo_t848873357  Enumerator_get_Current_m1663259701_gshared (Enumerator_t4026582614 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1663259701(__this, method) ((  KeyInfo_t848873357  (*) (Enumerator_t4026582614 *, const MethodInfo*))Enumerator_get_Current_m1663259701_gshared)(__this, method)
