﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerMovement
struct PlayerMovement_t3827129040;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerMovement::.ctor()
extern "C"  void PlayerMovement__ctor_m537500619 (PlayerMovement_t3827129040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerMovement::Awake()
extern "C"  void PlayerMovement_Awake_m775105838 (PlayerMovement_t3827129040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerMovement::FixedUpdate()
extern "C"  void PlayerMovement_FixedUpdate_m1673803014 (PlayerMovement_t3827129040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerMovement::Move()
extern "C"  void PlayerMovement_Move_m2717087658 (PlayerMovement_t3827129040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
