﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Text.Encoding
struct Encoding_t180559927;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqlit661380204.h"
#include "mscorlib_System_Text_Encoding180559927.h"

// System.IntPtr Mono.Data.SqliteClient.Sqlite::sqlite_open(System.String,System.Int32,System.IntPtr&)
extern "C"  IntPtr_t Sqlite_sqlite_open_m1087827322 (Il2CppObject * __this /* static, unused */, String_t* ___dbname0, int32_t ___db_mode1, IntPtr_t* ___errstr2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.Sqlite::sqlite_close(System.IntPtr)
extern "C"  void Sqlite_sqlite_close_m1774242346 (Il2CppObject * __this /* static, unused */, IntPtr_t ___sqlite_handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.Sqlite::sqlite_changes(System.IntPtr)
extern "C"  int32_t Sqlite_sqlite_changes_m2880385937 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.Sqlite::sqliteFree(System.IntPtr)
extern "C"  void Sqlite_sqliteFree_m3453836343 (Il2CppObject * __this /* static, unused */, IntPtr_t ___ptr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite_compile(System.IntPtr,System.IntPtr,System.IntPtr&,System.IntPtr&,System.IntPtr&)
extern "C"  int32_t Sqlite_sqlite_compile_m3875735358 (Il2CppObject * __this /* static, unused */, IntPtr_t ___sqlite_handle0, IntPtr_t ___zSql1, IntPtr_t* ___pzTail2, IntPtr_t* ___pVm3, IntPtr_t* ___errstr4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite_step(System.IntPtr,System.Int32&,System.IntPtr&,System.IntPtr&)
extern "C"  int32_t Sqlite_sqlite_step_m3605130666 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t* ___pN1, IntPtr_t* ___pazValue2, IntPtr_t* ___pazColName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite_finalize(System.IntPtr,System.IntPtr&)
extern "C"  int32_t Sqlite_sqlite_finalize_m2315478619 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, IntPtr_t* ___pzErrMsg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.Sqlite::sqlite_busy_timeout(System.IntPtr,System.Int32)
extern "C"  void Sqlite_sqlite_busy_timeout_m3809979918 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, int32_t ___ms1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.Sqlite::sqlite3_open16(System.String,System.IntPtr&)
extern "C"  int32_t Sqlite_sqlite3_open16_m3110095766 (Il2CppObject * __this /* static, unused */, String_t* ___dbname0, IntPtr_t* ___handle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.Sqlite::sqlite3_close(System.IntPtr)
extern "C"  void Sqlite_sqlite3_close_m3896572727 (Il2CppObject * __this /* static, unused */, IntPtr_t ___sqlite_handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.SqliteClient.Sqlite::sqlite3_errmsg16(System.IntPtr)
extern "C"  IntPtr_t Sqlite_sqlite3_errmsg16_m2962662443 (Il2CppObject * __this /* static, unused */, IntPtr_t ___sqlite_handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.Sqlite::sqlite3_changes(System.IntPtr)
extern "C"  int32_t Sqlite_sqlite3_changes_m4244137210 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_prepare16(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr&,System.IntPtr&)
extern "C"  int32_t Sqlite_sqlite3_prepare16_m2592742857 (Il2CppObject * __this /* static, unused */, IntPtr_t ___sqlite_handle0, IntPtr_t ___zSql1, int32_t ___zSqllen2, IntPtr_t* ___pVm3, IntPtr_t* ___pzTail4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_step(System.IntPtr)
extern "C"  int32_t Sqlite_sqlite3_step_m2034772046 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_finalize(System.IntPtr)
extern "C"  int32_t Sqlite_sqlite3_finalize_m4286290236 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.SqliteClient.Sqlite::sqlite3_column_name16(System.IntPtr,System.Int32)
extern "C"  IntPtr_t Sqlite_sqlite3_column_name16_m3464827510 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.SqliteClient.Sqlite::sqlite3_column_text16(System.IntPtr,System.Int32)
extern "C"  IntPtr_t Sqlite_sqlite3_column_text16_m1213797784 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.SqliteClient.Sqlite::sqlite3_column_blob(System.IntPtr,System.Int32)
extern "C"  IntPtr_t Sqlite_sqlite3_column_blob_m667396899 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.Sqlite::sqlite3_column_bytes16(System.IntPtr,System.Int32)
extern "C"  int32_t Sqlite_sqlite3_column_bytes16_m516228693 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.Sqlite::sqlite3_column_count(System.IntPtr)
extern "C"  int32_t Sqlite_sqlite3_column_count_m1643060835 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.Sqlite::sqlite3_column_type(System.IntPtr,System.Int32)
extern "C"  int32_t Sqlite_sqlite3_column_type_m3242015453 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.Data.SqliteClient.Sqlite::sqlite3_column_int64(System.IntPtr,System.Int32)
extern "C"  int64_t Sqlite_sqlite3_column_int64_m3912717969 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Mono.Data.SqliteClient.Sqlite::sqlite3_column_double(System.IntPtr,System.Int32)
extern "C"  double Sqlite_sqlite3_column_double_m1464837061 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.SqliteClient.Sqlite::sqlite3_column_decltype16(System.IntPtr,System.Int32)
extern "C"  IntPtr_t Sqlite_sqlite3_column_decltype16_m3515264015 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.Sqlite::sqlite3_bind_parameter_count(System.IntPtr)
extern "C"  int32_t Sqlite_sqlite3_bind_parameter_count_m847916594 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.SqliteClient.Sqlite::sqlite3_bind_parameter_name(System.IntPtr,System.Int32)
extern "C"  IntPtr_t Sqlite_sqlite3_bind_parameter_name_m2331255648 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, int32_t ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_bind_blob(System.IntPtr,System.Int32,System.Byte[],System.Int32,System.IntPtr)
extern "C"  int32_t Sqlite_sqlite3_bind_blob_m2003916280 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, int32_t ___n1, ByteU5BU5D_t58506160* ___blob2, int32_t ___length3, IntPtr_t ___freetype4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_bind_double(System.IntPtr,System.Int32,System.Double)
extern "C"  int32_t Sqlite_sqlite3_bind_double_m525303930 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, int32_t ___n1, double ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_bind_int(System.IntPtr,System.Int32,System.Int32)
extern "C"  int32_t Sqlite_sqlite3_bind_int_m367224749 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, int32_t ___n1, int32_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_bind_int64(System.IntPtr,System.Int32,System.Int64)
extern "C"  int32_t Sqlite_sqlite3_bind_int64_m3823319984 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, int32_t ___n1, int64_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_bind_null(System.IntPtr,System.Int32)
extern "C"  int32_t Sqlite_sqlite3_bind_null_m2331718516 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, int32_t ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_bind_text16(System.IntPtr,System.Int32,System.String,System.Int32,System.IntPtr)
extern "C"  int32_t Sqlite_sqlite3_bind_text16_m3978780170 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, int32_t ___n1, String_t* ___value2, int32_t ___length3, IntPtr_t ___freetype4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.Sqlite::sqlite3_busy_timeout(System.IntPtr,System.Int32)
extern "C"  void Sqlite_sqlite3_busy_timeout_m207214235 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, int32_t ___ms1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.SqliteClient.Sqlite::StringToHeap(System.String,System.Text.Encoding)
extern "C"  IntPtr_t Sqlite_StringToHeap_m3048525239 (Il2CppObject * __this /* static, unused */, String_t* ___s0, Encoding_t180559927 * ___encoding1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.SqliteClient.Sqlite::HeapToString(System.IntPtr,System.Text.Encoding)
extern "C"  String_t* Sqlite_HeapToString_m2398139703 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, Encoding_t180559927 * ___encoding1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
