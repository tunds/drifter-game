﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbDataAdapter
struct DbDataAdapter_t3684585719;
// System.Data.IDbCommand
struct IDbCommand_t2345198679;
// System.Object
struct Il2CppObject;
// System.Data.Common.DbCommand
struct DbCommand_t2323745021;
// System.Data.DataTable
struct DataTable_t2176726999;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_Common_DbCommand2323745021.h"
#include "System_Data_System_Data_DataTable2176726999.h"
#include "System_Data_System_Data_CommandBehavior326612048.h"

// System.Void System.Data.Common.DbDataAdapter::.ctor()
extern "C"  void DbDataAdapter__ctor_m3425795853 (DbDataAdapter_t3684585719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.IDbCommand System.Data.Common.DbDataAdapter::System.Data.IDbDataAdapter.get_SelectCommand()
extern "C"  Il2CppObject * DbDataAdapter_System_Data_IDbDataAdapter_get_SelectCommand_m1046471964 (DbDataAdapter_t3684585719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbDataAdapter::System.Data.IDbDataAdapter.set_SelectCommand(System.Data.IDbCommand)
extern "C"  void DbDataAdapter_System_Data_IDbDataAdapter_set_SelectCommand_m2575946325 (DbDataAdapter_t3684585719 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.IDbCommand System.Data.Common.DbDataAdapter::System.Data.IDbDataAdapter.get_UpdateCommand()
extern "C"  Il2CppObject * DbDataAdapter_System_Data_IDbDataAdapter_get_UpdateCommand_m2795624431 (DbDataAdapter_t3684585719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbDataAdapter::System.Data.IDbDataAdapter.set_UpdateCommand(System.Data.IDbCommand)
extern "C"  void DbDataAdapter_System_Data_IDbDataAdapter_set_UpdateCommand_m4106982632 (DbDataAdapter_t3684585719 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.IDbCommand System.Data.Common.DbDataAdapter::System.Data.IDbDataAdapter.get_DeleteCommand()
extern "C"  Il2CppObject * DbDataAdapter_System_Data_IDbDataAdapter_get_DeleteCommand_m2372331853 (DbDataAdapter_t3684585719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbDataAdapter::System.Data.IDbDataAdapter.set_DeleteCommand(System.Data.IDbCommand)
extern "C"  void DbDataAdapter_System_Data_IDbDataAdapter_set_DeleteCommand_m3543411654 (DbDataAdapter_t3684585719 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.IDbCommand System.Data.Common.DbDataAdapter::System.Data.IDbDataAdapter.get_InsertCommand()
extern "C"  Il2CppObject * DbDataAdapter_System_Data_IDbDataAdapter_get_InsertCommand_m3952442367 (DbDataAdapter_t3684585719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbDataAdapter::System.Data.IDbDataAdapter.set_InsertCommand(System.Data.IDbCommand)
extern "C"  void DbDataAdapter_System_Data_IDbDataAdapter_set_InsertCommand_m449170680 (DbDataAdapter_t3684585719 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Data.Common.DbDataAdapter::System.ICloneable.Clone()
extern "C"  Il2CppObject * DbDataAdapter_System_ICloneable_Clone_m2682737970 (DbDataAdapter_t3684585719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbDataAdapter::get_SelectCommand()
extern "C"  DbCommand_t2323745021 * DbDataAdapter_get_SelectCommand_m4054494125 (DbDataAdapter_t3684585719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbDataAdapter::set_SelectCommand(System.Data.Common.DbCommand)
extern "C"  void DbDataAdapter_set_SelectCommand_m205623854 (DbDataAdapter_t3684585719 * __this, DbCommand_t2323745021 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbDataAdapter::get_DeleteCommand()
extern "C"  DbCommand_t2323745021 * DbDataAdapter_get_DeleteCommand_m1085386718 (DbDataAdapter_t3684585719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbDataAdapter::set_DeleteCommand(System.Data.Common.DbCommand)
extern "C"  void DbDataAdapter_set_DeleteCommand_m1118130911 (DbDataAdapter_t3684585719 * __this, DbCommand_t2323745021 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbDataAdapter::get_InsertCommand()
extern "C"  DbCommand_t2323745021 * DbDataAdapter_get_InsertCommand_m2665497232 (DbDataAdapter_t3684585719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbDataAdapter::set_InsertCommand(System.Data.Common.DbCommand)
extern "C"  void DbDataAdapter_set_InsertCommand_m4189177489 (DbDataAdapter_t3684585719 * __this, DbCommand_t2323745021 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbDataAdapter::get_UpdateCommand()
extern "C"  DbCommand_t2323745021 * DbDataAdapter_get_UpdateCommand_m1508679296 (DbDataAdapter_t3684585719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbDataAdapter::set_UpdateCommand(System.Data.Common.DbCommand)
extern "C"  void DbDataAdapter_set_UpdateCommand_m199088769 (DbDataAdapter_t3684585719 * __this, DbCommand_t2323745021 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbDataAdapter::Dispose(System.Boolean)
extern "C"  void DbDataAdapter_Dispose_m3309680641 (DbDataAdapter_t3684585719 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.DbDataAdapter::Fill(System.Data.DataTable)
extern "C"  int32_t DbDataAdapter_Fill_m2407344533 (DbDataAdapter_t3684585719 * __this, DataTable_t2176726999 * ___dataTable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.DbDataAdapter::Fill(System.Data.DataTable,System.Data.IDbCommand,System.Data.CommandBehavior)
extern "C"  int32_t DbDataAdapter_Fill_m2287450364 (DbDataAdapter_t3684585719 * __this, DataTable_t2176726999 * ___dataTable0, Il2CppObject * ___command1, int32_t ___behavior2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
