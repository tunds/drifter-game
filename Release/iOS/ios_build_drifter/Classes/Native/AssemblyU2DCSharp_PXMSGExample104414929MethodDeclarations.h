﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PXMSGExample
struct PXMSGExample_t104414929;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_DialogResult3964159632.h"

// System.Void PXMSGExample::.ctor()
extern "C"  void PXMSGExample__ctor_m2869546730 (PXMSGExample_t104414929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PXMSGExample::Update()
extern "C"  void PXMSGExample_Update_m488497507 (PXMSGExample_t104414929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PXMSGExample::Method(System.Int32,PoqXert.MessageBox.DialogResult)
extern "C"  void PXMSGExample_Method_m3687707510 (PXMSGExample_t104414929 * __this, int32_t ___id0, int32_t ___btn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
