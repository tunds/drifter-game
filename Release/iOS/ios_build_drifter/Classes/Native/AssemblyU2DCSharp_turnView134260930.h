﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// turnView
struct  turnView_t134260930  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GameObject turnView::chr_skin1
	GameObject_t4012695102 * ___chr_skin1_2;
	// UnityEngine.GameObject turnView::chr_skin2
	GameObject_t4012695102 * ___chr_skin2_3;

public:
	inline static int32_t get_offset_of_chr_skin1_2() { return static_cast<int32_t>(offsetof(turnView_t134260930, ___chr_skin1_2)); }
	inline GameObject_t4012695102 * get_chr_skin1_2() const { return ___chr_skin1_2; }
	inline GameObject_t4012695102 ** get_address_of_chr_skin1_2() { return &___chr_skin1_2; }
	inline void set_chr_skin1_2(GameObject_t4012695102 * value)
	{
		___chr_skin1_2 = value;
		Il2CppCodeGenWriteBarrier(&___chr_skin1_2, value);
	}

	inline static int32_t get_offset_of_chr_skin2_3() { return static_cast<int32_t>(offsetof(turnView_t134260930, ___chr_skin2_3)); }
	inline GameObject_t4012695102 * get_chr_skin2_3() const { return ___chr_skin2_3; }
	inline GameObject_t4012695102 ** get_address_of_chr_skin2_3() { return &___chr_skin2_3; }
	inline void set_chr_skin2_3(GameObject_t4012695102 * value)
	{
		___chr_skin2_3 = value;
		Il2CppCodeGenWriteBarrier(&___chr_skin2_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
