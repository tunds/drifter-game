﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// anims_controls
struct anims_controls_t1059809747;

#include "codegen/il2cpp-codegen.h"

// System.Void anims_controls::.ctor()
extern "C"  void anims_controls__ctor_m2134425640 (anims_controls_t1059809747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void anims_controls::Awake()
extern "C"  void anims_controls_Awake_m2372030859 (anims_controls_t1059809747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void anims_controls::Att()
extern "C"  void anims_controls_Att_m4257138919 (anims_controls_t1059809747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void anims_controls::KickOver()
extern "C"  void anims_controls_KickOver_m573535318 (anims_controls_t1059809747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void anims_controls::DeathOver()
extern "C"  void anims_controls_DeathOver_m2811472430 (anims_controls_t1059809747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void anims_controls::DeathOver2()
extern "C"  void anims_controls_DeathOver2_m1256309030 (anims_controls_t1059809747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void anims_controls::HitOver()
extern "C"  void anims_controls_HitOver_m3690841421 (anims_controls_t1059809747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void anims_controls::DamageOver()
extern "C"  void anims_controls_DamageOver_m1274120191 (anims_controls_t1059809747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
