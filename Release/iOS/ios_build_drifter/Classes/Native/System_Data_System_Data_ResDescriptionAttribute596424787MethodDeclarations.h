﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.ResDescriptionAttribute
struct ResDescriptionAttribute_t596424787;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void System.Data.ResDescriptionAttribute::.ctor(System.String)
extern "C"  void ResDescriptionAttribute__ctor_m3581460013 (ResDescriptionAttribute_t596424787 * __this, String_t* ___description0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.ResDescriptionAttribute::get_Description()
extern "C"  String_t* ResDescriptionAttribute_get_Description_m4012560905 (ResDescriptionAttribute_t596424787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
