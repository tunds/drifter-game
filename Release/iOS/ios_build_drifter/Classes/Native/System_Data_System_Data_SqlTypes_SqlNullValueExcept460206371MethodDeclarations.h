﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.SqlTypes.SqlNullValueException
struct SqlNullValueException_t460206371;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void System.Data.SqlTypes.SqlNullValueException::.ctor()
extern "C"  void SqlNullValueException__ctor_m2865277409 (SqlNullValueException_t460206371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.SqlTypes.SqlNullValueException::.ctor(System.String)
extern "C"  void SqlNullValueException__ctor_m3735145153 (SqlNullValueException_t460206371 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.SqlTypes.SqlNullValueException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SqlNullValueException__ctor_m3497867170 (SqlNullValueException_t460206371 * __this, SerializationInfo_t2995724695 * ___si0, StreamingContext_t986364934  ___sc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.SqlTypes.SqlNullValueException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SqlNullValueException_System_Runtime_Serialization_ISerializable_GetObjectData_m3963627658 (SqlNullValueException_t460206371 * __this, SerializationInfo_t2995724695 * ___si0, StreamingContext_t986364934  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
