﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_Field>
struct DefaultComparer_t3118284701;

#include "codegen/il2cpp-codegen.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Field3780330969.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_Field>::.ctor()
extern "C"  void DefaultComparer__ctor_m2789567549_gshared (DefaultComparer_t3118284701 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2789567549(__this, method) ((  void (*) (DefaultComparer_t3118284701 *, const MethodInfo*))DefaultComparer__ctor_m2789567549_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_Field>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2929350234_gshared (DefaultComparer_t3118284701 * __this, DB_Field_t3780330969  ___x0, DB_Field_t3780330969  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2929350234(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3118284701 *, DB_Field_t3780330969 , DB_Field_t3780330969 , const MethodInfo*))DefaultComparer_Compare_m2929350234_gshared)(__this, ___x0, ___y1, method)
