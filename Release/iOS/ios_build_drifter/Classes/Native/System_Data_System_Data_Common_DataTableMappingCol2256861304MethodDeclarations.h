﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DataTableMappingCollection
struct DataTableMappingCollection_t2256861304;
// System.Object
struct Il2CppObject;
// System.Data.Common.DataTableMapping
struct DataTableMapping_t171110970;
// System.String
struct String_t;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Data_System_Data_Common_DataTableMapping171110970.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Data_System_Data_Common_DataTableMappingCol2256861304.h"
#include "System_Data_System_Data_MissingMappingAction2387118769.h"

// System.Void System.Data.Common.DataTableMappingCollection::.ctor()
extern "C"  void DataTableMappingCollection__ctor_m762132842 (DataTableMappingCollection_t2256861304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Data.Common.DataTableMappingCollection::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * DataTableMappingCollection_System_Collections_IList_get_Item_m3720631689 (DataTableMappingCollection_t2256861304 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DataTableMappingCollection::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void DataTableMappingCollection_System_Collections_IList_set_Item_m2238237974 (DataTableMappingCollection_t2256861304 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DataTableMappingCollection::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool DataTableMappingCollection_System_Collections_ICollection_get_IsSynchronized_m3540176280 (DataTableMappingCollection_t2256861304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Data.Common.DataTableMappingCollection::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * DataTableMappingCollection_System_Collections_ICollection_get_SyncRoot_m3551362178 (DataTableMappingCollection_t2256861304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DataTableMappingCollection::System.Collections.IList.get_IsFixedSize()
extern "C"  bool DataTableMappingCollection_System_Collections_IList_get_IsFixedSize_m1335559603 (DataTableMappingCollection_t2256861304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DataTableMappingCollection::System.Collections.IList.get_IsReadOnly()
extern "C"  bool DataTableMappingCollection_System_Collections_IList_get_IsReadOnly_m1466922726 (DataTableMappingCollection_t2256861304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.DataTableMappingCollection::get_Count()
extern "C"  int32_t DataTableMappingCollection_get_Count_m4171431250 (DataTableMappingCollection_t2256861304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DataTableMapping System.Data.Common.DataTableMappingCollection::get_Item(System.Int32)
extern "C"  DataTableMapping_t171110970 * DataTableMappingCollection_get_Item_m442719420 (DataTableMappingCollection_t2256861304 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DataTableMappingCollection::set_Item(System.Int32,System.Data.Common.DataTableMapping)
extern "C"  void DataTableMappingCollection_set_Item_m59610815 (DataTableMappingCollection_t2256861304 * __this, int32_t ___index0, DataTableMapping_t171110970 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DataTableMapping System.Data.Common.DataTableMappingCollection::get_Item(System.String)
extern "C"  DataTableMapping_t171110970 * DataTableMappingCollection_get_Item_m1296098551 (DataTableMappingCollection_t2256861304 * __this, String_t* ___sourceTable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.DataTableMappingCollection::Add(System.Object)
extern "C"  int32_t DataTableMappingCollection_Add_m3788368423 (DataTableMappingCollection_t2256861304 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DataTableMappingCollection::Clear()
extern "C"  void DataTableMappingCollection_Clear_m2463233429 (DataTableMappingCollection_t2256861304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DataTableMappingCollection::Contains(System.Object)
extern "C"  bool DataTableMappingCollection_Contains_m3824908805 (DataTableMappingCollection_t2256861304 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DataTableMappingCollection::Contains(System.String)
extern "C"  bool DataTableMappingCollection_Contains_m3602798451 (DataTableMappingCollection_t2256861304 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DataTableMappingCollection::CopyTo(System.Array,System.Int32)
extern "C"  void DataTableMappingCollection_CopyTo_m2831471047 (DataTableMappingCollection_t2256861304 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DataTableMapping System.Data.Common.DataTableMappingCollection::GetTableMappingBySchemaAction(System.Data.Common.DataTableMappingCollection,System.String,System.String,System.Data.MissingMappingAction)
extern "C"  DataTableMapping_t171110970 * DataTableMappingCollection_GetTableMappingBySchemaAction_m1296796662 (Il2CppObject * __this /* static, unused */, DataTableMappingCollection_t2256861304 * ___tableMappings0, String_t* ___sourceTable1, String_t* ___dataSetTable2, int32_t ___mappingAction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Data.Common.DataTableMappingCollection::GetEnumerator()
extern "C"  Il2CppObject * DataTableMappingCollection_GetEnumerator_m2043611552 (DataTableMappingCollection_t2256861304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.DataTableMappingCollection::IndexOf(System.Object)
extern "C"  int32_t DataTableMappingCollection_IndexOf_m3324949759 (DataTableMappingCollection_t2256861304 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.DataTableMappingCollection::IndexOfDataSetTable(System.String)
extern "C"  int32_t DataTableMappingCollection_IndexOfDataSetTable_m3136252727 (DataTableMappingCollection_t2256861304 * __this, String_t* ___dataSetTable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DataTableMappingCollection::Insert(System.Int32,System.Object)
extern "C"  void DataTableMappingCollection_Insert_m3823976818 (DataTableMappingCollection_t2256861304 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DataTableMappingCollection::Remove(System.Object)
extern "C"  void DataTableMappingCollection_Remove_m1744762486 (DataTableMappingCollection_t2256861304 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DataTableMappingCollection::RemoveAt(System.Int32)
extern "C"  void DataTableMappingCollection_RemoveAt_m4133385218 (DataTableMappingCollection_t2256861304 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
