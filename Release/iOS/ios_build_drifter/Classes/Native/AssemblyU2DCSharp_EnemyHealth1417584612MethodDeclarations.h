﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnemyHealth
struct EnemyHealth_t1417584612;

#include "codegen/il2cpp-codegen.h"

// System.Void EnemyHealth::.ctor()
extern "C"  void EnemyHealth__ctor_m1528331271 (EnemyHealth_t1417584612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyHealth::Awake()
extern "C"  void EnemyHealth_Awake_m1765936490 (EnemyHealth_t1417584612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyHealth::Update()
extern "C"  void EnemyHealth_Update_m1860491238 (EnemyHealth_t1417584612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyHealth::HitDamage(System.Int32)
extern "C"  void EnemyHealth_HitDamage_m819917400 (EnemyHealth_t1417584612 * __this, int32_t ___amount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyHealth::Death()
extern "C"  void EnemyHealth_Death_m3913393817 (EnemyHealth_t1417584612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
