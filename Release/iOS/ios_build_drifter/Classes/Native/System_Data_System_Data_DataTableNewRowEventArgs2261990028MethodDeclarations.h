﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.DataTableNewRowEventArgs
struct DataTableNewRowEventArgs_t2261990028;
// System.Data.DataRow
struct DataRow_t3654701923;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_DataRow3654701923.h"

// System.Void System.Data.DataTableNewRowEventArgs::.ctor(System.Data.DataRow)
extern "C"  void DataTableNewRowEventArgs__ctor_m1082149775 (DataTableNewRowEventArgs_t2261990028 * __this, DataRow_t3654701923 * ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
