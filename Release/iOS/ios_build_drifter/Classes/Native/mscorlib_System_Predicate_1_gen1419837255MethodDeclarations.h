﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Predicate`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct Predicate_1_t1419837255;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K848873357.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Predicate`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3968683824_gshared (Predicate_1_t1419837255 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Predicate_1__ctor_m3968683824(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t1419837255 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m3968683824_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1271957970_gshared (Predicate_1_t1419837255 * __this, KeyInfo_t848873357  ___obj0, const MethodInfo* method);
#define Predicate_1_Invoke_m1271957970(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1419837255 *, KeyInfo_t848873357 , const MethodInfo*))Predicate_1_Invoke_m1271957970_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4172860129_gshared (Predicate_1_t1419837255 * __this, KeyInfo_t848873357  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m4172860129(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t1419837255 *, KeyInfo_t848873357 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m4172860129_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1603694914_gshared (Predicate_1_t1419837255 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Predicate_1_EndInvoke_m1603694914(__this, ___result0, method) ((  bool (*) (Predicate_1_t1419837255 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m1603694914_gshared)(__this, ___result0, method)
