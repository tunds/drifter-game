﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct Collection_1_t1894480567;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// SQLiteDatabase.SQLiteDB/DB_DataPair[]
struct DB_DataPairU5BU5D_t2315727976;
// System.Collections.Generic.IEnumerator`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct IEnumerator_1_t1408599285;
// System.Collections.Generic.IList`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct IList_1_t2091985151;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataPair4220460133.h"

// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::.ctor()
extern "C"  void Collection_1__ctor_m3954820426_gshared (Collection_1_t1894480567 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3954820426(__this, method) ((  void (*) (Collection_1_t1894480567 *, const MethodInfo*))Collection_1__ctor_m3954820426_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m855959281_gshared (Collection_1_t1894480567 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m855959281(__this, method) ((  bool (*) (Collection_1_t1894480567 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m855959281_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m621258554_gshared (Collection_1_t1894480567 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m621258554(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1894480567 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m621258554_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m923492021_gshared (Collection_1_t1894480567 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m923492021(__this, method) ((  Il2CppObject * (*) (Collection_1_t1894480567 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m923492021_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m779888220_gshared (Collection_1_t1894480567 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m779888220(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1894480567 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m779888220_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m384033520_gshared (Collection_1_t1894480567 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m384033520(__this, ___value0, method) ((  bool (*) (Collection_1_t1894480567 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m384033520_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1548340916_gshared (Collection_1_t1894480567 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1548340916(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1894480567 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1548340916_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m415121631_gshared (Collection_1_t1894480567 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m415121631(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1894480567 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m415121631_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m272787945_gshared (Collection_1_t1894480567 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m272787945(__this, ___value0, method) ((  void (*) (Collection_1_t1894480567 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m272787945_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3239661164_gshared (Collection_1_t1894480567 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3239661164(__this, method) ((  bool (*) (Collection_1_t1894480567 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3239661164_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2766803288_gshared (Collection_1_t1894480567 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2766803288(__this, method) ((  Il2CppObject * (*) (Collection_1_t1894480567 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2766803288_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m4203513439_gshared (Collection_1_t1894480567 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m4203513439(__this, method) ((  bool (*) (Collection_1_t1894480567 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m4203513439_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1697984698_gshared (Collection_1_t1894480567 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1697984698(__this, method) ((  bool (*) (Collection_1_t1894480567 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1697984698_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1378815199_gshared (Collection_1_t1894480567 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1378815199(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1894480567 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1378815199_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m113881334_gshared (Collection_1_t1894480567 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m113881334(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1894480567 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m113881334_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Add(T)
extern "C"  void Collection_1_Add_m533359093_gshared (Collection_1_t1894480567 * __this, DB_DataPair_t4220460133  ___item0, const MethodInfo* method);
#define Collection_1_Add_m533359093(__this, ___item0, method) ((  void (*) (Collection_1_t1894480567 *, DB_DataPair_t4220460133 , const MethodInfo*))Collection_1_Add_m533359093_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Clear()
extern "C"  void Collection_1_Clear_m1360953717_gshared (Collection_1_t1894480567 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1360953717(__this, method) ((  void (*) (Collection_1_t1894480567 *, const MethodInfo*))Collection_1_Clear_m1360953717_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::ClearItems()
extern "C"  void Collection_1_ClearItems_m4057776301_gshared (Collection_1_t1894480567 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m4057776301(__this, method) ((  void (*) (Collection_1_t1894480567 *, const MethodInfo*))Collection_1_ClearItems_m4057776301_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Contains(T)
extern "C"  bool Collection_1_Contains_m1711801379_gshared (Collection_1_t1894480567 * __this, DB_DataPair_t4220460133  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1711801379(__this, ___item0, method) ((  bool (*) (Collection_1_t1894480567 *, DB_DataPair_t4220460133 , const MethodInfo*))Collection_1_Contains_m1711801379_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m77045413_gshared (Collection_1_t1894480567 * __this, DB_DataPairU5BU5D_t2315727976* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m77045413(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1894480567 *, DB_DataPairU5BU5D_t2315727976*, int32_t, const MethodInfo*))Collection_1_CopyTo_m77045413_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1146487046_gshared (Collection_1_t1894480567 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1146487046(__this, method) ((  Il2CppObject* (*) (Collection_1_t1894480567 *, const MethodInfo*))Collection_1_GetEnumerator_m1146487046_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2689339625_gshared (Collection_1_t1894480567 * __this, DB_DataPair_t4220460133  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2689339625(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1894480567 *, DB_DataPair_t4220460133 , const MethodInfo*))Collection_1_IndexOf_m2689339625_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1751587420_gshared (Collection_1_t1894480567 * __this, int32_t ___index0, DB_DataPair_t4220460133  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1751587420(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1894480567 *, int32_t, DB_DataPair_t4220460133 , const MethodInfo*))Collection_1_Insert_m1751587420_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2987506319_gshared (Collection_1_t1894480567 * __this, int32_t ___index0, DB_DataPair_t4220460133  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2987506319(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1894480567 *, int32_t, DB_DataPair_t4220460133 , const MethodInfo*))Collection_1_InsertItem_m2987506319_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Remove(T)
extern "C"  bool Collection_1_Remove_m3634720990_gshared (Collection_1_t1894480567 * __this, DB_DataPair_t4220460133  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3634720990(__this, ___item0, method) ((  bool (*) (Collection_1_t1894480567 *, DB_DataPair_t4220460133 , const MethodInfo*))Collection_1_Remove_m3634720990_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3920407586_gshared (Collection_1_t1894480567 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3920407586(__this, ___index0, method) ((  void (*) (Collection_1_t1894480567 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3920407586_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3724996546_gshared (Collection_1_t1894480567 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3724996546(__this, ___index0, method) ((  void (*) (Collection_1_t1894480567 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3724996546_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1582457266_gshared (Collection_1_t1894480567 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1582457266(__this, method) ((  int32_t (*) (Collection_1_t1894480567 *, const MethodInfo*))Collection_1_get_Count_m1582457266_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Item(System.Int32)
extern "C"  DB_DataPair_t4220460133  Collection_1_get_Item_m2010790310_gshared (Collection_1_t1894480567 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2010790310(__this, ___index0, method) ((  DB_DataPair_t4220460133  (*) (Collection_1_t1894480567 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2010790310_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1457927219_gshared (Collection_1_t1894480567 * __this, int32_t ___index0, DB_DataPair_t4220460133  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1457927219(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1894480567 *, int32_t, DB_DataPair_t4220460133 , const MethodInfo*))Collection_1_set_Item_m1457927219_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m4076192230_gshared (Collection_1_t1894480567 * __this, int32_t ___index0, DB_DataPair_t4220460133  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m4076192230(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1894480567 *, int32_t, DB_DataPair_t4220460133 , const MethodInfo*))Collection_1_SetItem_m4076192230_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2594259881_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2594259881(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2594259881_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::ConvertItem(System.Object)
extern "C"  DB_DataPair_t4220460133  Collection_1_ConvertItem_m2054333829_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2054333829(__this /* static, unused */, ___item0, method) ((  DB_DataPair_t4220460133  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2054333829_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1463348965_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1463348965(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1463348965_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m4275142011_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m4275142011(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m4275142011_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m4094898948_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m4094898948(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m4094898948_gshared)(__this /* static, unused */, ___list0, method)
