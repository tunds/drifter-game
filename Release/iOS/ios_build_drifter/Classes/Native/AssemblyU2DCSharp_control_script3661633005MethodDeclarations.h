﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// control_script
struct control_script_t3661633005;

#include "codegen/il2cpp-codegen.h"

// System.Void control_script::.ctor()
extern "C"  void control_script__ctor_m1109070158 (control_script_t3661633005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void control_script::Awake()
extern "C"  void control_script_Awake_m1346675377 (control_script_t3661633005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void control_script::Walk()
extern "C"  void control_script_Walk_m791838143 (control_script_t3661633005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void control_script::Run()
extern "C"  void control_script_Run_m1545541879 (control_script_t3661633005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void control_script::OtherIdle()
extern "C"  void control_script_OtherIdle_m1418871536 (control_script_t3661633005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void control_script::Attack()
extern "C"  void control_script_Attack_m964443454 (control_script_t3661633005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void control_script::LowKick()
extern "C"  void control_script_LowKick_m3339733222 (control_script_t3661633005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void control_script::Death()
extern "C"  void control_script_Death_m3494132704 (control_script_t3661633005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void control_script::Death2()
extern "C"  void control_script_Death2_m943941044 (control_script_t3661633005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void control_script::Strike()
extern "C"  void control_script_Strike_m2220620840 (control_script_t3661633005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void control_script::Damage()
extern "C"  void control_script_Damage_m2014915749 (control_script_t3661633005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void control_script::Update()
extern "C"  void control_script_Update_m1748298623 (control_script_t3661633005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
