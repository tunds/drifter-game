﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TurnOBJ
struct TurnOBJ_t699144986;

#include "codegen/il2cpp-codegen.h"

// System.Void TurnOBJ::.ctor()
extern "C"  void TurnOBJ__ctor_m945877393 (TurnOBJ_t699144986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TurnOBJ::Update()
extern "C"  void TurnOBJ_Update_m984290204 (TurnOBJ_t699144986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
