﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct Collection_1_t2817861087;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// Mono.Data.Sqlite.SqliteKeyReader/KeyInfo[]
struct KeyInfoU5BU5D_t1118564256;
// System.Collections.Generic.IEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct IEnumerator_1_t2331979805;
// System.Collections.Generic.IList`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct IList_1_t3015365671;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K848873357.h"

// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor()
extern "C"  void Collection_1__ctor_m2248287602_gshared (Collection_1_t2817861087 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2248287602(__this, method) ((  void (*) (Collection_1_t2817861087 *, const MethodInfo*))Collection_1__ctor_m2248287602_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4152246021_gshared (Collection_1_t2817861087 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4152246021(__this, method) ((  bool (*) (Collection_1_t2817861087 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4152246021_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m964092946_gshared (Collection_1_t2817861087 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m964092946(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2817861087 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m964092946_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2230685217_gshared (Collection_1_t2817861087 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2230685217(__this, method) ((  Il2CppObject * (*) (Collection_1_t2817861087 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2230685217_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m113630780_gshared (Collection_1_t2817861087 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m113630780(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2817861087 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m113630780_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1628734468_gshared (Collection_1_t2817861087 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1628734468(__this, ___value0, method) ((  bool (*) (Collection_1_t2817861087 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1628734468_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m120886932_gshared (Collection_1_t2817861087 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m120886932(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2817861087 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m120886932_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2999082247_gshared (Collection_1_t2817861087 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2999082247(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2817861087 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2999082247_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3422268609_gshared (Collection_1_t2817861087 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3422268609(__this, ___value0, method) ((  void (*) (Collection_1_t2817861087 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3422268609_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1377969368_gshared (Collection_1_t2817861087 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1377969368(__this, method) ((  bool (*) (Collection_1_t2817861087 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1377969368_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3467939978_gshared (Collection_1_t2817861087 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3467939978(__this, method) ((  Il2CppObject * (*) (Collection_1_t2817861087 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3467939978_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1505789555_gshared (Collection_1_t2817861087 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1505789555(__this, method) ((  bool (*) (Collection_1_t2817861087 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1505789555_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m641130022_gshared (Collection_1_t2817861087 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m641130022(__this, method) ((  bool (*) (Collection_1_t2817861087 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m641130022_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m861308817_gshared (Collection_1_t2817861087 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m861308817(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2817861087 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m861308817_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m808936222_gshared (Collection_1_t2817861087 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m808936222(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2817861087 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m808936222_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Add(T)
extern "C"  void Collection_1_Add_m2556519629_gshared (Collection_1_t2817861087 * __this, KeyInfo_t848873357  ___item0, const MethodInfo* method);
#define Collection_1_Add_m2556519629(__this, ___item0, method) ((  void (*) (Collection_1_t2817861087 *, KeyInfo_t848873357 , const MethodInfo*))Collection_1_Add_m2556519629_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Clear()
extern "C"  void Collection_1_Clear_m3949388189_gshared (Collection_1_t2817861087 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3949388189(__this, method) ((  void (*) (Collection_1_t2817861087 *, const MethodInfo*))Collection_1_Clear_m3949388189_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2584885637_gshared (Collection_1_t2817861087 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2584885637(__this, method) ((  void (*) (Collection_1_t2817861087 *, const MethodInfo*))Collection_1_ClearItems_m2584885637_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Contains(T)
extern "C"  bool Collection_1_Contains_m362329743_gshared (Collection_1_t2817861087 * __this, KeyInfo_t848873357  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m362329743(__this, ___item0, method) ((  bool (*) (Collection_1_t2817861087 *, KeyInfo_t848873357 , const MethodInfo*))Collection_1_Contains_m362329743_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2934346621_gshared (Collection_1_t2817861087 * __this, KeyInfoU5BU5D_t1118564256* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2934346621(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2817861087 *, KeyInfoU5BU5D_t1118564256*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2934346621_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1065848550_gshared (Collection_1_t2817861087 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1065848550(__this, method) ((  Il2CppObject* (*) (Collection_1_t2817861087 *, const MethodInfo*))Collection_1_GetEnumerator_m1065848550_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1463049481_gshared (Collection_1_t2817861087 * __this, KeyInfo_t848873357  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1463049481(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2817861087 *, KeyInfo_t848873357 , const MethodInfo*))Collection_1_IndexOf_m1463049481_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3877463348_gshared (Collection_1_t2817861087 * __this, int32_t ___index0, KeyInfo_t848873357  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3877463348(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2817861087 *, int32_t, KeyInfo_t848873357 , const MethodInfo*))Collection_1_Insert_m3877463348_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m74897767_gshared (Collection_1_t2817861087 * __this, int32_t ___index0, KeyInfo_t848873357  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m74897767(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2817861087 *, int32_t, KeyInfo_t848873357 , const MethodInfo*))Collection_1_InsertItem_m74897767_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Remove(T)
extern "C"  bool Collection_1_Remove_m1800916554_gshared (Collection_1_t2817861087 * __this, KeyInfo_t848873357  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1800916554(__this, ___item0, method) ((  bool (*) (Collection_1_t2817861087 *, KeyInfo_t848873357 , const MethodInfo*))Collection_1_Remove_m1800916554_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1751316218_gshared (Collection_1_t2817861087 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1751316218(__this, ___index0, method) ((  void (*) (Collection_1_t2817861087 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1751316218_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2287330458_gshared (Collection_1_t2817861087 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2287330458(__this, ___index0, method) ((  void (*) (Collection_1_t2817861087 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2287330458_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m2222168466_gshared (Collection_1_t2817861087 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m2222168466(__this, method) ((  int32_t (*) (Collection_1_t2817861087 *, const MethodInfo*))Collection_1_get_Count_m2222168466_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Item(System.Int32)
extern "C"  KeyInfo_t848873357  Collection_1_get_Item_m1180935264_gshared (Collection_1_t2817861087 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1180935264(__this, ___index0, method) ((  KeyInfo_t848873357  (*) (Collection_1_t2817861087 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1180935264_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m20261131_gshared (Collection_1_t2817861087 * __this, int32_t ___index0, KeyInfo_t848873357  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m20261131(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2817861087 *, int32_t, KeyInfo_t848873357 , const MethodInfo*))Collection_1_set_Item_m20261131_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1258869262_gshared (Collection_1_t2817861087 * __this, int32_t ___index0, KeyInfo_t848873357  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1258869262(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2817861087 *, int32_t, KeyInfo_t848873357 , const MethodInfo*))Collection_1_SetItem_m1258869262_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1860168637_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1860168637(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1860168637_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::ConvertItem(System.Object)
extern "C"  KeyInfo_t848873357  Collection_1_ConvertItem_m3161727807_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3161727807(__this /* static, unused */, ___item0, method) ((  KeyInfo_t848873357  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3161727807_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2713366461_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2713366461(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2713366461_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1895721191_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1895721191(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1895721191_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1956634392_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1956634392(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1956634392_gshared)(__this /* static, unused */, ___list0, method)
