﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen756363860.h"
#include "mscorlib_System_Array2840145358.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K848873357.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3596078588_gshared (InternalEnumerator_1_t756363860 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3596078588(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t756363860 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3596078588_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m210783780_gshared (InternalEnumerator_1_t756363860 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m210783780(__this, method) ((  void (*) (InternalEnumerator_1_t756363860 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m210783780_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m194883152_gshared (InternalEnumerator_1_t756363860 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m194883152(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t756363860 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m194883152_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2055430867_gshared (InternalEnumerator_1_t756363860 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2055430867(__this, method) ((  void (*) (InternalEnumerator_1_t756363860 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2055430867_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2925199376_gshared (InternalEnumerator_1_t756363860 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2925199376(__this, method) ((  bool (*) (InternalEnumerator_1_t756363860 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2925199376_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Current()
extern "C"  KeyInfo_t848873357  InternalEnumerator_1_get_Current_m3577494147_gshared (InternalEnumerator_1_t756363860 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3577494147(__this, method) ((  KeyInfo_t848873357  (*) (InternalEnumerator_1_t756363860 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3577494147_gshared)(__this, method)
