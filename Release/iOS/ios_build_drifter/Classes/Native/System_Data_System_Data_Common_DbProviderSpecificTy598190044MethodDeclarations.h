﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbProviderSpecificTypePropertyAttribute
struct DbProviderSpecificTypePropertyAttribute_t598190044;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Data.Common.DbProviderSpecificTypePropertyAttribute::.ctor(System.Boolean)
extern "C"  void DbProviderSpecificTypePropertyAttribute__ctor_m4114179583 (DbProviderSpecificTypePropertyAttribute_t598190044 * __this, bool ___isProviderSpecificTypeProperty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
