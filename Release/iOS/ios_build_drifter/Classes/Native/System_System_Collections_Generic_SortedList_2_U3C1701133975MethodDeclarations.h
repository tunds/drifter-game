﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Object,System.Object>
struct U3CGetEnumeratorU3Ec__Iterator1_t1701133975;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"

// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Object,System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1__ctor_m1840757407_gshared (U3CGetEnumeratorU3Ec__Iterator1_t1701133975 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1__ctor_m1840757407(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator1_t1701133975 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1__ctor_m1840757407_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Object,System.Object>::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_Current()
extern "C"  KeyValuePair_2_t3312956448  U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3737045750_gshared (U3CGetEnumeratorU3Ec__Iterator1_t1701133975 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3737045750(__this, method) ((  KeyValuePair_2_t3312956448  (*) (U3CGetEnumeratorU3Ec__Iterator1_t1701133975 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3737045750_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3016969841_gshared (U3CGetEnumeratorU3Ec__Iterator1_t1701133975 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3016969841(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator1_t1701133975 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3016969841_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Object,System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m433074623_gshared (U3CGetEnumeratorU3Ec__Iterator1_t1701133975 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m433074623(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator1_t1701133975 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m433074623_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Object,System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1_Dispose_m3634592540_gshared (U3CGetEnumeratorU3Ec__Iterator1_t1701133975 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_Dispose_m3634592540(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator1_t1701133975 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_Dispose_m3634592540_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Object,System.Object>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1_Reset_m3782157644_gshared (U3CGetEnumeratorU3Ec__Iterator1_t1701133975 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_Reset_m3782157644(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator1_t1701133975 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_Reset_m3782157644_gshared)(__this, method)
