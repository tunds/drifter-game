﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.CriticalHandle
struct CriticalHandle_t19319977;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr676692020.h"

// System.Void System.Runtime.InteropServices.CriticalHandle::.ctor(System.IntPtr)
extern "C"  void CriticalHandle__ctor_m1807249103 (CriticalHandle_t19319977 * __this, IntPtr_t ___invalidHandleValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.CriticalHandle::Finalize()
extern "C"  void CriticalHandle_Finalize_m1504065693 (CriticalHandle_t19319977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.CriticalHandle::Close()
extern "C"  void CriticalHandle_Close_m3991531195 (CriticalHandle_t19319977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.CriticalHandle::Dispose()
extern "C"  void CriticalHandle_Dispose_m1190420642 (CriticalHandle_t19319977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.CriticalHandle::Dispose(System.Boolean)
extern "C"  void CriticalHandle_Dispose_m856445593 (CriticalHandle_t19319977 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.CriticalHandle::SetHandle(System.IntPtr)
extern "C"  void CriticalHandle_SetHandle_m3616726279 (CriticalHandle_t19319977 * __this, IntPtr_t ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
