﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbProviderFactory
struct DbProviderFactory_t2435213707;
// System.Data.DataRow
struct DataRow_t3654701923;
// System.String
struct String_t;
// System.Data.DataTable
struct DataTable_t2176726999;
// System.Data.DataSet
struct DataSet_t3654702571;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_DataRow3654701923.h"
#include "mscorlib_System_String968488902.h"

// System.Data.Common.DbProviderFactory System.Data.Common.DbProviderFactories::GetFactory(System.Data.DataRow)
extern "C"  DbProviderFactory_t2435213707 * DbProviderFactories_GetFactory_m1140710612 (Il2CppObject * __this /* static, unused */, DataRow_t3654701923 * ___providerRow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbProviderFactory System.Data.Common.DbProviderFactories::GetFactory(System.String)
extern "C"  DbProviderFactory_t2435213707 * DbProviderFactories_GetFactory_m1249875471 (Il2CppObject * __this /* static, unused */, String_t* ___providerInvariantName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable System.Data.Common.DbProviderFactories::GetFactoryClasses()
extern "C"  DataTable_t2176726999 * DbProviderFactories_GetFactoryClasses_m863250423 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataSet System.Data.Common.DbProviderFactories::GetConfigEntries()
extern "C"  DataSet_t3654702571 * DbProviderFactories_GetConfigEntries_m1201888673 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbProviderFactories::.cctor()
extern "C"  void DbProviderFactories__cctor_m1504528114 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
