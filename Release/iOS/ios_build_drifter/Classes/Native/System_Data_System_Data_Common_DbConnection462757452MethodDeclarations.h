﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbConnection
struct DbConnection_t462757452;
// System.Data.IDbCommand
struct IDbCommand_t2345198679;
// System.Data.Common.DbCommand
struct DbCommand_t2323745021;
// System.Transactions.Transaction
struct Transaction_t3175846586;
// System.Data.DataTable
struct DataTable_t2176726999;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Data.Common.DbProviderFactory
struct DbProviderFactory_t2435213707;

#include "codegen/il2cpp-codegen.h"
#include "System_Transactions_System_Transactions_Transactio3175846586.h"
#include "mscorlib_System_String968488902.h"
#include "System_Data_System_Data_Common_DbCommand2323745021.h"
#include "System_Data_System_Data_DbType2586775211.h"

// System.Void System.Data.Common.DbConnection::.ctor()
extern "C"  void DbConnection__ctor_m1396822102 (DbConnection_t462757452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.IDbCommand System.Data.Common.DbConnection::System.Data.IDbConnection.CreateCommand()
extern "C"  Il2CppObject * DbConnection_System_Data_IDbConnection_CreateCommand_m1751391871 (DbConnection_t462757452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbConnection::CreateCommand()
extern "C"  DbCommand_t2323745021 * DbConnection_CreateCommand_m1718958439 (DbConnection_t462757452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbConnection::EnlistTransaction(System.Transactions.Transaction)
extern "C"  void DbConnection_EnlistTransaction_m2773989187 (DbConnection_t462757452 * __this, Transaction_t3175846586 * ___transaction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable System.Data.Common.DbConnection::GetSchema(System.String)
extern "C"  DataTable_t2176726999 * DbConnection_GetSchema_m3829581731 (DbConnection_t462757452 * __this, String_t* ___collectionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbConnection::AddParameter(System.Data.Common.DbCommand,System.String,System.Data.DbType,System.Int32)
extern "C"  void DbConnection_AddParameter_m2168050353 (DbConnection_t462757452 * __this, DbCommand_t2323745021 * ___command0, String_t* ___parameterName1, int32_t ___parameterType2, int32_t ___parameterSize3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable System.Data.Common.DbConnection::GetSchema(System.String,System.String[])
extern "C"  DataTable_t2176726999 * DbConnection_GetSchema_m1590555133 (DbConnection_t462757452 * __this, String_t* ___collectionName0, StringU5BU5D_t2956870243* ___restrictionValues1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbProviderFactory System.Data.Common.DbConnection::get_DbProviderFactory()
extern "C"  DbProviderFactory_t2435213707 * DbConnection_get_DbProviderFactory_m1634519292 (DbConnection_t462757452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
