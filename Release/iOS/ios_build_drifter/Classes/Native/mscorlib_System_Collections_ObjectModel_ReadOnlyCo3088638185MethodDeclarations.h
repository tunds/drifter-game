﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct ReadOnlyCollection_1_t3088638185;
// System.Collections.Generic.IList`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct IList_1_t2091985151;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// SQLiteDatabase.SQLiteDB/DB_DataPair[]
struct DB_DataPairU5BU5D_t2315727976;
// System.Collections.Generic.IEnumerator`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct IEnumerator_1_t1408599285;

#include "codegen/il2cpp-codegen.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataPair4220460133.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m514747465_gshared (ReadOnlyCollection_1_t3088638185 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m514747465(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3088638185 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m514747465_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m58806707_gshared (ReadOnlyCollection_1_t3088638185 * __this, DB_DataPair_t4220460133  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m58806707(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3088638185 *, DB_DataPair_t4220460133 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m58806707_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3829698935_gshared (ReadOnlyCollection_1_t3088638185 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3829698935(__this, method) ((  void (*) (ReadOnlyCollection_1_t3088638185 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3829698935_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1882558682_gshared (ReadOnlyCollection_1_t3088638185 * __this, int32_t ___index0, DB_DataPair_t4220460133  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1882558682(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3088638185 *, int32_t, DB_DataPair_t4220460133 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1882558682_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2861685984_gshared (ReadOnlyCollection_1_t3088638185 * __this, DB_DataPair_t4220460133  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2861685984(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3088638185 *, DB_DataPair_t4220460133 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2861685984_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4051378848_gshared (ReadOnlyCollection_1_t3088638185 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4051378848(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3088638185 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4051378848_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  DB_DataPair_t4220460133  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3064941988_gshared (ReadOnlyCollection_1_t3088638185 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3064941988(__this, ___index0, method) ((  DB_DataPair_t4220460133  (*) (ReadOnlyCollection_1_t3088638185 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3064941988_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2767258417_gshared (ReadOnlyCollection_1_t3088638185 * __this, int32_t ___index0, DB_DataPair_t4220460133  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2767258417(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3088638185 *, int32_t, DB_DataPair_t4220460133 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2767258417_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1423811759_gshared (ReadOnlyCollection_1_t3088638185 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1423811759(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3088638185 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1423811759_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m924860536_gshared (ReadOnlyCollection_1_t3088638185 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m924860536(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3088638185 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m924860536_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2062775667_gshared (ReadOnlyCollection_1_t3088638185 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2062775667(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3088638185 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2062775667_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3736264286_gshared (ReadOnlyCollection_1_t3088638185 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3736264286(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3088638185 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3736264286_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2062364550_gshared (ReadOnlyCollection_1_t3088638185 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2062364550(__this, method) ((  void (*) (ReadOnlyCollection_1_t3088638185 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2062364550_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2768049710_gshared (ReadOnlyCollection_1_t3088638185 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2768049710(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3088638185 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2768049710_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m578860470_gshared (ReadOnlyCollection_1_t3088638185 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m578860470(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3088638185 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m578860470_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m39737569_gshared (ReadOnlyCollection_1_t3088638185 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m39737569(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3088638185 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m39737569_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m4145116839_gshared (ReadOnlyCollection_1_t3088638185 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m4145116839(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3088638185 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m4145116839_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1234958833_gshared (ReadOnlyCollection_1_t3088638185 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1234958833(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3088638185 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1234958833_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3886770798_gshared (ReadOnlyCollection_1_t3088638185 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3886770798(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3088638185 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3886770798_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2527812058_gshared (ReadOnlyCollection_1_t3088638185 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2527812058(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3088638185 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2527812058_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2472837149_gshared (ReadOnlyCollection_1_t3088638185 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2472837149(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3088638185 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2472837149_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3166177084_gshared (ReadOnlyCollection_1_t3088638185 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3166177084(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3088638185 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3166177084_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3636477153_gshared (ReadOnlyCollection_1_t3088638185 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3636477153(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3088638185 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3636477153_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m147050616_gshared (ReadOnlyCollection_1_t3088638185 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m147050616(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3088638185 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m147050616_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m219662885_gshared (ReadOnlyCollection_1_t3088638185 * __this, DB_DataPair_t4220460133  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m219662885(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3088638185 *, DB_DataPair_t4220460133 , const MethodInfo*))ReadOnlyCollection_1_Contains_m219662885_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m4244688355_gshared (ReadOnlyCollection_1_t3088638185 * __this, DB_DataPairU5BU5D_t2315727976* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m4244688355(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3088638185 *, DB_DataPairU5BU5D_t2315727976*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m4244688355_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m765227016_gshared (ReadOnlyCollection_1_t3088638185 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m765227016(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3088638185 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m765227016_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2361116839_gshared (ReadOnlyCollection_1_t3088638185 * __this, DB_DataPair_t4220460133  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2361116839(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3088638185 *, DB_DataPair_t4220460133 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2361116839_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m4292452788_gshared (ReadOnlyCollection_1_t3088638185 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m4292452788(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3088638185 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m4292452788_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Item(System.Int32)
extern "C"  DB_DataPair_t4220460133  ReadOnlyCollection_1_get_Item_m3125932388_gshared (ReadOnlyCollection_1_t3088638185 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3125932388(__this, ___index0, method) ((  DB_DataPair_t4220460133  (*) (ReadOnlyCollection_1_t3088638185 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3125932388_gshared)(__this, ___index0, method)
