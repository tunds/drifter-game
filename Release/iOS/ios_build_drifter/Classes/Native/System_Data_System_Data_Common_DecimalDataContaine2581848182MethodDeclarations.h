﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DecimalDataContainer
struct DecimalDataContainer_t2581848182;
// System.Data.ISafeDataRecord
struct ISafeDataRecord_t3927591524;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Data.Common.DecimalDataContainer::.ctor()
extern "C"  void DecimalDataContainer__ctor_m3060268652 (DecimalDataContainer_t2581848182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DecimalDataContainer::SetValueFromSafeDataRecord(System.Int32,System.Data.ISafeDataRecord,System.Int32)
extern "C"  void DecimalDataContainer_SetValueFromSafeDataRecord_m1931494701 (DecimalDataContainer_t2581848182 * __this, int32_t ___index0, Il2CppObject * ___record1, int32_t ___field2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DecimalDataContainer::SetValue(System.Int32,System.Object)
extern "C"  void DecimalDataContainer_SetValue_m2650420198 (DecimalDataContainer_t2581848182 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
