﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLiteDatabase.DBSchema
struct DBSchema_t4247391996;
// System.String
struct String_t;
// System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct List_1_t282322642;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataType4220602565.h"

// System.Void SQLiteDatabase.DBSchema::.ctor(System.String)
extern "C"  void DBSchema__ctor_m2925071800 (DBSchema_t4247391996 * __this, String_t* ___tableName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLiteDatabase.DBSchema::get_TableName()
extern "C"  String_t* DBSchema_get_TableName_m230189915 (DBSchema_t4247391996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiteDatabase.DBSchema::AddField(System.String,SQLiteDatabase.SQLiteDB/DB_DataType,System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void DBSchema_AddField_m776412250 (DBSchema_t4247391996 * __this, String_t* ___fieldName0, int32_t ___fieldType1, int32_t ___size2, bool ___isNotNull3, bool ___isPrimaryKey4, bool ___isUnique5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field> SQLiteDatabase.DBSchema::GetTableFields()
extern "C"  List_1_t282322642 * DBSchema_GetTableFields_m2566209761 (DBSchema_t4247391996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
