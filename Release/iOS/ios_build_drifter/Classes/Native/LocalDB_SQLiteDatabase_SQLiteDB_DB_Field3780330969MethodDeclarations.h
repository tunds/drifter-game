﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct DB_Field_t3780330969;
struct DB_Field_t3780330969_marshaled_pinvoke;

extern "C" void DB_Field_t3780330969_marshal_pinvoke(const DB_Field_t3780330969& unmarshaled, DB_Field_t3780330969_marshaled_pinvoke& marshaled);
extern "C" void DB_Field_t3780330969_marshal_pinvoke_back(const DB_Field_t3780330969_marshaled_pinvoke& marshaled, DB_Field_t3780330969& unmarshaled);
extern "C" void DB_Field_t3780330969_marshal_pinvoke_cleanup(DB_Field_t3780330969_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct DB_Field_t3780330969;
struct DB_Field_t3780330969_marshaled_com;

extern "C" void DB_Field_t3780330969_marshal_com(const DB_Field_t3780330969& unmarshaled, DB_Field_t3780330969_marshaled_com& marshaled);
extern "C" void DB_Field_t3780330969_marshal_com_back(const DB_Field_t3780330969_marshaled_com& marshaled, DB_Field_t3780330969& unmarshaled);
extern "C" void DB_Field_t3780330969_marshal_com_cleanup(DB_Field_t3780330969_marshaled_com& marshaled);
