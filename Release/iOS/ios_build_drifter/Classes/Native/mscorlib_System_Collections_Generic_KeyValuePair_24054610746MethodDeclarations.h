﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24054610746.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m606127727_gshared (KeyValuePair_2_t4054610746 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m606127727(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4054610746 *, int64_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m606127727_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Key()
extern "C"  int64_t KeyValuePair_2_get_Key_m1653476505_gshared (KeyValuePair_2_t4054610746 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1653476505(__this, method) ((  int64_t (*) (KeyValuePair_2_t4054610746 *, const MethodInfo*))KeyValuePair_2_get_Key_m1653476505_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3195669082_gshared (KeyValuePair_2_t4054610746 * __this, int64_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3195669082(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4054610746 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Key_m3195669082_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2983326873_gshared (KeyValuePair_2_t4054610746 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2983326873(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t4054610746 *, const MethodInfo*))KeyValuePair_2_get_Value_m2983326873_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2008326490_gshared (KeyValuePair_2_t4054610746 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2008326490(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4054610746 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2008326490_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2061720648_gshared (KeyValuePair_2_t4054610746 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2061720648(__this, method) ((  String_t* (*) (KeyValuePair_2_t4054610746 *, const MethodInfo*))KeyValuePair_2_ToString_m2061720648_gshared)(__this, method)
