﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteStatement
struct SqliteStatement_t3906494218;
// Mono.Data.Sqlite.SQLiteBase
struct SQLiteBase_t3947283844;
// Mono.Data.Sqlite.SqliteStatementHandle
struct SqliteStatementHandle_t1861022482;
// System.String
struct String_t;
// Mono.Data.Sqlite.SqliteParameter
struct SqliteParameter_t3651135812;
// System.String[]
struct StringU5BU5D_t2956870243;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteBase3947283844.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteStatementH1861022482.h"
#include "mscorlib_System_String968488902.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteStatement3906494218.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteParameter3651135812.h"

// System.Void Mono.Data.Sqlite.SqliteStatement::.ctor(Mono.Data.Sqlite.SQLiteBase,Mono.Data.Sqlite.SqliteStatementHandle,System.String,Mono.Data.Sqlite.SqliteStatement)
extern "C"  void SqliteStatement__ctor_m2373094752 (SqliteStatement_t3906494218 * __this, SQLiteBase_t3947283844 * ___sqlbase0, SqliteStatementHandle_t1861022482 * ___stmt1, String_t* ___strCommand2, SqliteStatement_t3906494218 * ___previous3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteStatement::MapParameter(System.String,Mono.Data.Sqlite.SqliteParameter)
extern "C"  bool SqliteStatement_MapParameter_m2116742411 (SqliteStatement_t3906494218 * __this, String_t* ___s0, SqliteParameter_t3651135812 * ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteStatement::Dispose()
extern "C"  void SqliteStatement_Dispose_m1565955477 (SqliteStatement_t3906494218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteStatement::BindParameters()
extern "C"  void SqliteStatement_BindParameters_m2268466547 (SqliteStatement_t3906494218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteStatement::BindParameter(System.Int32,Mono.Data.Sqlite.SqliteParameter)
extern "C"  void SqliteStatement_BindParameter_m2674168159 (SqliteStatement_t3906494218 * __this, int32_t ___index0, SqliteParameter_t3651135812 * ___param1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Mono.Data.Sqlite.SqliteStatement::get_TypeDefinitions()
extern "C"  StringU5BU5D_t2956870243* SqliteStatement_get_TypeDefinitions_m332599954 (SqliteStatement_t3906494218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteStatement::SetTypes(System.String)
extern "C"  void SqliteStatement_SetTypes_m3218270975 (SqliteStatement_t3906494218 * __this, String_t* ___typedefs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
