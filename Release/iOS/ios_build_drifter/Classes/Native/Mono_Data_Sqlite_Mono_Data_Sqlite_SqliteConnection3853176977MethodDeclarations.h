﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteConnection
struct SqliteConnection_t3853176977;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// Mono.Data.Sqlite.SqliteTransaction
struct SqliteTransaction_t3177548665;
// System.Data.Common.DbTransaction
struct DbTransaction_t4162579344;
// Mono.Data.Sqlite.SqliteCommand
struct SqliteCommand_t4229878246;
// System.Data.Common.DbCommand
struct DbCommand_t2323745021;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Collections.Generic.SortedList`2<System.String,System.String>
struct SortedList_2_t1454243397;
// System.Transactions.Transaction
struct Transaction_t3175846586;
// System.Data.DataTable
struct DataTable_t2176726999;
// System.Data.Common.DbProviderFactory
struct DbProviderFactory_t2435213707;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection3853176977.h"
#include "System_Data_System_Data_ConnectionState270608870.h"
#include "System_Data_System_Data_IsolationLevel3729656617.h"
#include "System_Transactions_System_Transactions_Transactio3175846586.h"

// System.Void Mono.Data.Sqlite.SqliteConnection::.ctor()
extern "C"  void SqliteConnection__ctor_m2647571585 (SqliteConnection_t3853176977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteConnection::.ctor(System.String)
extern "C"  void SqliteConnection__ctor_m1210514465 (SqliteConnection_t3853176977 * __this, String_t* ___connectionString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteConnection::.ctor(Mono.Data.Sqlite.SqliteConnection)
extern "C"  void SqliteConnection__ctor_m1859557022 (SqliteConnection_t3853176977 * __this, SqliteConnection_t3853176977 * ___connection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteConnection::Clone()
extern "C"  Il2CppObject * SqliteConnection_Clone_m3937753073 (SqliteConnection_t3853176977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteConnection::Dispose(System.Boolean)
extern "C"  void SqliteConnection_Dispose_m3284241781 (SqliteConnection_t3853176977 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteConnection::OnStateChange(System.Data.ConnectionState)
extern "C"  void SqliteConnection_OnStateChange_m1680745315 (SqliteConnection_t3853176977 * __this, int32_t ___newState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteTransaction Mono.Data.Sqlite.SqliteConnection::BeginTransaction()
extern "C"  SqliteTransaction_t3177548665 * SqliteConnection_BeginTransaction_m3392163190 (SqliteConnection_t3853176977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbTransaction Mono.Data.Sqlite.SqliteConnection::BeginDbTransaction(System.Data.IsolationLevel)
extern "C"  DbTransaction_t4162579344 * SqliteConnection_BeginDbTransaction_m3997286646 (SqliteConnection_t3853176977 * __this, int32_t ___isolationLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteConnection::Close()
extern "C"  void SqliteConnection_Close_m63463831 (SqliteConnection_t3853176977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteConnection::get_ConnectionString()
extern "C"  String_t* SqliteConnection_get_ConnectionString_m1600745080 (SqliteConnection_t3853176977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteConnection::set_ConnectionString(System.String)
extern "C"  void SqliteConnection_set_ConnectionString_m1728758515 (SqliteConnection_t3853176977 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteCommand Mono.Data.Sqlite.SqliteConnection::CreateCommand()
extern "C"  SqliteCommand_t4229878246 * SqliteConnection_CreateCommand_m110869411 (SqliteConnection_t3853176977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand Mono.Data.Sqlite.SqliteConnection::CreateDbCommand()
extern "C"  DbCommand_t2323745021 * SqliteConnection_CreateDbCommand_m2852395884 (SqliteConnection_t3853176977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteConnection::MapMonoKeyword(System.String[],System.Collections.Generic.SortedList`2<System.String,System.String>)
extern "C"  void SqliteConnection_MapMonoKeyword_m2357418281 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t2956870243* ___arPiece0, SortedList_2_t1454243397 * ___ls1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteConnection::MapMonoUriPath(System.String)
extern "C"  String_t* SqliteConnection_MapMonoUriPath_m4035013392 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteConnection::MapUriPath(System.String)
extern "C"  String_t* SqliteConnection_MapUriPath_m1214637773 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.SortedList`2<System.String,System.String> Mono.Data.Sqlite.SqliteConnection::ParseConnectionString(System.String)
extern "C"  SortedList_2_t1454243397 * SqliteConnection_ParseConnectionString_m657607584 (Il2CppObject * __this /* static, unused */, String_t* ___connectionString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteConnection::EnlistTransaction(System.Transactions.Transaction)
extern "C"  void SqliteConnection_EnlistTransaction_m1780809272 (SqliteConnection_t3853176977 * __this, Transaction_t3175846586 * ___transaction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteConnection::FindKey(System.Collections.Generic.SortedList`2<System.String,System.String>,System.String,System.String)
extern "C"  String_t* SqliteConnection_FindKey_m960722934 (Il2CppObject * __this /* static, unused */, SortedList_2_t1454243397 * ___items0, String_t* ___key1, String_t* ___defValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteConnection::Open()
extern "C"  void SqliteConnection_Open_m3535575149 (SqliteConnection_t3853176977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteConnection::get_DefaultTimeout()
extern "C"  int32_t SqliteConnection_get_DefaultTimeout_m3989203760 (SqliteConnection_t3853176977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.ConnectionState Mono.Data.Sqlite.SqliteConnection::get_State()
extern "C"  int32_t SqliteConnection_get_State_m3529836364 (SqliteConnection_t3853176977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteConnection::ExpandFileName(System.String)
extern "C"  String_t* SqliteConnection_ExpandFileName_m1396247713 (SqliteConnection_t3853176977 * __this, String_t* ___sourceFile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::GetSchema(System.String)
extern "C"  DataTable_t2176726999 * SqliteConnection_GetSchema_m2957697240 (SqliteConnection_t3853176977 * __this, String_t* ___collectionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::GetSchema(System.String,System.String[])
extern "C"  DataTable_t2176726999 * SqliteConnection_GetSchema_m1689190706 (SqliteConnection_t3853176977 * __this, String_t* ___collectionName0, StringU5BU5D_t2956870243* ___restrictionValues1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::Schema_ReservedWords()
extern "C"  DataTable_t2176726999 * SqliteConnection_Schema_ReservedWords_m1427330194 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::Schema_MetaDataCollections()
extern "C"  DataTable_t2176726999 * SqliteConnection_Schema_MetaDataCollections_m1569139127 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::Schema_DataSourceInformation()
extern "C"  DataTable_t2176726999 * SqliteConnection_Schema_DataSourceInformation_m4222016056 (SqliteConnection_t3853176977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::Schema_Columns(System.String,System.String,System.String)
extern "C"  DataTable_t2176726999 * SqliteConnection_Schema_Columns_m3677761068 (SqliteConnection_t3853176977 * __this, String_t* ___strCatalog0, String_t* ___strTable1, String_t* ___strColumn2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::Schema_Indexes(System.String,System.String,System.String)
extern "C"  DataTable_t2176726999 * SqliteConnection_Schema_Indexes_m3895149865 (SqliteConnection_t3853176977 * __this, String_t* ___strCatalog0, String_t* ___strTable1, String_t* ___strIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::Schema_Triggers(System.String,System.String,System.String)
extern "C"  DataTable_t2176726999 * SqliteConnection_Schema_Triggers_m2686810734 (SqliteConnection_t3853176977 * __this, String_t* ___catalog0, String_t* ___table1, String_t* ___triggerName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::Schema_Tables(System.String,System.String,System.String)
extern "C"  DataTable_t2176726999 * SqliteConnection_Schema_Tables_m2074096516 (SqliteConnection_t3853176977 * __this, String_t* ___strCatalog0, String_t* ___strTable1, String_t* ___strType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::Schema_Views(System.String,System.String)
extern "C"  DataTable_t2176726999 * SqliteConnection_Schema_Views_m788693247 (SqliteConnection_t3853176977 * __this, String_t* ___strCatalog0, String_t* ___strView1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::Schema_Catalogs(System.String)
extern "C"  DataTable_t2176726999 * SqliteConnection_Schema_Catalogs_m3339212887 (SqliteConnection_t3853176977 * __this, String_t* ___strCatalog0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::Schema_DataTypes()
extern "C"  DataTable_t2176726999 * SqliteConnection_Schema_DataTypes_m1400077216 (SqliteConnection_t3853176977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::Schema_IndexColumns(System.String,System.String,System.String,System.String)
extern "C"  DataTable_t2176726999 * SqliteConnection_Schema_IndexColumns_m3933752122 (SqliteConnection_t3853176977 * __this, String_t* ___strCatalog0, String_t* ___strTable1, String_t* ___strIndex2, String_t* ___strColumn3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::Schema_ViewColumns(System.String,System.String,System.String)
extern "C"  DataTable_t2176726999 * SqliteConnection_Schema_ViewColumns_m2860419505 (SqliteConnection_t3853176977 * __this, String_t* ___strCatalog0, String_t* ___strView1, String_t* ___strColumn2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteConnection::Schema_ForeignKeys(System.String,System.String,System.String)
extern "C"  DataTable_t2176726999 * SqliteConnection_Schema_ForeignKeys_m3589753921 (SqliteConnection_t3853176977 * __this, String_t* ___strCatalog0, String_t* ___strTable1, String_t* ___strKeyName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbProviderFactory Mono.Data.Sqlite.SqliteConnection::get_DbProviderFactory()
extern "C"  DbProviderFactory_t2435213707 * SqliteConnection_get_DbProviderFactory_m786091487 (SqliteConnection_t3853176977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
