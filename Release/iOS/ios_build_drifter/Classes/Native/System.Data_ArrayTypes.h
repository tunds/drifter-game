﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Data.DataRow
struct DataRow_t3654701923;
// System.Data.DataColumn
struct DataColumn_t3354469747;
// System.Data.DataTable
struct DataTable_t2176726999;
// System.Data.DataRelation
struct DataRelation_t1483987353;
// System.Data.DbSourceMethodInfo
struct DbSourceMethodInfo_t3307618843;
// System.Data.Common.DataColumnMapping
struct DataColumnMapping_t2340601118;
// System.Data.IColumnMapping
struct IColumnMapping_t440254594;
// System.Data.DataColumnPropertyDescriptor
struct DataColumnPropertyDescriptor_t1327003831;
// System.Data.Common.SchemaInfo
struct SchemaInfo_t609393215;

#include "mscorlib_System_Array2840145358.h"
#include "System_Data_System_Data_DataRow3654701923.h"
#include "System_Data_System_Data_DataColumn3354469747.h"
#include "System_Data_System_Data_DataTable2176726999.h"
#include "System_Data_System_Data_DataRelation1483987353.h"
#include "System_Data_System_Data_DbSourceMethodInfo3307618843.h"
#include "System_Data_System_Data_Common_DataColumnMapping2340601118.h"
#include "System_Data_System_Data_Common_DbConnection_Column4182633796.h"
#include "System_Data_System_Data_DataColumnPropertyDescript1327003831.h"
#include "System_Data_System_Data_Common_SchemaInfo609393215.h"
#include "System_Data_System_Data_DbType2586775211.h"

#pragma once
// System.Data.DataRow[]
struct DataRowU5BU5D_t1036778418  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DataRow_t3654701923 * m_Items[1];

public:
	inline DataRow_t3654701923 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataRow_t3654701923 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataRow_t3654701923 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Data.DataColumn[]
struct DataColumnU5BU5D_t3410743138  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DataColumn_t3354469747 * m_Items[1];

public:
	inline DataColumn_t3354469747 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataColumn_t3354469747 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataColumn_t3354469747 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Data.DataTable[]
struct DataTableU5BU5D_t1761989358  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DataTable_t2176726999 * m_Items[1];

public:
	inline DataTable_t2176726999 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataTable_t2176726999 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataTable_t2176726999 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Data.DataRelation[]
struct DataRelationU5BU5D_t909637604  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DataRelation_t1483987353 * m_Items[1];

public:
	inline DataRelation_t1483987353 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataRelation_t1483987353 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataRelation_t1483987353 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Data.DbSourceMethodInfo[]
struct DbSourceMethodInfoU5BU5D_t1609409306  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DbSourceMethodInfo_t3307618843 * m_Items[1];

public:
	inline DbSourceMethodInfo_t3307618843 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DbSourceMethodInfo_t3307618843 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DbSourceMethodInfo_t3307618843 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Data.Common.DataColumnMapping[]
struct DataColumnMappingU5BU5D_t198231211  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DataColumnMapping_t2340601118 * m_Items[1];

public:
	inline DataColumnMapping_t2340601118 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataColumnMapping_t2340601118 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataColumnMapping_t2340601118 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Data.IColumnMapping[]
struct IColumnMappingU5BU5D_t3180983095  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Data.Common.DbConnection/ColumnInfo[]
struct ColumnInfoU5BU5D_t3124166957  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ColumnInfo_t4182633796  m_Items[1];

public:
	inline ColumnInfo_t4182633796  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ColumnInfo_t4182633796 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ColumnInfo_t4182633796  value)
	{
		m_Items[index] = value;
	}
};
// System.Data.DataColumnPropertyDescriptor[]
struct DataColumnPropertyDescriptorU5BU5D_t218698382  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DataColumnPropertyDescriptor_t1327003831 * m_Items[1];

public:
	inline DataColumnPropertyDescriptor_t1327003831 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataColumnPropertyDescriptor_t1327003831 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataColumnPropertyDescriptor_t1327003831 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Data.Common.SchemaInfo[]
struct SchemaInfoU5BU5D_t3721122278  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) SchemaInfo_t609393215 * m_Items[1];

public:
	inline SchemaInfo_t609393215 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SchemaInfo_t609393215 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SchemaInfo_t609393215 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Data.DbType[]
struct DbTypeU5BU5D_t3028795978  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
