﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct DefaultComparer_t154402233;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_816448501.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m1089155954_gshared (DefaultComparer_t154402233 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1089155954(__this, method) ((  void (*) (DefaultComparer_t154402233 *, const MethodInfo*))DefaultComparer__ctor_m1089155954_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3951578109_gshared (DefaultComparer_t154402233 * __this, KeyValuePair_2_t816448501  ___x0, KeyValuePair_2_t816448501  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3951578109(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t154402233 *, KeyValuePair_2_t816448501 , KeyValuePair_2_t816448501 , const MethodInfo*))DefaultComparer_Compare_m3951578109_gshared)(__this, ___x0, ___y1, method)
