﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteFunctionAttribute>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3743553698(__this, ___l0, method) ((  void (*) (Enumerator_t191752712 *, List_1_t2105969720 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1944362608(__this, method) ((  void (*) (Enumerator_t191752712 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1831781990(__this, method) ((  Il2CppObject * (*) (Enumerator_t191752712 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteFunctionAttribute>::Dispose()
#define Enumerator_Dispose_m42823431(__this, method) ((  void (*) (Enumerator_t191752712 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteFunctionAttribute>::VerifyState()
#define Enumerator_VerifyState_m2043957952(__this, method) ((  void (*) (Enumerator_t191752712 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteFunctionAttribute>::MoveNext()
#define Enumerator_MoveNext_m3769894432(__this, method) ((  bool (*) (Enumerator_t191752712 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteFunctionAttribute>::get_Current()
#define Enumerator_get_Current_m2736628540(__this, method) ((  SqliteFunctionAttribute_t1309010751 * (*) (Enumerator_t191752712 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
