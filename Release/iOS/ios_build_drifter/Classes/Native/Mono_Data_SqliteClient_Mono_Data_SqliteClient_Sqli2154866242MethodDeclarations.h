﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.SqliteClient.SqliteTransaction
struct SqliteTransaction_t2154866242;
// System.Data.Common.DbConnection
struct DbConnection_t462757452;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_Common_DbConnection462757452.h"

// System.Void Mono.Data.SqliteClient.SqliteTransaction::.ctor()
extern "C"  void SqliteTransaction__ctor_m4193169641 (SqliteTransaction_t2154866242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteTransaction::SetConnection(System.Data.Common.DbConnection)
extern "C"  void SqliteTransaction_SetConnection_m1734324017 (SqliteTransaction_t2154866242 * __this, DbConnection_t462757452 * ___conn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteTransaction::Commit()
extern "C"  void SqliteTransaction_Commit_m941214770 (SqliteTransaction_t2154866242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteTransaction::Rollback()
extern "C"  void SqliteTransaction_Rollback_m1065083935 (SqliteTransaction_t2154866242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
