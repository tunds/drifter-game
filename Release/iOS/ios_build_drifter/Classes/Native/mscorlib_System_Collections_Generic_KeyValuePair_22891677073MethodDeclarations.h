﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<System.String>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1559248839(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2891677073 *, String_t*, List_1_t1765447871 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<System.String>>::get_Key()
#define KeyValuePair_2_get_Key_m3710981885(__this, method) ((  String_t* (*) (KeyValuePair_2_t2891677073 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<System.String>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m386537986(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2891677073 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<System.String>>::get_Value()
#define KeyValuePair_2_get_Value_m4080338222(__this, method) ((  List_1_t1765447871 * (*) (KeyValuePair_2_t2891677073 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<System.String>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3840187650(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2891677073 *, List_1_t1765447871 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<System.String>>::ToString()
#define KeyValuePair_2_ToString_m3650679584(__this, method) ((  String_t* (*) (KeyValuePair_2_t2891677073 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
