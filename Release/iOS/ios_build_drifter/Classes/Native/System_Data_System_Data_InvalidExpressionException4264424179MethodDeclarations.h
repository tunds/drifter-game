﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.InvalidExpressionException
struct InvalidExpressionException_t4264424179;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void System.Data.InvalidExpressionException::.ctor()
extern "C"  void InvalidExpressionException__ctor_m1056324849 (InvalidExpressionException_t4264424179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.InvalidExpressionException::.ctor(System.String)
extern "C"  void InvalidExpressionException__ctor_m3023548849 (InvalidExpressionException_t4264424179 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.InvalidExpressionException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void InvalidExpressionException__ctor_m1744704178 (InvalidExpressionException_t4264424179 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
