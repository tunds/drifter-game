﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteConnectionHandle
struct SqliteConnectionHandle_t2345657177;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection2345657177.h"

// System.Void Mono.Data.Sqlite.SqliteConnectionHandle::.ctor(System.IntPtr)
extern "C"  void SqliteConnectionHandle__ctor_m1711692859 (SqliteConnectionHandle_t2345657177 * __this, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteConnectionHandle::.ctor()
extern "C"  void SqliteConnectionHandle__ctor_m2800811193 (SqliteConnectionHandle_t2345657177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteConnectionHandle::ReleaseHandle()
extern "C"  bool SqliteConnectionHandle_ReleaseHandle_m2383941160 (SqliteConnectionHandle_t2345657177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteConnectionHandle::get_IsInvalid()
extern "C"  bool SqliteConnectionHandle_get_IsInvalid_m2997425053 (SqliteConnectionHandle_t2345657177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.SqliteConnectionHandle::op_Implicit(Mono.Data.Sqlite.SqliteConnectionHandle)
extern "C"  IntPtr_t SqliteConnectionHandle_op_Implicit_m1636750216 (Il2CppObject * __this /* static, unused */, SqliteConnectionHandle_t2345657177 * ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteConnectionHandle Mono.Data.Sqlite.SqliteConnectionHandle::op_Implicit(System.IntPtr)
extern "C"  SqliteConnectionHandle_t2345657177 * SqliteConnectionHandle_op_Implicit_m2230854114 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
