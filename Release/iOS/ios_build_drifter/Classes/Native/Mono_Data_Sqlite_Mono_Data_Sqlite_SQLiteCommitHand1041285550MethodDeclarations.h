﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteCommitHandler
struct SQLiteCommitHandler_t1041285550;
// System.Object
struct Il2CppObject;
// Mono.Data.Sqlite.CommitEventArgs
struct CommitEventArgs_t4249446247;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_CommitEventArgs4249446247.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void Mono.Data.Sqlite.SQLiteCommitHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SQLiteCommitHandler__ctor_m819876462 (SQLiteCommitHandler_t1041285550 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteCommitHandler::Invoke(System.Object,Mono.Data.Sqlite.CommitEventArgs)
extern "C"  void SQLiteCommitHandler_Invoke_m2708893475 (SQLiteCommitHandler_t1041285550 * __this, Il2CppObject * ___sender0, CommitEventArgs_t4249446247 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_SQLiteCommitHandler_t1041285550(Il2CppObject* delegate, Il2CppObject * ___sender0, CommitEventArgs_t4249446247 * ___e1);
// System.IAsyncResult Mono.Data.Sqlite.SQLiteCommitHandler::BeginInvoke(System.Object,Mono.Data.Sqlite.CommitEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SQLiteCommitHandler_BeginInvoke_m4219263240 (SQLiteCommitHandler_t1041285550 * __this, Il2CppObject * ___sender0, CommitEventArgs_t4249446247 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteCommitHandler::EndInvoke(System.IAsyncResult)
extern "C"  void SQLiteCommitHandler_EndInvoke_m3995609982 (SQLiteCommitHandler_t1041285550 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
