﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct DefaultComparer_t186827089;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K848873357.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m2926578481_gshared (DefaultComparer_t186827089 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2926578481(__this, method) ((  void (*) (DefaultComparer_t186827089 *, const MethodInfo*))DefaultComparer__ctor_m2926578481_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m818367462_gshared (DefaultComparer_t186827089 * __this, KeyInfo_t848873357  ___x0, KeyInfo_t848873357  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m818367462(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t186827089 *, KeyInfo_t848873357 , KeyInfo_t848873357 , const MethodInfo*))DefaultComparer_Compare_m818367462_gshared)(__this, ___x0, ___y1, method)
