﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Data.VersionNotFoundException
struct VersionNotFoundException_t1168984187;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Data.XmlDataInferenceLoader
struct XmlDataInferenceLoader_t1258518718;
// System.Data.DataSet
struct DataSet_t3654702571;
// System.Xml.XmlDocument
struct XmlDocument_t3705263098;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Xml.XmlElement
struct XmlElement_t3562928333;
// System.Data.TableMapping
struct TableMapping_t3011643123;
// System.Data.DataColumn
struct DataColumn_t3354469747;
// System.Type
struct Type_t;
// System.Collections.Hashtable
struct Hashtable_t3875263730;
// System.Collections.ArrayList
struct ArrayList_t2121638921;
// System.Object
struct Il2CppObject;
// System.Data.XmlDataReader
struct XmlDataReader_t4112562711;
// System.Xml.XmlReader
struct XmlReader_t4229084514;
// System.Data.DataRow
struct DataRow_t3654701923;
// System.Data.XmlDiffLoader
struct XmlDiffLoader_t3888299394;
// System.Data.DataTable
struct DataTable_t2176726999;
// System.Data.XmlSchemaDataImporter
struct XmlSchemaDataImporter_t2811591463;
// System.Data.TableAdapterSchemaInfo
struct TableAdapterSchemaInfo_t1131857475;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t471922321;
// System.Xml.Schema.XmlSchemaParticle
struct XmlSchemaParticle_t3696384587;
// System.Data.DataRelation
struct DataRelation_t1483987353;
// System.Data.RelationStructure
struct RelationStructure_t3039531114;
// System.Xml.Schema.XmlSchemaGroupBase
struct XmlSchemaGroupBase_t3990058885;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t2590121;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_t1191708721;
// System.Xml.Schema.XmlSchemaAnnotated
struct XmlSchemaAnnotated_t2513933869;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t176365656;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t1500525009;
// System.Xml.Schema.XmlSchemaIdentityConstraint
struct XmlSchemaIdentityConstraint_t3473808128;
// System.Data.ConstraintStructure
struct ConstraintStructure_t742574505;
// System.Xml.Schema.XmlSchemaKeyref
struct XmlSchemaKeyref_t2789194649;
// System.Data.UniqueConstraint
struct UniqueConstraint_t1006662241;
// System.Xml.Schema.XmlSchemaAnnotation
struct XmlSchemaAnnotation_t1377046772;
// System.Data.Common.DbProviderFactory
struct DbProviderFactory_t2435213707;
// System.Data.Common.DbCommand
struct DbCommand_t2323745021;
// System.Data.Common.DataTableMapping
struct DataTableMapping_t171110970;
// System.Data.XmlSchemaWriter
struct XmlSchemaWriter_t2141469886;
// System.Xml.XmlWriter
struct XmlWriter_t89522450;
// System.Data.DataTableCollection
struct DataTableCollection_t2915263893;
// System.Data.DataRelationCollection
struct DataRelationCollection_t267599063;
// System.Data.DataTable[]
struct DataTableU5BU5D_t1761989358;
// System.Data.DataRelation[]
struct DataRelationU5BU5D_t909637604;
// System.Globalization.CultureInfo
struct CultureInfo_t3603717042;
// System.Data.PropertyCollection
struct PropertyCollection_t3599376422;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t4226329727;
// System.MonoTODOAttribute
struct MonoTODOAttribute_t1287393901;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Data_System_Data_UpdateRowSource1763489119.h"
#include "System_Data_System_Data_UpdateRowSource1763489119MethodDeclarations.h"
#include "System_Data_System_Data_UpdateStatus4065415150.h"
#include "System_Data_System_Data_UpdateStatus4065415150MethodDeclarations.h"
#include "System_Data_System_Data_VersionNotFoundException1168984187.h"
#include "System_Data_System_Data_VersionNotFoundException1168984187MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "System_Data_Locale2281372282MethodDeclarations.h"
#include "System_Data_System_Data_DataException1022144856MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "System_Data_System_Data_XmlConstants2555109579.h"
#include "System_Data_System_Data_XmlConstants2555109579MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlQualifiedName176365656MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlQualifiedName176365656.h"
#include "System_Data_System_Data_XmlDataInferenceLoader1258518718.h"
#include "System_Data_System_Data_XmlDataInferenceLoader1258518718MethodDeclarations.h"
#include "System_Data_System_Data_DataSet3654702571.h"
#include "System_Xml_System_Xml_XmlDocument3705263098.h"
#include "System_Data_System_Data_XmlReadMode2944115491.h"
#include "mscorlib_ArrayTypes.h"
#include "System_Data_System_Data_TableMappingCollection4250141105MethodDeclarations.h"
#include "System_Data_System_Data_RelationStructureCollectio1505059432MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList2121638921MethodDeclarations.h"
#include "System_Data_System_Data_DataSet3654702571MethodDeclarations.h"
#include "System_Data_System_Data_TableMapping3011643123MethodDeclarations.h"
#include "System_Data_System_Data_DataTable2176726999.h"
#include "System_Data_System_Data_TableMappingCollection4250141105.h"
#include "System_Data_System_Data_RelationStructureCollectio1505059432.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"
#include "System_Data_System_Data_DataTableCollection2915263893.h"
#include "System_Data_System_Data_InternalDataCollectionBase2754805833MethodDeclarations.h"
#include "System_Data_System_Data_InternalDataCollectionBase2754805833.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Data_System_Data_TableMapping3011643123.h"
#include "mscorlib_System_Boolean211005341.h"
#include "System_Xml_System_Xml_XmlDocument3705263098MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "System_Data_XmlHelper69615237MethodDeclarations.h"
#include "System_Data_System_Data_DataColumn3354469747MethodDeclarations.h"
#include "System_Data_System_Data_DataTable2176726999MethodDeclarations.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "System_Data_System_Data_DataColumnCollection3528392753MethodDeclarations.h"
#include "System_Data_System_Data_DataTableCollection2915263893MethodDeclarations.h"
#include "System_Data_System_Data_DataRelation1483987353MethodDeclarations.h"
#include "System_Data_System_Data_DataRelationCollection267599063MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlElement3562928333.h"
#include "System_Xml_System_Xml_XmlNode3592213601.h"
#include "mscorlib_System_Int322847414787.h"
#include "System_Data_System_Data_DataColumn3354469747.h"
#include "System_Data_System_Data_RelationStructure3039531114.h"
#include "System_Data_System_Data_DataRelation1483987353.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042.h"
#include "System_Xml_System_Xml_XmlNode3592213601MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "System_Xml_System_Xml_XmlNodeList3966370975.h"
#include "System_Xml_System_Xml_XmlNodeList3966370975MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNodeType3966624571.h"
#include "mscorlib_System_Collections_CollectionBase851261505MethodDeclarations.h"
#include "mscorlib_System_Collections_CollectionBase851261505.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "System_Data_System_Data_MappingType1033973435.h"
#include "System_Data_System_Data_DataColumnCollection3528392753.h"
#include "System_Data_System_Data_DataException1022144856.h"
#include "System.Data_ArrayTypes.h"
#include "System_Data_System_Data_DataRelationCollection267599063.h"
#include "System_Data_System_Data_RelationStructure3039531114MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlAttribute2022155821.h"
#include "System_Data_System_Data_ElementMappingType2531382879.h"
#include "System_Xml_System_Xml_XmlAttributeCollection571717291.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap1329997280MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap1329997280.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "mscorlib_System_Collections_Hashtable3875263730MethodDeclarations.h"
#include "System_Data_System_Data_XmlDataLoader3950023015.h"
#include "System_Data_System_Data_XmlDataLoader3950023015MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlConvert1882388356MethodDeclarations.h"
#include "mscorlib_System_Convert1097883944MethodDeclarations.h"
#include "mscorlib_System_TypeCode2164429820.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_DateTime339033936.h"
#include "System_Xml_System_Xml_XmlDateTimeSerializationMode167003369.h"
#include "mscorlib_System_Decimal1688557254.h"
#include "mscorlib_System_Double534516614.h"
#include "mscorlib_System_Int162847414729.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_SByte2855346064.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_UInt16985925268.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_UInt64985925421.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_Guid2778838590.h"
#include "System_Data_System_Data_XmlDataReader4112562711.h"
#include "System_Data_System_Data_XmlDataReader4112562711MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlReader4229084514.h"
#include "System_Xml_System_Xml_XmlReader4229084514MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNodeReader3079406212MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNodeReader3079406212.h"
#include "System_Data_System_Data_DataRowCollection1405583905MethodDeclarations.h"
#include "System_Data_System_Data_DataRow3654701923.h"
#include "System_Data_System_Data_DataRowCollection1405583905.h"
#include "System_Data_System_Data_DataRow3654701923MethodDeclarations.h"
#include "mscorlib_System_Activator690001546MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlException3490696160.h"
#include "System_Data_System_Data_XmlDiffLoader3888299394.h"
#include "System_Data_System_Data_XmlDiffLoader3888299394MethodDeclarations.h"
#include "System_Data_System_Data_DataRowVersion2975473339.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "System_Data_System_Data_DataRowState2071073396.h"
#include "System_Data_System_Data_XmlReadMode2944115491MethodDeclarations.h"
#include "System_Data_System_Data_XmlSchemaDataImporter2811591463.h"
#include "System_Data_System_Data_XmlSchemaDataImporter2811591463MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchema1932230565MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchema1932230565.h"
#include "System_Xml_System_Xml_Schema_ValidationEventHandle2777264566.h"
#include "System_Data_System_Data_TableAdapterSchemaInfo1131857475.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectCollec2238201602MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectEnumer3058853928MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaElement471922321MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectEnumer3058853928.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObject2900481284.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaElement471922321.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectCollec2238201602.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType3432810239.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexType1860629407.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaKeyref2789194649.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaIdentityCons3473808128.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotation1377046772.h"
#include "System_Data_System_Data_ConstraintStructure742574505.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2513933869MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable167066468MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexType1860629407MethodDeclarations.h"
#include "System.Xml_ArrayTypes.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaGroupBase3990058885.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle3696384587.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable167066468.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaGroupBase3990058885MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentType1065858792.h"
#include "System_Data_System_Data_PropertyCollection3599376422.h"
#include "System_Data_System_Data_TableStructure1082771224MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle3696384587MethodDeclarations.h"
#include "mscorlib_System_Decimal1688557254MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType3432810239MethodDeclarations.h"
#include "mscorlib_System_Collections_SortedList3322490541MethodDeclarations.h"
#include "System_Data_System_Data_TableStructure1082771224.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"
#include "mscorlib_System_Collections_SortedList3322490541.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttribute1191708721.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype2590121.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType1500525009.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttribute1191708721MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUse3827175708.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2513933869.h"
#include "mscorlib_System_Boolean211005341MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType1500525009MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeRes409858485MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet4078749516MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeRes409858485.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet4078749516.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeCo1763168354.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMaxLengthFac2333687858.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype2590121MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaIdentityCons3473808128MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaXPath4094864978MethodDeclarations.h"
#include "System_Data_System_Data_ConstraintStructure742574505MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaXPath4094864978.h"
#include "System_Data_System_Data_UniqueConstraint1006662241MethodDeclarations.h"
#include "System_Data_System_Data_ConstraintCollection392455726MethodDeclarations.h"
#include "System_Data_System_Data_ConstraintCollection392455726.h"
#include "System_Data_System_Data_UniqueConstraint1006662241.h"
#include "System_Data_System_Data_Constraint2349953968.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaKeyref2789194649MethodDeclarations.h"
#include "System_Data_System_Data_ForeignKeyConstraint1848099579MethodDeclarations.h"
#include "System_Data_System_Data_ForeignKeyConstraint1848099579.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotation1377046772MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAppInfo1340486148MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAppInfo1340486148.h"
#include "System_Data_System_Data_Common_DbProviderFactories3895152489MethodDeclarations.h"
#include "System_Data_System_Data_Common_DbProviderFactory2435213707.h"
#include "System_Xml_System_Xml_XmlElement3562928333MethodDeclarations.h"
#include "System_Data_System_Data_TableAdapterSchemaInfo1131857475MethodDeclarations.h"
#include "System_Data_System_Data_Common_DataTableMapping171110970MethodDeclarations.h"
#include "System_Data_System_Data_Common_DataAdapter2395949141MethodDeclarations.h"
#include "System_Data_System_Data_Common_DataTableMappingCol2256861304MethodDeclarations.h"
#include "System_Data_System_Data_Common_DataTableMapping171110970.h"
#include "System_Data_System_Data_Common_DbDataAdapter3684585719.h"
#include "System_Data_System_Data_Common_DataTableMappingCol2256861304.h"
#include "System_Data_System_Data_DbCommandInfo2041793742MethodDeclarations.h"
#include "mscorlib_System_Enum2778772662MethodDeclarations.h"
#include "System_Data_System_Data_DbSourceMethodInfo3307618843MethodDeclarations.h"
#include "System_Data_System_Data_Common_DbDataAdapter3684585719MethodDeclarations.h"
#include "System_Data_System_Data_DbCommandInfo2041793742.h"
#include "System_Data_System_Data_DbSourceMethodInfo3307618843.h"
#include "System_Data_System_Data_GenerateMethodsType1113224938.h"
#include "System_Data_System_Data_Common_DbCommand2323745021.h"
#include "System_Data_System_Data_Common_DbCommand2323745021MethodDeclarations.h"
#include "System_Data_System_Data_Common_DbProviderFactory2435213707MethodDeclarations.h"
#include "System_Data_System_Data_CommandType753495736.h"
#include "System_Data_System_Data_Common_DbParameterCollecti3381130713.h"
#include "System_Data_System_Data_Common_DbParameterCollecti3381130713MethodDeclarations.h"
#include "System_Data_System_Data_Common_DbParameter3306161371.h"
#include "System_Data_System_Data_Common_DbParameter3306161371MethodDeclarations.h"
#include "System_Data_System_Data_ParameterDirection608485289.h"
#include "System_Data_System_Data_Common_DataColumnMappingColl30373468MethodDeclarations.h"
#include "System_Data_System_Data_Common_DataColumnMappingColl30373468.h"
#include "System_Data_System_Data_Common_DataColumnMapping2340601118.h"
#include "System_Data_System_Data_XmlSchemaWriter2141469886.h"
#include "System_Data_System_Data_XmlSchemaWriter2141469886MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlWriter89522450.h"
#include "System_Data_System_Data_PropertyCollection3599376422MethodDeclarations.h"
#include "System_System_Collections_Specialized_ListDictiona4226329727MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlWriter89522450MethodDeclarations.h"
#include "System_System_Collections_Specialized_ListDictiona4226329727.h"
#include "System_Data_System_Data_Constraint2349953968MethodDeclarations.h"
#include "System_Data_System_Data_XmlWriteMode1570852254.h"
#include "System_Data_System_Data_XmlWriteMode1570852254MethodDeclarations.h"
#include "System_Data_System_MonoTODOAttribute1287393896.h"
#include "System_Data_System_MonoTODOAttribute1287393896MethodDeclarations.h"
#include "mscorlib_System_Attribute498693649MethodDeclarations.h"
#include "System_Data_XmlHelper69615237.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Data.VersionNotFoundException::.ctor()
extern Il2CppCodeGenString* _stringLiteral4045803389;
extern const uint32_t VersionNotFoundException__ctor_m2616865769_MetadataUsageId;
extern "C"  void VersionNotFoundException__ctor_m2616865769 (VersionNotFoundException_t1168984187 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VersionNotFoundException__ctor_m2616865769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m1156491176(NULL /*static, unused*/, _stringLiteral4045803389, /*hidden argument*/NULL);
		DataException__ctor_m829863666(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.VersionNotFoundException::.ctor(System.String)
extern "C"  void VersionNotFoundException__ctor_m3266235833 (VersionNotFoundException_t1168984187 * __this, String_t* ___s0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___s0;
		DataException__ctor_m829863666(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.VersionNotFoundException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void VersionNotFoundException__ctor_m2204542378 (VersionNotFoundException_t1168984187 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info0;
		StreamingContext_t986364934  L_1 = ___context1;
		DataException__ctor_m2024360785(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlConstants::.cctor()
extern Il2CppClass* XmlQualifiedName_t176365656_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlConstants_t2555109579_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3402981393;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern Il2CppCodeGenString* _stringLiteral109413500;
extern Il2CppCodeGenString* _stringLiteral104431;
extern Il2CppCodeGenString* _stringLiteral3327612;
extern Il2CppCodeGenString* _stringLiteral64711720;
extern Il2CppCodeGenString* _stringLiteral1141225885;
extern Il2CppCodeGenString* _stringLiteral3052374;
extern Il2CppCodeGenString* _stringLiteral1792749467;
extern Il2CppCodeGenString* _stringLiteral1542263633;
extern Il2CppCodeGenString* _stringLiteral2969009105;
extern Il2CppCodeGenString* _stringLiteral3039496;
extern Il2CppCodeGenString* _stringLiteral97526364;
extern Il2CppCodeGenString* _stringLiteral2302954900;
extern Il2CppCodeGenString* _stringLiteral1033453191;
extern Il2CppCodeGenString* _stringLiteral1145198778;
extern Il2CppCodeGenString* _stringLiteral1141514001;
extern Il2CppCodeGenString* _stringLiteral2882303968;
extern Il2CppCodeGenString* _stringLiteral3078954544;
extern Il2CppCodeGenString* _stringLiteral77225596;
extern const uint32_t XmlConstants__cctor_m4278539316_MetadataUsageId;
extern "C"  void XmlConstants__cctor_m4278539316 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlConstants__cctor_m4278539316_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlQualifiedName_t176365656 * L_0 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_0, _stringLiteral3402981393, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnString_0(L_0);
		XmlQualifiedName_t176365656 * L_1 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_1, _stringLiteral109413500, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnShort_1(L_1);
		XmlQualifiedName_t176365656 * L_2 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_2, _stringLiteral104431, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnInt_2(L_2);
		XmlQualifiedName_t176365656 * L_3 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_3, _stringLiteral3327612, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnLong_3(L_3);
		XmlQualifiedName_t176365656 * L_4 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_4, _stringLiteral64711720, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnBoolean_4(L_4);
		XmlQualifiedName_t176365656 * L_5 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_5, _stringLiteral1141225885, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnUnsignedByte_5(L_5);
		XmlQualifiedName_t176365656 * L_6 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_6, _stringLiteral3052374, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnChar_6(L_6);
		XmlQualifiedName_t176365656 * L_7 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_7, _stringLiteral1792749467, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnDateTime_7(L_7);
		XmlQualifiedName_t176365656 * L_8 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_8, _stringLiteral1542263633, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnDecimal_8(L_8);
		XmlQualifiedName_t176365656 * L_9 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_9, _stringLiteral2969009105, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnDouble_9(L_9);
		XmlQualifiedName_t176365656 * L_10 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_10, _stringLiteral3039496, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnSbyte_10(L_10);
		XmlQualifiedName_t176365656 * L_11 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_11, _stringLiteral97526364, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnFloat_11(L_11);
		XmlQualifiedName_t176365656 * L_12 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_12, _stringLiteral2302954900, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnDuration_12(L_12);
		XmlQualifiedName_t176365656 * L_13 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_13, _stringLiteral1033453191, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnUnsignedShort_13(L_13);
		XmlQualifiedName_t176365656 * L_14 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_14, _stringLiteral1145198778, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnUnsignedInt_14(L_14);
		XmlQualifiedName_t176365656 * L_15 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_15, _stringLiteral1141514001, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnUnsignedLong_15(L_15);
		XmlQualifiedName_t176365656 * L_16 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_16, _stringLiteral2882303968, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnUri_16(L_16);
		XmlQualifiedName_t176365656 * L_17 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_17, _stringLiteral3078954544, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnBase64Binary_17(L_17);
		XmlQualifiedName_t176365656 * L_18 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_18, _stringLiteral77225596, _stringLiteral1440052060, /*hidden argument*/NULL);
		((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->set_QnXmlQualifiedName_18(L_18);
		return;
	}
}
// System.Void System.Data.XmlDataInferenceLoader::.ctor(System.Data.DataSet,System.Xml.XmlDocument,System.Data.XmlReadMode,System.String[])
extern Il2CppClass* TableMappingCollection_t4250141105_il2cpp_TypeInfo_var;
extern Il2CppClass* RelationStructureCollection_t1505059432_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* DataTable_t2176726999_il2cpp_TypeInfo_var;
extern Il2CppClass* TableMapping_t3011643123_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t XmlDataInferenceLoader__ctor_m3485632556_MetadataUsageId;
extern "C"  void XmlDataInferenceLoader__ctor_m3485632556 (XmlDataInferenceLoader_t1258518718 * __this, DataSet_t3654702571 * ___ds0, XmlDocument_t3705263098 * ___doc1, int32_t ___mode2, StringU5BU5D_t2956870243* ___ignoredNamespaces3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataInferenceLoader__ctor_m3485632556_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	DataTable_t2176726999 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	XmlDataInferenceLoader_t1258518718 * G_B2_0 = NULL;
	XmlDataInferenceLoader_t1258518718 * G_B1_0 = NULL;
	ArrayList_t2121638921 * G_B3_0 = NULL;
	XmlDataInferenceLoader_t1258518718 * G_B3_1 = NULL;
	{
		TableMappingCollection_t4250141105 * L_0 = (TableMappingCollection_t4250141105 *)il2cpp_codegen_object_new(TableMappingCollection_t4250141105_il2cpp_TypeInfo_var);
		TableMappingCollection__ctor_m858778355(L_0, /*hidden argument*/NULL);
		__this->set_tables_4(L_0);
		RelationStructureCollection_t1505059432 * L_1 = (RelationStructureCollection_t1505059432 *)il2cpp_codegen_object_new(RelationStructureCollection_t1505059432_il2cpp_TypeInfo_var);
		RelationStructureCollection__ctor_m2201525120(L_1, /*hidden argument*/NULL);
		__this->set_relations_5(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		DataSet_t3654702571 * L_2 = ___ds0;
		__this->set_dataset_0(L_2);
		XmlDocument_t3705263098 * L_3 = ___doc1;
		__this->set_document_1(L_3);
		int32_t L_4 = ___mode2;
		__this->set_mode_2(L_4);
		StringU5BU5D_t2956870243* L_5 = ___ignoredNamespaces3;
		G_B1_0 = __this;
		if (!L_5)
		{
			G_B2_0 = __this;
			goto IL_0045;
		}
	}
	{
		StringU5BU5D_t2956870243* L_6 = ___ignoredNamespaces3;
		ArrayList_t2121638921 * L_7 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m2944534244(L_7, (Il2CppObject *)(Il2CppObject *)L_6, /*hidden argument*/NULL);
		G_B3_0 = L_7;
		G_B3_1 = G_B1_0;
		goto IL_004a;
	}

IL_0045:
	{
		ArrayList_t2121638921 * L_8 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_8, /*hidden argument*/NULL);
		G_B3_0 = L_8;
		G_B3_1 = G_B2_0;
	}

IL_004a:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_ignoredNamespaces_3(G_B3_0);
		DataSet_t3654702571 * L_9 = __this->get_dataset_0();
		NullCheck(L_9);
		DataTableCollection_t2915263893 * L_10 = DataSet_get_Tables_m87321279(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Il2CppObject * L_11 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* System.Collections.IEnumerator System.Data.InternalDataCollectionBase::GetEnumerator() */, L_10);
		V_0 = L_11;
	}

IL_0060:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0082;
		}

IL_0065:
		{
			Il2CppObject * L_12 = V_0;
			NullCheck(L_12);
			Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_12);
			V_1 = ((DataTable_t2176726999 *)CastclassClass(L_13, DataTable_t2176726999_il2cpp_TypeInfo_var));
			TableMappingCollection_t4250141105 * L_14 = __this->get_tables_4();
			DataTable_t2176726999 * L_15 = V_1;
			TableMapping_t3011643123 * L_16 = (TableMapping_t3011643123 *)il2cpp_codegen_object_new(TableMapping_t3011643123_il2cpp_TypeInfo_var);
			TableMapping__ctor_m738931010(L_16, L_15, /*hidden argument*/NULL);
			NullCheck(L_14);
			TableMappingCollection_Add_m3664613133(L_14, L_16, /*hidden argument*/NULL);
		}

IL_0082:
		{
			Il2CppObject * L_17 = V_0;
			NullCheck(L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_0065;
			}
		}

IL_008d:
		{
			IL2CPP_LEAVE(0xA6, FINALLY_0092);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0092;
	}

FINALLY_0092:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_19 = V_0;
			Il2CppObject * L_20 = ((Il2CppObject *)IsInst(L_19, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_2 = L_20;
			if (!L_20)
			{
				goto IL_00a5;
			}
		}

IL_009f:
		{
			Il2CppObject * L_21 = V_2;
			NullCheck(L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_21);
		}

IL_00a5:
		{
			IL2CPP_END_FINALLY(146)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(146)
	{
		IL2CPP_JUMP_TBL(0xA6, IL_00a6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a6:
	{
		return;
	}
}
// System.Void System.Data.XmlDataInferenceLoader::Infer(System.Data.DataSet,System.Xml.XmlDocument,System.Data.XmlReadMode,System.String[])
extern Il2CppClass* XmlDataInferenceLoader_t1258518718_il2cpp_TypeInfo_var;
extern const uint32_t XmlDataInferenceLoader_Infer_m3654205208_MetadataUsageId;
extern "C"  void XmlDataInferenceLoader_Infer_m3654205208 (Il2CppObject * __this /* static, unused */, DataSet_t3654702571 * ___dataset0, XmlDocument_t3705263098 * ___document1, int32_t ___mode2, StringU5BU5D_t2956870243* ___ignoredNamespaces3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataInferenceLoader_Infer_m3654205208_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DataSet_t3654702571 * L_0 = ___dataset0;
		XmlDocument_t3705263098 * L_1 = ___document1;
		int32_t L_2 = ___mode2;
		StringU5BU5D_t2956870243* L_3 = ___ignoredNamespaces3;
		XmlDataInferenceLoader_t1258518718 * L_4 = (XmlDataInferenceLoader_t1258518718 *)il2cpp_codegen_object_new(XmlDataInferenceLoader_t1258518718_il2cpp_TypeInfo_var);
		XmlDataInferenceLoader__ctor_m3485632556(L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		XmlDataInferenceLoader_ReadXml_m1710146885(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlDataInferenceLoader::ReadXml()
extern const Il2CppType* Int32_t2847414787_0_0_0_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNode_t3592213601_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlElement_t3562928333_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* TableMapping_t3011643123_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DataColumn_t3354469747_il2cpp_TypeInfo_var;
extern Il2CppClass* RelationStructure_t3039531114_il2cpp_TypeInfo_var;
extern Il2CppClass* DataException_t1022144856_il2cpp_TypeInfo_var;
extern Il2CppClass* DataRelation_t1483987353_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96598594;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern Il2CppCodeGenString* _stringLiteral2645413672;
extern Il2CppCodeGenString* _stringLiteral93658;
extern Il2CppCodeGenString* _stringLiteral95;
extern Il2CppCodeGenString* _stringLiteral3767428400;
extern Il2CppCodeGenString* _stringLiteral3235462334;
extern Il2CppCodeGenString* _stringLiteral3411508068;
extern Il2CppCodeGenString* _stringLiteral2879542002;
extern const uint32_t XmlDataInferenceLoader_ReadXml_m1710146885_MetadataUsageId;
extern "C"  void XmlDataInferenceLoader_ReadXml_m1710146885 (XmlDataInferenceLoader_t1258518718 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataInferenceLoader_ReadXml_m1710146885_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlElement_t3562928333 * V_0 = NULL;
	String_t* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	XmlNode_t3592213601 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	int32_t V_5 = 0;
	Il2CppObject * V_6 = NULL;
	TableMapping_t3011643123 * V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	Il2CppObject * V_10 = NULL;
	TableMapping_t3011643123 * V_11 = NULL;
	Il2CppObject * V_12 = NULL;
	Il2CppObject * V_13 = NULL;
	Il2CppObject * V_14 = NULL;
	TableMapping_t3011643123 * V_15 = NULL;
	Il2CppObject * V_16 = NULL;
	DataColumn_t3354469747 * V_17 = NULL;
	Il2CppObject * V_18 = NULL;
	Il2CppObject * V_19 = NULL;
	DataColumn_t3354469747 * V_20 = NULL;
	Il2CppObject * V_21 = NULL;
	Il2CppObject * V_22 = NULL;
	Il2CppObject * V_23 = NULL;
	RelationStructure_t3039531114 * V_24 = NULL;
	String_t* V_25 = NULL;
	DataTable_t2176726999 * V_26 = NULL;
	DataTable_t2176726999 * V_27 = NULL;
	DataColumn_t3354469747 * V_28 = NULL;
	DataColumn_t3354469747 * V_29 = NULL;
	DataRelation_t1483987353 * V_30 = NULL;
	Il2CppObject * V_31 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B22_0 = NULL;
	int32_t G_B30_0 = 0;
	String_t* G_B30_1 = NULL;
	String_t* G_B30_2 = NULL;
	String_t* G_B30_3 = NULL;
	TableMapping_t3011643123 * G_B30_4 = NULL;
	XmlDataInferenceLoader_t1258518718 * G_B30_5 = NULL;
	TableMapping_t3011643123 * G_B30_6 = NULL;
	int32_t G_B29_0 = 0;
	String_t* G_B29_1 = NULL;
	String_t* G_B29_2 = NULL;
	String_t* G_B29_3 = NULL;
	TableMapping_t3011643123 * G_B29_4 = NULL;
	XmlDataInferenceLoader_t1258518718 * G_B29_5 = NULL;
	TableMapping_t3011643123 * G_B29_6 = NULL;
	Type_t * G_B31_0 = NULL;
	int32_t G_B31_1 = 0;
	String_t* G_B31_2 = NULL;
	String_t* G_B31_3 = NULL;
	String_t* G_B31_4 = NULL;
	TableMapping_t3011643123 * G_B31_5 = NULL;
	XmlDataInferenceLoader_t1258518718 * G_B31_6 = NULL;
	TableMapping_t3011643123 * G_B31_7 = NULL;
	String_t* G_B79_0 = NULL;
	{
		XmlDocument_t3705263098 * L_0 = __this->get_document_1();
		NullCheck(L_0);
		XmlElement_t3562928333 * L_1 = XmlDocument_get_DocumentElement_m3688713126(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		DataSet_t3654702571 * L_2 = __this->get_dataset_0();
		CultureInfo_t3603717042 * L_3 = (CultureInfo_t3603717042 *)il2cpp_codegen_object_new(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo__ctor_m3707520096(L_3, _stringLiteral96598594, /*hidden argument*/NULL);
		NullCheck(L_2);
		DataSet_set_Locale_m2876356750(L_2, L_3, /*hidden argument*/NULL);
		XmlDocument_t3705263098 * L_4 = __this->get_document_1();
		NullCheck(L_4);
		XmlElement_t3562928333 * L_5 = XmlDocument_get_DocumentElement_m3688713126(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		XmlElement_t3562928333 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_7, _stringLiteral1440052060, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0052;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_9 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, _stringLiteral2645413672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0052:
	{
		bool L_10 = XmlDataInferenceLoader_IsDocumentElementTable_m1972492031(__this, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0069;
		}
	}
	{
		XmlElement_t3562928333 * L_11 = V_0;
		XmlDataInferenceLoader_InferTopLevelTable_m596841832(__this, L_11, /*hidden argument*/NULL);
		goto IL_0118;
	}

IL_0069:
	{
		XmlElement_t3562928333 * L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_12);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_14 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		DataSet_t3654702571 * L_15 = __this->get_dataset_0();
		String_t* L_16 = V_1;
		NullCheck(L_15);
		DataSet_set_DataSetName_m1679911553(L_15, L_16, /*hidden argument*/NULL);
		DataSet_t3654702571 * L_17 = __this->get_dataset_0();
		XmlElement_t3562928333 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_18);
		NullCheck(L_17);
		DataSet_set_Namespace_m1090260457(L_17, L_19, /*hidden argument*/NULL);
		DataSet_t3654702571 * L_20 = __this->get_dataset_0();
		XmlElement_t3562928333 * L_21 = V_0;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.Xml.XmlNode::get_Prefix() */, L_21);
		NullCheck(L_20);
		DataSet_set_Prefix_m1381159084(L_20, L_22, /*hidden argument*/NULL);
		XmlElement_t3562928333 * L_23 = V_0;
		NullCheck(L_23);
		XmlNodeList_t3966370975 * L_24 = VirtFuncInvoker0< XmlNodeList_t3966370975 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_23);
		NullCheck(L_24);
		Il2CppObject * L_25 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_24);
		V_2 = L_25;
	}

IL_00af:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00f2;
		}

IL_00b4:
		{
			Il2CppObject * L_26 = V_2;
			NullCheck(L_26);
			Il2CppObject * L_27 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_26);
			V_3 = ((XmlNode_t3592213601 *)CastclassClass(L_27, XmlNode_t3592213601_il2cpp_TypeInfo_var));
			XmlNode_t3592213601 * L_28 = V_3;
			NullCheck(L_28);
			String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_28);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_30 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_29, _stringLiteral1440052060, /*hidden argument*/NULL);
			if (!L_30)
			{
				goto IL_00da;
			}
		}

IL_00d5:
		{
			goto IL_00f2;
		}

IL_00da:
		{
			XmlNode_t3592213601 * L_31 = V_3;
			NullCheck(L_31);
			int32_t L_32 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Xml.XmlNodeType System.Xml.XmlNode::get_NodeType() */, L_31);
			if ((!(((uint32_t)L_32) == ((uint32_t)1))))
			{
				goto IL_00f2;
			}
		}

IL_00e6:
		{
			XmlNode_t3592213601 * L_33 = V_3;
			XmlDataInferenceLoader_InferTopLevelTable_m596841832(__this, ((XmlElement_t3562928333 *)IsInstClass(L_33, XmlElement_t3562928333_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		}

IL_00f2:
		{
			Il2CppObject * L_34 = V_2;
			NullCheck(L_34);
			bool L_35 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_34);
			if (L_35)
			{
				goto IL_00b4;
			}
		}

IL_00fd:
		{
			IL2CPP_LEAVE(0x118, FINALLY_0102);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0102;
	}

FINALLY_0102:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_36 = V_2;
			Il2CppObject * L_37 = ((Il2CppObject *)IsInst(L_36, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_4 = L_37;
			if (!L_37)
			{
				goto IL_0117;
			}
		}

IL_0110:
		{
			Il2CppObject * L_38 = V_4;
			NullCheck(L_38);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_38);
		}

IL_0117:
		{
			IL2CPP_END_FINALLY(258)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(258)
	{
		IL2CPP_JUMP_TBL(0x118, IL_0118)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0118:
	{
		V_5 = 0;
		TableMappingCollection_t4250141105 * L_39 = __this->get_tables_4();
		NullCheck(L_39);
		Il2CppObject * L_40 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* System.Collections.IEnumerator System.Collections.CollectionBase::GetEnumerator() */, L_39);
		V_6 = L_40;
	}

IL_0128:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0272;
		}

IL_012d:
		{
			Il2CppObject * L_41 = V_6;
			NullCheck(L_41);
			Il2CppObject * L_42 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_41);
			V_7 = ((TableMapping_t3011643123 *)CastclassClass(L_42, TableMapping_t3011643123_il2cpp_TypeInfo_var));
			TableMapping_t3011643123 * L_43 = V_7;
			NullCheck(L_43);
			DataColumn_t3354469747 * L_44 = L_43->get_PrimaryKey_5();
			if (!L_44)
			{
				goto IL_0158;
			}
		}

IL_0147:
		{
			TableMapping_t3011643123 * L_45 = V_7;
			NullCheck(L_45);
			DataColumn_t3354469747 * L_46 = L_45->get_PrimaryKey_5();
			NullCheck(L_46);
			String_t* L_47 = DataColumn_get_ColumnName_m409531680(L_46, /*hidden argument*/NULL);
			G_B22_0 = L_47;
			goto IL_016e;
		}

IL_0158:
		{
			TableMapping_t3011643123 * L_48 = V_7;
			NullCheck(L_48);
			DataTable_t2176726999 * L_49 = L_48->get_Table_1();
			NullCheck(L_49);
			String_t* L_50 = DataTable_get_TableName_m3141812994(L_49, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_51 = String_Concat_m138640077(NULL /*static, unused*/, L_50, _stringLiteral93658, /*hidden argument*/NULL);
			G_B22_0 = L_51;
		}

IL_016e:
		{
			V_8 = G_B22_0;
			String_t* L_52 = V_8;
			V_9 = L_52;
			TableMapping_t3011643123 * L_53 = V_7;
			NullCheck(L_53);
			TableMappingCollection_t4250141105 * L_54 = L_53->get_ChildTables_9();
			TableMapping_t3011643123 * L_55 = V_7;
			NullCheck(L_55);
			DataTable_t2176726999 * L_56 = L_55->get_Table_1();
			NullCheck(L_56);
			String_t* L_57 = DataTable_get_TableName_m3141812994(L_56, /*hidden argument*/NULL);
			NullCheck(L_54);
			TableMapping_t3011643123 * L_58 = TableMappingCollection_get_Item_m2768450663(L_54, L_57, /*hidden argument*/NULL);
			if (!L_58)
			{
				goto IL_01d8;
			}
		}

IL_0191:
		{
			String_t* L_59 = V_8;
			uint16_t L_60 = ((uint16_t)((int32_t)95));
			Il2CppObject * L_61 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_60);
			int32_t L_62 = V_5;
			int32_t L_63 = L_62;
			Il2CppObject * L_64 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_63);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_65 = String_Concat_m2809334143(NULL /*static, unused*/, L_59, L_61, L_64, /*hidden argument*/NULL);
			V_9 = L_65;
			goto IL_01ca;
		}

IL_01ad:
		{
			int32_t L_66 = V_5;
			V_5 = ((int32_t)((int32_t)L_66+(int32_t)1));
			String_t* L_67 = V_8;
			uint16_t L_68 = ((uint16_t)((int32_t)95));
			Il2CppObject * L_69 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_68);
			int32_t L_70 = V_5;
			int32_t L_71 = L_70;
			Il2CppObject * L_72 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_71);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_73 = String_Concat_m2809334143(NULL /*static, unused*/, L_67, L_69, L_72, /*hidden argument*/NULL);
			V_9 = L_73;
		}

IL_01ca:
		{
			TableMapping_t3011643123 * L_74 = V_7;
			String_t* L_75 = V_9;
			NullCheck(L_74);
			DataColumn_t3354469747 * L_76 = TableMapping_GetColumn_m1258267765(L_74, L_75, /*hidden argument*/NULL);
			if (L_76)
			{
				goto IL_01ad;
			}
		}

IL_01d8:
		{
			TableMapping_t3011643123 * L_77 = V_7;
			NullCheck(L_77);
			TableMappingCollection_t4250141105 * L_78 = L_77->get_ChildTables_9();
			NullCheck(L_78);
			Il2CppObject * L_79 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* System.Collections.IEnumerator System.Collections.CollectionBase::GetEnumerator() */, L_78);
			V_10 = L_79;
		}

IL_01e6:
		try
		{ // begin try (depth: 2)
			{
				goto IL_024a;
			}

IL_01eb:
			{
				Il2CppObject * L_80 = V_10;
				NullCheck(L_80);
				Il2CppObject * L_81 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_80);
				V_11 = ((TableMapping_t3011643123 *)CastclassClass(L_81, TableMapping_t3011643123_il2cpp_TypeInfo_var));
				TableMapping_t3011643123 * L_82 = V_11;
				TableMapping_t3011643123 * L_83 = V_11;
				String_t* L_84 = V_9;
				TableMapping_t3011643123 * L_85 = V_7;
				NullCheck(L_85);
				DataTable_t2176726999 * L_86 = L_85->get_Table_1();
				NullCheck(L_86);
				String_t* L_87 = DataTable_get_Prefix_m494218059(L_86, /*hidden argument*/NULL);
				TableMapping_t3011643123 * L_88 = V_7;
				NullCheck(L_88);
				DataTable_t2176726999 * L_89 = L_88->get_Table_1();
				NullCheck(L_89);
				String_t* L_90 = DataTable_get_Namespace_m3829929060(L_89, /*hidden argument*/NULL);
				TableMapping_t3011643123 * L_91 = V_7;
				NullCheck(L_91);
				DataColumn_t3354469747 * L_92 = L_91->get_PrimaryKey_5();
				G_B29_0 = 4;
				G_B29_1 = L_90;
				G_B29_2 = L_87;
				G_B29_3 = L_84;
				G_B29_4 = L_83;
				G_B29_5 = __this;
				G_B29_6 = L_82;
				if (!L_92)
				{
					G_B30_0 = 4;
					G_B30_1 = L_90;
					G_B30_2 = L_87;
					G_B30_3 = L_84;
					G_B30_4 = L_83;
					G_B30_5 = __this;
					G_B30_6 = L_82;
					goto IL_0236;
				}
			}

IL_0225:
			{
				TableMapping_t3011643123 * L_93 = V_7;
				NullCheck(L_93);
				DataColumn_t3354469747 * L_94 = L_93->get_PrimaryKey_5();
				NullCheck(L_94);
				Type_t * L_95 = DataColumn_get_DataType_m3376662490(L_94, /*hidden argument*/NULL);
				G_B31_0 = L_95;
				G_B31_1 = G_B29_0;
				G_B31_2 = G_B29_1;
				G_B31_3 = G_B29_2;
				G_B31_4 = G_B29_3;
				G_B31_5 = G_B29_4;
				G_B31_6 = G_B29_5;
				G_B31_7 = G_B29_6;
				goto IL_0240;
			}

IL_0236:
			{
				IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
				Type_t * L_96 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t2847414787_0_0_0_var), /*hidden argument*/NULL);
				G_B31_0 = L_96;
				G_B31_1 = G_B30_0;
				G_B31_2 = G_B30_1;
				G_B31_3 = G_B30_2;
				G_B31_4 = G_B30_3;
				G_B31_5 = G_B30_4;
				G_B31_6 = G_B30_5;
				G_B31_7 = G_B30_6;
			}

IL_0240:
			{
				NullCheck(G_B31_6);
				DataColumn_t3354469747 * L_97 = XmlDataInferenceLoader_GetMappedColumn_m4111948480(G_B31_6, G_B31_5, G_B31_4, G_B31_3, G_B31_2, G_B31_1, G_B31_0, /*hidden argument*/NULL);
				NullCheck(G_B31_7);
				G_B31_7->set_ReferenceKey_6(L_97);
			}

IL_024a:
			{
				Il2CppObject * L_98 = V_10;
				NullCheck(L_98);
				bool L_99 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_98);
				if (L_99)
				{
					goto IL_01eb;
				}
			}

IL_0256:
			{
				IL2CPP_LEAVE(0x272, FINALLY_025b);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_025b;
		}

FINALLY_025b:
		{ // begin finally (depth: 2)
			{
				Il2CppObject * L_100 = V_10;
				Il2CppObject * L_101 = ((Il2CppObject *)IsInst(L_100, IDisposable_t1628921374_il2cpp_TypeInfo_var));
				V_12 = L_101;
				if (!L_101)
				{
					goto IL_0271;
				}
			}

IL_026a:
			{
				Il2CppObject * L_102 = V_12;
				NullCheck(L_102);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_102);
			}

IL_0271:
			{
				IL2CPP_END_FINALLY(603)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(603)
		{
			IL2CPP_JUMP_TBL(0x272, IL_0272)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0272:
		{
			Il2CppObject * L_103 = V_6;
			NullCheck(L_103);
			bool L_104 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_103);
			if (L_104)
			{
				goto IL_012d;
			}
		}

IL_027e:
		{
			IL2CPP_LEAVE(0x29A, FINALLY_0283);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0283;
	}

FINALLY_0283:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_105 = V_6;
			Il2CppObject * L_106 = ((Il2CppObject *)IsInst(L_105, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_13 = L_106;
			if (!L_106)
			{
				goto IL_0299;
			}
		}

IL_0292:
		{
			Il2CppObject * L_107 = V_13;
			NullCheck(L_107);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_107);
		}

IL_0299:
		{
			IL2CPP_END_FINALLY(643)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(643)
	{
		IL2CPP_JUMP_TBL(0x29A, IL_029a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_029a:
	{
		TableMappingCollection_t4250141105 * L_108 = __this->get_tables_4();
		NullCheck(L_108);
		Il2CppObject * L_109 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* System.Collections.IEnumerator System.Collections.CollectionBase::GetEnumerator() */, L_108);
		V_14 = L_109;
	}

IL_02a7:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0406;
		}

IL_02ac:
		{
			Il2CppObject * L_110 = V_14;
			NullCheck(L_110);
			Il2CppObject * L_111 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_110);
			V_15 = ((TableMapping_t3011643123 *)CastclassClass(L_111, TableMapping_t3011643123_il2cpp_TypeInfo_var));
			TableMapping_t3011643123 * L_112 = V_15;
			NullCheck(L_112);
			bool L_113 = TableMapping_get_ExistsInDataSet_m1826415891(L_112, /*hidden argument*/NULL);
			if (!L_113)
			{
				goto IL_02cb;
			}
		}

IL_02c6:
		{
			goto IL_0406;
		}

IL_02cb:
		{
			TableMapping_t3011643123 * L_114 = V_15;
			NullCheck(L_114);
			DataColumn_t3354469747 * L_115 = L_114->get_PrimaryKey_5();
			if (!L_115)
			{
				goto IL_02ef;
			}
		}

IL_02d7:
		{
			TableMapping_t3011643123 * L_116 = V_15;
			NullCheck(L_116);
			DataTable_t2176726999 * L_117 = L_116->get_Table_1();
			NullCheck(L_117);
			DataColumnCollection_t3528392753 * L_118 = DataTable_get_Columns_m220042291(L_117, /*hidden argument*/NULL);
			TableMapping_t3011643123 * L_119 = V_15;
			NullCheck(L_119);
			DataColumn_t3354469747 * L_120 = L_119->get_PrimaryKey_5();
			NullCheck(L_118);
			DataColumnCollection_Add_m3379764877(L_118, L_120, /*hidden argument*/NULL);
		}

IL_02ef:
		{
			TableMapping_t3011643123 * L_121 = V_15;
			NullCheck(L_121);
			ArrayList_t2121638921 * L_122 = L_121->get_Elements_2();
			NullCheck(L_122);
			Il2CppObject * L_123 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_122);
			V_16 = L_123;
		}

IL_02fd:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0323;
			}

IL_0302:
			{
				Il2CppObject * L_124 = V_16;
				NullCheck(L_124);
				Il2CppObject * L_125 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_124);
				V_17 = ((DataColumn_t3354469747 *)CastclassClass(L_125, DataColumn_t3354469747_il2cpp_TypeInfo_var));
				TableMapping_t3011643123 * L_126 = V_15;
				NullCheck(L_126);
				DataTable_t2176726999 * L_127 = L_126->get_Table_1();
				NullCheck(L_127);
				DataColumnCollection_t3528392753 * L_128 = DataTable_get_Columns_m220042291(L_127, /*hidden argument*/NULL);
				DataColumn_t3354469747 * L_129 = V_17;
				NullCheck(L_128);
				DataColumnCollection_Add_m3379764877(L_128, L_129, /*hidden argument*/NULL);
			}

IL_0323:
			{
				Il2CppObject * L_130 = V_16;
				NullCheck(L_130);
				bool L_131 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_130);
				if (L_131)
				{
					goto IL_0302;
				}
			}

IL_032f:
			{
				IL2CPP_LEAVE(0x34B, FINALLY_0334);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0334;
		}

FINALLY_0334:
		{ // begin finally (depth: 2)
			{
				Il2CppObject * L_132 = V_16;
				Il2CppObject * L_133 = ((Il2CppObject *)IsInst(L_132, IDisposable_t1628921374_il2cpp_TypeInfo_var));
				V_18 = L_133;
				if (!L_133)
				{
					goto IL_034a;
				}
			}

IL_0343:
			{
				Il2CppObject * L_134 = V_18;
				NullCheck(L_134);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_134);
			}

IL_034a:
			{
				IL2CPP_END_FINALLY(820)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(820)
		{
			IL2CPP_JUMP_TBL(0x34B, IL_034b)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_034b:
		{
			TableMapping_t3011643123 * L_135 = V_15;
			NullCheck(L_135);
			ArrayList_t2121638921 * L_136 = L_135->get_Attributes_3();
			NullCheck(L_136);
			Il2CppObject * L_137 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_136);
			V_19 = L_137;
		}

IL_0359:
		try
		{ // begin try (depth: 2)
			{
				goto IL_037f;
			}

IL_035e:
			{
				Il2CppObject * L_138 = V_19;
				NullCheck(L_138);
				Il2CppObject * L_139 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_138);
				V_20 = ((DataColumn_t3354469747 *)CastclassClass(L_139, DataColumn_t3354469747_il2cpp_TypeInfo_var));
				TableMapping_t3011643123 * L_140 = V_15;
				NullCheck(L_140);
				DataTable_t2176726999 * L_141 = L_140->get_Table_1();
				NullCheck(L_141);
				DataColumnCollection_t3528392753 * L_142 = DataTable_get_Columns_m220042291(L_141, /*hidden argument*/NULL);
				DataColumn_t3354469747 * L_143 = V_20;
				NullCheck(L_142);
				DataColumnCollection_Add_m3379764877(L_142, L_143, /*hidden argument*/NULL);
			}

IL_037f:
			{
				Il2CppObject * L_144 = V_19;
				NullCheck(L_144);
				bool L_145 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_144);
				if (L_145)
				{
					goto IL_035e;
				}
			}

IL_038b:
			{
				IL2CPP_LEAVE(0x3A7, FINALLY_0390);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0390;
		}

FINALLY_0390:
		{ // begin finally (depth: 2)
			{
				Il2CppObject * L_146 = V_19;
				Il2CppObject * L_147 = ((Il2CppObject *)IsInst(L_146, IDisposable_t1628921374_il2cpp_TypeInfo_var));
				V_21 = L_147;
				if (!L_147)
				{
					goto IL_03a6;
				}
			}

IL_039f:
			{
				Il2CppObject * L_148 = V_21;
				NullCheck(L_148);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_148);
			}

IL_03a6:
			{
				IL2CPP_END_FINALLY(912)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(912)
		{
			IL2CPP_JUMP_TBL(0x3A7, IL_03a7)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_03a7:
		{
			TableMapping_t3011643123 * L_149 = V_15;
			NullCheck(L_149);
			DataColumn_t3354469747 * L_150 = L_149->get_SimpleContent_4();
			if (!L_150)
			{
				goto IL_03cb;
			}
		}

IL_03b3:
		{
			TableMapping_t3011643123 * L_151 = V_15;
			NullCheck(L_151);
			DataTable_t2176726999 * L_152 = L_151->get_Table_1();
			NullCheck(L_152);
			DataColumnCollection_t3528392753 * L_153 = DataTable_get_Columns_m220042291(L_152, /*hidden argument*/NULL);
			TableMapping_t3011643123 * L_154 = V_15;
			NullCheck(L_154);
			DataColumn_t3354469747 * L_155 = L_154->get_SimpleContent_4();
			NullCheck(L_153);
			DataColumnCollection_Add_m3379764877(L_153, L_155, /*hidden argument*/NULL);
		}

IL_03cb:
		{
			TableMapping_t3011643123 * L_156 = V_15;
			NullCheck(L_156);
			DataColumn_t3354469747 * L_157 = L_156->get_ReferenceKey_6();
			if (!L_157)
			{
				goto IL_03ef;
			}
		}

IL_03d7:
		{
			TableMapping_t3011643123 * L_158 = V_15;
			NullCheck(L_158);
			DataTable_t2176726999 * L_159 = L_158->get_Table_1();
			NullCheck(L_159);
			DataColumnCollection_t3528392753 * L_160 = DataTable_get_Columns_m220042291(L_159, /*hidden argument*/NULL);
			TableMapping_t3011643123 * L_161 = V_15;
			NullCheck(L_161);
			DataColumn_t3354469747 * L_162 = L_161->get_ReferenceKey_6();
			NullCheck(L_160);
			DataColumnCollection_Add_m3379764877(L_160, L_162, /*hidden argument*/NULL);
		}

IL_03ef:
		{
			DataSet_t3654702571 * L_163 = __this->get_dataset_0();
			NullCheck(L_163);
			DataTableCollection_t2915263893 * L_164 = DataSet_get_Tables_m87321279(L_163, /*hidden argument*/NULL);
			TableMapping_t3011643123 * L_165 = V_15;
			NullCheck(L_165);
			DataTable_t2176726999 * L_166 = L_165->get_Table_1();
			NullCheck(L_164);
			DataTableCollection_Add_m3310506721(L_164, L_166, /*hidden argument*/NULL);
		}

IL_0406:
		{
			Il2CppObject * L_167 = V_14;
			NullCheck(L_167);
			bool L_168 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_167);
			if (L_168)
			{
				goto IL_02ac;
			}
		}

IL_0412:
		{
			IL2CPP_LEAVE(0x42E, FINALLY_0417);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0417;
	}

FINALLY_0417:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_169 = V_14;
			Il2CppObject * L_170 = ((Il2CppObject *)IsInst(L_169, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_22 = L_170;
			if (!L_170)
			{
				goto IL_042d;
			}
		}

IL_0426:
		{
			Il2CppObject * L_171 = V_22;
			NullCheck(L_171);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_171);
		}

IL_042d:
		{
			IL2CPP_END_FINALLY(1047)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1047)
	{
		IL2CPP_JUMP_TBL(0x42E, IL_042e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_042e:
	{
		RelationStructureCollection_t1505059432 * L_172 = __this->get_relations_5();
		NullCheck(L_172);
		Il2CppObject * L_173 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* System.Collections.IEnumerator System.Collections.CollectionBase::GetEnumerator() */, L_172);
		V_23 = L_173;
	}

IL_043b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_05e9;
		}

IL_0440:
		{
			Il2CppObject * L_174 = V_23;
			NullCheck(L_174);
			Il2CppObject * L_175 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_174);
			V_24 = ((RelationStructure_t3039531114 *)CastclassClass(L_175, RelationStructure_t3039531114_il2cpp_TypeInfo_var));
			RelationStructure_t3039531114 * L_176 = V_24;
			NullCheck(L_176);
			String_t* L_177 = L_176->get_ExplicitName_0();
			if (!L_177)
			{
				goto IL_0466;
			}
		}

IL_045a:
		{
			RelationStructure_t3039531114 * L_178 = V_24;
			NullCheck(L_178);
			String_t* L_179 = L_178->get_ExplicitName_0();
			G_B79_0 = L_179;
			goto IL_047e;
		}

IL_0466:
		{
			RelationStructure_t3039531114 * L_180 = V_24;
			NullCheck(L_180);
			String_t* L_181 = L_180->get_ParentTableName_1();
			RelationStructure_t3039531114 * L_182 = V_24;
			NullCheck(L_182);
			String_t* L_183 = L_182->get_ChildTableName_2();
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_184 = String_Concat_m1825781833(NULL /*static, unused*/, L_181, _stringLiteral95, L_183, /*hidden argument*/NULL);
			G_B79_0 = L_184;
		}

IL_047e:
		{
			V_25 = G_B79_0;
			DataSet_t3654702571 * L_185 = __this->get_dataset_0();
			NullCheck(L_185);
			DataTableCollection_t2915263893 * L_186 = DataSet_get_Tables_m87321279(L_185, /*hidden argument*/NULL);
			RelationStructure_t3039531114 * L_187 = V_24;
			NullCheck(L_187);
			String_t* L_188 = L_187->get_ParentTableName_1();
			NullCheck(L_186);
			DataTable_t2176726999 * L_189 = DataTableCollection_get_Item_m2714089417(L_186, L_188, /*hidden argument*/NULL);
			V_26 = L_189;
			DataSet_t3654702571 * L_190 = __this->get_dataset_0();
			NullCheck(L_190);
			DataTableCollection_t2915263893 * L_191 = DataSet_get_Tables_m87321279(L_190, /*hidden argument*/NULL);
			RelationStructure_t3039531114 * L_192 = V_24;
			NullCheck(L_192);
			String_t* L_193 = L_192->get_ChildTableName_2();
			NullCheck(L_191);
			DataTable_t2176726999 * L_194 = DataTableCollection_get_Item_m2714089417(L_191, L_193, /*hidden argument*/NULL);
			V_27 = L_194;
			DataTable_t2176726999 * L_195 = V_26;
			NullCheck(L_195);
			DataColumnCollection_t3528392753 * L_196 = DataTable_get_Columns_m220042291(L_195, /*hidden argument*/NULL);
			RelationStructure_t3039531114 * L_197 = V_24;
			NullCheck(L_197);
			String_t* L_198 = L_197->get_ParentColumnName_3();
			NullCheck(L_196);
			DataColumn_t3354469747 * L_199 = DataColumnCollection_get_Item_m305848743(L_196, L_198, /*hidden argument*/NULL);
			V_28 = L_199;
			V_29 = (DataColumn_t3354469747 *)NULL;
			RelationStructure_t3039531114 * L_200 = V_24;
			NullCheck(L_200);
			String_t* L_201 = L_200->get_ParentTableName_1();
			RelationStructure_t3039531114 * L_202 = V_24;
			NullCheck(L_202);
			String_t* L_203 = L_202->get_ChildTableName_2();
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_204 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_201, L_203, /*hidden argument*/NULL);
			if (!L_204)
			{
				goto IL_0508;
			}
		}

IL_04e2:
		{
			DataTable_t2176726999 * L_205 = V_27;
			NullCheck(L_205);
			DataColumnCollection_t3528392753 * L_206 = DataTable_get_Columns_m220042291(L_205, /*hidden argument*/NULL);
			RelationStructure_t3039531114 * L_207 = V_24;
			NullCheck(L_207);
			String_t* L_208 = L_207->get_ChildColumnName_4();
			int32_t L_209 = V_5;
			int32_t L_210 = L_209;
			Il2CppObject * L_211 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_210);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_212 = String_Concat_m2809334143(NULL /*static, unused*/, L_208, _stringLiteral95, L_211, /*hidden argument*/NULL);
			NullCheck(L_206);
			DataColumn_t3354469747 * L_213 = DataColumnCollection_get_Item_m305848743(L_206, L_212, /*hidden argument*/NULL);
			V_29 = L_213;
		}

IL_0508:
		{
			DataColumn_t3354469747 * L_214 = V_29;
			if (L_214)
			{
				goto IL_0524;
			}
		}

IL_050f:
		{
			DataTable_t2176726999 * L_215 = V_27;
			NullCheck(L_215);
			DataColumnCollection_t3528392753 * L_216 = DataTable_get_Columns_m220042291(L_215, /*hidden argument*/NULL);
			RelationStructure_t3039531114 * L_217 = V_24;
			NullCheck(L_217);
			String_t* L_218 = L_217->get_ChildColumnName_4();
			NullCheck(L_216);
			DataColumn_t3354469747 * L_219 = DataColumnCollection_get_Item_m305848743(L_216, L_218, /*hidden argument*/NULL);
			V_29 = L_219;
		}

IL_0524:
		{
			DataTable_t2176726999 * L_220 = V_26;
			if (L_220)
			{
				goto IL_0542;
			}
		}

IL_052b:
		{
			RelationStructure_t3039531114 * L_221 = V_24;
			NullCheck(L_221);
			String_t* L_222 = L_221->get_ParentTableName_1();
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_223 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3767428400, L_222, /*hidden argument*/NULL);
			DataException_t1022144856 * L_224 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
			DataException__ctor_m829863666(L_224, L_223, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_224);
		}

IL_0542:
		{
			DataTable_t2176726999 * L_225 = V_27;
			if (L_225)
			{
				goto IL_0560;
			}
		}

IL_0549:
		{
			RelationStructure_t3039531114 * L_226 = V_24;
			NullCheck(L_226);
			String_t* L_227 = L_226->get_ChildTableName_2();
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_228 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3235462334, L_227, /*hidden argument*/NULL);
			DataException_t1022144856 * L_229 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
			DataException__ctor_m829863666(L_229, L_228, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_229);
		}

IL_0560:
		{
			DataColumn_t3354469747 * L_230 = V_28;
			if (L_230)
			{
				goto IL_057e;
			}
		}

IL_0567:
		{
			RelationStructure_t3039531114 * L_231 = V_24;
			NullCheck(L_231);
			String_t* L_232 = L_231->get_ParentColumnName_3();
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_233 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3411508068, L_232, /*hidden argument*/NULL);
			DataException_t1022144856 * L_234 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
			DataException__ctor_m829863666(L_234, L_233, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_234);
		}

IL_057e:
		{
			DataColumn_t3354469747 * L_235 = V_29;
			if (L_235)
			{
				goto IL_059c;
			}
		}

IL_0585:
		{
			RelationStructure_t3039531114 * L_236 = V_24;
			NullCheck(L_236);
			String_t* L_237 = L_236->get_ChildColumnName_4();
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_238 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2879542002, L_237, /*hidden argument*/NULL);
			DataException_t1022144856 * L_239 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
			DataException__ctor_m829863666(L_239, L_238, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_239);
		}

IL_059c:
		{
			String_t* L_240 = V_25;
			DataColumn_t3354469747 * L_241 = V_28;
			DataColumn_t3354469747 * L_242 = V_29;
			RelationStructure_t3039531114 * L_243 = V_24;
			NullCheck(L_243);
			bool L_244 = L_243->get_CreateConstraint_6();
			DataRelation_t1483987353 * L_245 = (DataRelation_t1483987353 *)il2cpp_codegen_object_new(DataRelation_t1483987353_il2cpp_TypeInfo_var);
			DataRelation__ctor_m2003897510(L_245, L_240, L_241, L_242, L_244, /*hidden argument*/NULL);
			V_30 = L_245;
			RelationStructure_t3039531114 * L_246 = V_24;
			NullCheck(L_246);
			bool L_247 = L_246->get_IsNested_5();
			if (!L_247)
			{
				goto IL_05d7;
			}
		}

IL_05bc:
		{
			DataRelation_t1483987353 * L_248 = V_30;
			NullCheck(L_248);
			VirtActionInvoker1< bool >::Invoke(9 /* System.Void System.Data.DataRelation::set_Nested(System.Boolean) */, L_248, (bool)1);
			DataRelation_t1483987353 * L_249 = V_30;
			NullCheck(L_249);
			DataTable_t2176726999 * L_250 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(12 /* System.Data.DataTable System.Data.DataRelation::get_ParentTable() */, L_249);
			DataRelation_t1483987353 * L_251 = V_30;
			NullCheck(L_251);
			DataColumnU5BU5D_t3410743138* L_252 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(10 /* System.Data.DataColumn[] System.Data.DataRelation::get_ParentColumns() */, L_251);
			NullCheck(L_250);
			DataTable_set_PrimaryKey_m2105023398(L_250, L_252, /*hidden argument*/NULL);
		}

IL_05d7:
		{
			DataSet_t3654702571 * L_253 = __this->get_dataset_0();
			NullCheck(L_253);
			DataRelationCollection_t267599063 * L_254 = DataSet_get_Relations_m498597843(L_253, /*hidden argument*/NULL);
			DataRelation_t1483987353 * L_255 = V_30;
			NullCheck(L_254);
			DataRelationCollection_Add_m2199449921(L_254, L_255, /*hidden argument*/NULL);
		}

IL_05e9:
		{
			Il2CppObject * L_256 = V_23;
			NullCheck(L_256);
			bool L_257 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_256);
			if (L_257)
			{
				goto IL_0440;
			}
		}

IL_05f5:
		{
			IL2CPP_LEAVE(0x611, FINALLY_05fa);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_05fa;
	}

FINALLY_05fa:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_258 = V_23;
			Il2CppObject * L_259 = ((Il2CppObject *)IsInst(L_258, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_31 = L_259;
			if (!L_259)
			{
				goto IL_0610;
			}
		}

IL_0609:
		{
			Il2CppObject * L_260 = V_31;
			NullCheck(L_260);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_260);
		}

IL_0610:
		{
			IL2CPP_END_FINALLY(1530)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1530)
	{
		IL2CPP_JUMP_TBL(0x611, IL_0611)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0611:
	{
		return;
	}
}
// System.Void System.Data.XmlDataInferenceLoader::InferTopLevelTable(System.Xml.XmlElement)
extern "C"  void XmlDataInferenceLoader_InferTopLevelTable_m596841832 (XmlDataInferenceLoader_t1258518718 * __this, XmlElement_t3562928333 * ___el0, const MethodInfo* method)
{
	{
		XmlElement_t3562928333 * L_0 = ___el0;
		XmlDataInferenceLoader_InferTableElement_m1785259436(__this, (TableMapping_t3011643123 *)NULL, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlDataInferenceLoader::InferColumnElement(System.Data.TableMapping,System.Xml.XmlElement)
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* MappingType_t1033973435_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DataException_t1022144856_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DataColumn_t3354469747_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3248661947;
extern const uint32_t XmlDataInferenceLoader_InferColumnElement_m2557478562_MetadataUsageId;
extern "C"  void XmlDataInferenceLoader_InferColumnElement_m2557478562 (XmlDataInferenceLoader_t1258518718 * __this, TableMapping_t3011643123 * ___table0, XmlElement_t3562928333 * ___el1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataInferenceLoader_InferColumnElement_m2557478562_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	DataColumn_t3354469747 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		XmlElement_t3562928333 * L_0 = ___el1;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_2 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		TableMapping_t3011643123 * L_3 = ___table0;
		String_t* L_4 = V_0;
		NullCheck(L_3);
		DataColumn_t3354469747 * L_5 = TableMapping_GetColumn_m1258267765(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		DataColumn_t3354469747 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0055;
		}
	}
	{
		DataColumn_t3354469747 * L_7 = V_1;
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_7);
		if ((((int32_t)L_8) == ((int32_t)1)))
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_9 = V_0;
		DataColumn_t3354469747 * L_10 = V_1;
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_10);
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(MappingType_t1033973435_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral3248661947, L_9, L_13, /*hidden argument*/NULL);
		DataException_t1022144856 * L_15 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_15, L_14, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0042:
	{
		TableMapping_t3011643123 * L_16 = ___table0;
		TableMapping_t3011643123 * L_17 = ___table0;
		NullCheck(L_17);
		ArrayList_t2121638921 * L_18 = L_17->get_Elements_2();
		DataColumn_t3354469747 * L_19 = V_1;
		NullCheck(L_18);
		int32_t L_20 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(33 /* System.Int32 System.Collections.ArrayList::IndexOf(System.Object) */, L_18, L_19);
		NullCheck(L_16);
		L_16->set_lastElementIndex_7(L_20);
		return;
	}

IL_0055:
	{
		TableMapping_t3011643123 * L_21 = ___table0;
		NullCheck(L_21);
		TableMappingCollection_t4250141105 * L_22 = L_21->get_ChildTables_9();
		String_t* L_23 = V_0;
		NullCheck(L_22);
		TableMapping_t3011643123 * L_24 = TableMappingCollection_get_Item_m2768450663(L_22, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0067;
		}
	}
	{
		return;
	}

IL_0067:
	{
		String_t* L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_27 = (DataColumn_t3354469747 *)il2cpp_codegen_object_new(DataColumn_t3354469747_il2cpp_TypeInfo_var);
		DataColumn__ctor_m611202532(L_27, L_25, L_26, /*hidden argument*/NULL);
		V_1 = L_27;
		DataColumn_t3354469747 * L_28 = V_1;
		XmlElement_t3562928333 * L_29 = ___el1;
		NullCheck(L_29);
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_29);
		NullCheck(L_28);
		DataColumn_set_Namespace_m2872284245(L_28, L_30, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_31 = V_1;
		XmlElement_t3562928333 * L_32 = ___el1;
		NullCheck(L_32);
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.Xml.XmlNode::get_Prefix() */, L_32);
		NullCheck(L_31);
		DataColumn_set_Prefix_m2169684416(L_31, L_33, /*hidden argument*/NULL);
		TableMapping_t3011643123 * L_34 = ___table0;
		NullCheck(L_34);
		ArrayList_t2121638921 * L_35 = L_34->get_Elements_2();
		TableMapping_t3011643123 * L_36 = ___table0;
		TableMapping_t3011643123 * L_37 = L_36;
		NullCheck(L_37);
		int32_t L_38 = L_37->get_lastElementIndex_7();
		int32_t L_39 = ((int32_t)((int32_t)L_38+(int32_t)1));
		V_2 = L_39;
		NullCheck(L_37);
		L_37->set_lastElementIndex_7(L_39);
		int32_t L_40 = V_2;
		DataColumn_t3354469747 * L_41 = V_1;
		NullCheck(L_35);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(36 /* System.Void System.Collections.ArrayList::Insert(System.Int32,System.Object) */, L_35, L_40, L_41);
		return;
	}
}
// System.Void System.Data.XmlDataInferenceLoader::CheckExtraneousElementColumn(System.Data.TableMapping,System.Xml.XmlElement)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern const uint32_t XmlDataInferenceLoader_CheckExtraneousElementColumn_m397505038_MetadataUsageId;
extern "C"  void XmlDataInferenceLoader_CheckExtraneousElementColumn_m397505038 (XmlDataInferenceLoader_t1258518718 * __this, TableMapping_t3011643123 * ___parentTable0, XmlElement_t3562928333 * ___el1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataInferenceLoader_CheckExtraneousElementColumn_m397505038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	DataColumn_t3354469747 * V_1 = NULL;
	{
		TableMapping_t3011643123 * L_0 = ___parentTable0;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		XmlElement_t3562928333 * L_1 = ___el1;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_3 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		TableMapping_t3011643123 * L_4 = ___parentTable0;
		String_t* L_5 = V_0;
		NullCheck(L_4);
		DataColumn_t3354469747 * L_6 = TableMapping_GetColumn_m1258267765(L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		DataColumn_t3354469747 * L_7 = V_1;
		if (!L_7)
		{
			goto IL_0028;
		}
	}
	{
		TableMapping_t3011643123 * L_8 = ___parentTable0;
		String_t* L_9 = V_0;
		NullCheck(L_8);
		TableMapping_RemoveElementColumn_m2741199941(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void System.Data.XmlDataInferenceLoader::PopulatePrimaryKey(System.Data.TableMapping)
extern const Il2CppType* Int32_t2847414787_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DataColumn_t3354469747_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93658;
extern const uint32_t XmlDataInferenceLoader_PopulatePrimaryKey_m1228797088_MetadataUsageId;
extern "C"  void XmlDataInferenceLoader_PopulatePrimaryKey_m1228797088 (XmlDataInferenceLoader_t1258518718 * __this, TableMapping_t3011643123 * ___table0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataInferenceLoader_PopulatePrimaryKey_m1228797088_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataColumn_t3354469747 * V_0 = NULL;
	{
		TableMapping_t3011643123 * L_0 = ___table0;
		NullCheck(L_0);
		DataTable_t2176726999 * L_1 = L_0->get_Table_1();
		NullCheck(L_1);
		String_t* L_2 = DataTable_get_TableName_m3141812994(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m138640077(NULL /*static, unused*/, L_2, _stringLiteral93658, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_4 = (DataColumn_t3354469747 *)il2cpp_codegen_object_new(DataColumn_t3354469747_il2cpp_TypeInfo_var);
		DataColumn__ctor_m273049649(L_4, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		DataColumn_t3354469747 * L_5 = V_0;
		NullCheck(L_5);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Data.DataColumn::set_ColumnMapping(System.Data.MappingType) */, L_5, 4);
		DataColumn_t3354469747 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t2847414787_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_6);
		DataColumn_set_DataType_m1172045637(L_6, L_7, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_8 = V_0;
		NullCheck(L_8);
		DataColumn_set_AllowDBNull_m4066005655(L_8, (bool)0, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_9 = V_0;
		NullCheck(L_9);
		DataColumn_set_AutoIncrement_m2609925417(L_9, (bool)1, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_10 = V_0;
		TableMapping_t3011643123 * L_11 = ___table0;
		NullCheck(L_11);
		DataTable_t2176726999 * L_12 = L_11->get_Table_1();
		NullCheck(L_12);
		String_t* L_13 = DataTable_get_Namespace_m3829929060(L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		DataColumn_set_Namespace_m2872284245(L_10, L_13, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_14 = V_0;
		TableMapping_t3011643123 * L_15 = ___table0;
		NullCheck(L_15);
		DataTable_t2176726999 * L_16 = L_15->get_Table_1();
		NullCheck(L_16);
		String_t* L_17 = DataTable_get_Prefix_m494218059(L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		DataColumn_set_Prefix_m2169684416(L_14, L_17, /*hidden argument*/NULL);
		TableMapping_t3011643123 * L_18 = ___table0;
		DataColumn_t3354469747 * L_19 = V_0;
		NullCheck(L_18);
		L_18->set_PrimaryKey_5(L_19);
		return;
	}
}
// System.Void System.Data.XmlDataInferenceLoader::PopulateRelationStructure(System.String,System.String,System.String)
extern Il2CppClass* RelationStructure_t3039531114_il2cpp_TypeInfo_var;
extern const uint32_t XmlDataInferenceLoader_PopulateRelationStructure_m648573609_MetadataUsageId;
extern "C"  void XmlDataInferenceLoader_PopulateRelationStructure_m648573609 (XmlDataInferenceLoader_t1258518718 * __this, String_t* ___parent0, String_t* ___child1, String_t* ___pkeyColumn2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataInferenceLoader_PopulateRelationStructure_m648573609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RelationStructure_t3039531114 * V_0 = NULL;
	{
		RelationStructureCollection_t1505059432 * L_0 = __this->get_relations_5();
		String_t* L_1 = ___parent0;
		String_t* L_2 = ___child1;
		NullCheck(L_0);
		RelationStructure_t3039531114 * L_3 = RelationStructureCollection_get_Item_m3280176159(L_0, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		RelationStructure_t3039531114 * L_4 = (RelationStructure_t3039531114 *)il2cpp_codegen_object_new(RelationStructure_t3039531114_il2cpp_TypeInfo_var);
		RelationStructure__ctor_m1596628542(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		RelationStructure_t3039531114 * L_5 = V_0;
		String_t* L_6 = ___parent0;
		NullCheck(L_5);
		L_5->set_ParentTableName_1(L_6);
		RelationStructure_t3039531114 * L_7 = V_0;
		String_t* L_8 = ___child1;
		NullCheck(L_7);
		L_7->set_ChildTableName_2(L_8);
		RelationStructure_t3039531114 * L_9 = V_0;
		String_t* L_10 = ___pkeyColumn2;
		NullCheck(L_9);
		L_9->set_ParentColumnName_3(L_10);
		RelationStructure_t3039531114 * L_11 = V_0;
		String_t* L_12 = ___pkeyColumn2;
		NullCheck(L_11);
		L_11->set_ChildColumnName_4(L_12);
		RelationStructure_t3039531114 * L_13 = V_0;
		NullCheck(L_13);
		L_13->set_CreateConstraint_6((bool)1);
		RelationStructure_t3039531114 * L_14 = V_0;
		NullCheck(L_14);
		L_14->set_IsNested_5((bool)1);
		RelationStructureCollection_t1505059432 * L_15 = __this->get_relations_5();
		RelationStructure_t3039531114 * L_16 = V_0;
		NullCheck(L_15);
		RelationStructureCollection_Add_m3781472769(L_15, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlDataInferenceLoader::InferRepeatedElement(System.Data.TableMapping,System.Xml.XmlElement)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral437501077;
extern const uint32_t XmlDataInferenceLoader_InferRepeatedElement_m3500509758_MetadataUsageId;
extern "C"  void XmlDataInferenceLoader_InferRepeatedElement_m3500509758 (XmlDataInferenceLoader_t1258518718 * __this, TableMapping_t3011643123 * ___parentTable0, XmlElement_t3562928333 * ___el1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataInferenceLoader_InferRepeatedElement_m3500509758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	TableMapping_t3011643123 * V_1 = NULL;
	{
		XmlElement_t3562928333 * L_0 = ___el1;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_2 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		TableMapping_t3011643123 * L_3 = ___parentTable0;
		XmlElement_t3562928333 * L_4 = ___el1;
		XmlDataInferenceLoader_CheckExtraneousElementColumn_m397505038(__this, L_3, L_4, /*hidden argument*/NULL);
		TableMapping_t3011643123 * L_5 = ___parentTable0;
		String_t* L_6 = V_0;
		XmlElement_t3562928333 * L_7 = ___el1;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_7);
		TableMapping_t3011643123 * L_9 = XmlDataInferenceLoader_GetMappedTable_m1908544612(__this, L_5, L_6, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		TableMapping_t3011643123 * L_10 = V_1;
		NullCheck(L_10);
		ArrayList_t2121638921 * L_11 = L_10->get_Elements_2();
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_11);
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		return;
	}

IL_0035:
	{
		TableMapping_t3011643123 * L_13 = V_1;
		NullCheck(L_13);
		DataColumn_t3354469747 * L_14 = L_13->get_SimpleContent_4();
		if (!L_14)
		{
			goto IL_0041;
		}
	}
	{
		return;
	}

IL_0041:
	{
		TableMapping_t3011643123 * L_15 = V_1;
		String_t* L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m138640077(NULL /*static, unused*/, L_16, _stringLiteral437501077, /*hidden argument*/NULL);
		XmlElement_t3562928333 * L_18 = ___el1;
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.Xml.XmlNode::get_Prefix() */, L_18);
		XmlElement_t3562928333 * L_20 = ___el1;
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_20);
		XmlDataInferenceLoader_GetMappedColumn_m4111948480(__this, L_15, L_17, L_19, L_21, 3, (Type_t *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlDataInferenceLoader::InferTableElement(System.Data.TableMapping,System.Xml.XmlElement)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlAttribute_t2022155821_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNode_t3592213601_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlElement_t3562928333_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral557947472;
extern Il2CppCodeGenString* _stringLiteral1952986079;
extern Il2CppCodeGenString* _stringLiteral90337836;
extern const uint32_t XmlDataInferenceLoader_InferTableElement_m1785259436_MetadataUsageId;
extern "C"  void XmlDataInferenceLoader_InferTableElement_m1785259436 (XmlDataInferenceLoader_t1258518718 * __this, TableMapping_t3011643123 * ___parentTable0, XmlElement_t3562928333 * ___el1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataInferenceLoader_InferTableElement_m1785259436_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	TableMapping_t3011643123 * V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	Il2CppObject * V_6 = NULL;
	XmlAttribute_t2022155821 * V_7 = NULL;
	DataColumn_t3354469747 * V_8 = NULL;
	Il2CppObject * V_9 = NULL;
	Il2CppObject * V_10 = NULL;
	XmlNode_t3592213601 * V_11 = NULL;
	int32_t V_12 = 0;
	XmlElement_t3562928333 * V_13 = NULL;
	String_t* V_14 = NULL;
	int32_t V_15 = 0;
	Il2CppObject * V_16 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TableMapping_t3011643123 * L_0 = ___parentTable0;
		XmlElement_t3562928333 * L_1 = ___el1;
		XmlDataInferenceLoader_CheckExtraneousElementColumn_m397505038(__this, L_0, L_1, /*hidden argument*/NULL);
		XmlElement_t3562928333 * L_2 = ___el1;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_4 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		TableMapping_t3011643123 * L_5 = ___parentTable0;
		String_t* L_6 = V_0;
		XmlElement_t3562928333 * L_7 = ___el1;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_7);
		TableMapping_t3011643123 * L_9 = XmlDataInferenceLoader_GetMappedTable_m1908544612(__this, L_5, L_6, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		V_2 = (bool)0;
		V_3 = (bool)0;
		V_4 = (bool)0;
		V_5 = (bool)0;
		XmlElement_t3562928333 * L_10 = ___el1;
		NullCheck(L_10);
		XmlAttributeCollection_t571717291 * L_11 = VirtFuncInvoker0< XmlAttributeCollection_t571717291 * >::Invoke(6 /* System.Xml.XmlAttributeCollection System.Xml.XmlNode::get_Attributes() */, L_10);
		NullCheck(L_11);
		Il2CppObject * L_12 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Collections.IEnumerator System.Xml.XmlNamedNodeMap::GetEnumerator() */, L_11);
		V_6 = L_12;
	}

IL_003a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00cc;
		}

IL_003f:
		{
			Il2CppObject * L_13 = V_6;
			NullCheck(L_13);
			Il2CppObject * L_14 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_13);
			V_7 = ((XmlAttribute_t2022155821 *)CastclassClass(L_14, XmlAttribute_t2022155821_il2cpp_TypeInfo_var));
			XmlAttribute_t2022155821 * L_15 = V_7;
			NullCheck(L_15);
			String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_15);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_17 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_16, _stringLiteral557947472, /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0079;
			}
		}

IL_0063:
		{
			XmlAttribute_t2022155821 * L_18 = V_7;
			NullCheck(L_18);
			String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_18);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_20 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_19, _stringLiteral1952986079, /*hidden argument*/NULL);
			if (!L_20)
			{
				goto IL_007e;
			}
		}

IL_0079:
		{
			goto IL_00cc;
		}

IL_007e:
		{
			ArrayList_t2121638921 * L_21 = __this->get_ignoredNamespaces_3();
			if (!L_21)
			{
				goto IL_00a5;
			}
		}

IL_0089:
		{
			ArrayList_t2121638921 * L_22 = __this->get_ignoredNamespaces_3();
			XmlAttribute_t2022155821 * L_23 = V_7;
			NullCheck(L_23);
			String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_23);
			NullCheck(L_22);
			bool L_25 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(32 /* System.Boolean System.Collections.ArrayList::Contains(System.Object) */, L_22, L_24);
			if (!L_25)
			{
				goto IL_00a5;
			}
		}

IL_00a0:
		{
			goto IL_00cc;
		}

IL_00a5:
		{
			V_3 = (bool)1;
			TableMapping_t3011643123 * L_26 = V_1;
			XmlAttribute_t2022155821 * L_27 = V_7;
			NullCheck(L_27);
			String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_27);
			IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
			String_t* L_29 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
			XmlAttribute_t2022155821 * L_30 = V_7;
			NullCheck(L_30);
			String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.Xml.XmlNode::get_Prefix() */, L_30);
			XmlAttribute_t2022155821 * L_32 = V_7;
			NullCheck(L_32);
			String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_32);
			DataColumn_t3354469747 * L_34 = XmlDataInferenceLoader_GetMappedColumn_m4111948480(__this, L_26, L_29, L_31, L_33, 2, (Type_t *)NULL, /*hidden argument*/NULL);
			V_8 = L_34;
		}

IL_00cc:
		{
			Il2CppObject * L_35 = V_6;
			NullCheck(L_35);
			bool L_36 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_35);
			if (L_36)
			{
				goto IL_003f;
			}
		}

IL_00d8:
		{
			IL2CPP_LEAVE(0xF4, FINALLY_00dd);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00dd;
	}

FINALLY_00dd:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_37 = V_6;
			Il2CppObject * L_38 = ((Il2CppObject *)IsInst(L_37, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_9 = L_38;
			if (!L_38)
			{
				goto IL_00f3;
			}
		}

IL_00ec:
		{
			Il2CppObject * L_39 = V_9;
			NullCheck(L_39);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_39);
		}

IL_00f3:
		{
			IL2CPP_END_FINALLY(221)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(221)
	{
		IL2CPP_JUMP_TBL(0xF4, IL_00f4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00f4:
	{
		XmlElement_t3562928333 * L_40 = ___el1;
		NullCheck(L_40);
		XmlNodeList_t3966370975 * L_41 = VirtFuncInvoker0< XmlNodeList_t3966370975 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_40);
		NullCheck(L_41);
		Il2CppObject * L_42 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_41);
		V_10 = L_42;
	}

IL_0101:
	try
	{ // begin try (depth: 1)
		{
			goto IL_022d;
		}

IL_0106:
		{
			Il2CppObject * L_43 = V_10;
			NullCheck(L_43);
			Il2CppObject * L_44 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_43);
			V_11 = ((XmlNode_t3592213601 *)CastclassClass(L_44, XmlNode_t3592213601_il2cpp_TypeInfo_var));
			XmlNode_t3592213601 * L_45 = V_11;
			NullCheck(L_45);
			int32_t L_46 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Xml.XmlNodeType System.Xml.XmlNode::get_NodeType() */, L_45);
			V_12 = L_46;
			int32_t L_47 = V_12;
			if ((((int32_t)L_47) == ((int32_t)7)))
			{
				goto IL_013a;
			}
		}

IL_0125:
		{
			int32_t L_48 = V_12;
			if ((((int32_t)L_48) == ((int32_t)8)))
			{
				goto IL_013a;
			}
		}

IL_012d:
		{
			int32_t L_49 = V_12;
			if ((((int32_t)L_49) == ((int32_t)1)))
			{
				goto IL_015d;
			}
		}

IL_0135:
		{
			goto IL_013f;
		}

IL_013a:
		{
			goto IL_022d;
		}

IL_013f:
		{
			V_4 = (bool)1;
			XmlElement_t3562928333 * L_50 = ___el1;
			ArrayList_t2121638921 * L_51 = __this->get_ignoredNamespaces_3();
			int32_t L_52 = XmlDataInferenceLoader_GetElementMappingType_m959501994(NULL /*static, unused*/, L_50, L_51, (Hashtable_t3875263730 *)NULL, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_52) == ((uint32_t)1))))
			{
				goto IL_0158;
			}
		}

IL_0155:
		{
			V_5 = (bool)1;
		}

IL_0158:
		{
			goto IL_022d;
		}

IL_015d:
		{
			V_2 = (bool)1;
			XmlNode_t3592213601 * L_53 = V_11;
			V_13 = ((XmlElement_t3562928333 *)IsInstClass(L_53, XmlElement_t3562928333_il2cpp_TypeInfo_var));
			XmlElement_t3562928333 * L_54 = V_13;
			NullCheck(L_54);
			String_t* L_55 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_54);
			IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
			String_t* L_56 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
			V_14 = L_56;
			XmlElement_t3562928333 * L_57 = V_13;
			ArrayList_t2121638921 * L_58 = __this->get_ignoredNamespaces_3();
			int32_t L_59 = XmlDataInferenceLoader_GetElementMappingType_m959501994(NULL /*static, unused*/, L_57, L_58, (Hashtable_t3875263730 *)NULL, /*hidden argument*/NULL);
			V_15 = L_59;
			int32_t L_60 = V_15;
			if (L_60 == 0)
			{
				goto IL_019e;
			}
			if (L_60 == 1)
			{
				goto IL_01ac;
			}
			if (L_60 == 2)
			{
				goto IL_01ea;
			}
		}

IL_0199:
		{
			goto IL_0228;
		}

IL_019e:
		{
			TableMapping_t3011643123 * L_61 = V_1;
			XmlElement_t3562928333 * L_62 = V_13;
			XmlDataInferenceLoader_InferColumnElement_m2557478562(__this, L_61, L_62, /*hidden argument*/NULL);
			goto IL_0228;
		}

IL_01ac:
		{
			TableMapping_t3011643123 * L_63 = V_1;
			NullCheck(L_63);
			DataColumn_t3354469747 * L_64 = L_63->get_PrimaryKey_5();
			if (L_64)
			{
				goto IL_01be;
			}
		}

IL_01b7:
		{
			TableMapping_t3011643123 * L_65 = V_1;
			XmlDataInferenceLoader_PopulatePrimaryKey_m1228797088(__this, L_65, /*hidden argument*/NULL);
		}

IL_01be:
		{
			TableMapping_t3011643123 * L_66 = V_1;
			NullCheck(L_66);
			DataTable_t2176726999 * L_67 = L_66->get_Table_1();
			NullCheck(L_67);
			String_t* L_68 = DataTable_get_TableName_m3141812994(L_67, /*hidden argument*/NULL);
			String_t* L_69 = V_14;
			TableMapping_t3011643123 * L_70 = V_1;
			NullCheck(L_70);
			DataColumn_t3354469747 * L_71 = L_70->get_PrimaryKey_5();
			NullCheck(L_71);
			String_t* L_72 = DataColumn_get_ColumnName_m409531680(L_71, /*hidden argument*/NULL);
			XmlDataInferenceLoader_PopulateRelationStructure_m648573609(__this, L_68, L_69, L_72, /*hidden argument*/NULL);
			TableMapping_t3011643123 * L_73 = V_1;
			XmlElement_t3562928333 * L_74 = V_13;
			XmlDataInferenceLoader_InferRepeatedElement_m3500509758(__this, L_73, L_74, /*hidden argument*/NULL);
			goto IL_0228;
		}

IL_01ea:
		{
			TableMapping_t3011643123 * L_75 = V_1;
			NullCheck(L_75);
			DataColumn_t3354469747 * L_76 = L_75->get_PrimaryKey_5();
			if (L_76)
			{
				goto IL_01fc;
			}
		}

IL_01f5:
		{
			TableMapping_t3011643123 * L_77 = V_1;
			XmlDataInferenceLoader_PopulatePrimaryKey_m1228797088(__this, L_77, /*hidden argument*/NULL);
		}

IL_01fc:
		{
			TableMapping_t3011643123 * L_78 = V_1;
			NullCheck(L_78);
			DataTable_t2176726999 * L_79 = L_78->get_Table_1();
			NullCheck(L_79);
			String_t* L_80 = DataTable_get_TableName_m3141812994(L_79, /*hidden argument*/NULL);
			String_t* L_81 = V_14;
			TableMapping_t3011643123 * L_82 = V_1;
			NullCheck(L_82);
			DataColumn_t3354469747 * L_83 = L_82->get_PrimaryKey_5();
			NullCheck(L_83);
			String_t* L_84 = DataColumn_get_ColumnName_m409531680(L_83, /*hidden argument*/NULL);
			XmlDataInferenceLoader_PopulateRelationStructure_m648573609(__this, L_80, L_81, L_84, /*hidden argument*/NULL);
			TableMapping_t3011643123 * L_85 = V_1;
			XmlElement_t3562928333 * L_86 = V_13;
			XmlDataInferenceLoader_InferTableElement_m1785259436(__this, L_85, L_86, /*hidden argument*/NULL);
			goto IL_0228;
		}

IL_0228:
		{
			goto IL_022d;
		}

IL_022d:
		{
			Il2CppObject * L_87 = V_10;
			NullCheck(L_87);
			bool L_88 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_87);
			if (L_88)
			{
				goto IL_0106;
			}
		}

IL_0239:
		{
			IL2CPP_LEAVE(0x255, FINALLY_023e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_023e;
	}

FINALLY_023e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_89 = V_10;
			Il2CppObject * L_90 = ((Il2CppObject *)IsInst(L_89, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_16 = L_90;
			if (!L_90)
			{
				goto IL_0254;
			}
		}

IL_024d:
		{
			Il2CppObject * L_91 = V_16;
			NullCheck(L_91);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_91);
		}

IL_0254:
		{
			IL2CPP_END_FINALLY(574)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(574)
	{
		IL2CPP_JUMP_TBL(0x255, IL_0255)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0255:
	{
		TableMapping_t3011643123 * L_92 = V_1;
		NullCheck(L_92);
		DataColumn_t3354469747 * L_93 = L_92->get_SimpleContent_4();
		if (L_93)
		{
			goto IL_02a3;
		}
	}
	{
		bool L_94 = V_2;
		if (L_94)
		{
			goto IL_02a3;
		}
	}
	{
		bool L_95 = V_4;
		if (!L_95)
		{
			goto IL_02a3;
		}
	}
	{
		bool L_96 = V_3;
		if (L_96)
		{
			goto IL_027a;
		}
	}
	{
		bool L_97 = V_5;
		if (!L_97)
		{
			goto IL_02a3;
		}
	}

IL_027a:
	{
		TableMapping_t3011643123 * L_98 = V_1;
		TableMapping_t3011643123 * L_99 = V_1;
		NullCheck(L_99);
		DataTable_t2176726999 * L_100 = L_99->get_Table_1();
		NullCheck(L_100);
		String_t* L_101 = DataTable_get_TableName_m3141812994(L_100, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_102 = String_Concat_m138640077(NULL /*static, unused*/, L_101, _stringLiteral90337836, /*hidden argument*/NULL);
		String_t* L_103 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_104 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		XmlDataInferenceLoader_GetMappedColumn_m4111948480(__this, L_98, L_102, L_103, L_104, 3, (Type_t *)NULL, /*hidden argument*/NULL);
	}

IL_02a3:
	{
		return;
	}
}
// System.Data.TableMapping System.Data.XmlDataInferenceLoader::GetMappedTable(System.Data.TableMapping,System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DataException_t1022144856_il2cpp_TypeInfo_var;
extern Il2CppClass* TableMapping_t3011643123_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2557934167;
extern const uint32_t XmlDataInferenceLoader_GetMappedTable_m1908544612_MetadataUsageId;
extern "C"  TableMapping_t3011643123 * XmlDataInferenceLoader_GetMappedTable_m1908544612 (XmlDataInferenceLoader_t1258518718 * __this, TableMapping_t3011643123 * ___parent0, String_t* ___tableName1, String_t* ___ns2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataInferenceLoader_GetMappedTable_m1908544612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TableMapping_t3011643123 * V_0 = NULL;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	TableMapping_t3011643123 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TableMappingCollection_t4250141105 * L_0 = __this->get_tables_4();
		String_t* L_1 = ___tableName1;
		NullCheck(L_0);
		TableMapping_t3011643123 * L_2 = TableMappingCollection_get_Item_m2768450663(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		TableMapping_t3011643123 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0061;
		}
	}
	{
		TableMapping_t3011643123 * L_4 = ___parent0;
		if (!L_4)
		{
			goto IL_005c;
		}
	}
	{
		TableMapping_t3011643123 * L_5 = V_0;
		NullCheck(L_5);
		TableMapping_t3011643123 * L_6 = L_5->get_ParentTable_8();
		if (!L_6)
		{
			goto IL_005c;
		}
	}
	{
		TableMapping_t3011643123 * L_7 = V_0;
		NullCheck(L_7);
		TableMapping_t3011643123 * L_8 = L_7->get_ParentTable_8();
		TableMapping_t3011643123 * L_9 = ___parent0;
		if ((((Il2CppObject*)(TableMapping_t3011643123 *)L_8) == ((Il2CppObject*)(TableMapping_t3011643123 *)L_9)))
		{
			goto IL_005c;
		}
	}
	{
		String_t* L_10 = ___tableName1;
		TableMapping_t3011643123 * L_11 = V_0;
		NullCheck(L_11);
		TableMapping_t3011643123 * L_12 = L_11->get_ParentTable_8();
		NullCheck(L_12);
		DataTable_t2176726999 * L_13 = L_12->get_Table_1();
		NullCheck(L_13);
		String_t* L_14 = DataTable_get_TableName_m3141812994(L_13, /*hidden argument*/NULL);
		TableMapping_t3011643123 * L_15 = ___parent0;
		NullCheck(L_15);
		DataTable_t2176726999 * L_16 = L_15->get_Table_1();
		NullCheck(L_16);
		String_t* L_17 = DataTable_get_TableName_m3141812994(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral2557934167, L_10, L_14, L_17, /*hidden argument*/NULL);
		DataException_t1022144856 * L_19 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_19, L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_005c:
	{
		goto IL_007c;
	}

IL_0061:
	{
		String_t* L_20 = ___tableName1;
		String_t* L_21 = ___ns2;
		TableMapping_t3011643123 * L_22 = (TableMapping_t3011643123 *)il2cpp_codegen_object_new(TableMapping_t3011643123_il2cpp_TypeInfo_var);
		TableMapping__ctor_m516615085(L_22, L_20, L_21, /*hidden argument*/NULL);
		V_0 = L_22;
		TableMapping_t3011643123 * L_23 = V_0;
		TableMapping_t3011643123 * L_24 = ___parent0;
		NullCheck(L_23);
		L_23->set_ParentTable_8(L_24);
		TableMappingCollection_t4250141105 * L_25 = __this->get_tables_4();
		TableMapping_t3011643123 * L_26 = V_0;
		NullCheck(L_25);
		TableMappingCollection_Add_m3664613133(L_25, L_26, /*hidden argument*/NULL);
	}

IL_007c:
	{
		TableMapping_t3011643123 * L_27 = ___parent0;
		if (!L_27)
		{
			goto IL_00f6;
		}
	}
	{
		V_1 = (bool)1;
		TableMapping_t3011643123 * L_28 = ___parent0;
		NullCheck(L_28);
		TableMappingCollection_t4250141105 * L_29 = L_28->get_ChildTables_9();
		NullCheck(L_29);
		Il2CppObject * L_30 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* System.Collections.IEnumerator System.Collections.CollectionBase::GetEnumerator() */, L_29);
		V_2 = L_30;
	}

IL_0090:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00be;
		}

IL_0095:
		{
			Il2CppObject * L_31 = V_2;
			NullCheck(L_31);
			Il2CppObject * L_32 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_31);
			V_3 = ((TableMapping_t3011643123 *)CastclassClass(L_32, TableMapping_t3011643123_il2cpp_TypeInfo_var));
			TableMapping_t3011643123 * L_33 = V_3;
			NullCheck(L_33);
			DataTable_t2176726999 * L_34 = L_33->get_Table_1();
			NullCheck(L_34);
			String_t* L_35 = DataTable_get_TableName_m3141812994(L_34, /*hidden argument*/NULL);
			String_t* L_36 = ___tableName1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_37 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
			if (!L_37)
			{
				goto IL_00be;
			}
		}

IL_00b7:
		{
			V_1 = (bool)0;
			goto IL_00c9;
		}

IL_00be:
		{
			Il2CppObject * L_38 = V_2;
			NullCheck(L_38);
			bool L_39 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_38);
			if (L_39)
			{
				goto IL_0095;
			}
		}

IL_00c9:
		{
			IL2CPP_LEAVE(0xE4, FINALLY_00ce);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00ce;
	}

FINALLY_00ce:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_40 = V_2;
			Il2CppObject * L_41 = ((Il2CppObject *)IsInst(L_40, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_4 = L_41;
			if (!L_41)
			{
				goto IL_00e3;
			}
		}

IL_00dc:
		{
			Il2CppObject * L_42 = V_4;
			NullCheck(L_42);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_42);
		}

IL_00e3:
		{
			IL2CPP_END_FINALLY(206)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(206)
	{
		IL2CPP_JUMP_TBL(0xE4, IL_00e4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00e4:
	{
		bool L_43 = V_1;
		if (!L_43)
		{
			goto IL_00f6;
		}
	}
	{
		TableMapping_t3011643123 * L_44 = ___parent0;
		NullCheck(L_44);
		TableMappingCollection_t4250141105 * L_45 = L_44->get_ChildTables_9();
		TableMapping_t3011643123 * L_46 = V_0;
		NullCheck(L_45);
		TableMappingCollection_Add_m3664613133(L_45, L_46, /*hidden argument*/NULL);
	}

IL_00f6:
	{
		TableMapping_t3011643123 * L_47 = V_0;
		return L_47;
	}
}
// System.Data.DataColumn System.Data.XmlDataInferenceLoader::GetMappedColumn(System.Data.TableMapping,System.String,System.String,System.String,System.Data.MappingType,System.Type)
extern Il2CppClass* DataColumn_t3354469747_il2cpp_TypeInfo_var;
extern Il2CppClass* MappingType_t1033973435_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DataException_t1022144856_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3470137465;
extern const uint32_t XmlDataInferenceLoader_GetMappedColumn_m4111948480_MetadataUsageId;
extern "C"  DataColumn_t3354469747 * XmlDataInferenceLoader_GetMappedColumn_m4111948480 (XmlDataInferenceLoader_t1258518718 * __this, TableMapping_t3011643123 * ___table0, String_t* ___name1, String_t* ___prefix2, String_t* ___ns3, int32_t ___type4, Type_t * ___optColType5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataInferenceLoader_GetMappedColumn_m4111948480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataColumn_t3354469747 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		TableMapping_t3011643123 * L_0 = ___table0;
		String_t* L_1 = ___name1;
		NullCheck(L_0);
		DataColumn_t3354469747 * L_2 = TableMapping_GetColumn_m1258267765(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DataColumn_t3354469747 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0095;
		}
	}
	{
		String_t* L_4 = ___name1;
		DataColumn_t3354469747 * L_5 = (DataColumn_t3354469747 *)il2cpp_codegen_object_new(DataColumn_t3354469747_il2cpp_TypeInfo_var);
		DataColumn__ctor_m273049649(L_5, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		DataColumn_t3354469747 * L_6 = V_0;
		String_t* L_7 = ___prefix2;
		NullCheck(L_6);
		DataColumn_set_Prefix_m2169684416(L_6, L_7, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_8 = V_0;
		String_t* L_9 = ___ns3;
		NullCheck(L_8);
		DataColumn_set_Namespace_m2872284245(L_8, L_9, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_10 = V_0;
		int32_t L_11 = ___type4;
		NullCheck(L_10);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Data.DataColumn::set_ColumnMapping(System.Data.MappingType) */, L_10, L_11);
		int32_t L_12 = ___type4;
		V_1 = L_12;
		int32_t L_13 = V_1;
		if (((int32_t)((int32_t)L_13-(int32_t)1)) == 0)
		{
			goto IL_004c;
		}
		if (((int32_t)((int32_t)L_13-(int32_t)1)) == 1)
		{
			goto IL_005e;
		}
		if (((int32_t)((int32_t)L_13-(int32_t)1)) == 2)
		{
			goto IL_0070;
		}
		if (((int32_t)((int32_t)L_13-(int32_t)1)) == 3)
		{
			goto IL_007c;
		}
	}
	{
		goto IL_0090;
	}

IL_004c:
	{
		TableMapping_t3011643123 * L_14 = ___table0;
		NullCheck(L_14);
		ArrayList_t2121638921 * L_15 = L_14->get_Elements_2();
		DataColumn_t3354469747 * L_16 = V_0;
		NullCheck(L_15);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_15, L_16);
		goto IL_0090;
	}

IL_005e:
	{
		TableMapping_t3011643123 * L_17 = ___table0;
		NullCheck(L_17);
		ArrayList_t2121638921 * L_18 = L_17->get_Attributes_3();
		DataColumn_t3354469747 * L_19 = V_0;
		NullCheck(L_18);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_18, L_19);
		goto IL_0090;
	}

IL_0070:
	{
		TableMapping_t3011643123 * L_20 = ___table0;
		DataColumn_t3354469747 * L_21 = V_0;
		NullCheck(L_20);
		L_20->set_SimpleContent_4(L_21);
		goto IL_0090;
	}

IL_007c:
	{
		DataColumn_t3354469747 * L_22 = V_0;
		Type_t * L_23 = ___optColType5;
		NullCheck(L_22);
		DataColumn_set_DataType_m1172045637(L_22, L_23, /*hidden argument*/NULL);
		TableMapping_t3011643123 * L_24 = ___table0;
		DataColumn_t3354469747 * L_25 = V_0;
		NullCheck(L_24);
		L_24->set_ReferenceKey_6(L_25);
		goto IL_0090;
	}

IL_0090:
	{
		goto IL_00c3;
	}

IL_0095:
	{
		DataColumn_t3354469747 * L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_26);
		int32_t L_28 = ___type4;
		if ((((int32_t)L_27) == ((int32_t)L_28)))
		{
			goto IL_00c3;
		}
	}
	{
		DataColumn_t3354469747 * L_29 = V_0;
		NullCheck(L_29);
		String_t* L_30 = DataColumn_get_ColumnName_m409531680(L_29, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_31 = V_0;
		NullCheck(L_31);
		int32_t L_32 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_31);
		int32_t L_33 = L_32;
		Il2CppObject * L_34 = Box(MappingType_t1033973435_il2cpp_TypeInfo_var, &L_33);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral3470137465, L_30, L_34, /*hidden argument*/NULL);
		DataException_t1022144856 * L_36 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_36, L_35, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_36);
	}

IL_00c3:
	{
		DataColumn_t3354469747 * L_37 = V_0;
		return L_37;
	}
}
// System.Void System.Data.XmlDataInferenceLoader::SetAsExistingTable(System.Xml.XmlElement,System.Collections.Hashtable)
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern const uint32_t XmlDataInferenceLoader_SetAsExistingTable_m3536871892_MetadataUsageId;
extern "C"  void XmlDataInferenceLoader_SetAsExistingTable_m3536871892 (Il2CppObject * __this /* static, unused */, XmlElement_t3562928333 * ___el0, Hashtable_t3875263730 * ___existingTables1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataInferenceLoader_SetAsExistingTable_m3536871892_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t2121638921 * V_0 = NULL;
	{
		Hashtable_t3875263730 * L_0 = ___existingTables1;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		Hashtable_t3875263730 * L_1 = ___existingTables1;
		XmlElement_t3562928333 * L_2 = ___el0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_2);
		NullCheck(L_1);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_1, L_3);
		V_0 = ((ArrayList_t2121638921 *)IsInstClass(L_4, ArrayList_t2121638921_il2cpp_TypeInfo_var));
		ArrayList_t2121638921 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0032;
		}
	}
	{
		ArrayList_t2121638921 * L_6 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		Hashtable_t3875263730 * L_7 = ___existingTables1;
		XmlElement_t3562928333 * L_8 = ___el0;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_8);
		ArrayList_t2121638921 * L_10 = V_0;
		NullCheck(L_7);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(27 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_7, L_9, L_10);
	}

IL_0032:
	{
		ArrayList_t2121638921 * L_11 = V_0;
		XmlElement_t3562928333 * L_12 = ___el0;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_12);
		NullCheck(L_11);
		bool L_14 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(32 /* System.Boolean System.Collections.ArrayList::Contains(System.Object) */, L_11, L_13);
		if (!L_14)
		{
			goto IL_0044;
		}
	}
	{
		return;
	}

IL_0044:
	{
		ArrayList_t2121638921 * L_15 = V_0;
		XmlElement_t3562928333 * L_16 = ___el0;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_16);
		NullCheck(L_15);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_15, L_17);
		return;
	}
}
// System.Data.ElementMappingType System.Data.XmlDataInferenceLoader::GetElementMappingType(System.Xml.XmlElement,System.Collections.ArrayList,System.Collections.Hashtable)
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlAttribute_t2022155821_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNode_t3592213601_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlElement_t3562928333_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral557947472;
extern Il2CppCodeGenString* _stringLiteral1952986079;
extern const uint32_t XmlDataInferenceLoader_GetElementMappingType_m959501994_MetadataUsageId;
extern "C"  int32_t XmlDataInferenceLoader_GetElementMappingType_m959501994 (Il2CppObject * __this /* static, unused */, XmlElement_t3562928333 * ___el0, ArrayList_t2121638921 * ___ignoredNamespaces1, Hashtable_t3875263730 * ___existingTables2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataInferenceLoader_GetElementMappingType_m959501994_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t2121638921 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	XmlAttribute_t2022155821 * V_2 = NULL;
	int32_t V_3 = 0;
	Il2CppObject * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	XmlNode_t3592213601 * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	XmlNode_t3592213601 * V_8 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B33_0 = 0;
	{
		Hashtable_t3875263730 * L_0 = ___existingTables2;
		if (!L_0)
		{
			goto IL_0031;
		}
	}
	{
		Hashtable_t3875263730 * L_1 = ___existingTables2;
		XmlElement_t3562928333 * L_2 = ___el0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_2);
		NullCheck(L_1);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_1, L_3);
		V_0 = ((ArrayList_t2121638921 *)IsInstClass(L_4, ArrayList_t2121638921_il2cpp_TypeInfo_var));
		ArrayList_t2121638921 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		ArrayList_t2121638921 * L_6 = V_0;
		XmlElement_t3562928333 * L_7 = ___el0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_7);
		NullCheck(L_6);
		bool L_9 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(32 /* System.Boolean System.Collections.ArrayList::Contains(System.Object) */, L_6, L_8);
		if (!L_9)
		{
			goto IL_0031;
		}
	}
	{
		return (int32_t)(2);
	}

IL_0031:
	{
		XmlElement_t3562928333 * L_10 = ___el0;
		NullCheck(L_10);
		XmlAttributeCollection_t571717291 * L_11 = VirtFuncInvoker0< XmlAttributeCollection_t571717291 * >::Invoke(6 /* System.Xml.XmlAttributeCollection System.Xml.XmlNode::get_Attributes() */, L_10);
		NullCheck(L_11);
		Il2CppObject * L_12 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Collections.IEnumerator System.Xml.XmlNamedNodeMap::GetEnumerator() */, L_11);
		V_1 = L_12;
	}

IL_003d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a7;
		}

IL_0042:
		{
			Il2CppObject * L_13 = V_1;
			NullCheck(L_13);
			Il2CppObject * L_14 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_13);
			V_2 = ((XmlAttribute_t2022155821 *)CastclassClass(L_14, XmlAttribute_t2022155821_il2cpp_TypeInfo_var));
			XmlAttribute_t2022155821 * L_15 = V_2;
			NullCheck(L_15);
			String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_15);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_17 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_16, _stringLiteral557947472, /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0078;
			}
		}

IL_0063:
		{
			XmlAttribute_t2022155821 * L_18 = V_2;
			NullCheck(L_18);
			String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_18);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_20 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_19, _stringLiteral1952986079, /*hidden argument*/NULL);
			if (!L_20)
			{
				goto IL_007d;
			}
		}

IL_0078:
		{
			goto IL_00a7;
		}

IL_007d:
		{
			ArrayList_t2121638921 * L_21 = ___ignoredNamespaces1;
			if (!L_21)
			{
				goto IL_0099;
			}
		}

IL_0083:
		{
			ArrayList_t2121638921 * L_22 = ___ignoredNamespaces1;
			XmlAttribute_t2022155821 * L_23 = V_2;
			NullCheck(L_23);
			String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_23);
			NullCheck(L_22);
			bool L_25 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(32 /* System.Boolean System.Collections.ArrayList::Contains(System.Object) */, L_22, L_24);
			if (!L_25)
			{
				goto IL_0099;
			}
		}

IL_0094:
		{
			goto IL_00a7;
		}

IL_0099:
		{
			XmlElement_t3562928333 * L_26 = ___el0;
			Hashtable_t3875263730 * L_27 = ___existingTables2;
			XmlDataInferenceLoader_SetAsExistingTable_m3536871892(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
			V_3 = 2;
			IL2CPP_LEAVE(0x196, FINALLY_00b7);
		}

IL_00a7:
		{
			Il2CppObject * L_28 = V_1;
			NullCheck(L_28);
			bool L_29 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_28);
			if (L_29)
			{
				goto IL_0042;
			}
		}

IL_00b2:
		{
			IL2CPP_LEAVE(0xCD, FINALLY_00b7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00b7;
	}

FINALLY_00b7:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_30 = V_1;
			Il2CppObject * L_31 = ((Il2CppObject *)IsInst(L_30, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_4 = L_31;
			if (!L_31)
			{
				goto IL_00cc;
			}
		}

IL_00c5:
		{
			Il2CppObject * L_32 = V_4;
			NullCheck(L_32);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_32);
		}

IL_00cc:
		{
			IL2CPP_END_FINALLY(183)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(183)
	{
		IL2CPP_JUMP_TBL(0x196, IL_0196)
		IL2CPP_JUMP_TBL(0xCD, IL_00cd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00cd:
	{
		XmlElement_t3562928333 * L_33 = ___el0;
		NullCheck(L_33);
		XmlNodeList_t3966370975 * L_34 = VirtFuncInvoker0< XmlNodeList_t3966370975 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_33);
		NullCheck(L_34);
		Il2CppObject * L_35 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_34);
		V_5 = L_35;
	}

IL_00da:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0108;
		}

IL_00df:
		{
			Il2CppObject * L_36 = V_5;
			NullCheck(L_36);
			Il2CppObject * L_37 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_36);
			V_6 = ((XmlNode_t3592213601 *)CastclassClass(L_37, XmlNode_t3592213601_il2cpp_TypeInfo_var));
			XmlNode_t3592213601 * L_38 = V_6;
			NullCheck(L_38);
			int32_t L_39 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Xml.XmlNodeType System.Xml.XmlNode::get_NodeType() */, L_38);
			if ((!(((uint32_t)L_39) == ((uint32_t)1))))
			{
				goto IL_0108;
			}
		}

IL_00fa:
		{
			XmlElement_t3562928333 * L_40 = ___el0;
			Hashtable_t3875263730 * L_41 = ___existingTables2;
			XmlDataInferenceLoader_SetAsExistingTable_m3536871892(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
			V_3 = 2;
			IL2CPP_LEAVE(0x196, FINALLY_0119);
		}

IL_0108:
		{
			Il2CppObject * L_42 = V_5;
			NullCheck(L_42);
			bool L_43 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_42);
			if (L_43)
			{
				goto IL_00df;
			}
		}

IL_0114:
		{
			IL2CPP_LEAVE(0x130, FINALLY_0119);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0119;
	}

FINALLY_0119:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_44 = V_5;
			Il2CppObject * L_45 = ((Il2CppObject *)IsInst(L_44, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_7 = L_45;
			if (!L_45)
			{
				goto IL_012f;
			}
		}

IL_0128:
		{
			Il2CppObject * L_46 = V_7;
			NullCheck(L_46);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_46);
		}

IL_012f:
		{
			IL2CPP_END_FINALLY(281)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(281)
	{
		IL2CPP_JUMP_TBL(0x196, IL_0196)
		IL2CPP_JUMP_TBL(0x130, IL_0130)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0130:
	{
		XmlElement_t3562928333 * L_47 = ___el0;
		NullCheck(L_47);
		XmlNode_t3592213601 * L_48 = VirtFuncInvoker0< XmlNode_t3592213601 * >::Invoke(18 /* System.Xml.XmlNode System.Xml.XmlNode::get_NextSibling() */, L_47);
		V_8 = L_48;
		goto IL_018d;
	}

IL_013d:
	{
		XmlNode_t3592213601 * L_49 = V_8;
		NullCheck(L_49);
		int32_t L_50 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Xml.XmlNodeType System.Xml.XmlNode::get_NodeType() */, L_49);
		if ((!(((uint32_t)L_50) == ((uint32_t)1))))
		{
			goto IL_0184;
		}
	}
	{
		XmlNode_t3592213601 * L_51 = V_8;
		NullCheck(L_51);
		String_t* L_52 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_51);
		XmlElement_t3562928333 * L_53 = ___el0;
		NullCheck(L_53);
		String_t* L_54 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_53);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_55 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_52, L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0184;
		}
	}
	{
		XmlElement_t3562928333 * L_56 = ___el0;
		Hashtable_t3875263730 * L_57 = ___existingTables2;
		XmlDataInferenceLoader_SetAsExistingTable_m3536871892(NULL /*static, unused*/, L_56, L_57, /*hidden argument*/NULL);
		XmlNode_t3592213601 * L_58 = V_8;
		ArrayList_t2121638921 * L_59 = ___ignoredNamespaces1;
		int32_t L_60 = XmlDataInferenceLoader_GetElementMappingType_m959501994(NULL /*static, unused*/, ((XmlElement_t3562928333 *)IsInstClass(L_58, XmlElement_t3562928333_il2cpp_TypeInfo_var)), L_59, (Hashtable_t3875263730 *)NULL, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_60) == ((uint32_t)2))))
		{
			goto IL_0182;
		}
	}
	{
		G_B33_0 = 2;
		goto IL_0183;
	}

IL_0182:
	{
		G_B33_0 = 1;
	}

IL_0183:
	{
		return (int32_t)(G_B33_0);
	}

IL_0184:
	{
		XmlNode_t3592213601 * L_61 = V_8;
		NullCheck(L_61);
		XmlNode_t3592213601 * L_62 = VirtFuncInvoker0< XmlNode_t3592213601 * >::Invoke(18 /* System.Xml.XmlNode System.Xml.XmlNode::get_NextSibling() */, L_61);
		V_8 = L_62;
	}

IL_018d:
	{
		XmlNode_t3592213601 * L_63 = V_8;
		if (L_63)
		{
			goto IL_013d;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0196:
	{
		int32_t L_64 = V_3;
		return L_64;
	}
}
// System.Boolean System.Data.XmlDataInferenceLoader::IsDocumentElementTable()
extern "C"  bool XmlDataInferenceLoader_IsDocumentElementTable_m1972492031 (XmlDataInferenceLoader_t1258518718 * __this, const MethodInfo* method)
{
	{
		XmlDocument_t3705263098 * L_0 = __this->get_document_1();
		NullCheck(L_0);
		XmlElement_t3562928333 * L_1 = XmlDocument_get_DocumentElement_m3688713126(L_0, /*hidden argument*/NULL);
		ArrayList_t2121638921 * L_2 = __this->get_ignoredNamespaces_3();
		bool L_3 = XmlDataInferenceLoader_IsDocumentElementTable_m2892000111(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean System.Data.XmlDataInferenceLoader::IsDocumentElementTable(System.Xml.XmlElement,System.Collections.ArrayList)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlAttribute_t2022155821_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNode_t3592213601_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlElement_t3562928333_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral557947472;
extern Il2CppCodeGenString* _stringLiteral1952986079;
extern const uint32_t XmlDataInferenceLoader_IsDocumentElementTable_m2892000111_MetadataUsageId;
extern "C"  bool XmlDataInferenceLoader_IsDocumentElementTable_m2892000111 (Il2CppObject * __this /* static, unused */, XmlElement_t3562928333 * ___top0, ArrayList_t2121638921 * ___ignoredNamespaces1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataInferenceLoader_IsDocumentElementTable_m2892000111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	XmlAttribute_t2022155821 * V_1 = NULL;
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	Hashtable_t3875263730 * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	XmlNode_t3592213601 * V_6 = NULL;
	XmlElement_t3562928333 * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlElement_t3562928333 * L_0 = ___top0;
		NullCheck(L_0);
		XmlAttributeCollection_t571717291 * L_1 = VirtFuncInvoker0< XmlAttributeCollection_t571717291 * >::Invoke(6 /* System.Xml.XmlAttributeCollection System.Xml.XmlNode::get_Attributes() */, L_0);
		NullCheck(L_1);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Collections.IEnumerator System.Xml.XmlNamedNodeMap::GetEnumerator() */, L_1);
		V_0 = L_2;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006f;
		}

IL_0011:
		{
			Il2CppObject * L_3 = V_0;
			NullCheck(L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_3);
			V_1 = ((XmlAttribute_t2022155821 *)CastclassClass(L_4, XmlAttribute_t2022155821_il2cpp_TypeInfo_var));
			XmlAttribute_t2022155821 * L_5 = V_1;
			NullCheck(L_5);
			String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_5);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_7 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_6, _stringLiteral557947472, /*hidden argument*/NULL);
			if (L_7)
			{
				goto IL_0047;
			}
		}

IL_0032:
		{
			XmlAttribute_t2022155821 * L_8 = V_1;
			NullCheck(L_8);
			String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_8);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_10 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_9, _stringLiteral1952986079, /*hidden argument*/NULL);
			if (!L_10)
			{
				goto IL_004c;
			}
		}

IL_0047:
		{
			goto IL_006f;
		}

IL_004c:
		{
			ArrayList_t2121638921 * L_11 = ___ignoredNamespaces1;
			if (!L_11)
			{
				goto IL_0068;
			}
		}

IL_0052:
		{
			ArrayList_t2121638921 * L_12 = ___ignoredNamespaces1;
			XmlAttribute_t2022155821 * L_13 = V_1;
			NullCheck(L_13);
			String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_13);
			NullCheck(L_12);
			bool L_15 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(32 /* System.Boolean System.Collections.ArrayList::Contains(System.Object) */, L_12, L_14);
			if (!L_15)
			{
				goto IL_0068;
			}
		}

IL_0063:
		{
			goto IL_006f;
		}

IL_0068:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x10F, FINALLY_007f);
		}

IL_006f:
		{
			Il2CppObject * L_16 = V_0;
			NullCheck(L_16);
			bool L_17 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_16);
			if (L_17)
			{
				goto IL_0011;
			}
		}

IL_007a:
		{
			IL2CPP_LEAVE(0x93, FINALLY_007f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_007f;
	}

FINALLY_007f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_18 = V_0;
			Il2CppObject * L_19 = ((Il2CppObject *)IsInst(L_18, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_3 = L_19;
			if (!L_19)
			{
				goto IL_0092;
			}
		}

IL_008c:
		{
			Il2CppObject * L_20 = V_3;
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_20);
		}

IL_0092:
		{
			IL2CPP_END_FINALLY(127)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(127)
	{
		IL2CPP_JUMP_TBL(0x10F, IL_010f)
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		Hashtable_t3875263730 * L_21 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_21, /*hidden argument*/NULL);
		V_4 = L_21;
		XmlElement_t3562928333 * L_22 = ___top0;
		NullCheck(L_22);
		XmlNodeList_t3966370975 * L_23 = VirtFuncInvoker0< XmlNodeList_t3966370975 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_22);
		NullCheck(L_23);
		Il2CppObject * L_24 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_23);
		V_5 = L_24;
	}

IL_00a7:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00e5;
		}

IL_00ac:
		{
			Il2CppObject * L_25 = V_5;
			NullCheck(L_25);
			Il2CppObject * L_26 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_25);
			V_6 = ((XmlNode_t3592213601 *)CastclassClass(L_26, XmlNode_t3592213601_il2cpp_TypeInfo_var));
			XmlNode_t3592213601 * L_27 = V_6;
			V_7 = ((XmlElement_t3562928333 *)IsInstClass(L_27, XmlElement_t3562928333_il2cpp_TypeInfo_var));
			XmlElement_t3562928333 * L_28 = V_7;
			if (L_28)
			{
				goto IL_00cf;
			}
		}

IL_00ca:
		{
			goto IL_00e5;
		}

IL_00cf:
		{
			XmlElement_t3562928333 * L_29 = V_7;
			ArrayList_t2121638921 * L_30 = ___ignoredNamespaces1;
			Hashtable_t3875263730 * L_31 = V_4;
			int32_t L_32 = XmlDataInferenceLoader_GetElementMappingType_m959501994(NULL /*static, unused*/, L_29, L_30, L_31, /*hidden argument*/NULL);
			if (L_32)
			{
				goto IL_00e5;
			}
		}

IL_00de:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x10F, FINALLY_00f6);
		}

IL_00e5:
		{
			Il2CppObject * L_33 = V_5;
			NullCheck(L_33);
			bool L_34 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_33);
			if (L_34)
			{
				goto IL_00ac;
			}
		}

IL_00f1:
		{
			IL2CPP_LEAVE(0x10D, FINALLY_00f6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00f6;
	}

FINALLY_00f6:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_35 = V_5;
			Il2CppObject * L_36 = ((Il2CppObject *)IsInst(L_35, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_8 = L_36;
			if (!L_36)
			{
				goto IL_010c;
			}
		}

IL_0105:
		{
			Il2CppObject * L_37 = V_8;
			NullCheck(L_37);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_37);
		}

IL_010c:
		{
			IL2CPP_END_FINALLY(246)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(246)
	{
		IL2CPP_JUMP_TBL(0x10F, IL_010f)
		IL2CPP_JUMP_TBL(0x10D, IL_010d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_010d:
	{
		return (bool)0;
	}

IL_010f:
	{
		bool L_38 = V_2;
		return L_38;
	}
}
// System.Object System.Data.XmlDataLoader::StringToObject(System.Type,System.String)
extern const Il2CppType* TimeSpan_t763862892_0_0_0_var;
extern const Il2CppType* Guid_t2778838590_0_0_0_var;
extern const Il2CppType* ByteU5BU5D_t58506160_0_0_0_var;
extern const Il2CppType* Type_t_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlConvert_t1882388356_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t2778693821_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t1688557254_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t2847414729_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* SByte_t2855346064_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t985925268_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t985925326_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t985925421_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppClass* Guid_t2778838590_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t XmlDataLoader_StringToObject_m1292975836_MetadataUsageId;
extern "C"  Il2CppObject * XmlDataLoader_StringToObject_m1292975836 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataLoader_StringToObject_m1292975836_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Type_t * L_0 = ___type0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		String_t* L_1 = ___value1;
		return L_1;
	}

IL_0008:
	{
		Type_t * L_2 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_3 = Type_GetTypeCode_m2969996822(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 0)
		{
			goto IL_0054;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 1)
		{
			goto IL_006c;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 2)
		{
			goto IL_00c2;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 3)
		{
			goto IL_0060;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 4)
		{
			goto IL_009e;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 5)
		{
			goto IL_00da;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 6)
		{
			goto IL_00aa;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 7)
		{
			goto IL_00e6;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 8)
		{
			goto IL_00b6;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 9)
		{
			goto IL_00f2;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 10)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 11)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 12)
		{
			goto IL_0086;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 13)
		{
			goto IL_0079;
		}
	}
	{
		goto IL_00fe;
	}

IL_0054:
	{
		String_t* L_5 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		bool L_6 = XmlConvert_ToBoolean_m3758854944(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		bool L_7 = L_6;
		Il2CppObject * L_8 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_7);
		return L_8;
	}

IL_0060:
	{
		String_t* L_9 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		uint8_t L_10 = XmlConvert_ToByte_m1845524822(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		uint8_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Byte_t2778693821_il2cpp_TypeInfo_var, &L_11);
		return L_12;
	}

IL_006c:
	{
		String_t* L_13 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		int32_t L_14 = XmlConvert_ToInt32_m3773620768(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		uint16_t L_15 = ((uint16_t)(((int32_t)((uint16_t)L_14))));
		Il2CppObject * L_16 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_15);
		return L_16;
	}

IL_0079:
	{
		String_t* L_17 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_18 = XmlConvert_ToDateTime_m4285748037(NULL /*static, unused*/, L_17, 2, /*hidden argument*/NULL);
		DateTime_t339033936  L_19 = L_18;
		Il2CppObject * L_20 = Box(DateTime_t339033936_il2cpp_TypeInfo_var, &L_19);
		return L_20;
	}

IL_0086:
	{
		String_t* L_21 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		Decimal_t1688557254  L_22 = XmlConvert_ToDecimal_m3464816128(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		Decimal_t1688557254  L_23 = L_22;
		Il2CppObject * L_24 = Box(Decimal_t1688557254_il2cpp_TypeInfo_var, &L_23);
		return L_24;
	}

IL_0092:
	{
		String_t* L_25 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		double L_26 = XmlConvert_ToDouble_m1758308804(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		double L_27 = L_26;
		Il2CppObject * L_28 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_27);
		return L_28;
	}

IL_009e:
	{
		String_t* L_29 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		int16_t L_30 = XmlConvert_ToInt16_m674493152(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		int16_t L_31 = L_30;
		Il2CppObject * L_32 = Box(Int16_t2847414729_il2cpp_TypeInfo_var, &L_31);
		return L_32;
	}

IL_00aa:
	{
		String_t* L_33 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		int32_t L_34 = XmlConvert_ToInt32_m3773620768(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		int32_t L_35 = L_34;
		Il2CppObject * L_36 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_35);
		return L_36;
	}

IL_00b6:
	{
		String_t* L_37 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		int64_t L_38 = XmlConvert_ToInt64_m778201600(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		int64_t L_39 = L_38;
		Il2CppObject * L_40 = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &L_39);
		return L_40;
	}

IL_00c2:
	{
		String_t* L_41 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		int8_t L_42 = XmlConvert_ToSByte_m29345728(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		int8_t L_43 = L_42;
		Il2CppObject * L_44 = Box(SByte_t2855346064_il2cpp_TypeInfo_var, &L_43);
		return L_44;
	}

IL_00ce:
	{
		String_t* L_45 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		float L_46 = XmlConvert_ToSingle_m34142998(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		float L_47 = L_46;
		Il2CppObject * L_48 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_47);
		return L_48;
	}

IL_00da:
	{
		String_t* L_49 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		uint16_t L_50 = XmlConvert_ToUInt16_m1167065128(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		uint16_t L_51 = L_50;
		Il2CppObject * L_52 = Box(UInt16_t985925268_il2cpp_TypeInfo_var, &L_51);
		return L_52;
	}

IL_00e6:
	{
		String_t* L_53 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		uint32_t L_54 = XmlConvert_ToUInt32_m2021978420(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		uint32_t L_55 = L_54;
		Il2CppObject * L_56 = Box(UInt32_t985925326_il2cpp_TypeInfo_var, &L_55);
		return L_56;
	}

IL_00f2:
	{
		String_t* L_57 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		uint64_t L_58 = XmlConvert_ToUInt64_m2681755830(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		uint64_t L_59 = L_58;
		Il2CppObject * L_60 = Box(UInt64_t985925421_il2cpp_TypeInfo_var, &L_59);
		return L_60;
	}

IL_00fe:
	{
		Type_t * L_61 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_62 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(TimeSpan_t763862892_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_61) == ((Il2CppObject*)(Type_t *)L_62))))
		{
			goto IL_011a;
		}
	}
	{
		String_t* L_63 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_64 = XmlConvert_ToTimeSpan_m83474552(NULL /*static, unused*/, L_63, /*hidden argument*/NULL);
		TimeSpan_t763862892  L_65 = L_64;
		Il2CppObject * L_66 = Box(TimeSpan_t763862892_il2cpp_TypeInfo_var, &L_65);
		return L_66;
	}

IL_011a:
	{
		Type_t * L_67 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_68 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Guid_t2778838590_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_67) == ((Il2CppObject*)(Type_t *)L_68))))
		{
			goto IL_0136;
		}
	}
	{
		String_t* L_69 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		Guid_t2778838590  L_70 = XmlConvert_ToGuid_m1476180436(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
		Guid_t2778838590  L_71 = L_70;
		Il2CppObject * L_72 = Box(Guid_t2778838590_il2cpp_TypeInfo_var, &L_71);
		return L_72;
	}

IL_0136:
	{
		Type_t * L_73 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_74 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ByteU5BU5D_t58506160_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_73) == ((Il2CppObject*)(Type_t *)L_74))))
		{
			goto IL_014d;
		}
	}
	{
		String_t* L_75 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		ByteU5BU5D_t58506160* L_76 = Convert_FromBase64String_m901846280(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		return (Il2CppObject *)L_76;
	}

IL_014d:
	{
		Type_t * L_77 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_78 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Type_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_77) == ((Il2CppObject*)(Type_t *)L_78))))
		{
			goto IL_0164;
		}
	}
	{
		String_t* L_79 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_80 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m2877589631, L_79, "System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
		return L_80;
	}

IL_0164:
	{
		String_t* L_81 = ___value1;
		Type_t * L_82 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		Il2CppObject * L_83 = Convert_ChangeType_m2922880930(NULL /*static, unused*/, L_81, L_82, /*hidden argument*/NULL);
		return L_83;
	}
}
// System.Void System.Data.XmlDataReader::.ctor(System.Data.DataSet,System.Xml.XmlReader,System.Data.XmlReadMode)
extern "C"  void XmlDataReader__ctor_m2922173333 (XmlDataReader_t4112562711 * __this, DataSet_t3654702571 * ___ds0, XmlReader_t4229084514 * ___xr1, int32_t ___m2, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		DataSet_t3654702571 * L_0 = ___ds0;
		__this->set_dataset_0(L_0);
		XmlReader_t4229084514 * L_1 = ___xr1;
		__this->set_reader_1(L_1);
		int32_t L_2 = ___m2;
		__this->set_mode_2(L_2);
		return;
	}
}
// System.Void System.Data.XmlDataReader::ReadXml(System.Data.DataSet,System.Xml.XmlReader,System.Data.XmlReadMode)
extern Il2CppClass* XmlDataReader_t4112562711_il2cpp_TypeInfo_var;
extern const uint32_t XmlDataReader_ReadXml_m4139667348_MetadataUsageId;
extern "C"  void XmlDataReader_ReadXml_m4139667348 (Il2CppObject * __this /* static, unused */, DataSet_t3654702571 * ___dataset0, XmlReader_t4229084514 * ___reader1, int32_t ___mode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataReader_ReadXml_m4139667348_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DataSet_t3654702571 * L_0 = ___dataset0;
		XmlReader_t4229084514 * L_1 = ___reader1;
		int32_t L_2 = ___mode2;
		XmlDataReader_t4112562711 * L_3 = (XmlDataReader_t4112562711 *)il2cpp_codegen_object_new(XmlDataReader_t4112562711_il2cpp_TypeInfo_var);
		XmlDataReader__ctor_m2922173333(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		XmlDataReader_Process_m3840338046(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlDataReader::Process()
extern "C"  void XmlDataReader_Process_m3840338046 (XmlDataReader_t4112562711 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		DataSet_t3654702571 * L_0 = __this->get_dataset_0();
		NullCheck(L_0);
		bool L_1 = DataSet_get_EnforceConstraints_m1069235578(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			DataSet_t3654702571 * L_2 = __this->get_dataset_0();
			NullCheck(L_2);
			DataSet_set_EnforceConstraints_m2529190639(L_2, (bool)0, /*hidden argument*/NULL);
			XmlReader_t4229084514 * L_3 = __this->get_reader_1();
			NullCheck(L_3);
			VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_3);
			int32_t L_4 = __this->get_mode_2();
			if ((!(((uint32_t)L_4) == ((uint32_t)5))))
			{
				goto IL_0061;
			}
		}

IL_0030:
		{
			goto IL_003b;
		}

IL_0035:
		{
			XmlDataReader_ReadTopLevelElement_m896075622(__this, /*hidden argument*/NULL);
		}

IL_003b:
		{
			XmlReader_t4229084514 * L_5 = __this->get_reader_1();
			NullCheck(L_5);
			int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_5);
			if ((!(((uint32_t)L_6) == ((uint32_t)1))))
			{
				goto IL_005c;
			}
		}

IL_004c:
		{
			XmlReader_t4229084514 * L_7 = __this->get_reader_1();
			NullCheck(L_7);
			bool L_8 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean System.Xml.XmlReader::get_EOF() */, L_7);
			if (!L_8)
			{
				goto IL_0035;
			}
		}

IL_005c:
		{
			goto IL_0067;
		}

IL_0061:
		{
			XmlDataReader_ReadTopLevelElement_m896075622(__this, /*hidden argument*/NULL);
		}

IL_0067:
		{
			IL2CPP_LEAVE(0x79, FINALLY_006c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		DataSet_t3654702571 * L_9 = __this->get_dataset_0();
		bool L_10 = V_0;
		NullCheck(L_9);
		DataSet_set_EnforceConstraints_m2529190639(L_9, L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x79, IL_0079)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0079:
	{
		return;
	}
}
// System.Boolean System.Data.XmlDataReader::IsTopLevelDataSet()
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlDocument_t3705263098_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlElement_t3562928333_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNodeReader_t3079406212_il2cpp_TypeInfo_var;
extern const uint32_t XmlDataReader_IsTopLevelDataSet_m1625390712_MetadataUsageId;
extern "C"  bool XmlDataReader_IsTopLevelDataSet_m1625390712 (XmlDataReader_t4112562711 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataReader_IsTopLevelDataSet_m1625390712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	DataTable_t2176726999 * V_1 = NULL;
	XmlDocument_t3705263098 * V_2 = NULL;
	XmlElement_t3562928333 * V_3 = NULL;
	{
		XmlReader_t4229084514 * L_0 = __this->get_reader_1();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_2 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DataSet_t3654702571 * L_3 = __this->get_dataset_0();
		NullCheck(L_3);
		DataTableCollection_t2915263893 * L_4 = DataSet_get_Tables_m87321279(L_3, /*hidden argument*/NULL);
		String_t* L_5 = V_0;
		NullCheck(L_4);
		DataTable_t2176726999 * L_6 = DataTableCollection_get_Item_m2714089417(L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		DataTable_t2176726999 * L_7 = V_1;
		if (L_7)
		{
			goto IL_002b;
		}
	}
	{
		return (bool)1;
	}

IL_002b:
	{
		XmlDocument_t3705263098 * L_8 = (XmlDocument_t3705263098 *)il2cpp_codegen_object_new(XmlDocument_t3705263098_il2cpp_TypeInfo_var);
		XmlDocument__ctor_m1037796712(L_8, /*hidden argument*/NULL);
		V_2 = L_8;
		XmlDocument_t3705263098 * L_9 = V_2;
		XmlReader_t4229084514 * L_10 = __this->get_reader_1();
		NullCheck(L_9);
		XmlNode_t3592213601 * L_11 = VirtFuncInvoker1< XmlNode_t3592213601 *, XmlReader_t4229084514 * >::Invoke(57 /* System.Xml.XmlNode System.Xml.XmlDocument::ReadNode(System.Xml.XmlReader) */, L_9, L_10);
		V_3 = ((XmlElement_t3562928333 *)CastclassClass(L_11, XmlElement_t3562928333_il2cpp_TypeInfo_var));
		XmlDocument_t3705263098 * L_12 = V_2;
		XmlElement_t3562928333 * L_13 = V_3;
		NullCheck(L_12);
		VirtFuncInvoker1< XmlNode_t3592213601 *, XmlNode_t3592213601 * >::Invoke(31 /* System.Xml.XmlNode System.Xml.XmlNode::AppendChild(System.Xml.XmlNode) */, L_12, L_13);
		XmlElement_t3562928333 * L_14 = V_3;
		XmlNodeReader_t3079406212 * L_15 = (XmlNodeReader_t3079406212 *)il2cpp_codegen_object_new(XmlNodeReader_t3079406212_il2cpp_TypeInfo_var);
		XmlNodeReader__ctor_m1147832059(L_15, L_14, /*hidden argument*/NULL);
		__this->set_reader_1(L_15);
		XmlReader_t4229084514 * L_16 = __this->get_reader_1();
		NullCheck(L_16);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_16);
		XmlElement_t3562928333 * L_17 = V_3;
		bool L_18 = XmlDataInferenceLoader_IsDocumentElementTable_m2892000111(NULL /*static, unused*/, L_17, (ArrayList_t2121638921 *)NULL, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_18) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Data.XmlDataReader::ReadTopLevelElement()
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlDataReader_ReadTopLevelElement_m896075622_MetadataUsageId;
extern "C"  void XmlDataReader_ReadTopLevelElement_m896075622 (XmlDataReader_t4112562711 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataReader_ReadTopLevelElement_m896075622_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_mode_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)5))))
		{
			goto IL_0061;
		}
	}
	{
		XmlReader_t4229084514 * L_1 = __this->get_reader_1();
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_3 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		DataSet_t3654702571 * L_4 = __this->get_dataset_0();
		NullCheck(L_4);
		String_t* L_5 = DataSet_get_DataSetName_m1556380856(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0051;
		}
	}
	{
		XmlReader_t4229084514 * L_7 = __this->get_reader_1();
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_7);
		DataSet_t3654702571 * L_9 = __this->get_dataset_0();
		NullCheck(L_9);
		String_t* L_10 = DataSet_get_Namespace_m20720144(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0061;
		}
	}

IL_0051:
	{
		XmlReader_t4229084514 * L_12 = __this->get_reader_1();
		NullCheck(L_12);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_12);
		goto IL_00f7;
	}

IL_0061:
	{
		int32_t L_13 = __this->get_mode_2();
		if ((((int32_t)L_13) == ((int32_t)5)))
		{
			goto IL_0078;
		}
	}
	{
		bool L_14 = XmlDataReader_IsTopLevelDataSet_m1625390712(__this, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00f1;
		}
	}

IL_0078:
	{
		XmlReader_t4229084514 * L_15 = __this->get_reader_1();
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XmlReader::get_Depth() */, L_15);
		V_0 = L_16;
		XmlReader_t4229084514 * L_17 = __this->get_reader_1();
		NullCheck(L_17);
		VirtFuncInvoker0< bool >::Invoke(42 /* System.Boolean System.Xml.XmlReader::Read() */, L_17);
		XmlReader_t4229084514 * L_18 = __this->get_reader_1();
		NullCheck(L_18);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_18);
	}

IL_009c:
	{
		XmlDataReader_ReadDataSetContent_m2392319466(__this, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_19 = __this->get_reader_1();
		NullCheck(L_19);
		int32_t L_20 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XmlReader::get_Depth() */, L_19);
		int32_t L_21 = V_0;
		if ((((int32_t)L_20) <= ((int32_t)L_21)))
		{
			goto IL_00c3;
		}
	}
	{
		XmlReader_t4229084514 * L_22 = __this->get_reader_1();
		NullCheck(L_22);
		bool L_23 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean System.Xml.XmlReader::get_EOF() */, L_22);
		if (!L_23)
		{
			goto IL_009c;
		}
	}

IL_00c3:
	{
		XmlReader_t4229084514 * L_24 = __this->get_reader_1();
		NullCheck(L_24);
		int32_t L_25 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_24);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_00e0;
		}
	}
	{
		XmlReader_t4229084514 * L_26 = __this->get_reader_1();
		NullCheck(L_26);
		VirtActionInvoker0::Invoke(45 /* System.Void System.Xml.XmlReader::ReadEndElement() */, L_26);
	}

IL_00e0:
	{
		XmlReader_t4229084514 * L_27 = __this->get_reader_1();
		NullCheck(L_27);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_27);
		goto IL_00f7;
	}

IL_00f1:
	{
		XmlDataReader_ReadDataSetContent_m2392319466(__this, /*hidden argument*/NULL);
	}

IL_00f7:
	{
		return;
	}
}
// System.Void System.Data.XmlDataReader::ReadDataSetContent()
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlDataReader_ReadDataSetContent_m2392319466_MetadataUsageId;
extern "C"  void XmlDataReader_ReadDataSetContent_m2392319466 (XmlDataReader_t4112562711 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataReader_ReadDataSetContent_m2392319466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataTable_t2176726999 * V_0 = NULL;
	DataRow_t3654701923 * V_1 = NULL;
	{
		DataSet_t3654702571 * L_0 = __this->get_dataset_0();
		NullCheck(L_0);
		DataTableCollection_t2915263893 * L_1 = DataSet_get_Tables_m87321279(L_0, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_2 = __this->get_reader_1();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_4 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		DataTable_t2176726999 * L_5 = DataTableCollection_get_Item_m2714089417(L_1, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		DataTable_t2176726999 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		DataTable_t2176726999 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = DataTable_get_Namespace_m3829929060(L_7, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_9 = __this->get_reader_1();
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005a;
		}
	}

IL_0042:
	{
		XmlReader_t4229084514 * L_12 = __this->get_reader_1();
		NullCheck(L_12);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_12);
		XmlReader_t4229084514 * L_13 = __this->get_reader_1();
		NullCheck(L_13);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_13);
		return;
	}

IL_005a:
	{
		DataTable_t2176726999 * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = DataTable_get_Namespace_m3829929060(L_14, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_16 = __this->get_reader_1();
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_008d;
		}
	}
	{
		XmlReader_t4229084514 * L_19 = __this->get_reader_1();
		NullCheck(L_19);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_19);
		XmlReader_t4229084514 * L_20 = __this->get_reader_1();
		NullCheck(L_20);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_20);
		return;
	}

IL_008d:
	{
		DataTable_t2176726999 * L_21 = V_0;
		NullCheck(L_21);
		DataRow_t3654701923 * L_22 = DataTable_NewRow_m3256434597(L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		DataRow_t3654701923 * L_23 = V_1;
		XmlDataReader_ReadElement_m2762657682(__this, L_23, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_24 = V_0;
		NullCheck(L_24);
		DataRowCollection_t1405583905 * L_25 = DataTable_get_Rows_m954608043(L_24, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_26 = V_1;
		NullCheck(L_25);
		DataRowCollection_Add_m2125891809(L_25, L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlDataReader::ReadElement(System.Data.DataRow)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral557947472;
extern Il2CppCodeGenString* _stringLiteral1952986079;
extern const uint32_t XmlDataReader_ReadElement_m2762657682_MetadataUsageId;
extern "C"  void XmlDataReader_ReadElement_m2762657682 (XmlDataReader_t4112562711 * __this, DataRow_t3654701923 * ___row0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataReader_ReadElement_m2762657682_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		XmlReader_t4229084514 * L_0 = __this->get_reader_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(40 /* System.Boolean System.Xml.XmlReader::MoveToFirstAttribute() */, L_0);
		if (!L_1)
		{
			goto IL_006c;
		}
	}

IL_0010:
	{
		XmlReader_t4229084514 * L_2 = __this->get_reader_1();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_3, _stringLiteral557947472, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0044;
		}
	}
	{
		XmlReader_t4229084514 * L_5 = __this->get_reader_1();
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_6, _stringLiteral1952986079, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0049;
		}
	}

IL_0044:
	{
		goto IL_0050;
	}

IL_0049:
	{
		DataRow_t3654701923 * L_8 = ___row0;
		XmlDataReader_ReadElementAttribute_m507573950(__this, L_8, /*hidden argument*/NULL);
	}

IL_0050:
	{
		XmlReader_t4229084514 * L_9 = __this->get_reader_1();
		NullCheck(L_9);
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(41 /* System.Boolean System.Xml.XmlReader::MoveToNextAttribute() */, L_9);
		if (L_10)
		{
			goto IL_0010;
		}
	}
	{
		XmlReader_t4229084514 * L_11 = __this->get_reader_1();
		NullCheck(L_11);
		VirtFuncInvoker0< bool >::Invoke(39 /* System.Boolean System.Xml.XmlReader::MoveToElement() */, L_11);
	}

IL_006c:
	{
		XmlReader_t4229084514 * L_12 = __this->get_reader_1();
		NullCheck(L_12);
		bool L_13 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XmlReader::get_IsEmptyElement() */, L_12);
		if (!L_13)
		{
			goto IL_0098;
		}
	}
	{
		XmlReader_t4229084514 * L_14 = __this->get_reader_1();
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_14);
		XmlReader_t4229084514 * L_15 = __this->get_reader_1();
		NullCheck(L_15);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_15);
		goto IL_0129;
	}

IL_0098:
	{
		XmlReader_t4229084514 * L_16 = __this->get_reader_1();
		NullCheck(L_16);
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XmlReader::get_Depth() */, L_16);
		V_0 = L_17;
		XmlReader_t4229084514 * L_18 = __this->get_reader_1();
		NullCheck(L_18);
		VirtFuncInvoker0< bool >::Invoke(42 /* System.Boolean System.Xml.XmlReader::Read() */, L_18);
		XmlReader_t4229084514 * L_19 = __this->get_reader_1();
		NullCheck(L_19);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_19);
	}

IL_00bc:
	{
		DataRow_t3654701923 * L_20 = ___row0;
		XmlDataReader_ReadElementContent_m1908626945(__this, L_20, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_21 = __this->get_reader_1();
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XmlReader::get_Depth() */, L_21);
		int32_t L_23 = V_0;
		if ((((int32_t)L_22) <= ((int32_t)L_23)))
		{
			goto IL_00e4;
		}
	}
	{
		XmlReader_t4229084514 * L_24 = __this->get_reader_1();
		NullCheck(L_24);
		bool L_25 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean System.Xml.XmlReader::get_EOF() */, L_24);
		if (!L_25)
		{
			goto IL_00bc;
		}
	}

IL_00e4:
	{
		XmlReader_t4229084514 * L_26 = __this->get_reader_1();
		NullCheck(L_26);
		bool L_27 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XmlReader::get_IsEmptyElement() */, L_26);
		if (!L_27)
		{
			goto IL_0100;
		}
	}
	{
		XmlReader_t4229084514 * L_28 = __this->get_reader_1();
		NullCheck(L_28);
		VirtFuncInvoker0< bool >::Invoke(42 /* System.Boolean System.Xml.XmlReader::Read() */, L_28);
	}

IL_0100:
	{
		XmlReader_t4229084514 * L_29 = __this->get_reader_1();
		NullCheck(L_29);
		int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_29);
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_011d;
		}
	}
	{
		XmlReader_t4229084514 * L_31 = __this->get_reader_1();
		NullCheck(L_31);
		VirtActionInvoker0::Invoke(45 /* System.Void System.Xml.XmlReader::ReadEndElement() */, L_31);
	}

IL_011d:
	{
		XmlReader_t4229084514 * L_32 = __this->get_reader_1();
		NullCheck(L_32);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_32);
	}

IL_0129:
	{
		return;
	}
}
// System.Void System.Data.XmlDataReader::ReadElementAttribute(System.Data.DataRow)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlDataReader_ReadElementAttribute_m507573950_MetadataUsageId;
extern "C"  void XmlDataReader_ReadElementAttribute_m507573950 (XmlDataReader_t4112562711 * __this, DataRow_t3654701923 * ___row0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataReader_ReadElementAttribute_m507573950_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataColumn_t3354469747 * V_0 = NULL;
	{
		DataRow_t3654701923 * L_0 = ___row0;
		NullCheck(L_0);
		DataTable_t2176726999 * L_1 = DataRow_get_Table_m1600403292(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		DataColumnCollection_t3528392753 * L_2 = DataTable_get_Columns_m220042291(L_1, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_3 = __this->get_reader_1();
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_5 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		DataColumn_t3354469747 * L_6 = DataColumnCollection_get_Item_m305848743(L_2, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		DataColumn_t3354469747 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		DataColumn_t3354469747 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = DataColumn_get_Namespace_m546686590(L_8, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_10 = __this->get_reader_1();
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0043;
		}
	}

IL_0042:
	{
		return;
	}

IL_0043:
	{
		DataRow_t3654701923 * L_13 = ___row0;
		DataColumn_t3354469747 * L_14 = V_0;
		DataColumn_t3354469747 * L_15 = V_0;
		NullCheck(L_15);
		Type_t * L_16 = DataColumn_get_DataType_m3376662490(L_15, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_17 = __this->get_reader_1();
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(26 /* System.String System.Xml.XmlReader::get_Value() */, L_17);
		Il2CppObject * L_19 = XmlDataReader_StringToObject_m140544044(NULL /*static, unused*/, L_16, L_18, /*hidden argument*/NULL);
		NullCheck(L_13);
		DataRow_set_Item_m3968127800(L_13, L_14, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlDataReader::ReadElementContent(System.Data.DataRow)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlDataReader_ReadElementContent_m1908626945_MetadataUsageId;
extern "C"  void XmlDataReader_ReadElementContent_m1908626945 (XmlDataReader_t4112562711 * __this, DataRow_t3654701923 * ___row0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataReader_ReadElementContent_m1908626945_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	DataColumn_t3354469747 * V_1 = NULL;
	DataColumnCollection_t3528392753 * V_2 = NULL;
	int32_t V_3 = 0;
	DataColumn_t3354469747 * V_4 = NULL;
	String_t* V_5 = NULL;
	DataRow_t3654701923 * V_6 = NULL;
	DataColumn_t3354469747 * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	{
		XmlReader_t4229084514 * L_0 = __this->get_reader_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 0)
		{
			goto IL_003f;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 1)
		{
			goto IL_0024;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 2)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 3)
		{
			goto IL_004b;
		}
	}

IL_0024:
	{
		int32_t L_3 = V_0;
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)13))) == 0)
		{
			goto IL_00d3;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)13))) == 1)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)13))) == 2)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_00e4;
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		DataRow_t3654701923 * L_4 = ___row0;
		XmlDataReader_ReadElementElement_m4185664350(__this, L_4, /*hidden argument*/NULL);
		goto IL_00e4;
	}

IL_004b:
	{
		V_1 = (DataColumn_t3354469747 *)NULL;
		DataRow_t3654701923 * L_5 = ___row0;
		NullCheck(L_5);
		DataTable_t2176726999 * L_6 = DataRow_get_Table_m1600403292(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		DataColumnCollection_t3528392753 * L_7 = DataTable_get_Columns_m220042291(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		V_3 = 0;
		goto IL_0082;
	}

IL_0060:
	{
		DataColumnCollection_t3528392753 * L_8 = V_2;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		DataColumn_t3354469747 * L_10 = DataColumnCollection_get_Item_m2766080524(L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		DataColumn_t3354469747 * L_11 = V_4;
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_11);
		if ((!(((uint32_t)L_12) == ((uint32_t)3))))
		{
			goto IL_007e;
		}
	}
	{
		DataColumn_t3354469747 * L_13 = V_4;
		V_1 = L_13;
		goto IL_008e;
	}

IL_007e:
	{
		int32_t L_14 = V_3;
		V_3 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0082:
	{
		int32_t L_15 = V_3;
		DataColumnCollection_t3528392753 * L_16 = V_2;
		NullCheck(L_16);
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.Data.InternalDataCollectionBase::get_Count() */, L_16);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0060;
		}
	}

IL_008e:
	{
		XmlReader_t4229084514 * L_18 = __this->get_reader_1();
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(50 /* System.String System.Xml.XmlReader::ReadString() */, L_18);
		V_5 = L_19;
		XmlReader_t4229084514 * L_20 = __this->get_reader_1();
		NullCheck(L_20);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_20);
		DataColumn_t3354469747 * L_21 = V_1;
		if (!L_21)
		{
			goto IL_00ce;
		}
	}
	{
		DataRow_t3654701923 * L_22 = ___row0;
		DataRow_t3654701923 * L_23 = L_22;
		V_6 = L_23;
		DataColumn_t3354469747 * L_24 = V_1;
		DataColumn_t3354469747 * L_25 = L_24;
		V_7 = L_25;
		DataRow_t3654701923 * L_26 = V_6;
		DataColumn_t3354469747 * L_27 = V_7;
		NullCheck(L_26);
		Il2CppObject * L_28 = DataRow_get_Item_m854463265(L_26, L_27, /*hidden argument*/NULL);
		V_8 = L_28;
		Il2CppObject * L_29 = V_8;
		String_t* L_30 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m389863537(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_23);
		DataRow_set_Item_m3968127800(L_23, L_25, L_31, /*hidden argument*/NULL);
	}

IL_00ce:
	{
		goto IL_00e4;
	}

IL_00d3:
	{
		XmlReader_t4229084514 * L_32 = __this->get_reader_1();
		NullCheck(L_32);
		VirtFuncInvoker0< String_t* >::Invoke(50 /* System.String System.Xml.XmlReader::ReadString() */, L_32);
		goto IL_00e4;
	}

IL_00e4:
	{
		return;
	}
}
// System.Void System.Data.XmlDataReader::ReadElementElement(System.Data.DataRow)
extern const Il2CppType* IXmlSerializable_t1192716491_0_0_0_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* IXmlSerializable_t1192716491_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t XmlDataReader_ReadElementElement_m4185664350_MetadataUsageId;
extern "C"  void XmlDataReader_ReadElementElement_m4185664350 (XmlDataReader_t4112562711 * __this, DataRow_t3654701923 * ___row0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataReader_ReadElementElement_m4185664350_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataColumn_t3354469747 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	XmlException_t3490696160 * V_4 = NULL;
	InvalidOperationException_t2420574324 * V_5 = NULL;
	DataRelationCollection_t267599063 * V_6 = NULL;
	int32_t V_7 = 0;
	DataRelation_t1483987353 * V_8 = NULL;
	DataTable_t2176726999 * V_9 = NULL;
	DataRow_t3654701923 * V_10 = NULL;
	int32_t V_11 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		DataRow_t3654701923 * L_0 = ___row0;
		NullCheck(L_0);
		DataTable_t2176726999 * L_1 = DataRow_get_Table_m1600403292(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		DataColumnCollection_t3528392753 * L_2 = DataTable_get_Columns_m220042291(L_1, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_3 = __this->get_reader_1();
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_5 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		DataColumn_t3354469747 * L_6 = DataColumnCollection_get_Item_m305848743(L_2, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		DataColumn_t3354469747 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		DataColumn_t3354469747 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = DataColumn_get_Namespace_m546686590(L_8, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_10 = __this->get_reader_1();
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0044;
		}
	}

IL_0042:
	{
		V_0 = (DataColumn_t3354469747 *)NULL;
	}

IL_0044:
	{
		DataColumn_t3354469747 * L_13 = V_0;
		if (!L_13)
		{
			goto IL_01b0;
		}
	}
	{
		DataColumn_t3354469747 * L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_14);
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_01b0;
		}
	}
	{
		DataColumn_t3354469747 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = DataColumn_get_Namespace_m546686590(L_16, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_18 = __this->get_reader_1();
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_007d;
		}
	}
	{
		XmlReader_t4229084514 * L_21 = __this->get_reader_1();
		NullCheck(L_21);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_21);
		return;
	}

IL_007d:
	{
		XmlReader_t4229084514 * L_22 = __this->get_reader_1();
		NullCheck(L_22);
		bool L_23 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XmlReader::get_IsEmptyElement() */, L_22);
		V_1 = L_23;
		XmlReader_t4229084514 * L_24 = __this->get_reader_1();
		NullCheck(L_24);
		int32_t L_25 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XmlReader::get_Depth() */, L_24);
		V_2 = L_25;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IXmlSerializable_t1192716491_0_0_0_var), /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_27 = V_0;
		NullCheck(L_27);
		Type_t * L_28 = DataColumn_get_DataType_m3376662490(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		bool L_29 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_26, L_28);
		if (!L_29)
		{
			goto IL_0141;
		}
	}

IL_00af:
	try
	{ // begin try (depth: 1)
		{
			DataColumn_t3354469747 * L_30 = V_0;
			NullCheck(L_30);
			Type_t * L_31 = DataColumn_get_DataType_m3376662490(L_30, /*hidden argument*/NULL);
			Il2CppObject * L_32 = Activator_CreateInstance_m2161363287(NULL /*static, unused*/, L_31, ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
			V_3 = ((Il2CppObject *)Castclass(L_32, IXmlSerializable_t1192716491_il2cpp_TypeInfo_var));
			XmlReader_t4229084514 * L_33 = __this->get_reader_1();
			NullCheck(L_33);
			bool L_34 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XmlReader::get_IsEmptyElement() */, L_33);
			if (L_34)
			{
				goto IL_00f2;
			}
		}

IL_00d6:
		{
			Il2CppObject * L_35 = V_3;
			XmlReader_t4229084514 * L_36 = __this->get_reader_1();
			NullCheck(L_35);
			InterfaceActionInvoker1< XmlReader_t4229084514 * >::Invoke(0 /* System.Void System.Xml.Serialization.IXmlSerializable::ReadXml(System.Xml.XmlReader) */, IXmlSerializable_t1192716491_il2cpp_TypeInfo_var, L_35, L_36);
			XmlReader_t4229084514 * L_37 = __this->get_reader_1();
			NullCheck(L_37);
			VirtActionInvoker0::Invoke(45 /* System.Void System.Xml.XmlReader::ReadEndElement() */, L_37);
			goto IL_00fd;
		}

IL_00f2:
		{
			XmlReader_t4229084514 * L_38 = __this->get_reader_1();
			NullCheck(L_38);
			VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_38);
		}

IL_00fd:
		{
			DataRow_t3654701923 * L_39 = ___row0;
			DataColumn_t3354469747 * L_40 = V_0;
			Il2CppObject * L_41 = V_3;
			NullCheck(L_39);
			DataRow_set_Item_m3968127800(L_39, L_40, L_41, /*hidden argument*/NULL);
			goto IL_013c;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (XmlException_t3490696160_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_010a;
		if(il2cpp_codegen_class_is_assignable_from (InvalidOperationException_t2420574324_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0123;
		throw e;
	}

CATCH_010a:
	{ // begin catch(System.Xml.XmlException)
		V_4 = ((XmlException_t3490696160 *)__exception_local);
		DataRow_t3654701923 * L_42 = ___row0;
		DataColumn_t3354469747 * L_43 = V_0;
		XmlReader_t4229084514 * L_44 = __this->get_reader_1();
		NullCheck(L_44);
		String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(46 /* System.String System.Xml.XmlReader::ReadInnerXml() */, L_44);
		NullCheck(L_42);
		DataRow_set_Item_m3968127800(L_42, L_43, L_45, /*hidden argument*/NULL);
		goto IL_013c;
	} // end catch (depth: 1)

CATCH_0123:
	{ // begin catch(System.InvalidOperationException)
		V_5 = ((InvalidOperationException_t2420574324 *)__exception_local);
		DataRow_t3654701923 * L_46 = ___row0;
		DataColumn_t3354469747 * L_47 = V_0;
		XmlReader_t4229084514 * L_48 = __this->get_reader_1();
		NullCheck(L_48);
		String_t* L_49 = VirtFuncInvoker0< String_t* >::Invoke(46 /* System.String System.Xml.XmlReader::ReadInnerXml() */, L_48);
		NullCheck(L_46);
		DataRow_set_Item_m3968127800(L_46, L_47, L_49, /*hidden argument*/NULL);
		goto IL_013c;
	} // end catch (depth: 1)

IL_013c:
	{
		goto IL_015e;
	}

IL_0141:
	{
		DataRow_t3654701923 * L_50 = ___row0;
		DataColumn_t3354469747 * L_51 = V_0;
		DataColumn_t3354469747 * L_52 = V_0;
		NullCheck(L_52);
		Type_t * L_53 = DataColumn_get_DataType_m3376662490(L_52, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_54 = __this->get_reader_1();
		NullCheck(L_54);
		String_t* L_55 = VirtFuncInvoker0< String_t* >::Invoke(44 /* System.String System.Xml.XmlReader::ReadElementString() */, L_54);
		Il2CppObject * L_56 = XmlDataReader_StringToObject_m140544044(NULL /*static, unused*/, L_53, L_55, /*hidden argument*/NULL);
		NullCheck(L_50);
		DataRow_set_Item_m3968127800(L_50, L_51, L_56, /*hidden argument*/NULL);
	}

IL_015e:
	{
		bool L_57 = V_1;
		if (L_57)
		{
			goto IL_01a3;
		}
	}
	{
		XmlReader_t4229084514 * L_58 = __this->get_reader_1();
		NullCheck(L_58);
		int32_t L_59 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XmlReader::get_Depth() */, L_58);
		int32_t L_60 = V_2;
		if ((((int32_t)L_59) <= ((int32_t)L_60)))
		{
			goto IL_01a3;
		}
	}
	{
		goto IL_0186;
	}

IL_017a:
	{
		XmlReader_t4229084514 * L_61 = __this->get_reader_1();
		NullCheck(L_61);
		VirtFuncInvoker0< bool >::Invoke(42 /* System.Boolean System.Xml.XmlReader::Read() */, L_61);
	}

IL_0186:
	{
		XmlReader_t4229084514 * L_62 = __this->get_reader_1();
		NullCheck(L_62);
		int32_t L_63 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XmlReader::get_Depth() */, L_62);
		int32_t L_64 = V_2;
		if ((((int32_t)L_63) > ((int32_t)L_64)))
		{
			goto IL_017a;
		}
	}
	{
		XmlReader_t4229084514 * L_65 = __this->get_reader_1();
		NullCheck(L_65);
		VirtFuncInvoker0< bool >::Invoke(42 /* System.Boolean System.Xml.XmlReader::Read() */, L_65);
	}

IL_01a3:
	{
		XmlReader_t4229084514 * L_66 = __this->get_reader_1();
		NullCheck(L_66);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_66);
		return;
	}

IL_01b0:
	{
		DataColumn_t3354469747 * L_67 = V_0;
		if (!L_67)
		{
			goto IL_01ce;
		}
	}
	{
		XmlReader_t4229084514 * L_68 = __this->get_reader_1();
		NullCheck(L_68);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_68);
		XmlReader_t4229084514 * L_69 = __this->get_reader_1();
		NullCheck(L_69);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_69);
		return;
	}

IL_01ce:
	{
		DataRow_t3654701923 * L_70 = ___row0;
		NullCheck(L_70);
		DataTable_t2176726999 * L_71 = DataRow_get_Table_m1600403292(L_70, /*hidden argument*/NULL);
		NullCheck(L_71);
		DataRelationCollection_t267599063 * L_72 = DataTable_get_ChildRelations_m2551063725(L_71, /*hidden argument*/NULL);
		V_6 = L_72;
		V_7 = 0;
		goto IL_02b9;
	}

IL_01e3:
	{
		DataRelationCollection_t267599063 * L_73 = V_6;
		int32_t L_74 = V_7;
		NullCheck(L_73);
		DataRelation_t1483987353 * L_75 = VirtFuncInvoker1< DataRelation_t1483987353 *, int32_t >::Invoke(14 /* System.Data.DataRelation System.Data.DataRelationCollection::get_Item(System.Int32) */, L_73, L_74);
		V_8 = L_75;
		DataRelation_t1483987353 * L_76 = V_8;
		NullCheck(L_76);
		bool L_77 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean System.Data.DataRelation::get_Nested() */, L_76);
		if (L_77)
		{
			goto IL_01ff;
		}
	}
	{
		goto IL_02b3;
	}

IL_01ff:
	{
		DataRelation_t1483987353 * L_78 = V_8;
		NullCheck(L_78);
		DataTable_t2176726999 * L_79 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(6 /* System.Data.DataTable System.Data.DataRelation::get_ChildTable() */, L_78);
		V_9 = L_79;
		DataTable_t2176726999 * L_80 = V_9;
		NullCheck(L_80);
		String_t* L_81 = DataTable_get_TableName_m3141812994(L_80, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_82 = __this->get_reader_1();
		NullCheck(L_82);
		String_t* L_83 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_82);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_84 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_83, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_85 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_81, L_84, /*hidden argument*/NULL);
		if (L_85)
		{
			goto IL_0245;
		}
	}
	{
		DataTable_t2176726999 * L_86 = V_9;
		NullCheck(L_86);
		String_t* L_87 = DataTable_get_Namespace_m3829929060(L_86, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_88 = __this->get_reader_1();
		NullCheck(L_88);
		String_t* L_89 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_88);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_90 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_87, L_89, /*hidden argument*/NULL);
		if (!L_90)
		{
			goto IL_024a;
		}
	}

IL_0245:
	{
		goto IL_02b3;
	}

IL_024a:
	{
		DataRelation_t1483987353 * L_91 = V_8;
		NullCheck(L_91);
		DataTable_t2176726999 * L_92 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(6 /* System.Data.DataTable System.Data.DataRelation::get_ChildTable() */, L_91);
		NullCheck(L_92);
		DataRow_t3654701923 * L_93 = DataTable_NewRow_m3256434597(L_92, /*hidden argument*/NULL);
		V_10 = L_93;
		DataRow_t3654701923 * L_94 = V_10;
		XmlDataReader_ReadElement_m2762657682(__this, L_94, /*hidden argument*/NULL);
		V_11 = 0;
		goto IL_028f;
	}

IL_0268:
	{
		DataRow_t3654701923 * L_95 = V_10;
		DataRelation_t1483987353 * L_96 = V_8;
		NullCheck(L_96);
		DataColumnU5BU5D_t3410743138* L_97 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(4 /* System.Data.DataColumn[] System.Data.DataRelation::get_ChildColumns() */, L_96);
		int32_t L_98 = V_11;
		NullCheck(L_97);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_97, L_98);
		int32_t L_99 = L_98;
		DataRow_t3654701923 * L_100 = ___row0;
		DataRelation_t1483987353 * L_101 = V_8;
		NullCheck(L_101);
		DataColumnU5BU5D_t3410743138* L_102 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(10 /* System.Data.DataColumn[] System.Data.DataRelation::get_ParentColumns() */, L_101);
		int32_t L_103 = V_11;
		NullCheck(L_102);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_102, L_103);
		int32_t L_104 = L_103;
		NullCheck(L_100);
		Il2CppObject * L_105 = DataRow_get_Item_m854463265(L_100, ((L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104))), /*hidden argument*/NULL);
		NullCheck(L_95);
		DataRow_set_Item_m3968127800(L_95, ((L_97)->GetAt(static_cast<il2cpp_array_size_t>(L_99))), L_105, /*hidden argument*/NULL);
		int32_t L_106 = V_11;
		V_11 = ((int32_t)((int32_t)L_106+(int32_t)1));
	}

IL_028f:
	{
		int32_t L_107 = V_11;
		DataRelation_t1483987353 * L_108 = V_8;
		NullCheck(L_108);
		DataColumnU5BU5D_t3410743138* L_109 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(4 /* System.Data.DataColumn[] System.Data.DataRelation::get_ChildColumns() */, L_108);
		NullCheck(L_109);
		if ((((int32_t)L_107) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_109)->max_length)))))))
		{
			goto IL_0268;
		}
	}
	{
		DataRelation_t1483987353 * L_110 = V_8;
		NullCheck(L_110);
		DataTable_t2176726999 * L_111 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(6 /* System.Data.DataTable System.Data.DataRelation::get_ChildTable() */, L_110);
		NullCheck(L_111);
		DataRowCollection_t1405583905 * L_112 = DataTable_get_Rows_m954608043(L_111, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_113 = V_10;
		NullCheck(L_112);
		DataRowCollection_Add_m2125891809(L_112, L_113, /*hidden argument*/NULL);
		return;
	}

IL_02b3:
	{
		int32_t L_114 = V_7;
		V_7 = ((int32_t)((int32_t)L_114+(int32_t)1));
	}

IL_02b9:
	{
		int32_t L_115 = V_7;
		DataRelationCollection_t267599063 * L_116 = V_6;
		NullCheck(L_116);
		int32_t L_117 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.Data.InternalDataCollectionBase::get_Count() */, L_116);
		if ((((int32_t)L_115) < ((int32_t)L_117)))
		{
			goto IL_01e3;
		}
	}
	{
		XmlReader_t4229084514 * L_118 = __this->get_reader_1();
		NullCheck(L_118);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_118);
		XmlReader_t4229084514 * L_119 = __this->get_reader_1();
		NullCheck(L_119);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_119);
		return;
	}
}
// System.Object System.Data.XmlDataReader::StringToObject(System.Type,System.String)
extern const Il2CppType* TimeSpan_t763862892_0_0_0_var;
extern const Il2CppType* Guid_t2778838590_0_0_0_var;
extern const Il2CppType* ByteU5BU5D_t58506160_0_0_0_var;
extern const Il2CppType* Type_t_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlConvert_t1882388356_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t2778693821_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t1688557254_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t2847414729_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* SByte_t2855346064_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t985925268_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t985925326_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t985925421_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppClass* Guid_t2778838590_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t XmlDataReader_StringToObject_m140544044_MetadataUsageId;
extern "C"  Il2CppObject * XmlDataReader_StringToObject_m140544044 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDataReader_StringToObject_m140544044_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Type_t * L_0 = ___type0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		String_t* L_1 = ___value1;
		return L_1;
	}

IL_0008:
	{
		Type_t * L_2 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_3 = Type_GetTypeCode_m2969996822(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 0)
		{
			goto IL_0054;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 1)
		{
			goto IL_006c;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 2)
		{
			goto IL_00c2;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 3)
		{
			goto IL_0060;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 4)
		{
			goto IL_009e;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 5)
		{
			goto IL_00da;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 6)
		{
			goto IL_00aa;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 7)
		{
			goto IL_00e6;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 8)
		{
			goto IL_00b6;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 9)
		{
			goto IL_00f2;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 10)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 11)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 12)
		{
			goto IL_0086;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)3)) == 13)
		{
			goto IL_0079;
		}
	}
	{
		goto IL_00fe;
	}

IL_0054:
	{
		String_t* L_5 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		bool L_6 = XmlConvert_ToBoolean_m3758854944(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		bool L_7 = L_6;
		Il2CppObject * L_8 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_7);
		return L_8;
	}

IL_0060:
	{
		String_t* L_9 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		uint8_t L_10 = XmlConvert_ToByte_m1845524822(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		uint8_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Byte_t2778693821_il2cpp_TypeInfo_var, &L_11);
		return L_12;
	}

IL_006c:
	{
		String_t* L_13 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		int32_t L_14 = XmlConvert_ToInt32_m3773620768(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		uint16_t L_15 = ((uint16_t)(((int32_t)((uint16_t)L_14))));
		Il2CppObject * L_16 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_15);
		return L_16;
	}

IL_0079:
	{
		String_t* L_17 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_18 = XmlConvert_ToDateTime_m4285748037(NULL /*static, unused*/, L_17, 2, /*hidden argument*/NULL);
		DateTime_t339033936  L_19 = L_18;
		Il2CppObject * L_20 = Box(DateTime_t339033936_il2cpp_TypeInfo_var, &L_19);
		return L_20;
	}

IL_0086:
	{
		String_t* L_21 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		Decimal_t1688557254  L_22 = XmlConvert_ToDecimal_m3464816128(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		Decimal_t1688557254  L_23 = L_22;
		Il2CppObject * L_24 = Box(Decimal_t1688557254_il2cpp_TypeInfo_var, &L_23);
		return L_24;
	}

IL_0092:
	{
		String_t* L_25 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		double L_26 = XmlConvert_ToDouble_m1758308804(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		double L_27 = L_26;
		Il2CppObject * L_28 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_27);
		return L_28;
	}

IL_009e:
	{
		String_t* L_29 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		int16_t L_30 = XmlConvert_ToInt16_m674493152(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		int16_t L_31 = L_30;
		Il2CppObject * L_32 = Box(Int16_t2847414729_il2cpp_TypeInfo_var, &L_31);
		return L_32;
	}

IL_00aa:
	{
		String_t* L_33 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		int32_t L_34 = XmlConvert_ToInt32_m3773620768(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		int32_t L_35 = L_34;
		Il2CppObject * L_36 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_35);
		return L_36;
	}

IL_00b6:
	{
		String_t* L_37 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		int64_t L_38 = XmlConvert_ToInt64_m778201600(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		int64_t L_39 = L_38;
		Il2CppObject * L_40 = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &L_39);
		return L_40;
	}

IL_00c2:
	{
		String_t* L_41 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		int8_t L_42 = XmlConvert_ToSByte_m29345728(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		int8_t L_43 = L_42;
		Il2CppObject * L_44 = Box(SByte_t2855346064_il2cpp_TypeInfo_var, &L_43);
		return L_44;
	}

IL_00ce:
	{
		String_t* L_45 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		float L_46 = XmlConvert_ToSingle_m34142998(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		float L_47 = L_46;
		Il2CppObject * L_48 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_47);
		return L_48;
	}

IL_00da:
	{
		String_t* L_49 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		uint16_t L_50 = XmlConvert_ToUInt16_m1167065128(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		uint16_t L_51 = L_50;
		Il2CppObject * L_52 = Box(UInt16_t985925268_il2cpp_TypeInfo_var, &L_51);
		return L_52;
	}

IL_00e6:
	{
		String_t* L_53 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		uint32_t L_54 = XmlConvert_ToUInt32_m2021978420(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		uint32_t L_55 = L_54;
		Il2CppObject * L_56 = Box(UInt32_t985925326_il2cpp_TypeInfo_var, &L_55);
		return L_56;
	}

IL_00f2:
	{
		String_t* L_57 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		uint64_t L_58 = XmlConvert_ToUInt64_m2681755830(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		uint64_t L_59 = L_58;
		Il2CppObject * L_60 = Box(UInt64_t985925421_il2cpp_TypeInfo_var, &L_59);
		return L_60;
	}

IL_00fe:
	{
		Type_t * L_61 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_62 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(TimeSpan_t763862892_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_61) == ((Il2CppObject*)(Type_t *)L_62))))
		{
			goto IL_011a;
		}
	}
	{
		String_t* L_63 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_64 = XmlConvert_ToTimeSpan_m83474552(NULL /*static, unused*/, L_63, /*hidden argument*/NULL);
		TimeSpan_t763862892  L_65 = L_64;
		Il2CppObject * L_66 = Box(TimeSpan_t763862892_il2cpp_TypeInfo_var, &L_65);
		return L_66;
	}

IL_011a:
	{
		Type_t * L_67 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_68 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Guid_t2778838590_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_67) == ((Il2CppObject*)(Type_t *)L_68))))
		{
			goto IL_0136;
		}
	}
	{
		String_t* L_69 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		Guid_t2778838590  L_70 = XmlConvert_ToGuid_m1476180436(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
		Guid_t2778838590  L_71 = L_70;
		Il2CppObject * L_72 = Box(Guid_t2778838590_il2cpp_TypeInfo_var, &L_71);
		return L_72;
	}

IL_0136:
	{
		Type_t * L_73 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_74 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ByteU5BU5D_t58506160_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_73) == ((Il2CppObject*)(Type_t *)L_74))))
		{
			goto IL_014d;
		}
	}
	{
		String_t* L_75 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		ByteU5BU5D_t58506160* L_76 = Convert_FromBase64String_m901846280(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		return (Il2CppObject *)L_76;
	}

IL_014d:
	{
		Type_t * L_77 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_78 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Type_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_77) == ((Il2CppObject*)(Type_t *)L_78))))
		{
			goto IL_0164;
		}
	}
	{
		String_t* L_79 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_80 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m2877589631, L_79, "System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
		return L_80;
	}

IL_0164:
	{
		String_t* L_81 = ___value1;
		Type_t * L_82 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		Il2CppObject * L_83 = Convert_ChangeType_m2922880930(NULL /*static, unused*/, L_81, L_82, /*hidden argument*/NULL);
		return L_83;
	}
}
// System.Void System.Data.XmlDiffLoader::.ctor(System.Data.DataSet)
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern const uint32_t XmlDiffLoader__ctor_m3102433049_MetadataUsageId;
extern "C"  void XmlDiffLoader__ctor_m3102433049 (XmlDiffLoader_t3888299394 * __this, DataSet_t3654702571 * ___DSet0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDiffLoader__ctor_m3102433049_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t3875263730 * L_0 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		__this->set_DiffGrRows_2(L_0);
		Hashtable_t3875263730 * L_1 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_1, /*hidden argument*/NULL);
		__this->set_ErrorRows_3(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		DataSet_t3654702571 * L_2 = ___DSet0;
		__this->set_DSet_0(L_2);
		return;
	}
}
// System.Void System.Data.XmlDiffLoader::Load(System.Xml.XmlReader)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4198501340;
extern Il2CppCodeGenString* _stringLiteral1269506330;
extern Il2CppCodeGenString* _stringLiteral2902081407;
extern Il2CppCodeGenString* _stringLiteral3000332139;
extern const uint32_t XmlDiffLoader_Load_m3514910400_MetadataUsageId;
extern "C"  void XmlDiffLoader_Load_m3514910400 (XmlDiffLoader_t3888299394 * __this, XmlReader_t4229084514 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDiffLoader_Load_m3514910400_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		DataSet_t3654702571 * L_0 = __this->get_DSet_0();
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		DataSet_t3654702571 * L_1 = __this->get_DSet_0();
		NullCheck(L_1);
		bool L_2 = DataSet_get_EnforceConstraints_m1069235578(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DataSet_t3654702571 * L_3 = __this->get_DSet_0();
		NullCheck(L_3);
		DataSet_set_EnforceConstraints_m2529190639(L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_0025:
	{
		XmlReader_t4229084514 * L_4 = ___reader0;
		NullCheck(L_4);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_4);
		XmlReader_t4229084514 * L_5 = ___reader0;
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XmlReader::get_IsEmptyElement() */, L_5);
		if (!L_6)
		{
			goto IL_003e;
		}
	}
	{
		XmlReader_t4229084514 * L_7 = ___reader0;
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_7);
		return;
	}

IL_003e:
	{
		XmlReader_t4229084514 * L_8 = ___reader0;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(49 /* System.Void System.Xml.XmlReader::ReadStartElement(System.String,System.String) */, L_8, _stringLiteral4198501340, _stringLiteral1269506330);
		XmlReader_t4229084514 * L_9 = ___reader0;
		NullCheck(L_9);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_9);
		goto IL_00e4;
	}

IL_005a:
	{
		XmlReader_t4229084514 * L_10 = ___reader0;
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_10);
		if ((!(((uint32_t)L_11) == ((uint32_t)1))))
		{
			goto IL_00de;
		}
	}
	{
		XmlReader_t4229084514 * L_12 = ___reader0;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_13, _stringLiteral2902081407, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_009c;
		}
	}
	{
		XmlReader_t4229084514 * L_15 = ___reader0;
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_16, _stringLiteral1269506330, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009c;
		}
	}
	{
		XmlReader_t4229084514 * L_18 = ___reader0;
		XmlDiffLoader_LoadBefore_m4069417695(__this, L_18, /*hidden argument*/NULL);
		goto IL_00d9;
	}

IL_009c:
	{
		XmlReader_t4229084514 * L_19 = ___reader0;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_19);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_21 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_20, _stringLiteral3000332139, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00d2;
		}
	}
	{
		XmlReader_t4229084514 * L_22 = ___reader0;
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_22);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_24 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_23, _stringLiteral1269506330, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00d2;
		}
	}
	{
		XmlReader_t4229084514 * L_25 = ___reader0;
		XmlDiffLoader_LoadErrors_m3540359627(__this, L_25, /*hidden argument*/NULL);
		goto IL_00d9;
	}

IL_00d2:
	{
		XmlReader_t4229084514 * L_26 = ___reader0;
		XmlDiffLoader_LoadCurrent_m1810173299(__this, L_26, /*hidden argument*/NULL);
	}

IL_00d9:
	{
		goto IL_00e4;
	}

IL_00de:
	{
		XmlReader_t4229084514 * L_27 = ___reader0;
		NullCheck(L_27);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_27);
	}

IL_00e4:
	{
		XmlReader_t4229084514 * L_28 = ___reader0;
		NullCheck(L_28);
		int32_t L_29 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_28);
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_005a;
		}
	}
	{
		XmlReader_t4229084514 * L_30 = ___reader0;
		NullCheck(L_30);
		VirtActionInvoker0::Invoke(45 /* System.Void System.Xml.XmlReader::ReadEndElement() */, L_30);
		DataSet_t3654702571 * L_31 = __this->get_DSet_0();
		if (!L_31)
		{
			goto IL_010e;
		}
	}
	{
		DataSet_t3654702571 * L_32 = __this->get_DSet_0();
		bool L_33 = V_0;
		NullCheck(L_32);
		DataSet_set_EnforceConstraints_m2529190639(L_32, L_33, /*hidden argument*/NULL);
	}

IL_010e:
	{
		return;
	}
}
// System.Void System.Data.XmlDiffLoader::LoadCurrent(System.Xml.XmlReader)
extern "C"  void XmlDiffLoader_LoadCurrent_m1810173299 (XmlDiffLoader_t3888299394 * __this, XmlReader_t4229084514 * ___reader0, const MethodInfo* method)
{
	DataTable_t2176726999 * V_0 = NULL;
	{
		XmlReader_t4229084514 * L_0 = ___reader0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XmlReader::get_IsEmptyElement() */, L_0);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		XmlReader_t4229084514 * L_2 = ___reader0;
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_2);
		return;
	}

IL_0012:
	{
		XmlReader_t4229084514 * L_3 = ___reader0;
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(48 /* System.Void System.Xml.XmlReader::ReadStartElement() */, L_3);
		XmlReader_t4229084514 * L_4 = ___reader0;
		NullCheck(L_4);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_4);
		goto IL_0061;
	}

IL_0024:
	{
		XmlReader_t4229084514 * L_5 = ___reader0;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_5);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_005b;
		}
	}
	{
		XmlReader_t4229084514 * L_7 = ___reader0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_7);
		DataTable_t2176726999 * L_9 = XmlDiffLoader_GetTable_m2626451904(__this, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		DataTable_t2176726999 * L_10 = V_0;
		if (!L_10)
		{
			goto IL_0050;
		}
	}
	{
		DataTable_t2176726999 * L_11 = V_0;
		XmlReader_t4229084514 * L_12 = ___reader0;
		XmlDiffLoader_LoadCurrentTable_m1506874120(__this, L_11, L_12, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_0050:
	{
		XmlReader_t4229084514 * L_13 = ___reader0;
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_13);
	}

IL_0056:
	{
		goto IL_0061;
	}

IL_005b:
	{
		XmlReader_t4229084514 * L_14 = ___reader0;
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_14);
	}

IL_0061:
	{
		XmlReader_t4229084514 * L_15 = ___reader0;
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_15);
		if ((!(((uint32_t)L_16) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0024;
		}
	}
	{
		XmlReader_t4229084514 * L_17 = ___reader0;
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(45 /* System.Void System.Xml.XmlReader::ReadEndElement() */, L_17);
		return;
	}
}
// System.Void System.Data.XmlDiffLoader::LoadBefore(System.Xml.XmlReader)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DataException_t1022144856_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2725907626;
extern Il2CppCodeGenString* _stringLiteral2568211123;
extern const uint32_t XmlDiffLoader_LoadBefore_m4069417695_MetadataUsageId;
extern "C"  void XmlDiffLoader_LoadBefore_m4069417695 (XmlDiffLoader_t3888299394 * __this, XmlReader_t4229084514 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDiffLoader_LoadBefore_m4069417695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataTable_t2176726999 * V_0 = NULL;
	{
		XmlReader_t4229084514 * L_0 = ___reader0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XmlReader::get_IsEmptyElement() */, L_0);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		XmlReader_t4229084514 * L_2 = ___reader0;
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_2);
		return;
	}

IL_0012:
	{
		XmlReader_t4229084514 * L_3 = ___reader0;
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(48 /* System.Void System.Xml.XmlReader::ReadStartElement() */, L_3);
		XmlReader_t4229084514 * L_4 = ___reader0;
		NullCheck(L_4);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_4);
		goto IL_007b;
	}

IL_0024:
	{
		XmlReader_t4229084514 * L_5 = ___reader0;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_5);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_0075;
		}
	}
	{
		XmlReader_t4229084514 * L_7 = ___reader0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_7);
		DataTable_t2176726999 * L_9 = XmlDiffLoader_GetTable_m2626451904(__this, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		DataTable_t2176726999 * L_10 = V_0;
		if (!L_10)
		{
			goto IL_0050;
		}
	}
	{
		DataTable_t2176726999 * L_11 = V_0;
		XmlReader_t4229084514 * L_12 = ___reader0;
		XmlDiffLoader_LoadBeforeTable_m4046043676(__this, L_11, L_12, /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0050:
	{
		XmlReader_t4229084514 * L_13 = ___reader0;
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2725907626, L_14, _stringLiteral2568211123, /*hidden argument*/NULL);
		String_t* L_16 = Locale_GetText_m1156491176(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		DataException_t1022144856 * L_17 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_17, L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_0070:
	{
		goto IL_007b;
	}

IL_0075:
	{
		XmlReader_t4229084514 * L_18 = ___reader0;
		NullCheck(L_18);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_18);
	}

IL_007b:
	{
		XmlReader_t4229084514 * L_19 = ___reader0;
		NullCheck(L_19);
		int32_t L_20 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_19);
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0024;
		}
	}
	{
		XmlReader_t4229084514 * L_21 = ___reader0;
		NullCheck(L_21);
		VirtActionInvoker0::Invoke(45 /* System.Void System.Xml.XmlReader::ReadEndElement() */, L_21);
		return;
	}
}
// System.Void System.Data.XmlDiffLoader::LoadErrors(System.Xml.XmlReader)
extern Il2CppClass* DataRow_t3654701923_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3355;
extern Il2CppCodeGenString* _stringLiteral1269506330;
extern Il2CppCodeGenString* _stringLiteral67232232;
extern const uint32_t XmlDiffLoader_LoadErrors_m3540359627_MetadataUsageId;
extern "C"  void XmlDiffLoader_LoadErrors_m3540359627 (XmlDiffLoader_t3888299394 * __this, XmlReader_t4229084514 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDiffLoader_LoadErrors_m3540359627_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataRow_t3654701923 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	{
		XmlReader_t4229084514 * L_0 = ___reader0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XmlReader::get_IsEmptyElement() */, L_0);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		XmlReader_t4229084514 * L_2 = ___reader0;
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_2);
		return;
	}

IL_0012:
	{
		XmlReader_t4229084514 * L_3 = ___reader0;
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(48 /* System.Void System.Xml.XmlReader::ReadStartElement() */, L_3);
		XmlReader_t4229084514 * L_4 = ___reader0;
		NullCheck(L_4);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_4);
		goto IL_00bf;
	}

IL_0024:
	{
		XmlReader_t4229084514 * L_5 = ___reader0;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_5);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_00b9;
		}
	}
	{
		V_0 = (DataRow_t3654701923 *)NULL;
		XmlReader_t4229084514 * L_7 = ___reader0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(33 /* System.String System.Xml.XmlReader::GetAttribute(System.String,System.String) */, L_7, _stringLiteral3355, _stringLiteral1269506330);
		V_1 = L_8;
		String_t* L_9 = V_1;
		if (!L_9)
		{
			goto IL_005b;
		}
	}
	{
		Hashtable_t3875263730 * L_10 = __this->get_ErrorRows_3();
		String_t* L_11 = V_1;
		NullCheck(L_10);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_10, L_11);
		V_0 = ((DataRow_t3654701923 *)CastclassClass(L_12, DataRow_t3654701923_il2cpp_TypeInfo_var));
	}

IL_005b:
	{
		XmlReader_t4229084514 * L_13 = ___reader0;
		NullCheck(L_13);
		bool L_14 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XmlReader::get_IsEmptyElement() */, L_13);
		if (!L_14)
		{
			goto IL_006b;
		}
	}
	{
		goto IL_00bf;
	}

IL_006b:
	{
		XmlReader_t4229084514 * L_15 = ___reader0;
		NullCheck(L_15);
		VirtActionInvoker0::Invoke(48 /* System.Void System.Xml.XmlReader::ReadStartElement() */, L_15);
		goto IL_00a7;
	}

IL_0076:
	{
		XmlReader_t4229084514 * L_16 = ___reader0;
		NullCheck(L_16);
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_16);
		if ((!(((uint32_t)L_17) == ((uint32_t)1))))
		{
			goto IL_00a0;
		}
	}
	{
		XmlReader_t4229084514 * L_18 = ___reader0;
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(33 /* System.String System.Xml.XmlReader::GetAttribute(System.String,System.String) */, L_18, _stringLiteral67232232, _stringLiteral1269506330);
		V_2 = L_19;
		DataRow_t3654701923 * L_20 = V_0;
		XmlReader_t4229084514 * L_21 = ___reader0;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_21);
		String_t* L_23 = V_2;
		NullCheck(L_20);
		DataRow_SetColumnError_m3581801103(L_20, L_22, L_23, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		XmlReader_t4229084514 * L_24 = ___reader0;
		NullCheck(L_24);
		VirtFuncInvoker0< bool >::Invoke(42 /* System.Boolean System.Xml.XmlReader::Read() */, L_24);
	}

IL_00a7:
	{
		XmlReader_t4229084514 * L_25 = ___reader0;
		NullCheck(L_25);
		int32_t L_26 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_25);
		if ((!(((uint32_t)L_26) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0076;
		}
	}
	{
		goto IL_00bf;
	}

IL_00b9:
	{
		XmlReader_t4229084514 * L_27 = ___reader0;
		NullCheck(L_27);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_27);
	}

IL_00bf:
	{
		XmlReader_t4229084514 * L_28 = ___reader0;
		NullCheck(L_28);
		int32_t L_29 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_28);
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0024;
		}
	}
	{
		XmlReader_t4229084514 * L_30 = ___reader0;
		NullCheck(L_30);
		VirtActionInvoker0::Invoke(45 /* System.Void System.Xml.XmlReader::ReadEndElement() */, L_30);
		return;
	}
}
// System.Void System.Data.XmlDiffLoader::LoadColumns(System.Data.DataTable,System.Data.DataRow,System.Xml.XmlReader,System.Data.DataRowVersion)
extern "C"  void XmlDiffLoader_LoadColumns_m455932784 (XmlDiffLoader_t3888299394 * __this, DataTable_t2176726999 * ___Table0, DataRow_t3654701923 * ___Row1, XmlReader_t4229084514 * ___reader2, int32_t ___loadType3, const MethodInfo* method)
{
	{
		DataTable_t2176726999 * L_0 = ___Table0;
		DataRow_t3654701923 * L_1 = ___Row1;
		XmlReader_t4229084514 * L_2 = ___reader2;
		int32_t L_3 = ___loadType3;
		XmlDiffLoader_LoadColumnAttributes_m3901174682(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_4 = ___Table0;
		DataRow_t3654701923 * L_5 = ___Row1;
		XmlReader_t4229084514 * L_6 = ___reader2;
		int32_t L_7 = ___loadType3;
		XmlDiffLoader_LoadColumnChildren_m2569100210(__this, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlDiffLoader::LoadColumnAttributes(System.Data.DataTable,System.Data.DataRow,System.Xml.XmlReader,System.Data.DataRowVersion)
extern Il2CppClass* XmlDiffLoader_t3888299394_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral557947472;
extern Il2CppCodeGenString* _stringLiteral1952986079;
extern Il2CppCodeGenString* _stringLiteral1269506330;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral3051547195;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern const uint32_t XmlDiffLoader_LoadColumnAttributes_m3901174682_MetadataUsageId;
extern "C"  void XmlDiffLoader_LoadColumnAttributes_m3901174682 (XmlDiffLoader_t3888299394 * __this, DataTable_t2176726999 * ___Table0, DataRow_t3654701923 * ___Row1, XmlReader_t4229084514 * ___reader2, int32_t ___loadType3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDiffLoader_LoadColumnAttributes_m3901174682_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t190145395 * V_1 = NULL;
	int32_t V_2 = 0;
	DataColumn_t3354469747 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	{
		XmlReader_t4229084514 * L_0 = ___reader2;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean System.Xml.XmlReader::get_HasAttributes() */, L_0);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		XmlReader_t4229084514 * L_2 = ___reader2;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(40 /* System.Boolean System.Xml.XmlReader::MoveToFirstAttribute() */, L_2);
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		return;
	}

IL_0017:
	{
		XmlReader_t4229084514 * L_4 = ___reader2;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_4);
		V_0 = L_5;
		String_t* L_6 = V_0;
		if (!L_6)
		{
			goto IL_00a5;
		}
	}
	{
		Dictionary_2_t190145395 * L_7 = ((XmlDiffLoader_t3888299394_StaticFields*)XmlDiffLoader_t3888299394_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_4();
		if (L_7)
		{
			goto IL_0083;
		}
	}
	{
		Dictionary_2_t190145395 * L_8 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_8, 6, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_1 = L_8;
		Dictionary_2_t190145395 * L_9 = V_1;
		NullCheck(L_9);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_9, _stringLiteral557947472, 0);
		Dictionary_2_t190145395 * L_10 = V_1;
		NullCheck(L_10);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_10, _stringLiteral1952986079, 0);
		Dictionary_2_t190145395 * L_11 = V_1;
		NullCheck(L_11);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_11, _stringLiteral1269506330, 0);
		Dictionary_2_t190145395 * L_12 = V_1;
		NullCheck(L_12);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_12, _stringLiteral3051173506, 0);
		Dictionary_2_t190145395 * L_13 = V_1;
		NullCheck(L_13);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_13, _stringLiteral3051547195, 0);
		Dictionary_2_t190145395 * L_14 = V_1;
		NullCheck(L_14);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_14, _stringLiteral1440052060, 0);
		Dictionary_2_t190145395 * L_15 = V_1;
		((XmlDiffLoader_t3888299394_StaticFields*)XmlDiffLoader_t3888299394_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map0_4(L_15);
	}

IL_0083:
	{
		Dictionary_2_t190145395 * L_16 = ((XmlDiffLoader_t3888299394_StaticFields*)XmlDiffLoader_t3888299394_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_4();
		String_t* L_17 = V_0;
		NullCheck(L_16);
		bool L_18 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(35 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_16, L_17, (&V_2));
		if (!L_18)
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_19 = V_2;
		if (!L_19)
		{
			goto IL_00a0;
		}
	}
	{
		goto IL_00a5;
	}

IL_00a0:
	{
		goto IL_0144;
	}

IL_00a5:
	{
		DataTable_t2176726999 * L_20 = ___Table0;
		NullCheck(L_20);
		DataColumnCollection_t3528392753 * L_21 = DataTable_get_Columns_m220042291(L_20, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_22 = ___reader2;
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_22);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_24 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		DataColumn_t3354469747 * L_25 = DataColumnCollection_get_Item_m305848743(L_21, L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		DataColumn_t3354469747 * L_26 = V_3;
		if (!L_26)
		{
			goto IL_00ce;
		}
	}
	{
		DataColumn_t3354469747 * L_27 = V_3;
		NullCheck(L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_27);
		if ((((int32_t)L_28) == ((int32_t)2)))
		{
			goto IL_00d3;
		}
	}

IL_00ce:
	{
		goto IL_0144;
	}

IL_00d3:
	{
		DataColumn_t3354469747 * L_29 = V_3;
		NullCheck(L_29);
		String_t* L_30 = DataColumn_get_Namespace_m546686590(L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_00f3;
		}
	}
	{
		XmlReader_t4229084514 * L_31 = ___reader2;
		NullCheck(L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_31);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_34 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_0109;
		}
	}

IL_00f3:
	{
		DataColumn_t3354469747 * L_35 = V_3;
		NullCheck(L_35);
		String_t* L_36 = DataColumn_get_Namespace_m546686590(L_35, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_37 = ___reader2;
		NullCheck(L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_37);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_39 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_36, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0144;
		}
	}

IL_0109:
	{
		DataColumn_t3354469747 * L_40 = V_3;
		NullCheck(L_40);
		Type_t * L_41 = DataColumn_get_DataType_m3376662490(L_40, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_42 = ___reader2;
		NullCheck(L_42);
		String_t* L_43 = VirtFuncInvoker0< String_t* >::Invoke(26 /* System.String System.Xml.XmlReader::get_Value() */, L_42);
		Il2CppObject * L_44 = XmlDataLoader_StringToObject_m1292975836(NULL /*static, unused*/, L_41, L_43, /*hidden argument*/NULL);
		V_4 = L_44;
		int32_t L_45 = ___loadType3;
		if ((!(((uint32_t)L_45) == ((uint32_t)((int32_t)512)))))
		{
			goto IL_0136;
		}
	}
	{
		DataRow_t3654701923 * L_46 = ___Row1;
		DataColumn_t3354469747 * L_47 = V_3;
		Il2CppObject * L_48 = V_4;
		NullCheck(L_46);
		DataRow_set_Item_m3968127800(L_46, L_47, L_48, /*hidden argument*/NULL);
		goto IL_0144;
	}

IL_0136:
	{
		DataRow_t3654701923 * L_49 = ___Row1;
		DataColumn_t3354469747 * L_50 = V_3;
		NullCheck(L_50);
		String_t* L_51 = DataColumn_get_ColumnName_m409531680(L_50, /*hidden argument*/NULL);
		Il2CppObject * L_52 = V_4;
		NullCheck(L_49);
		DataRow_SetOriginalValue_m2873008403(L_49, L_51, L_52, /*hidden argument*/NULL);
	}

IL_0144:
	{
		XmlReader_t4229084514 * L_53 = ___reader2;
		NullCheck(L_53);
		bool L_54 = VirtFuncInvoker0< bool >::Invoke(41 /* System.Boolean System.Xml.XmlReader::MoveToNextAttribute() */, L_53);
		if (L_54)
		{
			goto IL_0017;
		}
	}
	{
		XmlReader_t4229084514 * L_55 = ___reader2;
		NullCheck(L_55);
		VirtFuncInvoker0< bool >::Invoke(39 /* System.Boolean System.Xml.XmlReader::MoveToElement() */, L_55);
		return;
	}
}
// System.Void System.Data.XmlDiffLoader::LoadColumnChildren(System.Data.DataTable,System.Data.DataRow,System.Xml.XmlReader,System.Data.DataRowVersion)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern const uint32_t XmlDiffLoader_LoadColumnChildren_m2569100210_MetadataUsageId;
extern "C"  void XmlDiffLoader_LoadColumnChildren_m2569100210 (XmlDiffLoader_t3888299394 * __this, DataTable_t2176726999 * ___Table0, DataRow_t3654701923 * ___Row1, XmlReader_t4229084514 * ___reader2, int32_t ___loadType3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDiffLoader_LoadColumnChildren_m2569100210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	DataTable_t2176726999 * V_2 = NULL;
	{
		XmlReader_t4229084514 * L_0 = ___reader2;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XmlReader::get_IsEmptyElement() */, L_0);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		XmlReader_t4229084514 * L_2 = ___reader2;
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_2);
		return;
	}

IL_0012:
	{
		XmlReader_t4229084514 * L_3 = ___reader2;
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(48 /* System.Void System.Xml.XmlReader::ReadStartElement() */, L_3);
		XmlReader_t4229084514 * L_4 = ___reader2;
		NullCheck(L_4);
		VirtFuncInvoker0< int32_t >::Invoke(38 /* System.Xml.XmlNodeType System.Xml.XmlReader::MoveToContent() */, L_4);
		goto IL_00ee;
	}

IL_0024:
	{
		XmlReader_t4229084514 * L_5 = ___reader2;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_5);
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_003c;
		}
	}
	{
		XmlReader_t4229084514 * L_7 = ___reader2;
		NullCheck(L_7);
		VirtFuncInvoker0< bool >::Invoke(42 /* System.Boolean System.Xml.XmlReader::Read() */, L_7);
		goto IL_00ee;
	}

IL_003c:
	{
		XmlReader_t4229084514 * L_8 = ___reader2;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_10 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		DataTable_t2176726999 * L_11 = ___Table0;
		NullCheck(L_11);
		DataColumnCollection_t3528392753 * L_12 = DataTable_get_Columns_m220042291(L_11, /*hidden argument*/NULL);
		String_t* L_13 = V_0;
		NullCheck(L_12);
		bool L_14 = DataColumnCollection_Contains_m1368116008(L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00a3;
		}
	}
	{
		DataTable_t2176726999 * L_15 = ___Table0;
		NullCheck(L_15);
		DataColumnCollection_t3528392753 * L_16 = DataTable_get_Columns_m220042291(L_15, /*hidden argument*/NULL);
		String_t* L_17 = V_0;
		NullCheck(L_16);
		DataColumn_t3354469747 * L_18 = DataColumnCollection_get_Item_m305848743(L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Type_t * L_19 = DataColumn_get_DataType_m3376662490(L_18, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_20 = ___reader2;
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(50 /* System.String System.Xml.XmlReader::ReadString() */, L_20);
		Il2CppObject * L_22 = XmlDataLoader_StringToObject_m1292975836(NULL /*static, unused*/, L_19, L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		int32_t L_23 = ___loadType3;
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)512)))))
		{
			goto IL_008f;
		}
	}
	{
		DataRow_t3654701923 * L_24 = ___Row1;
		String_t* L_25 = V_0;
		Il2CppObject * L_26 = V_1;
		NullCheck(L_24);
		DataRow_set_Item_m1424269409(L_24, L_25, L_26, /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_008f:
	{
		DataRow_t3654701923 * L_27 = ___Row1;
		String_t* L_28 = V_0;
		Il2CppObject * L_29 = V_1;
		NullCheck(L_27);
		DataRow_SetOriginalValue_m2873008403(L_27, L_28, L_29, /*hidden argument*/NULL);
	}

IL_0097:
	{
		XmlReader_t4229084514 * L_30 = ___reader2;
		NullCheck(L_30);
		VirtFuncInvoker0< bool >::Invoke(42 /* System.Boolean System.Xml.XmlReader::Read() */, L_30);
		goto IL_00ee;
	}

IL_00a3:
	{
		XmlReader_t4229084514 * L_31 = ___reader2;
		NullCheck(L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_31);
		DataTable_t2176726999 * L_33 = XmlDiffLoader_GetTable_m2626451904(__this, L_32, /*hidden argument*/NULL);
		V_2 = L_33;
		DataTable_t2176726999 * L_34 = V_2;
		if (!L_34)
		{
			goto IL_00e8;
		}
	}
	{
		int32_t L_35 = ___loadType3;
		if ((!(((uint32_t)L_35) == ((uint32_t)((int32_t)256)))))
		{
			goto IL_00cf;
		}
	}
	{
		DataTable_t2176726999 * L_36 = V_2;
		XmlReader_t4229084514 * L_37 = ___reader2;
		XmlDiffLoader_LoadBeforeTable_m4046043676(__this, L_36, L_37, /*hidden argument*/NULL);
		goto IL_00e3;
	}

IL_00cf:
	{
		int32_t L_38 = ___loadType3;
		if ((!(((uint32_t)L_38) == ((uint32_t)((int32_t)512)))))
		{
			goto IL_00e3;
		}
	}
	{
		DataTable_t2176726999 * L_39 = V_2;
		XmlReader_t4229084514 * L_40 = ___reader2;
		XmlDiffLoader_LoadCurrentTable_m1506874120(__this, L_39, L_40, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		goto IL_00ee;
	}

IL_00e8:
	{
		XmlReader_t4229084514 * L_41 = ___reader2;
		NullCheck(L_41);
		VirtActionInvoker0::Invoke(52 /* System.Void System.Xml.XmlReader::Skip() */, L_41);
	}

IL_00ee:
	{
		XmlReader_t4229084514 * L_42 = ___reader2;
		NullCheck(L_42);
		int32_t L_43 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_42);
		if ((!(((uint32_t)L_43) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0024;
		}
	}
	{
		XmlReader_t4229084514 * L_44 = ___reader2;
		NullCheck(L_44);
		VirtActionInvoker0::Invoke(45 /* System.Void System.Xml.XmlReader::ReadEndElement() */, L_44);
		return;
	}
}
// System.Void System.Data.XmlDiffLoader::LoadBeforeTable(System.Data.DataTable,System.Xml.XmlReader)
extern Il2CppClass* DataRow_t3654701923_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3355;
extern Il2CppCodeGenString* _stringLiteral1269506330;
extern Il2CppCodeGenString* _stringLiteral28898708;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern const uint32_t XmlDiffLoader_LoadBeforeTable_m4046043676_MetadataUsageId;
extern "C"  void XmlDiffLoader_LoadBeforeTable_m4046043676 (XmlDiffLoader_t3888299394 * __this, DataTable_t2176726999 * ___Table0, XmlReader_t4229084514 * ___reader1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDiffLoader_LoadBeforeTable_m4046043676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	DataRow_t3654701923 * V_2 = NULL;
	{
		XmlReader_t4229084514 * L_0 = ___reader1;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(33 /* System.String System.Xml.XmlReader::GetAttribute(System.String,System.String) */, L_0, _stringLiteral3355, _stringLiteral1269506330);
		V_0 = L_1;
		XmlReader_t4229084514 * L_2 = ___reader1;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(33 /* System.String System.Xml.XmlReader::GetAttribute(System.String,System.String) */, L_2, _stringLiteral28898708, _stringLiteral3051173506);
		V_1 = L_3;
		Hashtable_t3875263730 * L_4 = __this->get_DiffGrRows_2();
		String_t* L_5 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_4, L_5);
		V_2 = ((DataRow_t3654701923 *)CastclassClass(L_6, DataRow_t3654701923_il2cpp_TypeInfo_var));
		DataRow_t3654701923 * L_7 = V_2;
		if (L_7)
		{
			goto IL_0072;
		}
	}
	{
		DataTable_t2176726999 * L_8 = ___Table0;
		NullCheck(L_8);
		DataRow_t3654701923 * L_9 = DataTable_NewRow_m3256434597(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		DataTable_t2176726999 * L_10 = ___Table0;
		DataRow_t3654701923 * L_11 = V_2;
		XmlReader_t4229084514 * L_12 = ___reader1;
		XmlDiffLoader_LoadColumns_m455932784(__this, L_10, L_11, L_12, ((int32_t)512), /*hidden argument*/NULL);
		DataTable_t2176726999 * L_13 = ___Table0;
		NullCheck(L_13);
		DataRowCollection_t1405583905 * L_14 = DataTable_get_Rows_m954608043(L_13, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_15 = V_2;
		String_t* L_16 = V_1;
		int32_t L_17 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		DataRowCollection_InsertAt_m3491267193(L_14, L_15, L_17, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_18 = V_2;
		NullCheck(L_18);
		DataRow_AcceptChanges_m818702046(L_18, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_19 = V_2;
		NullCheck(L_19);
		DataRow_Delete_m1530649130(L_19, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_0072:
	{
		DataTable_t2176726999 * L_20 = ___Table0;
		DataRow_t3654701923 * L_21 = V_2;
		XmlReader_t4229084514 * L_22 = ___reader1;
		XmlDiffLoader_LoadColumns_m455932784(__this, L_20, L_21, L_22, ((int32_t)256), /*hidden argument*/NULL);
	}

IL_0080:
	{
		return;
	}
}
// System.Void System.Data.XmlDiffLoader::LoadCurrentTable(System.Data.DataTable,System.Xml.XmlReader)
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3355;
extern Il2CppCodeGenString* _stringLiteral1269506330;
extern Il2CppCodeGenString* _stringLiteral3620082341;
extern Il2CppCodeGenString* _stringLiteral2771330761;
extern Il2CppCodeGenString* _stringLiteral3679453897;
extern Il2CppCodeGenString* _stringLiteral541787416;
extern Il2CppCodeGenString* _stringLiteral1371017168;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern const uint32_t XmlDiffLoader_LoadCurrentTable_m1506874120_MetadataUsageId;
extern "C"  void XmlDiffLoader_LoadCurrentTable_m1506874120 (XmlDiffLoader_t3888299394 * __this, DataTable_t2176726999 * ___Table0, XmlReader_t4229084514 * ___reader1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDiffLoader_LoadCurrentTable_m1506874120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	DataRow_t3654701923 * V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	{
		DataTable_t2176726999 * L_0 = ___Table0;
		NullCheck(L_0);
		DataRow_t3654701923 * L_1 = DataTable_NewRow_m3256434597(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		XmlReader_t4229084514 * L_2 = ___reader1;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(33 /* System.String System.Xml.XmlReader::GetAttribute(System.String,System.String) */, L_2, _stringLiteral3355, _stringLiteral1269506330);
		V_2 = L_3;
		XmlReader_t4229084514 * L_4 = ___reader1;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(32 /* System.String System.Xml.XmlReader::GetAttribute(System.String) */, L_4, _stringLiteral3620082341);
		V_3 = L_5;
		XmlReader_t4229084514 * L_6 = ___reader1;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(33 /* System.String System.Xml.XmlReader::GetAttribute(System.String,System.String) */, L_6, _stringLiteral2771330761, _stringLiteral1269506330);
		V_4 = L_7;
		String_t* L_8 = V_4;
		if (!L_8)
		{
			goto IL_0097;
		}
	}
	{
		String_t* L_9 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_10 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_11 = String_Compare_m279494420(NULL /*static, unused*/, L_9, _stringLiteral3679453897, (bool)1, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0069;
		}
	}
	{
		Hashtable_t3875263730 * L_12 = __this->get_DiffGrRows_2();
		String_t* L_13 = V_2;
		DataRow_t3654701923 * L_14 = V_1;
		NullCheck(L_12);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_12, L_13, L_14);
		V_0 = ((int32_t)16);
		goto IL_0092;
	}

IL_0069:
	{
		String_t* L_15 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_16 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_17 = String_Compare_m279494420(NULL /*static, unused*/, L_15, _stringLiteral541787416, (bool)1, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0087;
		}
	}
	{
		V_0 = 4;
		goto IL_0092;
	}

IL_0087:
	{
		InvalidOperationException_t2420574324 * L_18 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_18, _stringLiteral1371017168, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}

IL_0092:
	{
		goto IL_0099;
	}

IL_0097:
	{
		V_0 = 2;
	}

IL_0099:
	{
		String_t* L_19 = V_3;
		if (!L_19)
		{
			goto IL_00c2;
		}
	}
	{
		String_t* L_20 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_21 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_22 = String_Compare_m279494420(NULL /*static, unused*/, L_20, _stringLiteral3569038, (bool)1, L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00c2;
		}
	}
	{
		Hashtable_t3875263730 * L_23 = __this->get_ErrorRows_3();
		String_t* L_24 = V_2;
		DataRow_t3654701923 * L_25 = V_1;
		NullCheck(L_23);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_23, L_24, L_25);
	}

IL_00c2:
	{
		DataTable_t2176726999 * L_26 = ___Table0;
		DataRow_t3654701923 * L_27 = V_1;
		XmlReader_t4229084514 * L_28 = ___reader1;
		XmlDiffLoader_LoadColumns_m455932784(__this, L_26, L_27, L_28, ((int32_t)512), /*hidden argument*/NULL);
		DataTable_t2176726999 * L_29 = ___Table0;
		NullCheck(L_29);
		DataRowCollection_t1405583905 * L_30 = DataTable_get_Rows_m954608043(L_29, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_31 = V_1;
		NullCheck(L_30);
		DataRowCollection_Add_m2125891809(L_30, L_31, /*hidden argument*/NULL);
		int32_t L_32 = V_0;
		if ((((int32_t)L_32) == ((int32_t)4)))
		{
			goto IL_00e9;
		}
	}
	{
		DataRow_t3654701923 * L_33 = V_1;
		NullCheck(L_33);
		DataRow_AcceptChanges_m818702046(L_33, /*hidden argument*/NULL);
	}

IL_00e9:
	{
		return;
	}
}
// System.Data.DataTable System.Data.XmlDiffLoader::GetTable(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlDiffLoader_GetTable_m2626451904_MetadataUsageId;
extern "C"  DataTable_t2176726999 * XmlDiffLoader_GetTable_m2626451904 (XmlDiffLoader_t3888299394 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDiffLoader_GetTable_m2626451904_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DataSet_t3654702571 * L_0 = __this->get_DSet_0();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		DataSet_t3654702571 * L_1 = __this->get_DSet_0();
		NullCheck(L_1);
		DataTableCollection_t2915263893 * L_2 = DataSet_get_Tables_m87321279(L_1, /*hidden argument*/NULL);
		String_t* L_3 = ___name0;
		NullCheck(L_2);
		DataTable_t2176726999 * L_4 = DataTableCollection_get_Item_m2714089417(L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001d:
	{
		String_t* L_5 = ___name0;
		DataTable_t2176726999 * L_6 = __this->get_table_1();
		NullCheck(L_6);
		String_t* L_7 = DataTable_get_TableName_m3141812994(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003a;
		}
	}
	{
		DataTable_t2176726999 * L_9 = __this->get_table_1();
		return L_9;
	}

IL_003a:
	{
		return (DataTable_t2176726999 *)NULL;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::.ctor(System.Data.DataSet,System.Xml.XmlReader,System.Boolean)
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4031378104;
extern Il2CppCodeGenString* _stringLiteral3386979745;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern const uint32_t XmlSchemaDataImporter__ctor_m1313483257_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter__ctor_m1313483257 (XmlSchemaDataImporter_t2811591463 * __this, DataSet_t3654702571 * ___dataset0, XmlReader_t4229084514 * ___reader1, bool ___forDataSet2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter__ctor_m1313483257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		__this->set_relations_6(L_0);
		Hashtable_t3875263730 * L_1 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_1, /*hidden argument*/NULL);
		__this->set_reservedConstraints_7(L_1);
		ArrayList_t2121638921 * L_2 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_2, /*hidden argument*/NULL);
		__this->set_topLevelElements_9(L_2);
		ArrayList_t2121638921 * L_3 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_3, /*hidden argument*/NULL);
		__this->set_targetElements_10(L_3);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		DataSet_t3654702571 * L_4 = ___dataset0;
		__this->set_dataset_3(L_4);
		bool L_5 = ___forDataSet2;
		__this->set_forDataSet_4(L_5);
		DataSet_t3654702571 * L_6 = ___dataset0;
		NullCheck(L_6);
		DataSet_set_DataSetName_m1679911553(L_6, _stringLiteral4031378104, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_7 = ___reader1;
		XmlSchema_t1932230565 * L_8 = XmlSchema_Read_m807478306(NULL /*static, unused*/, L_7, (ValidationEventHandler_t2777264566 *)NULL, /*hidden argument*/NULL);
		__this->set_schema_5(L_8);
		XmlReader_t4229084514 * L_9 = ___reader1;
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_9);
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0095;
		}
	}
	{
		XmlReader_t4229084514 * L_11 = ___reader1;
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_12, _stringLiteral3386979745, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0095;
		}
	}
	{
		XmlReader_t4229084514 * L_14 = ___reader1;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_15, _stringLiteral1440052060, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0095;
		}
	}
	{
		XmlReader_t4229084514 * L_17 = ___reader1;
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(45 /* System.Void System.Xml.XmlReader::ReadEndElement() */, L_17);
	}

IL_0095:
	{
		XmlSchema_t1932230565 * L_18 = __this->get_schema_5();
		NullCheck(L_18);
		XmlSchema_Compile_m2629685169(L_18, (ValidationEventHandler_t2777264566 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Data.TableAdapterSchemaInfo System.Data.XmlSchemaDataImporter::get_CurrentAdapter()
extern "C"  TableAdapterSchemaInfo_t1131857475 * XmlSchemaDataImporter_get_CurrentAdapter_m1240851460 (XmlSchemaDataImporter_t2811591463 * __this, const MethodInfo* method)
{
	{
		TableAdapterSchemaInfo_t1131857475 * L_0 = __this->get_currentAdapter_12();
		return L_0;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::Process()
extern Il2CppClass* XmlSchemaElement_t471922321_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaKeyref_t2789194649_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaIdentityConstraint_t3473808128_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaAnnotation_t1377046772_il2cpp_TypeInfo_var;
extern Il2CppClass* ConstraintStructure_t742574505_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* RelationStructure_t3039531114_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_Process_m3436620654_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_Process_m3436620654 (XmlSchemaDataImporter_t2811591463 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_Process_m3436620654_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlSchemaObjectEnumerator_t3058853928 * V_0 = NULL;
	XmlSchemaObject_t2900481284 * V_1 = NULL;
	XmlSchemaElement_t471922321 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	XmlSchemaObjectEnumerator_t3058853928 * V_4 = NULL;
	XmlSchemaObject_t2900481284 * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	XmlSchemaObjectEnumerator_t3058853928 * V_7 = NULL;
	XmlSchemaObject_t2900481284 * V_8 = NULL;
	Il2CppObject * V_9 = NULL;
	XmlSchemaObjectEnumerator_t3058853928 * V_10 = NULL;
	XmlSchemaObject_t2900481284 * V_11 = NULL;
	XmlSchemaElement_t471922321 * V_12 = NULL;
	Il2CppObject * V_13 = NULL;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	XmlSchemaObjectEnumerator_t3058853928 * V_17 = NULL;
	XmlSchemaObject_t2900481284 * V_18 = NULL;
	Il2CppObject * V_19 = NULL;
	XmlSchemaObjectEnumerator_t3058853928 * V_20 = NULL;
	XmlSchemaObject_t2900481284 * V_21 = NULL;
	Il2CppObject * V_22 = NULL;
	XmlSchemaObjectEnumerator_t3058853928 * V_23 = NULL;
	XmlSchemaObject_t2900481284 * V_24 = NULL;
	Il2CppObject * V_25 = NULL;
	Il2CppObject * V_26 = NULL;
	RelationStructure_t3039531114 * V_27 = NULL;
	Il2CppObject * V_28 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlSchema_t1932230565 * L_0 = __this->get_schema_5();
		NullCheck(L_0);
		String_t* L_1 = XmlSchema_get_Id_m792700229(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		DataSet_t3654702571 * L_2 = __this->get_dataset_3();
		XmlSchema_t1932230565 * L_3 = __this->get_schema_5();
		NullCheck(L_3);
		String_t* L_4 = XmlSchema_get_Id_m792700229(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		DataSet_set_DataSetName_m1679911553(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		DataSet_t3654702571 * L_5 = __this->get_dataset_3();
		XmlSchema_t1932230565 * L_6 = __this->get_schema_5();
		NullCheck(L_6);
		String_t* L_7 = XmlSchema_get_TargetNamespace_m1263789570(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		DataSet_set_Namespace_m1090260457(L_5, L_7, /*hidden argument*/NULL);
		XmlSchema_t1932230565 * L_8 = __this->get_schema_5();
		NullCheck(L_8);
		XmlSchemaObjectCollection_t2238201602 * L_9 = XmlSchema_get_Items_m3467235754(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		XmlSchemaObjectEnumerator_t3058853928 * L_10 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_9, /*hidden argument*/NULL);
		V_0 = L_10;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00b1;
		}

IL_0052:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_11 = V_0;
			NullCheck(L_11);
			XmlSchemaObject_t2900481284 * L_12 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_11, /*hidden argument*/NULL);
			V_1 = L_12;
			XmlSchemaObject_t2900481284 * L_13 = V_1;
			V_2 = ((XmlSchemaElement_t471922321 *)IsInstClass(L_13, XmlSchemaElement_t471922321_il2cpp_TypeInfo_var));
			XmlSchemaElement_t471922321 * L_14 = V_2;
			if (!L_14)
			{
				goto IL_00b1;
			}
		}

IL_0066:
		{
			XmlSchemaElement_t471922321 * L_15 = __this->get_datasetElement_8();
			if (L_15)
			{
				goto IL_0084;
			}
		}

IL_0071:
		{
			XmlSchemaElement_t471922321 * L_16 = V_2;
			bool L_17 = XmlSchemaDataImporter_IsDataSetElement_m2426452300(__this, L_16, /*hidden argument*/NULL);
			if (!L_17)
			{
				goto IL_0084;
			}
		}

IL_007d:
		{
			XmlSchemaElement_t471922321 * L_18 = V_2;
			__this->set_datasetElement_8(L_18);
		}

IL_0084:
		{
			XmlSchemaElement_t471922321 * L_19 = V_2;
			NullCheck(L_19);
			XmlSchemaType_t3432810239 * L_20 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_19, /*hidden argument*/NULL);
			if (!((XmlSchemaComplexType_t1860629407 *)IsInstClass(L_20, XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var)))
			{
				goto IL_00b1;
			}
		}

IL_0094:
		{
			XmlSchemaElement_t471922321 * L_21 = V_2;
			NullCheck(L_21);
			XmlSchemaType_t3432810239 * L_22 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_21, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
			XmlSchemaComplexType_t1860629407 * L_23 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_schemaAnyType_2();
			if ((((Il2CppObject*)(XmlSchemaType_t3432810239 *)L_22) == ((Il2CppObject*)(XmlSchemaComplexType_t1860629407 *)L_23)))
			{
				goto IL_00b1;
			}
		}

IL_00a4:
		{
			ArrayList_t2121638921 * L_24 = __this->get_targetElements_10();
			XmlSchemaObject_t2900481284 * L_25 = V_1;
			NullCheck(L_24);
			VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_24, L_25);
		}

IL_00b1:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_26 = V_0;
			NullCheck(L_26);
			bool L_27 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_26);
			if (L_27)
			{
				goto IL_0052;
			}
		}

IL_00bc:
		{
			IL2CPP_LEAVE(0xD5, FINALLY_00c1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00c1;
	}

FINALLY_00c1:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_28 = V_0;
			Il2CppObject * L_29 = ((Il2CppObject *)IsInst(L_28, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_3 = L_29;
			if (!L_29)
			{
				goto IL_00d4;
			}
		}

IL_00ce:
		{
			Il2CppObject * L_30 = V_3;
			NullCheck(L_30);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_30);
		}

IL_00d4:
		{
			IL2CPP_END_FINALLY(193)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(193)
	{
		IL2CPP_JUMP_TBL(0xD5, IL_00d5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00d5:
	{
		XmlSchemaElement_t471922321 * L_31 = __this->get_datasetElement_8();
		if (!L_31)
		{
			goto IL_01a8;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_32 = __this->get_datasetElement_8();
		NullCheck(L_32);
		XmlSchemaObjectCollection_t2238201602 * L_33 = XmlSchemaElement_get_Constraints_m1205744096(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		XmlSchemaObjectEnumerator_t3058853928 * L_34 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_33, /*hidden argument*/NULL);
		V_4 = L_34;
	}

IL_00f2:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0119;
		}

IL_00f7:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_35 = V_4;
			NullCheck(L_35);
			XmlSchemaObject_t2900481284 * L_36 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_35, /*hidden argument*/NULL);
			V_5 = L_36;
			XmlSchemaObject_t2900481284 * L_37 = V_5;
			if (((XmlSchemaKeyref_t2789194649 *)IsInstClass(L_37, XmlSchemaKeyref_t2789194649_il2cpp_TypeInfo_var)))
			{
				goto IL_0119;
			}
		}

IL_010c:
		{
			XmlSchemaObject_t2900481284 * L_38 = V_5;
			XmlSchemaDataImporter_ReserveSelfIdentity_m4246710545(__this, ((XmlSchemaIdentityConstraint_t3473808128 *)CastclassClass(L_38, XmlSchemaIdentityConstraint_t3473808128_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		}

IL_0119:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_39 = V_4;
			NullCheck(L_39);
			bool L_40 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_39);
			if (L_40)
			{
				goto IL_00f7;
			}
		}

IL_0125:
		{
			IL2CPP_LEAVE(0x141, FINALLY_012a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_012a;
	}

FINALLY_012a:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_41 = V_4;
			Il2CppObject * L_42 = ((Il2CppObject *)IsInst(L_41, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_6 = L_42;
			if (!L_42)
			{
				goto IL_0140;
			}
		}

IL_0139:
		{
			Il2CppObject * L_43 = V_6;
			NullCheck(L_43);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_43);
		}

IL_0140:
		{
			IL2CPP_END_FINALLY(298)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(298)
	{
		IL2CPP_JUMP_TBL(0x141, IL_0141)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0141:
	{
		XmlSchemaElement_t471922321 * L_44 = __this->get_datasetElement_8();
		NullCheck(L_44);
		XmlSchemaObjectCollection_t2238201602 * L_45 = XmlSchemaElement_get_Constraints_m1205744096(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		XmlSchemaObjectEnumerator_t3058853928 * L_46 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_45, /*hidden argument*/NULL);
		V_7 = L_46;
	}

IL_0153:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0180;
		}

IL_0158:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_47 = V_7;
			NullCheck(L_47);
			XmlSchemaObject_t2900481284 * L_48 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_47, /*hidden argument*/NULL);
			V_8 = L_48;
			XmlSchemaObject_t2900481284 * L_49 = V_8;
			if (!((XmlSchemaKeyref_t2789194649 *)IsInstClass(L_49, XmlSchemaKeyref_t2789194649_il2cpp_TypeInfo_var)))
			{
				goto IL_0180;
			}
		}

IL_016d:
		{
			XmlSchemaElement_t471922321 * L_50 = __this->get_datasetElement_8();
			XmlSchemaObject_t2900481284 * L_51 = V_8;
			XmlSchemaDataImporter_ReserveRelationIdentity_m2792997361(__this, L_50, ((XmlSchemaKeyref_t2789194649 *)CastclassClass(L_51, XmlSchemaKeyref_t2789194649_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		}

IL_0180:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_52 = V_7;
			NullCheck(L_52);
			bool L_53 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_52);
			if (L_53)
			{
				goto IL_0158;
			}
		}

IL_018c:
		{
			IL2CPP_LEAVE(0x1A8, FINALLY_0191);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0191;
	}

FINALLY_0191:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_54 = V_7;
			Il2CppObject * L_55 = ((Il2CppObject *)IsInst(L_54, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_9 = L_55;
			if (!L_55)
			{
				goto IL_01a7;
			}
		}

IL_01a0:
		{
			Il2CppObject * L_56 = V_9;
			NullCheck(L_56);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_56);
		}

IL_01a7:
		{
			IL2CPP_END_FINALLY(401)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(401)
	{
		IL2CPP_JUMP_TBL(0x1A8, IL_01a8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01a8:
	{
		XmlSchema_t1932230565 * L_57 = __this->get_schema_5();
		NullCheck(L_57);
		XmlSchemaObjectCollection_t2238201602 * L_58 = XmlSchema_get_Items_m3467235754(L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		XmlSchemaObjectEnumerator_t3058853928 * L_59 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_58, /*hidden argument*/NULL);
		V_10 = L_59;
	}

IL_01ba:
	try
	{ // begin try (depth: 1)
		{
			goto IL_020d;
		}

IL_01bf:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_60 = V_10;
			NullCheck(L_60);
			XmlSchemaObject_t2900481284 * L_61 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_60, /*hidden argument*/NULL);
			V_11 = L_61;
			XmlSchemaObject_t2900481284 * L_62 = V_11;
			if (!((XmlSchemaElement_t471922321 *)IsInstClass(L_62, XmlSchemaElement_t471922321_il2cpp_TypeInfo_var)))
			{
				goto IL_020d;
			}
		}

IL_01d4:
		{
			XmlSchemaObject_t2900481284 * L_63 = V_11;
			V_12 = ((XmlSchemaElement_t471922321 *)IsInstClass(L_63, XmlSchemaElement_t471922321_il2cpp_TypeInfo_var));
			XmlSchemaElement_t471922321 * L_64 = V_12;
			NullCheck(L_64);
			XmlSchemaType_t3432810239 * L_65 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_64, /*hidden argument*/NULL);
			if (!((XmlSchemaComplexType_t1860629407 *)IsInstClass(L_65, XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var)))
			{
				goto IL_020d;
			}
		}

IL_01ee:
		{
			XmlSchemaElement_t471922321 * L_66 = V_12;
			NullCheck(L_66);
			XmlSchemaType_t3432810239 * L_67 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_66, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
			XmlSchemaComplexType_t1860629407 * L_68 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_schemaAnyType_2();
			if ((((Il2CppObject*)(XmlSchemaType_t3432810239 *)L_67) == ((Il2CppObject*)(XmlSchemaComplexType_t1860629407 *)L_68)))
			{
				goto IL_020d;
			}
		}

IL_01ff:
		{
			ArrayList_t2121638921 * L_69 = __this->get_targetElements_10();
			XmlSchemaObject_t2900481284 * L_70 = V_11;
			NullCheck(L_69);
			VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_69, L_70);
		}

IL_020d:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_71 = V_10;
			NullCheck(L_71);
			bool L_72 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_71);
			if (L_72)
			{
				goto IL_01bf;
			}
		}

IL_0219:
		{
			IL2CPP_LEAVE(0x235, FINALLY_021e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_021e;
	}

FINALLY_021e:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_73 = V_10;
			Il2CppObject * L_74 = ((Il2CppObject *)IsInst(L_73, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_13 = L_74;
			if (!L_74)
			{
				goto IL_0234;
			}
		}

IL_022d:
		{
			Il2CppObject * L_75 = V_13;
			NullCheck(L_75);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_75);
		}

IL_0234:
		{
			IL2CPP_END_FINALLY(542)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(542)
	{
		IL2CPP_JUMP_TBL(0x235, IL_0235)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0235:
	{
		ArrayList_t2121638921 * L_76 = __this->get_targetElements_10();
		NullCheck(L_76);
		int32_t L_77 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_76);
		V_14 = L_77;
		V_15 = 0;
		goto IL_0268;
	}

IL_024a:
	{
		ArrayList_t2121638921 * L_78 = __this->get_targetElements_10();
		int32_t L_79 = V_15;
		NullCheck(L_78);
		Il2CppObject * L_80 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_78, L_79);
		XmlSchemaDataImporter_ProcessGlobalElement_m4175921586(__this, ((XmlSchemaElement_t471922321 *)CastclassClass(L_80, XmlSchemaElement_t471922321_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		int32_t L_81 = V_15;
		V_15 = ((int32_t)((int32_t)L_81+(int32_t)1));
	}

IL_0268:
	{
		int32_t L_82 = V_15;
		int32_t L_83 = V_14;
		if ((((int32_t)L_82) < ((int32_t)L_83)))
		{
			goto IL_024a;
		}
	}
	{
		int32_t L_84 = V_14;
		V_16 = L_84;
		goto IL_0298;
	}

IL_027a:
	{
		ArrayList_t2121638921 * L_85 = __this->get_targetElements_10();
		int32_t L_86 = V_16;
		NullCheck(L_85);
		Il2CppObject * L_87 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_85, L_86);
		XmlSchemaDataImporter_ProcessDataTableElement_m1241068523(__this, ((XmlSchemaElement_t471922321 *)CastclassClass(L_87, XmlSchemaElement_t471922321_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		int32_t L_88 = V_16;
		V_16 = ((int32_t)((int32_t)L_88+(int32_t)1));
	}

IL_0298:
	{
		int32_t L_89 = V_16;
		ArrayList_t2121638921 * L_90 = __this->get_targetElements_10();
		NullCheck(L_90);
		int32_t L_91 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_90);
		if ((((int32_t)L_89) < ((int32_t)L_91)))
		{
			goto IL_027a;
		}
	}
	{
		XmlSchema_t1932230565 * L_92 = __this->get_schema_5();
		NullCheck(L_92);
		XmlSchemaObjectCollection_t2238201602 * L_93 = XmlSchema_get_Items_m3467235754(L_92, /*hidden argument*/NULL);
		NullCheck(L_93);
		XmlSchemaObjectEnumerator_t3058853928 * L_94 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_93, /*hidden argument*/NULL);
		V_17 = L_94;
	}

IL_02bc:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02e4;
		}

IL_02c1:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_95 = V_17;
			NullCheck(L_95);
			XmlSchemaObject_t2900481284 * L_96 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_95, /*hidden argument*/NULL);
			V_18 = L_96;
			XmlSchemaObject_t2900481284 * L_97 = V_18;
			if (!((XmlSchemaAnnotation_t1377046772 *)IsInstClass(L_97, XmlSchemaAnnotation_t1377046772_il2cpp_TypeInfo_var)))
			{
				goto IL_02e4;
			}
		}

IL_02d6:
		{
			XmlSchemaObject_t2900481284 * L_98 = V_18;
			XmlSchemaDataImporter_HandleAnnotations_m996806934(__this, ((XmlSchemaAnnotation_t1377046772 *)CastclassClass(L_98, XmlSchemaAnnotation_t1377046772_il2cpp_TypeInfo_var)), (bool)0, /*hidden argument*/NULL);
		}

IL_02e4:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_99 = V_17;
			NullCheck(L_99);
			bool L_100 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_99);
			if (L_100)
			{
				goto IL_02c1;
			}
		}

IL_02f0:
		{
			IL2CPP_LEAVE(0x30C, FINALLY_02f5);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_02f5;
	}

FINALLY_02f5:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_101 = V_17;
			Il2CppObject * L_102 = ((Il2CppObject *)IsInst(L_101, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_19 = L_102;
			if (!L_102)
			{
				goto IL_030b;
			}
		}

IL_0304:
		{
			Il2CppObject * L_103 = V_19;
			NullCheck(L_103);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_103);
		}

IL_030b:
		{
			IL2CPP_END_FINALLY(757)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(757)
	{
		IL2CPP_JUMP_TBL(0x30C, IL_030c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_030c:
	{
		XmlSchemaElement_t471922321 * L_104 = __this->get_datasetElement_8();
		if (!L_104)
		{
			goto IL_03f5;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_105 = __this->get_datasetElement_8();
		NullCheck(L_105);
		XmlSchemaObjectCollection_t2238201602 * L_106 = XmlSchemaElement_get_Constraints_m1205744096(L_105, /*hidden argument*/NULL);
		NullCheck(L_106);
		XmlSchemaObjectEnumerator_t3058853928 * L_107 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_106, /*hidden argument*/NULL);
		V_20 = L_107;
	}

IL_0329:
	try
	{ // begin try (depth: 1)
		{
			goto IL_035b;
		}

IL_032e:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_108 = V_20;
			NullCheck(L_108);
			XmlSchemaObject_t2900481284 * L_109 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_108, /*hidden argument*/NULL);
			V_21 = L_109;
			XmlSchemaObject_t2900481284 * L_110 = V_21;
			if (((XmlSchemaKeyref_t2789194649 *)IsInstClass(L_110, XmlSchemaKeyref_t2789194649_il2cpp_TypeInfo_var)))
			{
				goto IL_035b;
			}
		}

IL_0343:
		{
			Hashtable_t3875263730 * L_111 = __this->get_reservedConstraints_7();
			XmlSchemaObject_t2900481284 * L_112 = V_21;
			NullCheck(L_111);
			Il2CppObject * L_113 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_111, L_112);
			XmlSchemaDataImporter_ProcessSelfIdentity_m1913038921(__this, ((ConstraintStructure_t742574505 *)IsInstClass(L_113, ConstraintStructure_t742574505_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		}

IL_035b:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_114 = V_20;
			NullCheck(L_114);
			bool L_115 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_114);
			if (L_115)
			{
				goto IL_032e;
			}
		}

IL_0367:
		{
			IL2CPP_LEAVE(0x383, FINALLY_036c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_036c;
	}

FINALLY_036c:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_116 = V_20;
			Il2CppObject * L_117 = ((Il2CppObject *)IsInst(L_116, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_22 = L_117;
			if (!L_117)
			{
				goto IL_0382;
			}
		}

IL_037b:
		{
			Il2CppObject * L_118 = V_22;
			NullCheck(L_118);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_118);
		}

IL_0382:
		{
			IL2CPP_END_FINALLY(876)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(876)
	{
		IL2CPP_JUMP_TBL(0x383, IL_0383)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0383:
	{
		XmlSchemaElement_t471922321 * L_119 = __this->get_datasetElement_8();
		NullCheck(L_119);
		XmlSchemaObjectCollection_t2238201602 * L_120 = XmlSchemaElement_get_Constraints_m1205744096(L_119, /*hidden argument*/NULL);
		NullCheck(L_120);
		XmlSchemaObjectEnumerator_t3058853928 * L_121 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_120, /*hidden argument*/NULL);
		V_23 = L_121;
	}

IL_0395:
	try
	{ // begin try (depth: 1)
		{
			goto IL_03cd;
		}

IL_039a:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_122 = V_23;
			NullCheck(L_122);
			XmlSchemaObject_t2900481284 * L_123 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_122, /*hidden argument*/NULL);
			V_24 = L_123;
			XmlSchemaObject_t2900481284 * L_124 = V_24;
			if (!((XmlSchemaKeyref_t2789194649 *)IsInstClass(L_124, XmlSchemaKeyref_t2789194649_il2cpp_TypeInfo_var)))
			{
				goto IL_03cd;
			}
		}

IL_03af:
		{
			XmlSchemaElement_t471922321 * L_125 = __this->get_datasetElement_8();
			Hashtable_t3875263730 * L_126 = __this->get_reservedConstraints_7();
			XmlSchemaObject_t2900481284 * L_127 = V_24;
			NullCheck(L_126);
			Il2CppObject * L_128 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_126, L_127);
			XmlSchemaDataImporter_ProcessRelationIdentity_m2555776360(__this, L_125, ((ConstraintStructure_t742574505 *)IsInstClass(L_128, ConstraintStructure_t742574505_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		}

IL_03cd:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_129 = V_23;
			NullCheck(L_129);
			bool L_130 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_129);
			if (L_130)
			{
				goto IL_039a;
			}
		}

IL_03d9:
		{
			IL2CPP_LEAVE(0x3F5, FINALLY_03de);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_03de;
	}

FINALLY_03de:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_131 = V_23;
			Il2CppObject * L_132 = ((Il2CppObject *)IsInst(L_131, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_25 = L_132;
			if (!L_132)
			{
				goto IL_03f4;
			}
		}

IL_03ed:
		{
			Il2CppObject * L_133 = V_25;
			NullCheck(L_133);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_133);
		}

IL_03f4:
		{
			IL2CPP_END_FINALLY(990)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(990)
	{
		IL2CPP_JUMP_TBL(0x3F5, IL_03f5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_03f5:
	{
		ArrayList_t2121638921 * L_134 = __this->get_relations_6();
		NullCheck(L_134);
		Il2CppObject * L_135 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_134);
		V_26 = L_135;
	}

IL_0402:
	try
	{ // begin try (depth: 1)
		{
			goto IL_042d;
		}

IL_0407:
		{
			Il2CppObject * L_136 = V_26;
			NullCheck(L_136);
			Il2CppObject * L_137 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_136);
			V_27 = ((RelationStructure_t3039531114 *)CastclassClass(L_137, RelationStructure_t3039531114_il2cpp_TypeInfo_var));
			DataSet_t3654702571 * L_138 = __this->get_dataset_3();
			NullCheck(L_138);
			DataRelationCollection_t267599063 * L_139 = DataSet_get_Relations_m498597843(L_138, /*hidden argument*/NULL);
			RelationStructure_t3039531114 * L_140 = V_27;
			DataRelation_t1483987353 * L_141 = XmlSchemaDataImporter_GenerateRelationship_m939278488(__this, L_140, /*hidden argument*/NULL);
			NullCheck(L_139);
			DataRelationCollection_Add_m2199449921(L_139, L_141, /*hidden argument*/NULL);
		}

IL_042d:
		{
			Il2CppObject * L_142 = V_26;
			NullCheck(L_142);
			bool L_143 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_142);
			if (L_143)
			{
				goto IL_0407;
			}
		}

IL_0439:
		{
			IL2CPP_LEAVE(0x455, FINALLY_043e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_043e;
	}

FINALLY_043e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_144 = V_26;
			Il2CppObject * L_145 = ((Il2CppObject *)IsInst(L_144, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_28 = L_145;
			if (!L_145)
			{
				goto IL_0454;
			}
		}

IL_044d:
		{
			Il2CppObject * L_146 = V_28;
			NullCheck(L_146);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_146);
		}

IL_0454:
		{
			IL2CPP_END_FINALLY(1086)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1086)
	{
		IL2CPP_JUMP_TBL(0x455, IL_0455)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0455:
	{
		return;
	}
}
// System.Boolean System.Data.XmlSchemaDataImporter::IsDataSetElement(System.Xml.Schema.XmlSchemaElement)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern Il2CppClass* DataException_t1022144856_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaGroupBase_t3990058885_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaParticle_t3696384587_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2867372974;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3619623713;
extern const uint32_t XmlSchemaDataImporter_IsDataSetElement_m2426452300_MetadataUsageId;
extern "C"  bool XmlSchemaDataImporter_IsDataSetElement_m2426452300 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaElement_t471922321 * ___el0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_IsDataSetElement_m2426452300_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlAttributeU5BU5D_t656777856* V_0 = NULL;
	int32_t V_1 = 0;
	XmlAttribute_t2022155821 * V_2 = NULL;
	String_t* V_3 = NULL;
	Dictionary_2_t190145395 * V_4 = NULL;
	int32_t V_5 = 0;
	XmlSchemaComplexType_t1860629407 * V_6 = NULL;
	XmlSchemaGroupBase_t3990058885 * V_7 = NULL;
	XmlSchemaObjectEnumerator_t3058853928 * V_8 = NULL;
	XmlSchemaParticle_t3696384587 * V_9 = NULL;
	bool V_10 = false;
	Il2CppObject * V_11 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlSchemaElement_t471922321 * L_0 = ___el0;
		NullCheck(L_0);
		XmlAttributeU5BU5D_t656777856* L_1 = XmlSchemaAnnotated_get_UnhandledAttributes_m155687719(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00d7;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_2 = ___el0;
		NullCheck(L_2);
		XmlAttributeU5BU5D_t656777856* L_3 = XmlSchemaAnnotated_get_UnhandledAttributes_m155687719(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_00ce;
	}

IL_0019:
	{
		XmlAttributeU5BU5D_t656777856* L_4 = V_0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_2 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		XmlAttribute_t2022155821 * L_7 = V_2;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_8, _stringLiteral2867372974, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00ca;
		}
	}
	{
		XmlAttribute_t2022155821 * L_10 = V_2;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_11, _stringLiteral3051173506, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00ca;
		}
	}
	{
		XmlAttribute_t2022155821 * L_13 = V_2;
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_13);
		V_3 = L_14;
		String_t* L_15 = V_3;
		if (!L_15)
		{
			goto IL_00b4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		Dictionary_2_t190145395 * L_16 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24mapB_13();
		if (L_16)
		{
			goto IL_0087;
		}
	}
	{
		Dictionary_2_t190145395 * L_17 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_17, 2, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_4 = L_17;
		Dictionary_2_t190145395 * L_18 = V_4;
		NullCheck(L_18);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_18, _stringLiteral3569038, 0);
		Dictionary_2_t190145395 * L_19 = V_4;
		NullCheck(L_19);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_19, _stringLiteral97196323, 1);
		Dictionary_2_t190145395 * L_20 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24mapB_13(L_20);
	}

IL_0087:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		Dictionary_2_t190145395 * L_21 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24mapB_13();
		String_t* L_22 = V_3;
		NullCheck(L_21);
		bool L_23 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(35 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_21, L_22, (&V_5));
		if (!L_23)
		{
			goto IL_00b4;
		}
	}
	{
		int32_t L_24 = V_5;
		if (!L_24)
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_25 = V_5;
		if ((((int32_t)L_25) == ((int32_t)1)))
		{
			goto IL_00af;
		}
	}
	{
		goto IL_00b4;
	}

IL_00ad:
	{
		return (bool)1;
	}

IL_00af:
	{
		goto IL_00ca;
	}

IL_00b4:
	{
		XmlAttribute_t2022155821 * L_26 = V_2;
		NullCheck(L_26);
		String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_26);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3619623713, L_27, /*hidden argument*/NULL);
		DataException_t1022144856 * L_29 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_29, L_28, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29);
	}

IL_00ca:
	{
		int32_t L_30 = V_1;
		V_1 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00ce:
	{
		int32_t L_31 = V_1;
		XmlAttributeU5BU5D_t656777856* L_32 = V_0;
		NullCheck(L_32);
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length)))))))
		{
			goto IL_0019;
		}
	}

IL_00d7:
	{
		XmlSchema_t1932230565 * L_33 = __this->get_schema_5();
		NullCheck(L_33);
		XmlSchemaObjectTable_t167066468 * L_34 = XmlSchema_get_Elements_m1404485665(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		int32_t L_35 = XmlSchemaObjectTable_get_Count_m2682292569(L_34, /*hidden argument*/NULL);
		if ((((int32_t)L_35) == ((int32_t)1)))
		{
			goto IL_00ef;
		}
	}
	{
		return (bool)0;
	}

IL_00ef:
	{
		XmlSchemaElement_t471922321 * L_36 = ___el0;
		NullCheck(L_36);
		XmlSchemaType_t3432810239 * L_37 = XmlSchemaElement_get_SchemaType_m3318429078(L_36, /*hidden argument*/NULL);
		if (((XmlSchemaComplexType_t1860629407 *)IsInstClass(L_37, XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var)))
		{
			goto IL_0101;
		}
	}
	{
		return (bool)0;
	}

IL_0101:
	{
		XmlSchemaElement_t471922321 * L_38 = ___el0;
		NullCheck(L_38);
		XmlSchemaType_t3432810239 * L_39 = XmlSchemaElement_get_SchemaType_m3318429078(L_38, /*hidden argument*/NULL);
		V_6 = ((XmlSchemaComplexType_t1860629407 *)CastclassClass(L_39, XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var));
		XmlSchemaComplexType_t1860629407 * L_40 = V_6;
		NullCheck(L_40);
		XmlSchemaObjectTable_t167066468 * L_41 = XmlSchemaComplexType_get_AttributeUses_m1099936662(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		int32_t L_42 = XmlSchemaObjectTable_get_Count_m2682292569(L_41, /*hidden argument*/NULL);
		if ((((int32_t)L_42) <= ((int32_t)0)))
		{
			goto IL_0122;
		}
	}
	{
		return (bool)0;
	}

IL_0122:
	{
		XmlSchemaComplexType_t1860629407 * L_43 = V_6;
		NullCheck(L_43);
		XmlSchemaParticle_t3696384587 * L_44 = XmlSchemaComplexType_get_ContentTypeParticle_m2175008030(L_43, /*hidden argument*/NULL);
		V_7 = ((XmlSchemaGroupBase_t3990058885 *)IsInstClass(L_44, XmlSchemaGroupBase_t3990058885_il2cpp_TypeInfo_var));
		XmlSchemaGroupBase_t3990058885 * L_45 = V_7;
		if (!L_45)
		{
			goto IL_0148;
		}
	}
	{
		XmlSchemaGroupBase_t3990058885 * L_46 = V_7;
		NullCheck(L_46);
		XmlSchemaObjectCollection_t2238201602 * L_47 = VirtFuncInvoker0< XmlSchemaObjectCollection_t2238201602 * >::Invoke(17 /* System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaGroupBase::get_Items() */, L_46);
		NullCheck(L_47);
		int32_t L_48 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Collections.CollectionBase::get_Count() */, L_47);
		if (L_48)
		{
			goto IL_014a;
		}
	}

IL_0148:
	{
		return (bool)0;
	}

IL_014a:
	{
		XmlSchemaGroupBase_t3990058885 * L_49 = V_7;
		NullCheck(L_49);
		XmlSchemaObjectCollection_t2238201602 * L_50 = VirtFuncInvoker0< XmlSchemaObjectCollection_t2238201602 * >::Invoke(17 /* System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaGroupBase::get_Items() */, L_49);
		NullCheck(L_50);
		XmlSchemaObjectEnumerator_t3058853928 * L_51 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_50, /*hidden argument*/NULL);
		V_8 = L_51;
	}

IL_0158:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0180;
		}

IL_015d:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_52 = V_8;
			NullCheck(L_52);
			XmlSchemaObject_t2900481284 * L_53 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_52, /*hidden argument*/NULL);
			V_9 = ((XmlSchemaParticle_t3696384587 *)CastclassClass(L_53, XmlSchemaParticle_t3696384587_il2cpp_TypeInfo_var));
			XmlSchemaParticle_t3696384587 * L_54 = V_9;
			bool L_55 = XmlSchemaDataImporter_ContainsColumn_m1156723965(__this, L_54, /*hidden argument*/NULL);
			if (!L_55)
			{
				goto IL_0180;
			}
		}

IL_0178:
		{
			V_10 = (bool)0;
			IL2CPP_LEAVE(0x1AA, FINALLY_0191);
		}

IL_0180:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_56 = V_8;
			NullCheck(L_56);
			bool L_57 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_56);
			if (L_57)
			{
				goto IL_015d;
			}
		}

IL_018c:
		{
			IL2CPP_LEAVE(0x1A8, FINALLY_0191);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0191;
	}

FINALLY_0191:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_58 = V_8;
			Il2CppObject * L_59 = ((Il2CppObject *)IsInst(L_58, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_11 = L_59;
			if (!L_59)
			{
				goto IL_01a7;
			}
		}

IL_01a0:
		{
			Il2CppObject * L_60 = V_11;
			NullCheck(L_60);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_60);
		}

IL_01a7:
		{
			IL2CPP_END_FINALLY(401)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(401)
	{
		IL2CPP_JUMP_TBL(0x1AA, IL_01aa)
		IL2CPP_JUMP_TBL(0x1A8, IL_01a8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01a8:
	{
		return (bool)1;
	}

IL_01aa:
	{
		bool L_61 = V_10;
		return L_61;
	}
}
// System.Boolean System.Data.XmlSchemaDataImporter::ContainsColumn(System.Xml.Schema.XmlSchemaParticle)
extern Il2CppClass* XmlSchemaElement_t471922321_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaGroupBase_t3990058885_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaParticle_t3696384587_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_ContainsColumn_m1156723965_MetadataUsageId;
extern "C"  bool XmlSchemaDataImporter_ContainsColumn_m1156723965 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaParticle_t3696384587 * ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ContainsColumn_m1156723965_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlSchemaElement_t471922321 * V_0 = NULL;
	XmlSchemaComplexType_t1860629407 * V_1 = NULL;
	XmlSchemaGroupBase_t3990058885 * V_2 = NULL;
	int32_t V_3 = 0;
	{
		XmlSchemaParticle_t3696384587 * L_0 = ___p0;
		V_0 = ((XmlSchemaElement_t471922321 *)IsInstClass(L_0, XmlSchemaElement_t471922321_il2cpp_TypeInfo_var));
		XmlSchemaElement_t471922321 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0050;
		}
	}
	{
		V_1 = (XmlSchemaComplexType_t1860629407 *)NULL;
		XmlSchemaElement_t471922321 * L_2 = V_0;
		NullCheck(L_2);
		XmlSchemaType_t3432810239 * L_3 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_2, /*hidden argument*/NULL);
		V_1 = ((XmlSchemaComplexType_t1860629407 *)IsInstClass(L_3, XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var));
		XmlSchemaComplexType_t1860629407 * L_4 = V_1;
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		XmlSchemaComplexType_t1860629407 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		XmlSchemaComplexType_t1860629407 * L_6 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_schemaAnyType_2();
		if ((!(((Il2CppObject*)(XmlSchemaComplexType_t1860629407 *)L_5) == ((Il2CppObject*)(XmlSchemaComplexType_t1860629407 *)L_6))))
		{
			goto IL_002e;
		}
	}

IL_002c:
	{
		return (bool)1;
	}

IL_002e:
	{
		XmlSchemaComplexType_t1860629407 * L_7 = V_1;
		NullCheck(L_7);
		XmlSchemaObjectTable_t167066468 * L_8 = XmlSchemaComplexType_get_AttributeUses_m1099936662(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = XmlSchemaObjectTable_get_Count_m2682292569(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0041;
		}
	}
	{
		return (bool)0;
	}

IL_0041:
	{
		XmlSchemaComplexType_t1860629407 * L_10 = V_1;
		NullCheck(L_10);
		int32_t L_11 = XmlSchemaComplexType_get_ContentType_m7999845(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_004e;
		}
	}
	{
		return (bool)1;
	}

IL_004e:
	{
		return (bool)0;
	}

IL_0050:
	{
		XmlSchemaParticle_t3696384587 * L_12 = ___p0;
		V_2 = ((XmlSchemaGroupBase_t3990058885 *)IsInstClass(L_12, XmlSchemaGroupBase_t3990058885_il2cpp_TypeInfo_var));
		V_3 = 0;
		goto IL_0080;
	}

IL_005e:
	{
		XmlSchemaGroupBase_t3990058885 * L_13 = V_2;
		NullCheck(L_13);
		XmlSchemaObjectCollection_t2238201602 * L_14 = VirtFuncInvoker0< XmlSchemaObjectCollection_t2238201602 * >::Invoke(17 /* System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaGroupBase::get_Items() */, L_13);
		int32_t L_15 = V_3;
		NullCheck(L_14);
		XmlSchemaObject_t2900481284 * L_16 = VirtFuncInvoker1< XmlSchemaObject_t2900481284 *, int32_t >::Invoke(29 /* System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObjectCollection::get_Item(System.Int32) */, L_14, L_15);
		bool L_17 = XmlSchemaDataImporter_ContainsColumn_m1156723965(__this, ((XmlSchemaParticle_t3696384587 *)CastclassClass(L_16, XmlSchemaParticle_t3696384587_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_007c;
		}
	}
	{
		return (bool)1;
	}

IL_007c:
	{
		int32_t L_18 = V_3;
		V_3 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0080:
	{
		int32_t L_19 = V_3;
		XmlSchemaGroupBase_t3990058885 * L_20 = V_2;
		NullCheck(L_20);
		XmlSchemaObjectCollection_t2238201602 * L_21 = VirtFuncInvoker0< XmlSchemaObjectCollection_t2238201602 * >::Invoke(17 /* System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaGroupBase::get_Items() */, L_20);
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Collections.CollectionBase::get_Count() */, L_21);
		if ((((int32_t)L_19) < ((int32_t)L_22)))
		{
			goto IL_005e;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::ProcessGlobalElement(System.Xml.Schema.XmlSchemaElement)
extern Il2CppClass* XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_ProcessGlobalElement_m4175921586_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_ProcessGlobalElement_m4175921586 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaElement_t471922321 * ___el0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ProcessGlobalElement_m4175921586_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DataSet_t3654702571 * L_0 = __this->get_dataset_3();
		NullCheck(L_0);
		DataTableCollection_t2915263893 * L_1 = DataSet_get_Tables_m87321279(L_0, /*hidden argument*/NULL);
		XmlSchemaElement_t471922321 * L_2 = ___el0;
		NullCheck(L_2);
		XmlQualifiedName_t176365656 * L_3 = XmlSchemaElement_get_QualifiedName_m2908521753(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = XmlQualifiedName_get_Name_m607016698(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_5 = DataTableCollection_Contains_m3063800444(L_1, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0021;
		}
	}
	{
		return;
	}

IL_0021:
	{
		XmlSchemaElement_t471922321 * L_6 = ___el0;
		NullCheck(L_6);
		XmlSchemaType_t3432810239 * L_7 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_6, /*hidden argument*/NULL);
		if (!((XmlSchemaComplexType_t1860629407 *)IsInstClass(L_7, XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var)))
		{
			goto IL_0041;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_8 = ___el0;
		NullCheck(L_8);
		XmlSchemaType_t3432810239 * L_9 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		XmlSchemaComplexType_t1860629407 * L_10 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_schemaAnyType_2();
		if ((!(((Il2CppObject*)(XmlSchemaType_t3432810239 *)L_9) == ((Il2CppObject*)(XmlSchemaComplexType_t1860629407 *)L_10))))
		{
			goto IL_0042;
		}
	}

IL_0041:
	{
		return;
	}

IL_0042:
	{
		XmlSchemaElement_t471922321 * L_11 = ___el0;
		bool L_12 = XmlSchemaDataImporter_IsDataSetElement_m2426452300(__this, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0056;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_13 = ___el0;
		XmlSchemaDataImporter_ProcessDataSetElement_m2938042519(__this, L_13, /*hidden argument*/NULL);
		return;
	}

IL_0056:
	{
		DataSet_t3654702571 * L_14 = __this->get_dataset_3();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_15 = CultureInfo_get_CurrentCulture_m2905498779(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		DataSet_set_Locale_m2876356750(L_14, L_15, /*hidden argument*/NULL);
		ArrayList_t2121638921 * L_16 = __this->get_topLevelElements_9();
		XmlSchemaElement_t471922321 * L_17 = ___el0;
		NullCheck(L_16);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_16, L_17);
		XmlSchemaElement_t471922321 * L_18 = ___el0;
		XmlSchemaDataImporter_ProcessDataTableElement_m1241068523(__this, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::ProcessDataSetElement(System.Xml.Schema.XmlSchemaElement)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral144055052;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral3051547195;
extern Il2CppCodeGenString* _stringLiteral2281372282;
extern const uint32_t XmlSchemaDataImporter_ProcessDataSetElement_m2938042519_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_ProcessDataSetElement_m2938042519 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaElement_t471922321 * ___el0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ProcessDataSetElement_m2938042519_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	XmlAttributeU5BU5D_t656777856* V_1 = NULL;
	int32_t V_2 = 0;
	XmlAttribute_t2022155821 * V_3 = NULL;
	CultureInfo_t3603717042 * V_4 = NULL;
	XmlSchemaComplexType_t1860629407 * V_5 = NULL;
	XmlSchemaParticle_t3696384587 * V_6 = NULL;
	XmlSchemaParticle_t3696384587 * G_B19_0 = NULL;
	{
		DataSet_t3654702571 * L_0 = __this->get_dataset_3();
		XmlSchemaElement_t471922321 * L_1 = ___el0;
		NullCheck(L_1);
		String_t* L_2 = XmlSchemaElement_get_Name_m2136710865(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		DataSet_set_DataSetName_m1679911553(L_0, L_2, /*hidden argument*/NULL);
		XmlSchemaElement_t471922321 * L_3 = ___el0;
		__this->set_datasetElement_8(L_3);
		V_0 = (bool)0;
		XmlSchemaElement_t471922321 * L_4 = ___el0;
		NullCheck(L_4);
		XmlAttributeU5BU5D_t656777856* L_5 = XmlSchemaAnnotated_get_UnhandledAttributes_m155687719(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0105;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_6 = ___el0;
		NullCheck(L_6);
		XmlAttributeU5BU5D_t656777856* L_7 = XmlSchemaAnnotated_get_UnhandledAttributes_m155687719(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = 0;
		goto IL_00fc;
	}

IL_0033:
	{
		XmlAttributeU5BU5D_t656777856* L_8 = V_1;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		V_3 = ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
		XmlAttribute_t2022155821 * L_11 = V_3;
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_12, _stringLiteral144055052, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0063;
		}
	}
	{
		XmlAttribute_t2022155821 * L_14 = V_3;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_15, _stringLiteral3051173506, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0063;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0063:
	{
		XmlAttribute_t2022155821 * L_17 = V_3;
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_18, _stringLiteral3051547195, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00b4;
		}
	}
	{
		DataSet_t3654702571 * L_20 = __this->get_dataset_3();
		NullCheck(L_20);
		PropertyCollection_t3599376422 * L_21 = DataSet_get_ExtendedProperties_m3188566177(L_20, /*hidden argument*/NULL);
		XmlAttribute_t2022155821 * L_22 = V_3;
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(16 /* System.String System.Xml.XmlNode::get_Name() */, L_22);
		NullCheck(L_21);
		bool L_24 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(34 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_21, L_23);
		if (L_24)
		{
			goto IL_00b4;
		}
	}
	{
		DataSet_t3654702571 * L_25 = __this->get_dataset_3();
		NullCheck(L_25);
		PropertyCollection_t3599376422 * L_26 = DataSet_get_ExtendedProperties_m3188566177(L_25, /*hidden argument*/NULL);
		XmlAttribute_t2022155821 * L_27 = V_3;
		NullCheck(L_27);
		String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(16 /* System.String System.Xml.XmlNode::get_Name() */, L_27);
		XmlAttribute_t2022155821 * L_29 = V_3;
		NullCheck(L_29);
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_29);
		NullCheck(L_26);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_26, L_28, L_30);
		goto IL_00f8;
	}

IL_00b4:
	{
		XmlAttribute_t2022155821 * L_31 = V_3;
		NullCheck(L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_31);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_33 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_32, _stringLiteral2281372282, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00f8;
		}
	}
	{
		XmlAttribute_t2022155821 * L_34 = V_3;
		NullCheck(L_34);
		String_t* L_35 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_34);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_36 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_35, _stringLiteral3051173506, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00f8;
		}
	}
	{
		XmlAttribute_t2022155821 * L_37 = V_3;
		NullCheck(L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_37);
		CultureInfo_t3603717042 * L_39 = (CultureInfo_t3603717042 *)il2cpp_codegen_object_new(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo__ctor_m3707520096(L_39, L_38, /*hidden argument*/NULL);
		V_4 = L_39;
		DataSet_t3654702571 * L_40 = __this->get_dataset_3();
		CultureInfo_t3603717042 * L_41 = V_4;
		NullCheck(L_40);
		DataSet_set_Locale_m2876356750(L_40, L_41, /*hidden argument*/NULL);
	}

IL_00f8:
	{
		int32_t L_42 = V_2;
		V_2 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_00fc:
	{
		int32_t L_43 = V_2;
		XmlAttributeU5BU5D_t656777856* L_44 = V_1;
		NullCheck(L_44);
		if ((((int32_t)L_43) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_44)->max_length)))))))
		{
			goto IL_0033;
		}
	}

IL_0105:
	{
		bool L_45 = V_0;
		if (L_45)
		{
			goto IL_012b;
		}
	}
	{
		DataSet_t3654702571 * L_46 = __this->get_dataset_3();
		NullCheck(L_46);
		bool L_47 = DataSet_get_LocaleSpecified_m1230188946(L_46, /*hidden argument*/NULL);
		if (L_47)
		{
			goto IL_012b;
		}
	}
	{
		DataSet_t3654702571 * L_48 = __this->get_dataset_3();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_49 = CultureInfo_get_CurrentCulture_m2905498779(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_48);
		DataSet_set_Locale_m2876356750(L_48, L_49, /*hidden argument*/NULL);
	}

IL_012b:
	{
		V_5 = (XmlSchemaComplexType_t1860629407 *)NULL;
		XmlSchemaElement_t471922321 * L_50 = ___el0;
		NullCheck(L_50);
		XmlSchemaType_t3432810239 * L_51 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_50, /*hidden argument*/NULL);
		V_5 = ((XmlSchemaComplexType_t1860629407 *)IsInstClass(L_51, XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var));
		XmlSchemaComplexType_t1860629407 * L_52 = V_5;
		if (!L_52)
		{
			goto IL_014e;
		}
	}
	{
		XmlSchemaComplexType_t1860629407 * L_53 = V_5;
		NullCheck(L_53);
		XmlSchemaParticle_t3696384587 * L_54 = XmlSchemaComplexType_get_ContentTypeParticle_m2175008030(L_53, /*hidden argument*/NULL);
		G_B19_0 = L_54;
		goto IL_014f;
	}

IL_014e:
	{
		G_B19_0 = ((XmlSchemaParticle_t3696384587 *)(NULL));
	}

IL_014f:
	{
		V_6 = G_B19_0;
		XmlSchemaParticle_t3696384587 * L_55 = V_6;
		if (!L_55)
		{
			goto IL_0160;
		}
	}
	{
		XmlSchemaParticle_t3696384587 * L_56 = V_6;
		XmlSchemaDataImporter_HandleDataSetContentTypeParticle_m2951522367(__this, L_56, /*hidden argument*/NULL);
	}

IL_0160:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::HandleDataSetContentTypeParticle(System.Xml.Schema.XmlSchemaParticle)
extern Il2CppClass* XmlSchemaElement_t471922321_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlQualifiedName_t176365656_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaGroupBase_t3990058885_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaParticle_t3696384587_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_HandleDataSetContentTypeParticle_m2951522367_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_HandleDataSetContentTypeParticle_m2951522367 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaParticle_t3696384587 * ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_HandleDataSetContentTypeParticle_m2951522367_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlSchemaElement_t471922321 * V_0 = NULL;
	XmlSchemaObjectEnumerator_t3058853928 * V_1 = NULL;
	XmlSchemaParticle_t3696384587 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlSchemaParticle_t3696384587 * L_0 = ___p0;
		V_0 = ((XmlSchemaElement_t471922321 *)IsInstClass(L_0, XmlSchemaElement_t471922321_il2cpp_TypeInfo_var));
		XmlSchemaElement_t471922321 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_003f;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_2 = V_0;
		NullCheck(L_2);
		XmlSchemaType_t3432810239 * L_3 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_2, /*hidden argument*/NULL);
		if (!((XmlSchemaComplexType_t1860629407 *)IsInstClass(L_3, XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var)))
		{
			goto IL_003a;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_4 = V_0;
		NullCheck(L_4);
		XmlQualifiedName_t176365656 * L_5 = XmlSchemaElement_get_RefName_m3872471184(L_4, /*hidden argument*/NULL);
		XmlSchemaElement_t471922321 * L_6 = V_0;
		NullCheck(L_6);
		XmlQualifiedName_t176365656 * L_7 = XmlSchemaElement_get_QualifiedName_m2908521753(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		bool L_8 = XmlQualifiedName_op_Inequality_m188426612(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003a;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_9 = V_0;
		XmlSchemaDataImporter_ProcessDataTableElement_m1241068523(__this, L_9, /*hidden argument*/NULL);
	}

IL_003a:
	{
		goto IL_0097;
	}

IL_003f:
	{
		XmlSchemaParticle_t3696384587 * L_10 = ___p0;
		if (!((XmlSchemaGroupBase_t3990058885 *)IsInstClass(L_10, XmlSchemaGroupBase_t3990058885_il2cpp_TypeInfo_var)))
		{
			goto IL_0097;
		}
	}
	{
		XmlSchemaParticle_t3696384587 * L_11 = ___p0;
		NullCheck(((XmlSchemaGroupBase_t3990058885 *)CastclassClass(L_11, XmlSchemaGroupBase_t3990058885_il2cpp_TypeInfo_var)));
		XmlSchemaObjectCollection_t2238201602 * L_12 = VirtFuncInvoker0< XmlSchemaObjectCollection_t2238201602 * >::Invoke(17 /* System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaGroupBase::get_Items() */, ((XmlSchemaGroupBase_t3990058885 *)CastclassClass(L_11, XmlSchemaGroupBase_t3990058885_il2cpp_TypeInfo_var)));
		NullCheck(L_12);
		XmlSchemaObjectEnumerator_t3058853928 * L_13 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_12, /*hidden argument*/NULL);
		V_1 = L_13;
	}

IL_005b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0073;
		}

IL_0060:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_14 = V_1;
			NullCheck(L_14);
			XmlSchemaObject_t2900481284 * L_15 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_14, /*hidden argument*/NULL);
			V_2 = ((XmlSchemaParticle_t3696384587 *)CastclassClass(L_15, XmlSchemaParticle_t3696384587_il2cpp_TypeInfo_var));
			XmlSchemaParticle_t3696384587 * L_16 = V_2;
			XmlSchemaDataImporter_HandleDataSetContentTypeParticle_m2951522367(__this, L_16, /*hidden argument*/NULL);
		}

IL_0073:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_17 = V_1;
			NullCheck(L_17);
			bool L_18 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_17);
			if (L_18)
			{
				goto IL_0060;
			}
		}

IL_007e:
		{
			IL2CPP_LEAVE(0x97, FINALLY_0083);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0083;
	}

FINALLY_0083:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_19 = V_1;
			Il2CppObject * L_20 = ((Il2CppObject *)IsInst(L_19, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_3 = L_20;
			if (!L_20)
			{
				goto IL_0096;
			}
		}

IL_0090:
		{
			Il2CppObject * L_21 = V_3;
			NullCheck(L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_21);
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(131)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(131)
	{
		IL2CPP_JUMP_TBL(0x97, IL_0097)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0097:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::ProcessDataTableElement(System.Xml.Schema.XmlSchemaElement)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* DataTable_t2176726999_il2cpp_TypeInfo_var;
extern Il2CppClass* TableStructure_t1082771224_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* DictionaryEntry_t130027246_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaAttribute_t1191708721_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaElement_t471922321_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaGroupBase_t3990058885_il2cpp_TypeInfo_var;
extern Il2CppClass* DataColumn_t3354469747_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t1688557254_il2cpp_TypeInfo_var;
extern Il2CppClass* SortedList_t3322490541_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3051547195;
extern Il2CppCodeGenString* _stringLiteral2281372282;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral91291148;
extern const uint32_t XmlSchemaDataImporter_ProcessDataTableElement_m1241068523_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_ProcessDataTableElement_m1241068523 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaElement_t471922321 * ___el0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ProcessDataTableElement_m1241068523_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	DataTable_t2176726999 * V_1 = NULL;
	TableStructure_t1082771224 * V_2 = NULL;
	XmlAttributeU5BU5D_t656777856* V_3 = NULL;
	int32_t V_4 = 0;
	XmlAttribute_t2022155821 * V_5 = NULL;
	XmlSchemaComplexType_t1860629407 * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	DictionaryEntry_t130027246  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Il2CppObject * V_9 = NULL;
	int32_t V_10 = 0;
	String_t* V_11 = NULL;
	DataColumn_t3354469747 * V_12 = NULL;
	SortedList_t3322490541 * V_13 = NULL;
	Il2CppObject * V_14 = NULL;
	DictionaryEntry_t130027246  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Il2CppObject * V_16 = NULL;
	Il2CppObject * V_17 = NULL;
	DictionaryEntry_t130027246  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Il2CppObject * V_19 = NULL;
	Il2CppObject * V_20 = NULL;
	DataColumn_t3354469747 * V_21 = NULL;
	Il2CppObject * V_22 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlSchemaElement_t471922321 * L_0 = ___el0;
		NullCheck(L_0);
		XmlQualifiedName_t176365656 * L_1 = XmlSchemaElement_get_QualifiedName_m2908521753(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = XmlQualifiedName_get_Name_m607016698(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_3 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		DataSet_t3654702571 * L_4 = __this->get_dataset_3();
		NullCheck(L_4);
		DataTableCollection_t2915263893 * L_5 = DataSet_get_Tables_m87321279(L_4, /*hidden argument*/NULL);
		String_t* L_6 = V_0;
		NullCheck(L_5);
		bool L_7 = DataTableCollection_Contains_m3063800444(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0028;
		}
	}
	{
		return;
	}

IL_0028:
	{
		String_t* L_8 = V_0;
		DataTable_t2176726999 * L_9 = (DataTable_t2176726999 *)il2cpp_codegen_object_new(DataTable_t2176726999_il2cpp_TypeInfo_var);
		DataTable__ctor_m1313393457(L_9, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		DataTable_t2176726999 * L_10 = V_1;
		XmlSchemaElement_t471922321 * L_11 = ___el0;
		NullCheck(L_11);
		XmlQualifiedName_t176365656 * L_12 = XmlSchemaElement_get_QualifiedName_m2908521753(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = XmlQualifiedName_get_Namespace_m2987642414(L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		DataTable_set_Namespace_m1042478933(L_10, L_13, /*hidden argument*/NULL);
		TableStructure_t1082771224 * L_14 = __this->get_currentTable_11();
		V_2 = L_14;
		DataTable_t2176726999 * L_15 = V_1;
		TableStructure_t1082771224 * L_16 = (TableStructure_t1082771224 *)il2cpp_codegen_object_new(TableStructure_t1082771224_il2cpp_TypeInfo_var);
		TableStructure__ctor_m422151719(L_16, L_15, /*hidden argument*/NULL);
		__this->set_currentTable_11(L_16);
		DataSet_t3654702571 * L_17 = __this->get_dataset_3();
		NullCheck(L_17);
		DataTableCollection_t2915263893 * L_18 = DataSet_get_Tables_m87321279(L_17, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_19 = V_1;
		NullCheck(L_18);
		DataTableCollection_Add_m3310506721(L_18, L_19, /*hidden argument*/NULL);
		XmlSchemaElement_t471922321 * L_20 = ___el0;
		NullCheck(L_20);
		XmlAttributeU5BU5D_t656777856* L_21 = XmlSchemaAnnotated_get_UnhandledAttributes_m155687719(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0106;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_22 = ___el0;
		NullCheck(L_22);
		XmlAttributeU5BU5D_t656777856* L_23 = XmlSchemaAnnotated_get_UnhandledAttributes_m155687719(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00fc;
	}

IL_007e:
	{
		XmlAttributeU5BU5D_t656777856* L_24 = V_3;
		int32_t L_25 = V_4;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		V_5 = ((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26)));
		XmlAttribute_t2022155821 * L_27 = V_5;
		NullCheck(L_27);
		String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_29 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_28, _stringLiteral3051547195, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00b8;
		}
	}
	{
		DataTable_t2176726999 * L_30 = V_1;
		NullCheck(L_30);
		PropertyCollection_t3599376422 * L_31 = DataTable_get_ExtendedProperties_m3062393549(L_30, /*hidden argument*/NULL);
		XmlAttribute_t2022155821 * L_32 = V_5;
		NullCheck(L_32);
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(16 /* System.String System.Xml.XmlNode::get_Name() */, L_32);
		XmlAttribute_t2022155821 * L_34 = V_5;
		NullCheck(L_34);
		String_t* L_35 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_34);
		NullCheck(L_31);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_31, L_33, L_35);
		goto IL_00f6;
	}

IL_00b8:
	{
		XmlAttribute_t2022155821 * L_36 = V_5;
		NullCheck(L_36);
		String_t* L_37 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_36);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_38 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_37, _stringLiteral2281372282, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_00f6;
		}
	}
	{
		XmlAttribute_t2022155821 * L_39 = V_5;
		NullCheck(L_39);
		String_t* L_40 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_39);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_41 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_40, _stringLiteral3051173506, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_00f6;
		}
	}
	{
		DataTable_t2176726999 * L_42 = V_1;
		XmlAttribute_t2022155821 * L_43 = V_5;
		NullCheck(L_43);
		String_t* L_44 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_43);
		CultureInfo_t3603717042 * L_45 = (CultureInfo_t3603717042 *)il2cpp_codegen_object_new(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo__ctor_m3707520096(L_45, L_44, /*hidden argument*/NULL);
		NullCheck(L_42);
		DataTable_set_Locale_m1062226426(L_42, L_45, /*hidden argument*/NULL);
	}

IL_00f6:
	{
		int32_t L_46 = V_4;
		V_4 = ((int32_t)((int32_t)L_46+(int32_t)1));
	}

IL_00fc:
	{
		int32_t L_47 = V_4;
		XmlAttributeU5BU5D_t656777856* L_48 = V_3;
		NullCheck(L_48);
		if ((((int32_t)L_47) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_48)->max_length)))))))
		{
			goto IL_007e;
		}
	}

IL_0106:
	{
		V_6 = (XmlSchemaComplexType_t1860629407 *)NULL;
		XmlSchemaElement_t471922321 * L_49 = ___el0;
		NullCheck(L_49);
		XmlSchemaType_t3432810239 * L_50 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_49, /*hidden argument*/NULL);
		V_6 = ((XmlSchemaComplexType_t1860629407 *)CastclassClass(L_50, XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var));
		XmlSchemaComplexType_t1860629407 * L_51 = V_6;
		NullCheck(L_51);
		XmlSchemaObjectTable_t167066468 * L_52 = XmlSchemaComplexType_get_AttributeUses_m1099936662(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		Il2CppObject * L_53 = XmlSchemaObjectTable_GetEnumerator_m4054791657(L_52, /*hidden argument*/NULL);
		V_7 = L_53;
	}

IL_0124:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0149;
		}

IL_0129:
		{
			Il2CppObject * L_54 = V_7;
			NullCheck(L_54);
			Il2CppObject * L_55 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_54);
			V_8 = ((*(DictionaryEntry_t130027246 *)((DictionaryEntry_t130027246 *)UnBox (L_55, DictionaryEntry_t130027246_il2cpp_TypeInfo_var))));
			Il2CppObject * L_56 = DictionaryEntry_get_Value_m4281303039((&V_8), /*hidden argument*/NULL);
			XmlSchemaDataImporter_ImportColumnAttribute_m1880307109(__this, ((XmlSchemaAttribute_t1191708721 *)CastclassClass(L_56, XmlSchemaAttribute_t1191708721_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		}

IL_0149:
		{
			Il2CppObject * L_57 = V_7;
			NullCheck(L_57);
			bool L_58 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_57);
			if (L_58)
			{
				goto IL_0129;
			}
		}

IL_0155:
		{
			IL2CPP_LEAVE(0x171, FINALLY_015a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_015a;
	}

FINALLY_015a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_59 = V_7;
			Il2CppObject * L_60 = ((Il2CppObject *)IsInst(L_59, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_9 = L_60;
			if (!L_60)
			{
				goto IL_0170;
			}
		}

IL_0169:
		{
			Il2CppObject * L_61 = V_9;
			NullCheck(L_61);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_61);
		}

IL_0170:
		{
			IL2CPP_END_FINALLY(346)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(346)
	{
		IL2CPP_JUMP_TBL(0x171, IL_0171)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0171:
	{
		XmlSchemaComplexType_t1860629407 * L_62 = V_6;
		NullCheck(L_62);
		XmlSchemaParticle_t3696384587 * L_63 = XmlSchemaComplexType_get_ContentTypeParticle_m2175008030(L_62, /*hidden argument*/NULL);
		if (!((XmlSchemaElement_t471922321 *)IsInstClass(L_63, XmlSchemaElement_t471922321_il2cpp_TypeInfo_var)))
		{
			goto IL_019a;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_64 = ___el0;
		XmlSchemaComplexType_t1860629407 * L_65 = V_6;
		NullCheck(L_65);
		XmlSchemaParticle_t3696384587 * L_66 = XmlSchemaComplexType_get_ContentTypeParticle_m2175008030(L_65, /*hidden argument*/NULL);
		XmlSchemaDataImporter_ImportColumnElement_m3899597382(__this, L_64, ((XmlSchemaElement_t471922321 *)CastclassClass(L_66, XmlSchemaElement_t471922321_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_01be;
	}

IL_019a:
	{
		XmlSchemaComplexType_t1860629407 * L_67 = V_6;
		NullCheck(L_67);
		XmlSchemaParticle_t3696384587 * L_68 = XmlSchemaComplexType_get_ContentTypeParticle_m2175008030(L_67, /*hidden argument*/NULL);
		if (!((XmlSchemaGroupBase_t3990058885 *)IsInstClass(L_68, XmlSchemaGroupBase_t3990058885_il2cpp_TypeInfo_var)))
		{
			goto IL_01be;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_69 = ___el0;
		XmlSchemaComplexType_t1860629407 * L_70 = V_6;
		NullCheck(L_70);
		XmlSchemaParticle_t3696384587 * L_71 = XmlSchemaComplexType_get_ContentTypeParticle_m2175008030(L_70, /*hidden argument*/NULL);
		XmlSchemaDataImporter_ImportColumnGroupBase_m637005790(__this, L_69, ((XmlSchemaGroupBase_t3990058885 *)CastclassClass(L_71, XmlSchemaGroupBase_t3990058885_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
	}

IL_01be:
	{
		XmlSchemaComplexType_t1860629407 * L_72 = V_6;
		NullCheck(L_72);
		int32_t L_73 = XmlSchemaComplexType_get_ContentType_m7999845(L_72, /*hidden argument*/NULL);
		V_10 = L_73;
		int32_t L_74 = V_10;
		if (!L_74)
		{
			goto IL_01d3;
		}
	}
	{
		goto IL_0251;
	}

IL_01d3:
	{
		XmlSchemaElement_t471922321 * L_75 = ___el0;
		NullCheck(L_75);
		XmlQualifiedName_t176365656 * L_76 = XmlSchemaElement_get_QualifiedName_m2908521753(L_75, /*hidden argument*/NULL);
		NullCheck(L_76);
		String_t* L_77 = XmlQualifiedName_get_Name_m607016698(L_76, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_78 = String_Concat_m138640077(NULL /*static, unused*/, L_77, _stringLiteral91291148, /*hidden argument*/NULL);
		V_11 = L_78;
		String_t* L_79 = V_11;
		DataColumn_t3354469747 * L_80 = (DataColumn_t3354469747 *)il2cpp_codegen_object_new(DataColumn_t3354469747_il2cpp_TypeInfo_var);
		DataColumn__ctor_m273049649(L_80, L_79, /*hidden argument*/NULL);
		V_12 = L_80;
		DataColumn_t3354469747 * L_81 = V_12;
		XmlSchemaElement_t471922321 * L_82 = ___el0;
		NullCheck(L_82);
		XmlQualifiedName_t176365656 * L_83 = XmlSchemaElement_get_QualifiedName_m2908521753(L_82, /*hidden argument*/NULL);
		NullCheck(L_83);
		String_t* L_84 = XmlQualifiedName_get_Namespace_m2987642414(L_83, /*hidden argument*/NULL);
		NullCheck(L_81);
		DataColumn_set_Namespace_m2872284245(L_81, L_84, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_85 = V_12;
		XmlSchemaElement_t471922321 * L_86 = ___el0;
		NullCheck(L_86);
		Decimal_t1688557254  L_87 = XmlSchemaParticle_get_MinOccurs_m2470369063(L_86, /*hidden argument*/NULL);
		Decimal_t1688557254  L_88;
		memset(&L_88, 0, sizeof(L_88));
		Decimal__ctor_m3224507889(&L_88, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1688557254_il2cpp_TypeInfo_var);
		bool L_89 = Decimal_op_Equality_m4013483186(NULL /*static, unused*/, L_87, L_88, /*hidden argument*/NULL);
		NullCheck(L_85);
		DataColumn_set_AllowDBNull_m4066005655(L_85, L_89, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_90 = V_12;
		NullCheck(L_90);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Data.DataColumn::set_ColumnMapping(System.Data.MappingType) */, L_90, 3);
		DataColumn_t3354469747 * L_91 = V_12;
		XmlSchemaComplexType_t1860629407 * L_92 = V_6;
		NullCheck(L_92);
		XmlSchemaDatatype_t2590121 * L_93 = XmlSchemaType_get_Datatype_m2483034461(L_92, /*hidden argument*/NULL);
		Type_t * L_94 = XmlSchemaDataImporter_ConvertDatatype_m2516858589(__this, L_93, /*hidden argument*/NULL);
		NullCheck(L_91);
		DataColumn_set_DataType_m1172045637(L_91, L_94, /*hidden argument*/NULL);
		TableStructure_t1082771224 * L_95 = __this->get_currentTable_11();
		NullCheck(L_95);
		ArrayList_t2121638921 * L_96 = L_95->get_NonOrdinalColumns_2();
		DataColumn_t3354469747 * L_97 = V_12;
		NullCheck(L_96);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_96, L_97);
		goto IL_0251;
	}

IL_0251:
	{
		SortedList_t3322490541 * L_98 = (SortedList_t3322490541 *)il2cpp_codegen_object_new(SortedList_t3322490541_il2cpp_TypeInfo_var);
		SortedList__ctor_m3703218657(L_98, /*hidden argument*/NULL);
		V_13 = L_98;
		TableStructure_t1082771224 * L_99 = __this->get_currentTable_11();
		NullCheck(L_99);
		Hashtable_t3875263730 * L_100 = L_99->get_OrdinalColumns_1();
		NullCheck(L_100);
		Il2CppObject * L_101 = VirtFuncInvoker0< Il2CppObject * >::Invoke(32 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_100);
		V_14 = L_101;
	}

IL_026a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0292;
		}

IL_026f:
		{
			Il2CppObject * L_102 = V_14;
			NullCheck(L_102);
			Il2CppObject * L_103 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_102);
			V_15 = ((*(DictionaryEntry_t130027246 *)((DictionaryEntry_t130027246 *)UnBox (L_103, DictionaryEntry_t130027246_il2cpp_TypeInfo_var))));
			SortedList_t3322490541 * L_104 = V_13;
			Il2CppObject * L_105 = DictionaryEntry_get_Value_m4281303039((&V_15), /*hidden argument*/NULL);
			Il2CppObject * L_106 = DictionaryEntry_get_Key_m3516209325((&V_15), /*hidden argument*/NULL);
			NullCheck(L_104);
			VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Void System.Collections.SortedList::Add(System.Object,System.Object) */, L_104, L_105, L_106);
		}

IL_0292:
		{
			Il2CppObject * L_107 = V_14;
			NullCheck(L_107);
			bool L_108 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_107);
			if (L_108)
			{
				goto IL_026f;
			}
		}

IL_029e:
		{
			IL2CPP_LEAVE(0x2BA, FINALLY_02a3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_02a3;
	}

FINALLY_02a3:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_109 = V_14;
			Il2CppObject * L_110 = ((Il2CppObject *)IsInst(L_109, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_16 = L_110;
			if (!L_110)
			{
				goto IL_02b9;
			}
		}

IL_02b2:
		{
			Il2CppObject * L_111 = V_16;
			NullCheck(L_111);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_111);
		}

IL_02b9:
		{
			IL2CPP_END_FINALLY(675)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(675)
	{
		IL2CPP_JUMP_TBL(0x2BA, IL_02ba)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_02ba:
	{
		SortedList_t3322490541 * L_112 = V_13;
		NullCheck(L_112);
		Il2CppObject * L_113 = VirtFuncInvoker0< Il2CppObject * >::Invoke(33 /* System.Collections.IDictionaryEnumerator System.Collections.SortedList::GetEnumerator() */, L_112);
		V_17 = L_113;
	}

IL_02c3:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02ed;
		}

IL_02c8:
		{
			Il2CppObject * L_114 = V_17;
			NullCheck(L_114);
			Il2CppObject * L_115 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_114);
			V_18 = ((*(DictionaryEntry_t130027246 *)((DictionaryEntry_t130027246 *)UnBox (L_115, DictionaryEntry_t130027246_il2cpp_TypeInfo_var))));
			DataTable_t2176726999 * L_116 = V_1;
			NullCheck(L_116);
			DataColumnCollection_t3528392753 * L_117 = DataTable_get_Columns_m220042291(L_116, /*hidden argument*/NULL);
			Il2CppObject * L_118 = DictionaryEntry_get_Value_m4281303039((&V_18), /*hidden argument*/NULL);
			NullCheck(L_117);
			DataColumnCollection_Add_m3379764877(L_117, ((DataColumn_t3354469747 *)CastclassClass(L_118, DataColumn_t3354469747_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		}

IL_02ed:
		{
			Il2CppObject * L_119 = V_17;
			NullCheck(L_119);
			bool L_120 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_119);
			if (L_120)
			{
				goto IL_02c8;
			}
		}

IL_02f9:
		{
			IL2CPP_LEAVE(0x315, FINALLY_02fe);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_02fe;
	}

FINALLY_02fe:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_121 = V_17;
			Il2CppObject * L_122 = ((Il2CppObject *)IsInst(L_121, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_19 = L_122;
			if (!L_122)
			{
				goto IL_0314;
			}
		}

IL_030d:
		{
			Il2CppObject * L_123 = V_19;
			NullCheck(L_123);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_123);
		}

IL_0314:
		{
			IL2CPP_END_FINALLY(766)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(766)
	{
		IL2CPP_JUMP_TBL(0x315, IL_0315)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0315:
	{
		TableStructure_t1082771224 * L_124 = __this->get_currentTable_11();
		NullCheck(L_124);
		ArrayList_t2121638921 * L_125 = L_124->get_NonOrdinalColumns_2();
		NullCheck(L_125);
		Il2CppObject * L_126 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_125);
		V_20 = L_126;
	}

IL_0327:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0347;
		}

IL_032c:
		{
			Il2CppObject * L_127 = V_20;
			NullCheck(L_127);
			Il2CppObject * L_128 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_127);
			V_21 = ((DataColumn_t3354469747 *)CastclassClass(L_128, DataColumn_t3354469747_il2cpp_TypeInfo_var));
			DataTable_t2176726999 * L_129 = V_1;
			NullCheck(L_129);
			DataColumnCollection_t3528392753 * L_130 = DataTable_get_Columns_m220042291(L_129, /*hidden argument*/NULL);
			DataColumn_t3354469747 * L_131 = V_21;
			NullCheck(L_130);
			DataColumnCollection_Add_m3379764877(L_130, L_131, /*hidden argument*/NULL);
		}

IL_0347:
		{
			Il2CppObject * L_132 = V_20;
			NullCheck(L_132);
			bool L_133 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_132);
			if (L_133)
			{
				goto IL_032c;
			}
		}

IL_0353:
		{
			IL2CPP_LEAVE(0x36F, FINALLY_0358);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0358;
	}

FINALLY_0358:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_134 = V_20;
			Il2CppObject * L_135 = ((Il2CppObject *)IsInst(L_134, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_22 = L_135;
			if (!L_135)
			{
				goto IL_036e;
			}
		}

IL_0367:
		{
			Il2CppObject * L_136 = V_22;
			NullCheck(L_136);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_136);
		}

IL_036e:
		{
			IL2CPP_END_FINALLY(856)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(856)
	{
		IL2CPP_JUMP_TBL(0x36F, IL_036f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_036f:
	{
		TableStructure_t1082771224 * L_137 = V_2;
		__this->set_currentTable_11(L_137);
		return;
	}
}
// System.Data.DataRelation System.Data.XmlSchemaDataImporter::GenerateRelationship(System.Data.RelationStructure)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DataColumnU5BU5D_t3410743138_il2cpp_TypeInfo_var;
extern Il2CppClass* DataRelation_t1483987353_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_GenerateRelationship_m939278488_MetadataUsageId;
extern "C"  DataRelation_t1483987353 * XmlSchemaDataImporter_GenerateRelationship_m939278488 (XmlSchemaDataImporter_t2811591463 * __this, RelationStructure_t3039531114 * ___rs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_GenerateRelationship_m939278488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataTable_t2176726999 * V_0 = NULL;
	DataTable_t2176726999 * V_1 = NULL;
	DataRelation_t1483987353 * V_2 = NULL;
	String_t* V_3 = NULL;
	StringU5BU5D_t2956870243* V_4 = NULL;
	StringU5BU5D_t2956870243* V_5 = NULL;
	DataColumnU5BU5D_t3410743138* V_6 = NULL;
	int32_t V_7 = 0;
	DataColumnU5BU5D_t3410743138* V_8 = NULL;
	int32_t V_9 = 0;
	DataColumn_t3354469747 * V_10 = NULL;
	DataColumn_t3354469747 * V_11 = NULL;
	String_t* G_B3_0 = NULL;
	{
		DataSet_t3654702571 * L_0 = __this->get_dataset_3();
		NullCheck(L_0);
		DataTableCollection_t2915263893 * L_1 = DataSet_get_Tables_m87321279(L_0, /*hidden argument*/NULL);
		RelationStructure_t3039531114 * L_2 = ___rs0;
		NullCheck(L_2);
		String_t* L_3 = L_2->get_ParentTableName_1();
		NullCheck(L_1);
		DataTable_t2176726999 * L_4 = DataTableCollection_get_Item_m2714089417(L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		DataSet_t3654702571 * L_5 = __this->get_dataset_3();
		NullCheck(L_5);
		DataTableCollection_t2915263893 * L_6 = DataSet_get_Tables_m87321279(L_5, /*hidden argument*/NULL);
		RelationStructure_t3039531114 * L_7 = ___rs0;
		NullCheck(L_7);
		String_t* L_8 = L_7->get_ChildTableName_2();
		NullCheck(L_6);
		DataTable_t2176726999 * L_9 = DataTableCollection_get_Item_m2714089417(L_6, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		RelationStructure_t3039531114 * L_10 = ___rs0;
		NullCheck(L_10);
		String_t* L_11 = L_10->get_ExplicitName_0();
		if (!L_11)
		{
			goto IL_0044;
		}
	}
	{
		RelationStructure_t3039531114 * L_12 = ___rs0;
		NullCheck(L_12);
		String_t* L_13 = L_12->get_ExplicitName_0();
		G_B3_0 = L_13;
		goto IL_0066;
	}

IL_0044:
	{
		DataTable_t2176726999 * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = DataTable_get_TableName_m3141812994(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_16 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		uint16_t L_17 = ((uint16_t)((int32_t)95));
		Il2CppObject * L_18 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_17);
		DataTable_t2176726999 * L_19 = V_1;
		NullCheck(L_19);
		String_t* L_20 = DataTable_get_TableName_m3141812994(L_19, /*hidden argument*/NULL);
		String_t* L_21 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m2809334143(NULL /*static, unused*/, L_16, L_18, L_21, /*hidden argument*/NULL);
		G_B3_0 = L_22;
	}

IL_0066:
	{
		V_3 = G_B3_0;
		XmlSchemaElement_t471922321 * L_23 = __this->get_datasetElement_8();
		if (!L_23)
		{
			goto IL_013b;
		}
	}
	{
		RelationStructure_t3039531114 * L_24 = ___rs0;
		NullCheck(L_24);
		String_t* L_25 = L_24->get_ParentColumnName_3();
		NullCheck(L_25);
		StringU5BU5D_t2956870243* L_26 = String_Split_m290179486(L_25, (CharU5BU5D_t3416858730*)(CharU5BU5D_t3416858730*)NULL, /*hidden argument*/NULL);
		V_4 = L_26;
		RelationStructure_t3039531114 * L_27 = ___rs0;
		NullCheck(L_27);
		String_t* L_28 = L_27->get_ChildColumnName_4();
		NullCheck(L_28);
		StringU5BU5D_t2956870243* L_29 = String_Split_m290179486(L_28, (CharU5BU5D_t3416858730*)(CharU5BU5D_t3416858730*)NULL, /*hidden argument*/NULL);
		V_5 = L_29;
		StringU5BU5D_t2956870243* L_30 = V_4;
		NullCheck(L_30);
		V_6 = ((DataColumnU5BU5D_t3410743138*)SZArrayNew(DataColumnU5BU5D_t3410743138_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))))));
		V_7 = 0;
		goto IL_00c1;
	}

IL_00a1:
	{
		DataColumnU5BU5D_t3410743138* L_31 = V_6;
		int32_t L_32 = V_7;
		DataTable_t2176726999 * L_33 = V_0;
		NullCheck(L_33);
		DataColumnCollection_t3528392753 * L_34 = DataTable_get_Columns_m220042291(L_33, /*hidden argument*/NULL);
		StringU5BU5D_t2956870243* L_35 = V_4;
		int32_t L_36 = V_7;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_36);
		int32_t L_37 = L_36;
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_38 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, ((L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37))), /*hidden argument*/NULL);
		NullCheck(L_34);
		DataColumn_t3354469747 * L_39 = DataColumnCollection_get_Item_m305848743(L_34, L_38, /*hidden argument*/NULL);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		ArrayElementTypeCheck (L_31, L_39);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (DataColumn_t3354469747 *)L_39);
		int32_t L_40 = V_7;
		V_7 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_00c1:
	{
		int32_t L_41 = V_7;
		DataColumnU5BU5D_t3410743138* L_42 = V_6;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_42)->max_length)))))))
		{
			goto IL_00a1;
		}
	}
	{
		StringU5BU5D_t2956870243* L_43 = V_5;
		NullCheck(L_43);
		V_8 = ((DataColumnU5BU5D_t3410743138*)SZArrayNew(DataColumnU5BU5D_t3410743138_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_43)->max_length))))));
		V_9 = 0;
		goto IL_011a;
	}

IL_00df:
	{
		DataColumnU5BU5D_t3410743138* L_44 = V_8;
		int32_t L_45 = V_9;
		DataTable_t2176726999 * L_46 = V_1;
		NullCheck(L_46);
		DataColumnCollection_t3528392753 * L_47 = DataTable_get_Columns_m220042291(L_46, /*hidden argument*/NULL);
		StringU5BU5D_t2956870243* L_48 = V_5;
		int32_t L_49 = V_9;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_49);
		int32_t L_50 = L_49;
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_51 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, ((L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50))), /*hidden argument*/NULL);
		NullCheck(L_47);
		DataColumn_t3354469747 * L_52 = DataColumnCollection_get_Item_m305848743(L_47, L_51, /*hidden argument*/NULL);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		ArrayElementTypeCheck (L_44, L_52);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(L_45), (DataColumn_t3354469747 *)L_52);
		DataColumnU5BU5D_t3410743138* L_53 = V_8;
		int32_t L_54 = V_9;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		int32_t L_55 = L_54;
		if (((L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55))))
		{
			goto IL_0114;
		}
	}
	{
		DataColumnU5BU5D_t3410743138* L_56 = V_8;
		int32_t L_57 = V_9;
		DataColumnU5BU5D_t3410743138* L_58 = V_6;
		int32_t L_59 = V_9;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, L_59);
		int32_t L_60 = L_59;
		DataTable_t2176726999 * L_61 = V_1;
		DataColumn_t3354469747 * L_62 = XmlSchemaDataImporter_CreateChildColumn_m1891437899(__this, ((L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_60))), L_61, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		ArrayElementTypeCheck (L_56, L_62);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(L_57), (DataColumn_t3354469747 *)L_62);
	}

IL_0114:
	{
		int32_t L_63 = V_9;
		V_9 = ((int32_t)((int32_t)L_63+(int32_t)1));
	}

IL_011a:
	{
		int32_t L_64 = V_9;
		DataColumnU5BU5D_t3410743138* L_65 = V_8;
		NullCheck(L_65);
		if ((((int32_t)L_64) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_65)->max_length)))))))
		{
			goto IL_00df;
		}
	}
	{
		String_t* L_66 = V_3;
		DataColumnU5BU5D_t3410743138* L_67 = V_6;
		DataColumnU5BU5D_t3410743138* L_68 = V_8;
		RelationStructure_t3039531114 * L_69 = ___rs0;
		NullCheck(L_69);
		bool L_70 = L_69->get_CreateConstraint_6();
		DataRelation_t1483987353 * L_71 = (DataRelation_t1483987353 *)il2cpp_codegen_object_new(DataRelation_t1483987353_il2cpp_TypeInfo_var);
		DataRelation__ctor_m751722022(L_71, L_66, L_67, L_68, L_70, /*hidden argument*/NULL);
		V_2 = L_71;
		goto IL_018e;
	}

IL_013b:
	{
		DataTable_t2176726999 * L_72 = V_0;
		NullCheck(L_72);
		DataColumnCollection_t3528392753 * L_73 = DataTable_get_Columns_m220042291(L_72, /*hidden argument*/NULL);
		RelationStructure_t3039531114 * L_74 = ___rs0;
		NullCheck(L_74);
		String_t* L_75 = L_74->get_ParentColumnName_3();
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_76 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		NullCheck(L_73);
		DataColumn_t3354469747 * L_77 = DataColumnCollection_get_Item_m305848743(L_73, L_76, /*hidden argument*/NULL);
		V_10 = L_77;
		DataTable_t2176726999 * L_78 = V_1;
		NullCheck(L_78);
		DataColumnCollection_t3528392753 * L_79 = DataTable_get_Columns_m220042291(L_78, /*hidden argument*/NULL);
		RelationStructure_t3039531114 * L_80 = ___rs0;
		NullCheck(L_80);
		String_t* L_81 = L_80->get_ChildColumnName_4();
		String_t* L_82 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_81, /*hidden argument*/NULL);
		NullCheck(L_79);
		DataColumn_t3354469747 * L_83 = DataColumnCollection_get_Item_m305848743(L_79, L_82, /*hidden argument*/NULL);
		V_11 = L_83;
		DataColumn_t3354469747 * L_84 = V_11;
		if (L_84)
		{
			goto IL_017d;
		}
	}
	{
		DataColumn_t3354469747 * L_85 = V_10;
		DataTable_t2176726999 * L_86 = V_1;
		DataColumn_t3354469747 * L_87 = XmlSchemaDataImporter_CreateChildColumn_m1891437899(__this, L_85, L_86, /*hidden argument*/NULL);
		V_11 = L_87;
	}

IL_017d:
	{
		String_t* L_88 = V_3;
		DataColumn_t3354469747 * L_89 = V_10;
		DataColumn_t3354469747 * L_90 = V_11;
		RelationStructure_t3039531114 * L_91 = ___rs0;
		NullCheck(L_91);
		bool L_92 = L_91->get_CreateConstraint_6();
		DataRelation_t1483987353 * L_93 = (DataRelation_t1483987353 *)il2cpp_codegen_object_new(DataRelation_t1483987353_il2cpp_TypeInfo_var);
		DataRelation__ctor_m2003897510(L_93, L_88, L_89, L_90, L_92, /*hidden argument*/NULL);
		V_2 = L_93;
	}

IL_018e:
	{
		DataRelation_t1483987353 * L_94 = V_2;
		RelationStructure_t3039531114 * L_95 = ___rs0;
		NullCheck(L_95);
		bool L_96 = L_95->get_IsNested_5();
		NullCheck(L_94);
		VirtActionInvoker1< bool >::Invoke(9 /* System.Void System.Data.DataRelation::set_Nested(System.Boolean) */, L_94, L_96);
		RelationStructure_t3039531114 * L_97 = ___rs0;
		NullCheck(L_97);
		bool L_98 = L_97->get_CreateConstraint_6();
		if (!L_98)
		{
			goto IL_01b6;
		}
	}
	{
		DataRelation_t1483987353 * L_99 = V_2;
		NullCheck(L_99);
		DataTable_t2176726999 * L_100 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(12 /* System.Data.DataTable System.Data.DataRelation::get_ParentTable() */, L_99);
		DataRelation_t1483987353 * L_101 = V_2;
		NullCheck(L_101);
		DataColumnU5BU5D_t3410743138* L_102 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(10 /* System.Data.DataColumn[] System.Data.DataRelation::get_ParentColumns() */, L_101);
		NullCheck(L_100);
		DataTable_set_PrimaryKey_m2105023398(L_100, L_102, /*hidden argument*/NULL);
	}

IL_01b6:
	{
		DataRelation_t1483987353 * L_103 = V_2;
		return L_103;
	}
}
// System.Data.DataColumn System.Data.XmlSchemaDataImporter::CreateChildColumn(System.Data.DataColumn,System.Data.DataTable)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_CreateChildColumn_m1891437899_MetadataUsageId;
extern "C"  DataColumn_t3354469747 * XmlSchemaDataImporter_CreateChildColumn_m1891437899 (XmlSchemaDataImporter_t2811591463 * __this, DataColumn_t3354469747 * ___parentColumn0, DataTable_t2176726999 * ___childTable1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_CreateChildColumn_m1891437899_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataColumn_t3354469747 * V_0 = NULL;
	{
		DataTable_t2176726999 * L_0 = ___childTable1;
		NullCheck(L_0);
		DataColumnCollection_t3528392753 * L_1 = DataTable_get_Columns_m220042291(L_0, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_2 = ___parentColumn0;
		NullCheck(L_2);
		String_t* L_3 = DataColumn_get_ColumnName_m409531680(L_2, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_4 = ___parentColumn0;
		NullCheck(L_4);
		Type_t * L_5 = DataColumn_get_DataType_m3376662490(L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		DataColumn_t3354469747 * L_6 = DataColumnCollection_Add_m2541835761(L_1, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		DataColumn_t3354469747 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_7);
		DataColumn_set_Namespace_m2872284245(L_7, L_8, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_9 = V_0;
		NullCheck(L_9);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Data.DataColumn::set_ColumnMapping(System.Data.MappingType) */, L_9, 4);
		DataColumn_t3354469747 * L_10 = V_0;
		return L_10;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::ImportColumnGroupBase(System.Xml.Schema.XmlSchemaElement,System.Xml.Schema.XmlSchemaGroupBase)
extern Il2CppClass* XmlSchemaParticle_t3696384587_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaElement_t471922321_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaGroupBase_t3990058885_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_ImportColumnGroupBase_m637005790_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_ImportColumnGroupBase_m637005790 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaElement_t471922321 * ___parent0, XmlSchemaGroupBase_t3990058885 * ___gb1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ImportColumnGroupBase_m637005790_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlSchemaObjectEnumerator_t3058853928 * V_0 = NULL;
	XmlSchemaParticle_t3696384587 * V_1 = NULL;
	XmlSchemaElement_t471922321 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlSchemaGroupBase_t3990058885 * L_0 = ___gb1;
		NullCheck(L_0);
		XmlSchemaObjectCollection_t2238201602 * L_1 = VirtFuncInvoker0< XmlSchemaObjectCollection_t2238201602 * >::Invoke(17 /* System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaGroupBase::get_Items() */, L_0);
		NullCheck(L_1);
		XmlSchemaObjectEnumerator_t3058853928 * L_2 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004f;
		}

IL_0011:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_3 = V_0;
			NullCheck(L_3);
			XmlSchemaObject_t2900481284 * L_4 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_3, /*hidden argument*/NULL);
			V_1 = ((XmlSchemaParticle_t3696384587 *)CastclassClass(L_4, XmlSchemaParticle_t3696384587_il2cpp_TypeInfo_var));
			XmlSchemaParticle_t3696384587 * L_5 = V_1;
			V_2 = ((XmlSchemaElement_t471922321 *)IsInstClass(L_5, XmlSchemaElement_t471922321_il2cpp_TypeInfo_var));
			XmlSchemaElement_t471922321 * L_6 = V_2;
			if (!L_6)
			{
				goto IL_0037;
			}
		}

IL_002a:
		{
			XmlSchemaElement_t471922321 * L_7 = ___parent0;
			XmlSchemaElement_t471922321 * L_8 = V_2;
			XmlSchemaDataImporter_ImportColumnElement_m3899597382(__this, L_7, L_8, /*hidden argument*/NULL);
			goto IL_004f;
		}

IL_0037:
		{
			XmlSchemaParticle_t3696384587 * L_9 = V_1;
			if (!((XmlSchemaGroupBase_t3990058885 *)IsInstClass(L_9, XmlSchemaGroupBase_t3990058885_il2cpp_TypeInfo_var)))
			{
				goto IL_004f;
			}
		}

IL_0042:
		{
			XmlSchemaElement_t471922321 * L_10 = ___parent0;
			XmlSchemaParticle_t3696384587 * L_11 = V_1;
			XmlSchemaDataImporter_ImportColumnGroupBase_m637005790(__this, L_10, ((XmlSchemaGroupBase_t3990058885 *)CastclassClass(L_11, XmlSchemaGroupBase_t3990058885_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		}

IL_004f:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_12 = V_0;
			NullCheck(L_12);
			bool L_13 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_12);
			if (L_13)
			{
				goto IL_0011;
			}
		}

IL_005a:
		{
			IL2CPP_LEAVE(0x73, FINALLY_005f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_005f;
	}

FINALLY_005f:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_14 = V_0;
			Il2CppObject * L_15 = ((Il2CppObject *)IsInst(L_14, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_3 = L_15;
			if (!L_15)
			{
				goto IL_0072;
			}
		}

IL_006c:
		{
			Il2CppObject * L_16 = V_3;
			NullCheck(L_16);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_16);
		}

IL_0072:
		{
			IL2CPP_END_FINALLY(95)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(95)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0073:
	{
		return;
	}
}
// System.Xml.Schema.XmlSchemaDatatype System.Data.XmlSchemaDataImporter::GetSchemaPrimitiveType(System.Object)
extern Il2CppClass* XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaDatatype_t2590121_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaSimpleType_t1500525009_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_GetSchemaPrimitiveType_m1245543317_MetadataUsageId;
extern "C"  XmlSchemaDatatype_t2590121 * XmlSchemaDataImporter_GetSchemaPrimitiveType_m1245543317 (XmlSchemaDataImporter_t2811591463 * __this, Il2CppObject * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_GetSchemaPrimitiveType_m1245543317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlSchemaDatatype_t2590121 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___type0;
		if (!((XmlSchemaComplexType_t1860629407 *)IsInstClass(L_0, XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (XmlSchemaDatatype_t2590121 *)NULL;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___type0;
		V_0 = ((XmlSchemaDatatype_t2590121 *)IsInstClass(L_1, XmlSchemaDatatype_t2590121_il2cpp_TypeInfo_var));
		XmlSchemaDatatype_t2590121 * L_2 = V_0;
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = ___type0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_4 = ___type0;
		NullCheck(((XmlSchemaSimpleType_t1500525009 *)CastclassClass(L_4, XmlSchemaSimpleType_t1500525009_il2cpp_TypeInfo_var)));
		XmlSchemaDatatype_t2590121 * L_5 = XmlSchemaType_get_Datatype_m2483034461(((XmlSchemaSimpleType_t1500525009 *)CastclassClass(L_4, XmlSchemaSimpleType_t1500525009_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_002c:
	{
		XmlSchemaDatatype_t2590121 * L_6 = V_0;
		return L_6;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::ImportColumnAttribute(System.Xml.Schema.XmlSchemaAttribute)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* DataColumn_t3354469747_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_ImportColumnAttribute_m1880307109_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_ImportColumnAttribute_m1880307109 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaAttribute_t1191708721 * ___attr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ImportColumnAttribute_m1880307109_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataColumn_t3354469747 * V_0 = NULL;
	XmlSchemaDatatype_t2590121 * V_1 = NULL;
	{
		DataColumn_t3354469747 * L_0 = (DataColumn_t3354469747 *)il2cpp_codegen_object_new(DataColumn_t3354469747_il2cpp_TypeInfo_var);
		DataColumn__ctor_m3502306929(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		DataColumn_t3354469747 * L_1 = V_0;
		XmlSchemaAttribute_t1191708721 * L_2 = ___attr0;
		NullCheck(L_2);
		XmlQualifiedName_t176365656 * L_3 = XmlSchemaAttribute_get_QualifiedName_m3638542329(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = XmlQualifiedName_get_Name_m607016698(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		DataColumn_set_ColumnName_m68362481(L_1, L_4, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_5 = V_0;
		XmlSchemaAttribute_t1191708721 * L_6 = ___attr0;
		NullCheck(L_6);
		XmlQualifiedName_t176365656 * L_7 = XmlSchemaAttribute_get_QualifiedName_m3638542329(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = XmlQualifiedName_get_Namespace_m2987642414(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		DataColumn_set_Namespace_m2872284245(L_5, L_8, /*hidden argument*/NULL);
		V_1 = (XmlSchemaDatatype_t2590121 *)NULL;
		XmlSchemaAttribute_t1191708721 * L_9 = ___attr0;
		NullCheck(L_9);
		XmlSchemaSimpleType_t1500525009 * L_10 = XmlSchemaAttribute_get_AttributeSchemaType_m324803664(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		XmlSchemaDatatype_t2590121 * L_11 = XmlSchemaType_get_Datatype_m2483034461(L_10, /*hidden argument*/NULL);
		XmlSchemaDatatype_t2590121 * L_12 = XmlSchemaDataImporter_GetSchemaPrimitiveType_m1245543317(__this, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		DataColumn_t3354469747 * L_13 = V_0;
		XmlSchemaDatatype_t2590121 * L_14 = V_1;
		Type_t * L_15 = XmlSchemaDataImporter_ConvertDatatype_m2516858589(__this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		DataColumn_set_DataType_m1172045637(L_13, L_15, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_16 = V_0;
		NullCheck(L_16);
		Type_t * L_17 = DataColumn_get_DataType_m3376662490(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18))))
		{
			goto IL_006e;
		}
	}
	{
		DataColumn_t3354469747 * L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_19);
		DataColumn_set_DataType_m1172045637(L_19, L_20, /*hidden argument*/NULL);
	}

IL_006e:
	{
		XmlSchemaAttribute_t1191708721 * L_21 = ___attr0;
		NullCheck(L_21);
		int32_t L_22 = XmlSchemaAttribute_get_Use_m4016972027(L_21, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)2))))
		{
			goto IL_0086;
		}
	}
	{
		DataColumn_t3354469747 * L_23 = V_0;
		NullCheck(L_23);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Data.DataColumn::set_ColumnMapping(System.Data.MappingType) */, L_23, 4);
		goto IL_009a;
	}

IL_0086:
	{
		DataColumn_t3354469747 * L_24 = V_0;
		NullCheck(L_24);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Data.DataColumn::set_ColumnMapping(System.Data.MappingType) */, L_24, 2);
		DataColumn_t3354469747 * L_25 = V_0;
		XmlSchemaAttribute_t1191708721 * L_26 = ___attr0;
		Il2CppObject * L_27 = XmlSchemaDataImporter_GetAttributeDefaultValue_m1546929417(__this, L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		DataColumn_set_DefaultValue_m199373492(L_25, L_27, /*hidden argument*/NULL);
	}

IL_009a:
	{
		XmlSchemaAttribute_t1191708721 * L_28 = ___attr0;
		NullCheck(L_28);
		int32_t L_29 = XmlSchemaAttribute_get_Use_m4016972027(L_28, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_29) == ((uint32_t)3))))
		{
			goto IL_00ad;
		}
	}
	{
		DataColumn_t3354469747 * L_30 = V_0;
		NullCheck(L_30);
		DataColumn_set_AllowDBNull_m4066005655(L_30, (bool)0, /*hidden argument*/NULL);
	}

IL_00ad:
	{
		DataColumn_t3354469747 * L_31 = V_0;
		XmlSchemaAttribute_t1191708721 * L_32 = ___attr0;
		NullCheck(L_32);
		XmlSchemaSimpleType_t1500525009 * L_33 = XmlSchemaAttribute_get_AttributeSchemaType_m324803664(L_32, /*hidden argument*/NULL);
		XmlSchemaDataImporter_FillFacet_m682853069(__this, L_31, L_33, /*hidden argument*/NULL);
		XmlSchemaAttribute_t1191708721 * L_34 = ___attr0;
		XmlSchemaAttribute_t1191708721 * L_35 = ___attr0;
		NullCheck(L_35);
		XmlQualifiedName_t176365656 * L_36 = XmlSchemaAttribute_get_QualifiedName_m3638542329(L_35, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_37 = V_0;
		XmlSchemaDataImporter_ImportColumnMetaInfo_m1182641313(__this, L_34, L_36, L_37, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_38 = V_0;
		XmlSchemaDataImporter_AddColumn_m1391011057(__this, L_38, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::ImportColumnElement(System.Xml.Schema.XmlSchemaElement,System.Xml.Schema.XmlSchemaElement)
extern Il2CppClass* DataColumn_t3354469747_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t1688557254_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_ImportColumnElement_m3899597382_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_ImportColumnElement_m3899597382 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaElement_t471922321 * ___parent0, XmlSchemaElement_t471922321 * ___el1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ImportColumnElement_m3899597382_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataColumn_t3354469747 * V_0 = NULL;
	{
		DataColumn_t3354469747 * L_0 = (DataColumn_t3354469747 *)il2cpp_codegen_object_new(DataColumn_t3354469747_il2cpp_TypeInfo_var);
		DataColumn__ctor_m3502306929(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		DataColumn_t3354469747 * L_1 = V_0;
		XmlSchemaElement_t471922321 * L_2 = ___el1;
		Il2CppObject * L_3 = XmlSchemaDataImporter_GetElementDefaultValue_m3219825097(__this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		DataColumn_set_DefaultValue_m199373492(L_1, L_3, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_4 = V_0;
		XmlSchemaElement_t471922321 * L_5 = ___el1;
		NullCheck(L_5);
		Decimal_t1688557254  L_6 = XmlSchemaParticle_get_MinOccurs_m2470369063(L_5, /*hidden argument*/NULL);
		Decimal_t1688557254  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Decimal__ctor_m3224507889(&L_7, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1688557254_il2cpp_TypeInfo_var);
		bool L_8 = Decimal_op_Equality_m4013483186(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		DataColumn_set_AllowDBNull_m4066005655(L_4, L_8, /*hidden argument*/NULL);
		XmlSchemaElement_t471922321 * L_9 = ___el1;
		NullCheck(L_9);
		XmlSchemaType_t3432810239 * L_10 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_9, /*hidden argument*/NULL);
		if (!((XmlSchemaComplexType_t1860629407 *)IsInstClass(L_10, XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var)))
		{
			goto IL_0058;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_11 = ___el1;
		NullCheck(L_11);
		XmlSchemaType_t3432810239 * L_12 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		XmlSchemaComplexType_t1860629407 * L_13 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_schemaAnyType_2();
		if ((((Il2CppObject*)(XmlSchemaType_t3432810239 *)L_12) == ((Il2CppObject*)(XmlSchemaComplexType_t1860629407 *)L_13)))
		{
			goto IL_0058;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_14 = ___parent0;
		XmlSchemaElement_t471922321 * L_15 = ___el1;
		DataColumn_t3354469747 * L_16 = V_0;
		XmlSchemaDataImporter_FillDataColumnComplexElement_m3772967117(__this, L_14, L_15, L_16, /*hidden argument*/NULL);
		goto IL_0084;
	}

IL_0058:
	{
		XmlSchemaElement_t471922321 * L_17 = ___el1;
		NullCheck(L_17);
		Decimal_t1688557254  L_18 = XmlSchemaParticle_get_MaxOccurs_m3147801657(L_17, /*hidden argument*/NULL);
		Decimal_t1688557254  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Decimal__ctor_m3224507889(&L_19, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1688557254_il2cpp_TypeInfo_var);
		bool L_20 = Decimal_op_Inequality_m4269318701(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_007c;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_21 = ___parent0;
		XmlSchemaElement_t471922321 * L_22 = ___el1;
		DataColumn_t3354469747 * L_23 = V_0;
		XmlSchemaDataImporter_FillDataColumnRepeatedSimpleElement_m2259279687(__this, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_0084;
	}

IL_007c:
	{
		XmlSchemaElement_t471922321 * L_24 = ___el1;
		DataColumn_t3354469747 * L_25 = V_0;
		XmlSchemaDataImporter_FillDataColumnSimpleElement_m1238370774(__this, L_24, L_25, /*hidden argument*/NULL);
	}

IL_0084:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::ImportColumnMetaInfo(System.Xml.Schema.XmlSchemaAnnotated,System.Xml.XmlQualifiedName,System.Data.DataColumn)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlConvert_t1882388356_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3051547195;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral2217226694;
extern Il2CppCodeGenString* _stringLiteral1853714980;
extern Il2CppCodeGenString* _stringLiteral2659644000;
extern Il2CppCodeGenString* _stringLiteral1127089393;
extern Il2CppCodeGenString* _stringLiteral1127103820;
extern Il2CppCodeGenString* _stringLiteral3491927682;
extern Il2CppCodeGenString* _stringLiteral457658961;
extern const uint32_t XmlSchemaDataImporter_ImportColumnMetaInfo_m1182641313_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_ImportColumnMetaInfo_m1182641313 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaAnnotated_t2513933869 * ___obj0, XmlQualifiedName_t176365656 * ___name1, DataColumn_t3354469747 * ___col2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ImportColumnMetaInfo_m1182641313_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlAttributeU5BU5D_t656777856* V_0 = NULL;
	int32_t V_1 = 0;
	XmlAttribute_t2022155821 * V_2 = NULL;
	String_t* V_3 = NULL;
	Dictionary_2_t190145395 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		XmlSchemaAnnotated_t2513933869 * L_0 = ___obj0;
		NullCheck(L_0);
		XmlAttributeU5BU5D_t656777856* L_1 = XmlSchemaAnnotated_get_UnhandledAttributes_m155687719(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_01c3;
		}
	}
	{
		XmlSchemaAnnotated_t2513933869 * L_2 = ___obj0;
		NullCheck(L_2);
		XmlAttributeU5BU5D_t656777856* L_3 = XmlSchemaAnnotated_get_UnhandledAttributes_m155687719(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_01ba;
	}

IL_0019:
	{
		XmlAttributeU5BU5D_t656777856* L_4 = V_0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_2 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		XmlAttribute_t2022155821 * L_7 = V_2;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_8, _stringLiteral3051547195, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		DataColumn_t3354469747 * L_10 = ___col2;
		NullCheck(L_10);
		PropertyCollection_t3599376422 * L_11 = DataColumn_get_ExtendedProperties_m3441609827(L_10, /*hidden argument*/NULL);
		XmlAttribute_t2022155821 * L_12 = V_2;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(16 /* System.String System.Xml.XmlNode::get_Name() */, L_12);
		XmlAttribute_t2022155821 * L_14 = V_2;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_14);
		NullCheck(L_11);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_11, L_13, L_15);
		goto IL_01b6;
	}

IL_004e:
	{
		XmlAttribute_t2022155821 * L_16 = V_2;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_17, _stringLiteral3051173506, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0068;
		}
	}
	{
		goto IL_01b6;
	}

IL_0068:
	{
		XmlAttribute_t2022155821 * L_19 = V_2;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_19);
		V_3 = L_20;
		String_t* L_21 = V_3;
		if (!L_21)
		{
			goto IL_01b6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		Dictionary_2_t190145395 * L_22 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24mapC_14();
		if (L_22)
		{
			goto IL_00e9;
		}
	}
	{
		Dictionary_2_t190145395 * L_23 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_23, 7, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_4 = L_23;
		Dictionary_2_t190145395 * L_24 = V_4;
		NullCheck(L_24);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_24, _stringLiteral2217226694, 0);
		Dictionary_2_t190145395 * L_25 = V_4;
		NullCheck(L_25);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_25, _stringLiteral1853714980, 1);
		Dictionary_2_t190145395 * L_26 = V_4;
		NullCheck(L_26);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_26, _stringLiteral2659644000, 2);
		Dictionary_2_t190145395 * L_27 = V_4;
		NullCheck(L_27);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_27, _stringLiteral1127089393, 3);
		Dictionary_2_t190145395 * L_28 = V_4;
		NullCheck(L_28);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_28, _stringLiteral1127103820, 4);
		Dictionary_2_t190145395 * L_29 = V_4;
		NullCheck(L_29);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_29, _stringLiteral3491927682, 5);
		Dictionary_2_t190145395 * L_30 = V_4;
		NullCheck(L_30);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_30, _stringLiteral457658961, 6);
		Dictionary_2_t190145395 * L_31 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24mapC_14(L_31);
	}

IL_00e9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		Dictionary_2_t190145395 * L_32 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24mapC_14();
		String_t* L_33 = V_3;
		NullCheck(L_32);
		bool L_34 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(35 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_32, L_33, (&V_5));
		if (!L_34)
		{
			goto IL_01b6;
		}
	}
	{
		int32_t L_35 = V_5;
		if (L_35 == 0)
		{
			goto IL_0123;
		}
		if (L_35 == 1)
		{
			goto IL_0134;
		}
		if (L_35 == 2)
		{
			goto IL_014a;
		}
		if (L_35 == 3)
		{
			goto IL_0160;
		}
		if (L_35 == 4)
		{
			goto IL_0177;
		}
		if (L_35 == 5)
		{
			goto IL_018e;
		}
		if (L_35 == 6)
		{
			goto IL_01a4;
		}
	}
	{
		goto IL_01b6;
	}

IL_0123:
	{
		DataColumn_t3354469747 * L_36 = ___col2;
		XmlAttribute_t2022155821 * L_37 = V_2;
		NullCheck(L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_37);
		NullCheck(L_36);
		DataColumn_set_Caption_m3162200106(L_36, L_38, /*hidden argument*/NULL);
		goto IL_01b6;
	}

IL_0134:
	{
		DataColumn_t3354469747 * L_39 = ___col2;
		XmlAttribute_t2022155821 * L_40 = V_2;
		NullCheck(L_40);
		String_t* L_41 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_40);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m2877589631, L_41, "System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
		NullCheck(L_39);
		DataColumn_set_DataType_m1172045637(L_39, L_42, /*hidden argument*/NULL);
		goto IL_01b6;
	}

IL_014a:
	{
		DataColumn_t3354469747 * L_43 = ___col2;
		XmlAttribute_t2022155821 * L_44 = V_2;
		NullCheck(L_44);
		String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_44);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t211005341_il2cpp_TypeInfo_var);
		bool L_46 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		NullCheck(L_43);
		DataColumn_set_AutoIncrement_m2609925417(L_43, L_46, /*hidden argument*/NULL);
		goto IL_01b6;
	}

IL_0160:
	{
		DataColumn_t3354469747 * L_47 = ___col2;
		XmlAttribute_t2022155821 * L_48 = V_2;
		NullCheck(L_48);
		String_t* L_49 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_48);
		int32_t L_50 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		NullCheck(L_47);
		DataColumn_set_AutoIncrementSeed_m2380713301(L_47, (((int64_t)((int64_t)L_50))), /*hidden argument*/NULL);
		goto IL_01b6;
	}

IL_0177:
	{
		DataColumn_t3354469747 * L_51 = ___col2;
		XmlAttribute_t2022155821 * L_52 = V_2;
		NullCheck(L_52);
		String_t* L_53 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_52);
		int32_t L_54 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		NullCheck(L_51);
		DataColumn_set_AutoIncrementStep_m2116274800(L_51, (((int64_t)((int64_t)L_54))), /*hidden argument*/NULL);
		goto IL_01b6;
	}

IL_018e:
	{
		DataColumn_t3354469747 * L_55 = ___col2;
		XmlAttribute_t2022155821 * L_56 = V_2;
		NullCheck(L_56);
		String_t* L_57 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_56);
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		bool L_58 = XmlConvert_ToBoolean_m3758854944(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		NullCheck(L_55);
		DataColumn_set_ReadOnly_m897759881(L_55, L_58, /*hidden argument*/NULL);
		goto IL_01b6;
	}

IL_01a4:
	{
		XmlAttribute_t2022155821 * L_59 = V_2;
		NullCheck(L_59);
		String_t* L_60 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_59);
		int32_t L_61 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		V_6 = L_61;
		goto IL_01b6;
	}

IL_01b6:
	{
		int32_t L_62 = V_1;
		V_1 = ((int32_t)((int32_t)L_62+(int32_t)1));
	}

IL_01ba:
	{
		int32_t L_63 = V_1;
		XmlAttributeU5BU5D_t656777856* L_64 = V_0;
		NullCheck(L_64);
		if ((((int32_t)L_63) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_64)->max_length)))))))
		{
			goto IL_0019;
		}
	}

IL_01c3:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::FillDataColumnComplexElement(System.Xml.Schema.XmlSchemaElement,System.Xml.Schema.XmlSchemaElement,System.Data.DataColumn)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppClass* RelationStructure_t3039531114_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlQualifiedName_t176365656_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3401530436;
extern const uint32_t XmlSchemaDataImporter_FillDataColumnComplexElement_m3772967117_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_FillDataColumnComplexElement_m3772967117 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaElement_t471922321 * ___parent0, XmlSchemaElement_t471922321 * ___el1, DataColumn_t3354469747 * ___col2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_FillDataColumnComplexElement_m3772967117_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	RelationStructure_t3039531114 * V_1 = NULL;
	{
		ArrayList_t2121638921 * L_0 = __this->get_targetElements_10();
		XmlSchemaElement_t471922321 * L_1 = ___el1;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(32 /* System.Boolean System.Collections.ArrayList::Contains(System.Object) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		XmlSchemaElement_t471922321 * L_3 = ___el1;
		NullCheck(L_3);
		XmlQualifiedName_t176365656 * L_4 = XmlSchemaElement_get_QualifiedName_m2908521753(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = XmlQualifiedName_get_Name_m607016698(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_6 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7 = V_0;
		DataSet_t3654702571 * L_8 = __this->get_dataset_3();
		NullCheck(L_8);
		String_t* L_9 = DataSet_get_DataSetName_m1556380856(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0044;
		}
	}
	{
		ArgumentException_t124305799 * L_11 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_11, _stringLiteral3401530436, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0044:
	{
		XmlSchemaElement_t471922321 * L_12 = ___el1;
		NullCheck(L_12);
		XmlSchemaAnnotation_t1377046772 * L_13 = XmlSchemaAnnotated_get_Annotation_m1267520785(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0061;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_14 = ___el1;
		NullCheck(L_14);
		XmlSchemaAnnotation_t1377046772 * L_15 = XmlSchemaAnnotated_get_Annotation_m1267520785(L_14, /*hidden argument*/NULL);
		XmlSchemaDataImporter_HandleAnnotations_m996806934(__this, L_15, (bool)1, /*hidden argument*/NULL);
		goto IL_00cc;
	}

IL_0061:
	{
		String_t* L_16 = V_0;
		bool L_17 = XmlSchemaDataImporter_DataSetDefinesKey_m3193089178(__this, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00cc;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_18 = ___parent0;
		XmlSchemaElement_t471922321 * L_19 = ___el1;
		DataColumn_t3354469747 * L_20 = ___col2;
		XmlSchemaDataImporter_AddParentKeyColumn_m3337141160(__this, L_18, L_19, L_20, /*hidden argument*/NULL);
		RelationStructure_t3039531114 * L_21 = (RelationStructure_t3039531114 *)il2cpp_codegen_object_new(RelationStructure_t3039531114_il2cpp_TypeInfo_var);
		RelationStructure__ctor_m1596628542(L_21, /*hidden argument*/NULL);
		V_1 = L_21;
		RelationStructure_t3039531114 * L_22 = V_1;
		XmlSchemaElement_t471922321 * L_23 = ___parent0;
		NullCheck(L_23);
		XmlQualifiedName_t176365656 * L_24 = XmlSchemaElement_get_QualifiedName_m2908521753(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		String_t* L_25 = XmlQualifiedName_get_Name_m607016698(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_26 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		NullCheck(L_22);
		L_22->set_ParentTableName_1(L_26);
		RelationStructure_t3039531114 * L_27 = V_1;
		String_t* L_28 = V_0;
		NullCheck(L_27);
		L_27->set_ChildTableName_2(L_28);
		RelationStructure_t3039531114 * L_29 = V_1;
		DataColumn_t3354469747 * L_30 = ___col2;
		NullCheck(L_30);
		String_t* L_31 = DataColumn_get_ColumnName_m409531680(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		L_29->set_ParentColumnName_3(L_31);
		RelationStructure_t3039531114 * L_32 = V_1;
		DataColumn_t3354469747 * L_33 = ___col2;
		NullCheck(L_33);
		String_t* L_34 = DataColumn_get_ColumnName_m409531680(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		L_32->set_ChildColumnName_4(L_34);
		RelationStructure_t3039531114 * L_35 = V_1;
		NullCheck(L_35);
		L_35->set_CreateConstraint_6((bool)1);
		RelationStructure_t3039531114 * L_36 = V_1;
		NullCheck(L_36);
		L_36->set_IsNested_5((bool)1);
		ArrayList_t2121638921 * L_37 = __this->get_relations_6();
		RelationStructure_t3039531114 * L_38 = V_1;
		NullCheck(L_37);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_37, L_38);
	}

IL_00cc:
	{
		XmlSchemaElement_t471922321 * L_39 = ___el1;
		NullCheck(L_39);
		XmlQualifiedName_t176365656 * L_40 = XmlSchemaElement_get_RefName_m3872471184(L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_41 = ((XmlQualifiedName_t176365656_StaticFields*)XmlQualifiedName_t176365656_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		bool L_42 = XmlQualifiedName_op_Equality_m273752697(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_00e8;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_43 = ___el1;
		XmlSchemaDataImporter_ProcessDataTableElement_m1241068523(__this, L_43, /*hidden argument*/NULL);
	}

IL_00e8:
	{
		return;
	}
}
// System.Boolean System.Data.XmlSchemaDataImporter::DataSetDefinesKey(System.String)
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* ConstraintStructure_t742574505_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_DataSetDefinesKey_m3193089178_MetadataUsageId;
extern "C"  bool XmlSchemaDataImporter_DataSetDefinesKey_m3193089178 (XmlSchemaDataImporter_t2811591463 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_DataSetDefinesKey_m3193089178_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ConstraintStructure_t742574505 * V_1 = NULL;
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Hashtable_t3875263730 * L_0 = __this->get_reservedConstraints_7();
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(25 /* System.Collections.ICollection System.Collections.Hashtable::get_Values() */, L_0);
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_1);
		V_0 = L_2;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0050;
		}

IL_0016:
		{
			Il2CppObject * L_3 = V_0;
			NullCheck(L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_3);
			V_1 = ((ConstraintStructure_t742574505 *)CastclassClass(L_4, ConstraintStructure_t742574505_il2cpp_TypeInfo_var));
			ConstraintStructure_t742574505 * L_5 = V_1;
			NullCheck(L_5);
			String_t* L_6 = L_5->get_TableName_0();
			String_t* L_7 = ___name0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_8 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_0050;
			}
		}

IL_0033:
		{
			ConstraintStructure_t742574505 * L_9 = V_1;
			NullCheck(L_9);
			bool L_10 = L_9->get_IsPrimaryKey_4();
			if (L_10)
			{
				goto IL_0049;
			}
		}

IL_003e:
		{
			ConstraintStructure_t742574505 * L_11 = V_1;
			NullCheck(L_11);
			bool L_12 = L_11->get_IsNested_6();
			if (!L_12)
			{
				goto IL_0050;
			}
		}

IL_0049:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x76, FINALLY_0060);
		}

IL_0050:
		{
			Il2CppObject * L_13 = V_0;
			NullCheck(L_13);
			bool L_14 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_13);
			if (L_14)
			{
				goto IL_0016;
			}
		}

IL_005b:
		{
			IL2CPP_LEAVE(0x74, FINALLY_0060);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_15 = V_0;
			Il2CppObject * L_16 = ((Il2CppObject *)IsInst(L_15, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_3 = L_16;
			if (!L_16)
			{
				goto IL_0073;
			}
		}

IL_006d:
		{
			Il2CppObject * L_17 = V_3;
			NullCheck(L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_17);
		}

IL_0073:
		{
			IL2CPP_END_FINALLY(96)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x76, IL_0076)
		IL2CPP_JUMP_TBL(0x74, IL_0074)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0074:
	{
		return (bool)0;
	}

IL_0076:
	{
		bool L_18 = V_2;
		return L_18;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::AddParentKeyColumn(System.Xml.Schema.XmlSchemaElement,System.Xml.Schema.XmlSchemaElement,System.Data.DataColumn)
extern const Il2CppType* Int32_t2847414787_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DataException_t1022144856_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral491254890;
extern Il2CppCodeGenString* _stringLiteral93658;
extern Il2CppCodeGenString* _stringLiteral3281359728;
extern const uint32_t XmlSchemaDataImporter_AddParentKeyColumn_m3337141160_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_AddParentKeyColumn_m3337141160 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaElement_t471922321 * ___parent0, XmlSchemaElement_t471922321 * ___el1, DataColumn_t3354469747 * ___col2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_AddParentKeyColumn_m3337141160_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		TableStructure_t1082771224 * L_0 = __this->get_currentTable_11();
		NullCheck(L_0);
		DataTable_t2176726999 * L_1 = L_0->get_Table_0();
		NullCheck(L_1);
		DataColumnU5BU5D_t3410743138* L_2 = DataTable_get_PrimaryKey_m478420397(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		TableStructure_t1082771224 * L_3 = __this->get_currentTable_11();
		NullCheck(L_3);
		DataTable_t2176726999 * L_4 = L_3->get_Table_0();
		NullCheck(L_4);
		String_t* L_5 = DataTable_get_TableName_m3141812994(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral491254890, L_5, /*hidden argument*/NULL);
		DataException_t1022144856 * L_7 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0038:
	{
		TableStructure_t1082771224 * L_8 = __this->get_currentTable_11();
		NullCheck(L_8);
		DataColumn_t3354469747 * L_9 = L_8->get_PrimaryKey_3();
		if (!L_9)
		{
			goto IL_00db;
		}
	}
	{
		DataColumn_t3354469747 * L_10 = ___col2;
		TableStructure_t1082771224 * L_11 = __this->get_currentTable_11();
		NullCheck(L_11);
		DataColumn_t3354469747 * L_12 = L_11->get_PrimaryKey_3();
		NullCheck(L_12);
		String_t* L_13 = DataColumn_get_ColumnName_m409531680(L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		DataColumn_set_ColumnName_m68362481(L_10, L_13, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_14 = ___col2;
		TableStructure_t1082771224 * L_15 = __this->get_currentTable_11();
		NullCheck(L_15);
		DataColumn_t3354469747 * L_16 = L_15->get_PrimaryKey_3();
		NullCheck(L_16);
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_16);
		NullCheck(L_14);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Data.DataColumn::set_ColumnMapping(System.Data.MappingType) */, L_14, L_17);
		DataColumn_t3354469747 * L_18 = ___col2;
		TableStructure_t1082771224 * L_19 = __this->get_currentTable_11();
		NullCheck(L_19);
		DataColumn_t3354469747 * L_20 = L_19->get_PrimaryKey_3();
		NullCheck(L_20);
		String_t* L_21 = DataColumn_get_Namespace_m546686590(L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		DataColumn_set_Namespace_m2872284245(L_18, L_21, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_22 = ___col2;
		TableStructure_t1082771224 * L_23 = __this->get_currentTable_11();
		NullCheck(L_23);
		DataColumn_t3354469747 * L_24 = L_23->get_PrimaryKey_3();
		NullCheck(L_24);
		Type_t * L_25 = DataColumn_get_DataType_m3376662490(L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		DataColumn_set_DataType_m1172045637(L_22, L_25, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_26 = ___col2;
		TableStructure_t1082771224 * L_27 = __this->get_currentTable_11();
		NullCheck(L_27);
		DataColumn_t3354469747 * L_28 = L_27->get_PrimaryKey_3();
		NullCheck(L_28);
		bool L_29 = DataColumn_get_AutoIncrement_m3258447900(L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		DataColumn_set_AutoIncrement_m2609925417(L_26, L_29, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_30 = ___col2;
		TableStructure_t1082771224 * L_31 = __this->get_currentTable_11();
		NullCheck(L_31);
		DataColumn_t3354469747 * L_32 = L_31->get_PrimaryKey_3();
		NullCheck(L_32);
		bool L_33 = DataColumn_get_AllowDBNull_m4042991754(L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		DataColumn_set_AllowDBNull_m4066005655(L_30, L_33, /*hidden argument*/NULL);
		XmlSchemaElement_t471922321 * L_34 = ___el1;
		XmlSchemaElement_t471922321 * L_35 = ___el1;
		NullCheck(L_35);
		XmlQualifiedName_t176365656 * L_36 = XmlSchemaElement_get_QualifiedName_m2908521753(L_35, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_37 = ___col2;
		XmlSchemaDataImporter_ImportColumnMetaInfo_m1182641313(__this, L_34, L_36, L_37, /*hidden argument*/NULL);
		return;
	}

IL_00db:
	{
		XmlSchemaElement_t471922321 * L_38 = ___parent0;
		NullCheck(L_38);
		XmlQualifiedName_t176365656 * L_39 = XmlSchemaElement_get_QualifiedName_m2908521753(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		String_t* L_40 = XmlQualifiedName_get_Name_m607016698(L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_41 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_42 = String_Concat_m138640077(NULL /*static, unused*/, L_41, _stringLiteral93658, /*hidden argument*/NULL);
		V_0 = L_42;
		V_1 = 0;
		goto IL_0113;
	}

IL_00fd:
	{
		String_t* L_43 = V_0;
		int32_t L_44 = V_1;
		int32_t L_45 = L_44;
		V_1 = ((int32_t)((int32_t)L_45+(int32_t)1));
		int32_t L_46 = L_45;
		Il2CppObject * L_47 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_46);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral3281359728, L_43, L_47, /*hidden argument*/NULL);
		V_0 = L_48;
	}

IL_0113:
	{
		TableStructure_t1082771224 * L_49 = __this->get_currentTable_11();
		String_t* L_50 = V_0;
		NullCheck(L_49);
		bool L_51 = TableStructure_ContainsColumn_m123918059(L_49, L_50, /*hidden argument*/NULL);
		if (L_51)
		{
			goto IL_00fd;
		}
	}
	{
		DataColumn_t3354469747 * L_52 = ___col2;
		String_t* L_53 = V_0;
		NullCheck(L_52);
		DataColumn_set_ColumnName_m68362481(L_52, L_53, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_54 = ___col2;
		NullCheck(L_54);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Data.DataColumn::set_ColumnMapping(System.Data.MappingType) */, L_54, 4);
		DataColumn_t3354469747 * L_55 = ___col2;
		XmlSchemaElement_t471922321 * L_56 = ___parent0;
		NullCheck(L_56);
		XmlQualifiedName_t176365656 * L_57 = XmlSchemaElement_get_QualifiedName_m2908521753(L_56, /*hidden argument*/NULL);
		NullCheck(L_57);
		String_t* L_58 = XmlQualifiedName_get_Namespace_m2987642414(L_57, /*hidden argument*/NULL);
		NullCheck(L_55);
		DataColumn_set_Namespace_m2872284245(L_55, L_58, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_59 = ___col2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_60 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t2847414787_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_59);
		DataColumn_set_DataType_m1172045637(L_59, L_60, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_61 = ___col2;
		NullCheck(L_61);
		DataColumn_set_AutoIncrement_m2609925417(L_61, (bool)1, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_62 = ___col2;
		NullCheck(L_62);
		DataColumn_set_AllowDBNull_m4066005655(L_62, (bool)0, /*hidden argument*/NULL);
		XmlSchemaElement_t471922321 * L_63 = ___el1;
		XmlSchemaElement_t471922321 * L_64 = ___el1;
		NullCheck(L_64);
		XmlQualifiedName_t176365656 * L_65 = XmlSchemaElement_get_QualifiedName_m2908521753(L_64, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_66 = ___col2;
		XmlSchemaDataImporter_ImportColumnMetaInfo_m1182641313(__this, L_63, L_65, L_66, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_67 = ___col2;
		XmlSchemaDataImporter_AddColumn_m1391011057(__this, L_67, /*hidden argument*/NULL);
		TableStructure_t1082771224 * L_68 = __this->get_currentTable_11();
		DataColumn_t3354469747 * L_69 = ___col2;
		NullCheck(L_68);
		L_68->set_PrimaryKey_3(L_69);
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::FillDataColumnRepeatedSimpleElement(System.Xml.Schema.XmlSchemaElement,System.Xml.Schema.XmlSchemaElement,System.Data.DataColumn)
extern const Il2CppType* Int32_t2847414787_0_0_0_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* DataTable_t2176726999_il2cpp_TypeInfo_var;
extern Il2CppClass* DataColumn_t3354469747_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* RelationStructure_t3039531114_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93658;
extern Il2CppCodeGenString* _stringLiteral437501077;
extern const uint32_t XmlSchemaDataImporter_FillDataColumnRepeatedSimpleElement_m2259279687_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_FillDataColumnRepeatedSimpleElement_m2259279687 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaElement_t471922321 * ___parent0, XmlSchemaElement_t471922321 * ___el1, DataColumn_t3354469747 * ___col2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_FillDataColumnRepeatedSimpleElement_m2259279687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataColumn_t3354469747 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	DataTable_t2176726999 * V_3 = NULL;
	DataColumn_t3354469747 * V_4 = NULL;
	DataColumn_t3354469747 * V_5 = NULL;
	RelationStructure_t3039531114 * V_6 = NULL;
	{
		ArrayList_t2121638921 * L_0 = __this->get_targetElements_10();
		XmlSchemaElement_t471922321 * L_1 = ___el1;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(32 /* System.Boolean System.Collections.ArrayList::Contains(System.Object) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		XmlSchemaElement_t471922321 * L_3 = ___parent0;
		XmlSchemaElement_t471922321 * L_4 = ___el1;
		DataColumn_t3354469747 * L_5 = ___col2;
		XmlSchemaDataImporter_AddParentKeyColumn_m3337141160(__this, L_3, L_4, L_5, /*hidden argument*/NULL);
		TableStructure_t1082771224 * L_6 = __this->get_currentTable_11();
		NullCheck(L_6);
		DataColumn_t3354469747 * L_7 = L_6->get_PrimaryKey_3();
		V_0 = L_7;
		XmlSchemaElement_t471922321 * L_8 = ___el1;
		NullCheck(L_8);
		XmlQualifiedName_t176365656 * L_9 = XmlSchemaElement_get_QualifiedName_m2908521753(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = XmlQualifiedName_get_Name_m607016698(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_11 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		XmlSchemaElement_t471922321 * L_12 = ___parent0;
		NullCheck(L_12);
		XmlQualifiedName_t176365656 * L_13 = XmlSchemaElement_get_QualifiedName_m2908521753(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		String_t* L_14 = XmlQualifiedName_get_Name_m607016698(L_13, /*hidden argument*/NULL);
		String_t* L_15 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		DataTable_t2176726999 * L_16 = (DataTable_t2176726999 *)il2cpp_codegen_object_new(DataTable_t2176726999_il2cpp_TypeInfo_var);
		DataTable__ctor_m798602097(L_16, /*hidden argument*/NULL);
		V_3 = L_16;
		DataTable_t2176726999 * L_17 = V_3;
		String_t* L_18 = V_1;
		NullCheck(L_17);
		DataTable_set_TableName_m3787716983(L_17, L_18, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_19 = V_3;
		XmlSchemaElement_t471922321 * L_20 = ___el1;
		NullCheck(L_20);
		XmlQualifiedName_t176365656 * L_21 = XmlSchemaElement_get_QualifiedName_m2908521753(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_22 = XmlQualifiedName_get_Namespace_m2987642414(L_21, /*hidden argument*/NULL);
		NullCheck(L_19);
		DataTable_set_Namespace_m1042478933(L_19, L_22, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_23 = (DataColumn_t3354469747 *)il2cpp_codegen_object_new(DataColumn_t3354469747_il2cpp_TypeInfo_var);
		DataColumn__ctor_m3502306929(L_23, /*hidden argument*/NULL);
		V_4 = L_23;
		DataColumn_t3354469747 * L_24 = V_4;
		String_t* L_25 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m138640077(NULL /*static, unused*/, L_25, _stringLiteral93658, /*hidden argument*/NULL);
		NullCheck(L_24);
		DataColumn_set_ColumnName_m68362481(L_24, L_26, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_27 = V_4;
		XmlSchemaElement_t471922321 * L_28 = ___parent0;
		NullCheck(L_28);
		XmlQualifiedName_t176365656 * L_29 = XmlSchemaElement_get_QualifiedName_m2908521753(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		String_t* L_30 = XmlQualifiedName_get_Namespace_m2987642414(L_29, /*hidden argument*/NULL);
		NullCheck(L_27);
		DataColumn_set_Namespace_m2872284245(L_27, L_30, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_31 = V_4;
		NullCheck(L_31);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Data.DataColumn::set_ColumnMapping(System.Data.MappingType) */, L_31, 4);
		DataColumn_t3354469747 * L_32 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t2847414787_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_32);
		DataColumn_set_DataType_m1172045637(L_32, L_33, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_34 = (DataColumn_t3354469747 *)il2cpp_codegen_object_new(DataColumn_t3354469747_il2cpp_TypeInfo_var);
		DataColumn__ctor_m3502306929(L_34, /*hidden argument*/NULL);
		V_5 = L_34;
		DataColumn_t3354469747 * L_35 = V_5;
		String_t* L_36 = V_1;
		String_t* L_37 = String_Concat_m138640077(NULL /*static, unused*/, L_36, _stringLiteral437501077, /*hidden argument*/NULL);
		NullCheck(L_35);
		DataColumn_set_ColumnName_m68362481(L_35, L_37, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_38 = V_5;
		XmlSchemaElement_t471922321 * L_39 = ___el1;
		NullCheck(L_39);
		XmlQualifiedName_t176365656 * L_40 = XmlSchemaElement_get_QualifiedName_m2908521753(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		String_t* L_41 = XmlQualifiedName_get_Namespace_m2987642414(L_40, /*hidden argument*/NULL);
		NullCheck(L_38);
		DataColumn_set_Namespace_m2872284245(L_38, L_41, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_42 = V_5;
		NullCheck(L_42);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Data.DataColumn::set_ColumnMapping(System.Data.MappingType) */, L_42, 3);
		DataColumn_t3354469747 * L_43 = V_5;
		NullCheck(L_43);
		DataColumn_set_AllowDBNull_m4066005655(L_43, (bool)0, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_44 = V_5;
		XmlSchemaElement_t471922321 * L_45 = ___el1;
		NullCheck(L_45);
		XmlSchemaType_t3432810239 * L_46 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_45, /*hidden argument*/NULL);
		XmlSchemaDatatype_t2590121 * L_47 = XmlSchemaDataImporter_GetSchemaPrimitiveType_m1245543317(__this, L_46, /*hidden argument*/NULL);
		Type_t * L_48 = XmlSchemaDataImporter_ConvertDatatype_m2516858589(__this, L_47, /*hidden argument*/NULL);
		NullCheck(L_44);
		DataColumn_set_DataType_m1172045637(L_44, L_48, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_49 = V_3;
		NullCheck(L_49);
		DataColumnCollection_t3528392753 * L_50 = DataTable_get_Columns_m220042291(L_49, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_51 = V_5;
		NullCheck(L_50);
		DataColumnCollection_Add_m3379764877(L_50, L_51, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_52 = V_3;
		NullCheck(L_52);
		DataColumnCollection_t3528392753 * L_53 = DataTable_get_Columns_m220042291(L_52, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_54 = V_4;
		NullCheck(L_53);
		DataColumnCollection_Add_m3379764877(L_53, L_54, /*hidden argument*/NULL);
		DataSet_t3654702571 * L_55 = __this->get_dataset_3();
		NullCheck(L_55);
		DataTableCollection_t2915263893 * L_56 = DataSet_get_Tables_m87321279(L_55, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_57 = V_3;
		NullCheck(L_56);
		DataTableCollection_Add_m3310506721(L_56, L_57, /*hidden argument*/NULL);
		RelationStructure_t3039531114 * L_58 = (RelationStructure_t3039531114 *)il2cpp_codegen_object_new(RelationStructure_t3039531114_il2cpp_TypeInfo_var);
		RelationStructure__ctor_m1596628542(L_58, /*hidden argument*/NULL);
		V_6 = L_58;
		RelationStructure_t3039531114 * L_59 = V_6;
		String_t* L_60 = V_2;
		NullCheck(L_59);
		L_59->set_ParentTableName_1(L_60);
		RelationStructure_t3039531114 * L_61 = V_6;
		DataTable_t2176726999 * L_62 = V_3;
		NullCheck(L_62);
		String_t* L_63 = DataTable_get_TableName_m3141812994(L_62, /*hidden argument*/NULL);
		NullCheck(L_61);
		L_61->set_ChildTableName_2(L_63);
		RelationStructure_t3039531114 * L_64 = V_6;
		DataColumn_t3354469747 * L_65 = V_0;
		NullCheck(L_65);
		String_t* L_66 = DataColumn_get_ColumnName_m409531680(L_65, /*hidden argument*/NULL);
		NullCheck(L_64);
		L_64->set_ParentColumnName_3(L_66);
		RelationStructure_t3039531114 * L_67 = V_6;
		DataColumn_t3354469747 * L_68 = V_4;
		NullCheck(L_68);
		String_t* L_69 = DataColumn_get_ColumnName_m409531680(L_68, /*hidden argument*/NULL);
		NullCheck(L_67);
		L_67->set_ChildColumnName_4(L_69);
		RelationStructure_t3039531114 * L_70 = V_6;
		NullCheck(L_70);
		L_70->set_IsNested_5((bool)1);
		RelationStructure_t3039531114 * L_71 = V_6;
		NullCheck(L_71);
		L_71->set_CreateConstraint_6((bool)1);
		ArrayList_t2121638921 * L_72 = __this->get_relations_6();
		RelationStructure_t3039531114 * L_73 = V_6;
		NullCheck(L_72);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_72, L_73);
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::FillDataColumnSimpleElement(System.Xml.Schema.XmlSchemaElement,System.Data.DataColumn)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaSimpleType_t1500525009_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_FillDataColumnSimpleElement_m1238370774_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_FillDataColumnSimpleElement_m1238370774 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaElement_t471922321 * ___el0, DataColumn_t3354469747 * ___col1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_FillDataColumnSimpleElement_m1238370774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DataColumn_t3354469747 * L_0 = ___col1;
		XmlSchemaElement_t471922321 * L_1 = ___el0;
		NullCheck(L_1);
		XmlQualifiedName_t176365656 * L_2 = XmlSchemaElement_get_QualifiedName_m2908521753(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = XmlQualifiedName_get_Name_m607016698(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_4 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		DataColumn_set_ColumnName_m68362481(L_0, L_4, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_5 = ___col1;
		XmlSchemaElement_t471922321 * L_6 = ___el0;
		NullCheck(L_6);
		XmlQualifiedName_t176365656 * L_7 = XmlSchemaElement_get_QualifiedName_m2908521753(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = XmlQualifiedName_get_Namespace_m2987642414(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		DataColumn_set_Namespace_m2872284245(L_5, L_8, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_9 = ___col1;
		NullCheck(L_9);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Data.DataColumn::set_ColumnMapping(System.Data.MappingType) */, L_9, 1);
		DataColumn_t3354469747 * L_10 = ___col1;
		XmlSchemaElement_t471922321 * L_11 = ___el0;
		NullCheck(L_11);
		XmlSchemaType_t3432810239 * L_12 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_11, /*hidden argument*/NULL);
		XmlSchemaDatatype_t2590121 * L_13 = XmlSchemaDataImporter_GetSchemaPrimitiveType_m1245543317(__this, L_12, /*hidden argument*/NULL);
		Type_t * L_14 = XmlSchemaDataImporter_ConvertDatatype_m2516858589(__this, L_13, /*hidden argument*/NULL);
		NullCheck(L_10);
		DataColumn_set_DataType_m1172045637(L_10, L_14, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_15 = ___col1;
		XmlSchemaElement_t471922321 * L_16 = ___el0;
		NullCheck(L_16);
		XmlSchemaType_t3432810239 * L_17 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_16, /*hidden argument*/NULL);
		XmlSchemaDataImporter_FillFacet_m682853069(__this, L_15, ((XmlSchemaSimpleType_t1500525009 *)IsInstClass(L_17, XmlSchemaSimpleType_t1500525009_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		XmlSchemaElement_t471922321 * L_18 = ___el0;
		XmlSchemaElement_t471922321 * L_19 = ___el0;
		NullCheck(L_19);
		XmlQualifiedName_t176365656 * L_20 = XmlSchemaElement_get_QualifiedName_m2908521753(L_19, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_21 = ___col1;
		XmlSchemaDataImporter_ImportColumnMetaInfo_m1182641313(__this, L_18, L_20, L_21, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_22 = ___col1;
		XmlSchemaDataImporter_AddColumn_m1391011057(__this, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::AddColumn(System.Data.DataColumn)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_AddColumn_m1391011057_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_AddColumn_m1391011057 (XmlSchemaDataImporter_t2811591463 * __this, DataColumn_t3354469747 * ___col0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_AddColumn_m1391011057_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DataColumn_t3354469747 * L_0 = ___col0;
		NullCheck(L_0);
		int32_t L_1 = DataColumn_get_Ordinal_m317938227(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		TableStructure_t1082771224 * L_2 = __this->get_currentTable_11();
		NullCheck(L_2);
		ArrayList_t2121638921 * L_3 = L_2->get_NonOrdinalColumns_2();
		DataColumn_t3354469747 * L_4 = ___col0;
		NullCheck(L_3);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_3, L_4);
		goto IL_003f;
	}

IL_0023:
	{
		TableStructure_t1082771224 * L_5 = __this->get_currentTable_11();
		NullCheck(L_5);
		Hashtable_t3875263730 * L_6 = L_5->get_OrdinalColumns_1();
		DataColumn_t3354469747 * L_7 = ___col0;
		DataColumn_t3354469747 * L_8 = ___col0;
		NullCheck(L_8);
		int32_t L_9 = DataColumn_get_Ordinal_m317938227(L_8, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_6);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_6, L_7, L_11);
	}

IL_003f:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::FillFacet(System.Data.DataColumn,System.Xml.Schema.XmlSchemaSimpleType)
extern Il2CppClass* XmlSchemaSimpleTypeRestriction_t409858485_il2cpp_TypeInfo_var;
extern Il2CppClass* DataException_t1022144856_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaFacet_t4078749516_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaMaxLengthFacet_t2333687858_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1337087299;
extern const uint32_t XmlSchemaDataImporter_FillFacet_m682853069_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_FillFacet_m682853069 (XmlSchemaDataImporter_t2811591463 * __this, DataColumn_t3354469747 * ___col0, XmlSchemaSimpleType_t1500525009 * ___st1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_FillFacet_m682853069_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlSchemaSimpleTypeRestriction_t409858485 * V_0 = NULL;
	XmlSchemaObjectEnumerator_t3058853928 * V_1 = NULL;
	XmlSchemaFacet_t4078749516 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	XmlSchemaSimpleTypeRestriction_t409858485 * G_B6_0 = NULL;
	{
		XmlSchemaSimpleType_t1500525009 * L_0 = ___st1;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		XmlSchemaSimpleType_t1500525009 * L_1 = ___st1;
		NullCheck(L_1);
		XmlSchemaSimpleTypeContent_t1763168354 * L_2 = XmlSchemaSimpleType_get_Content_m1777111111(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0012;
		}
	}

IL_0011:
	{
		return;
	}

IL_0012:
	{
		XmlSchemaSimpleType_t1500525009 * L_3 = ___st1;
		if (L_3)
		{
			goto IL_001e;
		}
	}
	{
		G_B6_0 = ((XmlSchemaSimpleTypeRestriction_t409858485 *)(NULL));
		goto IL_0029;
	}

IL_001e:
	{
		XmlSchemaSimpleType_t1500525009 * L_4 = ___st1;
		NullCheck(L_4);
		XmlSchemaSimpleTypeContent_t1763168354 * L_5 = XmlSchemaSimpleType_get_Content_m1777111111(L_4, /*hidden argument*/NULL);
		G_B6_0 = ((XmlSchemaSimpleTypeRestriction_t409858485 *)IsInstClass(L_5, XmlSchemaSimpleTypeRestriction_t409858485_il2cpp_TypeInfo_var));
	}

IL_0029:
	{
		V_0 = G_B6_0;
		XmlSchemaSimpleTypeRestriction_t409858485 * L_6 = V_0;
		if (L_6)
		{
			goto IL_003b;
		}
	}
	{
		DataException_t1022144856 * L_7 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_7, _stringLiteral1337087299, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003b:
	{
		XmlSchemaSimpleTypeRestriction_t409858485 * L_8 = V_0;
		NullCheck(L_8);
		XmlSchemaObjectCollection_t2238201602 * L_9 = XmlSchemaSimpleTypeRestriction_get_Facets_m2477381752(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		XmlSchemaObjectEnumerator_t3058853928 * L_10 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
	}

IL_0047:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0074;
		}

IL_004c:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_11 = V_1;
			NullCheck(L_11);
			XmlSchemaObject_t2900481284 * L_12 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_11, /*hidden argument*/NULL);
			V_2 = ((XmlSchemaFacet_t4078749516 *)CastclassClass(L_12, XmlSchemaFacet_t4078749516_il2cpp_TypeInfo_var));
			XmlSchemaFacet_t4078749516 * L_13 = V_2;
			if (!((XmlSchemaMaxLengthFacet_t2333687858 *)IsInstClass(L_13, XmlSchemaMaxLengthFacet_t2333687858_il2cpp_TypeInfo_var)))
			{
				goto IL_0074;
			}
		}

IL_0063:
		{
			DataColumn_t3354469747 * L_14 = ___col0;
			XmlSchemaFacet_t4078749516 * L_15 = V_2;
			NullCheck(L_15);
			String_t* L_16 = XmlSchemaFacet_get_Value_m35959282(L_15, /*hidden argument*/NULL);
			int32_t L_17 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
			NullCheck(L_14);
			DataColumn_set_MaxLength_m1864982381(L_14, L_17, /*hidden argument*/NULL);
		}

IL_0074:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_18 = V_1;
			NullCheck(L_18);
			bool L_19 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_18);
			if (L_19)
			{
				goto IL_004c;
			}
		}

IL_007f:
		{
			IL2CPP_LEAVE(0x98, FINALLY_0084);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0084;
	}

FINALLY_0084:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_20 = V_1;
			Il2CppObject * L_21 = ((Il2CppObject *)IsInst(L_20, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_3 = L_21;
			if (!L_21)
			{
				goto IL_0097;
			}
		}

IL_0091:
		{
			Il2CppObject * L_22 = V_3;
			NullCheck(L_22);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_22);
		}

IL_0097:
		{
			IL2CPP_END_FINALLY(132)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(132)
	{
		IL2CPP_JUMP_TBL(0x98, IL_0098)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0098:
	{
		return;
	}
}
// System.Type System.Data.XmlSchemaDataImporter::ConvertDatatype(System.Xml.Schema.XmlSchemaDatatype)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Decimal_t1688557254_0_0_0_var;
extern const Il2CppType* Int64_t2847414882_0_0_0_var;
extern const Il2CppType* UInt64_t985925421_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_ConvertDatatype_m2516858589_MetadataUsageId;
extern "C"  Type_t * XmlSchemaDataImporter_ConvertDatatype_m2516858589 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaDatatype_t2590121 * ___dt0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ConvertDatatype_m2516858589_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlSchemaDatatype_t2590121 * L_0 = ___dt0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		XmlSchemaDatatype_t2590121 * L_2 = ___dt0;
		NullCheck(L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(7 /* System.Type System.Xml.Schema.XmlSchemaDatatype::get_ValueType() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Decimal_t1688557254_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_3) == ((Il2CppObject*)(Type_t *)L_4))))
		{
			goto IL_005d;
		}
	}
	{
		XmlSchemaDatatype_t2590121 * L_5 = ___dt0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		XmlSchemaDatatype_t2590121 * L_6 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_schemaDecimalType_1();
		if ((!(((Il2CppObject*)(XmlSchemaDatatype_t2590121 *)L_5) == ((Il2CppObject*)(XmlSchemaDatatype_t2590121 *)L_6))))
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Decimal_t1688557254_0_0_0_var), /*hidden argument*/NULL);
		return L_7;
	}

IL_003c:
	{
		XmlSchemaDatatype_t2590121 * L_8 = ___dt0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		XmlSchemaDatatype_t2590121 * L_9 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_schemaIntegerType_0();
		if ((!(((Il2CppObject*)(XmlSchemaDatatype_t2590121 *)L_8) == ((Il2CppObject*)(XmlSchemaDatatype_t2590121 *)L_9))))
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int64_t2847414882_0_0_0_var), /*hidden argument*/NULL);
		return L_10;
	}

IL_0052:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt64_t985925421_0_0_0_var), /*hidden argument*/NULL);
		return L_11;
	}

IL_005d:
	{
		XmlSchemaDatatype_t2590121 * L_12 = ___dt0;
		NullCheck(L_12);
		Type_t * L_13 = VirtFuncInvoker0< Type_t * >::Invoke(7 /* System.Type System.Xml.Schema.XmlSchemaDatatype::get_ValueType() */, L_12);
		return L_13;
	}
}
// System.String System.Data.XmlSchemaDataImporter::GetSelectorTarget(System.String)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_GetSelectorTarget_m3671140954_MetadataUsageId;
extern "C"  String_t* XmlSchemaDataImporter_GetSelectorTarget_m3671140954 (XmlSchemaDataImporter_t2811591463 * __this, String_t* ___xpath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_GetSelectorTarget_m3671140954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___xpath0;
		V_0 = L_0;
		String_t* L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = String_LastIndexOf_m3245805612(L_1, ((int32_t)47), /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_4 = V_0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		String_t* L_6 = String_Substring_m2809233063(L_4, ((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_001c:
	{
		String_t* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = String_LastIndexOf_m3245805612(L_7, ((int32_t)58), /*hidden argument*/NULL);
		V_1 = L_8;
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_10 = V_0;
		int32_t L_11 = V_1;
		NullCheck(L_10);
		String_t* L_12 = String_Substring_m2809233063(L_10, ((int32_t)((int32_t)L_11+(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_12;
	}

IL_0036:
	{
		String_t* L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_14 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::ReserveSelfIdentity(System.Xml.Schema.XmlSchemaIdentityConstraint)
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaXPath_t4094864978_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* ConstraintStructure_t742574505_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral1101699624;
extern Il2CppCodeGenString* _stringLiteral718544157;
extern const uint32_t XmlSchemaDataImporter_ReserveSelfIdentity_m4246710545_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_ReserveSelfIdentity_m4246710545 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaIdentityConstraint_t3473808128 * ___ic0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ReserveSelfIdentity_m4246710545_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	BooleanU5BU5D_t3804927312* V_2 = NULL;
	int32_t V_3 = 0;
	XmlSchemaObjectEnumerator_t3058853928 * V_4 = NULL;
	XmlSchemaXPath_t4094864978 * V_5 = NULL;
	String_t* V_6 = NULL;
	bool V_7 = false;
	int32_t V_8 = 0;
	Il2CppObject * V_9 = NULL;
	bool V_10 = false;
	String_t* V_11 = NULL;
	XmlAttributeU5BU5D_t656777856* V_12 = NULL;
	int32_t V_13 = 0;
	XmlAttribute_t2022155821 * V_14 = NULL;
	String_t* V_15 = NULL;
	Dictionary_2_t190145395 * V_16 = NULL;
	int32_t V_17 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B5_0 = 0;
	{
		XmlSchemaIdentityConstraint_t3473808128 * L_0 = ___ic0;
		NullCheck(L_0);
		XmlSchemaXPath_t4094864978 * L_1 = XmlSchemaIdentityConstraint_get_Selector_m4041799890(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = XmlSchemaXPath_get_XPath_m2946469112(L_1, /*hidden argument*/NULL);
		String_t* L_3 = XmlSchemaDataImporter_GetSelectorTarget_m3671140954(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		XmlSchemaIdentityConstraint_t3473808128 * L_4 = ___ic0;
		NullCheck(L_4);
		XmlSchemaObjectCollection_t2238201602 * L_5 = XmlSchemaIdentityConstraint_get_Fields_m1221339212(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Collections.CollectionBase::get_Count() */, L_5);
		V_1 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)L_6));
		StringU5BU5D_t2956870243* L_7 = V_1;
		NullCheck(L_7);
		V_2 = ((BooleanU5BU5D_t3804927312*)SZArrayNew(BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		V_3 = 0;
		XmlSchemaIdentityConstraint_t3473808128 * L_8 = ___ic0;
		NullCheck(L_8);
		XmlSchemaObjectCollection_t2238201602 * L_9 = XmlSchemaIdentityConstraint_get_Fields_m1221339212(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		XmlSchemaObjectEnumerator_t3058853928 * L_10 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_9, /*hidden argument*/NULL);
		V_4 = L_10;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c2;
		}

IL_0040:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_11 = V_4;
			NullCheck(L_11);
			XmlSchemaObject_t2900481284 * L_12 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_11, /*hidden argument*/NULL);
			V_5 = ((XmlSchemaXPath_t4094864978 *)CastclassClass(L_12, XmlSchemaXPath_t4094864978_il2cpp_TypeInfo_var));
			XmlSchemaXPath_t4094864978 * L_13 = V_5;
			NullCheck(L_13);
			String_t* L_14 = XmlSchemaXPath_get_XPath_m2946469112(L_13, /*hidden argument*/NULL);
			V_6 = L_14;
			String_t* L_15 = V_6;
			NullCheck(L_15);
			int32_t L_16 = String_get_Length_m2979997331(L_15, /*hidden argument*/NULL);
			if ((((int32_t)L_16) <= ((int32_t)0)))
			{
				goto IL_0072;
			}
		}

IL_0064:
		{
			String_t* L_17 = V_6;
			NullCheck(L_17);
			uint16_t L_18 = String_get_Chars_m3015341861(L_17, 0, /*hidden argument*/NULL);
			G_B5_0 = ((((int32_t)L_18) == ((int32_t)((int32_t)64)))? 1 : 0);
			goto IL_0073;
		}

IL_0072:
		{
			G_B5_0 = 0;
		}

IL_0073:
		{
			V_7 = (bool)G_B5_0;
			String_t* L_19 = V_6;
			NullCheck(L_19);
			int32_t L_20 = String_LastIndexOf_m3245805612(L_19, ((int32_t)58), /*hidden argument*/NULL);
			V_8 = L_20;
			int32_t L_21 = V_8;
			if ((((int32_t)L_21) <= ((int32_t)0)))
			{
				goto IL_009a;
			}
		}

IL_0088:
		{
			String_t* L_22 = V_6;
			int32_t L_23 = V_8;
			NullCheck(L_22);
			String_t* L_24 = String_Substring_m2809233063(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)), /*hidden argument*/NULL);
			V_6 = L_24;
			goto IL_00ab;
		}

IL_009a:
		{
			bool L_25 = V_7;
			if (!L_25)
			{
				goto IL_00ab;
			}
		}

IL_00a1:
		{
			String_t* L_26 = V_6;
			NullCheck(L_26);
			String_t* L_27 = String_Substring_m2809233063(L_26, 1, /*hidden argument*/NULL);
			V_6 = L_27;
		}

IL_00ab:
		{
			String_t* L_28 = V_6;
			IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
			String_t* L_29 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
			V_6 = L_29;
			StringU5BU5D_t2956870243* L_30 = V_1;
			int32_t L_31 = V_3;
			String_t* L_32 = V_6;
			NullCheck(L_30);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_31);
			ArrayElementTypeCheck (L_30, L_32);
			(L_30)->SetAt(static_cast<il2cpp_array_size_t>(L_31), (String_t*)L_32);
			BooleanU5BU5D_t3804927312* L_33 = V_2;
			int32_t L_34 = V_3;
			bool L_35 = V_7;
			NullCheck(L_33);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
			(L_33)->SetAt(static_cast<il2cpp_array_size_t>(L_34), (bool)L_35);
			int32_t L_36 = V_3;
			V_3 = ((int32_t)((int32_t)L_36+(int32_t)1));
		}

IL_00c2:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_37 = V_4;
			NullCheck(L_37);
			bool L_38 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_37);
			if (L_38)
			{
				goto IL_0040;
			}
		}

IL_00ce:
		{
			IL2CPP_LEAVE(0xEA, FINALLY_00d3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00d3;
	}

FINALLY_00d3:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_39 = V_4;
			Il2CppObject * L_40 = ((Il2CppObject *)IsInst(L_39, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_9 = L_40;
			if (!L_40)
			{
				goto IL_00e9;
			}
		}

IL_00e2:
		{
			Il2CppObject * L_41 = V_9;
			NullCheck(L_41);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_41);
		}

IL_00e9:
		{
			IL2CPP_END_FINALLY(211)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(211)
	{
		IL2CPP_JUMP_TBL(0xEA, IL_00ea)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00ea:
	{
		V_10 = (bool)0;
		XmlSchemaIdentityConstraint_t3473808128 * L_42 = ___ic0;
		NullCheck(L_42);
		String_t* L_43 = XmlSchemaIdentityConstraint_get_Name_m474409296(L_42, /*hidden argument*/NULL);
		V_11 = L_43;
		XmlSchemaIdentityConstraint_t3473808128 * L_44 = ___ic0;
		NullCheck(L_44);
		XmlAttributeU5BU5D_t656777856* L_45 = XmlSchemaAnnotated_get_UnhandledAttributes_m155687719(L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_01ce;
		}
	}
	{
		XmlSchemaIdentityConstraint_t3473808128 * L_46 = ___ic0;
		NullCheck(L_46);
		XmlAttributeU5BU5D_t656777856* L_47 = XmlSchemaAnnotated_get_UnhandledAttributes_m155687719(L_46, /*hidden argument*/NULL);
		V_12 = L_47;
		V_13 = 0;
		goto IL_01c3;
	}

IL_0110:
	{
		XmlAttributeU5BU5D_t656777856* L_48 = V_12;
		int32_t L_49 = V_13;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_49);
		int32_t L_50 = L_49;
		V_14 = ((L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50)));
		XmlAttribute_t2022155821 * L_51 = V_14;
		NullCheck(L_51);
		String_t* L_52 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_51);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_53 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_52, _stringLiteral3051173506, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_0132;
		}
	}
	{
		goto IL_01bd;
	}

IL_0132:
	{
		XmlAttribute_t2022155821 * L_54 = V_14;
		NullCheck(L_54);
		String_t* L_55 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_54);
		V_15 = L_55;
		String_t* L_56 = V_15;
		if (!L_56)
		{
			goto IL_01bd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		Dictionary_2_t190145395 * L_57 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24mapD_15();
		if (L_57)
		{
			goto IL_0175;
		}
	}
	{
		Dictionary_2_t190145395 * L_58 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_58, 2, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_16 = L_58;
		Dictionary_2_t190145395 * L_59 = V_16;
		NullCheck(L_59);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_59, _stringLiteral1101699624, 0);
		Dictionary_2_t190145395 * L_60 = V_16;
		NullCheck(L_60);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_60, _stringLiteral718544157, 1);
		Dictionary_2_t190145395 * L_61 = V_16;
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24mapD_15(L_61);
	}

IL_0175:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		Dictionary_2_t190145395 * L_62 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24mapD_15();
		String_t* L_63 = V_15;
		NullCheck(L_62);
		bool L_64 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(35 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_62, L_63, (&V_17));
		if (!L_64)
		{
			goto IL_01bd;
		}
	}
	{
		int32_t L_65 = V_17;
		if (!L_65)
		{
			goto IL_019c;
		}
	}
	{
		int32_t L_66 = V_17;
		if ((((int32_t)L_66) == ((int32_t)1)))
		{
			goto IL_01aa;
		}
	}
	{
		goto IL_01bd;
	}

IL_019c:
	{
		XmlAttribute_t2022155821 * L_67 = V_14;
		NullCheck(L_67);
		String_t* L_68 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_67);
		V_11 = L_68;
		goto IL_01bd;
	}

IL_01aa:
	{
		XmlAttribute_t2022155821 * L_69 = V_14;
		NullCheck(L_69);
		String_t* L_70 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_69);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t211005341_il2cpp_TypeInfo_var);
		bool L_71 = Boolean_Parse_m3007515274(NULL /*static, unused*/, L_70, /*hidden argument*/NULL);
		V_10 = L_71;
		goto IL_01bd;
	}

IL_01bd:
	{
		int32_t L_72 = V_13;
		V_13 = ((int32_t)((int32_t)L_72+(int32_t)1));
	}

IL_01c3:
	{
		int32_t L_73 = V_13;
		XmlAttributeU5BU5D_t656777856* L_74 = V_12;
		NullCheck(L_74);
		if ((((int32_t)L_73) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_74)->max_length)))))))
		{
			goto IL_0110;
		}
	}

IL_01ce:
	{
		Hashtable_t3875263730 * L_75 = __this->get_reservedConstraints_7();
		XmlSchemaIdentityConstraint_t3473808128 * L_76 = ___ic0;
		String_t* L_77 = V_0;
		StringU5BU5D_t2956870243* L_78 = V_1;
		BooleanU5BU5D_t3804927312* L_79 = V_2;
		String_t* L_80 = V_11;
		bool L_81 = V_10;
		ConstraintStructure_t742574505 * L_82 = (ConstraintStructure_t742574505 *)il2cpp_codegen_object_new(ConstraintStructure_t742574505_il2cpp_TypeInfo_var);
		ConstraintStructure__ctor_m2982708479(L_82, L_77, L_78, L_79, L_80, L_81, (String_t*)NULL, (bool)0, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_75);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_75, L_76, L_82);
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::ProcessSelfIdentity(System.Data.ConstraintStructure)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DataException_t1022144856_il2cpp_TypeInfo_var;
extern Il2CppClass* DataColumnU5BU5D_t3410743138_il2cpp_TypeInfo_var;
extern Il2CppClass* UniqueConstraint_t1006662241_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1775756833;
extern Il2CppCodeGenString* _stringLiteral680784116;
extern Il2CppCodeGenString* _stringLiteral3344632008;
extern Il2CppCodeGenString* _stringLiteral1072950516;
extern const uint32_t XmlSchemaDataImporter_ProcessSelfIdentity_m1913038921_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_ProcessSelfIdentity_m1913038921 (XmlSchemaDataImporter_t2811591463 * __this, ConstraintStructure_t742574505 * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ProcessSelfIdentity_m1913038921_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	DataTable_t2176726999 * V_1 = NULL;
	DataColumnU5BU5D_t3410743138* V_2 = NULL;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	bool V_5 = false;
	DataColumn_t3354469747 * V_6 = NULL;
	bool V_7 = false;
	String_t* V_8 = NULL;
	{
		ConstraintStructure_t742574505 * L_0 = ___c0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_TableName_0();
		V_0 = L_1;
		DataSet_t3654702571 * L_2 = __this->get_dataset_3();
		NullCheck(L_2);
		DataTableCollection_t2915263893 * L_3 = DataSet_get_Tables_m87321279(L_2, /*hidden argument*/NULL);
		String_t* L_4 = V_0;
		NullCheck(L_3);
		DataTable_t2176726999 * L_5 = DataTableCollection_get_Item_m2714089417(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		DataTable_t2176726999 * L_6 = V_1;
		if (L_6)
		{
			goto IL_003c;
		}
	}
	{
		bool L_7 = __this->get_forDataSet_4();
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1775756833, L_8, /*hidden argument*/NULL);
		DataException_t1022144856 * L_10 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_003b:
	{
		return;
	}

IL_003c:
	{
		ConstraintStructure_t742574505 * L_11 = ___c0;
		NullCheck(L_11);
		StringU5BU5D_t2956870243* L_12 = L_11->get_Columns_1();
		NullCheck(L_12);
		V_2 = ((DataColumnU5BU5D_t3410743138*)SZArrayNew(DataColumnU5BU5D_t3410743138_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))));
		V_3 = 0;
		goto IL_00de;
	}

IL_0051:
	{
		ConstraintStructure_t742574505 * L_13 = ___c0;
		NullCheck(L_13);
		StringU5BU5D_t2956870243* L_14 = L_13->get_Columns_1();
		int32_t L_15 = V_3;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		V_4 = ((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16)));
		ConstraintStructure_t742574505 * L_17 = ___c0;
		NullCheck(L_17);
		BooleanU5BU5D_t3804927312* L_18 = L_17->get_IsAttribute_2();
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t L_20 = L_19;
		V_5 = (bool)((L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20)));
		DataTable_t2176726999 * L_21 = V_1;
		NullCheck(L_21);
		DataColumnCollection_t3528392753 * L_22 = DataTable_get_Columns_m220042291(L_21, /*hidden argument*/NULL);
		String_t* L_23 = V_4;
		NullCheck(L_22);
		DataColumn_t3354469747 * L_24 = DataColumnCollection_get_Item_m305848743(L_22, L_23, /*hidden argument*/NULL);
		V_6 = L_24;
		DataColumn_t3354469747 * L_25 = V_6;
		if (L_25)
		{
			goto IL_008c;
		}
	}
	{
		String_t* L_26 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral680784116, L_26, /*hidden argument*/NULL);
		DataException_t1022144856 * L_28 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_28, L_27, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_28);
	}

IL_008c:
	{
		bool L_29 = V_5;
		if (!L_29)
		{
			goto IL_00ab;
		}
	}
	{
		DataColumn_t3354469747 * L_30 = V_6;
		NullCheck(L_30);
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_30);
		if ((((int32_t)L_31) == ((int32_t)2)))
		{
			goto IL_00ab;
		}
	}
	{
		DataException_t1022144856 * L_32 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_32, _stringLiteral3344632008, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_32);
	}

IL_00ab:
	{
		bool L_33 = V_5;
		if (L_33)
		{
			goto IL_00ca;
		}
	}
	{
		DataColumn_t3354469747 * L_34 = V_6;
		NullCheck(L_34);
		int32_t L_35 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_34);
		if ((((int32_t)L_35) == ((int32_t)1)))
		{
			goto IL_00ca;
		}
	}
	{
		DataException_t1022144856 * L_36 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_36, _stringLiteral1072950516, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_36);
	}

IL_00ca:
	{
		DataColumnU5BU5D_t3410743138* L_37 = V_2;
		int32_t L_38 = V_3;
		DataTable_t2176726999 * L_39 = V_1;
		NullCheck(L_39);
		DataColumnCollection_t3528392753 * L_40 = DataTable_get_Columns_m220042291(L_39, /*hidden argument*/NULL);
		String_t* L_41 = V_4;
		NullCheck(L_40);
		DataColumn_t3354469747 * L_42 = DataColumnCollection_get_Item_m305848743(L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		ArrayElementTypeCheck (L_37, L_42);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(L_38), (DataColumn_t3354469747 *)L_42);
		int32_t L_43 = V_3;
		V_3 = ((int32_t)((int32_t)L_43+(int32_t)1));
	}

IL_00de:
	{
		int32_t L_44 = V_3;
		DataColumnU5BU5D_t3410743138* L_45 = V_2;
		NullCheck(L_45);
		if ((((int32_t)L_44) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length)))))))
		{
			goto IL_0051;
		}
	}
	{
		ConstraintStructure_t742574505 * L_46 = ___c0;
		NullCheck(L_46);
		bool L_47 = L_46->get_IsPrimaryKey_4();
		V_7 = L_47;
		ConstraintStructure_t742574505 * L_48 = ___c0;
		NullCheck(L_48);
		String_t* L_49 = L_48->get_ConstraintName_3();
		V_8 = L_49;
		DataTable_t2176726999 * L_50 = V_1;
		NullCheck(L_50);
		ConstraintCollection_t392455726 * L_51 = DataTable_get_Constraints_m4225751151(L_50, /*hidden argument*/NULL);
		String_t* L_52 = V_8;
		DataColumnU5BU5D_t3410743138* L_53 = V_2;
		bool L_54 = V_7;
		UniqueConstraint_t1006662241 * L_55 = (UniqueConstraint_t1006662241 *)il2cpp_codegen_object_new(UniqueConstraint_t1006662241_il2cpp_TypeInfo_var);
		UniqueConstraint__ctor_m3252790845(L_55, L_52, L_53, L_54, /*hidden argument*/NULL);
		NullCheck(L_51);
		ConstraintCollection_Add_m1120071443(L_51, L_55, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::ReserveRelationIdentity(System.Xml.Schema.XmlSchemaElement,System.Xml.Schema.XmlSchemaKeyref)
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaXPath_t4094864978_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern Il2CppClass* ConstraintStructure_t742574505_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral1101699624;
extern Il2CppCodeGenString* _stringLiteral3984701153;
extern Il2CppCodeGenString* _stringLiteral1101741897;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern const uint32_t XmlSchemaDataImporter_ReserveRelationIdentity_m2792997361_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_ReserveRelationIdentity_m2792997361 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaElement_t471922321 * ___element0, XmlSchemaKeyref_t2789194649 * ___keyref1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ReserveRelationIdentity_m2792997361_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	BooleanU5BU5D_t3804927312* V_2 = NULL;
	int32_t V_3 = 0;
	XmlSchemaObjectEnumerator_t3058853928 * V_4 = NULL;
	XmlSchemaXPath_t4094864978 * V_5 = NULL;
	String_t* V_6 = NULL;
	bool V_7 = false;
	int32_t V_8 = 0;
	Il2CppObject * V_9 = NULL;
	String_t* V_10 = NULL;
	bool V_11 = false;
	bool V_12 = false;
	XmlAttributeU5BU5D_t656777856* V_13 = NULL;
	int32_t V_14 = 0;
	XmlAttribute_t2022155821 * V_15 = NULL;
	String_t* V_16 = NULL;
	Dictionary_2_t190145395 * V_17 = NULL;
	int32_t V_18 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B5_0 = 0;
	{
		XmlSchemaKeyref_t2789194649 * L_0 = ___keyref1;
		NullCheck(L_0);
		XmlSchemaXPath_t4094864978 * L_1 = XmlSchemaIdentityConstraint_get_Selector_m4041799890(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = XmlSchemaXPath_get_XPath_m2946469112(L_1, /*hidden argument*/NULL);
		String_t* L_3 = XmlSchemaDataImporter_GetSelectorTarget_m3671140954(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		XmlSchemaKeyref_t2789194649 * L_4 = ___keyref1;
		NullCheck(L_4);
		XmlSchemaObjectCollection_t2238201602 * L_5 = XmlSchemaIdentityConstraint_get_Fields_m1221339212(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Collections.CollectionBase::get_Count() */, L_5);
		V_1 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)L_6));
		StringU5BU5D_t2956870243* L_7 = V_1;
		NullCheck(L_7);
		V_2 = ((BooleanU5BU5D_t3804927312*)SZArrayNew(BooleanU5BU5D_t3804927312_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))));
		V_3 = 0;
		XmlSchemaKeyref_t2789194649 * L_8 = ___keyref1;
		NullCheck(L_8);
		XmlSchemaObjectCollection_t2238201602 * L_9 = XmlSchemaIdentityConstraint_get_Fields_m1221339212(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		XmlSchemaObjectEnumerator_t3058853928 * L_10 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_9, /*hidden argument*/NULL);
		V_4 = L_10;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c2;
		}

IL_0040:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_11 = V_4;
			NullCheck(L_11);
			XmlSchemaObject_t2900481284 * L_12 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_11, /*hidden argument*/NULL);
			V_5 = ((XmlSchemaXPath_t4094864978 *)CastclassClass(L_12, XmlSchemaXPath_t4094864978_il2cpp_TypeInfo_var));
			XmlSchemaXPath_t4094864978 * L_13 = V_5;
			NullCheck(L_13);
			String_t* L_14 = XmlSchemaXPath_get_XPath_m2946469112(L_13, /*hidden argument*/NULL);
			V_6 = L_14;
			String_t* L_15 = V_6;
			NullCheck(L_15);
			int32_t L_16 = String_get_Length_m2979997331(L_15, /*hidden argument*/NULL);
			if ((((int32_t)L_16) <= ((int32_t)0)))
			{
				goto IL_0072;
			}
		}

IL_0064:
		{
			String_t* L_17 = V_6;
			NullCheck(L_17);
			uint16_t L_18 = String_get_Chars_m3015341861(L_17, 0, /*hidden argument*/NULL);
			G_B5_0 = ((((int32_t)L_18) == ((int32_t)((int32_t)64)))? 1 : 0);
			goto IL_0073;
		}

IL_0072:
		{
			G_B5_0 = 0;
		}

IL_0073:
		{
			V_7 = (bool)G_B5_0;
			String_t* L_19 = V_6;
			NullCheck(L_19);
			int32_t L_20 = String_LastIndexOf_m3245805612(L_19, ((int32_t)58), /*hidden argument*/NULL);
			V_8 = L_20;
			int32_t L_21 = V_8;
			if ((((int32_t)L_21) <= ((int32_t)0)))
			{
				goto IL_009a;
			}
		}

IL_0088:
		{
			String_t* L_22 = V_6;
			int32_t L_23 = V_8;
			NullCheck(L_22);
			String_t* L_24 = String_Substring_m2809233063(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)), /*hidden argument*/NULL);
			V_6 = L_24;
			goto IL_00ab;
		}

IL_009a:
		{
			bool L_25 = V_7;
			if (!L_25)
			{
				goto IL_00ab;
			}
		}

IL_00a1:
		{
			String_t* L_26 = V_6;
			NullCheck(L_26);
			String_t* L_27 = String_Substring_m2809233063(L_26, 1, /*hidden argument*/NULL);
			V_6 = L_27;
		}

IL_00ab:
		{
			String_t* L_28 = V_6;
			IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
			String_t* L_29 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
			V_6 = L_29;
			StringU5BU5D_t2956870243* L_30 = V_1;
			int32_t L_31 = V_3;
			String_t* L_32 = V_6;
			NullCheck(L_30);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_31);
			ArrayElementTypeCheck (L_30, L_32);
			(L_30)->SetAt(static_cast<il2cpp_array_size_t>(L_31), (String_t*)L_32);
			BooleanU5BU5D_t3804927312* L_33 = V_2;
			int32_t L_34 = V_3;
			bool L_35 = V_7;
			NullCheck(L_33);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
			(L_33)->SetAt(static_cast<il2cpp_array_size_t>(L_34), (bool)L_35);
			int32_t L_36 = V_3;
			V_3 = ((int32_t)((int32_t)L_36+(int32_t)1));
		}

IL_00c2:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_37 = V_4;
			NullCheck(L_37);
			bool L_38 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_37);
			if (L_38)
			{
				goto IL_0040;
			}
		}

IL_00ce:
		{
			IL2CPP_LEAVE(0xEA, FINALLY_00d3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00d3;
	}

FINALLY_00d3:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_39 = V_4;
			Il2CppObject * L_40 = ((Il2CppObject *)IsInst(L_39, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_9 = L_40;
			if (!L_40)
			{
				goto IL_00e9;
			}
		}

IL_00e2:
		{
			Il2CppObject * L_41 = V_9;
			NullCheck(L_41);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_41);
		}

IL_00e9:
		{
			IL2CPP_END_FINALLY(211)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(211)
	{
		IL2CPP_JUMP_TBL(0xEA, IL_00ea)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00ea:
	{
		XmlSchemaKeyref_t2789194649 * L_42 = ___keyref1;
		NullCheck(L_42);
		String_t* L_43 = XmlSchemaIdentityConstraint_get_Name_m474409296(L_42, /*hidden argument*/NULL);
		V_10 = L_43;
		V_11 = (bool)0;
		V_12 = (bool)0;
		XmlSchemaKeyref_t2789194649 * L_44 = ___keyref1;
		NullCheck(L_44);
		XmlAttributeU5BU5D_t656777856* L_45 = XmlSchemaAnnotated_get_UnhandledAttributes_m155687719(L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_020b;
		}
	}
	{
		XmlSchemaKeyref_t2789194649 * L_46 = ___keyref1;
		NullCheck(L_46);
		XmlAttributeU5BU5D_t656777856* L_47 = XmlSchemaAnnotated_get_UnhandledAttributes_m155687719(L_46, /*hidden argument*/NULL);
		V_13 = L_47;
		V_14 = 0;
		goto IL_0200;
	}

IL_0113:
	{
		XmlAttributeU5BU5D_t656777856* L_48 = V_13;
		int32_t L_49 = V_14;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_49);
		int32_t L_50 = L_49;
		V_15 = ((L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50)));
		XmlAttribute_t2022155821 * L_51 = V_15;
		NullCheck(L_51);
		String_t* L_52 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_51);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_53 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_52, _stringLiteral3051173506, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_0135;
		}
	}
	{
		goto IL_01fa;
	}

IL_0135:
	{
		XmlAttribute_t2022155821 * L_54 = V_15;
		NullCheck(L_54);
		String_t* L_55 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_54);
		V_16 = L_55;
		String_t* L_56 = V_16;
		if (!L_56)
		{
			goto IL_01fa;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		Dictionary_2_t190145395 * L_57 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24mapE_16();
		if (L_57)
		{
			goto IL_0185;
		}
	}
	{
		Dictionary_2_t190145395 * L_58 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_58, 3, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_17 = L_58;
		Dictionary_2_t190145395 * L_59 = V_17;
		NullCheck(L_59);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_59, _stringLiteral1101699624, 0);
		Dictionary_2_t190145395 * L_60 = V_17;
		NullCheck(L_60);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_60, _stringLiteral3984701153, 1);
		Dictionary_2_t190145395 * L_61 = V_17;
		NullCheck(L_61);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_61, _stringLiteral1101741897, 2);
		Dictionary_2_t190145395 * L_62 = V_17;
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24mapE_16(L_62);
	}

IL_0185:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
		Dictionary_2_t190145395 * L_63 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24mapE_16();
		String_t* L_64 = V_16;
		NullCheck(L_63);
		bool L_65 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(35 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_63, L_64, (&V_18));
		if (!L_65)
		{
			goto IL_01fa;
		}
	}
	{
		int32_t L_66 = V_18;
		if (L_66 == 0)
		{
			goto IL_01b0;
		}
		if (L_66 == 1)
		{
			goto IL_01be;
		}
		if (L_66 == 2)
		{
			goto IL_01dc;
		}
	}
	{
		goto IL_01fa;
	}

IL_01b0:
	{
		XmlAttribute_t2022155821 * L_67 = V_15;
		NullCheck(L_67);
		String_t* L_68 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_67);
		V_10 = L_68;
		goto IL_01fa;
	}

IL_01be:
	{
		XmlAttribute_t2022155821 * L_69 = V_15;
		NullCheck(L_69);
		String_t* L_70 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_69);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_71 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_70, _stringLiteral3569038, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_01d7;
		}
	}
	{
		V_11 = (bool)1;
	}

IL_01d7:
	{
		goto IL_01fa;
	}

IL_01dc:
	{
		XmlAttribute_t2022155821 * L_72 = V_15;
		NullCheck(L_72);
		String_t* L_73 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_72);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_74 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_73, _stringLiteral3569038, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_01f5;
		}
	}
	{
		V_12 = (bool)1;
	}

IL_01f5:
	{
		goto IL_01fa;
	}

IL_01fa:
	{
		int32_t L_75 = V_14;
		V_14 = ((int32_t)((int32_t)L_75+(int32_t)1));
	}

IL_0200:
	{
		int32_t L_76 = V_14;
		XmlAttributeU5BU5D_t656777856* L_77 = V_13;
		NullCheck(L_77);
		if ((((int32_t)L_76) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_77)->max_length)))))))
		{
			goto IL_0113;
		}
	}

IL_020b:
	{
		Hashtable_t3875263730 * L_78 = __this->get_reservedConstraints_7();
		XmlSchemaKeyref_t2789194649 * L_79 = ___keyref1;
		String_t* L_80 = V_0;
		StringU5BU5D_t2956870243* L_81 = V_1;
		BooleanU5BU5D_t3804927312* L_82 = V_2;
		String_t* L_83 = V_10;
		XmlSchemaKeyref_t2789194649 * L_84 = ___keyref1;
		NullCheck(L_84);
		XmlQualifiedName_t176365656 * L_85 = XmlSchemaKeyref_get_Refer_m503628750(L_84, /*hidden argument*/NULL);
		NullCheck(L_85);
		String_t* L_86 = XmlQualifiedName_get_Name_m607016698(L_85, /*hidden argument*/NULL);
		bool L_87 = V_11;
		bool L_88 = V_12;
		ConstraintStructure_t742574505 * L_89 = (ConstraintStructure_t742574505 *)il2cpp_codegen_object_new(ConstraintStructure_t742574505_il2cpp_TypeInfo_var);
		ConstraintStructure__ctor_m2982708479(L_89, L_80, L_81, L_82, L_83, (bool)0, L_86, L_87, L_88, /*hidden argument*/NULL);
		NullCheck(L_78);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_78, L_79, L_89);
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::ProcessRelationIdentity(System.Xml.Schema.XmlSchemaElement,System.Data.ConstraintStructure)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DataException_t1022144856_il2cpp_TypeInfo_var;
extern Il2CppClass* DataColumnU5BU5D_t3410743138_il2cpp_TypeInfo_var;
extern Il2CppClass* ForeignKeyConstraint_t1848099579_il2cpp_TypeInfo_var;
extern Il2CppClass* DataRelation_t1483987353_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1775756833;
extern Il2CppCodeGenString* _stringLiteral3344632008;
extern Il2CppCodeGenString* _stringLiteral1072950516;
extern const uint32_t XmlSchemaDataImporter_ProcessRelationIdentity_m2555776360_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_ProcessRelationIdentity_m2555776360 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaElement_t471922321 * ___element0, ConstraintStructure_t742574505 * ___c1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ProcessRelationIdentity_m2555776360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	DataColumnU5BU5D_t3410743138* V_1 = NULL;
	DataTable_t2176726999 * V_2 = NULL;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	bool V_5 = false;
	DataColumn_t3354469747 * V_6 = NULL;
	String_t* V_7 = NULL;
	UniqueConstraint_t1006662241 * V_8 = NULL;
	ForeignKeyConstraint_t1848099579 * V_9 = NULL;
	DataRelation_t1483987353 * V_10 = NULL;
	{
		ConstraintStructure_t742574505 * L_0 = ___c1;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_TableName_0();
		V_0 = L_1;
		DataSet_t3654702571 * L_2 = __this->get_dataset_3();
		NullCheck(L_2);
		DataTableCollection_t2915263893 * L_3 = DataSet_get_Tables_m87321279(L_2, /*hidden argument*/NULL);
		String_t* L_4 = V_0;
		NullCheck(L_3);
		DataTable_t2176726999 * L_5 = DataTableCollection_get_Item_m2714089417(L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		DataTable_t2176726999 * L_6 = V_2;
		if (L_6)
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1775756833, L_7, /*hidden argument*/NULL);
		DataException_t1022144856 * L_9 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0030:
	{
		ConstraintStructure_t742574505 * L_10 = ___c1;
		NullCheck(L_10);
		StringU5BU5D_t2956870243* L_11 = L_10->get_Columns_1();
		NullCheck(L_11);
		V_1 = ((DataColumnU5BU5D_t3410743138*)SZArrayNew(DataColumnU5BU5D_t3410743138_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))));
		V_3 = 0;
		goto IL_00af;
	}

IL_0045:
	{
		ConstraintStructure_t742574505 * L_12 = ___c1;
		NullCheck(L_12);
		StringU5BU5D_t2956870243* L_13 = L_12->get_Columns_1();
		int32_t L_14 = V_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		V_4 = ((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15)));
		ConstraintStructure_t742574505 * L_16 = ___c1;
		NullCheck(L_16);
		BooleanU5BU5D_t3804927312* L_17 = L_16->get_IsAttribute_2();
		int32_t L_18 = V_3;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		V_5 = (bool)((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19)));
		DataTable_t2176726999 * L_20 = V_2;
		NullCheck(L_20);
		DataColumnCollection_t3528392753 * L_21 = DataTable_get_Columns_m220042291(L_20, /*hidden argument*/NULL);
		String_t* L_22 = V_4;
		NullCheck(L_21);
		DataColumn_t3354469747 * L_23 = DataColumnCollection_get_Item_m305848743(L_21, L_22, /*hidden argument*/NULL);
		V_6 = L_23;
		bool L_24 = V_5;
		if (!L_24)
		{
			goto IL_0087;
		}
	}
	{
		DataColumn_t3354469747 * L_25 = V_6;
		NullCheck(L_25);
		int32_t L_26 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_25);
		if ((((int32_t)L_26) == ((int32_t)2)))
		{
			goto IL_0087;
		}
	}
	{
		DataException_t1022144856 * L_27 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_27, _stringLiteral3344632008, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_27);
	}

IL_0087:
	{
		bool L_28 = V_5;
		if (L_28)
		{
			goto IL_00a6;
		}
	}
	{
		DataColumn_t3354469747 * L_29 = V_6;
		NullCheck(L_29);
		int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_29);
		if ((((int32_t)L_30) == ((int32_t)1)))
		{
			goto IL_00a6;
		}
	}
	{
		DataException_t1022144856 * L_31 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_31, _stringLiteral1072950516, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_31);
	}

IL_00a6:
	{
		DataColumnU5BU5D_t3410743138* L_32 = V_1;
		int32_t L_33 = V_3;
		DataColumn_t3354469747 * L_34 = V_6;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_33);
		ArrayElementTypeCheck (L_32, L_34);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(L_33), (DataColumn_t3354469747 *)L_34);
		int32_t L_35 = V_3;
		V_3 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00af:
	{
		int32_t L_36 = V_3;
		DataColumnU5BU5D_t3410743138* L_37 = V_1;
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_37)->max_length)))))))
		{
			goto IL_0045;
		}
	}
	{
		ConstraintStructure_t742574505 * L_38 = ___c1;
		NullCheck(L_38);
		String_t* L_39 = L_38->get_ReferName_5();
		V_7 = L_39;
		String_t* L_40 = V_7;
		XmlSchemaElement_t471922321 * L_41 = ___element0;
		UniqueConstraint_t1006662241 * L_42 = XmlSchemaDataImporter_FindConstraint_m1313766018(__this, L_40, L_41, /*hidden argument*/NULL);
		V_8 = L_42;
		ConstraintStructure_t742574505 * L_43 = ___c1;
		NullCheck(L_43);
		String_t* L_44 = L_43->get_ConstraintName_3();
		UniqueConstraint_t1006662241 * L_45 = V_8;
		NullCheck(L_45);
		DataColumnU5BU5D_t3410743138* L_46 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(14 /* System.Data.DataColumn[] System.Data.UniqueConstraint::get_Columns() */, L_45);
		DataColumnU5BU5D_t3410743138* L_47 = V_1;
		ForeignKeyConstraint_t1848099579 * L_48 = (ForeignKeyConstraint_t1848099579 *)il2cpp_codegen_object_new(ForeignKeyConstraint_t1848099579_il2cpp_TypeInfo_var);
		ForeignKeyConstraint__ctor_m2016558553(L_48, L_44, L_46, L_47, /*hidden argument*/NULL);
		V_9 = L_48;
		DataTable_t2176726999 * L_49 = V_2;
		NullCheck(L_49);
		ConstraintCollection_t392455726 * L_50 = DataTable_get_Constraints_m4225751151(L_49, /*hidden argument*/NULL);
		ForeignKeyConstraint_t1848099579 * L_51 = V_9;
		NullCheck(L_50);
		ConstraintCollection_Add_m1120071443(L_50, L_51, /*hidden argument*/NULL);
		ConstraintStructure_t742574505 * L_52 = ___c1;
		NullCheck(L_52);
		bool L_53 = L_52->get_IsConstraintOnly_7();
		if (L_53)
		{
			goto IL_013f;
		}
	}
	{
		ConstraintStructure_t742574505 * L_54 = ___c1;
		NullCheck(L_54);
		String_t* L_55 = L_54->get_ConstraintName_3();
		UniqueConstraint_t1006662241 * L_56 = V_8;
		NullCheck(L_56);
		DataColumnU5BU5D_t3410743138* L_57 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(14 /* System.Data.DataColumn[] System.Data.UniqueConstraint::get_Columns() */, L_56);
		DataColumnU5BU5D_t3410743138* L_58 = V_1;
		DataRelation_t1483987353 * L_59 = (DataRelation_t1483987353 *)il2cpp_codegen_object_new(DataRelation_t1483987353_il2cpp_TypeInfo_var);
		DataRelation__ctor_m751722022(L_59, L_55, L_57, L_58, (bool)1, /*hidden argument*/NULL);
		V_10 = L_59;
		DataRelation_t1483987353 * L_60 = V_10;
		ConstraintStructure_t742574505 * L_61 = ___c1;
		NullCheck(L_61);
		bool L_62 = L_61->get_IsNested_6();
		NullCheck(L_60);
		VirtActionInvoker1< bool >::Invoke(9 /* System.Void System.Data.DataRelation::set_Nested(System.Boolean) */, L_60, L_62);
		DataRelation_t1483987353 * L_63 = V_10;
		UniqueConstraint_t1006662241 * L_64 = V_8;
		NullCheck(L_63);
		DataRelation_SetParentKeyConstraint_m2428311574(L_63, L_64, /*hidden argument*/NULL);
		DataRelation_t1483987353 * L_65 = V_10;
		ForeignKeyConstraint_t1848099579 * L_66 = V_9;
		NullCheck(L_65);
		DataRelation_SetChildKeyConstraint_m3967964062(L_65, L_66, /*hidden argument*/NULL);
		DataSet_t3654702571 * L_67 = __this->get_dataset_3();
		NullCheck(L_67);
		DataRelationCollection_t267599063 * L_68 = DataSet_get_Relations_m498597843(L_67, /*hidden argument*/NULL);
		DataRelation_t1483987353 * L_69 = V_10;
		NullCheck(L_68);
		DataRelationCollection_Add_m2199449921(L_68, L_69, /*hidden argument*/NULL);
	}

IL_013f:
	{
		return;
	}
}
// System.Data.UniqueConstraint System.Data.XmlSchemaDataImporter::FindConstraint(System.String,System.Xml.Schema.XmlSchemaElement)
extern Il2CppClass* XmlSchemaIdentityConstraint_t3473808128_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaKeyref_t2789194649_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UniqueConstraint_t1006662241_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* DataException_t1022144856_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1101699624;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral3466897556;
extern const uint32_t XmlSchemaDataImporter_FindConstraint_m1313766018_MetadataUsageId;
extern "C"  UniqueConstraint_t1006662241 * XmlSchemaDataImporter_FindConstraint_m1313766018 (XmlSchemaDataImporter_t2811591463 * __this, String_t* ___name0, XmlSchemaElement_t471922321 * ___element1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_FindConstraint_m1313766018_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlSchemaObjectEnumerator_t3058853928 * V_0 = NULL;
	XmlSchemaIdentityConstraint_t3473808128 * V_1 = NULL;
	String_t* V_2 = NULL;
	DataTable_t2176726999 * V_3 = NULL;
	String_t* V_4 = NULL;
	XmlAttributeU5BU5D_t656777856* V_5 = NULL;
	int32_t V_6 = 0;
	XmlAttribute_t2022155821 * V_7 = NULL;
	UniqueConstraint_t1006662241 * V_8 = NULL;
	Il2CppObject * V_9 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlSchemaElement_t471922321 * L_0 = ___element1;
		NullCheck(L_0);
		XmlSchemaObjectCollection_t2238201602 * L_1 = XmlSchemaElement_get_Constraints_m1205744096(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		XmlSchemaObjectEnumerator_t3058853928 * L_2 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00eb;
		}

IL_0011:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_3 = V_0;
			NullCheck(L_3);
			XmlSchemaObject_t2900481284 * L_4 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_3, /*hidden argument*/NULL);
			V_1 = ((XmlSchemaIdentityConstraint_t3473808128 *)CastclassClass(L_4, XmlSchemaIdentityConstraint_t3473808128_il2cpp_TypeInfo_var));
			XmlSchemaIdentityConstraint_t3473808128 * L_5 = V_1;
			if (!((XmlSchemaKeyref_t2789194649 *)IsInstClass(L_5, XmlSchemaKeyref_t2789194649_il2cpp_TypeInfo_var)))
			{
				goto IL_002d;
			}
		}

IL_0028:
		{
			goto IL_00eb;
		}

IL_002d:
		{
			XmlSchemaIdentityConstraint_t3473808128 * L_6 = V_1;
			NullCheck(L_6);
			String_t* L_7 = XmlSchemaIdentityConstraint_get_Name_m474409296(L_6, /*hidden argument*/NULL);
			String_t* L_8 = ___name0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_9 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_00eb;
			}
		}

IL_003e:
		{
			XmlSchemaIdentityConstraint_t3473808128 * L_10 = V_1;
			NullCheck(L_10);
			XmlSchemaXPath_t4094864978 * L_11 = XmlSchemaIdentityConstraint_get_Selector_m4041799890(L_10, /*hidden argument*/NULL);
			NullCheck(L_11);
			String_t* L_12 = XmlSchemaXPath_get_XPath_m2946469112(L_11, /*hidden argument*/NULL);
			String_t* L_13 = XmlSchemaDataImporter_GetSelectorTarget_m3671140954(__this, L_12, /*hidden argument*/NULL);
			V_2 = L_13;
			DataSet_t3654702571 * L_14 = __this->get_dataset_3();
			NullCheck(L_14);
			DataTableCollection_t2915263893 * L_15 = DataSet_get_Tables_m87321279(L_14, /*hidden argument*/NULL);
			String_t* L_16 = V_2;
			NullCheck(L_15);
			DataTable_t2176726999 * L_17 = DataTableCollection_get_Item_m2714089417(L_15, L_16, /*hidden argument*/NULL);
			V_3 = L_17;
			XmlSchemaIdentityConstraint_t3473808128 * L_18 = V_1;
			NullCheck(L_18);
			String_t* L_19 = XmlSchemaIdentityConstraint_get_Name_m474409296(L_18, /*hidden argument*/NULL);
			V_4 = L_19;
			XmlSchemaIdentityConstraint_t3473808128 * L_20 = V_1;
			NullCheck(L_20);
			XmlAttributeU5BU5D_t656777856* L_21 = XmlSchemaAnnotated_get_UnhandledAttributes_m155687719(L_20, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_00d2;
			}
		}

IL_0075:
		{
			XmlSchemaIdentityConstraint_t3473808128 * L_22 = V_1;
			NullCheck(L_22);
			XmlAttributeU5BU5D_t656777856* L_23 = XmlSchemaAnnotated_get_UnhandledAttributes_m155687719(L_22, /*hidden argument*/NULL);
			V_5 = L_23;
			V_6 = 0;
			goto IL_00c7;
		}

IL_0085:
		{
			XmlAttributeU5BU5D_t656777856* L_24 = V_5;
			int32_t L_25 = V_6;
			NullCheck(L_24);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
			int32_t L_26 = L_25;
			V_7 = ((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26)));
			XmlAttribute_t2022155821 * L_27 = V_7;
			NullCheck(L_27);
			String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_27);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_29 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_28, _stringLiteral1101699624, /*hidden argument*/NULL);
			if (!L_29)
			{
				goto IL_00c1;
			}
		}

IL_00a2:
		{
			XmlAttribute_t2022155821 * L_30 = V_7;
			NullCheck(L_30);
			String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_30);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_32 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_31, _stringLiteral3051173506, /*hidden argument*/NULL);
			if (!L_32)
			{
				goto IL_00c1;
			}
		}

IL_00b8:
		{
			XmlAttribute_t2022155821 * L_33 = V_7;
			NullCheck(L_33);
			String_t* L_34 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Xml.XmlNode::get_Value() */, L_33);
			V_4 = L_34;
		}

IL_00c1:
		{
			int32_t L_35 = V_6;
			V_6 = ((int32_t)((int32_t)L_35+(int32_t)1));
		}

IL_00c7:
		{
			int32_t L_36 = V_6;
			XmlAttributeU5BU5D_t656777856* L_37 = V_5;
			NullCheck(L_37);
			if ((((int32_t)L_36) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_37)->max_length)))))))
			{
				goto IL_0085;
			}
		}

IL_00d2:
		{
			DataTable_t2176726999 * L_38 = V_3;
			NullCheck(L_38);
			ConstraintCollection_t392455726 * L_39 = DataTable_get_Constraints_m4225751151(L_38, /*hidden argument*/NULL);
			String_t* L_40 = V_4;
			NullCheck(L_39);
			Constraint_t2349953968 * L_41 = ConstraintCollection_get_Item_m2351749703(L_39, L_40, /*hidden argument*/NULL);
			V_8 = ((UniqueConstraint_t1006662241 *)CastclassClass(L_41, UniqueConstraint_t1006662241_il2cpp_TypeInfo_var));
			IL2CPP_LEAVE(0x122, FINALLY_00fb);
		}

IL_00eb:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_42 = V_0;
			NullCheck(L_42);
			bool L_43 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_42);
			if (L_43)
			{
				goto IL_0011;
			}
		}

IL_00f6:
		{
			IL2CPP_LEAVE(0x111, FINALLY_00fb);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00fb;
	}

FINALLY_00fb:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_44 = V_0;
			Il2CppObject * L_45 = ((Il2CppObject *)IsInst(L_44, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_9 = L_45;
			if (!L_45)
			{
				goto IL_0110;
			}
		}

IL_0109:
		{
			Il2CppObject * L_46 = V_9;
			NullCheck(L_46);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_46);
		}

IL_0110:
		{
			IL2CPP_END_FINALLY(251)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(251)
	{
		IL2CPP_JUMP_TBL(0x122, IL_0122)
		IL2CPP_JUMP_TBL(0x111, IL_0111)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0111:
	{
		String_t* L_47 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3466897556, L_47, /*hidden argument*/NULL);
		DataException_t1022144856 * L_49 = (DataException_t1022144856 *)il2cpp_codegen_object_new(DataException_t1022144856_il2cpp_TypeInfo_var);
		DataException__ctor_m829863666(L_49, L_48, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_49);
	}

IL_0122:
	{
		UniqueConstraint_t1006662241 * L_50 = V_8;
		return L_50;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::HandleAnnotations(System.Xml.Schema.XmlSchemaAnnotation,System.Boolean)
extern Il2CppClass* XmlSchemaAppInfo_t1340486148_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlElement_t3562928333_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4197435992;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral3265935493;
extern Il2CppCodeGenString* _stringLiteral1410807069;
extern const uint32_t XmlSchemaDataImporter_HandleAnnotations_m996806934_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_HandleAnnotations_m996806934 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaAnnotation_t1377046772 * ___an0, bool ___nested1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_HandleAnnotations_m996806934_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlSchemaObjectEnumerator_t3058853928 * V_0 = NULL;
	XmlSchemaObject_t2900481284 * V_1 = NULL;
	XmlSchemaAppInfo_t1340486148 * V_2 = NULL;
	XmlNodeU5BU5D_t1808339196* V_3 = NULL;
	int32_t V_4 = 0;
	XmlNode_t3592213601 * V_5 = NULL;
	XmlElement_t3562928333 * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlSchemaAnnotation_t1377046772 * L_0 = ___an0;
		NullCheck(L_0);
		XmlSchemaObjectCollection_t2238201602 * L_1 = XmlSchemaAnnotation_get_Items_m3920661563(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		XmlSchemaObjectEnumerator_t3058853928 * L_2 = XmlSchemaObjectCollection_GetEnumerator_m3792827626(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00cb;
		}

IL_0011:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_3 = V_0;
			NullCheck(L_3);
			XmlSchemaObject_t2900481284 * L_4 = XmlSchemaObjectEnumerator_get_Current_m973495998(L_3, /*hidden argument*/NULL);
			V_1 = L_4;
			XmlSchemaObject_t2900481284 * L_5 = V_1;
			V_2 = ((XmlSchemaAppInfo_t1340486148 *)IsInstClass(L_5, XmlSchemaAppInfo_t1340486148_il2cpp_TypeInfo_var));
			XmlSchemaAppInfo_t1340486148 * L_6 = V_2;
			if (!L_6)
			{
				goto IL_00cb;
			}
		}

IL_0025:
		{
			XmlSchemaAppInfo_t1340486148 * L_7 = V_2;
			NullCheck(L_7);
			XmlNodeU5BU5D_t1808339196* L_8 = XmlSchemaAppInfo_get_Markup_m2715614730(L_7, /*hidden argument*/NULL);
			V_3 = L_8;
			V_4 = 0;
			goto IL_00c1;
		}

IL_0034:
		{
			XmlNodeU5BU5D_t1808339196* L_9 = V_3;
			int32_t L_10 = V_4;
			NullCheck(L_9);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
			int32_t L_11 = L_10;
			V_5 = ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11)));
			XmlNode_t3592213601 * L_12 = V_5;
			V_6 = ((XmlElement_t3562928333 *)IsInstClass(L_12, XmlElement_t3562928333_il2cpp_TypeInfo_var));
			XmlElement_t3562928333 * L_13 = V_6;
			if (!L_13)
			{
				goto IL_007f;
			}
		}

IL_004a:
		{
			XmlElement_t3562928333 * L_14 = V_6;
			NullCheck(L_14);
			String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_14);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_16 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_15, _stringLiteral4197435992, /*hidden argument*/NULL);
			if (!L_16)
			{
				goto IL_007f;
			}
		}

IL_0060:
		{
			XmlElement_t3562928333 * L_17 = V_6;
			NullCheck(L_17);
			String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_17);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_19 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_18, _stringLiteral3051173506, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_007f;
			}
		}

IL_0076:
		{
			XmlElement_t3562928333 * L_20 = V_6;
			bool L_21 = ___nested1;
			XmlSchemaDataImporter_HandleRelationshipAnnotation_m1112614008(__this, L_20, L_21, /*hidden argument*/NULL);
		}

IL_007f:
		{
			XmlElement_t3562928333 * L_22 = V_6;
			if (!L_22)
			{
				goto IL_00bb;
			}
		}

IL_0086:
		{
			XmlElement_t3562928333 * L_23 = V_6;
			NullCheck(L_23);
			String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_23);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_25 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_24, _stringLiteral3265935493, /*hidden argument*/NULL);
			if (!L_25)
			{
				goto IL_00bb;
			}
		}

IL_009c:
		{
			XmlElement_t3562928333 * L_26 = V_6;
			NullCheck(L_26);
			String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlNode::get_NamespaceURI() */, L_26);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_28 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_27, _stringLiteral1410807069, /*hidden argument*/NULL);
			if (!L_28)
			{
				goto IL_00bb;
			}
		}

IL_00b2:
		{
			XmlElement_t3562928333 * L_29 = V_6;
			bool L_30 = ___nested1;
			XmlSchemaDataImporter_HandleDataSourceAnnotation_m1129752357(__this, L_29, L_30, /*hidden argument*/NULL);
		}

IL_00bb:
		{
			int32_t L_31 = V_4;
			V_4 = ((int32_t)((int32_t)L_31+(int32_t)1));
		}

IL_00c1:
		{
			int32_t L_32 = V_4;
			XmlNodeU5BU5D_t1808339196* L_33 = V_3;
			NullCheck(L_33);
			if ((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length)))))))
			{
				goto IL_0034;
			}
		}

IL_00cb:
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_34 = V_0;
			NullCheck(L_34);
			bool L_35 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.Schema.XmlSchemaObjectEnumerator::MoveNext() */, L_34);
			if (L_35)
			{
				goto IL_0011;
			}
		}

IL_00d6:
		{
			IL2CPP_LEAVE(0xF1, FINALLY_00db);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00db;
	}

FINALLY_00db:
	{ // begin finally (depth: 1)
		{
			XmlSchemaObjectEnumerator_t3058853928 * L_36 = V_0;
			Il2CppObject * L_37 = ((Il2CppObject *)IsInst(L_36, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_7 = L_37;
			if (!L_37)
			{
				goto IL_00f0;
			}
		}

IL_00e9:
		{
			Il2CppObject * L_38 = V_7;
			NullCheck(L_38);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_38);
		}

IL_00f0:
		{
			IL2CPP_END_FINALLY(219)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(219)
	{
		IL2CPP_JUMP_TBL(0xF1, IL_00f1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00f1:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::HandleDataSourceAnnotation(System.Xml.XmlElement,System.Boolean)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNode_t3592213601_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlElement_t3562928333_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DbProviderFactories_t3895152489_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392472373;
extern Il2CppCodeGenString* _stringLiteral3372116497;
extern Il2CppCodeGenString* _stringLiteral222165348;
extern Il2CppCodeGenString* _stringLiteral2497456773;
extern const uint32_t XmlSchemaDataImporter_HandleDataSourceAnnotation_m1129752357_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_HandleDataSourceAnnotation_m1129752357 (XmlSchemaDataImporter_t2811591463 * __this, XmlElement_t3562928333 * ___el0, bool ___nested1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_HandleDataSourceAnnotation_m1129752357_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	DbProviderFactory_t2435213707 * V_2 = NULL;
	XmlElement_t3562928333 * V_3 = NULL;
	XmlElement_t3562928333 * V_4 = NULL;
	XmlElement_t3562928333 * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	XmlNode_t3592213601 * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	Il2CppObject * V_9 = NULL;
	XmlNode_t3592213601 * V_10 = NULL;
	Il2CppObject * V_11 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (String_t*)NULL;
		V_1 = (String_t*)NULL;
		V_2 = (DbProviderFactory_t2435213707 *)NULL;
		V_4 = (XmlElement_t3562928333 *)NULL;
		XmlElement_t3562928333 * L_0 = ___el0;
		NullCheck(L_0);
		XmlNodeList_t3966370975 * L_1 = VirtFuncInvoker0< XmlNodeList_t3966370975 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_0);
		NullCheck(L_1);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_1);
		V_6 = L_2;
	}

IL_0016:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a2;
		}

IL_001b:
		{
			Il2CppObject * L_3 = V_6;
			NullCheck(L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_3);
			V_7 = ((XmlNode_t3592213601 *)CastclassClass(L_4, XmlNode_t3592213601_il2cpp_TypeInfo_var));
			XmlNode_t3592213601 * L_5 = V_7;
			V_3 = ((XmlElement_t3562928333 *)IsInstClass(L_5, XmlElement_t3562928333_il2cpp_TypeInfo_var));
			XmlElement_t3562928333 * L_6 = V_3;
			if (L_6)
			{
				goto IL_003c;
			}
		}

IL_0037:
		{
			goto IL_00a2;
		}

IL_003c:
		{
			XmlElement_t3562928333 * L_7 = V_3;
			NullCheck(L_7);
			String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_7);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_9 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_8, _stringLiteral3392472373, /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_008a;
			}
		}

IL_0051:
		{
			XmlElement_t3562928333 * L_10 = V_3;
			NullCheck(L_10);
			XmlNode_t3592213601 * L_11 = VirtFuncInvoker0< XmlNode_t3592213601 * >::Invoke(10 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_10);
			XmlElement_t3562928333 * L_12 = ((XmlElement_t3562928333 *)IsInstClass(L_11, XmlElement_t3562928333_il2cpp_TypeInfo_var));
			V_5 = L_12;
			if (!L_12)
			{
				goto IL_008a;
			}
		}

IL_0064:
		{
			XmlElement_t3562928333 * L_13 = V_5;
			NullCheck(L_13);
			String_t* L_14 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_13, _stringLiteral3372116497);
			V_0 = L_14;
			XmlElement_t3562928333 * L_15 = V_5;
			NullCheck(L_15);
			String_t* L_16 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_15, _stringLiteral222165348);
			V_1 = L_16;
			String_t* L_17 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(DbProviderFactories_t3895152489_il2cpp_TypeInfo_var);
			DbProviderFactory_t2435213707 * L_18 = DbProviderFactories_GetFactory_m1249875471(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
			V_2 = L_18;
			goto IL_00a2;
		}

IL_008a:
		{
			XmlElement_t3562928333 * L_19 = V_3;
			NullCheck(L_19);
			String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_19);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_21 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_20, _stringLiteral2497456773, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_00a2;
			}
		}

IL_009f:
		{
			XmlElement_t3562928333 * L_22 = V_3;
			V_4 = L_22;
		}

IL_00a2:
		{
			Il2CppObject * L_23 = V_6;
			NullCheck(L_23);
			bool L_24 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_23);
			if (L_24)
			{
				goto IL_001b;
			}
		}

IL_00ae:
		{
			IL2CPP_LEAVE(0xCA, FINALLY_00b3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00b3;
	}

FINALLY_00b3:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_25 = V_6;
			Il2CppObject * L_26 = ((Il2CppObject *)IsInst(L_25, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_8 = L_26;
			if (!L_26)
			{
				goto IL_00c9;
			}
		}

IL_00c2:
		{
			Il2CppObject * L_27 = V_8;
			NullCheck(L_27);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_27);
		}

IL_00c9:
		{
			IL2CPP_END_FINALLY(179)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(179)
	{
		IL2CPP_JUMP_TBL(0xCA, IL_00ca)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00ca:
	{
		XmlElement_t3562928333 * L_28 = V_4;
		if (!L_28)
		{
			goto IL_012f;
		}
	}
	{
		DbProviderFactory_t2435213707 * L_29 = V_2;
		if (!L_29)
		{
			goto IL_012f;
		}
	}
	{
		XmlElement_t3562928333 * L_30 = V_4;
		NullCheck(L_30);
		XmlNodeList_t3966370975 * L_31 = VirtFuncInvoker0< XmlNodeList_t3966370975 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_30);
		NullCheck(L_31);
		Il2CppObject * L_32 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_31);
		V_9 = L_32;
	}

IL_00e5:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0107;
		}

IL_00ea:
		{
			Il2CppObject * L_33 = V_9;
			NullCheck(L_33);
			Il2CppObject * L_34 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_33);
			V_10 = ((XmlNode_t3592213601 *)CastclassClass(L_34, XmlNode_t3592213601_il2cpp_TypeInfo_var));
			XmlNode_t3592213601 * L_35 = V_10;
			DbProviderFactory_t2435213707 * L_36 = V_2;
			String_t* L_37 = V_1;
			XmlSchemaDataImporter_ProcessTableAdapter_m3783769469(__this, ((XmlElement_t3562928333 *)IsInstClass(L_35, XmlElement_t3562928333_il2cpp_TypeInfo_var)), L_36, L_37, /*hidden argument*/NULL);
		}

IL_0107:
		{
			Il2CppObject * L_38 = V_9;
			NullCheck(L_38);
			bool L_39 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_38);
			if (L_39)
			{
				goto IL_00ea;
			}
		}

IL_0113:
		{
			IL2CPP_LEAVE(0x12F, FINALLY_0118);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0118;
	}

FINALLY_0118:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_40 = V_9;
			Il2CppObject * L_41 = ((Il2CppObject *)IsInst(L_40, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_11 = L_41;
			if (!L_41)
			{
				goto IL_012e;
			}
		}

IL_0127:
		{
			Il2CppObject * L_42 = V_11;
			NullCheck(L_42);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_42);
		}

IL_012e:
		{
			IL2CPP_END_FINALLY(280)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(280)
	{
		IL2CPP_JUMP_TBL(0x12F, IL_012f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_012f:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::ProcessTableAdapter(System.Xml.XmlElement,System.Data.Common.DbProviderFactory,System.String)
extern Il2CppClass* TableAdapterSchemaInfo_t1131857475_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNode_t3592213601_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlElement_t3562928333_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* DataTableMapping_t171110970_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral161443079;
extern Il2CppCodeGenString* _stringLiteral2420395;
extern Il2CppCodeGenString* _stringLiteral64376707;
extern Il2CppCodeGenString* _stringLiteral1617086044;
extern Il2CppCodeGenString* _stringLiteral2453882708;
extern Il2CppCodeGenString* _stringLiteral3937743768;
extern Il2CppCodeGenString* _stringLiteral259089797;
extern Il2CppCodeGenString* _stringLiteral80563118;
extern const uint32_t XmlSchemaDataImporter_ProcessTableAdapter_m3783769469_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_ProcessTableAdapter_m3783769469 (XmlSchemaDataImporter_t2811591463 * __this, XmlElement_t3562928333 * ___el0, DbProviderFactory_t2435213707 * ___provider1, String_t* ___connStr2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ProcessTableAdapter_m3783769469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlElement_t3562928333 * V_0 = NULL;
	String_t* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	XmlNode_t3592213601 * V_3 = NULL;
	String_t* V_4 = NULL;
	Dictionary_2_t190145395 * V_5 = NULL;
	int32_t V_6 = 0;
	Il2CppObject * V_7 = NULL;
	XmlNode_t3592213601 * V_8 = NULL;
	Il2CppObject * V_9 = NULL;
	DataTableMapping_t171110970 * V_10 = NULL;
	Il2CppObject * V_11 = NULL;
	XmlNode_t3592213601 * V_12 = NULL;
	Il2CppObject * V_13 = NULL;
	Il2CppObject * V_14 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_1 = (String_t*)NULL;
		XmlElement_t3562928333 * L_0 = ___el0;
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		DbProviderFactory_t2435213707 * L_1 = ___provider1;
		TableAdapterSchemaInfo_t1131857475 * L_2 = (TableAdapterSchemaInfo_t1131857475 *)il2cpp_codegen_object_new(TableAdapterSchemaInfo_t1131857475_il2cpp_TypeInfo_var);
		TableAdapterSchemaInfo__ctor_m2798241732(L_2, L_1, /*hidden argument*/NULL);
		__this->set_currentAdapter_12(L_2);
		TableAdapterSchemaInfo_t1131857475 * L_3 = __this->get_currentAdapter_12();
		String_t* L_4 = ___connStr2;
		NullCheck(L_3);
		L_3->set_ConnectionString_3(L_4);
		TableAdapterSchemaInfo_t1131857475 * L_5 = __this->get_currentAdapter_12();
		XmlElement_t3562928333 * L_6 = ___el0;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_6, _stringLiteral161443079);
		NullCheck(L_5);
		L_5->set_BaseClass_4(L_7);
		XmlElement_t3562928333 * L_8 = ___el0;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_8, _stringLiteral2420395);
		V_1 = L_9;
		TableAdapterSchemaInfo_t1131857475 * L_10 = __this->get_currentAdapter_12();
		XmlElement_t3562928333 * L_11 = ___el0;
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_11, _stringLiteral64376707);
		NullCheck(L_10);
		L_10->set_Name_5(L_12);
		TableAdapterSchemaInfo_t1131857475 * L_13 = __this->get_currentAdapter_12();
		NullCheck(L_13);
		String_t* L_14 = L_13->get_Name_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0084;
		}
	}
	{
		TableAdapterSchemaInfo_t1131857475 * L_16 = __this->get_currentAdapter_12();
		XmlElement_t3562928333 * L_17 = ___el0;
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_17, _stringLiteral1617086044);
		NullCheck(L_16);
		L_16->set_Name_5(L_18);
	}

IL_0084:
	{
		XmlElement_t3562928333 * L_19 = ___el0;
		NullCheck(L_19);
		XmlNodeList_t3966370975 * L_20 = VirtFuncInvoker0< XmlNodeList_t3966370975 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_19);
		NullCheck(L_20);
		Il2CppObject * L_21 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_20);
		V_2 = L_21;
	}

IL_0090:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0212;
		}

IL_0095:
		{
			Il2CppObject * L_22 = V_2;
			NullCheck(L_22);
			Il2CppObject * L_23 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_22);
			V_3 = ((XmlNode_t3592213601 *)CastclassClass(L_23, XmlNode_t3592213601_il2cpp_TypeInfo_var));
			XmlNode_t3592213601 * L_24 = V_3;
			V_0 = ((XmlElement_t3562928333 *)IsInstClass(L_24, XmlElement_t3562928333_il2cpp_TypeInfo_var));
			XmlElement_t3562928333 * L_25 = V_0;
			if (L_25)
			{
				goto IL_00b3;
			}
		}

IL_00ae:
		{
			goto IL_0212;
		}

IL_00b3:
		{
			XmlElement_t3562928333 * L_26 = V_0;
			NullCheck(L_26);
			String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_26);
			V_4 = L_27;
			String_t* L_28 = V_4;
			if (!L_28)
			{
				goto IL_0212;
			}
		}

IL_00c2:
		{
			IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
			Dictionary_2_t190145395 * L_29 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24mapF_17();
			if (L_29)
			{
				goto IL_0102;
			}
		}

IL_00cc:
		{
			Dictionary_2_t190145395 * L_30 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
			Dictionary_2__ctor_m1958628151(L_30, 3, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
			V_5 = L_30;
			Dictionary_2_t190145395 * L_31 = V_5;
			NullCheck(L_31);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_31, _stringLiteral2453882708, 0);
			Dictionary_2_t190145395 * L_32 = V_5;
			NullCheck(L_32);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_32, _stringLiteral3937743768, 0);
			Dictionary_2_t190145395 * L_33 = V_5;
			NullCheck(L_33);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_33, _stringLiteral259089797, 1);
			Dictionary_2_t190145395 * L_34 = V_5;
			IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
			((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24mapF_17(L_34);
		}

IL_0102:
		{
			IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
			Dictionary_2_t190145395 * L_35 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24mapF_17();
			String_t* L_36 = V_4;
			NullCheck(L_35);
			bool L_37 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(35 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_35, L_36, (&V_6));
			if (!L_37)
			{
				goto IL_0212;
			}
		}

IL_0115:
		{
			int32_t L_38 = V_6;
			if (!L_38)
			{
				goto IL_0129;
			}
		}

IL_011c:
		{
			int32_t L_39 = V_6;
			if ((((int32_t)L_39) == ((int32_t)1)))
			{
				goto IL_0183;
			}
		}

IL_0124:
		{
			goto IL_0212;
		}

IL_0129:
		{
			XmlElement_t3562928333 * L_40 = V_0;
			NullCheck(L_40);
			XmlNodeList_t3966370975 * L_41 = VirtFuncInvoker0< XmlNodeList_t3966370975 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_40);
			NullCheck(L_41);
			Il2CppObject * L_42 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_41);
			V_7 = L_42;
		}

IL_0136:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0156;
			}

IL_013b:
			{
				Il2CppObject * L_43 = V_7;
				NullCheck(L_43);
				Il2CppObject * L_44 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_43);
				V_8 = ((XmlNode_t3592213601 *)CastclassClass(L_44, XmlNode_t3592213601_il2cpp_TypeInfo_var));
				XmlNode_t3592213601 * L_45 = V_8;
				XmlSchemaDataImporter_ProcessDbSource_m1138771216(__this, ((XmlElement_t3562928333 *)IsInstClass(L_45, XmlElement_t3562928333_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			}

IL_0156:
			{
				Il2CppObject * L_46 = V_7;
				NullCheck(L_46);
				bool L_47 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_46);
				if (L_47)
				{
					goto IL_013b;
				}
			}

IL_0162:
			{
				IL2CPP_LEAVE(0x17E, FINALLY_0167);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0167;
		}

FINALLY_0167:
		{ // begin finally (depth: 2)
			{
				Il2CppObject * L_48 = V_7;
				Il2CppObject * L_49 = ((Il2CppObject *)IsInst(L_48, IDisposable_t1628921374_il2cpp_TypeInfo_var));
				V_9 = L_49;
				if (!L_49)
				{
					goto IL_017d;
				}
			}

IL_0176:
			{
				Il2CppObject * L_50 = V_9;
				NullCheck(L_50);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_50);
			}

IL_017d:
			{
				IL2CPP_END_FINALLY(359)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(359)
		{
			IL2CPP_JUMP_TBL(0x17E, IL_017e)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_017e:
		{
			goto IL_0212;
		}

IL_0183:
		{
			DataTableMapping_t171110970 * L_51 = (DataTableMapping_t171110970 *)il2cpp_codegen_object_new(DataTableMapping_t171110970_il2cpp_TypeInfo_var);
			DataTableMapping__ctor_m239450280(L_51, /*hidden argument*/NULL);
			V_10 = L_51;
			DataTableMapping_t171110970 * L_52 = V_10;
			NullCheck(L_52);
			DataTableMapping_set_SourceTable_m2860690022(L_52, _stringLiteral80563118, /*hidden argument*/NULL);
			DataTableMapping_t171110970 * L_53 = V_10;
			String_t* L_54 = V_1;
			NullCheck(L_53);
			DataTableMapping_set_DataSetTable_m1224049139(L_53, L_54, /*hidden argument*/NULL);
			XmlElement_t3562928333 * L_55 = V_0;
			NullCheck(L_55);
			XmlNodeList_t3966370975 * L_56 = VirtFuncInvoker0< XmlNodeList_t3966370975 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_55);
			NullCheck(L_56);
			Il2CppObject * L_57 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_56);
			V_11 = L_57;
		}

IL_01ab:
		try
		{ // begin try (depth: 2)
			{
				goto IL_01cd;
			}

IL_01b0:
			{
				Il2CppObject * L_58 = V_11;
				NullCheck(L_58);
				Il2CppObject * L_59 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_58);
				V_12 = ((XmlNode_t3592213601 *)CastclassClass(L_59, XmlNode_t3592213601_il2cpp_TypeInfo_var));
				XmlNode_t3592213601 * L_60 = V_12;
				DataTableMapping_t171110970 * L_61 = V_10;
				XmlSchemaDataImporter_ProcessColumnMapping_m1831631887(__this, ((XmlElement_t3562928333 *)IsInstClass(L_60, XmlElement_t3562928333_il2cpp_TypeInfo_var)), L_61, /*hidden argument*/NULL);
			}

IL_01cd:
			{
				Il2CppObject * L_62 = V_11;
				NullCheck(L_62);
				bool L_63 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_62);
				if (L_63)
				{
					goto IL_01b0;
				}
			}

IL_01d9:
			{
				IL2CPP_LEAVE(0x1F5, FINALLY_01de);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_01de;
		}

FINALLY_01de:
		{ // begin finally (depth: 2)
			{
				Il2CppObject * L_64 = V_11;
				Il2CppObject * L_65 = ((Il2CppObject *)IsInst(L_64, IDisposable_t1628921374_il2cpp_TypeInfo_var));
				V_13 = L_65;
				if (!L_65)
				{
					goto IL_01f4;
				}
			}

IL_01ed:
			{
				Il2CppObject * L_66 = V_13;
				NullCheck(L_66);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_66);
			}

IL_01f4:
			{
				IL2CPP_END_FINALLY(478)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(478)
		{
			IL2CPP_JUMP_TBL(0x1F5, IL_01f5)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_01f5:
		{
			TableAdapterSchemaInfo_t1131857475 * L_67 = __this->get_currentAdapter_12();
			NullCheck(L_67);
			DbDataAdapter_t3684585719 * L_68 = L_67->get_Adapter_1();
			NullCheck(L_68);
			DataTableMappingCollection_t2256861304 * L_69 = DataAdapter_get_TableMappings_m3344405610(L_68, /*hidden argument*/NULL);
			DataTableMapping_t171110970 * L_70 = V_10;
			NullCheck(L_69);
			DataTableMappingCollection_Add_m3788368423(L_69, L_70, /*hidden argument*/NULL);
			goto IL_0212;
		}

IL_0212:
		{
			Il2CppObject * L_71 = V_2;
			NullCheck(L_71);
			bool L_72 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_71);
			if (L_72)
			{
				goto IL_0095;
			}
		}

IL_021d:
		{
			IL2CPP_LEAVE(0x238, FINALLY_0222);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0222;
	}

FINALLY_0222:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_73 = V_2;
			Il2CppObject * L_74 = ((Il2CppObject *)IsInst(L_73, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_14 = L_74;
			if (!L_74)
			{
				goto IL_0237;
			}
		}

IL_0230:
		{
			Il2CppObject * L_75 = V_14;
			NullCheck(L_75);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_75);
		}

IL_0237:
		{
			IL2CPP_END_FINALLY(546)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(546)
	{
		IL2CPP_JUMP_TBL(0x238, IL_0238)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0238:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::ProcessDbSource(System.Xml.XmlElement)
extern const Il2CppType* GenerateMethodsType_t1113224938_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppClass* DbCommandInfo_t2041793742_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2778772662_il2cpp_TypeInfo_var;
extern Il2CppClass* GenerateMethodsType_t1113224938_il2cpp_TypeInfo_var;
extern Il2CppClass* DbSourceMethodInfo_t3307618843_il2cpp_TypeInfo_var;
extern Il2CppClass* DbSourceMethodInfoU5BU5D_t1609409306_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNode_t3592213601_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlElement_t3562928333_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3780601743;
extern Il2CppCodeGenString* _stringLiteral846162013;
extern Il2CppCodeGenString* _stringLiteral4073017058;
extern Il2CppCodeGenString* _stringLiteral3449934766;
extern Il2CppCodeGenString* _stringLiteral2401410697;
extern Il2CppCodeGenString* _stringLiteral563490826;
extern Il2CppCodeGenString* _stringLiteral197105666;
extern Il2CppCodeGenString* _stringLiteral4052172047;
extern Il2CppCodeGenString* _stringLiteral2687951707;
extern Il2CppCodeGenString* _stringLiteral2420395;
extern Il2CppCodeGenString* _stringLiteral3744098039;
extern Il2CppCodeGenString* _stringLiteral2472812175;
extern Il2CppCodeGenString* _stringLiteral1237848626;
extern Il2CppCodeGenString* _stringLiteral253405730;
extern Il2CppCodeGenString* _stringLiteral1544583936;
extern const uint32_t XmlSchemaDataImporter_ProcessDbSource_m1138771216_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_ProcessDbSource_m1138771216 (XmlSchemaDataImporter_t2811591463 * __this, XmlElement_t3562928333 * ___el0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ProcessDbSource_m1138771216_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	XmlElement_t3562928333 * V_2 = NULL;
	DbCommandInfo_t2041793742 * V_3 = NULL;
	DbSourceMethodInfo_t3307618843 * V_4 = NULL;
	int32_t V_5 = 0;
	DbSourceMethodInfo_t3307618843 * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	XmlNode_t3592213601 * V_8 = NULL;
	String_t* V_9 = NULL;
	Dictionary_2_t190145395 * V_10 = NULL;
	int32_t V_11 = 0;
	Il2CppObject * V_12 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_1 = (String_t*)NULL;
		XmlElement_t3562928333 * L_0 = ___el0;
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		XmlElement_t3562928333 * L_1 = ___el0;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_1, _stringLiteral3780601743);
		V_1 = L_2;
		String_t* L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0031;
		}
	}
	{
		TableAdapterSchemaInfo_t1131857475 * L_5 = __this->get_currentAdapter_12();
		String_t* L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		bool L_7 = Convert_ToBoolean_m309004443(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set_ShortCommands_6(L_7);
	}

IL_0031:
	{
		DbCommandInfo_t2041793742 * L_8 = (DbCommandInfo_t2041793742 *)il2cpp_codegen_object_new(DbCommandInfo_t2041793742_il2cpp_TypeInfo_var);
		DbCommandInfo__ctor_m1311026010(L_8, /*hidden argument*/NULL);
		V_3 = L_8;
		XmlElement_t3562928333 * L_9 = ___el0;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_9, _stringLiteral846162013);
		V_1 = L_10;
		String_t* L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_028a;
		}
	}
	{
		V_4 = (DbSourceMethodInfo_t3307618843 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GenerateMethodsType_t1113224938_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2778772662_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = Enum_Parse_m2929309979(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_5 = ((*(int32_t*)((int32_t*)UnBox (L_15, GenerateMethodsType_t1113224938_il2cpp_TypeInfo_var))));
		int32_t L_16 = V_5;
		if (((int32_t)((int32_t)L_16-(int32_t)1)) == 0)
		{
			goto IL_0082;
		}
		if (((int32_t)((int32_t)L_16-(int32_t)1)) == 1)
		{
			goto IL_0111;
		}
		if (((int32_t)((int32_t)L_16-(int32_t)1)) == 2)
		{
			goto IL_018c;
		}
	}
	{
		goto IL_0285;
	}

IL_0082:
	{
		DbSourceMethodInfo_t3307618843 * L_17 = (DbSourceMethodInfo_t3307618843 *)il2cpp_codegen_object_new(DbSourceMethodInfo_t3307618843_il2cpp_TypeInfo_var);
		DbSourceMethodInfo__ctor_m1314392265(L_17, /*hidden argument*/NULL);
		V_4 = L_17;
		DbSourceMethodInfo_t3307618843 * L_18 = V_4;
		XmlElement_t3562928333 * L_19 = ___el0;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_19, _stringLiteral4073017058);
		NullCheck(L_18);
		L_18->set_Name_1(L_20);
		DbSourceMethodInfo_t3307618843 * L_21 = V_4;
		XmlElement_t3562928333 * L_22 = ___el0;
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_22, _stringLiteral3449934766);
		NullCheck(L_21);
		L_21->set_Modifier_2(L_23);
		DbSourceMethodInfo_t3307618843 * L_24 = V_4;
		NullCheck(L_24);
		String_t* L_25 = L_24->get_Modifier_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_26 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00ca;
		}
	}
	{
		DbSourceMethodInfo_t3307618843 * L_27 = V_4;
		NullCheck(L_27);
		L_27->set_Modifier_2(_stringLiteral2401410697);
	}

IL_00ca:
	{
		DbSourceMethodInfo_t3307618843 * L_28 = V_4;
		XmlElement_t3562928333 * L_29 = ___el0;
		NullCheck(L_29);
		String_t* L_30 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_29, _stringLiteral563490826);
		NullCheck(L_28);
		L_28->set_ScalarCallRetval_4(L_30);
		DbSourceMethodInfo_t3307618843 * L_31 = V_4;
		XmlElement_t3562928333 * L_32 = ___el0;
		NullCheck(L_32);
		String_t* L_33 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_32, _stringLiteral197105666);
		NullCheck(L_31);
		L_31->set_QueryType_3(L_33);
		DbSourceMethodInfo_t3307618843 * L_34 = V_4;
		NullCheck(L_34);
		L_34->set_MethodType_0(1);
		DbCommandInfo_t2041793742 * L_35 = V_3;
		NullCheck(L_35);
		L_35->set_Methods_1(((DbSourceMethodInfoU5BU5D_t1609409306*)SZArrayNew(DbSourceMethodInfoU5BU5D_t1609409306_il2cpp_TypeInfo_var, (uint32_t)1)));
		DbCommandInfo_t2041793742 * L_36 = V_3;
		NullCheck(L_36);
		DbSourceMethodInfoU5BU5D_t1609409306* L_37 = L_36->get_Methods_1();
		DbSourceMethodInfo_t3307618843 * L_38 = V_4;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 0);
		ArrayElementTypeCheck (L_37, L_38);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(0), (DbSourceMethodInfo_t3307618843 *)L_38);
		goto IL_0285;
	}

IL_0111:
	{
		DbSourceMethodInfo_t3307618843 * L_39 = (DbSourceMethodInfo_t3307618843 *)il2cpp_codegen_object_new(DbSourceMethodInfo_t3307618843_il2cpp_TypeInfo_var);
		DbSourceMethodInfo__ctor_m1314392265(L_39, /*hidden argument*/NULL);
		V_4 = L_39;
		DbSourceMethodInfo_t3307618843 * L_40 = V_4;
		XmlElement_t3562928333 * L_41 = ___el0;
		NullCheck(L_41);
		String_t* L_42 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_41, _stringLiteral4052172047);
		NullCheck(L_40);
		L_40->set_Name_1(L_42);
		DbSourceMethodInfo_t3307618843 * L_43 = V_4;
		XmlElement_t3562928333 * L_44 = ___el0;
		NullCheck(L_44);
		String_t* L_45 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_44, _stringLiteral2687951707);
		NullCheck(L_43);
		L_43->set_Modifier_2(L_45);
		DbSourceMethodInfo_t3307618843 * L_46 = V_4;
		NullCheck(L_46);
		String_t* L_47 = L_46->get_Modifier_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_48 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0159;
		}
	}
	{
		DbSourceMethodInfo_t3307618843 * L_49 = V_4;
		NullCheck(L_49);
		L_49->set_Modifier_2(_stringLiteral2401410697);
	}

IL_0159:
	{
		DbSourceMethodInfo_t3307618843 * L_50 = V_4;
		NullCheck(L_50);
		L_50->set_ScalarCallRetval_4((String_t*)NULL);
		DbSourceMethodInfo_t3307618843 * L_51 = V_4;
		NullCheck(L_51);
		L_51->set_QueryType_3((String_t*)NULL);
		DbSourceMethodInfo_t3307618843 * L_52 = V_4;
		NullCheck(L_52);
		L_52->set_MethodType_0(2);
		DbCommandInfo_t2041793742 * L_53 = V_3;
		NullCheck(L_53);
		L_53->set_Methods_1(((DbSourceMethodInfoU5BU5D_t1609409306*)SZArrayNew(DbSourceMethodInfoU5BU5D_t1609409306_il2cpp_TypeInfo_var, (uint32_t)1)));
		DbCommandInfo_t2041793742 * L_54 = V_3;
		NullCheck(L_54);
		DbSourceMethodInfoU5BU5D_t1609409306* L_55 = L_54->get_Methods_1();
		DbSourceMethodInfo_t3307618843 * L_56 = V_4;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 0);
		ArrayElementTypeCheck (L_55, L_56);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(0), (DbSourceMethodInfo_t3307618843 *)L_56);
		goto IL_0285;
	}

IL_018c:
	{
		DbSourceMethodInfo_t3307618843 * L_57 = (DbSourceMethodInfo_t3307618843 *)il2cpp_codegen_object_new(DbSourceMethodInfo_t3307618843_il2cpp_TypeInfo_var);
		DbSourceMethodInfo__ctor_m1314392265(L_57, /*hidden argument*/NULL);
		V_4 = L_57;
		DbSourceMethodInfo_t3307618843 * L_58 = V_4;
		XmlElement_t3562928333 * L_59 = ___el0;
		NullCheck(L_59);
		String_t* L_60 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_59, _stringLiteral4073017058);
		NullCheck(L_58);
		L_58->set_Name_1(L_60);
		DbSourceMethodInfo_t3307618843 * L_61 = V_4;
		XmlElement_t3562928333 * L_62 = ___el0;
		NullCheck(L_62);
		String_t* L_63 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_62, _stringLiteral3449934766);
		NullCheck(L_61);
		L_61->set_Modifier_2(L_63);
		DbSourceMethodInfo_t3307618843 * L_64 = V_4;
		NullCheck(L_64);
		String_t* L_65 = L_64->get_Modifier_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_66 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_01d4;
		}
	}
	{
		DbSourceMethodInfo_t3307618843 * L_67 = V_4;
		NullCheck(L_67);
		L_67->set_Modifier_2(_stringLiteral2401410697);
	}

IL_01d4:
	{
		DbSourceMethodInfo_t3307618843 * L_68 = V_4;
		XmlElement_t3562928333 * L_69 = ___el0;
		NullCheck(L_69);
		String_t* L_70 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_69, _stringLiteral563490826);
		NullCheck(L_68);
		L_68->set_ScalarCallRetval_4(L_70);
		DbSourceMethodInfo_t3307618843 * L_71 = V_4;
		XmlElement_t3562928333 * L_72 = ___el0;
		NullCheck(L_72);
		String_t* L_73 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_72, _stringLiteral197105666);
		NullCheck(L_71);
		L_71->set_QueryType_3(L_73);
		DbSourceMethodInfo_t3307618843 * L_74 = V_4;
		NullCheck(L_74);
		L_74->set_MethodType_0(1);
		DbCommandInfo_t2041793742 * L_75 = V_3;
		NullCheck(L_75);
		L_75->set_Methods_1(((DbSourceMethodInfoU5BU5D_t1609409306*)SZArrayNew(DbSourceMethodInfoU5BU5D_t1609409306_il2cpp_TypeInfo_var, (uint32_t)2)));
		DbCommandInfo_t2041793742 * L_76 = V_3;
		NullCheck(L_76);
		DbSourceMethodInfoU5BU5D_t1609409306* L_77 = L_76->get_Methods_1();
		DbSourceMethodInfo_t3307618843 * L_78 = V_4;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, 0);
		ArrayElementTypeCheck (L_77, L_78);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(0), (DbSourceMethodInfo_t3307618843 *)L_78);
		DbSourceMethodInfo_t3307618843 * L_79 = (DbSourceMethodInfo_t3307618843 *)il2cpp_codegen_object_new(DbSourceMethodInfo_t3307618843_il2cpp_TypeInfo_var);
		DbSourceMethodInfo__ctor_m1314392265(L_79, /*hidden argument*/NULL);
		V_4 = L_79;
		DbSourceMethodInfo_t3307618843 * L_80 = V_4;
		XmlElement_t3562928333 * L_81 = ___el0;
		NullCheck(L_81);
		String_t* L_82 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_81, _stringLiteral4052172047);
		NullCheck(L_80);
		L_80->set_Name_1(L_82);
		DbSourceMethodInfo_t3307618843 * L_83 = V_4;
		XmlElement_t3562928333 * L_84 = ___el0;
		NullCheck(L_84);
		String_t* L_85 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_84, _stringLiteral2687951707);
		NullCheck(L_83);
		L_83->set_Modifier_2(L_85);
		DbSourceMethodInfo_t3307618843 * L_86 = V_4;
		NullCheck(L_86);
		String_t* L_87 = L_86->get_Modifier_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_88 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_87, /*hidden argument*/NULL);
		if (!L_88)
		{
			goto IL_025e;
		}
	}
	{
		DbSourceMethodInfo_t3307618843 * L_89 = V_4;
		NullCheck(L_89);
		L_89->set_Modifier_2(_stringLiteral2401410697);
	}

IL_025e:
	{
		DbSourceMethodInfo_t3307618843 * L_90 = V_4;
		NullCheck(L_90);
		L_90->set_ScalarCallRetval_4((String_t*)NULL);
		DbSourceMethodInfo_t3307618843 * L_91 = V_4;
		NullCheck(L_91);
		L_91->set_QueryType_3((String_t*)NULL);
		DbSourceMethodInfo_t3307618843 * L_92 = V_4;
		NullCheck(L_92);
		L_92->set_MethodType_0(2);
		DbCommandInfo_t2041793742 * L_93 = V_3;
		NullCheck(L_93);
		DbSourceMethodInfoU5BU5D_t1609409306* L_94 = L_93->get_Methods_1();
		DbSourceMethodInfo_t3307618843 * L_95 = V_4;
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, 1);
		ArrayElementTypeCheck (L_94, L_95);
		(L_94)->SetAt(static_cast<il2cpp_array_size_t>(1), (DbSourceMethodInfo_t3307618843 *)L_95);
		goto IL_0285;
	}

IL_0285:
	{
		goto IL_0314;
	}

IL_028a:
	{
		DbSourceMethodInfo_t3307618843 * L_96 = (DbSourceMethodInfo_t3307618843 *)il2cpp_codegen_object_new(DbSourceMethodInfo_t3307618843_il2cpp_TypeInfo_var);
		DbSourceMethodInfo__ctor_m1314392265(L_96, /*hidden argument*/NULL);
		V_6 = L_96;
		DbSourceMethodInfo_t3307618843 * L_97 = V_6;
		XmlElement_t3562928333 * L_98 = ___el0;
		NullCheck(L_98);
		String_t* L_99 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_98, _stringLiteral2420395);
		NullCheck(L_97);
		L_97->set_Name_1(L_99);
		DbSourceMethodInfo_t3307618843 * L_100 = V_6;
		XmlElement_t3562928333 * L_101 = ___el0;
		NullCheck(L_101);
		String_t* L_102 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_101, _stringLiteral3744098039);
		NullCheck(L_100);
		L_100->set_Modifier_2(L_102);
		DbSourceMethodInfo_t3307618843 * L_103 = V_6;
		NullCheck(L_103);
		String_t* L_104 = L_103->get_Modifier_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_105 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_104, /*hidden argument*/NULL);
		if (!L_105)
		{
			goto IL_02d2;
		}
	}
	{
		DbSourceMethodInfo_t3307618843 * L_106 = V_6;
		NullCheck(L_106);
		L_106->set_Modifier_2(_stringLiteral2401410697);
	}

IL_02d2:
	{
		DbSourceMethodInfo_t3307618843 * L_107 = V_6;
		XmlElement_t3562928333 * L_108 = ___el0;
		NullCheck(L_108);
		String_t* L_109 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_108, _stringLiteral563490826);
		NullCheck(L_107);
		L_107->set_ScalarCallRetval_4(L_109);
		DbSourceMethodInfo_t3307618843 * L_110 = V_6;
		XmlElement_t3562928333 * L_111 = ___el0;
		NullCheck(L_111);
		String_t* L_112 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_111, _stringLiteral197105666);
		NullCheck(L_110);
		L_110->set_QueryType_3(L_112);
		DbSourceMethodInfo_t3307618843 * L_113 = V_6;
		NullCheck(L_113);
		L_113->set_MethodType_0(0);
		DbCommandInfo_t2041793742 * L_114 = V_3;
		NullCheck(L_114);
		L_114->set_Methods_1(((DbSourceMethodInfoU5BU5D_t1609409306*)SZArrayNew(DbSourceMethodInfoU5BU5D_t1609409306_il2cpp_TypeInfo_var, (uint32_t)1)));
		DbCommandInfo_t2041793742 * L_115 = V_3;
		NullCheck(L_115);
		DbSourceMethodInfoU5BU5D_t1609409306* L_116 = L_115->get_Methods_1();
		DbSourceMethodInfo_t3307618843 * L_117 = V_6;
		NullCheck(L_116);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_116, 0);
		ArrayElementTypeCheck (L_116, L_117);
		(L_116)->SetAt(static_cast<il2cpp_array_size_t>(0), (DbSourceMethodInfo_t3307618843 *)L_117);
	}

IL_0314:
	{
		XmlElement_t3562928333 * L_118 = ___el0;
		NullCheck(L_118);
		XmlNodeList_t3966370975 * L_119 = VirtFuncInvoker0< XmlNodeList_t3966370975 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_118);
		NullCheck(L_119);
		Il2CppObject * L_120 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_119);
		V_7 = L_120;
	}

IL_0321:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0472;
		}

IL_0326:
		{
			Il2CppObject * L_121 = V_7;
			NullCheck(L_121);
			Il2CppObject * L_122 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_121);
			V_8 = ((XmlNode_t3592213601 *)CastclassClass(L_122, XmlNode_t3592213601_il2cpp_TypeInfo_var));
			XmlNode_t3592213601 * L_123 = V_8;
			V_2 = ((XmlElement_t3562928333 *)IsInstClass(L_123, XmlElement_t3562928333_il2cpp_TypeInfo_var));
			XmlElement_t3562928333 * L_124 = V_2;
			if (L_124)
			{
				goto IL_0347;
			}
		}

IL_0342:
		{
			goto IL_0472;
		}

IL_0347:
		{
			XmlElement_t3562928333 * L_125 = V_2;
			NullCheck(L_125);
			String_t* L_126 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_125);
			V_9 = L_126;
			String_t* L_127 = V_9;
			if (!L_127)
			{
				goto IL_0472;
			}
		}

IL_0356:
		{
			IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
			Dictionary_2_t190145395 * L_128 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map10_18();
			if (L_128)
			{
				goto IL_03a3;
			}
		}

IL_0360:
		{
			Dictionary_2_t190145395 * L_129 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
			Dictionary_2__ctor_m1958628151(L_129, 4, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
			V_10 = L_129;
			Dictionary_2_t190145395 * L_130 = V_10;
			NullCheck(L_130);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_130, _stringLiteral2472812175, 0);
			Dictionary_2_t190145395 * L_131 = V_10;
			NullCheck(L_131);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_131, _stringLiteral1237848626, 1);
			Dictionary_2_t190145395 * L_132 = V_10;
			NullCheck(L_132);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_132, _stringLiteral253405730, 2);
			Dictionary_2_t190145395 * L_133 = V_10;
			NullCheck(L_133);
			VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_133, _stringLiteral1544583936, 3);
			Dictionary_2_t190145395 * L_134 = V_10;
			IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
			((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map10_18(L_134);
		}

IL_03a3:
		{
			IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var);
			Dictionary_2_t190145395 * L_135 = ((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map10_18();
			String_t* L_136 = V_9;
			NullCheck(L_135);
			bool L_137 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(35 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_135, L_136, (&V_11));
			if (!L_137)
			{
				goto IL_0472;
			}
		}

IL_03b6:
		{
			int32_t L_138 = V_11;
			if (L_138 == 0)
			{
				goto IL_03d2;
			}
			if (L_138 == 1)
			{
				goto IL_0400;
			}
			if (L_138 == 2)
			{
				goto IL_0426;
			}
			if (L_138 == 3)
			{
				goto IL_044c;
			}
		}

IL_03cd:
		{
			goto IL_0472;
		}

IL_03d2:
		{
			DbCommandInfo_t2041793742 * L_139 = V_3;
			XmlElement_t3562928333 * L_140 = V_2;
			NullCheck(L_140);
			XmlNode_t3592213601 * L_141 = VirtFuncInvoker0< XmlNode_t3592213601 * >::Invoke(10 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_140);
			DbCommand_t2323745021 * L_142 = XmlSchemaDataImporter_ProcessDbCommand_m2276008986(__this, ((XmlElement_t3562928333 *)IsInstClass(L_141, XmlElement_t3562928333_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			NullCheck(L_139);
			L_139->set_Command_0(L_142);
			TableAdapterSchemaInfo_t1131857475 * L_143 = __this->get_currentAdapter_12();
			NullCheck(L_143);
			ArrayList_t2121638921 * L_144 = L_143->get_Commands_7();
			DbCommandInfo_t2041793742 * L_145 = V_3;
			NullCheck(L_144);
			VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_144, L_145);
			goto IL_0472;
		}

IL_0400:
		{
			TableAdapterSchemaInfo_t1131857475 * L_146 = __this->get_currentAdapter_12();
			NullCheck(L_146);
			DbDataAdapter_t3684585719 * L_147 = L_146->get_Adapter_1();
			XmlElement_t3562928333 * L_148 = V_2;
			NullCheck(L_148);
			XmlNode_t3592213601 * L_149 = VirtFuncInvoker0< XmlNode_t3592213601 * >::Invoke(10 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_148);
			DbCommand_t2323745021 * L_150 = XmlSchemaDataImporter_ProcessDbCommand_m2276008986(__this, ((XmlElement_t3562928333 *)IsInstClass(L_149, XmlElement_t3562928333_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			NullCheck(L_147);
			DbDataAdapter_set_InsertCommand_m4189177489(L_147, L_150, /*hidden argument*/NULL);
			goto IL_0472;
		}

IL_0426:
		{
			TableAdapterSchemaInfo_t1131857475 * L_151 = __this->get_currentAdapter_12();
			NullCheck(L_151);
			DbDataAdapter_t3684585719 * L_152 = L_151->get_Adapter_1();
			XmlElement_t3562928333 * L_153 = V_2;
			NullCheck(L_153);
			XmlNode_t3592213601 * L_154 = VirtFuncInvoker0< XmlNode_t3592213601 * >::Invoke(10 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_153);
			DbCommand_t2323745021 * L_155 = XmlSchemaDataImporter_ProcessDbCommand_m2276008986(__this, ((XmlElement_t3562928333 *)IsInstClass(L_154, XmlElement_t3562928333_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			NullCheck(L_152);
			DbDataAdapter_set_UpdateCommand_m199088769(L_152, L_155, /*hidden argument*/NULL);
			goto IL_0472;
		}

IL_044c:
		{
			TableAdapterSchemaInfo_t1131857475 * L_156 = __this->get_currentAdapter_12();
			NullCheck(L_156);
			DbDataAdapter_t3684585719 * L_157 = L_156->get_Adapter_1();
			XmlElement_t3562928333 * L_158 = V_2;
			NullCheck(L_158);
			XmlNode_t3592213601 * L_159 = VirtFuncInvoker0< XmlNode_t3592213601 * >::Invoke(10 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_158);
			DbCommand_t2323745021 * L_160 = XmlSchemaDataImporter_ProcessDbCommand_m2276008986(__this, ((XmlElement_t3562928333 *)IsInstClass(L_159, XmlElement_t3562928333_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			NullCheck(L_157);
			DbDataAdapter_set_DeleteCommand_m1118130911(L_157, L_160, /*hidden argument*/NULL);
			goto IL_0472;
		}

IL_0472:
		{
			Il2CppObject * L_161 = V_7;
			NullCheck(L_161);
			bool L_162 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_161);
			if (L_162)
			{
				goto IL_0326;
			}
		}

IL_047e:
		{
			IL2CPP_LEAVE(0x49A, FINALLY_0483);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0483;
	}

FINALLY_0483:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_163 = V_7;
			Il2CppObject * L_164 = ((Il2CppObject *)IsInst(L_163, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_12 = L_164;
			if (!L_164)
			{
				goto IL_0499;
			}
		}

IL_0492:
		{
			Il2CppObject * L_165 = V_12;
			NullCheck(L_165);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_165);
		}

IL_0499:
		{
			IL2CPP_END_FINALLY(1155)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1155)
	{
		IL2CPP_JUMP_TBL(0x49A, IL_049a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_049a:
	{
		return;
	}
}
// System.Data.Common.DbCommand System.Data.XmlSchemaDataImporter::ProcessDbCommand(System.Xml.XmlElement)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNode_t3592213601_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlElement_t3562928333_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral206630149;
extern Il2CppCodeGenString* _stringLiteral206611192;
extern Il2CppCodeGenString* _stringLiteral2452200970;
extern Il2CppCodeGenString* _stringLiteral3013844560;
extern const uint32_t XmlSchemaDataImporter_ProcessDbCommand_m2276008986_MetadataUsageId;
extern "C"  DbCommand_t2323745021 * XmlSchemaDataImporter_ProcessDbCommand_m2276008986 (XmlSchemaDataImporter_t2811591463 * __this, XmlElement_t3562928333 * ___el0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ProcessDbCommand_m2276008986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlElement_t3562928333 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	ArrayList_t2121638921 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	XmlNode_t3592213601 * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	DbCommand_t2323745021 * V_7 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_1 = (String_t*)NULL;
		V_2 = (String_t*)NULL;
		V_3 = (ArrayList_t2121638921 *)NULL;
		XmlElement_t3562928333 * L_0 = ___el0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		return (DbCommand_t2323745021 *)NULL;
	}

IL_000e:
	{
		XmlElement_t3562928333 * L_1 = ___el0;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_1, _stringLiteral206630149);
		V_2 = L_2;
		XmlElement_t3562928333 * L_3 = ___el0;
		NullCheck(L_3);
		XmlNodeList_t3966370975 * L_4 = VirtFuncInvoker0< XmlNodeList_t3966370975 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_3);
		NullCheck(L_4);
		Il2CppObject * L_5 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_4);
		V_4 = L_5;
	}

IL_0027:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0097;
		}

IL_002c:
		{
			Il2CppObject * L_6 = V_4;
			NullCheck(L_6);
			Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_6);
			V_5 = ((XmlNode_t3592213601 *)CastclassClass(L_7, XmlNode_t3592213601_il2cpp_TypeInfo_var));
			XmlNode_t3592213601 * L_8 = V_5;
			V_0 = ((XmlElement_t3562928333 *)IsInstClass(L_8, XmlElement_t3562928333_il2cpp_TypeInfo_var));
			XmlElement_t3562928333 * L_9 = V_0;
			if (!L_9)
			{
				goto IL_0069;
			}
		}

IL_0048:
		{
			XmlElement_t3562928333 * L_10 = V_0;
			NullCheck(L_10);
			String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_10);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_12 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_11, _stringLiteral206611192, /*hidden argument*/NULL);
			if (!L_12)
			{
				goto IL_0069;
			}
		}

IL_005d:
		{
			XmlElement_t3562928333 * L_13 = V_0;
			NullCheck(L_13);
			String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String System.Xml.XmlNode::get_InnerText() */, L_13);
			V_1 = L_14;
			goto IL_0097;
		}

IL_0069:
		{
			XmlElement_t3562928333 * L_15 = V_0;
			if (!L_15)
			{
				goto IL_0097;
			}
		}

IL_006f:
		{
			XmlElement_t3562928333 * L_16 = V_0;
			NullCheck(L_16);
			String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Xml.XmlNode::get_LocalName() */, L_16);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_18 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_17, _stringLiteral2452200970, /*hidden argument*/NULL);
			if (!L_18)
			{
				goto IL_0097;
			}
		}

IL_0084:
		{
			XmlElement_t3562928333 * L_19 = V_0;
			NullCheck(L_19);
			bool L_20 = XmlElement_get_IsEmpty_m1811931046(L_19, /*hidden argument*/NULL);
			if (L_20)
			{
				goto IL_0097;
			}
		}

IL_008f:
		{
			XmlElement_t3562928333 * L_21 = V_0;
			ArrayList_t2121638921 * L_22 = XmlSchemaDataImporter_ProcessDbParameters_m4109702477(__this, L_21, /*hidden argument*/NULL);
			V_3 = L_22;
		}

IL_0097:
		{
			Il2CppObject * L_23 = V_4;
			NullCheck(L_23);
			bool L_24 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_23);
			if (L_24)
			{
				goto IL_002c;
			}
		}

IL_00a3:
		{
			IL2CPP_LEAVE(0xBF, FINALLY_00a8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00a8;
	}

FINALLY_00a8:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_25 = V_4;
			Il2CppObject * L_26 = ((Il2CppObject *)IsInst(L_25, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_6 = L_26;
			if (!L_26)
			{
				goto IL_00be;
			}
		}

IL_00b7:
		{
			Il2CppObject * L_27 = V_6;
			NullCheck(L_27);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_27);
		}

IL_00be:
		{
			IL2CPP_END_FINALLY(168)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(168)
	{
		IL2CPP_JUMP_TBL(0xBF, IL_00bf)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00bf:
	{
		TableAdapterSchemaInfo_t1131857475 * L_28 = __this->get_currentAdapter_12();
		NullCheck(L_28);
		DbProviderFactory_t2435213707 * L_29 = L_28->get_Provider_0();
		NullCheck(L_29);
		DbCommand_t2323745021 * L_30 = VirtFuncInvoker0< DbCommand_t2323745021 * >::Invoke(4 /* System.Data.Common.DbCommand System.Data.Common.DbProviderFactory::CreateCommand() */, L_29);
		V_7 = L_30;
		DbCommand_t2323745021 * L_31 = V_7;
		String_t* L_32 = V_1;
		NullCheck(L_31);
		VirtActionInvoker1< String_t* >::Invoke(18 /* System.Void System.Data.Common.DbCommand::set_CommandText(System.String) */, L_31, L_32);
		String_t* L_33 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_34 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_33, _stringLiteral3013844560, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00f6;
		}
	}
	{
		DbCommand_t2323745021 * L_35 = V_7;
		NullCheck(L_35);
		VirtActionInvoker1< int32_t >::Invoke(21 /* System.Void System.Data.Common.DbCommand::set_CommandType(System.Data.CommandType) */, L_35, 4);
		goto IL_00fe;
	}

IL_00f6:
	{
		DbCommand_t2323745021 * L_36 = V_7;
		NullCheck(L_36);
		VirtActionInvoker1< int32_t >::Invoke(21 /* System.Void System.Data.Common.DbCommand::set_CommandType(System.Data.CommandType) */, L_36, 1);
	}

IL_00fe:
	{
		ArrayList_t2121638921 * L_37 = V_3;
		if (!L_37)
		{
			goto IL_0116;
		}
	}
	{
		DbCommand_t2323745021 * L_38 = V_7;
		NullCheck(L_38);
		DbParameterCollection_t3381130713 * L_39 = DbCommand_get_Parameters_m3316044920(L_38, /*hidden argument*/NULL);
		ArrayList_t2121638921 * L_40 = V_3;
		NullCheck(L_40);
		ObjectU5BU5D_t11523773* L_41 = VirtFuncInvoker0< ObjectU5BU5D_t11523773* >::Invoke(47 /* System.Object[] System.Collections.ArrayList::ToArray() */, L_40);
		NullCheck(L_39);
		VirtActionInvoker1< Il2CppArray * >::Invoke(28 /* System.Void System.Data.Common.DbParameterCollection::AddRange(System.Array) */, L_39, (Il2CppArray *)(Il2CppArray *)L_41);
	}

IL_0116:
	{
		DbCommand_t2323745021 * L_42 = V_7;
		return L_42;
	}
}
// System.Collections.ArrayList System.Data.XmlSchemaDataImporter::ProcessDbParameters(System.Xml.XmlElement)
extern const Il2CppType* ParameterDirection_t608485289_0_0_0_var;
extern const Il2CppType* DataRowVersion_t2975473339_0_0_0_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNode_t3592213601_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlElement_t3562928333_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2778772662_il2cpp_TypeInfo_var;
extern Il2CppClass* ParameterDirection_t608485289_il2cpp_TypeInfo_var;
extern Il2CppClass* IDbDataParameter_t14893419_il2cpp_TypeInfo_var;
extern Il2CppClass* DataRowVersion_t2975473339_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2233971630;
extern Il2CppCodeGenString* _stringLiteral419354036;
extern Il2CppCodeGenString* _stringLiteral4040228075;
extern Il2CppCodeGenString* _stringLiteral2039909624;
extern Il2CppCodeGenString* _stringLiteral1041377119;
extern Il2CppCodeGenString* _stringLiteral627790942;
extern Il2CppCodeGenString* _stringLiteral79698218;
extern Il2CppCodeGenString* _stringLiteral2577441;
extern Il2CppCodeGenString* _stringLiteral1029290865;
extern Il2CppCodeGenString* _stringLiteral1115619606;
extern Il2CppCodeGenString* _stringLiteral1245132829;
extern const uint32_t XmlSchemaDataImporter_ProcessDbParameters_m4109702477_MetadataUsageId;
extern "C"  ArrayList_t2121638921 * XmlSchemaDataImporter_ProcessDbParameters_m4109702477 (XmlSchemaDataImporter_t2811591463 * __this, XmlElement_t3562928333 * ___el0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ProcessDbParameters_m4109702477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	XmlElement_t3562928333 * V_1 = NULL;
	DbParameter_t3306161371 * V_2 = NULL;
	ArrayList_t2121638921 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	XmlNode_t3592213601 * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (String_t*)NULL;
		V_2 = (DbParameter_t3306161371 *)NULL;
		ArrayList_t2121638921 * L_0 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		V_3 = L_0;
		XmlElement_t3562928333 * L_1 = ___el0;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		ArrayList_t2121638921 * L_2 = V_3;
		return L_2;
	}

IL_0012:
	{
		XmlElement_t3562928333 * L_3 = ___el0;
		NullCheck(L_3);
		XmlNodeList_t3966370975 * L_4 = VirtFuncInvoker0< XmlNodeList_t3966370975 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_3);
		NullCheck(L_4);
		Il2CppObject * L_5 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_4);
		V_4 = L_5;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0180;
		}

IL_0024:
		{
			Il2CppObject * L_6 = V_4;
			NullCheck(L_6);
			Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_6);
			V_5 = ((XmlNode_t3592213601 *)CastclassClass(L_7, XmlNode_t3592213601_il2cpp_TypeInfo_var));
			XmlNode_t3592213601 * L_8 = V_5;
			V_1 = ((XmlElement_t3562928333 *)IsInstClass(L_8, XmlElement_t3562928333_il2cpp_TypeInfo_var));
			XmlElement_t3562928333 * L_9 = V_1;
			if (L_9)
			{
				goto IL_0045;
			}
		}

IL_0040:
		{
			goto IL_0180;
		}

IL_0045:
		{
			TableAdapterSchemaInfo_t1131857475 * L_10 = __this->get_currentAdapter_12();
			NullCheck(L_10);
			DbProviderFactory_t2435213707 * L_11 = L_10->get_Provider_0();
			NullCheck(L_11);
			DbParameter_t3306161371 * L_12 = VirtFuncInvoker0< DbParameter_t3306161371 * >::Invoke(7 /* System.Data.Common.DbParameter System.Data.Common.DbProviderFactory::CreateParameter() */, L_11);
			V_2 = L_12;
			XmlElement_t3562928333 * L_13 = V_1;
			NullCheck(L_13);
			String_t* L_14 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_13, _stringLiteral2233971630);
			V_0 = L_14;
			String_t* L_15 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_16 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
			if (L_16)
			{
				goto IL_0079;
			}
		}

IL_006d:
		{
			DbParameter_t3306161371 * L_17 = V_2;
			String_t* L_18 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
			bool L_19 = Convert_ToBoolean_m309004443(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
			NullCheck(L_17);
			VirtActionInvoker1< bool >::Invoke(18 /* System.Void System.Data.Common.DbParameter::set_IsNullable(System.Boolean) */, L_17, L_19);
		}

IL_0079:
		{
			DbParameter_t3306161371 * L_20 = V_2;
			XmlElement_t3562928333 * L_21 = V_1;
			NullCheck(L_21);
			String_t* L_22 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_21, _stringLiteral419354036);
			NullCheck(L_20);
			VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.Data.Common.DbParameter::set_ParameterName(System.String) */, L_20, L_22);
			XmlElement_t3562928333 * L_23 = V_1;
			NullCheck(L_23);
			String_t* L_24 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_23, _stringLiteral4040228075);
			V_0 = L_24;
			String_t* L_25 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_26 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
			if (L_26)
			{
				goto IL_00ad;
			}
		}

IL_00a1:
		{
			XmlElement_t3562928333 * L_27 = V_1;
			NullCheck(L_27);
			String_t* L_28 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_27, _stringLiteral2039909624);
			V_0 = L_28;
		}

IL_00ad:
		{
			DbParameter_t3306161371 * L_29 = V_2;
			String_t* L_30 = V_0;
			NullCheck(L_29);
			VirtActionInvoker1< Il2CppObject * >::Invoke(24 /* System.Void System.Data.Common.DbParameter::set_FrameworkDbType(System.Object) */, L_29, L_30);
			XmlElement_t3562928333 * L_31 = V_1;
			NullCheck(L_31);
			String_t* L_32 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_31, _stringLiteral1041377119);
			V_0 = L_32;
			DbParameter_t3306161371 * L_33 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_34 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ParameterDirection_t608485289_0_0_0_var), /*hidden argument*/NULL);
			String_t* L_35 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Enum_t2778772662_il2cpp_TypeInfo_var);
			Il2CppObject * L_36 = Enum_Parse_m2929309979(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
			NullCheck(L_33);
			VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void System.Data.Common.DbParameter::set_Direction(System.Data.ParameterDirection) */, L_33, ((*(int32_t*)((int32_t*)UnBox (L_36, ParameterDirection_t608485289_il2cpp_TypeInfo_var)))));
			DbParameter_t3306161371 * L_37 = V_2;
			XmlElement_t3562928333 * L_38 = V_1;
			NullCheck(L_38);
			String_t* L_39 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_38, _stringLiteral627790942);
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
			uint8_t L_40 = Convert_ToByte_m25971949(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
			NullCheck(L_37);
			InterfaceActionInvoker1< uint8_t >::Invoke(0 /* System.Void System.Data.IDbDataParameter::set_Precision(System.Byte) */, IDbDataParameter_t14893419_il2cpp_TypeInfo_var, L_37, L_40);
			DbParameter_t3306161371 * L_41 = V_2;
			XmlElement_t3562928333 * L_42 = V_1;
			NullCheck(L_42);
			String_t* L_43 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_42, _stringLiteral79698218);
			uint8_t L_44 = Convert_ToByte_m25971949(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
			NullCheck(L_41);
			InterfaceActionInvoker1< uint8_t >::Invoke(1 /* System.Void System.Data.IDbDataParameter::set_Scale(System.Byte) */, IDbDataParameter_t14893419_il2cpp_TypeInfo_var, L_41, L_44);
			DbParameter_t3306161371 * L_45 = V_2;
			XmlElement_t3562928333 * L_46 = V_1;
			NullCheck(L_46);
			String_t* L_47 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_46, _stringLiteral2577441);
			int32_t L_48 = Convert_ToInt32_m4085381007(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
			NullCheck(L_45);
			VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void System.Data.Common.DbParameter::set_Size(System.Int32) */, L_45, L_48);
			DbParameter_t3306161371 * L_49 = V_2;
			XmlElement_t3562928333 * L_50 = V_1;
			NullCheck(L_50);
			String_t* L_51 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_50, _stringLiteral1029290865);
			NullCheck(L_49);
			VirtActionInvoker1< String_t* >::Invoke(20 /* System.Void System.Data.Common.DbParameter::set_SourceColumn(System.String) */, L_49, L_51);
			XmlElement_t3562928333 * L_52 = V_1;
			NullCheck(L_52);
			String_t* L_53 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_52, _stringLiteral1115619606);
			V_0 = L_53;
			String_t* L_54 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_55 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
			if (L_55)
			{
				goto IL_0151;
			}
		}

IL_0145:
		{
			DbParameter_t3306161371 * L_56 = V_2;
			String_t* L_57 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
			bool L_58 = Convert_ToBoolean_m309004443(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
			NullCheck(L_56);
			VirtActionInvoker1< bool >::Invoke(21 /* System.Void System.Data.Common.DbParameter::set_SourceColumnNullMapping(System.Boolean) */, L_56, L_58);
		}

IL_0151:
		{
			XmlElement_t3562928333 * L_59 = V_1;
			NullCheck(L_59);
			String_t* L_60 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_59, _stringLiteral1245132829);
			V_0 = L_60;
			DbParameter_t3306161371 * L_61 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_62 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DataRowVersion_t2975473339_0_0_0_var), /*hidden argument*/NULL);
			String_t* L_63 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Enum_t2778772662_il2cpp_TypeInfo_var);
			Il2CppObject * L_64 = Enum_Parse_m2929309979(NULL /*static, unused*/, L_62, L_63, /*hidden argument*/NULL);
			NullCheck(L_61);
			VirtActionInvoker1< int32_t >::Invoke(23 /* System.Void System.Data.Common.DbParameter::set_SourceVersion(System.Data.DataRowVersion) */, L_61, ((*(int32_t*)((int32_t*)UnBox (L_64, DataRowVersion_t2975473339_il2cpp_TypeInfo_var)))));
			ArrayList_t2121638921 * L_65 = V_3;
			DbParameter_t3306161371 * L_66 = V_2;
			NullCheck(L_65);
			VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_65, L_66);
		}

IL_0180:
		{
			Il2CppObject * L_67 = V_4;
			NullCheck(L_67);
			bool L_68 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_67);
			if (L_68)
			{
				goto IL_0024;
			}
		}

IL_018c:
		{
			IL2CPP_LEAVE(0x1A8, FINALLY_0191);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0191;
	}

FINALLY_0191:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_69 = V_4;
			Il2CppObject * L_70 = ((Il2CppObject *)IsInst(L_69, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_6 = L_70;
			if (!L_70)
			{
				goto IL_01a7;
			}
		}

IL_01a0:
		{
			Il2CppObject * L_71 = V_6;
			NullCheck(L_71);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_71);
		}

IL_01a7:
		{
			IL2CPP_END_FINALLY(401)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(401)
	{
		IL2CPP_JUMP_TBL(0x1A8, IL_01a8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01a8:
	{
		ArrayList_t2121638921 * L_72 = V_3;
		return L_72;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::ProcessColumnMapping(System.Xml.XmlElement,System.Data.Common.DataTableMapping)
extern Il2CppCodeGenString* _stringLiteral1029290865;
extern Il2CppCodeGenString* _stringLiteral3415181742;
extern const uint32_t XmlSchemaDataImporter_ProcessColumnMapping_m1831631887_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_ProcessColumnMapping_m1831631887 (XmlSchemaDataImporter_t2811591463 * __this, XmlElement_t3562928333 * ___el0, DataTableMapping_t171110970 * ___tableMapping1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_ProcessColumnMapping_m1831631887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlElement_t3562928333 * L_0 = ___el0;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		DataTableMapping_t171110970 * L_1 = ___tableMapping1;
		NullCheck(L_1);
		DataColumnMappingCollection_t30373468 * L_2 = DataTableMapping_get_ColumnMappings_m2821688027(L_1, /*hidden argument*/NULL);
		XmlElement_t3562928333 * L_3 = ___el0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_3, _stringLiteral1029290865);
		XmlElement_t3562928333 * L_5 = ___el0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_5, _stringLiteral3415181742);
		NullCheck(L_2);
		DataColumnMappingCollection_Add_m2718598938(L_2, L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::HandleRelationshipAnnotation(System.Xml.XmlElement,System.Boolean)
extern Il2CppClass* RelationStructure_t3039531114_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3299543210;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral94631196;
extern Il2CppCodeGenString* _stringLiteral2070338933;
extern Il2CppCodeGenString* _stringLiteral1659519939;
extern const uint32_t XmlSchemaDataImporter_HandleRelationshipAnnotation_m1112614008_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter_HandleRelationshipAnnotation_m1112614008 (XmlSchemaDataImporter_t2811591463 * __this, XmlElement_t3562928333 * ___el0, bool ___nested1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_HandleRelationshipAnnotation_m1112614008_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	RelationStructure_t3039531114 * V_5 = NULL;
	{
		XmlElement_t3562928333 * L_0 = ___el0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(40 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_0, _stringLiteral3373707);
		V_0 = L_1;
		XmlElement_t3562928333 * L_2 = ___el0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(41 /* System.String System.Xml.XmlElement::GetAttribute(System.String,System.String) */, L_2, _stringLiteral3299543210, _stringLiteral3051173506);
		V_1 = L_3;
		XmlElement_t3562928333 * L_4 = ___el0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(41 /* System.String System.Xml.XmlElement::GetAttribute(System.String,System.String) */, L_4, _stringLiteral94631196, _stringLiteral3051173506);
		V_2 = L_5;
		XmlElement_t3562928333 * L_6 = ___el0;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(41 /* System.String System.Xml.XmlElement::GetAttribute(System.String,System.String) */, L_6, _stringLiteral2070338933, _stringLiteral3051173506);
		V_3 = L_7;
		XmlElement_t3562928333 * L_8 = ___el0;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(41 /* System.String System.Xml.XmlElement::GetAttribute(System.String,System.String) */, L_8, _stringLiteral1659519939, _stringLiteral3051173506);
		V_4 = L_9;
		RelationStructure_t3039531114 * L_10 = (RelationStructure_t3039531114 *)il2cpp_codegen_object_new(RelationStructure_t3039531114_il2cpp_TypeInfo_var);
		RelationStructure__ctor_m1596628542(L_10, /*hidden argument*/NULL);
		V_5 = L_10;
		RelationStructure_t3039531114 * L_11 = V_5;
		String_t* L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_13 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		L_11->set_ExplicitName_0(L_13);
		RelationStructure_t3039531114 * L_14 = V_5;
		String_t* L_15 = V_1;
		String_t* L_16 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		L_14->set_ParentTableName_1(L_16);
		RelationStructure_t3039531114 * L_17 = V_5;
		String_t* L_18 = V_2;
		String_t* L_19 = XmlHelper_Decode_m2147947678(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		L_17->set_ChildTableName_2(L_19);
		RelationStructure_t3039531114 * L_20 = V_5;
		String_t* L_21 = V_3;
		NullCheck(L_20);
		L_20->set_ParentColumnName_3(L_21);
		RelationStructure_t3039531114 * L_22 = V_5;
		String_t* L_23 = V_4;
		NullCheck(L_22);
		L_22->set_ChildColumnName_4(L_23);
		RelationStructure_t3039531114 * L_24 = V_5;
		bool L_25 = ___nested1;
		NullCheck(L_24);
		L_24->set_IsNested_5(L_25);
		RelationStructure_t3039531114 * L_26 = V_5;
		NullCheck(L_26);
		L_26->set_CreateConstraint_6((bool)0);
		ArrayList_t2121638921 * L_27 = __this->get_relations_6();
		RelationStructure_t3039531114 * L_28 = V_5;
		NullCheck(L_27);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_27, L_28);
		return;
	}
}
// System.Object System.Data.XmlSchemaDataImporter::GetElementDefaultValue(System.Xml.Schema.XmlSchemaElement)
extern Il2CppClass* XmlQualifiedName_t176365656_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaElement_t471922321_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_GetElementDefaultValue_m3219825097_MetadataUsageId;
extern "C"  Il2CppObject * XmlSchemaDataImporter_GetElementDefaultValue_m3219825097 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaElement_t471922321 * ___elem0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_GetElementDefaultValue_m3219825097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlSchemaElement_t471922321 * V_0 = NULL;
	{
		XmlSchemaElement_t471922321 * L_0 = ___elem0;
		NullCheck(L_0);
		XmlQualifiedName_t176365656 * L_1 = XmlSchemaElement_get_RefName_m3872471184(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_2 = ((XmlQualifiedName_t176365656_StaticFields*)XmlQualifiedName_t176365656_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		bool L_3 = XmlQualifiedName_op_Equality_m273752697(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		XmlSchemaElement_t471922321 * L_4 = ___elem0;
		NullCheck(L_4);
		String_t* L_5 = XmlSchemaElement_get_DefaultValue_m1707025846(L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001c:
	{
		XmlSchema_t1932230565 * L_6 = __this->get_schema_5();
		NullCheck(L_6);
		XmlSchemaObjectTable_t167066468 * L_7 = XmlSchema_get_Elements_m1404485665(L_6, /*hidden argument*/NULL);
		XmlSchemaElement_t471922321 * L_8 = ___elem0;
		NullCheck(L_8);
		XmlQualifiedName_t176365656 * L_9 = XmlSchemaElement_get_RefName_m3872471184(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		XmlSchemaObject_t2900481284 * L_10 = XmlSchemaObjectTable_get_Item_m2018580464(L_7, L_9, /*hidden argument*/NULL);
		V_0 = ((XmlSchemaElement_t471922321 *)IsInstClass(L_10, XmlSchemaElement_t471922321_il2cpp_TypeInfo_var));
		XmlSchemaElement_t471922321 * L_11 = V_0;
		if (L_11)
		{
			goto IL_0040;
		}
	}
	{
		return NULL;
	}

IL_0040:
	{
		XmlSchemaElement_t471922321 * L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = XmlSchemaElement_get_DefaultValue_m1707025846(L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Object System.Data.XmlSchemaDataImporter::GetAttributeDefaultValue(System.Xml.Schema.XmlSchemaAttribute)
extern Il2CppClass* XmlQualifiedName_t176365656_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaAttribute_t1191708721_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaDataImporter_GetAttributeDefaultValue_m1546929417_MetadataUsageId;
extern "C"  Il2CppObject * XmlSchemaDataImporter_GetAttributeDefaultValue_m1546929417 (XmlSchemaDataImporter_t2811591463 * __this, XmlSchemaAttribute_t1191708721 * ___attr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter_GetAttributeDefaultValue_m1546929417_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlSchemaAttribute_t1191708721 * V_0 = NULL;
	{
		XmlSchemaAttribute_t1191708721 * L_0 = ___attr0;
		NullCheck(L_0);
		String_t* L_1 = XmlSchemaAttribute_get_DefaultValue_m3443587414(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		XmlSchemaAttribute_t1191708721 * L_2 = ___attr0;
		NullCheck(L_2);
		String_t* L_3 = XmlSchemaAttribute_get_DefaultValue_m3443587414(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		XmlSchemaAttribute_t1191708721 * L_4 = ___attr0;
		NullCheck(L_4);
		String_t* L_5 = XmlSchemaAttribute_get_FixedValue_m2020843011(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0024;
		}
	}
	{
		XmlSchemaAttribute_t1191708721 * L_6 = ___attr0;
		NullCheck(L_6);
		String_t* L_7 = XmlSchemaAttribute_get_FixedValue_m2020843011(L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0024:
	{
		XmlSchemaAttribute_t1191708721 * L_8 = ___attr0;
		NullCheck(L_8);
		XmlQualifiedName_t176365656 * L_9 = XmlSchemaAttribute_get_RefName_m3633453936(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_10 = ((XmlQualifiedName_t176365656_StaticFields*)XmlQualifiedName_t176365656_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		bool L_11 = XmlQualifiedName_op_Equality_m273752697(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_003b;
		}
	}
	{
		return NULL;
	}

IL_003b:
	{
		XmlSchema_t1932230565 * L_12 = __this->get_schema_5();
		NullCheck(L_12);
		XmlSchemaObjectTable_t167066468 * L_13 = XmlSchema_get_Attributes_m631487041(L_12, /*hidden argument*/NULL);
		XmlSchemaAttribute_t1191708721 * L_14 = ___attr0;
		NullCheck(L_14);
		XmlQualifiedName_t176365656 * L_15 = XmlSchemaAttribute_get_RefName_m3633453936(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		XmlSchemaObject_t2900481284 * L_16 = XmlSchemaObjectTable_get_Item_m2018580464(L_13, L_15, /*hidden argument*/NULL);
		V_0 = ((XmlSchemaAttribute_t1191708721 *)IsInstClass(L_16, XmlSchemaAttribute_t1191708721_il2cpp_TypeInfo_var));
		XmlSchemaAttribute_t1191708721 * L_17 = V_0;
		if (L_17)
		{
			goto IL_005f;
		}
	}
	{
		return NULL;
	}

IL_005f:
	{
		XmlSchemaAttribute_t1191708721 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = XmlSchemaAttribute_get_DefaultValue_m3443587414(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0071;
		}
	}
	{
		XmlSchemaAttribute_t1191708721 * L_20 = V_0;
		NullCheck(L_20);
		String_t* L_21 = XmlSchemaAttribute_get_DefaultValue_m3443587414(L_20, /*hidden argument*/NULL);
		return L_21;
	}

IL_0071:
	{
		XmlSchemaAttribute_t1191708721 * L_22 = V_0;
		NullCheck(L_22);
		String_t* L_23 = XmlSchemaAttribute_get_FixedValue_m2020843011(L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.Void System.Data.XmlSchemaDataImporter::.cctor()
extern Il2CppClass* XmlSchema_t1932230565_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaAttribute_t1191708721_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlQualifiedName_t176365656_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaElement_t471922321_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral101574;
extern Il2CppCodeGenString* _stringLiteral1958052158;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern Il2CppCodeGenString* _stringLiteral97299;
extern Il2CppCodeGenString* _stringLiteral1542263633;
extern const uint32_t XmlSchemaDataImporter__cctor_m1892208812_MetadataUsageId;
extern "C"  void XmlSchemaDataImporter__cctor_m1892208812 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaDataImporter__cctor_m1892208812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlSchema_t1932230565 * V_0 = NULL;
	XmlSchemaAttribute_t1191708721 * V_1 = NULL;
	XmlSchemaAttribute_t1191708721 * V_2 = NULL;
	XmlSchemaElement_t471922321 * V_3 = NULL;
	{
		XmlSchema_t1932230565 * L_0 = (XmlSchema_t1932230565 *)il2cpp_codegen_object_new(XmlSchema_t1932230565_il2cpp_TypeInfo_var);
		XmlSchema__ctor_m188227104(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		XmlSchemaAttribute_t1191708721 * L_1 = (XmlSchemaAttribute_t1191708721 *)il2cpp_codegen_object_new(XmlSchemaAttribute_t1191708721_il2cpp_TypeInfo_var);
		XmlSchemaAttribute__ctor_m3705353354(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		XmlSchemaAttribute_t1191708721 * L_2 = V_1;
		NullCheck(L_2);
		XmlSchemaAttribute_set_Name_m2805062912(L_2, _stringLiteral101574, /*hidden argument*/NULL);
		XmlSchemaAttribute_t1191708721 * L_3 = V_1;
		XmlQualifiedName_t176365656 * L_4 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_4, _stringLiteral1958052158, _stringLiteral1440052060, /*hidden argument*/NULL);
		NullCheck(L_3);
		XmlSchemaAttribute_set_SchemaTypeName_m3488898191(L_3, L_4, /*hidden argument*/NULL);
		XmlSchema_t1932230565 * L_5 = V_0;
		NullCheck(L_5);
		XmlSchemaObjectCollection_t2238201602 * L_6 = XmlSchema_get_Items_m3467235754(L_5, /*hidden argument*/NULL);
		XmlSchemaAttribute_t1191708721 * L_7 = V_1;
		NullCheck(L_6);
		XmlSchemaObjectCollection_Add_m2084724462(L_6, L_7, /*hidden argument*/NULL);
		XmlSchemaAttribute_t1191708721 * L_8 = (XmlSchemaAttribute_t1191708721 *)il2cpp_codegen_object_new(XmlSchemaAttribute_t1191708721_il2cpp_TypeInfo_var);
		XmlSchemaAttribute__ctor_m3705353354(L_8, /*hidden argument*/NULL);
		V_2 = L_8;
		XmlSchemaAttribute_t1191708721 * L_9 = V_2;
		NullCheck(L_9);
		XmlSchemaAttribute_set_Name_m2805062912(L_9, _stringLiteral97299, /*hidden argument*/NULL);
		XmlSchemaAttribute_t1191708721 * L_10 = V_2;
		XmlQualifiedName_t176365656 * L_11 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_11, _stringLiteral1542263633, _stringLiteral1440052060, /*hidden argument*/NULL);
		NullCheck(L_10);
		XmlSchemaAttribute_set_SchemaTypeName_m3488898191(L_10, L_11, /*hidden argument*/NULL);
		XmlSchema_t1932230565 * L_12 = V_0;
		NullCheck(L_12);
		XmlSchemaObjectCollection_t2238201602 * L_13 = XmlSchema_get_Items_m3467235754(L_12, /*hidden argument*/NULL);
		XmlSchemaAttribute_t1191708721 * L_14 = V_2;
		NullCheck(L_13);
		XmlSchemaObjectCollection_Add_m2084724462(L_13, L_14, /*hidden argument*/NULL);
		XmlSchemaElement_t471922321 * L_15 = (XmlSchemaElement_t471922321 *)il2cpp_codegen_object_new(XmlSchemaElement_t471922321_il2cpp_TypeInfo_var);
		XmlSchemaElement__ctor_m1057102058(L_15, /*hidden argument*/NULL);
		V_3 = L_15;
		XmlSchemaElement_t471922321 * L_16 = V_3;
		NullCheck(L_16);
		XmlSchemaElement_set_Name_m2260992352(L_16, _stringLiteral97299, /*hidden argument*/NULL);
		XmlSchema_t1932230565 * L_17 = V_0;
		NullCheck(L_17);
		XmlSchemaObjectCollection_t2238201602 * L_18 = XmlSchema_get_Items_m3467235754(L_17, /*hidden argument*/NULL);
		XmlSchemaElement_t471922321 * L_19 = V_3;
		NullCheck(L_18);
		XmlSchemaObjectCollection_Add_m2084724462(L_18, L_19, /*hidden argument*/NULL);
		XmlSchema_t1932230565 * L_20 = V_0;
		NullCheck(L_20);
		XmlSchema_Compile_m2629685169(L_20, (ValidationEventHandler_t2777264566 *)NULL, /*hidden argument*/NULL);
		XmlSchemaAttribute_t1191708721 * L_21 = V_1;
		NullCheck(L_21);
		XmlSchemaSimpleType_t1500525009 * L_22 = XmlSchemaAttribute_get_AttributeSchemaType_m324803664(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		XmlSchemaDatatype_t2590121 * L_23 = XmlSchemaType_get_Datatype_m2483034461(L_22, /*hidden argument*/NULL);
		((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->set_schemaIntegerType_0(L_23);
		XmlSchemaAttribute_t1191708721 * L_24 = V_2;
		NullCheck(L_24);
		XmlSchemaSimpleType_t1500525009 * L_25 = XmlSchemaAttribute_get_AttributeSchemaType_m324803664(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		XmlSchemaDatatype_t2590121 * L_26 = XmlSchemaType_get_Datatype_m2483034461(L_25, /*hidden argument*/NULL);
		((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->set_schemaDecimalType_1(L_26);
		XmlSchemaElement_t471922321 * L_27 = V_3;
		NullCheck(L_27);
		XmlSchemaType_t3432810239 * L_28 = XmlSchemaElement_get_ElementSchemaType_m2695446462(L_27, /*hidden argument*/NULL);
		((XmlSchemaDataImporter_t2811591463_StaticFields*)XmlSchemaDataImporter_t2811591463_il2cpp_TypeInfo_var->static_fields)->set_schemaAnyType_2(((XmlSchemaComplexType_t1860629407 *)IsInstClass(L_28, XmlSchemaComplexType_t1860629407_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::.ctor(System.Data.DataSet,System.Xml.XmlWriter,System.Data.DataTableCollection,System.Data.DataRelationCollection)
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern Il2CppClass* DataTableU5BU5D_t1761989358_il2cpp_TypeInfo_var;
extern Il2CppClass* DataRelationU5BU5D_t909637604_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaWriter__ctor_m553200337_MetadataUsageId;
extern "C"  void XmlSchemaWriter__ctor_m553200337 (XmlSchemaWriter_t2141469886 * __this, DataSet_t3654702571 * ___dataset0, XmlWriter_t89522450 * ___writer1, DataTableCollection_t2915263893 * ___tables2, DataRelationCollection_t267599063 * ___relations3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter__ctor_m553200337_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	XmlSchemaWriter_t2141469886 * G_B2_0 = NULL;
	XmlSchemaWriter_t2141469886 * G_B1_0 = NULL;
	CultureInfo_t3603717042 * G_B3_0 = NULL;
	XmlSchemaWriter_t2141469886 * G_B3_1 = NULL;
	{
		ArrayList_t2121638921 * L_0 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		__this->set_globalTypeTables_8(L_0);
		Hashtable_t3875263730 * L_1 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_1, /*hidden argument*/NULL);
		__this->set_additionalNamespaces_9(L_1);
		ArrayList_t2121638921 * L_2 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_2, /*hidden argument*/NULL);
		__this->set_annotation_10(L_2);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		DataSet_t3654702571 * L_3 = ___dataset0;
		NullCheck(L_3);
		String_t* L_4 = DataSet_get_DataSetName_m1556380856(L_3, /*hidden argument*/NULL);
		__this->set_dataSetName_4(L_4);
		DataSet_t3654702571 * L_5 = ___dataset0;
		NullCheck(L_5);
		String_t* L_6 = DataSet_get_Namespace_m20720144(L_5, /*hidden argument*/NULL);
		__this->set_dataSetNamespace_5(L_6);
		DataSet_t3654702571 * L_7 = ___dataset0;
		NullCheck(L_7);
		bool L_8 = DataSet_get_LocaleSpecified_m1230188946(L_7, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_8)
		{
			G_B2_0 = __this;
			goto IL_0056;
		}
	}
	{
		DataSet_t3654702571 * L_9 = ___dataset0;
		NullCheck(L_9);
		CultureInfo_t3603717042 * L_10 = DataSet_get_Locale_m4139362567(L_9, /*hidden argument*/NULL);
		G_B3_0 = L_10;
		G_B3_1 = G_B1_0;
		goto IL_0057;
	}

IL_0056:
	{
		G_B3_0 = ((CultureInfo_t3603717042 *)(NULL));
		G_B3_1 = G_B2_0;
	}

IL_0057:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_dataSetLocale_7(G_B3_0);
		DataSet_t3654702571 * L_11 = ___dataset0;
		NullCheck(L_11);
		PropertyCollection_t3599376422 * L_12 = DataSet_get_ExtendedProperties_m3188566177(L_11, /*hidden argument*/NULL);
		__this->set_dataSetProperties_6(L_12);
		XmlWriter_t89522450 * L_13 = ___writer1;
		__this->set_w_0(L_13);
		DataTableCollection_t2915263893 * L_14 = ___tables2;
		if (!L_14)
		{
			goto IL_00ac;
		}
	}
	{
		DataTableCollection_t2915263893 * L_15 = ___tables2;
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.Data.InternalDataCollectionBase::get_Count() */, L_15);
		__this->set_tables_1(((DataTableU5BU5D_t1761989358*)SZArrayNew(DataTableU5BU5D_t1761989358_il2cpp_TypeInfo_var, (uint32_t)L_16)));
		V_0 = 0;
		goto IL_00a0;
	}

IL_008d:
	{
		DataTableU5BU5D_t1761989358* L_17 = __this->get_tables_1();
		int32_t L_18 = V_0;
		DataTableCollection_t2915263893 * L_19 = ___tables2;
		int32_t L_20 = V_0;
		NullCheck(L_19);
		DataTable_t2176726999 * L_21 = DataTableCollection_get_Item_m1735387050(L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		ArrayElementTypeCheck (L_17, L_21);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (DataTable_t2176726999 *)L_21);
		int32_t L_22 = V_0;
		V_0 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_00a0:
	{
		int32_t L_23 = V_0;
		DataTableCollection_t2915263893 * L_24 = ___tables2;
		NullCheck(L_24);
		int32_t L_25 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.Data.InternalDataCollectionBase::get_Count() */, L_24);
		if ((((int32_t)L_23) < ((int32_t)L_25)))
		{
			goto IL_008d;
		}
	}

IL_00ac:
	{
		DataRelationCollection_t267599063 * L_26 = ___relations3;
		if (!L_26)
		{
			goto IL_00ed;
		}
	}
	{
		DataRelationCollection_t267599063 * L_27 = ___relations3;
		NullCheck(L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.Data.InternalDataCollectionBase::get_Count() */, L_27);
		__this->set_relations_2(((DataRelationU5BU5D_t909637604*)SZArrayNew(DataRelationU5BU5D_t909637604_il2cpp_TypeInfo_var, (uint32_t)L_28)));
		V_1 = 0;
		goto IL_00e0;
	}

IL_00cc:
	{
		DataRelationU5BU5D_t909637604* L_29 = __this->get_relations_2();
		int32_t L_30 = V_1;
		DataRelationCollection_t267599063 * L_31 = ___relations3;
		int32_t L_32 = V_1;
		NullCheck(L_31);
		DataRelation_t1483987353 * L_33 = VirtFuncInvoker1< DataRelation_t1483987353 *, int32_t >::Invoke(14 /* System.Data.DataRelation System.Data.DataRelationCollection::get_Item(System.Int32) */, L_31, L_32);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		ArrayElementTypeCheck (L_29, L_33);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(L_30), (DataRelation_t1483987353 *)L_33);
		int32_t L_34 = V_1;
		V_1 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00e0:
	{
		int32_t L_35 = V_1;
		DataRelationCollection_t267599063 * L_36 = ___relations3;
		NullCheck(L_36);
		int32_t L_37 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.Data.InternalDataCollectionBase::get_Count() */, L_36);
		if ((((int32_t)L_35) < ((int32_t)L_37)))
		{
			goto IL_00cc;
		}
	}

IL_00ed:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::.ctor(System.Xml.XmlWriter,System.Data.DataTable[],System.Data.DataRelation[],System.String,System.String,System.Globalization.CultureInfo)
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern Il2CppClass* PropertyCollection_t3599376422_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaWriter__ctor_m4080395814_MetadataUsageId;
extern "C"  void XmlSchemaWriter__ctor_m4080395814 (XmlSchemaWriter_t2141469886 * __this, XmlWriter_t89522450 * ___writer0, DataTableU5BU5D_t1761989358* ___tables1, DataRelationU5BU5D_t909637604* ___relations2, String_t* ___mainDataTable3, String_t* ___dataSetName4, CultureInfo_t3603717042 * ___locale5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter__ctor_m4080395814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		__this->set_globalTypeTables_8(L_0);
		Hashtable_t3875263730 * L_1 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_1, /*hidden argument*/NULL);
		__this->set_additionalNamespaces_9(L_1);
		ArrayList_t2121638921 * L_2 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_2, /*hidden argument*/NULL);
		__this->set_annotation_10(L_2);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_3 = ___writer0;
		__this->set_w_0(L_3);
		DataTableU5BU5D_t1761989358* L_4 = ___tables1;
		__this->set_tables_1(L_4);
		DataRelationU5BU5D_t909637604* L_5 = ___relations2;
		__this->set_relations_2(L_5);
		String_t* L_6 = ___mainDataTable3;
		__this->set_mainDataTable_3(L_6);
		String_t* L_7 = ___dataSetName4;
		__this->set_dataSetName_4(L_7);
		CultureInfo_t3603717042 * L_8 = ___locale5;
		__this->set_dataSetLocale_7(L_8);
		PropertyCollection_t3599376422 * L_9 = (PropertyCollection_t3599376422 *)il2cpp_codegen_object_new(PropertyCollection_t3599376422_il2cpp_TypeInfo_var);
		PropertyCollection__ctor_m1268107038(L_9, /*hidden argument*/NULL);
		__this->set_dataSetProperties_6(L_9);
		DataTableU5BU5D_t1761989358* L_10 = ___tables1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		NullCheck(((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		DataSet_t3654702571 * L_12 = DataTable_get_DataSet_m3509335390(((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		DataTableU5BU5D_t1761989358* L_13 = ___tables1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		int32_t L_14 = 0;
		NullCheck(((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14))));
		DataSet_t3654702571 * L_15 = DataTable_get_DataSet_m3509335390(((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14))), /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_16 = DataSet_get_Namespace_m20720144(L_15, /*hidden argument*/NULL);
		__this->set_dataSetNamespace_5(L_16);
		goto IL_0092;
	}

IL_0084:
	{
		DataTableU5BU5D_t1761989358* L_17 = ___tables1;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		int32_t L_18 = 0;
		NullCheck(((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18))));
		String_t* L_19 = DataTable_get_Namespace_m3829929060(((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18))), /*hidden argument*/NULL);
		__this->set_dataSetNamespace_5(L_19);
	}

IL_0092:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::WriteXmlSchema(System.Data.DataSet,System.Xml.XmlWriter)
extern "C"  void XmlSchemaWriter_WriteXmlSchema_m3024174918 (Il2CppObject * __this /* static, unused */, DataSet_t3654702571 * ___dataset0, XmlWriter_t89522450 * ___writer1, const MethodInfo* method)
{
	{
		DataSet_t3654702571 * L_0 = ___dataset0;
		XmlWriter_t89522450 * L_1 = ___writer1;
		DataSet_t3654702571 * L_2 = ___dataset0;
		NullCheck(L_2);
		DataTableCollection_t2915263893 * L_3 = DataSet_get_Tables_m87321279(L_2, /*hidden argument*/NULL);
		DataSet_t3654702571 * L_4 = ___dataset0;
		NullCheck(L_4);
		DataRelationCollection_t267599063 * L_5 = DataSet_get_Relations_m498597843(L_4, /*hidden argument*/NULL);
		XmlSchemaWriter_WriteXmlSchema_m1494422280(NULL /*static, unused*/, L_0, L_1, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::WriteXmlSchema(System.Data.DataSet,System.Xml.XmlWriter,System.Data.DataTableCollection,System.Data.DataRelationCollection)
extern Il2CppClass* XmlSchemaWriter_t2141469886_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaWriter_WriteXmlSchema_m1494422280_MetadataUsageId;
extern "C"  void XmlSchemaWriter_WriteXmlSchema_m1494422280 (Il2CppObject * __this /* static, unused */, DataSet_t3654702571 * ___dataset0, XmlWriter_t89522450 * ___writer1, DataTableCollection_t2915263893 * ___tables2, DataRelationCollection_t267599063 * ___relations3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_WriteXmlSchema_m1494422280_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DataSet_t3654702571 * L_0 = ___dataset0;
		XmlWriter_t89522450 * L_1 = ___writer1;
		DataTableCollection_t2915263893 * L_2 = ___tables2;
		DataRelationCollection_t267599063 * L_3 = ___relations3;
		XmlSchemaWriter_t2141469886 * L_4 = (XmlSchemaWriter_t2141469886 *)il2cpp_codegen_object_new(XmlSchemaWriter_t2141469886_il2cpp_TypeInfo_var);
		XmlSchemaWriter__ctor_m553200337(L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		XmlSchemaWriter_WriteSchema_m3203862312(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::WriteXmlSchema(System.Xml.XmlWriter,System.Data.DataTable[],System.Data.DataRelation[],System.String,System.String,System.Globalization.CultureInfo)
extern Il2CppClass* XmlSchemaWriter_t2141469886_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaWriter_WriteXmlSchema_m843176399_MetadataUsageId;
extern "C"  void XmlSchemaWriter_WriteXmlSchema_m843176399 (Il2CppObject * __this /* static, unused */, XmlWriter_t89522450 * ___writer0, DataTableU5BU5D_t1761989358* ___tables1, DataRelationU5BU5D_t909637604* ___relations2, String_t* ___mainDataTable3, String_t* ___dataSetName4, CultureInfo_t3603717042 * ___locale5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_WriteXmlSchema_m843176399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlWriter_t89522450 * L_0 = ___writer0;
		DataTableU5BU5D_t1761989358* L_1 = ___tables1;
		DataRelationU5BU5D_t909637604* L_2 = ___relations2;
		String_t* L_3 = ___mainDataTable3;
		String_t* L_4 = ___dataSetName4;
		CultureInfo_t3603717042 * L_5 = ___locale5;
		XmlSchemaWriter_t2141469886 * L_6 = (XmlSchemaWriter_t2141469886 *)il2cpp_codegen_object_new(XmlSchemaWriter_t2141469886_il2cpp_TypeInfo_var);
		XmlSchemaWriter__ctor_m4080395814(L_6, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		XmlSchemaWriter_WriteSchema_m3203862312(L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Data.XmlSchemaWriter::get_ConstraintPrefix()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104204755;
extern const uint32_t XmlSchemaWriter_get_ConstraintPrefix_m4028319215_MetadataUsageId;
extern "C"  String_t* XmlSchemaWriter_get_ConstraintPrefix_m4028319215 (XmlSchemaWriter_t2141469886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_get_ConstraintPrefix_m4028319215_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		String_t* L_0 = __this->get_dataSetNamespace_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_2 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		uint16_t L_3 = ((uint16_t)((int32_t)58));
		Il2CppObject * L_4 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral104204755, L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_0030;
	}

IL_002b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
	}

IL_0030:
	{
		return G_B3_0;
	}
}
// System.Void System.Data.XmlSchemaWriter::WriteSchema()
extern Il2CppClass* ListDictionary_t4226329727_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* DataColumn_t3354469747_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern Il2CppClass* DataRelation_t1483987353_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3835;
extern Il2CppCodeGenString* _stringLiteral3386979745;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern Il2CppCodeGenString* _stringLiteral3355;
extern Il2CppCodeGenString* _stringLiteral3437180618;
extern Il2CppCodeGenString* _stringLiteral114177052;
extern Il2CppCodeGenString* _stringLiteral104204755;
extern Il2CppCodeGenString* _stringLiteral557947472;
extern Il2CppCodeGenString* _stringLiteral3229858384;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral3230232073;
extern Il2CppCodeGenString* _stringLiteral3051547195;
extern Il2CppCodeGenString* _stringLiteral3622944929;
extern Il2CppCodeGenString* _stringLiteral3047026844;
extern Il2CppCodeGenString* _stringLiteral2391003201;
extern Il2CppCodeGenString* _stringLiteral1984571827;
extern Il2CppCodeGenString* _stringLiteral3110171557;
extern Il2CppCodeGenString* _stringLiteral1252218203;
extern Il2CppCodeGenString* _stringLiteral940574838;
extern const uint32_t XmlSchemaWriter_WriteSchema_m3203862312_MetadataUsageId;
extern "C"  void XmlSchemaWriter_WriteSchema_m3203862312 (XmlSchemaWriter_t2141469886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_WriteSchema_m3203862312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ListDictionary_t4226329727 * V_0 = NULL;
	ListDictionary_t4226329727 * V_1 = NULL;
	DataTableU5BU5D_t1761989358* V_2 = NULL;
	int32_t V_3 = 0;
	DataTable_t2176726999 * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	DataColumn_t3354469747 * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	String_t* V_9 = NULL;
	Il2CppObject * V_10 = NULL;
	Il2CppObject * V_11 = NULL;
	String_t* V_12 = NULL;
	Il2CppObject * V_13 = NULL;
	DataTableU5BU5D_t1761989358* V_14 = NULL;
	int32_t V_15 = 0;
	DataTable_t2176726999 * V_16 = NULL;
	bool V_17 = false;
	Il2CppObject * V_18 = NULL;
	DataRelation_t1483987353 * V_19 = NULL;
	Il2CppObject * V_20 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ListDictionary_t4226329727 * L_0 = (ListDictionary_t4226329727 *)il2cpp_codegen_object_new(ListDictionary_t4226329727_il2cpp_TypeInfo_var);
		ListDictionary__ctor_m1687707920(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ListDictionary_t4226329727 * L_1 = (ListDictionary_t4226329727 *)il2cpp_codegen_object_new(ListDictionary_t4226329727_il2cpp_TypeInfo_var);
		ListDictionary__ctor_m1687707920(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		DataTableU5BU5D_t1761989358* L_2 = __this->get_tables_1();
		V_2 = L_2;
		V_3 = 0;
		goto IL_0098;
	}

IL_001a:
	{
		DataTableU5BU5D_t1761989358* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_4 = ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
		DataTable_t2176726999 * L_6 = V_4;
		NullCheck(L_6);
		DataColumnCollection_t3528392753 * L_7 = DataTable_get_Columns_m220042291(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Il2CppObject * L_8 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* System.Collections.IEnumerator System.Data.InternalDataCollectionBase::GetEnumerator() */, L_7);
		V_5 = L_8;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0056;
		}

IL_0032:
		{
			Il2CppObject * L_9 = V_5;
			NullCheck(L_9);
			Il2CppObject * L_10 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_9);
			V_6 = ((DataColumn_t3354469747 *)CastclassClass(L_10, DataColumn_t3354469747_il2cpp_TypeInfo_var));
			DataColumn_t3354469747 * L_11 = V_6;
			NullCheck(L_11);
			String_t* L_12 = DataColumn_get_Prefix_m1148206961(L_11, /*hidden argument*/NULL);
			DataColumn_t3354469747 * L_13 = V_6;
			NullCheck(L_13);
			String_t* L_14 = DataColumn_get_Namespace_m546686590(L_13, /*hidden argument*/NULL);
			ListDictionary_t4226329727 * L_15 = V_0;
			ListDictionary_t4226329727 * L_16 = V_1;
			XmlSchemaWriter_CheckNamespace_m566926279(__this, L_12, L_14, L_15, L_16, /*hidden argument*/NULL);
		}

IL_0056:
		{
			Il2CppObject * L_17 = V_5;
			NullCheck(L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_0032;
			}
		}

IL_0062:
		{
			IL2CPP_LEAVE(0x7E, FINALLY_0067);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0067;
	}

FINALLY_0067:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_19 = V_5;
			Il2CppObject * L_20 = ((Il2CppObject *)IsInst(L_19, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_7 = L_20;
			if (!L_20)
			{
				goto IL_007d;
			}
		}

IL_0076:
		{
			Il2CppObject * L_21 = V_7;
			NullCheck(L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_21);
		}

IL_007d:
		{
			IL2CPP_END_FINALLY(103)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(103)
	{
		IL2CPP_JUMP_TBL(0x7E, IL_007e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007e:
	{
		DataTable_t2176726999 * L_22 = V_4;
		NullCheck(L_22);
		String_t* L_23 = DataTable_get_Prefix_m494218059(L_22, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_24 = V_4;
		NullCheck(L_24);
		String_t* L_25 = DataTable_get_Namespace_m3829929060(L_24, /*hidden argument*/NULL);
		ListDictionary_t4226329727 * L_26 = V_0;
		ListDictionary_t4226329727 * L_27 = V_1;
		XmlSchemaWriter_CheckNamespace_m566926279(__this, L_23, L_25, L_26, L_27, /*hidden argument*/NULL);
		int32_t L_28 = V_3;
		V_3 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_0098:
	{
		int32_t L_29 = V_3;
		DataTableU5BU5D_t1761989358* L_30 = V_2;
		NullCheck(L_30);
		if ((((int32_t)L_29) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		XmlWriter_t89522450 * L_31 = __this->get_w_0();
		NullCheck(L_31);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_31, _stringLiteral3835, _stringLiteral3386979745, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_32 = __this->get_w_0();
		String_t* L_33 = __this->get_dataSetName_4();
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_34 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		XmlWriter_WriteAttributeString_m1069599196(L_32, _stringLiteral3355, L_34, /*hidden argument*/NULL);
		String_t* L_35 = __this->get_dataSetNamespace_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_37 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0121;
		}
	}
	{
		XmlWriter_t89522450 * L_38 = __this->get_w_0();
		String_t* L_39 = __this->get_dataSetNamespace_5();
		NullCheck(L_38);
		XmlWriter_WriteAttributeString_m1069599196(L_38, _stringLiteral3437180618, L_39, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_40 = __this->get_w_0();
		String_t* L_41 = __this->get_dataSetNamespace_5();
		NullCheck(L_40);
		XmlWriter_WriteAttributeString_m1630990645(L_40, _stringLiteral114177052, _stringLiteral104204755, _stringLiteral557947472, L_41, /*hidden argument*/NULL);
	}

IL_0121:
	{
		XmlWriter_t89522450 * L_42 = __this->get_w_0();
		String_t* L_43 = __this->get_dataSetNamespace_5();
		NullCheck(L_42);
		XmlWriter_WriteAttributeString_m1069599196(L_42, _stringLiteral114177052, L_43, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_44 = __this->get_w_0();
		NullCheck(L_44);
		XmlWriter_WriteAttributeString_m1630990645(L_44, _stringLiteral114177052, _stringLiteral3835, _stringLiteral557947472, _stringLiteral1440052060, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_45 = __this->get_w_0();
		NullCheck(L_45);
		XmlWriter_WriteAttributeString_m1630990645(L_45, _stringLiteral114177052, _stringLiteral3229858384, _stringLiteral557947472, _stringLiteral3051173506, /*hidden argument*/NULL);
		DataTableU5BU5D_t1761989358* L_46 = __this->get_tables_1();
		DataRelationU5BU5D_t909637604* L_47 = __this->get_relations_2();
		bool L_48 = XmlSchemaWriter_CheckExtendedPropertyExists_m4066524780(__this, L_46, L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_01ab;
		}
	}
	{
		XmlWriter_t89522450 * L_49 = __this->get_w_0();
		NullCheck(L_49);
		XmlWriter_WriteAttributeString_m1630990645(L_49, _stringLiteral114177052, _stringLiteral3230232073, _stringLiteral557947472, _stringLiteral3051547195, /*hidden argument*/NULL);
	}

IL_01ab:
	{
		String_t* L_50 = __this->get_dataSetNamespace_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_51 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_52 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_50, L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_01ea;
		}
	}
	{
		XmlWriter_t89522450 * L_53 = __this->get_w_0();
		NullCheck(L_53);
		XmlWriter_WriteAttributeString_m1069599196(L_53, _stringLiteral3622944929, _stringLiteral3047026844, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_54 = __this->get_w_0();
		NullCheck(L_54);
		XmlWriter_WriteAttributeString_m1069599196(L_54, _stringLiteral2391003201, _stringLiteral3047026844, /*hidden argument*/NULL);
	}

IL_01ea:
	{
		ListDictionary_t4226329727 * L_55 = V_0;
		NullCheck(L_55);
		Il2CppObject * L_56 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Collections.ICollection System.Collections.Specialized.ListDictionary::get_Keys() */, L_55);
		NullCheck(L_56);
		Il2CppObject * L_57 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_56);
		V_8 = L_57;
	}

IL_01f7:
	try
	{ // begin try (depth: 1)
		{
			goto IL_022e;
		}

IL_01fc:
		{
			Il2CppObject * L_58 = V_8;
			NullCheck(L_58);
			Il2CppObject * L_59 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_58);
			V_9 = ((String_t*)CastclassSealed(L_59, String_t_il2cpp_TypeInfo_var));
			XmlWriter_t89522450 * L_60 = __this->get_w_0();
			String_t* L_61 = V_9;
			ListDictionary_t4226329727 * L_62 = V_0;
			String_t* L_63 = V_9;
			NullCheck(L_62);
			Il2CppObject * L_64 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(4 /* System.Object System.Collections.Specialized.ListDictionary::get_Item(System.Object) */, L_62, L_63);
			NullCheck(L_60);
			XmlWriter_WriteAttributeString_m1630990645(L_60, _stringLiteral114177052, L_61, _stringLiteral557947472, ((String_t*)IsInstSealed(L_64, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		}

IL_022e:
		{
			Il2CppObject * L_65 = V_8;
			NullCheck(L_65);
			bool L_66 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_65);
			if (L_66)
			{
				goto IL_01fc;
			}
		}

IL_023a:
		{
			IL2CPP_LEAVE(0x256, FINALLY_023f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_023f;
	}

FINALLY_023f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_67 = V_8;
			Il2CppObject * L_68 = ((Il2CppObject *)IsInst(L_67, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_10 = L_68;
			if (!L_68)
			{
				goto IL_0255;
			}
		}

IL_024e:
		{
			Il2CppObject * L_69 = V_10;
			NullCheck(L_69);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_69);
		}

IL_0255:
		{
			IL2CPP_END_FINALLY(575)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(575)
	{
		IL2CPP_JUMP_TBL(0x256, IL_0256)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0256:
	{
		ListDictionary_t4226329727 * L_70 = V_1;
		NullCheck(L_70);
		int32_t L_71 = VirtFuncInvoker0< int32_t >::Invoke(14 /* System.Int32 System.Collections.Specialized.ListDictionary::get_Count() */, L_70);
		if ((((int32_t)L_71) <= ((int32_t)0)))
		{
			goto IL_0272;
		}
	}
	{
		XmlWriter_t89522450 * L_72 = __this->get_w_0();
		NullCheck(L_72);
		VirtActionInvoker1< String_t* >::Invoke(14 /* System.Void System.Xml.XmlWriter::WriteComment(System.String) */, L_72, _stringLiteral1984571827);
	}

IL_0272:
	{
		ListDictionary_t4226329727 * L_73 = V_1;
		NullCheck(L_73);
		Il2CppObject * L_74 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Collections.ICollection System.Collections.Specialized.ListDictionary::get_Keys() */, L_73);
		NullCheck(L_74);
		Il2CppObject * L_75 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_74);
		V_11 = L_75;
	}

IL_027f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02e6;
		}

IL_0284:
		{
			Il2CppObject * L_76 = V_11;
			NullCheck(L_76);
			Il2CppObject * L_77 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_76);
			V_12 = ((String_t*)CastclassSealed(L_77, String_t_il2cpp_TypeInfo_var));
			XmlWriter_t89522450 * L_78 = __this->get_w_0();
			NullCheck(L_78);
			VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_78, _stringLiteral3835, _stringLiteral3110171557, _stringLiteral1440052060);
			XmlWriter_t89522450 * L_79 = __this->get_w_0();
			String_t* L_80 = V_12;
			NullCheck(L_79);
			XmlWriter_WriteAttributeString_m1069599196(L_79, _stringLiteral1252218203, L_80, /*hidden argument*/NULL);
			XmlWriter_t89522450 * L_81 = __this->get_w_0();
			ListDictionary_t4226329727 * L_82 = V_1;
			String_t* L_83 = V_12;
			NullCheck(L_82);
			Il2CppObject * L_84 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(4 /* System.Object System.Collections.Specialized.ListDictionary::get_Item(System.Object) */, L_82, L_83);
			NullCheck(L_81);
			XmlWriter_WriteAttributeString_m1069599196(L_81, _stringLiteral940574838, ((String_t*)IsInstSealed(L_84, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			XmlWriter_t89522450 * L_85 = __this->get_w_0();
			NullCheck(L_85);
			VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_85);
		}

IL_02e6:
		{
			Il2CppObject * L_86 = V_11;
			NullCheck(L_86);
			bool L_87 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_86);
			if (L_87)
			{
				goto IL_0284;
			}
		}

IL_02f2:
		{
			IL2CPP_LEAVE(0x30E, FINALLY_02f7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_02f7;
	}

FINALLY_02f7:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_88 = V_11;
			Il2CppObject * L_89 = ((Il2CppObject *)IsInst(L_88, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_13 = L_89;
			if (!L_89)
			{
				goto IL_030d;
			}
		}

IL_0306:
		{
			Il2CppObject * L_90 = V_13;
			NullCheck(L_90);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_90);
		}

IL_030d:
		{
			IL2CPP_END_FINALLY(759)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(759)
	{
		IL2CPP_JUMP_TBL(0x30E, IL_030e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_030e:
	{
		DataTableU5BU5D_t1761989358* L_91 = __this->get_tables_1();
		V_14 = L_91;
		V_15 = 0;
		goto IL_03a8;
	}

IL_031e:
	{
		DataTableU5BU5D_t1761989358* L_92 = V_14;
		int32_t L_93 = V_15;
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, L_93);
		int32_t L_94 = L_93;
		V_16 = ((L_92)->GetAt(static_cast<il2cpp_array_size_t>(L_94)));
		V_17 = (bool)1;
		DataTable_t2176726999 * L_95 = V_16;
		NullCheck(L_95);
		DataRelationCollection_t267599063 * L_96 = DataTable_get_ParentRelations_m3551104541(L_95, /*hidden argument*/NULL);
		NullCheck(L_96);
		Il2CppObject * L_97 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* System.Collections.IEnumerator System.Data.InternalDataCollectionBase::GetEnumerator() */, L_96);
		V_18 = L_97;
	}

IL_0336:
	try
	{ // begin try (depth: 1)
		{
			goto IL_035d;
		}

IL_033b:
		{
			Il2CppObject * L_98 = V_18;
			NullCheck(L_98);
			Il2CppObject * L_99 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_98);
			V_19 = ((DataRelation_t1483987353 *)CastclassClass(L_99, DataRelation_t1483987353_il2cpp_TypeInfo_var));
			DataRelation_t1483987353 * L_100 = V_19;
			NullCheck(L_100);
			bool L_101 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean System.Data.DataRelation::get_Nested() */, L_100);
			if (!L_101)
			{
				goto IL_035d;
			}
		}

IL_0355:
		{
			V_17 = (bool)0;
			goto IL_0369;
		}

IL_035d:
		{
			Il2CppObject * L_102 = V_18;
			NullCheck(L_102);
			bool L_103 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_102);
			if (L_103)
			{
				goto IL_033b;
			}
		}

IL_0369:
		{
			IL2CPP_LEAVE(0x385, FINALLY_036e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_036e;
	}

FINALLY_036e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_104 = V_18;
			Il2CppObject * L_105 = ((Il2CppObject *)IsInst(L_104, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_20 = L_105;
			if (!L_105)
			{
				goto IL_0384;
			}
		}

IL_037d:
		{
			Il2CppObject * L_106 = V_20;
			NullCheck(L_106);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_106);
		}

IL_0384:
		{
			IL2CPP_END_FINALLY(878)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(878)
	{
		IL2CPP_JUMP_TBL(0x385, IL_0385)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0385:
	{
		bool L_107 = V_17;
		if (L_107)
		{
			goto IL_03a2;
		}
	}
	{
		DataTableU5BU5D_t1761989358* L_108 = __this->get_tables_1();
		NullCheck(L_108);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_108)->max_length))))) >= ((int32_t)2)))
		{
			goto IL_03a2;
		}
	}
	{
		DataTable_t2176726999 * L_109 = V_16;
		XmlSchemaWriter_WriteTableElement_m3446834206(__this, L_109, /*hidden argument*/NULL);
	}

IL_03a2:
	{
		int32_t L_110 = V_15;
		V_15 = ((int32_t)((int32_t)L_110+(int32_t)1));
	}

IL_03a8:
	{
		int32_t L_111 = V_15;
		DataTableU5BU5D_t1761989358* L_112 = V_14;
		NullCheck(L_112);
		if ((((int32_t)L_111) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_112)->max_length)))))))
		{
			goto IL_031e;
		}
	}
	{
		XmlSchemaWriter_WriteDataSetElement_m2963877451(__this, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_113 = __this->get_w_0();
		NullCheck(L_113);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_113);
		XmlWriter_t89522450 * L_114 = __this->get_w_0();
		NullCheck(L_114);
		VirtActionInvoker0::Invoke(11 /* System.Void System.Xml.XmlWriter::Flush() */, L_114);
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::WriteDataSetElement()
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* DataRelation_t1483987353_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3835;
extern Il2CppCodeGenString* _stringLiteral2632130300;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3229858384;
extern Il2CppCodeGenString* _stringLiteral2867372974;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral2281372282;
extern Il2CppCodeGenString* _stringLiteral305807467;
extern Il2CppCodeGenString* _stringLiteral144055052;
extern Il2CppCodeGenString* _stringLiteral405639178;
extern Il2CppCodeGenString* _stringLiteral2933743009;
extern Il2CppCodeGenString* _stringLiteral64804601;
extern Il2CppCodeGenString* _stringLiteral48;
extern Il2CppCodeGenString* _stringLiteral3587293323;
extern Il2CppCodeGenString* _stringLiteral1782112740;
extern Il2CppCodeGenString* _stringLiteral112787;
extern Il2CppCodeGenString* _stringLiteral2739923759;
extern Il2CppCodeGenString* _stringLiteral3501647439;
extern const uint32_t XmlSchemaWriter_WriteDataSetElement_m2963877451_MetadataUsageId;
extern "C"  void XmlSchemaWriter_WriteDataSetElement_m2963877451 (XmlSchemaWriter_t2141469886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_WriteDataSetElement_m2963877451_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	DataTableU5BU5D_t1761989358* V_1 = NULL;
	int32_t V_2 = 0;
	DataTable_t2176726999 * V_3 = NULL;
	bool V_4 = false;
	Il2CppObject * V_5 = NULL;
	DataRelation_t1483987353 * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	bool V_8 = false;
	Il2CppObject * V_9 = NULL;
	Il2CppObject * V_10 = NULL;
	Il2CppObject * V_11 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlWriter_t89522450 * L_0 = __this->get_w_0();
		NullCheck(L_0);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_0, _stringLiteral3835, _stringLiteral2632130300, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_1 = __this->get_w_0();
		String_t* L_2 = __this->get_dataSetName_4();
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_3 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		XmlWriter_WriteAttributeString_m1069599196(L_1, _stringLiteral3373707, L_3, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_4 = __this->get_w_0();
		NullCheck(L_4);
		XmlWriter_WriteAttributeString_m1630990645(L_4, _stringLiteral3229858384, _stringLiteral2867372974, _stringLiteral3051173506, _stringLiteral3569038, /*hidden argument*/NULL);
		CultureInfo_t3603717042 * L_5 = __this->get_dataSetLocale_7();
		V_0 = (bool)((((Il2CppObject*)(CultureInfo_t3603717042 *)L_5) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_6 = V_0;
		if (L_6)
		{
			goto IL_0089;
		}
	}
	{
		XmlWriter_t89522450 * L_7 = __this->get_w_0();
		CultureInfo_t3603717042 * L_8 = __this->get_dataSetLocale_7();
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Globalization.CultureInfo::get_Name() */, L_8);
		NullCheck(L_7);
		XmlWriter_WriteAttributeString_m1630990645(L_7, _stringLiteral3229858384, _stringLiteral2281372282, _stringLiteral3051173506, L_9, /*hidden argument*/NULL);
	}

IL_0089:
	{
		String_t* L_10 = __this->get_mainDataTable_3();
		if (!L_10)
		{
			goto IL_00c9;
		}
	}
	{
		String_t* L_11 = __this->get_mainDataTable_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_13 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00c9;
		}
	}
	{
		XmlWriter_t89522450 * L_14 = __this->get_w_0();
		String_t* L_15 = __this->get_mainDataTable_3();
		NullCheck(L_14);
		XmlWriter_WriteAttributeString_m1630990645(L_14, _stringLiteral3229858384, _stringLiteral305807467, _stringLiteral3051173506, L_15, /*hidden argument*/NULL);
	}

IL_00c9:
	{
		bool L_16 = V_0;
		if (!L_16)
		{
			goto IL_00ee;
		}
	}
	{
		XmlWriter_t89522450 * L_17 = __this->get_w_0();
		NullCheck(L_17);
		XmlWriter_WriteAttributeString_m1630990645(L_17, _stringLiteral3229858384, _stringLiteral144055052, _stringLiteral3051173506, _stringLiteral3569038, /*hidden argument*/NULL);
	}

IL_00ee:
	{
		PropertyCollection_t3599376422 * L_18 = __this->get_dataSetProperties_6();
		XmlSchemaWriter_AddExtendedPropertyAttributes_m1280916214(__this, L_18, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_19 = __this->get_w_0();
		NullCheck(L_19);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_19, _stringLiteral3835, _stringLiteral405639178, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_20 = __this->get_w_0();
		NullCheck(L_20);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_20, _stringLiteral3835, _stringLiteral2933743009, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_21 = __this->get_w_0();
		NullCheck(L_21);
		XmlWriter_WriteAttributeString_m1069599196(L_21, _stringLiteral64804601, _stringLiteral48, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_22 = __this->get_w_0();
		NullCheck(L_22);
		XmlWriter_WriteAttributeString_m1069599196(L_22, _stringLiteral3587293323, _stringLiteral1782112740, /*hidden argument*/NULL);
		DataTableU5BU5D_t1761989358* L_23 = __this->get_tables_1();
		V_1 = L_23;
		V_2 = 0;
		goto IL_027c;
	}

IL_0166:
	{
		DataTableU5BU5D_t1761989358* L_24 = V_1;
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		V_3 = ((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26)));
		V_4 = (bool)1;
		DataTable_t2176726999 * L_27 = V_3;
		NullCheck(L_27);
		DataRelationCollection_t267599063 * L_28 = DataTable_get_ParentRelations_m3551104541(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Il2CppObject * L_29 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* System.Collections.IEnumerator System.Data.InternalDataCollectionBase::GetEnumerator() */, L_28);
		V_5 = L_29;
	}

IL_017a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01a1;
		}

IL_017f:
		{
			Il2CppObject * L_30 = V_5;
			NullCheck(L_30);
			Il2CppObject * L_31 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_30);
			V_6 = ((DataRelation_t1483987353 *)CastclassClass(L_31, DataRelation_t1483987353_il2cpp_TypeInfo_var));
			DataRelation_t1483987353 * L_32 = V_6;
			NullCheck(L_32);
			bool L_33 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean System.Data.DataRelation::get_Nested() */, L_32);
			if (!L_33)
			{
				goto IL_01a1;
			}
		}

IL_0199:
		{
			V_4 = (bool)0;
			goto IL_01ad;
		}

IL_01a1:
		{
			Il2CppObject * L_34 = V_5;
			NullCheck(L_34);
			bool L_35 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_34);
			if (L_35)
			{
				goto IL_017f;
			}
		}

IL_01ad:
		{
			IL2CPP_LEAVE(0x1C9, FINALLY_01b2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01b2;
	}

FINALLY_01b2:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_36 = V_5;
			Il2CppObject * L_37 = ((Il2CppObject *)IsInst(L_36, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_7 = L_37;
			if (!L_37)
			{
				goto IL_01c8;
			}
		}

IL_01c1:
		{
			Il2CppObject * L_38 = V_7;
			NullCheck(L_38);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_38);
		}

IL_01c8:
		{
			IL2CPP_END_FINALLY(434)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(434)
	{
		IL2CPP_JUMP_TBL(0x1C9, IL_01c9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01c9:
	{
		V_8 = (bool)0;
		bool L_39 = V_4;
		if (L_39)
		{
			goto IL_01e7;
		}
	}
	{
		DataTableU5BU5D_t1761989358* L_40 = __this->get_tables_1();
		NullCheck(L_40);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_40)->max_length))))) >= ((int32_t)2)))
		{
			goto IL_01e7;
		}
	}
	{
		V_4 = (bool)1;
		V_8 = (bool)1;
	}

IL_01e7:
	{
		bool L_41 = V_4;
		if (!L_41)
		{
			goto IL_0278;
		}
	}
	{
		String_t* L_42 = __this->get_dataSetNamespace_5();
		DataTable_t2176726999 * L_43 = V_3;
		NullCheck(L_43);
		String_t* L_44 = DataTable_get_Namespace_m3829929060(L_43, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_45 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_42, L_44, /*hidden argument*/NULL);
		if (L_45)
		{
			goto IL_020b;
		}
	}
	{
		bool L_46 = V_8;
		if (!L_46)
		{
			goto IL_0271;
		}
	}

IL_020b:
	{
		XmlWriter_t89522450 * L_47 = __this->get_w_0();
		NullCheck(L_47);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_47, _stringLiteral3835, _stringLiteral2632130300, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_48 = __this->get_w_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_49 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_48);
		XmlWriter_WriteStartAttribute_m904857238(L_48, _stringLiteral112787, L_49, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_50 = __this->get_w_0();
		DataTable_t2176726999 * L_51 = V_3;
		NullCheck(L_51);
		String_t* L_52 = DataTable_get_TableName_m3141812994(L_51, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_53 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_54 = V_3;
		NullCheck(L_54);
		String_t* L_55 = DataTable_get_Namespace_m3829929060(L_54, /*hidden argument*/NULL);
		NullCheck(L_50);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(20 /* System.Void System.Xml.XmlWriter::WriteQualifiedName(System.String,System.String) */, L_50, L_53, L_55);
		XmlWriter_t89522450 * L_56 = __this->get_w_0();
		NullCheck(L_56);
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, L_56);
		XmlWriter_t89522450 * L_57 = __this->get_w_0();
		NullCheck(L_57);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_57);
		goto IL_0278;
	}

IL_0271:
	{
		DataTable_t2176726999 * L_58 = V_3;
		XmlSchemaWriter_WriteTableElement_m3446834206(__this, L_58, /*hidden argument*/NULL);
	}

IL_0278:
	{
		int32_t L_59 = V_2;
		V_2 = ((int32_t)((int32_t)L_59+(int32_t)1));
	}

IL_027c:
	{
		int32_t L_60 = V_2;
		DataTableU5BU5D_t1761989358* L_61 = V_1;
		NullCheck(L_61);
		if ((((int32_t)L_60) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_61)->max_length)))))))
		{
			goto IL_0166;
		}
	}
	{
		XmlWriter_t89522450 * L_62 = __this->get_w_0();
		NullCheck(L_62);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_62);
		XmlWriter_t89522450 * L_63 = __this->get_w_0();
		NullCheck(L_63);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_63);
		XmlSchemaWriter_WriteConstraints_m1550895281(__this, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_64 = __this->get_w_0();
		NullCheck(L_64);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_64);
		ArrayList_t2121638921 * L_65 = __this->get_annotation_10();
		NullCheck(L_65);
		int32_t L_66 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_65);
		if ((((int32_t)L_66) <= ((int32_t)0)))
		{
			goto IL_0368;
		}
	}
	{
		XmlWriter_t89522450 * L_67 = __this->get_w_0();
		NullCheck(L_67);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_67, _stringLiteral3835, _stringLiteral2739923759, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_68 = __this->get_w_0();
		NullCheck(L_68);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_68, _stringLiteral3835, _stringLiteral3501647439, _stringLiteral1440052060);
		ArrayList_t2121638921 * L_69 = __this->get_annotation_10();
		NullCheck(L_69);
		Il2CppObject * L_70 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_69);
		V_9 = L_70;
	}

IL_02fe:
	try
	{ // begin try (depth: 1)
		{
			goto IL_032a;
		}

IL_0303:
		{
			Il2CppObject * L_71 = V_9;
			NullCheck(L_71);
			Il2CppObject * L_72 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_71);
			V_10 = L_72;
			Il2CppObject * L_73 = V_10;
			if (((DataRelation_t1483987353 *)IsInstClass(L_73, DataRelation_t1483987353_il2cpp_TypeInfo_var)))
			{
				goto IL_031d;
			}
		}

IL_0318:
		{
			goto IL_032a;
		}

IL_031d:
		{
			Il2CppObject * L_74 = V_10;
			XmlSchemaWriter_WriteDataRelationAnnotation_m3530929873(__this, ((DataRelation_t1483987353 *)CastclassClass(L_74, DataRelation_t1483987353_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		}

IL_032a:
		{
			Il2CppObject * L_75 = V_9;
			NullCheck(L_75);
			bool L_76 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_75);
			if (L_76)
			{
				goto IL_0303;
			}
		}

IL_0336:
		{
			IL2CPP_LEAVE(0x352, FINALLY_033b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_033b;
	}

FINALLY_033b:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_77 = V_9;
			Il2CppObject * L_78 = ((Il2CppObject *)IsInst(L_77, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_11 = L_78;
			if (!L_78)
			{
				goto IL_0351;
			}
		}

IL_034a:
		{
			Il2CppObject * L_79 = V_11;
			NullCheck(L_79);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_79);
		}

IL_0351:
		{
			IL2CPP_END_FINALLY(827)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(827)
	{
		IL2CPP_JUMP_TBL(0x352, IL_0352)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0352:
	{
		XmlWriter_t89522450 * L_80 = __this->get_w_0();
		NullCheck(L_80);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_80);
		XmlWriter_t89522450 * L_81 = __this->get_w_0();
		NullCheck(L_81);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_81);
	}

IL_0368:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::WriteDataRelationAnnotation(System.Data.DataRelation)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3229858384;
extern Il2CppCodeGenString* _stringLiteral4197435992;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3299543210;
extern Il2CppCodeGenString* _stringLiteral94631196;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral2070338933;
extern Il2CppCodeGenString* _stringLiteral1659519939;
extern const uint32_t XmlSchemaWriter_WriteDataRelationAnnotation_m3530929873_MetadataUsageId;
extern "C"  void XmlSchemaWriter_WriteDataRelationAnnotation_m3530929873 (XmlSchemaWriter_t2141469886 * __this, DataRelation_t1483987353 * ___rel0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_WriteDataRelationAnnotation_m3530929873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	DataColumnU5BU5D_t3410743138* V_1 = NULL;
	int32_t V_2 = 0;
	DataColumn_t3354469747 * V_3 = NULL;
	DataColumnU5BU5D_t3410743138* V_4 = NULL;
	int32_t V_5 = 0;
	DataColumn_t3354469747 * V_6 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		XmlWriter_t89522450 * L_1 = __this->get_w_0();
		NullCheck(L_1);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_1, _stringLiteral3229858384, _stringLiteral4197435992, _stringLiteral3051173506);
		XmlWriter_t89522450 * L_2 = __this->get_w_0();
		DataRelation_t1483987353 * L_3 = ___rel0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(13 /* System.String System.Data.DataRelation::get_RelationName() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_5 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		XmlWriter_WriteAttributeString_m1069599196(L_2, _stringLiteral3373707, L_5, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_6 = __this->get_w_0();
		DataRelation_t1483987353 * L_7 = ___rel0;
		NullCheck(L_7);
		DataTable_t2176726999 * L_8 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(12 /* System.Data.DataTable System.Data.DataRelation::get_ParentTable() */, L_7);
		NullCheck(L_8);
		String_t* L_9 = DataTable_get_TableName_m3141812994(L_8, /*hidden argument*/NULL);
		String_t* L_10 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		XmlWriter_WriteAttributeString_m1630990645(L_6, _stringLiteral3229858384, _stringLiteral3299543210, _stringLiteral3051173506, L_10, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_11 = __this->get_w_0();
		DataRelation_t1483987353 * L_12 = ___rel0;
		NullCheck(L_12);
		DataTable_t2176726999 * L_13 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(6 /* System.Data.DataTable System.Data.DataRelation::get_ChildTable() */, L_12);
		NullCheck(L_13);
		String_t* L_14 = DataTable_get_TableName_m3141812994(L_13, /*hidden argument*/NULL);
		String_t* L_15 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		XmlWriter_WriteAttributeString_m1630990645(L_11, _stringLiteral3229858384, _stringLiteral94631196, _stringLiteral3051173506, L_15, /*hidden argument*/NULL);
		String_t* L_16 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_16;
		DataRelation_t1483987353 * L_17 = ___rel0;
		NullCheck(L_17);
		DataColumnU5BU5D_t3410743138* L_18 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(10 /* System.Data.DataColumn[] System.Data.DataRelation::get_ParentColumns() */, L_17);
		V_1 = L_18;
		V_2 = 0;
		goto IL_00c2;
	}

IL_00a3:
	{
		DataColumnU5BU5D_t3410743138* L_19 = V_1;
		int32_t L_20 = V_2;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		V_3 = ((L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21)));
		String_t* L_22 = V_0;
		DataColumn_t3354469747 * L_23 = V_3;
		NullCheck(L_23);
		String_t* L_24 = DataColumn_get_ColumnName_m409531680(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_25 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1825781833(NULL /*static, unused*/, L_22, L_25, _stringLiteral32, /*hidden argument*/NULL);
		V_0 = L_26;
		int32_t L_27 = V_2;
		V_2 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00c2:
	{
		int32_t L_28 = V_2;
		DataColumnU5BU5D_t3410743138* L_29 = V_1;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_00a3;
		}
	}
	{
		XmlWriter_t89522450 * L_30 = __this->get_w_0();
		String_t* L_31 = V_0;
		NullCheck(L_31);
		String_t* L_32 = String_TrimEnd_m3980947229(L_31, ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		NullCheck(L_30);
		XmlWriter_WriteAttributeString_m1630990645(L_30, _stringLiteral3229858384, _stringLiteral2070338933, _stringLiteral3051173506, L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_33;
		DataRelation_t1483987353 * L_34 = ___rel0;
		NullCheck(L_34);
		DataColumnU5BU5D_t3410743138* L_35 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(4 /* System.Data.DataColumn[] System.Data.DataRelation::get_ChildColumns() */, L_34);
		V_4 = L_35;
		V_5 = 0;
		goto IL_012c;
	}

IL_0107:
	{
		DataColumnU5BU5D_t3410743138* L_36 = V_4;
		int32_t L_37 = V_5;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = L_37;
		V_6 = ((L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38)));
		String_t* L_39 = V_0;
		DataColumn_t3354469747 * L_40 = V_6;
		NullCheck(L_40);
		String_t* L_41 = DataColumn_get_ColumnName_m409531680(L_40, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_42 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Concat_m1825781833(NULL /*static, unused*/, L_39, L_42, _stringLiteral32, /*hidden argument*/NULL);
		V_0 = L_43;
		int32_t L_44 = V_5;
		V_5 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_012c:
	{
		int32_t L_45 = V_5;
		DataColumnU5BU5D_t3410743138* L_46 = V_4;
		NullCheck(L_46);
		if ((((int32_t)L_45) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_46)->max_length)))))))
		{
			goto IL_0107;
		}
	}
	{
		XmlWriter_t89522450 * L_47 = __this->get_w_0();
		String_t* L_48 = V_0;
		NullCheck(L_48);
		String_t* L_49 = String_TrimEnd_m3980947229(L_48, ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		NullCheck(L_47);
		XmlWriter_WriteAttributeString_m1630990645(L_47, _stringLiteral3229858384, _stringLiteral1659519939, _stringLiteral3051173506, L_49, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_50 = __this->get_w_0();
		NullCheck(L_50);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_50);
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::WriteConstraints()
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* Constraint_t2349953968_il2cpp_TypeInfo_var;
extern Il2CppClass* UniqueConstraint_t1006662241_il2cpp_TypeInfo_var;
extern Il2CppClass* ForeignKeyConstraint_t1848099579_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DataRelation_t1483987353_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaWriter_WriteConstraints_m1550895281_MetadataUsageId;
extern "C"  void XmlSchemaWriter_WriteConstraints_m1550895281 (XmlSchemaWriter_t2141469886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_WriteConstraints_m1550895281_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t2121638921 * V_0 = NULL;
	DataTableU5BU5D_t1761989358* V_1 = NULL;
	int32_t V_2 = 0;
	DataTable_t2176726999 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Constraint_t2349953968 * V_5 = NULL;
	UniqueConstraint_t1006662241 * V_6 = NULL;
	ForeignKeyConstraint_t1848099579 * V_7 = NULL;
	bool V_8 = false;
	DataRelationU5BU5D_t909637604* V_9 = NULL;
	int32_t V_10 = 0;
	DataRelation_t1483987353 * V_11 = NULL;
	DataRelation_t1483987353 * V_12 = NULL;
	Il2CppObject * V_13 = NULL;
	DataRelationU5BU5D_t909637604* V_14 = NULL;
	int32_t V_15 = 0;
	DataRelation_t1483987353 * V_16 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ArrayList_t2121638921 * L_0 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		DataTableU5BU5D_t1761989358* L_1 = __this->get_tables_1();
		V_1 = L_1;
		V_2 = 0;
		goto IL_0123;
	}

IL_0014:
	{
		DataTableU5BU5D_t1761989358* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_3 = ((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)));
		DataTable_t2176726999 * L_5 = V_3;
		NullCheck(L_5);
		ConstraintCollection_t392455726 * L_6 = DataTable_get_Constraints_m4225751151(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Il2CppObject * L_7 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* System.Collections.IEnumerator System.Data.InternalDataCollectionBase::GetEnumerator() */, L_6);
		V_4 = L_7;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00f7;
		}

IL_002a:
		{
			Il2CppObject * L_8 = V_4;
			NullCheck(L_8);
			Il2CppObject * L_9 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_8);
			V_5 = ((Constraint_t2349953968 *)CastclassClass(L_9, Constraint_t2349953968_il2cpp_TypeInfo_var));
			Constraint_t2349953968 * L_10 = V_5;
			V_6 = ((UniqueConstraint_t1006662241 *)IsInstClass(L_10, UniqueConstraint_t1006662241_il2cpp_TypeInfo_var));
			UniqueConstraint_t1006662241 * L_11 = V_6;
			if (!L_11)
			{
				goto IL_0056;
			}
		}

IL_0048:
		{
			UniqueConstraint_t1006662241 * L_12 = V_6;
			ArrayList_t2121638921 * L_13 = V_0;
			XmlSchemaWriter_AddUniqueConstraints_m621813724(__this, L_12, L_13, /*hidden argument*/NULL);
			goto IL_00f7;
		}

IL_0056:
		{
			Constraint_t2349953968 * L_14 = V_5;
			V_7 = ((ForeignKeyConstraint_t1848099579 *)IsInstClass(L_14, ForeignKeyConstraint_t1848099579_il2cpp_TypeInfo_var));
			V_8 = (bool)0;
			DataRelationU5BU5D_t909637604* L_15 = __this->get_relations_2();
			if (!L_15)
			{
				goto IL_00b0;
			}
		}

IL_006d:
		{
			DataRelationU5BU5D_t909637604* L_16 = __this->get_relations_2();
			V_9 = L_16;
			V_10 = 0;
			goto IL_00a5;
		}

IL_007d:
		{
			DataRelationU5BU5D_t909637604* L_17 = V_9;
			int32_t L_18 = V_10;
			NullCheck(L_17);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
			int32_t L_19 = L_18;
			V_11 = ((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19)));
			DataRelation_t1483987353 * L_20 = V_11;
			NullCheck(L_20);
			String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(13 /* System.String System.Data.DataRelation::get_RelationName() */, L_20);
			ForeignKeyConstraint_t1848099579 * L_22 = V_7;
			NullCheck(L_22);
			String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Data.Constraint::get_ConstraintName() */, L_22);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_24 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
			if (!L_24)
			{
				goto IL_009f;
			}
		}

IL_009c:
		{
			V_8 = (bool)1;
		}

IL_009f:
		{
			int32_t L_25 = V_10;
			V_10 = ((int32_t)((int32_t)L_25+(int32_t)1));
		}

IL_00a5:
		{
			int32_t L_26 = V_10;
			DataRelationU5BU5D_t909637604* L_27 = V_9;
			NullCheck(L_27);
			if ((((int32_t)L_26) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length)))))))
			{
				goto IL_007d;
			}
		}

IL_00b0:
		{
			DataTableU5BU5D_t1761989358* L_28 = __this->get_tables_1();
			NullCheck(L_28);
			if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length))))) <= ((int32_t)1)))
			{
				goto IL_00f7;
			}
		}

IL_00be:
		{
			ForeignKeyConstraint_t1848099579 * L_29 = V_7;
			if (!L_29)
			{
				goto IL_00f7;
			}
		}

IL_00c5:
		{
			bool L_30 = V_8;
			if (L_30)
			{
				goto IL_00f7;
			}
		}

IL_00cc:
		{
			ForeignKeyConstraint_t1848099579 * L_31 = V_7;
			NullCheck(L_31);
			String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Data.Constraint::get_ConstraintName() */, L_31);
			ForeignKeyConstraint_t1848099579 * L_33 = V_7;
			NullCheck(L_33);
			DataColumnU5BU5D_t3410743138* L_34 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(21 /* System.Data.DataColumn[] System.Data.ForeignKeyConstraint::get_RelatedColumns() */, L_33);
			ForeignKeyConstraint_t1848099579 * L_35 = V_7;
			NullCheck(L_35);
			DataColumnU5BU5D_t3410743138* L_36 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(16 /* System.Data.DataColumn[] System.Data.ForeignKeyConstraint::get_Columns() */, L_35);
			DataRelation_t1483987353 * L_37 = (DataRelation_t1483987353 *)il2cpp_codegen_object_new(DataRelation_t1483987353_il2cpp_TypeInfo_var);
			DataRelation__ctor_m510466167(L_37, L_32, L_34, L_36, /*hidden argument*/NULL);
			V_12 = L_37;
			DataRelation_t1483987353 * L_38 = V_12;
			ArrayList_t2121638921 * L_39 = V_0;
			XmlSchemaWriter_AddForeignKeys_m1874584300(__this, L_38, L_39, (bool)1, /*hidden argument*/NULL);
			goto IL_00f7;
		}

IL_00f7:
		{
			Il2CppObject * L_40 = V_4;
			NullCheck(L_40);
			bool L_41 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_40);
			if (L_41)
			{
				goto IL_002a;
			}
		}

IL_0103:
		{
			IL2CPP_LEAVE(0x11F, FINALLY_0108);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0108;
	}

FINALLY_0108:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_42 = V_4;
			Il2CppObject * L_43 = ((Il2CppObject *)IsInst(L_42, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_13 = L_43;
			if (!L_43)
			{
				goto IL_011e;
			}
		}

IL_0117:
		{
			Il2CppObject * L_44 = V_13;
			NullCheck(L_44);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_44);
		}

IL_011e:
		{
			IL2CPP_END_FINALLY(264)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(264)
	{
		IL2CPP_JUMP_TBL(0x11F, IL_011f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_011f:
	{
		int32_t L_45 = V_2;
		V_2 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_0123:
	{
		int32_t L_46 = V_2;
		DataTableU5BU5D_t1761989358* L_47 = V_1;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		DataRelationU5BU5D_t909637604* L_48 = __this->get_relations_2();
		if (!L_48)
		{
			goto IL_0194;
		}
	}
	{
		DataRelationU5BU5D_t909637604* L_49 = __this->get_relations_2();
		V_14 = L_49;
		V_15 = 0;
		goto IL_0189;
	}

IL_0147:
	{
		DataRelationU5BU5D_t909637604* L_50 = V_14;
		int32_t L_51 = V_15;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		int32_t L_52 = L_51;
		V_16 = ((L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_52)));
		DataRelation_t1483987353 * L_53 = V_16;
		NullCheck(L_53);
		UniqueConstraint_t1006662241 * L_54 = VirtFuncInvoker0< UniqueConstraint_t1006662241 * >::Invoke(11 /* System.Data.UniqueConstraint System.Data.DataRelation::get_ParentKeyConstraint() */, L_53);
		if (!L_54)
		{
			goto IL_0166;
		}
	}
	{
		DataRelation_t1483987353 * L_55 = V_16;
		NullCheck(L_55);
		ForeignKeyConstraint_t1848099579 * L_56 = VirtFuncInvoker0< ForeignKeyConstraint_t1848099579 * >::Invoke(5 /* System.Data.ForeignKeyConstraint System.Data.DataRelation::get_ChildKeyConstraint() */, L_55);
		if (L_56)
		{
			goto IL_0179;
		}
	}

IL_0166:
	{
		ArrayList_t2121638921 * L_57 = __this->get_annotation_10();
		DataRelation_t1483987353 * L_58 = V_16;
		NullCheck(L_57);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_57, L_58);
		goto IL_0183;
	}

IL_0179:
	{
		DataRelation_t1483987353 * L_59 = V_16;
		ArrayList_t2121638921 * L_60 = V_0;
		XmlSchemaWriter_AddForeignKeys_m1874584300(__this, L_59, L_60, (bool)0, /*hidden argument*/NULL);
	}

IL_0183:
	{
		int32_t L_61 = V_15;
		V_15 = ((int32_t)((int32_t)L_61+(int32_t)1));
	}

IL_0189:
	{
		int32_t L_62 = V_15;
		DataRelationU5BU5D_t909637604* L_63 = V_14;
		NullCheck(L_63);
		if ((((int32_t)L_62) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_63)->max_length)))))))
		{
			goto IL_0147;
		}
	}

IL_0194:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::AddUniqueConstraints(System.Data.UniqueConstraint,System.Collections.ArrayList)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3835;
extern Il2CppCodeGenString* _stringLiteral3454438353;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral95;
extern Il2CppCodeGenString* _stringLiteral3229858384;
extern Il2CppCodeGenString* _stringLiteral1101699624;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral718544157;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral1191572447;
extern Il2CppCodeGenString* _stringLiteral114256029;
extern Il2CppCodeGenString* _stringLiteral45710;
extern Il2CppCodeGenString* _stringLiteral97427706;
extern Il2CppCodeGenString* _stringLiteral64;
extern const uint32_t XmlSchemaWriter_AddUniqueConstraints_m621813724_MetadataUsageId;
extern "C"  void XmlSchemaWriter_AddUniqueConstraints_m621813724 (XmlSchemaWriter_t2141469886 * __this, UniqueConstraint_t1006662241 * ___uniq0, ArrayList_t2121638921 * ___names1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_AddUniqueConstraints_m621813724_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataColumnU5BU5D_t3410743138* V_0 = NULL;
	int32_t V_1 = 0;
	DataColumn_t3354469747 * V_2 = NULL;
	String_t* V_3 = NULL;
	DataColumnU5BU5D_t3410743138* V_4 = NULL;
	int32_t V_5 = 0;
	DataColumn_t3354469747 * V_6 = NULL;
	{
		UniqueConstraint_t1006662241 * L_0 = ___uniq0;
		NullCheck(L_0);
		DataColumnU5BU5D_t3410743138* L_1 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(14 /* System.Data.DataColumn[] System.Data.UniqueConstraint::get_Columns() */, L_0);
		V_0 = L_1;
		V_1 = 0;
		goto IL_0023;
	}

IL_000e:
	{
		DataColumnU5BU5D_t3410743138* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_2 = ((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)));
		DataColumn_t3354469747 * L_5 = V_2;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_5);
		if ((!(((uint32_t)L_6) == ((uint32_t)4))))
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_8 = V_1;
		DataColumnU5BU5D_t3410743138* L_9 = V_0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		XmlWriter_t89522450 * L_10 = __this->get_w_0();
		NullCheck(L_10);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_10, _stringLiteral3835, _stringLiteral3454438353, _stringLiteral1440052060);
		ArrayList_t2121638921 * L_11 = ___names1;
		UniqueConstraint_t1006662241 * L_12 = ___uniq0;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Data.Constraint::get_ConstraintName() */, L_12);
		NullCheck(L_11);
		bool L_14 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(32 /* System.Boolean System.Collections.ArrayList::Contains(System.Object) */, L_11, L_13);
		if (L_14)
		{
			goto IL_0079;
		}
	}
	{
		UniqueConstraint_t1006662241 * L_15 = ___uniq0;
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Data.Constraint::get_ConstraintName() */, L_15);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_17 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		XmlWriter_t89522450 * L_18 = __this->get_w_0();
		String_t* L_19 = V_3;
		NullCheck(L_18);
		XmlWriter_WriteAttributeString_m1069599196(L_18, _stringLiteral3373707, L_19, /*hidden argument*/NULL);
		goto IL_00d5;
	}

IL_0079:
	{
		UniqueConstraint_t1006662241 * L_20 = ___uniq0;
		NullCheck(L_20);
		DataTable_t2176726999 * L_21 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(6 /* System.Data.DataTable System.Data.Constraint::get_Table() */, L_20);
		NullCheck(L_21);
		String_t* L_22 = DataTable_get_TableName_m3141812994(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_23 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		UniqueConstraint_t1006662241 * L_24 = ___uniq0;
		NullCheck(L_24);
		String_t* L_25 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Data.Constraint::get_ConstraintName() */, L_24);
		String_t* L_26 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m1825781833(NULL /*static, unused*/, L_23, _stringLiteral95, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		XmlWriter_t89522450 * L_28 = __this->get_w_0();
		String_t* L_29 = V_3;
		NullCheck(L_28);
		XmlWriter_WriteAttributeString_m1069599196(L_28, _stringLiteral3373707, L_29, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_30 = __this->get_w_0();
		UniqueConstraint_t1006662241 * L_31 = ___uniq0;
		NullCheck(L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Data.Constraint::get_ConstraintName() */, L_31);
		String_t* L_33 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		XmlWriter_WriteAttributeString_m1630990645(L_30, _stringLiteral3229858384, _stringLiteral1101699624, _stringLiteral3051173506, L_33, /*hidden argument*/NULL);
	}

IL_00d5:
	{
		ArrayList_t2121638921 * L_34 = ___names1;
		String_t* L_35 = V_3;
		NullCheck(L_34);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_34, L_35);
		UniqueConstraint_t1006662241 * L_36 = ___uniq0;
		NullCheck(L_36);
		bool L_37 = UniqueConstraint_get_IsPrimaryKey_m966562299(L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0107;
		}
	}
	{
		XmlWriter_t89522450 * L_38 = __this->get_w_0();
		NullCheck(L_38);
		XmlWriter_WriteAttributeString_m1630990645(L_38, _stringLiteral3229858384, _stringLiteral718544157, _stringLiteral3051173506, _stringLiteral3569038, /*hidden argument*/NULL);
	}

IL_0107:
	{
		UniqueConstraint_t1006662241 * L_39 = ___uniq0;
		NullCheck(L_39);
		PropertyCollection_t3599376422 * L_40 = Constraint_get_ExtendedProperties_m78551136(L_39, /*hidden argument*/NULL);
		XmlSchemaWriter_AddExtendedPropertyAttributes_m1280916214(__this, L_40, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_41 = __this->get_w_0();
		NullCheck(L_41);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_41, _stringLiteral3835, _stringLiteral1191572447, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_42 = __this->get_w_0();
		String_t* L_43 = XmlSchemaWriter_get_ConstraintPrefix_m4028319215(__this, /*hidden argument*/NULL);
		UniqueConstraint_t1006662241 * L_44 = ___uniq0;
		NullCheck(L_44);
		DataTable_t2176726999 * L_45 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(6 /* System.Data.DataTable System.Data.Constraint::get_Table() */, L_44);
		NullCheck(L_45);
		String_t* L_46 = DataTable_get_TableName_m3141812994(L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_47 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral45710, L_43, L_47, /*hidden argument*/NULL);
		NullCheck(L_42);
		XmlWriter_WriteAttributeString_m1069599196(L_42, _stringLiteral114256029, L_48, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_49 = __this->get_w_0();
		NullCheck(L_49);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_49);
		UniqueConstraint_t1006662241 * L_50 = ___uniq0;
		NullCheck(L_50);
		DataColumnU5BU5D_t3410743138* L_51 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(14 /* System.Data.DataColumn[] System.Data.UniqueConstraint::get_Columns() */, L_50);
		V_4 = L_51;
		V_5 = 0;
		goto IL_020f;
	}

IL_0178:
	{
		DataColumnU5BU5D_t3410743138* L_52 = V_4;
		int32_t L_53 = V_5;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		int32_t L_54 = L_53;
		V_6 = ((L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54)));
		XmlWriter_t89522450 * L_55 = __this->get_w_0();
		NullCheck(L_55);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_55, _stringLiteral3835, _stringLiteral97427706, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_56 = __this->get_w_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_57 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_56);
		XmlWriter_WriteStartAttribute_m904857238(L_56, _stringLiteral114256029, L_57, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_58 = V_6;
		NullCheck(L_58);
		int32_t L_59 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_58);
		if ((!(((uint32_t)L_59) == ((uint32_t)2))))
		{
			goto IL_01cb;
		}
	}
	{
		XmlWriter_t89522450 * L_60 = __this->get_w_0();
		NullCheck(L_60);
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, L_60, _stringLiteral64);
	}

IL_01cb:
	{
		XmlWriter_t89522450 * L_61 = __this->get_w_0();
		String_t* L_62 = XmlSchemaWriter_get_ConstraintPrefix_m4028319215(__this, /*hidden argument*/NULL);
		NullCheck(L_61);
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, L_61, L_62);
		XmlWriter_t89522450 * L_63 = __this->get_w_0();
		DataColumn_t3354469747 * L_64 = V_6;
		NullCheck(L_64);
		String_t* L_65 = DataColumn_get_ColumnName_m409531680(L_64, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_66 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		NullCheck(L_63);
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, L_63, L_66);
		XmlWriter_t89522450 * L_67 = __this->get_w_0();
		NullCheck(L_67);
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, L_67);
		XmlWriter_t89522450 * L_68 = __this->get_w_0();
		NullCheck(L_68);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_68);
		int32_t L_69 = V_5;
		V_5 = ((int32_t)((int32_t)L_69+(int32_t)1));
	}

IL_020f:
	{
		int32_t L_70 = V_5;
		DataColumnU5BU5D_t3410743138* L_71 = V_4;
		NullCheck(L_71);
		if ((((int32_t)L_70) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_71)->max_length)))))))
		{
			goto IL_0178;
		}
	}
	{
		XmlWriter_t89522450 * L_72 = __this->get_w_0();
		NullCheck(L_72);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_72);
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::AddForeignKeys(System.Data.DataRelation,System.Collections.ArrayList,System.Boolean)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* Constraint_t2349953968_il2cpp_TypeInfo_var;
extern Il2CppClass* UniqueConstraint_t1006662241_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3835;
extern Il2CppCodeGenString* _stringLiteral3160312276;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral95;
extern Il2CppCodeGenString* _stringLiteral108391552;
extern Il2CppCodeGenString* _stringLiteral3229858384;
extern Il2CppCodeGenString* _stringLiteral1101741897;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral3984701153;
extern Il2CppCodeGenString* _stringLiteral1191572447;
extern Il2CppCodeGenString* _stringLiteral114256029;
extern Il2CppCodeGenString* _stringLiteral45710;
extern Il2CppCodeGenString* _stringLiteral97427706;
extern Il2CppCodeGenString* _stringLiteral64;
extern const uint32_t XmlSchemaWriter_AddForeignKeys_m1874584300_MetadataUsageId;
extern "C"  void XmlSchemaWriter_AddForeignKeys_m1874584300 (XmlSchemaWriter_t2141469886 * __this, DataRelation_t1483987353 * ___rel0, ArrayList_t2121638921 * ___names1, bool ___isConstraintOnly2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_AddForeignKeys_m1874584300_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataColumnU5BU5D_t3410743138* V_0 = NULL;
	int32_t V_1 = 0;
	DataColumn_t3354469747 * V_2 = NULL;
	DataColumnU5BU5D_t3410743138* V_3 = NULL;
	int32_t V_4 = 0;
	DataColumn_t3354469747 * V_5 = NULL;
	UniqueConstraint_t1006662241 * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	Constraint_t2349953968 * V_8 = NULL;
	Il2CppObject * V_9 = NULL;
	String_t* V_10 = NULL;
	DataColumnU5BU5D_t3410743138* V_11 = NULL;
	int32_t V_12 = 0;
	DataColumn_t3354469747 * V_13 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		DataRelation_t1483987353 * L_0 = ___rel0;
		NullCheck(L_0);
		DataColumnU5BU5D_t3410743138* L_1 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(10 /* System.Data.DataColumn[] System.Data.DataRelation::get_ParentColumns() */, L_0);
		V_0 = L_1;
		V_1 = 0;
		goto IL_0023;
	}

IL_000e:
	{
		DataColumnU5BU5D_t3410743138* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_2 = ((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)));
		DataColumn_t3354469747 * L_5 = V_2;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_5);
		if ((!(((uint32_t)L_6) == ((uint32_t)4))))
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_8 = V_1;
		DataColumnU5BU5D_t3410743138* L_9 = V_0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		DataRelation_t1483987353 * L_10 = ___rel0;
		NullCheck(L_10);
		DataColumnU5BU5D_t3410743138* L_11 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(4 /* System.Data.DataColumn[] System.Data.DataRelation::get_ChildColumns() */, L_10);
		V_3 = L_11;
		V_4 = 0;
		goto IL_0055;
	}

IL_003b:
	{
		DataColumnU5BU5D_t3410743138* L_12 = V_3;
		int32_t L_13 = V_4;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_5 = ((L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14)));
		DataColumn_t3354469747 * L_15 = V_5;
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_15);
		if ((!(((uint32_t)L_16) == ((uint32_t)4))))
		{
			goto IL_004f;
		}
	}
	{
		return;
	}

IL_004f:
	{
		int32_t L_17 = V_4;
		V_4 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0055:
	{
		int32_t L_18 = V_4;
		DataColumnU5BU5D_t3410743138* L_19 = V_3;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_003b;
		}
	}
	{
		XmlWriter_t89522450 * L_20 = __this->get_w_0();
		NullCheck(L_20);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_20, _stringLiteral3835, _stringLiteral3160312276, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_21 = __this->get_w_0();
		DataRelation_t1483987353 * L_22 = ___rel0;
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(13 /* System.String System.Data.DataRelation::get_RelationName() */, L_22);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_24 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		XmlWriter_WriteAttributeString_m1069599196(L_21, _stringLiteral3373707, L_24, /*hidden argument*/NULL);
		V_6 = (UniqueConstraint_t1006662241 *)NULL;
		bool L_25 = ___isConstraintOnly2;
		if (!L_25)
		{
			goto IL_011b;
		}
	}
	{
		DataRelation_t1483987353 * L_26 = ___rel0;
		NullCheck(L_26);
		DataTable_t2176726999 * L_27 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(12 /* System.Data.DataTable System.Data.DataRelation::get_ParentTable() */, L_26);
		NullCheck(L_27);
		ConstraintCollection_t392455726 * L_28 = DataTable_get_Constraints_m4225751151(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Il2CppObject * L_29 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* System.Collections.IEnumerator System.Data.InternalDataCollectionBase::GetEnumerator() */, L_28);
		V_7 = L_29;
	}

IL_00af:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00ee;
		}

IL_00b4:
		{
			Il2CppObject * L_30 = V_7;
			NullCheck(L_30);
			Il2CppObject * L_31 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_30);
			V_8 = ((Constraint_t2349953968 *)CastclassClass(L_31, Constraint_t2349953968_il2cpp_TypeInfo_var));
			Constraint_t2349953968 * L_32 = V_8;
			V_6 = ((UniqueConstraint_t1006662241 *)IsInstClass(L_32, UniqueConstraint_t1006662241_il2cpp_TypeInfo_var));
			UniqueConstraint_t1006662241 * L_33 = V_6;
			if (L_33)
			{
				goto IL_00d7;
			}
		}

IL_00d2:
		{
			goto IL_00ee;
		}

IL_00d7:
		{
			UniqueConstraint_t1006662241 * L_34 = V_6;
			NullCheck(L_34);
			DataColumnU5BU5D_t3410743138* L_35 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(14 /* System.Data.DataColumn[] System.Data.UniqueConstraint::get_Columns() */, L_34);
			DataRelation_t1483987353 * L_36 = ___rel0;
			NullCheck(L_36);
			DataColumnU5BU5D_t3410743138* L_37 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(10 /* System.Data.DataColumn[] System.Data.DataRelation::get_ParentColumns() */, L_36);
			if ((!(((Il2CppObject*)(DataColumnU5BU5D_t3410743138*)L_35) == ((Il2CppObject*)(DataColumnU5BU5D_t3410743138*)L_37))))
			{
				goto IL_00ee;
			}
		}

IL_00e9:
		{
			goto IL_00fa;
		}

IL_00ee:
		{
			Il2CppObject * L_38 = V_7;
			NullCheck(L_38);
			bool L_39 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_38);
			if (L_39)
			{
				goto IL_00b4;
			}
		}

IL_00fa:
		{
			IL2CPP_LEAVE(0x116, FINALLY_00ff);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00ff;
	}

FINALLY_00ff:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_40 = V_7;
			Il2CppObject * L_41 = ((Il2CppObject *)IsInst(L_40, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_9 = L_41;
			if (!L_41)
			{
				goto IL_0115;
			}
		}

IL_010e:
		{
			Il2CppObject * L_42 = V_9;
			NullCheck(L_42);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_42);
		}

IL_0115:
		{
			IL2CPP_END_FINALLY(255)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(255)
	{
		IL2CPP_JUMP_TBL(0x116, IL_0116)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0116:
	{
		goto IL_0123;
	}

IL_011b:
	{
		DataRelation_t1483987353 * L_43 = ___rel0;
		NullCheck(L_43);
		UniqueConstraint_t1006662241 * L_44 = VirtFuncInvoker0< UniqueConstraint_t1006662241 * >::Invoke(11 /* System.Data.UniqueConstraint System.Data.DataRelation::get_ParentKeyConstraint() */, L_43);
		V_6 = L_44;
	}

IL_0123:
	{
		DataRelation_t1483987353 * L_45 = ___rel0;
		NullCheck(L_45);
		DataTable_t2176726999 * L_46 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(12 /* System.Data.DataTable System.Data.DataRelation::get_ParentTable() */, L_45);
		NullCheck(L_46);
		String_t* L_47 = DataTable_get_TableName_m3141812994(L_46, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_48 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		UniqueConstraint_t1006662241 * L_49 = V_6;
		NullCheck(L_49);
		String_t* L_50 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Data.Constraint::get_ConstraintName() */, L_49);
		String_t* L_51 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_52 = String_Concat_m1825781833(NULL /*static, unused*/, L_48, _stringLiteral95, L_51, /*hidden argument*/NULL);
		V_10 = L_52;
		ArrayList_t2121638921 * L_53 = ___names1;
		String_t* L_54 = V_10;
		NullCheck(L_53);
		bool L_55 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(32 /* System.Boolean System.Collections.ArrayList::Contains(System.Object) */, L_53, L_54);
		if (!L_55)
		{
			goto IL_0190;
		}
	}
	{
		XmlWriter_t89522450 * L_56 = __this->get_w_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_57 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_56);
		XmlWriter_WriteStartAttribute_m904857238(L_56, _stringLiteral108391552, L_57, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_58 = __this->get_w_0();
		String_t* L_59 = V_10;
		String_t* L_60 = __this->get_dataSetNamespace_5();
		NullCheck(L_58);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(20 /* System.Void System.Xml.XmlWriter::WriteQualifiedName(System.String,System.String) */, L_58, L_59, L_60);
		XmlWriter_t89522450 * L_61 = __this->get_w_0();
		NullCheck(L_61);
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, L_61);
		goto IL_01cd;
	}

IL_0190:
	{
		XmlWriter_t89522450 * L_62 = __this->get_w_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_63 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_62);
		XmlWriter_WriteStartAttribute_m904857238(L_62, _stringLiteral108391552, L_63, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_64 = __this->get_w_0();
		UniqueConstraint_t1006662241 * L_65 = V_6;
		NullCheck(L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Data.Constraint::get_ConstraintName() */, L_65);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_67 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		String_t* L_68 = __this->get_dataSetNamespace_5();
		NullCheck(L_64);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(20 /* System.Void System.Xml.XmlWriter::WriteQualifiedName(System.String,System.String) */, L_64, L_67, L_68);
		XmlWriter_t89522450 * L_69 = __this->get_w_0();
		NullCheck(L_69);
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, L_69);
	}

IL_01cd:
	{
		bool L_70 = ___isConstraintOnly2;
		if (!L_70)
		{
			goto IL_01f7;
		}
	}
	{
		XmlWriter_t89522450 * L_71 = __this->get_w_0();
		NullCheck(L_71);
		XmlWriter_WriteAttributeString_m1630990645(L_71, _stringLiteral3229858384, _stringLiteral1101741897, _stringLiteral3051173506, _stringLiteral3569038, /*hidden argument*/NULL);
		goto IL_0221;
	}

IL_01f7:
	{
		DataRelation_t1483987353 * L_72 = ___rel0;
		NullCheck(L_72);
		bool L_73 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean System.Data.DataRelation::get_Nested() */, L_72);
		if (!L_73)
		{
			goto IL_0221;
		}
	}
	{
		XmlWriter_t89522450 * L_74 = __this->get_w_0();
		NullCheck(L_74);
		XmlWriter_WriteAttributeString_m1630990645(L_74, _stringLiteral3229858384, _stringLiteral3984701153, _stringLiteral3051173506, _stringLiteral3569038, /*hidden argument*/NULL);
	}

IL_0221:
	{
		UniqueConstraint_t1006662241 * L_75 = V_6;
		NullCheck(L_75);
		PropertyCollection_t3599376422 * L_76 = Constraint_get_ExtendedProperties_m78551136(L_75, /*hidden argument*/NULL);
		XmlSchemaWriter_AddExtendedPropertyAttributes_m1280916214(__this, L_76, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_77 = __this->get_w_0();
		NullCheck(L_77);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_77, _stringLiteral3835, _stringLiteral1191572447, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_78 = __this->get_w_0();
		String_t* L_79 = XmlSchemaWriter_get_ConstraintPrefix_m4028319215(__this, /*hidden argument*/NULL);
		DataRelation_t1483987353 * L_80 = ___rel0;
		NullCheck(L_80);
		DataTable_t2176726999 * L_81 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(6 /* System.Data.DataTable System.Data.DataRelation::get_ChildTable() */, L_80);
		NullCheck(L_81);
		String_t* L_82 = DataTable_get_TableName_m3141812994(L_81, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_83 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_82, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_84 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral45710, L_79, L_83, /*hidden argument*/NULL);
		NullCheck(L_78);
		XmlWriter_WriteAttributeString_m1069599196(L_78, _stringLiteral114256029, L_84, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_85 = __this->get_w_0();
		NullCheck(L_85);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_85);
		DataRelation_t1483987353 * L_86 = ___rel0;
		NullCheck(L_86);
		DataColumnU5BU5D_t3410743138* L_87 = VirtFuncInvoker0< DataColumnU5BU5D_t3410743138* >::Invoke(4 /* System.Data.DataColumn[] System.Data.DataRelation::get_ChildColumns() */, L_86);
		V_11 = L_87;
		V_12 = 0;
		goto IL_032a;
	}

IL_0293:
	{
		DataColumnU5BU5D_t3410743138* L_88 = V_11;
		int32_t L_89 = V_12;
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, L_89);
		int32_t L_90 = L_89;
		V_13 = ((L_88)->GetAt(static_cast<il2cpp_array_size_t>(L_90)));
		XmlWriter_t89522450 * L_91 = __this->get_w_0();
		NullCheck(L_91);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_91, _stringLiteral3835, _stringLiteral97427706, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_92 = __this->get_w_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_93 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_92);
		XmlWriter_WriteStartAttribute_m904857238(L_92, _stringLiteral114256029, L_93, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_94 = V_13;
		NullCheck(L_94);
		int32_t L_95 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Data.MappingType System.Data.DataColumn::get_ColumnMapping() */, L_94);
		if ((!(((uint32_t)L_95) == ((uint32_t)2))))
		{
			goto IL_02e6;
		}
	}
	{
		XmlWriter_t89522450 * L_96 = __this->get_w_0();
		NullCheck(L_96);
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, L_96, _stringLiteral64);
	}

IL_02e6:
	{
		XmlWriter_t89522450 * L_97 = __this->get_w_0();
		String_t* L_98 = XmlSchemaWriter_get_ConstraintPrefix_m4028319215(__this, /*hidden argument*/NULL);
		NullCheck(L_97);
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, L_97, L_98);
		XmlWriter_t89522450 * L_99 = __this->get_w_0();
		DataColumn_t3354469747 * L_100 = V_13;
		NullCheck(L_100);
		String_t* L_101 = DataColumn_get_ColumnName_m409531680(L_100, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_102 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_101, /*hidden argument*/NULL);
		NullCheck(L_99);
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, L_99, L_102);
		XmlWriter_t89522450 * L_103 = __this->get_w_0();
		NullCheck(L_103);
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, L_103);
		XmlWriter_t89522450 * L_104 = __this->get_w_0();
		NullCheck(L_104);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_104);
		int32_t L_105 = V_12;
		V_12 = ((int32_t)((int32_t)L_105+(int32_t)1));
	}

IL_032a:
	{
		int32_t L_106 = V_12;
		DataColumnU5BU5D_t3410743138* L_107 = V_11;
		NullCheck(L_107);
		if ((((int32_t)L_106) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_107)->max_length)))))))
		{
			goto IL_0293;
		}
	}
	{
		XmlWriter_t89522450 * L_108 = __this->get_w_0();
		NullCheck(L_108);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_108);
		return;
	}
}
// System.Boolean System.Data.XmlSchemaWriter::CheckExtendedPropertyExists(System.Data.DataTable[],System.Data.DataRelation[])
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* DataColumn_t3354469747_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Constraint_t2349953968_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaWriter_CheckExtendedPropertyExists_m4066524780_MetadataUsageId;
extern "C"  bool XmlSchemaWriter_CheckExtendedPropertyExists_m4066524780 (XmlSchemaWriter_t2141469886 * __this, DataTableU5BU5D_t1761989358* ___tables0, DataRelationU5BU5D_t909637604* ___relations1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_CheckExtendedPropertyExists_m4066524780_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataTableU5BU5D_t1761989358* V_0 = NULL;
	int32_t V_1 = 0;
	DataTable_t2176726999 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	DataColumn_t3354469747 * V_4 = NULL;
	bool V_5 = false;
	Il2CppObject * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	Constraint_t2349953968 * V_8 = NULL;
	Il2CppObject * V_9 = NULL;
	DataRelationU5BU5D_t909637604* V_10 = NULL;
	int32_t V_11 = 0;
	DataRelation_t1483987353 * V_12 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		PropertyCollection_t3599376422 * L_0 = __this->get_dataSetProperties_6();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_0);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		return (bool)1;
	}

IL_0013:
	{
		DataTableU5BU5D_t1761989358* L_2 = ___tables0;
		V_0 = L_2;
		V_1 = 0;
		goto IL_00f7;
	}

IL_001c:
	{
		DataTableU5BU5D_t1761989358* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_2 = ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
		DataTable_t2176726999 * L_6 = V_2;
		NullCheck(L_6);
		PropertyCollection_t3599376422 * L_7 = DataTable_get_ExtendedProperties_m3062393549(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_7);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_0033;
		}
	}
	{
		return (bool)1;
	}

IL_0033:
	{
		DataTable_t2176726999 * L_9 = V_2;
		NullCheck(L_9);
		DataColumnCollection_t3528392753 * L_10 = DataTable_get_Columns_m220042291(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Il2CppObject * L_11 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* System.Collections.IEnumerator System.Data.InternalDataCollectionBase::GetEnumerator() */, L_10);
		V_3 = L_11;
	}

IL_003f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006b;
		}

IL_0044:
		{
			Il2CppObject * L_12 = V_3;
			NullCheck(L_12);
			Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_12);
			V_4 = ((DataColumn_t3354469747 *)CastclassClass(L_13, DataColumn_t3354469747_il2cpp_TypeInfo_var));
			DataColumn_t3354469747 * L_14 = V_4;
			NullCheck(L_14);
			PropertyCollection_t3599376422 * L_15 = DataColumn_get_ExtendedProperties_m3441609827(L_14, /*hidden argument*/NULL);
			NullCheck(L_15);
			int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_15);
			if ((((int32_t)L_16) <= ((int32_t)0)))
			{
				goto IL_006b;
			}
		}

IL_0063:
		{
			V_5 = (bool)1;
			IL2CPP_LEAVE(0x141, FINALLY_007b);
		}

IL_006b:
		{
			Il2CppObject * L_17 = V_3;
			NullCheck(L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_0044;
			}
		}

IL_0076:
		{
			IL2CPP_LEAVE(0x91, FINALLY_007b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_007b;
	}

FINALLY_007b:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_19 = V_3;
			Il2CppObject * L_20 = ((Il2CppObject *)IsInst(L_19, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_6 = L_20;
			if (!L_20)
			{
				goto IL_0090;
			}
		}

IL_0089:
		{
			Il2CppObject * L_21 = V_6;
			NullCheck(L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_21);
		}

IL_0090:
		{
			IL2CPP_END_FINALLY(123)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(123)
	{
		IL2CPP_JUMP_TBL(0x141, IL_0141)
		IL2CPP_JUMP_TBL(0x91, IL_0091)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0091:
	{
		DataTable_t2176726999 * L_22 = V_2;
		NullCheck(L_22);
		ConstraintCollection_t392455726 * L_23 = DataTable_get_Constraints_m4225751151(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Il2CppObject * L_24 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* System.Collections.IEnumerator System.Data.InternalDataCollectionBase::GetEnumerator() */, L_23);
		V_7 = L_24;
	}

IL_009e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00cb;
		}

IL_00a3:
		{
			Il2CppObject * L_25 = V_7;
			NullCheck(L_25);
			Il2CppObject * L_26 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_25);
			V_8 = ((Constraint_t2349953968 *)CastclassClass(L_26, Constraint_t2349953968_il2cpp_TypeInfo_var));
			Constraint_t2349953968 * L_27 = V_8;
			NullCheck(L_27);
			PropertyCollection_t3599376422 * L_28 = Constraint_get_ExtendedProperties_m78551136(L_27, /*hidden argument*/NULL);
			NullCheck(L_28);
			int32_t L_29 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_28);
			if ((((int32_t)L_29) <= ((int32_t)0)))
			{
				goto IL_00cb;
			}
		}

IL_00c3:
		{
			V_5 = (bool)1;
			IL2CPP_LEAVE(0x141, FINALLY_00dc);
		}

IL_00cb:
		{
			Il2CppObject * L_30 = V_7;
			NullCheck(L_30);
			bool L_31 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_30);
			if (L_31)
			{
				goto IL_00a3;
			}
		}

IL_00d7:
		{
			IL2CPP_LEAVE(0xF3, FINALLY_00dc);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00dc;
	}

FINALLY_00dc:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_32 = V_7;
			Il2CppObject * L_33 = ((Il2CppObject *)IsInst(L_32, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_9 = L_33;
			if (!L_33)
			{
				goto IL_00f2;
			}
		}

IL_00eb:
		{
			Il2CppObject * L_34 = V_9;
			NullCheck(L_34);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_34);
		}

IL_00f2:
		{
			IL2CPP_END_FINALLY(220)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(220)
	{
		IL2CPP_JUMP_TBL(0x141, IL_0141)
		IL2CPP_JUMP_TBL(0xF3, IL_00f3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00f3:
	{
		int32_t L_35 = V_1;
		V_1 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00f7:
	{
		int32_t L_36 = V_1;
		DataTableU5BU5D_t1761989358* L_37 = V_0;
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_37)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		DataRelationU5BU5D_t909637604* L_38 = ___relations1;
		if (L_38)
		{
			goto IL_0108;
		}
	}
	{
		return (bool)0;
	}

IL_0108:
	{
		DataRelationU5BU5D_t909637604* L_39 = ___relations1;
		V_10 = L_39;
		V_11 = 0;
		goto IL_0134;
	}

IL_0113:
	{
		DataRelationU5BU5D_t909637604* L_40 = V_10;
		int32_t L_41 = V_11;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_41);
		int32_t L_42 = L_41;
		V_12 = ((L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42)));
		DataRelation_t1483987353 * L_43 = V_12;
		NullCheck(L_43);
		PropertyCollection_t3599376422 * L_44 = DataRelation_get_ExtendedProperties_m1057031625(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		int32_t L_45 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_44);
		if ((((int32_t)L_45) <= ((int32_t)0)))
		{
			goto IL_012e;
		}
	}
	{
		return (bool)1;
	}

IL_012e:
	{
		int32_t L_46 = V_11;
		V_11 = ((int32_t)((int32_t)L_46+(int32_t)1));
	}

IL_0134:
	{
		int32_t L_47 = V_11;
		DataRelationU5BU5D_t909637604* L_48 = V_10;
		NullCheck(L_48);
		if ((((int32_t)L_47) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_48)->max_length)))))))
		{
			goto IL_0113;
		}
	}
	{
		return (bool)0;
	}

IL_0141:
	{
		bool L_49 = V_5;
		return L_49;
	}
}
// System.Void System.Data.XmlSchemaWriter::AddExtendedPropertyAttributes(System.Data.PropertyCollection)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* DictionaryEntry_t130027246_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlConvert_t1882388356_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3230232073;
extern Il2CppCodeGenString* _stringLiteral3051547195;
extern const uint32_t XmlSchemaWriter_AddExtendedPropertyAttributes_m1280916214_MetadataUsageId;
extern "C"  void XmlSchemaWriter_AddExtendedPropertyAttributes_m1280916214 (XmlSchemaWriter_t2141469886 * __this, PropertyCollection_t3599376422 * ___props0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_AddExtendedPropertyAttributes_m1280916214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	DictionaryEntry_t130027246  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		PropertyCollection_t3599376422 * L_0 = ___props0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(32 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_0);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006c;
		}

IL_000c:
		{
			Il2CppObject * L_2 = V_0;
			NullCheck(L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_2);
			V_1 = ((*(DictionaryEntry_t130027246 *)((DictionaryEntry_t130027246 *)UnBox (L_3, DictionaryEntry_t130027246_il2cpp_TypeInfo_var))));
			XmlWriter_t89522450 * L_4 = __this->get_w_0();
			Il2CppObject * L_5 = DictionaryEntry_get_Key_m3516209325((&V_1), /*hidden argument*/NULL);
			NullCheck(L_5);
			String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
			IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
			String_t* L_7 = XmlConvert_EncodeName_m948344207(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			NullCheck(L_4);
			VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(23 /* System.Void System.Xml.XmlWriter::WriteStartAttribute(System.String,System.String,System.String) */, L_4, _stringLiteral3230232073, L_7, _stringLiteral3051547195);
			Il2CppObject * L_8 = DictionaryEntry_get_Value_m4281303039((&V_1), /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_0061;
			}
		}

IL_004a:
		{
			XmlWriter_t89522450 * L_9 = __this->get_w_0();
			Il2CppObject * L_10 = DictionaryEntry_get_Value_m4281303039((&V_1), /*hidden argument*/NULL);
			String_t* L_11 = DataSet_WriteObjectXml_m942680311(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			NullCheck(L_9);
			VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, L_9, L_11);
		}

IL_0061:
		{
			XmlWriter_t89522450 * L_12 = __this->get_w_0();
			NullCheck(L_12);
			VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, L_12);
		}

IL_006c:
		{
			Il2CppObject * L_13 = V_0;
			NullCheck(L_13);
			bool L_14 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_13);
			if (L_14)
			{
				goto IL_000c;
			}
		}

IL_0077:
		{
			IL2CPP_LEAVE(0x90, FINALLY_007c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_007c;
	}

FINALLY_007c:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_15 = V_0;
			Il2CppObject * L_16 = ((Il2CppObject *)IsInst(L_15, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_2 = L_16;
			if (!L_16)
			{
				goto IL_008f;
			}
		}

IL_0089:
		{
			Il2CppObject * L_17 = V_2;
			NullCheck(L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_17);
		}

IL_008f:
		{
			IL2CPP_END_FINALLY(124)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(124)
	{
		IL2CPP_JUMP_TBL(0x90, IL_0090)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0090:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::WriteTableElement(System.Data.DataTable)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3835;
extern Il2CppCodeGenString* _stringLiteral2632130300;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern const uint32_t XmlSchemaWriter_WriteTableElement_m3446834206_MetadataUsageId;
extern "C"  void XmlSchemaWriter_WriteTableElement_m3446834206 (XmlSchemaWriter_t2141469886 * __this, DataTable_t2176726999 * ___table0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_WriteTableElement_m3446834206_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlWriter_t89522450 * L_0 = __this->get_w_0();
		NullCheck(L_0);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_0, _stringLiteral3835, _stringLiteral2632130300, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_1 = __this->get_w_0();
		DataTable_t2176726999 * L_2 = ___table0;
		NullCheck(L_2);
		String_t* L_3 = DataTable_get_TableName_m3141812994(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_4 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		XmlWriter_WriteAttributeString_m1069599196(L_1, _stringLiteral3373707, L_4, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_5 = ___table0;
		NullCheck(L_5);
		PropertyCollection_t3599376422 * L_6 = DataTable_get_ExtendedProperties_m3062393549(L_5, /*hidden argument*/NULL);
		XmlSchemaWriter_AddExtendedPropertyAttributes_m1280916214(__this, L_6, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_7 = ___table0;
		XmlSchemaWriter_WriteTableType_m163055952(__this, L_7, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_8 = __this->get_w_0();
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_8);
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::WriteTableType(System.Data.DataTable)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlConvert_t1882388356_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* DataRelation_t1483987353_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* DataColumn_t3354469747_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3835;
extern Il2CppCodeGenString* _stringLiteral405639178;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern Il2CppCodeGenString* _stringLiteral3068112487;
extern Il2CppCodeGenString* _stringLiteral3229858384;
extern Il2CppCodeGenString* _stringLiteral4182770465;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral457658961;
extern Il2CppCodeGenString* _stringLiteral3682409535;
extern Il2CppCodeGenString* _stringLiteral3016401;
extern Il2CppCodeGenString* _stringLiteral1349547969;
extern const uint32_t XmlSchemaWriter_WriteTableType_m163055952_MetadataUsageId;
extern "C"  void XmlSchemaWriter_WriteTableType_m163055952 (XmlSchemaWriter_t2141469886 * __this, DataTable_t2176726999 * ___table0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_WriteTableType_m163055952_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t2121638921 * V_0 = NULL;
	ArrayList_t2121638921 * V_1 = NULL;
	DataColumn_t3354469747 * V_2 = NULL;
	bool V_3 = false;
	Il2CppObject * V_4 = NULL;
	DataRelation_t1483987353 * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	DataColumn_t3354469747 * V_8 = NULL;
	Il2CppObject * V_9 = NULL;
	Il2CppObject * V_10 = NULL;
	DataRelation_t1483987353 * V_11 = NULL;
	Il2CppObject * V_12 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		DataTable_t2176726999 * L_0 = ___table0;
		DataSet_SplitColumns_m85014974(NULL /*static, unused*/, L_0, (&V_1), (&V_0), (&V_2), /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_1 = __this->get_w_0();
		NullCheck(L_1);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_1, _stringLiteral3835, _stringLiteral405639178, _stringLiteral1440052060);
		DataColumn_t3354469747 * L_2 = V_2;
		if (!L_2)
		{
			goto IL_00f3;
		}
	}
	{
		XmlWriter_t89522450 * L_3 = __this->get_w_0();
		NullCheck(L_3);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_3, _stringLiteral3835, _stringLiteral3068112487, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_4 = __this->get_w_0();
		DataColumn_t3354469747 * L_5 = V_2;
		NullCheck(L_5);
		String_t* L_6 = DataColumn_get_ColumnName_m409531680(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_7 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		XmlWriter_WriteAttributeString_m1630990645(L_4, _stringLiteral3229858384, _stringLiteral4182770465, _stringLiteral3051173506, L_7, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_8 = __this->get_w_0();
		DataColumn_t3354469747 * L_9 = V_2;
		NullCheck(L_9);
		int32_t L_10 = DataColumn_get_Ordinal_m317938227(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		String_t* L_11 = XmlConvert_ToString_m1296446159(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		XmlWriter_WriteAttributeString_m1630990645(L_8, _stringLiteral3229858384, _stringLiteral457658961, _stringLiteral3051173506, L_11, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_12 = __this->get_w_0();
		NullCheck(L_12);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_12, _stringLiteral3835, _stringLiteral3682409535, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_13 = __this->get_w_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_13);
		XmlWriter_WriteStartAttribute_m904857238(L_13, _stringLiteral3016401, L_14, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_15 = V_2;
		NullCheck(L_15);
		Type_t * L_16 = DataColumn_get_DataType_m3376662490(L_15, /*hidden argument*/NULL);
		XmlQualifiedName_t176365656 * L_17 = XmlSchemaWriter_MapType_m2141526126(__this, L_16, /*hidden argument*/NULL);
		XmlSchemaWriter_WriteQName_m2811822933(__this, L_17, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_18 = __this->get_w_0();
		NullCheck(L_18);
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, L_18);
		ArrayList_t2121638921 * L_19 = V_1;
		XmlSchemaWriter_WriteTableAttributes_m1436721665(__this, L_19, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_20 = __this->get_w_0();
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_20);
		goto IL_0243;
	}

IL_00f3:
	{
		ArrayList_t2121638921 * L_21 = V_1;
		XmlSchemaWriter_WriteTableAttributes_m1436721665(__this, L_21, /*hidden argument*/NULL);
		V_3 = (bool)0;
		DataTable_t2176726999 * L_22 = ___table0;
		NullCheck(L_22);
		DataRelationCollection_t267599063 * L_23 = DataTable_get_ParentRelations_m3551104541(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Il2CppObject * L_24 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* System.Collections.IEnumerator System.Data.InternalDataCollectionBase::GetEnumerator() */, L_23);
		V_4 = L_24;
	}

IL_0109:
	try
	{ // begin try (depth: 1)
		{
			goto IL_012f;
		}

IL_010e:
		{
			Il2CppObject * L_25 = V_4;
			NullCheck(L_25);
			Il2CppObject * L_26 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_25);
			V_5 = ((DataRelation_t1483987353 *)CastclassClass(L_26, DataRelation_t1483987353_il2cpp_TypeInfo_var));
			DataRelation_t1483987353 * L_27 = V_5;
			NullCheck(L_27);
			bool L_28 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean System.Data.DataRelation::get_Nested() */, L_27);
			if (!L_28)
			{
				goto IL_012f;
			}
		}

IL_0128:
		{
			V_3 = (bool)1;
			goto IL_013b;
		}

IL_012f:
		{
			Il2CppObject * L_29 = V_4;
			NullCheck(L_29);
			bool L_30 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_29);
			if (L_30)
			{
				goto IL_010e;
			}
		}

IL_013b:
		{
			IL2CPP_LEAVE(0x157, FINALLY_0140);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0140;
	}

FINALLY_0140:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_31 = V_4;
			Il2CppObject * L_32 = ((Il2CppObject *)IsInst(L_31, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_6 = L_32;
			if (!L_32)
			{
				goto IL_0156;
			}
		}

IL_014f:
		{
			Il2CppObject * L_33 = V_6;
			NullCheck(L_33);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_33);
		}

IL_0156:
		{
			IL2CPP_END_FINALLY(320)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(320)
	{
		IL2CPP_JUMP_TBL(0x157, IL_0157)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0157:
	{
		ArrayList_t2121638921 * L_34 = V_0;
		NullCheck(L_34);
		int32_t L_35 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_34);
		if ((((int32_t)L_35) > ((int32_t)0)))
		{
			goto IL_0177;
		}
	}
	{
		bool L_36 = V_3;
		if (!L_36)
		{
			goto IL_0243;
		}
	}
	{
		DataTableU5BU5D_t1761989358* L_37 = __this->get_tables_1();
		NullCheck(L_37);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_37)->max_length))))) >= ((int32_t)2)))
		{
			goto IL_0243;
		}
	}

IL_0177:
	{
		XmlWriter_t89522450 * L_38 = __this->get_w_0();
		NullCheck(L_38);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_38, _stringLiteral3835, _stringLiteral1349547969, _stringLiteral1440052060);
		ArrayList_t2121638921 * L_39 = V_0;
		NullCheck(L_39);
		Il2CppObject * L_40 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_39);
		V_7 = L_40;
	}

IL_0199:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01b4;
		}

IL_019e:
		{
			Il2CppObject * L_41 = V_7;
			NullCheck(L_41);
			Il2CppObject * L_42 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_41);
			V_8 = ((DataColumn_t3354469747 *)CastclassClass(L_42, DataColumn_t3354469747_il2cpp_TypeInfo_var));
			DataColumn_t3354469747 * L_43 = V_8;
			XmlSchemaWriter_WriteTableTypeParticles_m4266320007(__this, L_43, /*hidden argument*/NULL);
		}

IL_01b4:
		{
			Il2CppObject * L_44 = V_7;
			NullCheck(L_44);
			bool L_45 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_44);
			if (L_45)
			{
				goto IL_019e;
			}
		}

IL_01c0:
		{
			IL2CPP_LEAVE(0x1DC, FINALLY_01c5);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01c5;
	}

FINALLY_01c5:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_46 = V_7;
			Il2CppObject * L_47 = ((Il2CppObject *)IsInst(L_46, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_9 = L_47;
			if (!L_47)
			{
				goto IL_01db;
			}
		}

IL_01d4:
		{
			Il2CppObject * L_48 = V_9;
			NullCheck(L_48);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_48);
		}

IL_01db:
		{
			IL2CPP_END_FINALLY(453)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(453)
	{
		IL2CPP_JUMP_TBL(0x1DC, IL_01dc)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01dc:
	{
		DataTable_t2176726999 * L_49 = ___table0;
		NullCheck(L_49);
		DataRelationCollection_t267599063 * L_50 = DataTable_get_ChildRelations_m2551063725(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		Il2CppObject * L_51 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* System.Collections.IEnumerator System.Data.InternalDataCollectionBase::GetEnumerator() */, L_50);
		V_10 = L_51;
	}

IL_01e9:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0210;
		}

IL_01ee:
		{
			Il2CppObject * L_52 = V_10;
			NullCheck(L_52);
			Il2CppObject * L_53 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_52);
			V_11 = ((DataRelation_t1483987353 *)CastclassClass(L_53, DataRelation_t1483987353_il2cpp_TypeInfo_var));
			DataRelation_t1483987353 * L_54 = V_11;
			NullCheck(L_54);
			bool L_55 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean System.Data.DataRelation::get_Nested() */, L_54);
			if (!L_55)
			{
				goto IL_0210;
			}
		}

IL_0208:
		{
			DataRelation_t1483987353 * L_56 = V_11;
			XmlSchemaWriter_WriteChildRelations_m841801207(__this, L_56, /*hidden argument*/NULL);
		}

IL_0210:
		{
			Il2CppObject * L_57 = V_10;
			NullCheck(L_57);
			bool L_58 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_57);
			if (L_58)
			{
				goto IL_01ee;
			}
		}

IL_021c:
		{
			IL2CPP_LEAVE(0x238, FINALLY_0221);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0221;
	}

FINALLY_0221:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_59 = V_10;
			Il2CppObject * L_60 = ((Il2CppObject *)IsInst(L_59, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_12 = L_60;
			if (!L_60)
			{
				goto IL_0237;
			}
		}

IL_0230:
		{
			Il2CppObject * L_61 = V_12;
			NullCheck(L_61);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_61);
		}

IL_0237:
		{
			IL2CPP_END_FINALLY(545)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(545)
	{
		IL2CPP_JUMP_TBL(0x238, IL_0238)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0238:
	{
		XmlWriter_t89522450 * L_62 = __this->get_w_0();
		NullCheck(L_62);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_62);
	}

IL_0243:
	{
		XmlWriter_t89522450 * L_63 = __this->get_w_0();
		NullCheck(L_63);
		VirtActionInvoker0::Invoke(19 /* System.Void System.Xml.XmlWriter::WriteFullEndElement() */, L_63);
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::WriteTableTypeParticles(System.Data.DataColumn)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Char_t2778706699_0_0_0_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlConvert_t1882388356_il2cpp_TypeInfo_var;
extern Il2CppClass* DataColumn_t3354469747_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlConstants_t2555109579_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlQualifiedName_t176365656_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3835;
extern Il2CppCodeGenString* _stringLiteral2632130300;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3229858384;
extern Il2CppCodeGenString* _stringLiteral2217226694;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral2659644000;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral1127089393;
extern Il2CppCodeGenString* _stringLiteral1127103820;
extern Il2CppCodeGenString* _stringLiteral1544803905;
extern Il2CppCodeGenString* _stringLiteral3491927682;
extern Il2CppCodeGenString* _stringLiteral3575610;
extern Il2CppCodeGenString* _stringLiteral1853714980;
extern Il2CppCodeGenString* _stringLiteral64804601;
extern Il2CppCodeGenString* _stringLiteral48;
extern const uint32_t XmlSchemaWriter_WriteTableTypeParticles_m4266320007_MetadataUsageId;
extern "C"  void XmlSchemaWriter_WriteTableTypeParticles_m4266320007 (XmlSchemaWriter_t2141469886 * __this, DataColumn_t3354469747 * ___col0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_WriteTableTypeParticles_m4266320007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlQualifiedName_t176365656 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		XmlWriter_t89522450 * L_0 = __this->get_w_0();
		NullCheck(L_0);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_0, _stringLiteral3835, _stringLiteral2632130300, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_1 = __this->get_w_0();
		DataColumn_t3354469747 * L_2 = ___col0;
		NullCheck(L_2);
		String_t* L_3 = DataColumn_get_ColumnName_m409531680(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_4 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		XmlWriter_WriteAttributeString_m1069599196(L_1, _stringLiteral3373707, L_4, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_5 = ___col0;
		NullCheck(L_5);
		String_t* L_6 = DataColumn_get_ColumnName_m409531680(L_5, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_7 = ___col0;
		NullCheck(L_7);
		String_t* L_8 = DataColumn_get_Caption_m971990217(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0080;
		}
	}
	{
		DataColumn_t3354469747 * L_10 = ___col0;
		NullCheck(L_10);
		String_t* L_11 = DataColumn_get_Caption_m971990217(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_13 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0080;
		}
	}
	{
		XmlWriter_t89522450 * L_14 = __this->get_w_0();
		DataColumn_t3354469747 * L_15 = ___col0;
		NullCheck(L_15);
		String_t* L_16 = DataColumn_get_Caption_m971990217(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		XmlWriter_WriteAttributeString_m1630990645(L_14, _stringLiteral3229858384, _stringLiteral2217226694, _stringLiteral3051173506, L_16, /*hidden argument*/NULL);
	}

IL_0080:
	{
		DataColumn_t3354469747 * L_17 = ___col0;
		NullCheck(L_17);
		bool L_18 = DataColumn_get_AutoIncrement_m3258447900(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00aa;
		}
	}
	{
		XmlWriter_t89522450 * L_19 = __this->get_w_0();
		NullCheck(L_19);
		XmlWriter_WriteAttributeString_m1630990645(L_19, _stringLiteral3229858384, _stringLiteral2659644000, _stringLiteral3051173506, _stringLiteral3569038, /*hidden argument*/NULL);
	}

IL_00aa:
	{
		DataColumn_t3354469747 * L_20 = ___col0;
		NullCheck(L_20);
		int64_t L_21 = DataColumn_get_AutoIncrementSeed_m1857508850(L_20, /*hidden argument*/NULL);
		if ((((int64_t)L_21) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_00dc;
		}
	}
	{
		XmlWriter_t89522450 * L_22 = __this->get_w_0();
		DataColumn_t3354469747 * L_23 = ___col0;
		NullCheck(L_23);
		int64_t L_24 = DataColumn_get_AutoIncrementSeed_m1857508850(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		String_t* L_25 = XmlConvert_ToString_m1296449104(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		XmlWriter_WriteAttributeString_m1630990645(L_22, _stringLiteral3229858384, _stringLiteral1127089393, _stringLiteral3051173506, L_25, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		DataColumn_t3354469747 * L_26 = ___col0;
		NullCheck(L_26);
		int64_t L_27 = DataColumn_get_AutoIncrementStep_m1871373197(L_26, /*hidden argument*/NULL);
		if ((((int64_t)L_27) == ((int64_t)(((int64_t)((int64_t)1))))))
		{
			goto IL_010e;
		}
	}
	{
		XmlWriter_t89522450 * L_28 = __this->get_w_0();
		DataColumn_t3354469747 * L_29 = ___col0;
		NullCheck(L_29);
		int64_t L_30 = DataColumn_get_AutoIncrementStep_m1871373197(L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		String_t* L_31 = XmlConvert_ToString_m1296449104(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		XmlWriter_WriteAttributeString_m1630990645(L_28, _stringLiteral3229858384, _stringLiteral1127103820, _stringLiteral3051173506, L_31, /*hidden argument*/NULL);
	}

IL_010e:
	{
		DataColumn_t3354469747 * L_32 = ___col0;
		NullCheck(L_32);
		Type_t * L_33 = DataColumn_get_DataType_m3376662490(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DataColumn_t3354469747_il2cpp_TypeInfo_var);
		Il2CppObject * L_34 = DataColumn_GetDefaultValueForType_m983945556(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_35 = ___col0;
		NullCheck(L_35);
		Il2CppObject * L_36 = DataColumn_get_DefaultValue_m2400376481(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		bool L_37 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_34, L_36);
		if (L_37)
		{
			goto IL_0144;
		}
	}
	{
		XmlWriter_t89522450 * L_38 = __this->get_w_0();
		DataColumn_t3354469747 * L_39 = ___col0;
		NullCheck(L_39);
		Il2CppObject * L_40 = DataColumn_get_DefaultValue_m2400376481(L_39, /*hidden argument*/NULL);
		String_t* L_41 = DataSet_WriteObjectXml_m942680311(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		NullCheck(L_38);
		XmlWriter_WriteAttributeString_m1069599196(L_38, _stringLiteral1544803905, L_41, /*hidden argument*/NULL);
	}

IL_0144:
	{
		DataColumn_t3354469747 * L_42 = ___col0;
		NullCheck(L_42);
		bool L_43 = DataColumn_get_ReadOnly_m3035570536(L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_016e;
		}
	}
	{
		XmlWriter_t89522450 * L_44 = __this->get_w_0();
		NullCheck(L_44);
		XmlWriter_WriteAttributeString_m1630990645(L_44, _stringLiteral3229858384, _stringLiteral3491927682, _stringLiteral3051173506, _stringLiteral3569038, /*hidden argument*/NULL);
	}

IL_016e:
	{
		V_0 = (XmlQualifiedName_t176365656 *)NULL;
		DataColumn_t3354469747 * L_45 = ___col0;
		NullCheck(L_45);
		int32_t L_46 = DataColumn_get_MaxLength_m3416668844(L_45, /*hidden argument*/NULL);
		if ((((int32_t)L_46) >= ((int32_t)0)))
		{
			goto IL_01b0;
		}
	}
	{
		XmlWriter_t89522450 * L_47 = __this->get_w_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_47);
		XmlWriter_WriteStartAttribute_m904857238(L_47, _stringLiteral3575610, L_48, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_49 = ___col0;
		NullCheck(L_49);
		Type_t * L_50 = DataColumn_get_DataType_m3376662490(L_49, /*hidden argument*/NULL);
		XmlQualifiedName_t176365656 * L_51 = XmlSchemaWriter_MapType_m2141526126(__this, L_50, /*hidden argument*/NULL);
		V_0 = L_51;
		XmlQualifiedName_t176365656 * L_52 = V_0;
		XmlSchemaWriter_WriteQName_m2811822933(__this, L_52, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_53 = __this->get_w_0();
		NullCheck(L_53);
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, L_53);
	}

IL_01b0:
	{
		XmlQualifiedName_t176365656 * L_54 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_55 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnString_0();
		IL2CPP_RUNTIME_CLASS_INIT(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		bool L_56 = XmlQualifiedName_op_Equality_m273752697(NULL /*static, unused*/, L_54, L_55, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_0227;
		}
	}
	{
		DataColumn_t3354469747 * L_57 = ___col0;
		NullCheck(L_57);
		Type_t * L_58 = DataColumn_get_DataType_m3376662490(L_57, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_59 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_58) == ((Il2CppObject*)(Type_t *)L_59)))
		{
			goto IL_0227;
		}
	}
	{
		DataColumn_t3354469747 * L_60 = ___col0;
		NullCheck(L_60);
		Type_t * L_61 = DataColumn_get_DataType_m3376662490(L_60, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_62 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Char_t2778706699_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_61) == ((Il2CppObject*)(Type_t *)L_62)))
		{
			goto IL_0227;
		}
	}
	{
		XmlWriter_t89522450 * L_63 = __this->get_w_0();
		NullCheck(L_63);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(23 /* System.Void System.Xml.XmlWriter::WriteStartAttribute(System.String,System.String,System.String) */, L_63, _stringLiteral3229858384, _stringLiteral1853714980, _stringLiteral3051173506);
		DataColumn_t3354469747 * L_64 = ___col0;
		NullCheck(L_64);
		Type_t * L_65 = DataColumn_get_DataType_m3376662490(L_64, /*hidden argument*/NULL);
		NullCheck(L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_65);
		V_1 = L_66;
		XmlWriter_t89522450 * L_67 = __this->get_w_0();
		String_t* L_68 = V_1;
		NullCheck(L_67);
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, L_67, L_68);
		XmlWriter_t89522450 * L_69 = __this->get_w_0();
		NullCheck(L_69);
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, L_69);
	}

IL_0227:
	{
		DataColumn_t3354469747 * L_70 = ___col0;
		NullCheck(L_70);
		bool L_71 = DataColumn_get_AllowDBNull_m4042991754(L_70, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_0247;
		}
	}
	{
		XmlWriter_t89522450 * L_72 = __this->get_w_0();
		NullCheck(L_72);
		XmlWriter_WriteAttributeString_m1069599196(L_72, _stringLiteral64804601, _stringLiteral48, /*hidden argument*/NULL);
	}

IL_0247:
	{
		DataColumn_t3354469747 * L_73 = ___col0;
		NullCheck(L_73);
		PropertyCollection_t3599376422 * L_74 = DataColumn_get_ExtendedProperties_m3441609827(L_73, /*hidden argument*/NULL);
		XmlSchemaWriter_AddExtendedPropertyAttributes_m1280916214(__this, L_74, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_75 = ___col0;
		NullCheck(L_75);
		int32_t L_76 = DataColumn_get_MaxLength_m3416668844(L_75, /*hidden argument*/NULL);
		if ((((int32_t)L_76) <= ((int32_t)(-1))))
		{
			goto IL_0266;
		}
	}
	{
		DataColumn_t3354469747 * L_77 = ___col0;
		XmlSchemaWriter_WriteSimpleType_m3522381358(__this, L_77, /*hidden argument*/NULL);
	}

IL_0266:
	{
		XmlWriter_t89522450 * L_78 = __this->get_w_0();
		NullCheck(L_78);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_78);
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::WriteChildRelations(System.Data.DataRelation)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3835;
extern Il2CppCodeGenString* _stringLiteral2632130300;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern Il2CppCodeGenString* _stringLiteral112787;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral64804601;
extern Il2CppCodeGenString* _stringLiteral48;
extern Il2CppCodeGenString* _stringLiteral3587293323;
extern Il2CppCodeGenString* _stringLiteral1782112740;
extern const uint32_t XmlSchemaWriter_WriteChildRelations_m841801207_MetadataUsageId;
extern "C"  void XmlSchemaWriter_WriteChildRelations_m841801207 (XmlSchemaWriter_t2141469886 * __this, DataRelation_t1483987353 * ___rel0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_WriteChildRelations_m841801207_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DataRelation_t1483987353 * L_0 = ___rel0;
		NullCheck(L_0);
		DataTable_t2176726999 * L_1 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(6 /* System.Data.DataTable System.Data.DataRelation::get_ChildTable() */, L_0);
		NullCheck(L_1);
		String_t* L_2 = DataTable_get_Namespace_m3829929060(L_1, /*hidden argument*/NULL);
		String_t* L_3 = __this->get_dataSetNamespace_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		DataTableU5BU5D_t1761989358* L_5 = __this->get_tables_1();
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) >= ((int32_t)2)))
		{
			goto IL_008e;
		}
	}

IL_0029:
	{
		XmlWriter_t89522450 * L_6 = __this->get_w_0();
		NullCheck(L_6);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_6, _stringLiteral3835, _stringLiteral2632130300, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_7 = __this->get_w_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_7);
		XmlWriter_WriteStartAttribute_m904857238(L_7, _stringLiteral112787, L_8, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_9 = __this->get_w_0();
		DataRelation_t1483987353 * L_10 = ___rel0;
		NullCheck(L_10);
		DataTable_t2176726999 * L_11 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(6 /* System.Data.DataTable System.Data.DataRelation::get_ChildTable() */, L_10);
		NullCheck(L_11);
		String_t* L_12 = DataTable_get_TableName_m3141812994(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_13 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		DataRelation_t1483987353 * L_14 = ___rel0;
		NullCheck(L_14);
		DataTable_t2176726999 * L_15 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(6 /* System.Data.DataTable System.Data.DataRelation::get_ChildTable() */, L_14);
		NullCheck(L_15);
		String_t* L_16 = DataTable_get_Namespace_m3829929060(L_15, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(20 /* System.Void System.Xml.XmlWriter::WriteQualifiedName(System.String,System.String) */, L_9, L_13, L_16);
		XmlWriter_t89522450 * L_17 = __this->get_w_0();
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, L_17);
		goto IL_012a;
	}

IL_008e:
	{
		XmlWriter_t89522450 * L_18 = __this->get_w_0();
		NullCheck(L_18);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_18, _stringLiteral3835, _stringLiteral2632130300, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_19 = __this->get_w_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_19);
		XmlWriter_WriteStartAttribute_m904857238(L_19, _stringLiteral3373707, L_20, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_21 = __this->get_w_0();
		DataRelation_t1483987353 * L_22 = ___rel0;
		NullCheck(L_22);
		DataTable_t2176726999 * L_23 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(6 /* System.Data.DataTable System.Data.DataRelation::get_ChildTable() */, L_22);
		NullCheck(L_23);
		String_t* L_24 = DataTable_get_TableName_m3141812994(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		String_t* L_25 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		DataRelation_t1483987353 * L_26 = ___rel0;
		NullCheck(L_26);
		DataTable_t2176726999 * L_27 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(6 /* System.Data.DataTable System.Data.DataRelation::get_ChildTable() */, L_26);
		NullCheck(L_27);
		String_t* L_28 = DataTable_get_Namespace_m3829929060(L_27, /*hidden argument*/NULL);
		NullCheck(L_21);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(20 /* System.Void System.Xml.XmlWriter::WriteQualifiedName(System.String,System.String) */, L_21, L_25, L_28);
		XmlWriter_t89522450 * L_29 = __this->get_w_0();
		NullCheck(L_29);
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, L_29);
		XmlWriter_t89522450 * L_30 = __this->get_w_0();
		NullCheck(L_30);
		XmlWriter_WriteAttributeString_m1069599196(L_30, _stringLiteral64804601, _stringLiteral48, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_31 = __this->get_w_0();
		NullCheck(L_31);
		XmlWriter_WriteAttributeString_m1069599196(L_31, _stringLiteral3587293323, _stringLiteral1782112740, /*hidden argument*/NULL);
		ArrayList_t2121638921 * L_32 = __this->get_globalTypeTables_8();
		DataRelation_t1483987353 * L_33 = ___rel0;
		NullCheck(L_33);
		DataTable_t2176726999 * L_34 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(6 /* System.Data.DataTable System.Data.DataRelation::get_ChildTable() */, L_33);
		NullCheck(L_32);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_32, L_34);
	}

IL_012a:
	{
		DataTableU5BU5D_t1761989358* L_35 = __this->get_tables_1();
		NullCheck(L_35);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length))))) <= ((int32_t)1)))
		{
			goto IL_0144;
		}
	}
	{
		DataRelation_t1483987353 * L_36 = ___rel0;
		NullCheck(L_36);
		DataTable_t2176726999 * L_37 = VirtFuncInvoker0< DataTable_t2176726999 * >::Invoke(6 /* System.Data.DataTable System.Data.DataRelation::get_ChildTable() */, L_36);
		XmlSchemaWriter_WriteTableType_m163055952(__this, L_37, /*hidden argument*/NULL);
	}

IL_0144:
	{
		XmlWriter_t89522450 * L_38 = __this->get_w_0();
		NullCheck(L_38);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_38);
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::WriteTableAttributes(System.Collections.ArrayList)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* DataColumn_t3354469747_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3835;
extern Il2CppCodeGenString* _stringLiteral13085340;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern Il2CppCodeGenString* _stringLiteral3148996;
extern Il2CppCodeGenString* _stringLiteral3047026844;
extern Il2CppCodeGenString* _stringLiteral96801;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3229858384;
extern Il2CppCodeGenString* _stringLiteral3491927682;
extern Il2CppCodeGenString* _stringLiteral3051173506;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral3575610;
extern Il2CppCodeGenString* _stringLiteral116103;
extern Il2CppCodeGenString* _stringLiteral3901827999;
extern Il2CppCodeGenString* _stringLiteral1544803905;
extern const uint32_t XmlSchemaWriter_WriteTableAttributes_m1436721665_MetadataUsageId;
extern "C"  void XmlSchemaWriter_WriteTableAttributes_m1436721665 (XmlSchemaWriter_t2141469886 * __this, ArrayList_t2121638921 * ___atts0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_WriteTableAttributes_m1436721665_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	DataColumn_t3354469747 * V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B6_0 = NULL;
	{
		ArrayList_t2121638921 * L_0 = ___atts0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_0);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01b6;
		}

IL_000c:
		{
			Il2CppObject * L_2 = V_0;
			NullCheck(L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_2);
			V_1 = ((DataColumn_t3354469747 *)CastclassClass(L_3, DataColumn_t3354469747_il2cpp_TypeInfo_var));
			XmlWriter_t89522450 * L_4 = __this->get_w_0();
			NullCheck(L_4);
			VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_4, _stringLiteral3835, _stringLiteral13085340, _stringLiteral1440052060);
			DataColumn_t3354469747 * L_5 = V_1;
			NullCheck(L_5);
			String_t* L_6 = DataColumn_get_ColumnName_m409531680(L_5, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
			String_t* L_7 = XmlHelper_Encode_m2974847862(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			DataColumn_t3354469747 * L_8 = V_1;
			NullCheck(L_8);
			String_t* L_9 = DataColumn_get_Namespace_m546686590(L_8, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
			bool L_11 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
			if (!L_11)
			{
				goto IL_00c2;
			}
		}

IL_0053:
		{
			XmlWriter_t89522450 * L_12 = __this->get_w_0();
			NullCheck(L_12);
			XmlWriter_WriteAttributeString_m1069599196(L_12, _stringLiteral3148996, _stringLiteral3047026844, /*hidden argument*/NULL);
			DataColumn_t3354469747 * L_13 = V_1;
			NullCheck(L_13);
			String_t* L_14 = DataColumn_get_Prefix_m1148206961(L_13, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_15 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
			bool L_16 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
			if (!L_16)
			{
				goto IL_009c;
			}
		}

IL_007d:
		{
			Hashtable_t3875263730 * L_17 = __this->get_additionalNamespaces_9();
			NullCheck(L_17);
			int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_17);
			int32_t L_19 = L_18;
			Il2CppObject * L_20 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_19);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_21 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral96801, L_20, /*hidden argument*/NULL);
			G_B6_0 = L_21;
			goto IL_00a2;
		}

IL_009c:
		{
			DataColumn_t3354469747 * L_22 = V_1;
			NullCheck(L_22);
			String_t* L_23 = DataColumn_get_Prefix_m1148206961(L_22, /*hidden argument*/NULL);
			G_B6_0 = L_23;
		}

IL_00a2:
		{
			V_3 = G_B6_0;
			String_t* L_24 = V_3;
			String_t* L_25 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_26 = String_Concat_m1825781833(NULL /*static, unused*/, L_24, _stringLiteral58, L_25, /*hidden argument*/NULL);
			V_2 = L_26;
			Hashtable_t3875263730 * L_27 = __this->get_additionalNamespaces_9();
			String_t* L_28 = V_3;
			DataColumn_t3354469747 * L_29 = V_1;
			NullCheck(L_29);
			String_t* L_30 = DataColumn_get_Namespace_m546686590(L_29, /*hidden argument*/NULL);
			NullCheck(L_27);
			VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(27 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_27, L_28, L_30);
		}

IL_00c2:
		{
			XmlWriter_t89522450 * L_31 = __this->get_w_0();
			String_t* L_32 = V_2;
			NullCheck(L_31);
			XmlWriter_WriteAttributeString_m1069599196(L_31, _stringLiteral3373707, L_32, /*hidden argument*/NULL);
			DataColumn_t3354469747 * L_33 = V_1;
			NullCheck(L_33);
			PropertyCollection_t3599376422 * L_34 = DataColumn_get_ExtendedProperties_m3441609827(L_33, /*hidden argument*/NULL);
			XmlSchemaWriter_AddExtendedPropertyAttributes_m1280916214(__this, L_34, /*hidden argument*/NULL);
			DataColumn_t3354469747 * L_35 = V_1;
			NullCheck(L_35);
			bool L_36 = DataColumn_get_ReadOnly_m3035570536(L_35, /*hidden argument*/NULL);
			if (!L_36)
			{
				goto IL_0109;
			}
		}

IL_00ea:
		{
			XmlWriter_t89522450 * L_37 = __this->get_w_0();
			NullCheck(L_37);
			XmlWriter_WriteAttributeString_m1630990645(L_37, _stringLiteral3229858384, _stringLiteral3491927682, _stringLiteral3051173506, _stringLiteral3569038, /*hidden argument*/NULL);
		}

IL_0109:
		{
			DataColumn_t3354469747 * L_38 = V_1;
			NullCheck(L_38);
			int32_t L_39 = DataColumn_get_MaxLength_m3416668844(L_38, /*hidden argument*/NULL);
			if ((((int32_t)L_39) >= ((int32_t)0)))
			{
				goto IL_0147;
			}
		}

IL_0115:
		{
			XmlWriter_t89522450 * L_40 = __this->get_w_0();
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_41 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
			NullCheck(L_40);
			XmlWriter_WriteStartAttribute_m904857238(L_40, _stringLiteral3575610, L_41, /*hidden argument*/NULL);
			DataColumn_t3354469747 * L_42 = V_1;
			NullCheck(L_42);
			Type_t * L_43 = DataColumn_get_DataType_m3376662490(L_42, /*hidden argument*/NULL);
			XmlQualifiedName_t176365656 * L_44 = XmlSchemaWriter_MapType_m2141526126(__this, L_43, /*hidden argument*/NULL);
			XmlSchemaWriter_WriteQName_m2811822933(__this, L_44, /*hidden argument*/NULL);
			XmlWriter_t89522450 * L_45 = __this->get_w_0();
			NullCheck(L_45);
			VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, L_45);
		}

IL_0147:
		{
			DataColumn_t3354469747 * L_46 = V_1;
			NullCheck(L_46);
			bool L_47 = DataColumn_get_AllowDBNull_m4042991754(L_46, /*hidden argument*/NULL);
			if (L_47)
			{
				goto IL_0167;
			}
		}

IL_0152:
		{
			XmlWriter_t89522450 * L_48 = __this->get_w_0();
			NullCheck(L_48);
			XmlWriter_WriteAttributeString_m1069599196(L_48, _stringLiteral116103, _stringLiteral3901827999, /*hidden argument*/NULL);
		}

IL_0167:
		{
			DataColumn_t3354469747 * L_49 = V_1;
			NullCheck(L_49);
			Il2CppObject * L_50 = DataColumn_get_DefaultValue_m2400376481(L_49, /*hidden argument*/NULL);
			DataColumn_t3354469747 * L_51 = V_1;
			NullCheck(L_51);
			Type_t * L_52 = DataColumn_get_DataType_m3376662490(L_51, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(DataColumn_t3354469747_il2cpp_TypeInfo_var);
			Il2CppObject * L_53 = DataColumn_GetDefaultValueForType_m983945556(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
			if ((((Il2CppObject*)(Il2CppObject *)L_50) == ((Il2CppObject*)(Il2CppObject *)L_53)))
			{
				goto IL_0198;
			}
		}

IL_017d:
		{
			XmlWriter_t89522450 * L_54 = __this->get_w_0();
			DataColumn_t3354469747 * L_55 = V_1;
			NullCheck(L_55);
			Il2CppObject * L_56 = DataColumn_get_DefaultValue_m2400376481(L_55, /*hidden argument*/NULL);
			String_t* L_57 = DataSet_WriteObjectXml_m942680311(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
			NullCheck(L_54);
			XmlWriter_WriteAttributeString_m1069599196(L_54, _stringLiteral1544803905, L_57, /*hidden argument*/NULL);
		}

IL_0198:
		{
			DataColumn_t3354469747 * L_58 = V_1;
			NullCheck(L_58);
			int32_t L_59 = DataColumn_get_MaxLength_m3416668844(L_58, /*hidden argument*/NULL);
			if ((((int32_t)L_59) <= ((int32_t)(-1))))
			{
				goto IL_01ab;
			}
		}

IL_01a4:
		{
			DataColumn_t3354469747 * L_60 = V_1;
			XmlSchemaWriter_WriteSimpleType_m3522381358(__this, L_60, /*hidden argument*/NULL);
		}

IL_01ab:
		{
			XmlWriter_t89522450 * L_61 = __this->get_w_0();
			NullCheck(L_61);
			VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_61);
		}

IL_01b6:
		{
			Il2CppObject * L_62 = V_0;
			NullCheck(L_62);
			bool L_63 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_62);
			if (L_63)
			{
				goto IL_000c;
			}
		}

IL_01c1:
		{
			IL2CPP_LEAVE(0x1DC, FINALLY_01c6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01c6;
	}

FINALLY_01c6:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_64 = V_0;
			Il2CppObject * L_65 = ((Il2CppObject *)IsInst(L_64, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_4 = L_65;
			if (!L_65)
			{
				goto IL_01db;
			}
		}

IL_01d4:
		{
			Il2CppObject * L_66 = V_4;
			NullCheck(L_66);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_66);
		}

IL_01db:
		{
			IL2CPP_END_FINALLY(454)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(454)
	{
		IL2CPP_JUMP_TBL(0x1DC, IL_01dc)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01dc:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::WriteSimpleType(System.Data.DataColumn)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlConvert_t1882388356_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3835;
extern Il2CppCodeGenString* _stringLiteral2863401996;
extern Il2CppCodeGenString* _stringLiteral1440052060;
extern Il2CppCodeGenString* _stringLiteral2733904844;
extern Il2CppCodeGenString* _stringLiteral3016401;
extern Il2CppCodeGenString* _stringLiteral3503567210;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t XmlSchemaWriter_WriteSimpleType_m3522381358_MetadataUsageId;
extern "C"  void XmlSchemaWriter_WriteSimpleType_m3522381358 (XmlSchemaWriter_t2141469886 * __this, DataColumn_t3354469747 * ___col0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_WriteSimpleType_m3522381358_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlWriter_t89522450 * L_0 = __this->get_w_0();
		NullCheck(L_0);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_0, _stringLiteral3835, _stringLiteral2863401996, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_1 = __this->get_w_0();
		NullCheck(L_1);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_1, _stringLiteral3835, _stringLiteral2733904844, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_2 = __this->get_w_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_2);
		XmlWriter_WriteStartAttribute_m904857238(L_2, _stringLiteral3016401, L_3, /*hidden argument*/NULL);
		DataColumn_t3354469747 * L_4 = ___col0;
		NullCheck(L_4);
		Type_t * L_5 = DataColumn_get_DataType_m3376662490(L_4, /*hidden argument*/NULL);
		XmlQualifiedName_t176365656 * L_6 = XmlSchemaWriter_MapType_m2141526126(__this, L_5, /*hidden argument*/NULL);
		XmlSchemaWriter_WriteQName_m2811822933(__this, L_6, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_7 = __this->get_w_0();
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, L_7);
		XmlWriter_t89522450 * L_8 = __this->get_w_0();
		NullCheck(L_8);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, L_8, _stringLiteral3835, _stringLiteral3503567210, _stringLiteral1440052060);
		XmlWriter_t89522450 * L_9 = __this->get_w_0();
		DataColumn_t3354469747 * L_10 = ___col0;
		NullCheck(L_10);
		int32_t L_11 = DataColumn_get_MaxLength_m3416668844(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		String_t* L_12 = XmlConvert_ToString_m1296446159(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		XmlWriter_WriteAttributeString_m1069599196(L_9, _stringLiteral111972721, L_12, /*hidden argument*/NULL);
		XmlWriter_t89522450 * L_13 = __this->get_w_0();
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_13);
		XmlWriter_t89522450 * L_14 = __this->get_w_0();
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_14);
		XmlWriter_t89522450 * L_15 = __this->get_w_0();
		NullCheck(L_15);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_15);
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::WriteQName(System.Xml.XmlQualifiedName)
extern "C"  void XmlSchemaWriter_WriteQName_m2811822933 (XmlSchemaWriter_t2141469886 * __this, XmlQualifiedName_t176365656 * ___name0, const MethodInfo* method)
{
	{
		XmlWriter_t89522450 * L_0 = __this->get_w_0();
		XmlQualifiedName_t176365656 * L_1 = ___name0;
		NullCheck(L_1);
		String_t* L_2 = XmlQualifiedName_get_Name_m607016698(L_1, /*hidden argument*/NULL);
		XmlQualifiedName_t176365656 * L_3 = ___name0;
		NullCheck(L_3);
		String_t* L_4 = XmlQualifiedName_get_Namespace_m2987642414(L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(20 /* System.Void System.Xml.XmlWriter::WriteQualifiedName(System.String,System.String) */, L_0, L_2, L_4);
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::CheckNamespace(System.String,System.String,System.Collections.Specialized.ListDictionary,System.Collections.Specialized.ListDictionary)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96801;
extern const uint32_t XmlSchemaWriter_CheckNamespace_m566926279_MetadataUsageId;
extern "C"  void XmlSchemaWriter_CheckNamespace_m566926279 (XmlSchemaWriter_t2141469886 * __this, String_t* ___prefix0, String_t* ___ns1, ListDictionary_t4226329727 * ___names2, ListDictionary_t4226329727 * ___includes3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_CheckNamespace_m566926279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = ___ns1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_2 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		String_t* L_3 = __this->get_dataSetNamespace_5();
		String_t* L_4 = ___ns1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0083;
		}
	}
	{
		ListDictionary_t4226329727 * L_6 = ___names2;
		String_t* L_7 = ___prefix0;
		NullCheck(L_6);
		Il2CppObject * L_8 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(4 /* System.Object System.Collections.Specialized.ListDictionary::get_Item(System.Object) */, L_6, L_7);
		String_t* L_9 = ___ns1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Inequality_m2125462205(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_8, String_t_il2cpp_TypeInfo_var)), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0083;
		}
	}
	{
		V_0 = 1;
		goto IL_0078;
	}

IL_0040:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral96801, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		ListDictionary_t4226329727 * L_15 = ___names2;
		String_t* L_16 = V_1;
		NullCheck(L_15);
		Il2CppObject * L_17 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(4 /* System.Object System.Collections.Specialized.ListDictionary::get_Item(System.Object) */, L_15, L_16);
		if (L_17)
		{
			goto IL_0074;
		}
	}
	{
		ListDictionary_t4226329727 * L_18 = ___names2;
		String_t* L_19 = V_1;
		String_t* L_20 = ___ns1;
		NullCheck(L_18);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(8 /* System.Void System.Collections.Specialized.ListDictionary::Add(System.Object,System.Object) */, L_18, L_19, L_20);
		String_t* L_21 = V_1;
		String_t* L_22 = ___ns1;
		ListDictionary_t4226329727 * L_23 = ___includes3;
		XmlSchemaWriter_HandleExternalNamespace_m456176137(__this, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_0083;
	}

IL_0074:
	{
		int32_t L_24 = V_0;
		V_0 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_25 = V_0;
		if ((((int32_t)L_25) < ((int32_t)((int32_t)2147483647LL))))
		{
			goto IL_0040;
		}
	}

IL_0083:
	{
		return;
	}
}
// System.Void System.Data.XmlSchemaWriter::HandleExternalNamespace(System.String,System.String,System.Collections.Specialized.ListDictionary)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral95;
extern Il2CppCodeGenString* _stringLiteral1489371;
extern const uint32_t XmlSchemaWriter_HandleExternalNamespace_m456176137_MetadataUsageId;
extern "C"  void XmlSchemaWriter_HandleExternalNamespace_m456176137 (XmlSchemaWriter_t2141469886 * __this, String_t* ___prefix0, String_t* ___ns1, ListDictionary_t4226329727 * ___includes2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_HandleExternalNamespace_m456176137_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ListDictionary_t4226329727 * L_0 = ___includes2;
		String_t* L_1 = ___ns1;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(10 /* System.Boolean System.Collections.Specialized.ListDictionary::Contains(System.Object) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		ListDictionary_t4226329727 * L_3 = ___includes2;
		String_t* L_4 = ___ns1;
		String_t* L_5 = ___prefix0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral95, L_5, _stringLiteral1489371, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(8 /* System.Void System.Collections.Specialized.ListDictionary::Add(System.Object,System.Object) */, L_3, L_4, L_6);
		return;
	}
}
// System.Xml.XmlQualifiedName System.Data.XmlSchemaWriter::MapType(System.Type)
extern const Il2CppType* TimeSpan_t763862892_0_0_0_var;
extern const Il2CppType* Uri_t2776692961_0_0_0_var;
extern const Il2CppType* ByteU5BU5D_t58506160_0_0_0_var;
extern const Il2CppType* XmlQualifiedName_t176365656_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlConstants_t2555109579_il2cpp_TypeInfo_var;
extern const uint32_t XmlSchemaWriter_MapType_m2141526126_MetadataUsageId;
extern "C"  XmlQualifiedName_t176365656 * XmlSchemaWriter_MapType_m2141526126 (XmlSchemaWriter_t2141469886 * __this, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlSchemaWriter_MapType_m2141526126_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_1 = Type_GetTypeCode_m2969996822(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 0)
		{
			goto IL_006c;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 1)
		{
			goto IL_00a8;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 2)
		{
			goto IL_008a;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 3)
		{
			goto IL_0072;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 4)
		{
			goto IL_005a;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 5)
		{
			goto IL_0096;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 6)
		{
			goto IL_0060;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 7)
		{
			goto IL_009c;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 8)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 9)
		{
			goto IL_00a2;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 10)
		{
			goto IL_0090;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 11)
		{
			goto IL_0084;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 12)
		{
			goto IL_007e;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 13)
		{
			goto IL_0078;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 14)
		{
			goto IL_00a8;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)3)) == 15)
		{
			goto IL_0054;
		}
	}
	{
		goto IL_00a8;
	}

IL_0054:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_3 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnString_0();
		return L_3;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_4 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnShort_1();
		return L_4;
	}

IL_0060:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_5 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnInt_2();
		return L_5;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_6 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnLong_3();
		return L_6;
	}

IL_006c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_7 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnBoolean_4();
		return L_7;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_8 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnUnsignedByte_5();
		return L_8;
	}

IL_0078:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_9 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnDateTime_7();
		return L_9;
	}

IL_007e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_10 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnDecimal_8();
		return L_10;
	}

IL_0084:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_11 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnDouble_9();
		return L_11;
	}

IL_008a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_12 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnSbyte_10();
		return L_12;
	}

IL_0090:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_13 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnFloat_11();
		return L_13;
	}

IL_0096:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_14 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnUnsignedShort_13();
		return L_14;
	}

IL_009c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_15 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnUnsignedInt_14();
		return L_15;
	}

IL_00a2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_16 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnUnsignedLong_15();
		return L_16;
	}

IL_00a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(TimeSpan_t763862892_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_18 = ___type0;
		if ((!(((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18))))
		{
			goto IL_00be;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_19 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnDuration_12();
		return L_19;
	}

IL_00be:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Uri_t2776692961_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_21 = ___type0;
		if ((!(((Il2CppObject*)(Type_t *)L_20) == ((Il2CppObject*)(Type_t *)L_21))))
		{
			goto IL_00d4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_22 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnUri_16();
		return L_22;
	}

IL_00d4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_23 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ByteU5BU5D_t58506160_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_24 = ___type0;
		if ((!(((Il2CppObject*)(Type_t *)L_23) == ((Il2CppObject*)(Type_t *)L_24))))
		{
			goto IL_00ea;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_25 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnBase64Binary_17();
		return L_25;
	}

IL_00ea:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(XmlQualifiedName_t176365656_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_27 = ___type0;
		if ((!(((Il2CppObject*)(Type_t *)L_26) == ((Il2CppObject*)(Type_t *)L_27))))
		{
			goto IL_0100;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_28 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnXmlQualifiedName_18();
		return L_28;
	}

IL_0100:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlConstants_t2555109579_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_29 = ((XmlConstants_t2555109579_StaticFields*)XmlConstants_t2555109579_il2cpp_TypeInfo_var->static_fields)->get_QnString_0();
		return L_29;
	}
}
// System.Void System.MonoTODOAttribute::.ctor()
extern "C"  void MonoTODOAttribute__ctor_m3959270498 (MonoTODOAttribute_t1287393901 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String XmlHelper::Decode(System.String)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlConvert_t1882388356_il2cpp_TypeInfo_var;
extern const uint32_t XmlHelper_Decode_m2147947678_MetadataUsageId;
extern "C"  String_t* XmlHelper_Decode_m2147947678 (Il2CppObject * __this /* static, unused */, String_t* ___xmlName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlHelper_Decode_m2147947678_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		Hashtable_t3875263730 * L_0 = ((XmlHelper_t69615237_StaticFields*)XmlHelper_t69615237_il2cpp_TypeInfo_var->static_fields)->get_localSchemaNameCache_0();
		String_t* L_1 = ___xmlName0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, L_1);
		V_0 = ((String_t*)CastclassSealed(L_2, String_t_il2cpp_TypeInfo_var));
		String_t* L_3 = V_0;
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_4 = ___xmlName0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		String_t* L_5 = XmlConvert_DecodeName_m3628614327(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		Hashtable_t3875263730 * L_6 = ((XmlHelper_t69615237_StaticFields*)XmlHelper_t69615237_il2cpp_TypeInfo_var->static_fields)->get_localSchemaNameCache_0();
		String_t* L_7 = ___xmlName0;
		String_t* L_8 = V_0;
		NullCheck(L_6);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(27 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_6, L_7, L_8);
	}

IL_002a:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
// System.String XmlHelper::Encode(System.String)
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlConvert_t1882388356_il2cpp_TypeInfo_var;
extern const uint32_t XmlHelper_Encode_m2974847862_MetadataUsageId;
extern "C"  String_t* XmlHelper_Encode_m2974847862 (Il2CppObject * __this /* static, unused */, String_t* ___schemaName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlHelper_Encode_m2974847862_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		Hashtable_t3875263730 * L_0 = ((XmlHelper_t69615237_StaticFields*)XmlHelper_t69615237_il2cpp_TypeInfo_var->static_fields)->get_localXmlNameCache_1();
		String_t* L_1 = ___schemaName0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, L_1);
		V_0 = ((String_t*)CastclassSealed(L_2, String_t_il2cpp_TypeInfo_var));
		String_t* L_3 = V_0;
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_4 = ___schemaName0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		String_t* L_5 = XmlConvert_EncodeLocalName_m3492384786(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(XmlHelper_t69615237_il2cpp_TypeInfo_var);
		Hashtable_t3875263730 * L_6 = ((XmlHelper_t69615237_StaticFields*)XmlHelper_t69615237_il2cpp_TypeInfo_var->static_fields)->get_localXmlNameCache_1();
		String_t* L_7 = ___schemaName0;
		String_t* L_8 = V_0;
		NullCheck(L_6);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(27 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_6, L_7, L_8);
	}

IL_002a:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
// System.Void XmlHelper::.cctor()
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlHelper_t69615237_il2cpp_TypeInfo_var;
extern const uint32_t XmlHelper__cctor_m2474544994_MetadataUsageId;
extern "C"  void XmlHelper__cctor_m2474544994 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlHelper__cctor_m2474544994_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t3875263730 * L_0 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		((XmlHelper_t69615237_StaticFields*)XmlHelper_t69615237_il2cpp_TypeInfo_var->static_fields)->set_localSchemaNameCache_0(L_0);
		Hashtable_t3875263730 * L_1 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_1, /*hidden argument*/NULL);
		((XmlHelper_t69615237_StaticFields*)XmlHelper_t69615237_il2cpp_TypeInfo_var->static_fields)->set_localXmlNameCache_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
