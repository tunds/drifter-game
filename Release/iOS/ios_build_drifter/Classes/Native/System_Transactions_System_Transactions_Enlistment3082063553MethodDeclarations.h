﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Transactions.Enlistment
struct Enlistment_t3082063553;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Transactions.Enlistment::.ctor()
extern "C"  void Enlistment__ctor_m1854573282 (Enlistment_t3082063553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Enlistment::Done()
extern "C"  void Enlistment_Done_m423463204 (Enlistment_t3082063553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
