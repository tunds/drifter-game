﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MenuManager
struct MenuManager_t3994435886;

#include "codegen/il2cpp-codegen.h"

// System.Void MenuManager::.ctor()
extern "C"  void MenuManager__ctor_m1757457917 (MenuManager_t3994435886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuManager::Awake()
extern "C"  void MenuManager_Awake_m1995063136 (MenuManager_t3994435886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuManager::Update()
extern "C"  void MenuManager_Update_m373482672 (MenuManager_t3994435886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuManager::goGame()
extern "C"  void MenuManager_goGame_m4256307329 (MenuManager_t3994435886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuManager::goSettings()
extern "C"  void MenuManager_goSettings_m1443924338 (MenuManager_t3994435886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuManager::goLeaderboards()
extern "C"  void MenuManager_goLeaderboards_m1539624485 (MenuManager_t3994435886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuManager::goInstructions()
extern "C"  void MenuManager_goInstructions_m2482561908 (MenuManager_t3994435886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuManager::goHome()
extern "C"  void MenuManager_goHome_m2898478 (MenuManager_t3994435886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
