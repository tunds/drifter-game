﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Data_Mono_Data_SqlExpressions_BinaryOpExpre3970441148.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.BoolOperation
struct  BoolOperation_t2628794143  : public BinaryOpExpression_t3970441148
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
