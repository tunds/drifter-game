﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.FillErrorEventHandler
struct FillErrorEventHandler_t141101000;
// System.Object
struct Il2CppObject;
// System.Data.FillErrorEventArgs
struct FillErrorEventArgs_t1571131301;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_Data_System_Data_FillErrorEventArgs1571131301.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Data.FillErrorEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void FillErrorEventHandler__ctor_m3207749762 (FillErrorEventHandler_t141101000 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.FillErrorEventHandler::Invoke(System.Object,System.Data.FillErrorEventArgs)
extern "C"  void FillErrorEventHandler_Invoke_m251169783 (FillErrorEventHandler_t141101000 * __this, Il2CppObject * ___sender0, FillErrorEventArgs_t1571131301 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_FillErrorEventHandler_t141101000(Il2CppObject* delegate, Il2CppObject * ___sender0, FillErrorEventArgs_t1571131301 * ___e1);
// System.IAsyncResult System.Data.FillErrorEventHandler::BeginInvoke(System.Object,System.Data.FillErrorEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FillErrorEventHandler_BeginInvoke_m1334248468 (FillErrorEventHandler_t141101000 * __this, Il2CppObject * ___sender0, FillErrorEventArgs_t1571131301 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.FillErrorEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void FillErrorEventHandler_EndInvoke_m3778962322 (FillErrorEventHandler_t141101000 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
