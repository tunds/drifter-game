﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PoqXert.MessageBox.MSGBoxStyle
struct MSGBoxStyle_t1923386386;
// UnityEngine.Sprite
struct Sprite_t4006040370;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_Sprite4006040370.h"

// System.Void PoqXert.MessageBox.MSGBoxStyle::.ctor()
extern "C"  void MSGBoxStyle__ctor_m1731184625 (MSGBoxStyle_t1923386386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::get_captionColor()
extern "C"  Color_t1588175760  MSGBoxStyle_get_captionColor_m334910080 (MSGBoxStyle_t1923386386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MSGBoxStyle::set_captionColor(UnityEngine.Color)
extern "C"  void MSGBoxStyle_set_captionColor_m234508889 (MSGBoxStyle_t1923386386 * __this, Color_t1588175760  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::get_backgroundColor()
extern "C"  Color_t1588175760  MSGBoxStyle_get_backgroundColor_m3445433268 (MSGBoxStyle_t1923386386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MSGBoxStyle::set_backgroundColor(UnityEngine.Color)
extern "C"  void MSGBoxStyle_set_backgroundColor_m2284919135 (MSGBoxStyle_t1923386386 * __this, Color_t1588175760  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::get_btnYesColor()
extern "C"  Color_t1588175760  MSGBoxStyle_get_btnYesColor_m1144113751 (MSGBoxStyle_t1923386386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MSGBoxStyle::set_btnYesColor(UnityEngine.Color)
extern "C"  void MSGBoxStyle_set_btnYesColor_m4268583836 (MSGBoxStyle_t1923386386 * __this, Color_t1588175760  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::get_btnNoColor()
extern "C"  Color_t1588175760  MSGBoxStyle_get_btnNoColor_m437349321 (MSGBoxStyle_t1923386386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MSGBoxStyle::set_btnNoColor(UnityEngine.Color)
extern "C"  void MSGBoxStyle_set_btnNoColor_m3380012272 (MSGBoxStyle_t1923386386 * __this, Color_t1588175760  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::get_btnCancelColor()
extern "C"  Color_t1588175760  MSGBoxStyle_get_btnCancelColor_m4250514928 (MSGBoxStyle_t1923386386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MSGBoxStyle::set_btnCancelColor(UnityEngine.Color)
extern "C"  void MSGBoxStyle_set_btnCancelColor_m62151721 (MSGBoxStyle_t1923386386 * __this, Color_t1588175760  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite PoqXert.MessageBox.MSGBoxStyle::get_icon()
extern "C"  Sprite_t4006040370 * MSGBoxStyle_get_icon_m2425158486 (MSGBoxStyle_t1923386386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MSGBoxStyle::set_icon(UnityEngine.Sprite)
extern "C"  void MSGBoxStyle_set_icon_m2791749517 (MSGBoxStyle_t1923386386 * __this, Sprite_t4006040370 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
