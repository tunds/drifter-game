﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.String XmlHelper::Decode(System.String)
extern "C"  String_t* XmlHelper_Decode_m2147947678 (Il2CppObject * __this /* static, unused */, String_t* ___xmlName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String XmlHelper::Encode(System.String)
extern "C"  String_t* XmlHelper_Encode_m2974847862 (Il2CppObject * __this /* static, unused */, String_t* ___schemaName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XmlHelper::.cctor()
extern "C"  void XmlHelper__cctor_m2474544994 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
