﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLiteDatabase.SQLiteEventListener/OnError
struct OnError_t314785609;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SQLiteDatabase.SQLiteEventListener/OnError::.ctor(System.Object,System.IntPtr)
extern "C"  void OnError__ctor_m1086636545 (OnError_t314785609 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiteDatabase.SQLiteEventListener/OnError::Invoke(System.String)
extern "C"  void OnError_Invoke_m3917243719 (OnError_t314785609 * __this, String_t* ___err0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OnError_t314785609(Il2CppObject* delegate, String_t* ___err0);
// System.IAsyncResult SQLiteDatabase.SQLiteEventListener/OnError::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnError_BeginInvoke_m1468012748 (OnError_t314785609 * __this, String_t* ___err0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiteDatabase.SQLiteEventListener/OnError::EndInvoke(System.IAsyncResult)
extern "C"  void OnError_EndInvoke_m2953010321 (OnError_t314785609 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
