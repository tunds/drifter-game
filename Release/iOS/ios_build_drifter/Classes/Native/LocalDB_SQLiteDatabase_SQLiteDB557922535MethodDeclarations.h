﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLiteDatabase.SQLiteDB
struct SQLiteDB_t557922535;
// System.String
struct String_t;
// SQLiteDatabase.DBSchema
struct DBSchema_t4247391996;
// System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct List_1_t722451806;
// SQLiteDatabase.DBReader
struct DBReader_t4220400158;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "LocalDB_SQLiteDatabase_DBSchema4247391996.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_ConditionPair1745523828.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Condition1501727354.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataType4220602565.h"

// System.Void SQLiteDatabase.SQLiteDB::.ctor()
extern "C"  void SQLiteDB__ctor_m463384575 (SQLiteDB_t557922535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLiteDatabase.SQLiteDB::get_Exists()
extern "C"  bool SQLiteDB_get_Exists_m3278641920 (SQLiteDB_t557922535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLiteDatabase.SQLiteDB SQLiteDatabase.SQLiteDB::get_Instance()
extern "C"  SQLiteDB_t557922535 * SQLiteDB_get_Instance_m2094062401 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLiteDatabase.SQLiteDB::get_DBLocation()
extern "C"  String_t* SQLiteDB_get_DBLocation_m603624094 (SQLiteDB_t557922535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiteDatabase.SQLiteDB::set_DBLocation(System.String)
extern "C"  void SQLiteDB_set_DBLocation_m3743704717 (SQLiteDB_t557922535 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLiteDatabase.SQLiteDB::ConnectToDefaultDatabase(System.String,System.Boolean)
extern "C"  bool SQLiteDB_ConnectToDefaultDatabase_m3847386093 (SQLiteDB_t557922535 * __this, String_t* ___dbName0, bool ___loadFresh1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLiteDatabase.SQLiteDB::CreateDatabase(System.String,System.Boolean)
extern "C"  bool SQLiteDB_CreateDatabase_m2289450989 (SQLiteDB_t557922535 * __this, String_t* ___dbName0, bool ___isOverWrite1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLiteDatabase.SQLiteDB::CreateTable(SQLiteDatabase.DBSchema)
extern "C"  bool SQLiteDB_CreateTable_m3808471283 (SQLiteDB_t557922535 * __this, DBSchema_t4247391996 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLiteDatabase.SQLiteDB::DeleteTable(System.String)
extern "C"  bool SQLiteDB_DeleteTable_m3319765176 (SQLiteDB_t557922535 * __this, String_t* ___tableName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLiteDatabase.SQLiteDB::ClearTable(System.String)
extern "C"  int32_t SQLiteDB_ClearTable_m4150127616 (SQLiteDB_t557922535 * __this, String_t* ___tableName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLiteDatabase.SQLiteDB::DeleteDatabase()
extern "C"  bool SQLiteDB_DeleteDatabase_m2858673313 (SQLiteDB_t557922535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLiteDatabase.SQLiteDB::Insert(System.String,System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>)
extern "C"  int32_t SQLiteDB_Insert_m4013404553 (SQLiteDB_t557922535 * __this, String_t* ___tableName0, List_1_t722451806 * ___dataList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLiteDatabase.SQLiteDB::Update(System.String,System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>,SQLiteDatabase.SQLiteDB/DB_ConditionPair)
extern "C"  int32_t SQLiteDB_Update_m4162022779 (SQLiteDB_t557922535 * __this, String_t* ___tableName0, List_1_t722451806 * ___dataList1, DB_ConditionPair_t1745523828  ___condition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLiteDatabase.SQLiteDB::DeleteRow(System.String,SQLiteDatabase.SQLiteDB/DB_ConditionPair)
extern "C"  int32_t SQLiteDB_DeleteRow_m1399588258 (SQLiteDB_t557922535 * __this, String_t* ___tableName0, DB_ConditionPair_t1745523828  ___condition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLiteDatabase.SQLiteDB::GetConditionSymbol(SQLiteDatabase.SQLiteDB/DB_Condition)
extern "C"  String_t* SQLiteDB_GetConditionSymbol_m1038213543 (SQLiteDB_t557922535 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLiteDatabase.SQLiteDB::GetDataType(SQLiteDatabase.SQLiteDB/DB_DataType)
extern "C"  String_t* SQLiteDB_GetDataType_m3557671473 (SQLiteDB_t557922535 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLiteDatabase.DBReader SQLiteDatabase.SQLiteDB::Select(System.String)
extern "C"  DBReader_t4220400158 * SQLiteDB_Select_m2209825900 (SQLiteDB_t557922535 * __this, String_t* ___query0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLiteDatabase.DBReader SQLiteDatabase.SQLiteDB::GetAllData(System.String)
extern "C"  DBReader_t4220400158 * SQLiteDB_GetAllData_m261559315 (SQLiteDB_t557922535 * __this, String_t* ___tableName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLiteDatabase.SQLiteDB::IsTableExists(System.String)
extern "C"  bool SQLiteDB_IsTableExists_m4049229627 (SQLiteDB_t557922535 * __this, String_t* ___tableName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiteDatabase.SQLiteDB::Dispose()
extern "C"  void SQLiteDB_Dispose_m2829228156 (SQLiteDB_t557922535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
