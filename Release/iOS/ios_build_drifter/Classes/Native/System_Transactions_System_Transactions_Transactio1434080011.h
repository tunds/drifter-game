﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Transactions.TransactionManager
struct  TransactionManager_t1434080011  : public Il2CppObject
{
public:

public:
};

struct TransactionManager_t1434080011_StaticFields
{
public:
	// System.TimeSpan System.Transactions.TransactionManager::defaultTimeout
	TimeSpan_t763862892  ___defaultTimeout_0;
	// System.TimeSpan System.Transactions.TransactionManager::maxTimeout
	TimeSpan_t763862892  ___maxTimeout_1;

public:
	inline static int32_t get_offset_of_defaultTimeout_0() { return static_cast<int32_t>(offsetof(TransactionManager_t1434080011_StaticFields, ___defaultTimeout_0)); }
	inline TimeSpan_t763862892  get_defaultTimeout_0() const { return ___defaultTimeout_0; }
	inline TimeSpan_t763862892 * get_address_of_defaultTimeout_0() { return &___defaultTimeout_0; }
	inline void set_defaultTimeout_0(TimeSpan_t763862892  value)
	{
		___defaultTimeout_0 = value;
	}

	inline static int32_t get_offset_of_maxTimeout_1() { return static_cast<int32_t>(offsetof(TransactionManager_t1434080011_StaticFields, ___maxTimeout_1)); }
	inline TimeSpan_t763862892  get_maxTimeout_1() const { return ___maxTimeout_1; }
	inline TimeSpan_t763862892 * get_address_of_maxTimeout_1() { return &___maxTimeout_1; }
	inline void set_maxTimeout_1(TimeSpan_t763862892  value)
	{
		___maxTimeout_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
