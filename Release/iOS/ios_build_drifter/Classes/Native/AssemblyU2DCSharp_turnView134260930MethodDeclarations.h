﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// turnView
struct turnView_t134260930;

#include "codegen/il2cpp-codegen.h"

// System.Void turnView::.ctor()
extern "C"  void turnView__ctor_m1382338585 (turnView_t134260930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void turnView::Start()
extern "C"  void turnView_Start_m329476377 (turnView_t134260930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void turnView::Update()
extern "C"  void turnView_Update_m1629685268 (turnView_t134260930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void turnView::OnGUI()
extern "C"  void turnView_OnGUI_m877737235 (turnView_t134260930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
