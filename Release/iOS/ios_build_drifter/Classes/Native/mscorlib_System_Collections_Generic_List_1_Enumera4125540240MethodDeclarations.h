﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3994157758MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4094297699(__this, ___l0, method) ((  void (*) (Enumerator_t4125540240 *, List_1_t1744789952 *, const MethodInfo*))Enumerator__ctor_m2537912693_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2302499855(__this, method) ((  void (*) (Enumerator_t4125540240 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1835451837_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3239418107(__this, method) ((  Il2CppObject * (*) (Enumerator_t4125540240 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2236123177_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::Dispose()
#define Enumerator_Dispose_m1825053960(__this, method) ((  void (*) (Enumerator_t4125540240 *, const MethodInfo*))Enumerator_Dispose_m4095472794_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::VerifyState()
#define Enumerator_VerifyState_m3407222849(__this, method) ((  void (*) (Enumerator_t4125540240 *, const MethodInfo*))Enumerator_VerifyState_m1320146643_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::MoveNext()
#define Enumerator_MoveNext_m629634683(__this, method) ((  bool (*) (Enumerator_t4125540240 *, const MethodInfo*))Enumerator_MoveNext_m2293141801_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::get_Current()
#define Enumerator_get_Current_m1903994680(__this, method) ((  KeyValuePair_2_t947830983  (*) (Enumerator_t4125540240 *, const MethodInfo*))Enumerator_get_Current_m4111885770_gshared)(__this, method)
