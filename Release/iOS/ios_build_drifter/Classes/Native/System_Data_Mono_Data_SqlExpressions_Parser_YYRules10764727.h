﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t2956870243;

#include "mscorlib_System_MarshalByRefObject2055500882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.Parser/YYRules
struct  YYRules_t10764727  : public MarshalByRefObject_t2055500882
{
public:

public:
};

struct YYRules_t10764727_StaticFields
{
public:
	// System.String[] Mono.Data.SqlExpressions.Parser/YYRules::yyRule
	StringU5BU5D_t2956870243* ___yyRule_1;

public:
	inline static int32_t get_offset_of_yyRule_1() { return static_cast<int32_t>(offsetof(YYRules_t10764727_StaticFields, ___yyRule_1)); }
	inline StringU5BU5D_t2956870243* get_yyRule_1() const { return ___yyRule_1; }
	inline StringU5BU5D_t2956870243** get_address_of_yyRule_1() { return &___yyRule_1; }
	inline void set_yyRule_1(StringU5BU5D_t2956870243* value)
	{
		___yyRule_1 = value;
		Il2CppCodeGenWriteBarrier(&___yyRule_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
