﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerHealth
struct PlayerHealth_t3877793981;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerHealth::.ctor()
extern "C"  void PlayerHealth__ctor_m43779710 (PlayerHealth_t3877793981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerHealth::Start()
extern "C"  void PlayerHealth_Start_m3285884798 (PlayerHealth_t3877793981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerHealth::TookDamage(System.Int32)
extern "C"  void PlayerHealth_TookDamage_m4294364413 (PlayerHealth_t3877793981 * __this, int32_t ___amount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerHealth::Die()
extern "C"  void PlayerHealth_Die_m284211932 (PlayerHealth_t3877793981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
