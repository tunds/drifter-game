﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4127950636.h"
#include "mscorlib_System_Array2840145358.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataPair4220460133.h"

// System.Void System.Array/InternalEnumerator`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1255555596_gshared (InternalEnumerator_1_t4127950636 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1255555596(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4127950636 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1255555596_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1788002324_gshared (InternalEnumerator_1_t4127950636 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1788002324(__this, method) ((  void (*) (InternalEnumerator_1_t4127950636 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1788002324_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m146188042_gshared (InternalEnumerator_1_t4127950636 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m146188042(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4127950636 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m146188042_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3803228899_gshared (InternalEnumerator_1_t4127950636 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3803228899(__this, method) ((  void (*) (InternalEnumerator_1_t4127950636 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3803228899_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m439492036_gshared (InternalEnumerator_1_t4127950636 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m439492036(__this, method) ((  bool (*) (InternalEnumerator_1_t4127950636 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m439492036_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Current()
extern "C"  DB_DataPair_t4220460133  InternalEnumerator_1_get_Current_m3300836149_gshared (InternalEnumerator_1_t4127950636 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3300836149(__this, method) ((  DB_DataPair_t4220460133  (*) (InternalEnumerator_1_t4127950636 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3300836149_gshared)(__this, method)
