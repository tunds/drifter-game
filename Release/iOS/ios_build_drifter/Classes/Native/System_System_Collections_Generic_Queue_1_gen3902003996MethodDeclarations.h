﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2545193960MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<System.WeakReference>::.ctor()
#define Queue_1__ctor_m3275874577(__this, method) ((  void (*) (Queue_1_t3902003996 *, const MethodInfo*))Queue_1__ctor_m3042804833_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.WeakReference>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m4187341097(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t3902003996 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3260144643_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<System.WeakReference>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m442150153(__this, method) ((  bool (*) (Queue_1_t3902003996 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m63917275_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<System.WeakReference>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m2815874633(__this, method) ((  Il2CppObject * (*) (Queue_1_t3902003996 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m2093948217_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.WeakReference>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2043515149(__this, method) ((  Il2CppObject* (*) (Queue_1_t3902003996 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m472615211_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.WeakReference>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m371849720(__this, method) ((  Il2CppObject * (*) (Queue_1_t3902003996 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3688614462_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.WeakReference>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m1671533460(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t3902003996 *, WeakReferenceU5BU5D_t3012077625*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3592753262_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<System.WeakReference>::Dequeue()
#define Queue_1_Dequeue_m4277485923(__this, method) ((  WeakReference_t2193916456 * (*) (Queue_1_t3902003996 *, const MethodInfo*))Queue_1_Dequeue_m102813934_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<System.WeakReference>::Peek()
#define Queue_1_Peek_m3991481923(__this, method) ((  WeakReference_t2193916456 * (*) (Queue_1_t3902003996 *, const MethodInfo*))Queue_1_Peek_m3013356031_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.WeakReference>::Enqueue(T)
#define Queue_1_Enqueue_m127595752(__this, ___item0, method) ((  void (*) (Queue_1_t3902003996 *, WeakReference_t2193916456 *, const MethodInfo*))Queue_1_Enqueue_m4079343671_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<System.WeakReference>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m2145418662(__this, ___new_size0, method) ((  void (*) (Queue_1_t3902003996 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m1573690380_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<System.WeakReference>::get_Count()
#define Queue_1_get_Count_m1730826447(__this, method) ((  int32_t (*) (Queue_1_t3902003996 *, const MethodInfo*))Queue_1_get_Count_m1429559317_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.WeakReference>::GetEnumerator()
#define Queue_1_GetEnumerator_m3247089338(__this, method) ((  Enumerator_t1076658417  (*) (Queue_1_t3902003996 *, const MethodInfo*))Queue_1_GetEnumerator_m3965043378_gshared)(__this, method)
