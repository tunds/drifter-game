﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloseManager
struct CloseManager_t3949459125;

#include "codegen/il2cpp-codegen.h"

// System.Void CloseManager::.ctor()
extern "C"  void CloseManager__ctor_m3403790214 (CloseManager_t3949459125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloseManager::goHome()
extern "C"  void CloseManager_goHome_m3794559429 (CloseManager_t3949459125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
