﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SQLiter.SQLite
struct SQLite_t668408974;
// GlobalObjectManager
struct GlobalObjectManager_t849077355;
// UnityEngine.UI.InputField
struct InputField_t2345609593;
// UnityEngine.UI.Text
struct Text_t3286458198;
// System.String
struct String_t;
// SQLiteDatabase.SQLiteDB
struct SQLiteDB_t557922535;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventsManager
struct  EventsManager_t4180097076  : public MonoBehaviour_t3012272455
{
public:
	// SQLiter.SQLite EventsManager::data
	SQLite_t668408974 * ___data_2;
	// GlobalObjectManager EventsManager::persistentData
	GlobalObjectManager_t849077355 * ___persistentData_3;
	// UnityEngine.UI.InputField EventsManager::input
	InputField_t2345609593 * ___input_4;
	// UnityEngine.UI.Text EventsManager::time
	Text_t3286458198 * ___time_5;
	// System.String EventsManager::name
	String_t* ___name_6;
	// SQLiteDatabase.SQLiteDB EventsManager::db
	SQLiteDB_t557922535 * ___db_7;

public:
	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(EventsManager_t4180097076, ___data_2)); }
	inline SQLite_t668408974 * get_data_2() const { return ___data_2; }
	inline SQLite_t668408974 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(SQLite_t668408974 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier(&___data_2, value);
	}

	inline static int32_t get_offset_of_persistentData_3() { return static_cast<int32_t>(offsetof(EventsManager_t4180097076, ___persistentData_3)); }
	inline GlobalObjectManager_t849077355 * get_persistentData_3() const { return ___persistentData_3; }
	inline GlobalObjectManager_t849077355 ** get_address_of_persistentData_3() { return &___persistentData_3; }
	inline void set_persistentData_3(GlobalObjectManager_t849077355 * value)
	{
		___persistentData_3 = value;
		Il2CppCodeGenWriteBarrier(&___persistentData_3, value);
	}

	inline static int32_t get_offset_of_input_4() { return static_cast<int32_t>(offsetof(EventsManager_t4180097076, ___input_4)); }
	inline InputField_t2345609593 * get_input_4() const { return ___input_4; }
	inline InputField_t2345609593 ** get_address_of_input_4() { return &___input_4; }
	inline void set_input_4(InputField_t2345609593 * value)
	{
		___input_4 = value;
		Il2CppCodeGenWriteBarrier(&___input_4, value);
	}

	inline static int32_t get_offset_of_time_5() { return static_cast<int32_t>(offsetof(EventsManager_t4180097076, ___time_5)); }
	inline Text_t3286458198 * get_time_5() const { return ___time_5; }
	inline Text_t3286458198 ** get_address_of_time_5() { return &___time_5; }
	inline void set_time_5(Text_t3286458198 * value)
	{
		___time_5 = value;
		Il2CppCodeGenWriteBarrier(&___time_5, value);
	}

	inline static int32_t get_offset_of_name_6() { return static_cast<int32_t>(offsetof(EventsManager_t4180097076, ___name_6)); }
	inline String_t* get_name_6() const { return ___name_6; }
	inline String_t** get_address_of_name_6() { return &___name_6; }
	inline void set_name_6(String_t* value)
	{
		___name_6 = value;
		Il2CppCodeGenWriteBarrier(&___name_6, value);
	}

	inline static int32_t get_offset_of_db_7() { return static_cast<int32_t>(offsetof(EventsManager_t4180097076, ___db_7)); }
	inline SQLiteDB_t557922535 * get_db_7() const { return ___db_7; }
	inline SQLiteDB_t557922535 ** get_address_of_db_7() { return &___db_7; }
	inline void set_db_7(SQLiteDB_t557922535 * value)
	{
		___db_7 = value;
		Il2CppCodeGenWriteBarrier(&___db_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
