﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct List_1_t282322642;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLiteDatabase.DBSchema
struct  DBSchema_t4247391996  : public Il2CppObject
{
public:
	// System.String SQLiteDatabase.DBSchema::_name
	String_t* ____name_0;
	// System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field> SQLiteDatabase.DBSchema::fieldList
	List_1_t282322642 * ___fieldList_1;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(DBSchema_t4247391996, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier(&____name_0, value);
	}

	inline static int32_t get_offset_of_fieldList_1() { return static_cast<int32_t>(offsetof(DBSchema_t4247391996, ___fieldList_1)); }
	inline List_1_t282322642 * get_fieldList_1() const { return ___fieldList_1; }
	inline List_1_t282322642 ** get_address_of_fieldList_1() { return &___fieldList_1; }
	inline void set_fieldList_1(List_1_t282322642 * value)
	{
		___fieldList_1 = value;
		Il2CppCodeGenWriteBarrier(&___fieldList_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
