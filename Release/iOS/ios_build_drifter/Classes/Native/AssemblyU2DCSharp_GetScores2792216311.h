﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text[]
struct TextU5BU5D_t4157799059;
// SQLiter.SQLite
struct SQLite_t668408974;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetScores
struct  GetScores_t2792216311  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.UI.Text[] GetScores::names
	TextU5BU5D_t4157799059* ___names_2;
	// UnityEngine.UI.Text[] GetScores::scores
	TextU5BU5D_t4157799059* ___scores_3;
	// SQLiter.SQLite GetScores::data
	SQLite_t668408974 * ___data_4;

public:
	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(GetScores_t2792216311, ___names_2)); }
	inline TextU5BU5D_t4157799059* get_names_2() const { return ___names_2; }
	inline TextU5BU5D_t4157799059** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(TextU5BU5D_t4157799059* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier(&___names_2, value);
	}

	inline static int32_t get_offset_of_scores_3() { return static_cast<int32_t>(offsetof(GetScores_t2792216311, ___scores_3)); }
	inline TextU5BU5D_t4157799059* get_scores_3() const { return ___scores_3; }
	inline TextU5BU5D_t4157799059** get_address_of_scores_3() { return &___scores_3; }
	inline void set_scores_3(TextU5BU5D_t4157799059* value)
	{
		___scores_3 = value;
		Il2CppCodeGenWriteBarrier(&___scores_3, value);
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(GetScores_t2792216311, ___data_4)); }
	inline SQLite_t668408974 * get_data_4() const { return ___data_4; }
	inline SQLite_t668408974 ** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(SQLite_t668408974 * value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier(&___data_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
