﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4090124299.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Data_System_Data_Common_DbConnection_Column4182633796.h"

// System.Void System.Array/InternalEnumerator`1<System.Data.Common.DbConnection/ColumnInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3636387722_gshared (InternalEnumerator_1_t4090124299 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3636387722(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4090124299 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3636387722_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Data.Common.DbConnection/ColumnInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1170900438_gshared (InternalEnumerator_1_t4090124299 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1170900438(__this, method) ((  void (*) (InternalEnumerator_1_t4090124299 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1170900438_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Data.Common.DbConnection/ColumnInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1769847234_gshared (InternalEnumerator_1_t4090124299 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1769847234(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4090124299 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1769847234_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Data.Common.DbConnection/ColumnInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2373610977_gshared (InternalEnumerator_1_t4090124299 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2373610977(__this, method) ((  void (*) (InternalEnumerator_1_t4090124299 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2373610977_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Data.Common.DbConnection/ColumnInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1380228930_gshared (InternalEnumerator_1_t4090124299 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1380228930(__this, method) ((  bool (*) (InternalEnumerator_1_t4090124299 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1380228930_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Data.Common.DbConnection/ColumnInfo>::get_Current()
extern "C"  ColumnInfo_t4182633796  InternalEnumerator_1_get_Current_m3836512977_gshared (InternalEnumerator_1_t4090124299 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3836512977(__this, method) ((  ColumnInfo_t4182633796  (*) (InternalEnumerator_1_t4090124299 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3836512977_gshared)(__this, method)
