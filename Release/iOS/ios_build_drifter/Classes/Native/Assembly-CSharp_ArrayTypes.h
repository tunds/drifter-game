﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// PoqXert.MessageBox.MSGBoxStyle
struct MSGBoxStyle_t1923386386;

#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_MSGBoxStyle1923386386.h"

#pragma once
// PoqXert.MessageBox.MSGBoxStyle[]
struct MSGBoxStyleU5BU5D_t4110104423  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) MSGBoxStyle_t1923386386 * m_Items[1];

public:
	inline MSGBoxStyle_t1923386386 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MSGBoxStyle_t1923386386 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MSGBoxStyle_t1923386386 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
