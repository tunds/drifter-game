﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Transactions.PreparingEnlistment
struct PreparingEnlistment_t3015517605;
// System.Transactions.Transaction
struct Transaction_t3175846586;
// System.Transactions.IEnlistmentNotification
struct IEnlistmentNotification_t1752309717;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Transactions_System_Transactions_Transactio3175846586.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void System.Transactions.PreparingEnlistment::.ctor(System.Transactions.Transaction,System.Transactions.IEnlistmentNotification)
extern "C"  void PreparingEnlistment__ctor_m3145960357 (PreparingEnlistment_t3015517605 * __this, Transaction_t3175846586 * ___tx0, Il2CppObject * ___enlisted1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.PreparingEnlistment::ForceRollback()
extern "C"  void PreparingEnlistment_ForceRollback_m1708279619 (PreparingEnlistment_t3015517605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.PreparingEnlistment::ForceRollback(System.Exception)
extern "C"  void PreparingEnlistment_ForceRollback_m2493706739 (PreparingEnlistment_t3015517605 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.PreparingEnlistment::Prepared()
extern "C"  void PreparingEnlistment_Prepared_m4090146571 (PreparingEnlistment_t3015517605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.PreparingEnlistment::get_IsPrepared()
extern "C"  bool PreparingEnlistment_get_IsPrepared_m4211467806 (PreparingEnlistment_t3015517605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
