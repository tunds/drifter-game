﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3962101249.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24054610746.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1161707284_gshared (InternalEnumerator_1_t3962101249 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1161707284(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3962101249 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1161707284_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2662749708_gshared (InternalEnumerator_1_t3962101249 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2662749708(__this, method) ((  void (*) (InternalEnumerator_1_t3962101249 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2662749708_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1565140418_gshared (InternalEnumerator_1_t3962101249 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1565140418(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3962101249 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1565140418_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1803014635_gshared (InternalEnumerator_1_t3962101249 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1803014635(__this, method) ((  void (*) (InternalEnumerator_1_t3962101249 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1803014635_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1464645436_gshared (InternalEnumerator_1_t3962101249 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1464645436(__this, method) ((  bool (*) (InternalEnumerator_1_t3962101249 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1464645436_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t4054610746  InternalEnumerator_1_get_Current_m3132308989_gshared (InternalEnumerator_1_t3962101249 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3132308989(__this, method) ((  KeyValuePair_2_t4054610746  (*) (InternalEnumerator_1_t3962101249 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3132308989_gshared)(__this, method)
