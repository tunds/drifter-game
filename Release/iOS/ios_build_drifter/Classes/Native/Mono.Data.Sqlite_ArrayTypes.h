﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// Mono.Data.Sqlite.SqliteFunction
struct SqliteFunction_t3537217675;
// Mono.Data.Sqlite.SqliteStatement
struct SqliteStatement_t3906494218;
// Mono.Data.Sqlite.SQLiteType
struct SQLiteType_t3947843053;
// Mono.Data.Sqlite.SqliteFunction/AggregateData
struct AggregateData_t2045614569;
// Mono.Data.Sqlite.SqliteFunctionAttribute
struct SqliteFunctionAttribute_t1309010751;
// Mono.Data.Sqlite.SqliteParameter
struct SqliteParameter_t3651135812;

#include "mscorlib_System_Array2840145358.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteFunction3537217675.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteStatement3906494218.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteTypeNames1996750281.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_TypeAffinity3864856329.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteType3947843053.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteFunction_A2045614569.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteFunctionAt1309010751.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K848873357.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteParameter3651135812.h"

#pragma once
// Mono.Data.Sqlite.SqliteFunction[]
struct SqliteFunctionU5BU5D_t1563239658  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) SqliteFunction_t3537217675 * m_Items[1];

public:
	inline SqliteFunction_t3537217675 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SqliteFunction_t3537217675 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SqliteFunction_t3537217675 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Data.Sqlite.SqliteStatement[]
struct SqliteStatementU5BU5D_t83992975  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) SqliteStatement_t3906494218 * m_Items[1];

public:
	inline SqliteStatement_t3906494218 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SqliteStatement_t3906494218 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SqliteStatement_t3906494218 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Data.Sqlite.SQLiteTypeNames[]
struct SQLiteTypeNamesU5BU5D_t692931828  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) SQLiteTypeNames_t1996750281  m_Items[1];

public:
	inline SQLiteTypeNames_t1996750281  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SQLiteTypeNames_t1996750281 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SQLiteTypeNames_t1996750281  value)
	{
		m_Items[index] = value;
	}
};
// Mono.Data.Sqlite.TypeAffinity[]
struct TypeAffinityU5BU5D_t3402806964  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Mono.Data.Sqlite.SQLiteType[]
struct SQLiteTypeU5BU5D_t2174329792  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) SQLiteType_t3947843053 * m_Items[1];

public:
	inline SQLiteType_t3947843053 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SQLiteType_t3947843053 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SQLiteType_t3947843053 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Data.Sqlite.SqliteFunction/AggregateData[]
struct AggregateDataU5BU5D_t1932663892  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) AggregateData_t2045614569 * m_Items[1];

public:
	inline AggregateData_t2045614569 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AggregateData_t2045614569 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AggregateData_t2045614569 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Data.Sqlite.SqliteFunctionAttribute[]
struct SqliteFunctionAttributeU5BU5D_t494148838  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) SqliteFunctionAttribute_t1309010751 * m_Items[1];

public:
	inline SqliteFunctionAttribute_t1309010751 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SqliteFunctionAttribute_t1309010751 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SqliteFunctionAttribute_t1309010751 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Mono.Data.Sqlite.SqliteKeyReader/KeyInfo[]
struct KeyInfoU5BU5D_t1118564256  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) KeyInfo_t848873357  m_Items[1];

public:
	inline KeyInfo_t848873357  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KeyInfo_t848873357 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KeyInfo_t848873357  value)
	{
		m_Items[index] = value;
	}
};
// Mono.Data.Sqlite.SqliteParameter[]
struct SqliteParameterU5BU5D_t2580649261  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) SqliteParameter_t3651135812 * m_Items[1];

public:
	inline SqliteParameter_t3651135812 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SqliteParameter_t3651135812 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SqliteParameter_t3651135812 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
