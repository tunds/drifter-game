﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct EqualityComparer_1_t3654850783;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.EqualityComparer`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1522422310_gshared (EqualityComparer_1_t3654850783 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m1522422310(__this, method) ((  void (*) (EqualityComparer_1_t3654850783 *, const MethodInfo*))EqualityComparer_1__ctor_m1522422310_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3763322439_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3763322439(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3763322439_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2790913535_gshared (EqualityComparer_1_t3654850783 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2790913535(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t3654850783 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2790913535_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1435198827_gshared (EqualityComparer_1_t3654850783 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1435198827(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t3654850783 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1435198827_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Default()
extern "C"  EqualityComparer_1_t3654850783 * EqualityComparer_1_get_Default_m3871364944_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m3871364944(__this /* static, unused */, method) ((  EqualityComparer_1_t3654850783 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m3871364944_gshared)(__this /* static, unused */, method)
