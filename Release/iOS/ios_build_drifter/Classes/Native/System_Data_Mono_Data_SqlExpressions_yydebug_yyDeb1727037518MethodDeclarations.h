﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.SqlExpressions.yydebug.yyDebugSimple
struct yyDebugSimple_t1727037518;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Mono.Data.SqlExpressions.yydebug.yyDebugSimple::.ctor()
extern "C"  void yyDebugSimple__ctor_m3857481748 (yyDebugSimple_t1727037518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqlExpressions.yydebug.yyDebugSimple::println(System.String)
extern "C"  void yyDebugSimple_println_m209570401 (yyDebugSimple_t1727037518 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqlExpressions.yydebug.yyDebugSimple::push(System.Int32,System.Object)
extern "C"  void yyDebugSimple_push_m3090619657 (yyDebugSimple_t1727037518 * __this, int32_t ___state0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqlExpressions.yydebug.yyDebugSimple::lex(System.Int32,System.Int32,System.String,System.Object)
extern "C"  void yyDebugSimple_lex_m4175259871 (yyDebugSimple_t1727037518 * __this, int32_t ___state0, int32_t ___token1, String_t* ___name2, Il2CppObject * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqlExpressions.yydebug.yyDebugSimple::shift(System.Int32,System.Int32,System.Int32)
extern "C"  void yyDebugSimple_shift_m3849427397 (yyDebugSimple_t1727037518 * __this, int32_t ___from0, int32_t ___to1, int32_t ___errorFlag2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqlExpressions.yydebug.yyDebugSimple::pop(System.Int32)
extern "C"  void yyDebugSimple_pop_m3282692212 (yyDebugSimple_t1727037518 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqlExpressions.yydebug.yyDebugSimple::discard(System.Int32,System.Int32,System.String,System.Object)
extern "C"  void yyDebugSimple_discard_m262389760 (yyDebugSimple_t1727037518 * __this, int32_t ___state0, int32_t ___token1, String_t* ___name2, Il2CppObject * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqlExpressions.yydebug.yyDebugSimple::reduce(System.Int32,System.Int32,System.Int32,System.String,System.Int32)
extern "C"  void yyDebugSimple_reduce_m247434260 (yyDebugSimple_t1727037518 * __this, int32_t ___from0, int32_t ___to1, int32_t ___rule2, String_t* ___text3, int32_t ___len4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqlExpressions.yydebug.yyDebugSimple::shift(System.Int32,System.Int32)
extern "C"  void yyDebugSimple_shift_m2592282706 (yyDebugSimple_t1727037518 * __this, int32_t ___from0, int32_t ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqlExpressions.yydebug.yyDebugSimple::accept(System.Object)
extern "C"  void yyDebugSimple_accept_m199829180 (yyDebugSimple_t1727037518 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqlExpressions.yydebug.yyDebugSimple::error(System.String)
extern "C"  void yyDebugSimple_error_m3249923048 (yyDebugSimple_t1727037518 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqlExpressions.yydebug.yyDebugSimple::reject()
extern "C"  void yyDebugSimple_reject_m3348942511 (yyDebugSimple_t1727037518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
