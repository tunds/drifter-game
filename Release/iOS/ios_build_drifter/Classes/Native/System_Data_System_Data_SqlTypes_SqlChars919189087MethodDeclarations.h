﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.SqlTypes.SqlChars
struct SqlChars_t919189087;
// System.Xml.XmlReader
struct XmlReader_t4229084514;
// System.Xml.XmlWriter
struct XmlWriter_t89522450;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t176365656;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t3827173367;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlReader4229084514.h"
#include "System_Xml_System_Xml_XmlWriter89522450.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSet3827173367.h"

// System.Void System.Data.SqlTypes.SqlChars::.ctor()
extern "C"  void SqlChars__ctor_m1049724259 (SqlChars_t919189087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.SqlTypes.SqlChars::System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader)
extern "C"  void SqlChars_System_Xml_Serialization_IXmlSerializable_ReadXml_m2779935505 (SqlChars_t919189087 * __this, XmlReader_t4229084514 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.SqlTypes.SqlChars::System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter)
extern "C"  void SqlChars_System_Xml_Serialization_IXmlSerializable_WriteXml_m1302649906 (SqlChars_t919189087 * __this, XmlWriter_t89522450 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.SqlTypes.SqlChars::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SqlChars_System_Runtime_Serialization_ISerializable_GetObjectData_m3583424712 (SqlChars_t919189087 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.SqlTypes.SqlChars System.Data.SqlTypes.SqlChars::get_Null()
extern "C"  SqlChars_t919189087 * SqlChars_get_Null_m1651673387 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlQualifiedName System.Data.SqlTypes.SqlChars::GetXsdType(System.Xml.Schema.XmlSchemaSet)
extern "C"  XmlQualifiedName_t176365656 * SqlChars_GetXsdType_m1237510272 (Il2CppObject * __this /* static, unused */, XmlSchemaSet_t3827173367 * ___schemaSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
