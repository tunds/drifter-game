﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.SqliteClient.SqliteBusyException
struct SqliteBusyException_t975985850;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Mono.Data.SqliteClient.SqliteBusyException::.ctor()
extern "C"  void SqliteBusyException__ctor_m2561528689 (SqliteBusyException_t975985850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteBusyException::.ctor(System.String)
extern "C"  void SqliteBusyException__ctor_m4276661041 (SqliteBusyException_t975985850 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
