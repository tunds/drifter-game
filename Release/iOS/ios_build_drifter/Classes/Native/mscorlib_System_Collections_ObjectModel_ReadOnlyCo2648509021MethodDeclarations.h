﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct ReadOnlyCollection_1_t2648509021;
// System.Collections.Generic.IList`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct IList_1_t1651855987;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// SQLiteDatabase.SQLiteDB/DB_Field[]
struct DB_FieldU5BU5D_t1717179044;
// System.Collections.Generic.IEnumerator`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct IEnumerator_1_t968470121;

#include "codegen/il2cpp-codegen.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Field3780330969.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m528038617_gshared (ReadOnlyCollection_1_t2648509021 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m528038617(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2648509021 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m528038617_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3995228227_gshared (ReadOnlyCollection_1_t2648509021 * __this, DB_Field_t3780330969  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3995228227(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2648509021 *, DB_Field_t3780330969 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3995228227_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1304714471_gshared (ReadOnlyCollection_1_t2648509021 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1304714471(__this, method) ((  void (*) (ReadOnlyCollection_1_t2648509021 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1304714471_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1651221354_gshared (ReadOnlyCollection_1_t2648509021 * __this, int32_t ___index0, DB_Field_t3780330969  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1651221354(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2648509021 *, int32_t, DB_Field_t3780330969 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1651221354_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m509459604_gshared (ReadOnlyCollection_1_t2648509021 * __this, DB_Field_t3780330969  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m509459604(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2648509021 *, DB_Field_t3780330969 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m509459604_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3820041520_gshared (ReadOnlyCollection_1_t2648509021 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3820041520(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2648509021 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3820041520_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  DB_Field_t3780330969  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2820826646_gshared (ReadOnlyCollection_1_t2648509021 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2820826646(__this, ___index0, method) ((  DB_Field_t3780330969  (*) (ReadOnlyCollection_1_t2648509021 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2820826646_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3790385601_gshared (ReadOnlyCollection_1_t2648509021 * __this, int32_t ___index0, DB_Field_t3780330969  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3790385601(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2648509021 *, int32_t, DB_Field_t3780330969 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3790385601_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1193046651_gshared (ReadOnlyCollection_1_t2648509021 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1193046651(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2648509021 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1193046651_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m444737288_gshared (ReadOnlyCollection_1_t2648509021 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m444737288(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2648509021 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m444737288_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2841428631_gshared (ReadOnlyCollection_1_t2648509021 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2841428631(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2648509021 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2841428631_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m4258393478_gshared (ReadOnlyCollection_1_t2648509021 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m4258393478(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2648509021 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m4258393478_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m252532246_gshared (ReadOnlyCollection_1_t2648509021 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m252532246(__this, method) ((  void (*) (ReadOnlyCollection_1_t2648509021 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m252532246_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m142635770_gshared (ReadOnlyCollection_1_t2648509021 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m142635770(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2648509021 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m142635770_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1874063582_gshared (ReadOnlyCollection_1_t2648509021 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1874063582(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2648509021 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1874063582_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1458214993_gshared (ReadOnlyCollection_1_t2648509021 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1458214993(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2648509021 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1458214993_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m4033012023_gshared (ReadOnlyCollection_1_t2648509021 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m4033012023(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2648509021 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m4033012023_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2054676833_gshared (ReadOnlyCollection_1_t2648509021 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2054676833(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2648509021 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2054676833_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1534544418_gshared (ReadOnlyCollection_1_t2648509021 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1534544418(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2648509021 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1534544418_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m855242068_gshared (ReadOnlyCollection_1_t2648509021 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m855242068(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2648509021 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m855242068_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1085302249_gshared (ReadOnlyCollection_1_t2648509021 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1085302249(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2648509021 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1085302249_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1320302576_gshared (ReadOnlyCollection_1_t2648509021 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1320302576(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2648509021 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1320302576_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2614485467_gshared (ReadOnlyCollection_1_t2648509021 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2614485467(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2648509021 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2614485467_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1799222248_gshared (ReadOnlyCollection_1_t2648509021 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1799222248(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2648509021 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1799222248_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1625583065_gshared (ReadOnlyCollection_1_t2648509021 * __this, DB_Field_t3780330969  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1625583065(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2648509021 *, DB_Field_t3780330969 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1625583065_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m716735603_gshared (ReadOnlyCollection_1_t2648509021 * __this, DB_FieldU5BU5D_t1717179044* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m716735603(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2648509021 *, DB_FieldU5BU5D_t1717179044*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m716735603_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m70422832_gshared (ReadOnlyCollection_1_t2648509021 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m70422832(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2648509021 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m70422832_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m256873599_gshared (ReadOnlyCollection_1_t2648509021 * __this, DB_Field_t3780330969  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m256873599(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2648509021 *, DB_Field_t3780330969 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m256873599_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3485421788_gshared (ReadOnlyCollection_1_t2648509021 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3485421788(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2648509021 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3485421788_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32)
extern "C"  DB_Field_t3780330969  ReadOnlyCollection_1_get_Item_m3301530326_gshared (ReadOnlyCollection_1_t2648509021 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3301530326(__this, ___index0, method) ((  DB_Field_t3780330969  (*) (ReadOnlyCollection_1_t2648509021 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3301530326_gshared)(__this, ___index0, method)
