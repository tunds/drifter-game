﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_Generic_Comparer_1_gen380153173.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Comparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct  DefaultComparer_t3558413865  : public Comparer_1_t380153173
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
