﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2494265714.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Data_System_Data_DbType2586775211.h"

// System.Void System.Array/InternalEnumerator`1<System.Data.DbType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2557780558_gshared (InternalEnumerator_1_t2494265714 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2557780558(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2494265714 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2557780558_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Data.DbType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3025249426_gshared (InternalEnumerator_1_t2494265714 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3025249426(__this, method) ((  void (*) (InternalEnumerator_1_t2494265714 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3025249426_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Data.DbType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3182472318_gshared (InternalEnumerator_1_t2494265714 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3182472318(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2494265714 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3182472318_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Data.DbType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4206630309_gshared (InternalEnumerator_1_t2494265714 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4206630309(__this, method) ((  void (*) (InternalEnumerator_1_t2494265714 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4206630309_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Data.DbType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2280147454_gshared (InternalEnumerator_1_t2494265714 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2280147454(__this, method) ((  bool (*) (InternalEnumerator_1_t2494265714 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2280147454_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Data.DbType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m467844501_gshared (InternalEnumerator_1_t2494265714 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m467844501(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2494265714 *, const MethodInfo*))InternalEnumerator_1_get_Current_m467844501_gshared)(__this, method)
