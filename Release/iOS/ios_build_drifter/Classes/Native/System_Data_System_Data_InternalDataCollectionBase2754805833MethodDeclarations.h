﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.InternalDataCollectionBase
struct InternalDataCollectionBase_t2754805833;
// System.Collections.ArrayList
struct ArrayList_t2121638921;
// System.Object
struct Il2CppObject;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"

// System.Void System.Data.InternalDataCollectionBase::.ctor()
extern "C"  void InternalDataCollectionBase__ctor_m1509675099 (InternalDataCollectionBase_t2754805833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.InternalDataCollectionBase::get_Count()
extern "C"  int32_t InternalDataCollectionBase_get_Count_m2124458619 (InternalDataCollectionBase_t2754805833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.InternalDataCollectionBase::get_IsSynchronized()
extern "C"  bool InternalDataCollectionBase_get_IsSynchronized_m3837304314 (InternalDataCollectionBase_t2754805833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Data.InternalDataCollectionBase::get_List()
extern "C"  ArrayList_t2121638921 * InternalDataCollectionBase_get_List_m2323300804 (InternalDataCollectionBase_t2754805833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Data.InternalDataCollectionBase::get_SyncRoot()
extern "C"  Il2CppObject * InternalDataCollectionBase_get_SyncRoot_m4177733284 (InternalDataCollectionBase_t2754805833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.InternalDataCollectionBase::CopyTo(System.Array,System.Int32)
extern "C"  void InternalDataCollectionBase_CopyTo_m2499063160 (InternalDataCollectionBase_t2754805833 * __this, Il2CppArray * ___ar0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Data.InternalDataCollectionBase::GetEnumerator()
extern "C"  Il2CppObject * InternalDataCollectionBase_GetEnumerator_m2882440085 (InternalDataCollectionBase_t2754805833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
