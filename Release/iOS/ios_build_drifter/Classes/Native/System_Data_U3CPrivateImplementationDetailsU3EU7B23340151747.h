﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "System_Data_U3CPrivateImplementationDetailsU3EU7B22366886725.h"
#include "System_Data_U3CPrivateImplementationDetailsU3EU7B22366888523.h"
#include "System_Data_U3CPrivateImplementationDetailsU3EU7B20214898668.h"
#include "System_Data_U3CPrivateImplementationDetailsU3EU7B20359038729.h"
#include "System_Data_U3CPrivateImplementationDetailsU3EU7B20214898602.h"
#include "System_Data_U3CPrivateImplementationDetailsU3EU7B22777878858.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}
struct  U3CPrivateImplementationDetailsU3EU7B208ed9b8U2D43e9U2D4190U2D912cU2D9dd9f5304e9bU7D_t3340151747  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7B208ed9b8U2D43e9U2D4190U2D912cU2D9dd9f5304e9bU7D_t3340151747_StaticFields
{
public:
	// <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}/$ArrayType=168 <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}::$field-0
	U24ArrayTypeU3D168_t2366886725  ___U24fieldU2D0_0;
	// <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}/$ArrayType=168 <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}::$field-1
	U24ArrayTypeU3D168_t2366886725  ___U24fieldU2D1_1;
	// <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}/$ArrayType=328 <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}::$field-2
	U24ArrayTypeU3D328_t2366888523  ___U24fieldU2D2_2;
	// <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}/$ArrayType=64 <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}::$field-3
	U24ArrayTypeU3D64_t214898668  ___U24fieldU2D3_3;
	// <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}/$ArrayType=328 <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}::$field-4
	U24ArrayTypeU3D328_t2366888523  ___U24fieldU2D4_4;
	// <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}/$ArrayType=328 <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}::$field-5
	U24ArrayTypeU3D328_t2366888523  ___U24fieldU2D5_5;
	// <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}/$ArrayType=64 <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}::$field-6
	U24ArrayTypeU3D64_t214898668  ___U24fieldU2D6_6;
	// <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}/$ArrayType=1084 <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}::$field-7
	U24ArrayTypeU3D1084_t359038729  ___U24fieldU2D7_7;
	// <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}/$ArrayType=1084 <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}::$field-8
	U24ArrayTypeU3D1084_t359038729  ___U24fieldU2D8_8;
	// <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}/$ArrayType=40 <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}::$field-9
	U24ArrayTypeU3D40_t214898602  ___U24fieldU2D9_9;
	// <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}/$ArrayType=8 <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}::$field-A
	U24ArrayTypeU3D8_t2777878858  ___U24fieldU2DA_10;

public:
	inline static int32_t get_offset_of_U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B208ed9b8U2D43e9U2D4190U2D912cU2D9dd9f5304e9bU7D_t3340151747_StaticFields, ___U24fieldU2D0_0)); }
	inline U24ArrayTypeU3D168_t2366886725  get_U24fieldU2D0_0() const { return ___U24fieldU2D0_0; }
	inline U24ArrayTypeU3D168_t2366886725 * get_address_of_U24fieldU2D0_0() { return &___U24fieldU2D0_0; }
	inline void set_U24fieldU2D0_0(U24ArrayTypeU3D168_t2366886725  value)
	{
		___U24fieldU2D0_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B208ed9b8U2D43e9U2D4190U2D912cU2D9dd9f5304e9bU7D_t3340151747_StaticFields, ___U24fieldU2D1_1)); }
	inline U24ArrayTypeU3D168_t2366886725  get_U24fieldU2D1_1() const { return ___U24fieldU2D1_1; }
	inline U24ArrayTypeU3D168_t2366886725 * get_address_of_U24fieldU2D1_1() { return &___U24fieldU2D1_1; }
	inline void set_U24fieldU2D1_1(U24ArrayTypeU3D168_t2366886725  value)
	{
		___U24fieldU2D1_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B208ed9b8U2D43e9U2D4190U2D912cU2D9dd9f5304e9bU7D_t3340151747_StaticFields, ___U24fieldU2D2_2)); }
	inline U24ArrayTypeU3D328_t2366888523  get_U24fieldU2D2_2() const { return ___U24fieldU2D2_2; }
	inline U24ArrayTypeU3D328_t2366888523 * get_address_of_U24fieldU2D2_2() { return &___U24fieldU2D2_2; }
	inline void set_U24fieldU2D2_2(U24ArrayTypeU3D328_t2366888523  value)
	{
		___U24fieldU2D2_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B208ed9b8U2D43e9U2D4190U2D912cU2D9dd9f5304e9bU7D_t3340151747_StaticFields, ___U24fieldU2D3_3)); }
	inline U24ArrayTypeU3D64_t214898668  get_U24fieldU2D3_3() const { return ___U24fieldU2D3_3; }
	inline U24ArrayTypeU3D64_t214898668 * get_address_of_U24fieldU2D3_3() { return &___U24fieldU2D3_3; }
	inline void set_U24fieldU2D3_3(U24ArrayTypeU3D64_t214898668  value)
	{
		___U24fieldU2D3_3 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D4_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B208ed9b8U2D43e9U2D4190U2D912cU2D9dd9f5304e9bU7D_t3340151747_StaticFields, ___U24fieldU2D4_4)); }
	inline U24ArrayTypeU3D328_t2366888523  get_U24fieldU2D4_4() const { return ___U24fieldU2D4_4; }
	inline U24ArrayTypeU3D328_t2366888523 * get_address_of_U24fieldU2D4_4() { return &___U24fieldU2D4_4; }
	inline void set_U24fieldU2D4_4(U24ArrayTypeU3D328_t2366888523  value)
	{
		___U24fieldU2D4_4 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B208ed9b8U2D43e9U2D4190U2D912cU2D9dd9f5304e9bU7D_t3340151747_StaticFields, ___U24fieldU2D5_5)); }
	inline U24ArrayTypeU3D328_t2366888523  get_U24fieldU2D5_5() const { return ___U24fieldU2D5_5; }
	inline U24ArrayTypeU3D328_t2366888523 * get_address_of_U24fieldU2D5_5() { return &___U24fieldU2D5_5; }
	inline void set_U24fieldU2D5_5(U24ArrayTypeU3D328_t2366888523  value)
	{
		___U24fieldU2D5_5 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B208ed9b8U2D43e9U2D4190U2D912cU2D9dd9f5304e9bU7D_t3340151747_StaticFields, ___U24fieldU2D6_6)); }
	inline U24ArrayTypeU3D64_t214898668  get_U24fieldU2D6_6() const { return ___U24fieldU2D6_6; }
	inline U24ArrayTypeU3D64_t214898668 * get_address_of_U24fieldU2D6_6() { return &___U24fieldU2D6_6; }
	inline void set_U24fieldU2D6_6(U24ArrayTypeU3D64_t214898668  value)
	{
		___U24fieldU2D6_6 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B208ed9b8U2D43e9U2D4190U2D912cU2D9dd9f5304e9bU7D_t3340151747_StaticFields, ___U24fieldU2D7_7)); }
	inline U24ArrayTypeU3D1084_t359038729  get_U24fieldU2D7_7() const { return ___U24fieldU2D7_7; }
	inline U24ArrayTypeU3D1084_t359038729 * get_address_of_U24fieldU2D7_7() { return &___U24fieldU2D7_7; }
	inline void set_U24fieldU2D7_7(U24ArrayTypeU3D1084_t359038729  value)
	{
		___U24fieldU2D7_7 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B208ed9b8U2D43e9U2D4190U2D912cU2D9dd9f5304e9bU7D_t3340151747_StaticFields, ___U24fieldU2D8_8)); }
	inline U24ArrayTypeU3D1084_t359038729  get_U24fieldU2D8_8() const { return ___U24fieldU2D8_8; }
	inline U24ArrayTypeU3D1084_t359038729 * get_address_of_U24fieldU2D8_8() { return &___U24fieldU2D8_8; }
	inline void set_U24fieldU2D8_8(U24ArrayTypeU3D1084_t359038729  value)
	{
		___U24fieldU2D8_8 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B208ed9b8U2D43e9U2D4190U2D912cU2D9dd9f5304e9bU7D_t3340151747_StaticFields, ___U24fieldU2D9_9)); }
	inline U24ArrayTypeU3D40_t214898602  get_U24fieldU2D9_9() const { return ___U24fieldU2D9_9; }
	inline U24ArrayTypeU3D40_t214898602 * get_address_of_U24fieldU2D9_9() { return &___U24fieldU2D9_9; }
	inline void set_U24fieldU2D9_9(U24ArrayTypeU3D40_t214898602  value)
	{
		___U24fieldU2D9_9 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B208ed9b8U2D43e9U2D4190U2D912cU2D9dd9f5304e9bU7D_t3340151747_StaticFields, ___U24fieldU2DA_10)); }
	inline U24ArrayTypeU3D8_t2777878858  get_U24fieldU2DA_10() const { return ___U24fieldU2DA_10; }
	inline U24ArrayTypeU3D8_t2777878858 * get_address_of_U24fieldU2DA_10() { return &___U24fieldU2DA_10; }
	inline void set_U24fieldU2DA_10(U24ArrayTypeU3D8_t2777878858  value)
	{
		___U24fieldU2DA_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
