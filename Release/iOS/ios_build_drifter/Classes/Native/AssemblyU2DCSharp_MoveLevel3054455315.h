﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// GlobalObjectManager
struct GlobalObjectManager_t849077355;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveLevel
struct  MoveLevel_t3054455315  : public MonoBehaviour_t3012272455
{
public:
	// System.String MoveLevel::levelName
	String_t* ___levelName_2;
	// GlobalObjectManager MoveLevel::persistentData
	GlobalObjectManager_t849077355 * ___persistentData_3;

public:
	inline static int32_t get_offset_of_levelName_2() { return static_cast<int32_t>(offsetof(MoveLevel_t3054455315, ___levelName_2)); }
	inline String_t* get_levelName_2() const { return ___levelName_2; }
	inline String_t** get_address_of_levelName_2() { return &___levelName_2; }
	inline void set_levelName_2(String_t* value)
	{
		___levelName_2 = value;
		Il2CppCodeGenWriteBarrier(&___levelName_2, value);
	}

	inline static int32_t get_offset_of_persistentData_3() { return static_cast<int32_t>(offsetof(MoveLevel_t3054455315, ___persistentData_3)); }
	inline GlobalObjectManager_t849077355 * get_persistentData_3() const { return ___persistentData_3; }
	inline GlobalObjectManager_t849077355 ** get_address_of_persistentData_3() { return &___persistentData_3; }
	inline void set_persistentData_3(GlobalObjectManager_t849077355 * value)
	{
		___persistentData_3 = value;
		Il2CppCodeGenWriteBarrier(&___persistentData_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
