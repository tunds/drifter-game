﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.DataTable
struct DataTable_t2176726999;

#include "codegen/il2cpp-codegen.h"

// System.Data.DataTable System.Data.Common.DbConnection/MetaDataCollections::get_Instance()
extern "C"  DataTable_t2176726999 * MetaDataCollections_get_Instance_m3411333935 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbConnection/MetaDataCollections::.cctor()
extern "C"  void MetaDataCollections__cctor_m4007229166 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
