﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnemyMovement
struct EnemyMovement_t1797938231;

#include "codegen/il2cpp-codegen.h"

// System.Void EnemyMovement::.ctor()
extern "C"  void EnemyMovement__ctor_m1262408468 (EnemyMovement_t1797938231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyMovement::Awake()
extern "C"  void EnemyMovement_Awake_m1500013687 (EnemyMovement_t1797938231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyMovement::Update()
extern "C"  void EnemyMovement_Update_m2206818937 (EnemyMovement_t1797938231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
