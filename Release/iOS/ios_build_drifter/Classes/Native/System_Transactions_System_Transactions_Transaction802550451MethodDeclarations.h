﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Transactions.Transaction/AsyncCommit
struct AsyncCommit_t802550451;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Transactions.Transaction/AsyncCommit::.ctor(System.Object,System.IntPtr)
extern "C"  void AsyncCommit__ctor_m433227045 (AsyncCommit_t802550451 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction/AsyncCommit::Invoke()
extern "C"  void AsyncCommit_Invoke_m4102084031 (AsyncCommit_t802550451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AsyncCommit_t802550451(Il2CppObject* delegate);
// System.IAsyncResult System.Transactions.Transaction/AsyncCommit::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AsyncCommit_BeginInvoke_m3788276972 (AsyncCommit_t802550451 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction/AsyncCommit::EndInvoke(System.IAsyncResult)
extern "C"  void AsyncCommit_EndInvoke_m1956139445 (AsyncCommit_t802550451 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
