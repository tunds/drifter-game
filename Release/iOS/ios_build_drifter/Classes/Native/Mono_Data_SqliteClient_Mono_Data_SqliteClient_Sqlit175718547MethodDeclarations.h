﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.SqliteClient.SqliteExecutionException
struct SqliteExecutionException_t175718547;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Mono.Data.SqliteClient.SqliteExecutionException::.ctor()
extern "C"  void SqliteExecutionException__ctor_m3454219336 (SqliteExecutionException_t175718547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteExecutionException::.ctor(System.String)
extern "C"  void SqliteExecutionException__ctor_m2226364346 (SqliteExecutionException_t175718547 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
