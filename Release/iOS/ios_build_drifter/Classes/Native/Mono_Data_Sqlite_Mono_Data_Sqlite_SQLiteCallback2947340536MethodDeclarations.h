﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteCallback
struct SQLiteCallback_t2947340536;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void Mono.Data.Sqlite.SQLiteCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void SQLiteCallback__ctor_m1604935144 (SQLiteCallback_t2947340536 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteCallback::Invoke(System.IntPtr,System.Int32,System.IntPtr)
extern "C"  void SQLiteCallback_Invoke_m2500231059 (SQLiteCallback_t2947340536 * __this, IntPtr_t ___context0, int32_t ___nArgs1, IntPtr_t ___argsptr2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_SQLiteCallback_t2947340536(Il2CppObject* delegate, IntPtr_t ___context0, int32_t ___nArgs1, IntPtr_t ___argsptr2);
// System.IAsyncResult Mono.Data.Sqlite.SQLiteCallback::BeginInvoke(System.IntPtr,System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SQLiteCallback_BeginInvoke_m2889065476 (SQLiteCallback_t2947340536 * __this, IntPtr_t ___context0, int32_t ___nArgs1, IntPtr_t ___argsptr2, AsyncCallback_t1363551830 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteCallback::EndInvoke(System.IAsyncResult)
extern "C"  void SQLiteCallback_EndInvoke_m703071224 (SQLiteCallback_t2947340536 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
