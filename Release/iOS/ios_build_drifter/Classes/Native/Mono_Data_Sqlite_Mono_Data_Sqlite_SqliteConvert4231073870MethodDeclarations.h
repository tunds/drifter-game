﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteConvert
struct SqliteConvert_t4231073870;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Type
struct Type_t;
// Mono.Data.Sqlite.SQLiteType
struct SQLiteType_t3947843053;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteDateFormat4009100393.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteType3947843053.h"
#include "System_Data_System_Data_DbType2586775211.h"
#include "mscorlib_System_Type2779229935.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_TypeAffinity3864856329.h"

// System.Void Mono.Data.Sqlite.SqliteConvert::.ctor(Mono.Data.Sqlite.SQLiteDateFormats)
extern "C"  void SqliteConvert__ctor_m4145342011 (SqliteConvert_t4231073870 * __this, int32_t ___fmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteConvert::.cctor()
extern "C"  void SqliteConvert__cctor_m1244765593 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Data.Sqlite.SqliteConvert::ToUTF8(System.String)
extern "C"  ByteU5BU5D_t58506160* SqliteConvert_ToUTF8_m870050332 (Il2CppObject * __this /* static, unused */, String_t* ___sourceText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Data.Sqlite.SqliteConvert::ToUTF8(System.DateTime)
extern "C"  ByteU5BU5D_t58506160* SqliteConvert_ToUTF8_m2824708818 (SqliteConvert_t4231073870 * __this, DateTime_t339033936  ___dateTimeValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteConvert::ToString(System.IntPtr,System.Int32)
extern "C"  String_t* SqliteConvert_ToString_m968553090 (SqliteConvert_t4231073870 * __this, IntPtr_t ___nativestring0, int32_t ___nativestringlen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteConvert::UTF8ToString(System.IntPtr,System.Int32)
extern "C"  String_t* SqliteConvert_UTF8ToString_m34290835 (Il2CppObject * __this /* static, unused */, IntPtr_t ___nativestring0, int32_t ___nativestringlen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Data.Sqlite.SqliteConvert::ToDateTime(System.String)
extern "C"  DateTime_t339033936  SqliteConvert_ToDateTime_m1665077731 (SqliteConvert_t4231073870 * __this, String_t* ___dateText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Data.Sqlite.SqliteConvert::ToDateTime(System.Double)
extern "C"  DateTime_t339033936  SqliteConvert_ToDateTime_m1096838691 (SqliteConvert_t4231073870 * __this, double ___julianDay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Mono.Data.Sqlite.SqliteConvert::ToJulianDay(System.DateTime)
extern "C"  double SqliteConvert_ToJulianDay_m3763840317 (SqliteConvert_t4231073870 * __this, DateTime_t339033936  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteConvert::ToString(System.DateTime)
extern "C"  String_t* SqliteConvert_ToString_m3219086873 (SqliteConvert_t4231073870 * __this, DateTime_t339033936  ___dateValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Data.Sqlite.SqliteConvert::ToDateTime(System.IntPtr,System.Int32)
extern "C"  DateTime_t339033936  SqliteConvert_ToDateTime_m4234585026 (SqliteConvert_t4231073870 * __this, IntPtr_t ___ptr0, int32_t ___len1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Mono.Data.Sqlite.SqliteConvert::Split(System.String,System.Char)
extern "C"  StringU5BU5D_t2956870243* SqliteConvert_Split_m1451154830 (Il2CppObject * __this /* static, unused */, String_t* ___source0, uint16_t ___separator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteConvert::ToBoolean(System.String)
extern "C"  bool SqliteConvert_ToBoolean_m1468309317 (Il2CppObject * __this /* static, unused */, String_t* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Mono.Data.Sqlite.SqliteConvert::SQLiteTypeToType(Mono.Data.Sqlite.SQLiteType)
extern "C"  Type_t * SqliteConvert_SQLiteTypeToType_m1501963038 (Il2CppObject * __this /* static, unused */, SQLiteType_t3947843053 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DbType Mono.Data.Sqlite.SqliteConvert::TypeToDbType(System.Type)
extern "C"  int32_t SqliteConvert_TypeToDbType_m1609356846 (Il2CppObject * __this /* static, unused */, Type_t * ___typ0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteConvert::DbTypeToColumnSize(System.Data.DbType)
extern "C"  int32_t SqliteConvert_DbTypeToColumnSize_m4089362873 (Il2CppObject * __this /* static, unused */, int32_t ___typ0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteConvert::DbTypeToNumericPrecision(System.Data.DbType)
extern "C"  Il2CppObject * SqliteConvert_DbTypeToNumericPrecision_m1906391692 (Il2CppObject * __this /* static, unused */, int32_t ___typ0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteConvert::DbTypeToNumericScale(System.Data.DbType)
extern "C"  Il2CppObject * SqliteConvert_DbTypeToNumericScale_m1030600280 (Il2CppObject * __this /* static, unused */, int32_t ___typ0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteConvert::DbTypeToTypeName(System.Data.DbType)
extern "C"  String_t* SqliteConvert_DbTypeToTypeName_m1740644078 (Il2CppObject * __this /* static, unused */, int32_t ___typ0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Mono.Data.Sqlite.SqliteConvert::DbTypeToType(System.Data.DbType)
extern "C"  Type_t * SqliteConvert_DbTypeToType_m844504698 (Il2CppObject * __this /* static, unused */, int32_t ___typ0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.TypeAffinity Mono.Data.Sqlite.SqliteConvert::TypeToAffinity(System.Type)
extern "C"  int32_t SqliteConvert_TypeToAffinity_m1612605988 (Il2CppObject * __this /* static, unused */, Type_t * ___typ0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DbType Mono.Data.Sqlite.SqliteConvert::TypeNameToDbType(System.String)
extern "C"  int32_t SqliteConvert_TypeNameToDbType_m1099231468 (Il2CppObject * __this /* static, unused */, String_t* ___Name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
