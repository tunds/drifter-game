﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// door
struct door_t3089326;
// UnityEngine.Collider
struct Collider_t955670625;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"

// System.Void door::.ctor()
extern "C"  void door__ctor_m2507632546 (door_t3089326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void door::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void door_OnTriggerEnter_m3274929942 (door_t3089326 * __this, Collider_t955670625 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void door::OnTriggerExit(UnityEngine.Collider)
extern "C"  void door_OnTriggerExit_m2110870476 (door_t3089326 * __this, Collider_t955670625 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void door::Main()
extern "C"  void door_Main_m827669723 (door_t3089326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
