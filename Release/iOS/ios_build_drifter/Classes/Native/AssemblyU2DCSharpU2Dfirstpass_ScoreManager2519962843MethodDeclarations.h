﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager
struct ScoreManager_t2519962843;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager::.ctor()
extern "C"  void ScoreManager__ctor_m2997847252 (ScoreManager_t2519962843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager::Start()
extern "C"  void ScoreManager_Start_m1944985044 (ScoreManager_t2519962843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager::Update()
extern "C"  void ScoreManager_Update_m170846393 (ScoreManager_t2519962843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
