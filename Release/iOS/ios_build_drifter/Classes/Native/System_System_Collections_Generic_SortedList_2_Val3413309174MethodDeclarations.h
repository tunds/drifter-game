﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2<System.Object,System.Object>
struct SortedList_2_t2672481741;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedList_2_Val3413309174.h"

// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C"  void ValueEnumerator__ctor_m467708084_gshared (ValueEnumerator_t3413309174 * __this, SortedList_2_t2672481741 * ___l0, const MethodInfo* method);
#define ValueEnumerator__ctor_m467708084(__this, ___l0, method) ((  void (*) (ValueEnumerator_t3413309174 *, SortedList_2_t2672481741 *, const MethodInfo*))ValueEnumerator__ctor_m467708084_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void ValueEnumerator_System_Collections_IEnumerator_Reset_m3695045382_gshared (ValueEnumerator_t3413309174 * __this, const MethodInfo* method);
#define ValueEnumerator_System_Collections_IEnumerator_Reset_m3695045382(__this, method) ((  void (*) (ValueEnumerator_t3413309174 *, const MethodInfo*))ValueEnumerator_System_Collections_IEnumerator_Reset_m3695045382_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ValueEnumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * ValueEnumerator_System_Collections_IEnumerator_get_Current_m1147872828_gshared (ValueEnumerator_t3413309174 * __this, const MethodInfo* method);
#define ValueEnumerator_System_Collections_IEnumerator_get_Current_m1147872828(__this, method) ((  Il2CppObject * (*) (ValueEnumerator_t3413309174 *, const MethodInfo*))ValueEnumerator_System_Collections_IEnumerator_get_Current_m1147872828_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Object,System.Object>::Dispose()
extern "C"  void ValueEnumerator_Dispose_m2114845361_gshared (ValueEnumerator_t3413309174 * __this, const MethodInfo* method);
#define ValueEnumerator_Dispose_m2114845361(__this, method) ((  void (*) (ValueEnumerator_t3413309174 *, const MethodInfo*))ValueEnumerator_Dispose_m2114845361_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ValueEnumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool ValueEnumerator_MoveNext_m2264913354_gshared (ValueEnumerator_t3413309174 * __this, const MethodInfo* method);
#define ValueEnumerator_MoveNext_m2264913354(__this, method) ((  bool (*) (ValueEnumerator_t3413309174 *, const MethodInfo*))ValueEnumerator_MoveNext_m2264913354_gshared)(__this, method)
// TValue System.Collections.Generic.SortedList`2/ValueEnumerator<System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * ValueEnumerator_get_Current_m1637083936_gshared (ValueEnumerator_t3413309174 * __this, const MethodInfo* method);
#define ValueEnumerator_get_Current_m1637083936(__this, method) ((  Il2CppObject * (*) (ValueEnumerator_t3413309174 *, const MethodInfo*))ValueEnumerator_get_Current_m1637083936_gshared)(__this, method)
