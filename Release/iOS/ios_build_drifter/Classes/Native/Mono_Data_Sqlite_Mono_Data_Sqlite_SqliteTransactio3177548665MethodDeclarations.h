﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteTransaction
struct SqliteTransaction_t3177548665;
// Mono.Data.Sqlite.SqliteConnection
struct SqliteConnection_t3853176977;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection3853176977.h"

// System.Void Mono.Data.Sqlite.SqliteTransaction::.ctor(Mono.Data.Sqlite.SqliteConnection,System.Boolean)
extern "C"  void SqliteTransaction__ctor_m2170477159 (SqliteTransaction_t3177548665 * __this, SqliteConnection_t3853176977 * ___connection0, bool ___deferredLock1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteTransaction::Commit()
extern "C"  void SqliteTransaction_Commit_m2108112722 (SqliteTransaction_t3177548665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteConnection Mono.Data.Sqlite.SqliteTransaction::get_Connection()
extern "C"  SqliteConnection_t3853176977 * SqliteTransaction_get_Connection_m1401291746 (SqliteTransaction_t3177548665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteTransaction::Dispose(System.Boolean)
extern "C"  void SqliteTransaction_Dispose_m3056966333 (SqliteTransaction_t3177548665 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteTransaction::Rollback()
extern "C"  void SqliteTransaction_Rollback_m1467551551 (SqliteTransaction_t3177548665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteTransaction::IssueRollback(Mono.Data.Sqlite.SqliteConnection)
extern "C"  void SqliteTransaction_IssueRollback_m3574712059 (Il2CppObject * __this /* static, unused */, SqliteConnection_t3853176977 * ___cnn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteTransaction::IsValid(System.Boolean)
extern "C"  bool SqliteTransaction_IsValid_m1964190542 (SqliteTransaction_t3177548665 * __this, bool ___throwError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
