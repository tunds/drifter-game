﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteFunction
struct SqliteFunction_t3537217675;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.String
struct String_t;
// Mono.Data.Sqlite.SqliteFunction[]
struct SqliteFunctionU5BU5D_t1563239658;
// Mono.Data.Sqlite.SQLiteBase
struct SQLiteBase_t3947283844;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteBase3947283844.h"

// System.Void Mono.Data.Sqlite.SqliteFunction::.cctor()
extern "C"  void SqliteFunction__cctor_m3366218950 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteFunction::Invoke(System.Object[])
extern "C"  Il2CppObject * SqliteFunction_Invoke_m1146720882 (SqliteFunction_t3537217675 * __this, ObjectU5BU5D_t11523773* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFunction::Step(System.Object[],System.Int32,System.Object&)
extern "C"  void SqliteFunction_Step_m1133056432 (SqliteFunction_t3537217675 * __this, ObjectU5BU5D_t11523773* ___args0, int32_t ___stepNumber1, Il2CppObject ** ___contextData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteFunction::Final(System.Object)
extern "C"  Il2CppObject * SqliteFunction_Final_m2653275460 (SqliteFunction_t3537217675 * __this, Il2CppObject * ___contextData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteFunction::Compare(System.String,System.String)
extern "C"  int32_t SqliteFunction_Compare_m3631589208 (SqliteFunction_t3537217675 * __this, String_t* ___param10, String_t* ___param21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] Mono.Data.Sqlite.SqliteFunction::ConvertParams(System.Int32,System.IntPtr)
extern "C"  ObjectU5BU5D_t11523773* SqliteFunction_ConvertParams_m130988400 (SqliteFunction_t3537217675 * __this, int32_t ___nArgs0, IntPtr_t ___argsptr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFunction::SetReturnValue(System.IntPtr,System.Object)
extern "C"  void SqliteFunction_SetReturnValue_m1351424102 (SqliteFunction_t3537217675 * __this, IntPtr_t ___context0, Il2CppObject * ___returnValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFunction::ScalarCallback(System.IntPtr,System.Int32,System.IntPtr)
extern "C"  void SqliteFunction_ScalarCallback_m17571071 (SqliteFunction_t3537217675 * __this, IntPtr_t ___context0, int32_t ___nArgs1, IntPtr_t ___argsptr2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteFunction::CompareCallback(System.IntPtr,System.Int32,System.IntPtr,System.Int32,System.IntPtr)
extern "C"  int32_t SqliteFunction_CompareCallback_m3792783241 (SqliteFunction_t3537217675 * __this, IntPtr_t ___ptr0, int32_t ___len11, IntPtr_t ___ptr12, int32_t ___len23, IntPtr_t ___ptr24, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteFunction::CompareCallback16(System.IntPtr,System.Int32,System.IntPtr,System.Int32,System.IntPtr)
extern "C"  int32_t SqliteFunction_CompareCallback16_m604656388 (SqliteFunction_t3537217675 * __this, IntPtr_t ___ptr0, int32_t ___len11, IntPtr_t ___ptr12, int32_t ___len23, IntPtr_t ___ptr24, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFunction::StepCallback(System.IntPtr,System.Int32,System.IntPtr)
extern "C"  void SqliteFunction_StepCallback_m942605023 (SqliteFunction_t3537217675 * __this, IntPtr_t ___context0, int32_t ___nArgs1, IntPtr_t ___argsptr2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFunction::FinalCallback(System.IntPtr)
extern "C"  void SqliteFunction_FinalCallback_m2347198036 (SqliteFunction_t3537217675 * __this, IntPtr_t ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFunction::Dispose(System.Boolean)
extern "C"  void SqliteFunction_Dispose_m2411322555 (SqliteFunction_t3537217675 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFunction::Dispose()
extern "C"  void SqliteFunction_Dispose_m3231936068 (SqliteFunction_t3537217675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteFunction[] Mono.Data.Sqlite.SqliteFunction::BindFunctions(Mono.Data.Sqlite.SQLiteBase)
extern "C"  SqliteFunctionU5BU5D_t1563239658* SqliteFunction_BindFunctions_m2614589765 (Il2CppObject * __this /* static, unused */, SQLiteBase_t3947283844 * ___sqlbase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
