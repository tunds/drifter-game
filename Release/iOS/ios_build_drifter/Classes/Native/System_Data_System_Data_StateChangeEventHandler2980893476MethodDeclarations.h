﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.StateChangeEventHandler
struct StateChangeEventHandler_t2980893476;
// System.Object
struct Il2CppObject;
// System.Data.StateChangeEventArgs
struct StateChangeEventArgs_t685302217;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_Data_System_Data_StateChangeEventArgs685302217.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Data.StateChangeEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void StateChangeEventHandler__ctor_m2492439134 (StateChangeEventHandler_t2980893476 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.StateChangeEventHandler::Invoke(System.Object,System.Data.StateChangeEventArgs)
extern "C"  void StateChangeEventHandler_Invoke_m127947311 (StateChangeEventHandler_t2980893476 * __this, Il2CppObject * ___sender0, StateChangeEventArgs_t685302217 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_StateChangeEventHandler_t2980893476(Il2CppObject* delegate, Il2CppObject * ___sender0, StateChangeEventArgs_t685302217 * ___e1);
// System.IAsyncResult System.Data.StateChangeEventHandler::BeginInvoke(System.Object,System.Data.StateChangeEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StateChangeEventHandler_BeginInvoke_m3658269396 (StateChangeEventHandler_t2980893476 * __this, Il2CppObject * ___sender0, StateChangeEventArgs_t685302217 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.StateChangeEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void StateChangeEventHandler_EndInvoke_m169536366 (StateChangeEventHandler_t2980893476 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
