﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PoqXert.MessageBox.MSGBoxStyle>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m954945661(__this, ___l0, method) ((  void (*) (Enumerator_t806128347 *, List_1_t2720345355 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m262711733(__this, method) ((  void (*) (Enumerator_t806128347 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4093118507(__this, method) ((  Il2CppObject * (*) (Enumerator_t806128347 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PoqXert.MessageBox.MSGBoxStyle>::Dispose()
#define Enumerator_Dispose_m499256738(__this, method) ((  void (*) (Enumerator_t806128347 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PoqXert.MessageBox.MSGBoxStyle>::VerifyState()
#define Enumerator_VerifyState_m2517773275(__this, method) ((  void (*) (Enumerator_t806128347 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PoqXert.MessageBox.MSGBoxStyle>::MoveNext()
#define Enumerator_MoveNext_m1169271141(__this, method) ((  bool (*) (Enumerator_t806128347 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PoqXert.MessageBox.MSGBoxStyle>::get_Current()
#define Enumerator_get_Current_m2786881972(__this, method) ((  MSGBoxStyle_t1923386386 * (*) (Enumerator_t806128347 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
