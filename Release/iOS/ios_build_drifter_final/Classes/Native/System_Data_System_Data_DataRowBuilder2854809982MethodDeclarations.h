﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.DataRowBuilder
struct DataRowBuilder_t2854809982;
// System.Data.DataTable
struct DataTable_t2176726999;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_DataTable2176726999.h"

// System.Void System.Data.DataRowBuilder::.ctor(System.Data.DataTable,System.Int32,System.Int32)
extern "C"  void DataRowBuilder__ctor_m3645778221 (DataRowBuilder_t2854809982 * __this, DataTable_t2176726999 * ___table0, int32_t ___rowID1, int32_t ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable System.Data.DataRowBuilder::get_Table()
extern "C"  DataTable_t2176726999 * DataRowBuilder_get_Table_m1528205397 (DataRowBuilder_t2854809982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
