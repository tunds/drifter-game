﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.SqliteClient.SqliteParameterCollection
struct SqliteParameterCollection_t3807043211;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Data.Common.DbParameter
struct DbParameter_t3306161371;
// System.Array
struct Il2CppArray;
// Mono.Data.SqliteClient.SqliteParameter
struct SqliteParameter_t2628453389;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_String968488902.h"
#include "System_Data_System_Data_Common_DbParameter3306161371.h"
#include "mscorlib_System_Array2840145358.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli2628453389.h"

// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::.ctor()
extern "C"  void SqliteParameterCollection__ctor_m2482897600 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::CheckSqliteParam(System.Object)
extern "C"  void SqliteParameterCollection_CheckSqliteParam_m2862447415 (SqliteParameterCollection_t3807043211 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::RecreateNamedHash()
extern "C"  void SqliteParameterCollection_RecreateNamedHash_m107150870 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.SqliteClient.SqliteParameterCollection::GenerateParameterName()
extern "C"  String_t* SqliteParameterCollection_GenerateParameterName_m2602227002 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::isPrefixed(System.String)
extern "C"  bool SqliteParameterCollection_isPrefixed_m3917059723 (SqliteParameterCollection_t3807043211 * __this, String_t* ___parameterName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameter Mono.Data.SqliteClient.SqliteParameterCollection::GetParameter(System.Int32)
extern "C"  DbParameter_t3306161371 * SqliteParameterCollection_GetParameter_m4088518556 (SqliteParameterCollection_t3807043211 * __this, int32_t ___parameterIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::SetParameter(System.Int32,System.Data.Common.DbParameter)
extern "C"  void SqliteParameterCollection_SetParameter_m2753962653 (SqliteParameterCollection_t3807043211 * __this, int32_t ___parameterIndex0, DbParameter_t3306161371 * ___parameter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::get_Count()
extern "C"  int32_t SqliteParameterCollection_get_Count_m3376269234 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::get_IsFixedSize()
extern "C"  bool SqliteParameterCollection_get_IsFixedSize_m1792392136 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::get_IsReadOnly()
extern "C"  bool SqliteParameterCollection_get_IsReadOnly_m3282774577 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::get_IsSynchronized()
extern "C"  bool SqliteParameterCollection_get_IsSynchronized_m763380067 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.SqliteClient.SqliteParameterCollection::get_SyncRoot()
extern "C"  Il2CppObject * SqliteParameterCollection_get_SyncRoot_m2711312415 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::AddRange(System.Array)
extern "C"  void SqliteParameterCollection_AddRange_m1609136646 (SqliteParameterCollection_t3807043211 * __this, Il2CppArray * ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::Add(System.Object)
extern "C"  int32_t SqliteParameterCollection_Add_m2982982599 (SqliteParameterCollection_t3807043211 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::Clear()
extern "C"  void SqliteParameterCollection_Clear_m4183998187 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::CopyTo(System.Array,System.Int32)
extern "C"  void SqliteParameterCollection_CopyTo_m3803708317 (SqliteParameterCollection_t3807043211 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::Contains(System.Object)
extern "C"  bool SqliteParameterCollection_Contains_m3374294425 (SqliteParameterCollection_t3807043211 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::Contains(System.String)
extern "C"  bool SqliteParameterCollection_Contains_m3152184071 (SqliteParameterCollection_t3807043211 * __this, String_t* ___parameterName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::Contains(Mono.Data.SqliteClient.SqliteParameter)
extern "C"  bool SqliteParameterCollection_Contains_m2896464572 (SqliteParameterCollection_t3807043211 * __this, SqliteParameter_t2628453389 * ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Mono.Data.SqliteClient.SqliteParameterCollection::GetEnumerator()
extern "C"  Il2CppObject * SqliteParameterCollection_GetEnumerator_m2279596428 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::IndexOf(System.Object)
extern "C"  int32_t SqliteParameterCollection_IndexOf_m2154802847 (SqliteParameterCollection_t3807043211 * __this, Il2CppObject * ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::IndexOf(System.String)
extern "C"  int32_t SqliteParameterCollection_IndexOf_m1932692493 (SqliteParameterCollection_t3807043211 * __this, String_t* ___parameterName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::IndexOf(Mono.Data.SqliteClient.SqliteParameter)
extern "C"  int32_t SqliteParameterCollection_IndexOf_m2119734390 (SqliteParameterCollection_t3807043211 * __this, SqliteParameter_t2628453389 * ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::Insert(System.Int32,System.Object)
extern "C"  void SqliteParameterCollection_Insert_m3898561116 (SqliteParameterCollection_t3807043211 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::Remove(System.Object)
extern "C"  void SqliteParameterCollection_Remove_m2293346636 (SqliteParameterCollection_t3807043211 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::RemoveAt(System.Int32)
extern "C"  void SqliteParameterCollection_RemoveAt_m3959624684 (SqliteParameterCollection_t3807043211 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::RemoveAt(System.String)
extern "C"  void SqliteParameterCollection_RemoveAt_m2945979335 (SqliteParameterCollection_t3807043211 * __this, String_t* ___parameterName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::RemoveAt(Mono.Data.SqliteClient.SqliteParameter)
extern "C"  void SqliteParameterCollection_RemoveAt_m2152681980 (SqliteParameterCollection_t3807043211 * __this, SqliteParameter_t2628453389 * ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
