﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbCommand
struct DbCommand_t2323745021;
// System.Data.IDbConnection
struct IDbConnection_t2001212056;
// System.Data.IDataReader
struct IDataReader_t1445897193;
// System.Data.Common.DbConnection
struct DbConnection_t462757452;
// System.Data.Common.DbParameterCollection
struct DbParameterCollection_t3381130713;
// System.Data.Common.DbTransaction
struct DbTransaction_t4162579344;
// System.Data.Common.DbParameter
struct DbParameter_t3306161371;
// System.Data.Common.DbDataReader
struct DbDataReader_t2472406139;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_CommandBehavior326612048.h"
#include "System_Data_System_Data_Common_DbConnection462757452.h"
#include "System_Data_System_Data_Common_DbTransaction4162579344.h"

// System.Void System.Data.Common.DbCommand::.ctor()
extern "C"  void DbCommand__ctor_m2980138567 (DbCommand_t2323745021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.IDbConnection System.Data.Common.DbCommand::System.Data.IDbCommand.get_Connection()
extern "C"  Il2CppObject * DbCommand_System_Data_IDbCommand_get_Connection_m2431395582 (DbCommand_t2323745021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.IDataReader System.Data.Common.DbCommand::System.Data.IDbCommand.ExecuteReader()
extern "C"  Il2CppObject * DbCommand_System_Data_IDbCommand_ExecuteReader_m3946225426 (DbCommand_t2323745021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.IDataReader System.Data.Common.DbCommand::System.Data.IDbCommand.ExecuteReader(System.Data.CommandBehavior)
extern "C"  Il2CppObject * DbCommand_System_Data_IDbCommand_ExecuteReader_m3123607528 (DbCommand_t2323745021 * __this, int32_t ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbConnection System.Data.Common.DbCommand::get_Connection()
extern "C"  DbConnection_t462757452 * DbCommand_get_Connection_m503343133 (DbCommand_t2323745021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommand::set_Connection(System.Data.Common.DbConnection)
extern "C"  void DbCommand_set_Connection_m9416384 (DbCommand_t2323745021 * __this, DbConnection_t462757452 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameterCollection System.Data.Common.DbCommand::get_Parameters()
extern "C"  DbParameterCollection_t3381130713 * DbCommand_get_Parameters_m3316044920 (DbCommand_t2323745021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbTransaction System.Data.Common.DbCommand::get_Transaction()
extern "C"  DbTransaction_t4162579344 * DbCommand_get_Transaction_m1000654761 (DbCommand_t2323745021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommand::set_Transaction(System.Data.Common.DbTransaction)
extern "C"  void DbCommand_set_Transaction_m56756068 (DbCommand_t2323745021 * __this, DbTransaction_t4162579344 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameter System.Data.Common.DbCommand::CreateParameter()
extern "C"  DbParameter_t3306161371 * DbCommand_CreateParameter_m2747250700 (DbCommand_t2323745021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbDataReader System.Data.Common.DbCommand::ExecuteReader()
extern "C"  DbDataReader_t2472406139 * DbCommand_ExecuteReader_m3074786163 (DbCommand_t2323745021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbDataReader System.Data.Common.DbCommand::ExecuteReader(System.Data.CommandBehavior)
extern "C"  DbDataReader_t2472406139 * DbCommand_ExecuteReader_m1059380967 (DbCommand_t2323745021 * __this, int32_t ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
