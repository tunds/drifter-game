﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLiter.LoomManager
struct LoomManager_t3471199758;
// SQLiter.LoomManager/ILoom
struct ILoom_t69791370;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLiter.LoomManager::.ctor()
extern "C"  void LoomManager__ctor_m1310867251 (LoomManager_t3471199758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.LoomManager::.cctor()
extern "C"  void LoomManager__cctor_m1500082906 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLiter.LoomManager/ILoom SQLiter.LoomManager::get_Loom()
extern "C"  Il2CppObject * LoomManager_get_Loom_m2426461095 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.LoomManager::Awake()
extern "C"  void LoomManager_Awake_m1548472470 (LoomManager_t3471199758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.LoomManager::OnDestroy()
extern "C"  void LoomManager_OnDestroy_m2005615276 (LoomManager_t3471199758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.LoomManager::Update()
extern "C"  void LoomManager_Update_m3709041210 (LoomManager_t3471199758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
