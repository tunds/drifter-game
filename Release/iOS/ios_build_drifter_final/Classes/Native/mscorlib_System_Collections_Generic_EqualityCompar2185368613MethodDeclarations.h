﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int64>
struct DefaultComparer_t2185368613;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int64>::.ctor()
extern "C"  void DefaultComparer__ctor_m2613347418_gshared (DefaultComparer_t2185368613 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2613347418(__this, method) ((  void (*) (DefaultComparer_t2185368613 *, const MethodInfo*))DefaultComparer__ctor_m2613347418_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int64>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2452294489_gshared (DefaultComparer_t2185368613 * __this, int64_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2452294489(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2185368613 *, int64_t, const MethodInfo*))DefaultComparer_GetHashCode_m2452294489_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int64>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m710460527_gshared (DefaultComparer_t2185368613 * __this, int64_t ___x0, int64_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m710460527(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2185368613 *, int64_t, int64_t, const MethodInfo*))DefaultComparer_Equals_m710460527_gshared)(__this, ___x0, ___y1, method)
