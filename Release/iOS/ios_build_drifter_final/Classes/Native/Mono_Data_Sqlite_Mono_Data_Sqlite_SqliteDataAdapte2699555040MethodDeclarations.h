﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteDataAdapter
struct SqliteDataAdapter_t2699555040;
// System.EventHandler`1<System.Data.Common.RowUpdatingEventArgs>
struct EventHandler_1_t3629058674;
// System.Delegate
struct Delegate_t3660574010;
// System.MulticastDelegate
struct MulticastDelegate_t2585444626;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_MulticastDelegate2585444626.h"

// System.Void Mono.Data.Sqlite.SqliteDataAdapter::.ctor()
extern "C"  void SqliteDataAdapter__ctor_m3544324162 (SqliteDataAdapter_t2699555040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteDataAdapter::.cctor()
extern "C"  void SqliteDataAdapter__cctor_m2017770411 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteDataAdapter::add_RowUpdating(System.EventHandler`1<System.Data.Common.RowUpdatingEventArgs>)
extern "C"  void SqliteDataAdapter_add_RowUpdating_m2309816893 (SqliteDataAdapter_t2699555040 * __this, EventHandler_1_t3629058674 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteDataAdapter::remove_RowUpdating(System.EventHandler`1<System.Data.Common.RowUpdatingEventArgs>)
extern "C"  void SqliteDataAdapter_remove_RowUpdating_m977557058 (SqliteDataAdapter_t2699555040 * __this, EventHandler_1_t3629058674 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate Mono.Data.Sqlite.SqliteDataAdapter::FindBuilder(System.MulticastDelegate)
extern "C"  Delegate_t3660574010 * SqliteDataAdapter_FindBuilder_m542877589 (Il2CppObject * __this /* static, unused */, MulticastDelegate_t2585444626 * ___mcd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
