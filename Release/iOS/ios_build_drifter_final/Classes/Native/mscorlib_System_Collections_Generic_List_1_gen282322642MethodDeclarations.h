﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct List_1_t282322642;
// System.Collections.Generic.IEnumerable`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct IEnumerable_1_t2357518029;
// System.Collections.Generic.IEnumerator`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct IEnumerator_1_t968470121;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct ICollection_1_t4246162355;
// System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct ReadOnlyCollection_1_t2648509021;
// SQLiteDatabase.SQLiteDB/DB_Field[]
struct DB_FieldU5BU5D_t1717179044;
// System.Predicate`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct Predicate_1_t56327571;
// System.Collections.Generic.IComparer`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct IComparer_1_t2185071082;
// System.Comparison`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct Comparison_1_t2189038549;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Field3780330969.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2663072930.h"

// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::.ctor()
extern "C"  void List_1__ctor_m3759833351_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1__ctor_m3759833351(__this, method) ((  void (*) (List_1_t282322642 *, const MethodInfo*))List_1__ctor_m3759833351_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m921216116_gshared (List_1_t282322642 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m921216116(__this, ___collection0, method) ((  void (*) (List_1_t282322642 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m921216116_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m4067387388_gshared (List_1_t282322642 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m4067387388(__this, ___capacity0, method) ((  void (*) (List_1_t282322642 *, int32_t, const MethodInfo*))List_1__ctor_m4067387388_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::.cctor()
extern "C"  void List_1__cctor_m1729566178_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1729566178(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1729566178_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2650825333_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2650825333(__this, method) ((  Il2CppObject* (*) (List_1_t282322642 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2650825333_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1281377401_gshared (List_1_t282322642 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1281377401(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t282322642 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1281377401_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2566316232_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2566316232(__this, method) ((  Il2CppObject * (*) (List_1_t282322642 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m2566316232_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2608822901_gshared (List_1_t282322642 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2608822901(__this, ___item0, method) ((  int32_t (*) (List_1_t282322642 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2608822901_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m4037371371_gshared (List_1_t282322642 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m4037371371(__this, ___item0, method) ((  bool (*) (List_1_t282322642 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m4037371371_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m820211277_gshared (List_1_t282322642 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m820211277(__this, ___item0, method) ((  int32_t (*) (List_1_t282322642 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m820211277_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1572476992_gshared (List_1_t282322642 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1572476992(__this, ___index0, ___item1, method) ((  void (*) (List_1_t282322642 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1572476992_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m56043752_gshared (List_1_t282322642 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m56043752(__this, ___item0, method) ((  void (*) (List_1_t282322642 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m56043752_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2928094252_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2928094252(__this, method) ((  bool (*) (List_1_t282322642 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2928094252_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1061332369_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1061332369(__this, method) ((  bool (*) (List_1_t282322642 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1061332369_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1362425539_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1362425539(__this, method) ((  Il2CppObject * (*) (List_1_t282322642 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1362425539_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m410952090_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m410952090(__this, method) ((  bool (*) (List_1_t282322642 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m410952090_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3376759327_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3376759327(__this, method) ((  bool (*) (List_1_t282322642 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3376759327_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m391529354_gshared (List_1_t282322642 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m391529354(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t282322642 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m391529354_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m4230820887_gshared (List_1_t282322642 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m4230820887(__this, ___index0, ___value1, method) ((  void (*) (List_1_t282322642 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m4230820887_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::Add(T)
extern "C"  void List_1_Add_m372331508_gshared (List_1_t282322642 * __this, DB_Field_t3780330969  ___item0, const MethodInfo* method);
#define List_1_Add_m372331508(__this, ___item0, method) ((  void (*) (List_1_t282322642 *, DB_Field_t3780330969 , const MethodInfo*))List_1_Add_m372331508_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1489436719_gshared (List_1_t282322642 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1489436719(__this, ___newCount0, method) ((  void (*) (List_1_t282322642 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1489436719_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3293764397_gshared (List_1_t282322642 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3293764397(__this, ___collection0, method) ((  void (*) (List_1_t282322642 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3293764397_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2554736621_gshared (List_1_t282322642 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2554736621(__this, ___enumerable0, method) ((  void (*) (List_1_t282322642 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2554736621_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3159211658_gshared (List_1_t282322642 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3159211658(__this, ___collection0, method) ((  void (*) (List_1_t282322642 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3159211658_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2648509021 * List_1_AsReadOnly_m2348402491_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2348402491(__this, method) ((  ReadOnlyCollection_1_t2648509021 * (*) (List_1_t282322642 *, const MethodInfo*))List_1_AsReadOnly_m2348402491_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::Clear()
extern "C"  void List_1_Clear_m664065878_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_Clear_m664065878(__this, method) ((  void (*) (List_1_t282322642 *, const MethodInfo*))List_1_Clear_m664065878_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::Contains(T)
extern "C"  bool List_1_Contains_m715859016_gshared (List_1_t282322642 * __this, DB_Field_t3780330969  ___item0, const MethodInfo* method);
#define List_1_Contains_m715859016(__this, ___item0, method) ((  bool (*) (List_1_t282322642 *, DB_Field_t3780330969 , const MethodInfo*))List_1_Contains_m715859016_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m1463624915_gshared (List_1_t282322642 * __this, DB_FieldU5BU5D_t1717179044* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m1463624915(__this, ___array0, method) ((  void (*) (List_1_t282322642 *, DB_FieldU5BU5D_t1717179044*, const MethodInfo*))List_1_CopyTo_m1463624915_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1563272932_gshared (List_1_t282322642 * __this, DB_FieldU5BU5D_t1717179044* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1563272932(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t282322642 *, DB_FieldU5BU5D_t1717179044*, int32_t, const MethodInfo*))List_1_CopyTo_m1563272932_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::Find(System.Predicate`1<T>)
extern "C"  DB_Field_t3780330969  List_1_Find_m264683938_gshared (List_1_t282322642 * __this, Predicate_1_t56327571 * ___match0, const MethodInfo* method);
#define List_1_Find_m264683938(__this, ___match0, method) ((  DB_Field_t3780330969  (*) (List_1_t282322642 *, Predicate_1_t56327571 *, const MethodInfo*))List_1_Find_m264683938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3296196735_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t56327571 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3296196735(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t56327571 *, const MethodInfo*))List_1_CheckMatch_m3296196735_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2635015004_gshared (List_1_t282322642 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t56327571 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2635015004(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t282322642 *, int32_t, int32_t, Predicate_1_t56327571 *, const MethodInfo*))List_1_GetIndex_m2635015004_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::GetEnumerator()
extern "C"  Enumerator_t2663072930  List_1_GetEnumerator_m1527873925_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1527873925(__this, method) ((  Enumerator_t2663072930  (*) (List_1_t282322642 *, const MethodInfo*))List_1_GetEnumerator_m1527873925_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2780705200_gshared (List_1_t282322642 * __this, DB_Field_t3780330969  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2780705200(__this, ___item0, method) ((  int32_t (*) (List_1_t282322642 *, DB_Field_t3780330969 , const MethodInfo*))List_1_IndexOf_m2780705200_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3111278907_gshared (List_1_t282322642 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3111278907(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t282322642 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3111278907_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1309640116_gshared (List_1_t282322642 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1309640116(__this, ___index0, method) ((  void (*) (List_1_t282322642 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1309640116_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3339724379_gshared (List_1_t282322642 * __this, int32_t ___index0, DB_Field_t3780330969  ___item1, const MethodInfo* method);
#define List_1_Insert_m3339724379(__this, ___index0, ___item1, method) ((  void (*) (List_1_t282322642 *, int32_t, DB_Field_t3780330969 , const MethodInfo*))List_1_Insert_m3339724379_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m443356816_gshared (List_1_t282322642 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m443356816(__this, ___collection0, method) ((  void (*) (List_1_t282322642 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m443356816_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::Remove(T)
extern "C"  bool List_1_Remove_m862737987_gshared (List_1_t282322642 * __this, DB_Field_t3780330969  ___item0, const MethodInfo* method);
#define List_1_Remove_m862737987(__this, ___item0, method) ((  bool (*) (List_1_t282322642 *, DB_Field_t3780330969 , const MethodInfo*))List_1_Remove_m862737987_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1623692659_gshared (List_1_t282322642 * __this, Predicate_1_t56327571 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1623692659(__this, ___match0, method) ((  int32_t (*) (List_1_t282322642 *, Predicate_1_t56327571 *, const MethodInfo*))List_1_RemoveAll_m1623692659_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1213577249_gshared (List_1_t282322642 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1213577249(__this, ___index0, method) ((  void (*) (List_1_t282322642 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1213577249_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::Reverse()
extern "C"  void List_1_Reverse_m4174835883_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_Reverse_m4174835883(__this, method) ((  void (*) (List_1_t282322642 *, const MethodInfo*))List_1_Reverse_m4174835883_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::Sort()
extern "C"  void List_1_Sort_m898303703_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_Sort_m898303703(__this, method) ((  void (*) (List_1_t282322642 *, const MethodInfo*))List_1_Sort_m898303703_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1740152045_gshared (List_1_t282322642 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1740152045(__this, ___comparer0, method) ((  void (*) (List_1_t282322642 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1740152045_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3457173482_gshared (List_1_t282322642 * __this, Comparison_1_t2189038549 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3457173482(__this, ___comparison0, method) ((  void (*) (List_1_t282322642 *, Comparison_1_t2189038549 *, const MethodInfo*))List_1_Sort_m3457173482_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::ToArray()
extern "C"  DB_FieldU5BU5D_t1717179044* List_1_ToArray_m3648821316_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_ToArray_m3648821316(__this, method) ((  DB_FieldU5BU5D_t1717179044* (*) (List_1_t282322642 *, const MethodInfo*))List_1_ToArray_m3648821316_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1547266352_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1547266352(__this, method) ((  void (*) (List_1_t282322642 *, const MethodInfo*))List_1_TrimExcess_m1547266352_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m856543712_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m856543712(__this, method) ((  int32_t (*) (List_1_t282322642 *, const MethodInfo*))List_1_get_Capacity_m856543712_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2994592833_gshared (List_1_t282322642 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2994592833(__this, ___value0, method) ((  void (*) (List_1_t282322642 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2994592833_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Count()
extern "C"  int32_t List_1_get_Count_m119822795_gshared (List_1_t282322642 * __this, const MethodInfo* method);
#define List_1_get_Count_m119822795(__this, method) ((  int32_t (*) (List_1_t282322642 *, const MethodInfo*))List_1_get_Count_m119822795_gshared)(__this, method)
// T System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Item(System.Int32)
extern "C"  DB_Field_t3780330969  List_1_get_Item_m1451604935_gshared (List_1_t282322642 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1451604935(__this, ___index0, method) ((  DB_Field_t3780330969  (*) (List_1_t282322642 *, int32_t, const MethodInfo*))List_1_get_Item_m1451604935_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2944154738_gshared (List_1_t282322642 * __this, int32_t ___index0, DB_Field_t3780330969  ___value1, const MethodInfo* method);
#define List_1_set_Item_m2944154738(__this, ___index0, ___value1, method) ((  void (*) (List_1_t282322642 *, int32_t, DB_Field_t3780330969 , const MethodInfo*))List_1_set_Item_m2944154738_gshared)(__this, ___index0, ___value1, method)
