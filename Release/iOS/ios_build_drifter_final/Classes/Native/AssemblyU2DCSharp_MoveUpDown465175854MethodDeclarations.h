﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveUpDown
struct MoveUpDown_t465175854;

#include "codegen/il2cpp-codegen.h"

// System.Void MoveUpDown::.ctor()
extern "C"  void MoveUpDown__ctor_m1706657581 (MoveUpDown_t465175854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveUpDown::Start()
extern "C"  void MoveUpDown_Start_m653795373 (MoveUpDown_t465175854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveUpDown::Update()
extern "C"  void MoveUpDown_Update_m3093639552 (MoveUpDown_t465175854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
