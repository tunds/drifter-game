﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnemyManager
struct EnemyManager_t1035150117;

#include "codegen/il2cpp-codegen.h"

// System.Void EnemyManager::.ctor()
extern "C"  void EnemyManager__ctor_m1552517398 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::Start()
extern "C"  void EnemyManager_Start_m499655190 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::SpawnItem()
extern "C"  void EnemyManager_SpawnItem_m261633634 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
