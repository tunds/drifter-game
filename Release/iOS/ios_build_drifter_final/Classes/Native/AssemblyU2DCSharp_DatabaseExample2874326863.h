﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SQLiteDatabase.SQLiteDB
struct SQLiteDB_t557922535;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DatabaseExample
struct  DatabaseExample_t2874326863  : public MonoBehaviour_t3012272455
{
public:
	// SQLiteDatabase.SQLiteDB DatabaseExample::db
	SQLiteDB_t557922535 * ___db_2;
	// System.Collections.Generic.List`1<System.String> DatabaseExample::allIDs
	List_1_t1765447871 * ___allIDs_3;
	// System.Collections.Generic.List`1<System.String> DatabaseExample::allNames
	List_1_t1765447871 * ___allNames_4;
	// System.String DatabaseExample::_id
	String_t* ____id_5;
	// System.String DatabaseExample::_name
	String_t* ____name_6;
	// UnityEngine.Vector2 DatabaseExample::scrollPos
	Vector2_t3525329788  ___scrollPos_7;
	// System.String DatabaseExample::btnName_SaveUpdate
	String_t* ___btnName_SaveUpdate_8;
	// System.String DatabaseExample::btnName_CreateDeleteTable
	String_t* ___btnName_CreateDeleteTable_9;
	// System.Boolean DatabaseExample::isTableCreated
	bool ___isTableCreated_10;

public:
	inline static int32_t get_offset_of_db_2() { return static_cast<int32_t>(offsetof(DatabaseExample_t2874326863, ___db_2)); }
	inline SQLiteDB_t557922535 * get_db_2() const { return ___db_2; }
	inline SQLiteDB_t557922535 ** get_address_of_db_2() { return &___db_2; }
	inline void set_db_2(SQLiteDB_t557922535 * value)
	{
		___db_2 = value;
		Il2CppCodeGenWriteBarrier(&___db_2, value);
	}

	inline static int32_t get_offset_of_allIDs_3() { return static_cast<int32_t>(offsetof(DatabaseExample_t2874326863, ___allIDs_3)); }
	inline List_1_t1765447871 * get_allIDs_3() const { return ___allIDs_3; }
	inline List_1_t1765447871 ** get_address_of_allIDs_3() { return &___allIDs_3; }
	inline void set_allIDs_3(List_1_t1765447871 * value)
	{
		___allIDs_3 = value;
		Il2CppCodeGenWriteBarrier(&___allIDs_3, value);
	}

	inline static int32_t get_offset_of_allNames_4() { return static_cast<int32_t>(offsetof(DatabaseExample_t2874326863, ___allNames_4)); }
	inline List_1_t1765447871 * get_allNames_4() const { return ___allNames_4; }
	inline List_1_t1765447871 ** get_address_of_allNames_4() { return &___allNames_4; }
	inline void set_allNames_4(List_1_t1765447871 * value)
	{
		___allNames_4 = value;
		Il2CppCodeGenWriteBarrier(&___allNames_4, value);
	}

	inline static int32_t get_offset_of__id_5() { return static_cast<int32_t>(offsetof(DatabaseExample_t2874326863, ____id_5)); }
	inline String_t* get__id_5() const { return ____id_5; }
	inline String_t** get_address_of__id_5() { return &____id_5; }
	inline void set__id_5(String_t* value)
	{
		____id_5 = value;
		Il2CppCodeGenWriteBarrier(&____id_5, value);
	}

	inline static int32_t get_offset_of__name_6() { return static_cast<int32_t>(offsetof(DatabaseExample_t2874326863, ____name_6)); }
	inline String_t* get__name_6() const { return ____name_6; }
	inline String_t** get_address_of__name_6() { return &____name_6; }
	inline void set__name_6(String_t* value)
	{
		____name_6 = value;
		Il2CppCodeGenWriteBarrier(&____name_6, value);
	}

	inline static int32_t get_offset_of_scrollPos_7() { return static_cast<int32_t>(offsetof(DatabaseExample_t2874326863, ___scrollPos_7)); }
	inline Vector2_t3525329788  get_scrollPos_7() const { return ___scrollPos_7; }
	inline Vector2_t3525329788 * get_address_of_scrollPos_7() { return &___scrollPos_7; }
	inline void set_scrollPos_7(Vector2_t3525329788  value)
	{
		___scrollPos_7 = value;
	}

	inline static int32_t get_offset_of_btnName_SaveUpdate_8() { return static_cast<int32_t>(offsetof(DatabaseExample_t2874326863, ___btnName_SaveUpdate_8)); }
	inline String_t* get_btnName_SaveUpdate_8() const { return ___btnName_SaveUpdate_8; }
	inline String_t** get_address_of_btnName_SaveUpdate_8() { return &___btnName_SaveUpdate_8; }
	inline void set_btnName_SaveUpdate_8(String_t* value)
	{
		___btnName_SaveUpdate_8 = value;
		Il2CppCodeGenWriteBarrier(&___btnName_SaveUpdate_8, value);
	}

	inline static int32_t get_offset_of_btnName_CreateDeleteTable_9() { return static_cast<int32_t>(offsetof(DatabaseExample_t2874326863, ___btnName_CreateDeleteTable_9)); }
	inline String_t* get_btnName_CreateDeleteTable_9() const { return ___btnName_CreateDeleteTable_9; }
	inline String_t** get_address_of_btnName_CreateDeleteTable_9() { return &___btnName_CreateDeleteTable_9; }
	inline void set_btnName_CreateDeleteTable_9(String_t* value)
	{
		___btnName_CreateDeleteTable_9 = value;
		Il2CppCodeGenWriteBarrier(&___btnName_CreateDeleteTable_9, value);
	}

	inline static int32_t get_offset_of_isTableCreated_10() { return static_cast<int32_t>(offsetof(DatabaseExample_t2874326863, ___isTableCreated_10)); }
	inline bool get_isTableCreated_10() const { return ___isTableCreated_10; }
	inline bool* get_address_of_isTableCreated_10() { return &___isTableCreated_10; }
	inline void set_isTableCreated_10(bool value)
	{
		___isTableCreated_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
