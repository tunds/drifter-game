﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Object,System.Object>
struct GetEnumeratorU3Ec__Iterator0_t4274477349;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"

// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Object,System.Object>::.ctor()
extern "C"  void GetEnumeratorU3Ec__Iterator0__ctor_m3946720699_gshared (GetEnumeratorU3Ec__Iterator0_t4274477349 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0__ctor_m3946720699(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator0_t4274477349 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0__ctor_m3946720699_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Object,System.Object>::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_Current()
extern "C"  KeyValuePair_2_t3312956448  GetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m1425750618_gshared (GetEnumeratorU3Ec__Iterator0_t4274477349 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m1425750618(__this, method) ((  KeyValuePair_2_t3312956448  (*) (GetEnumeratorU3Ec__Iterator0_t4274477349 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m1425750618_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * GetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1804744021_gshared (GetEnumeratorU3Ec__Iterator0_t4274477349 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1804744021(__this, method) ((  Il2CppObject * (*) (GetEnumeratorU3Ec__Iterator0_t4274477349 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1804744021_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Object,System.Object>::MoveNext()
extern "C"  bool GetEnumeratorU3Ec__Iterator0_MoveNext_m3226789923_gshared (GetEnumeratorU3Ec__Iterator0_t4274477349 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_MoveNext_m3226789923(__this, method) ((  bool (*) (GetEnumeratorU3Ec__Iterator0_t4274477349 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_MoveNext_m3226789923_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Object,System.Object>::Dispose()
extern "C"  void GetEnumeratorU3Ec__Iterator0_Dispose_m240752440_gshared (GetEnumeratorU3Ec__Iterator0_t4274477349 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_Dispose_m240752440(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator0_t4274477349 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_Dispose_m240752440_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Object,System.Object>::Reset()
extern "C"  void GetEnumeratorU3Ec__Iterator0_Reset_m1593153640_gshared (GetEnumeratorU3Ec__Iterator0_t4274477349 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_Reset_m1593153640(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator0_t4274477349 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_Reset_m1593153640_gshared)(__this, method)
