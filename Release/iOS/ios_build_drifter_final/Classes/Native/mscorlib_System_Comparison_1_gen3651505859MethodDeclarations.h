﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3520123377MethodDeclarations.h"

// System.Void System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m362587477(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t3651505859 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m4100593155_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::Invoke(T,T)
#define Comparison_1_Invoke_m556108683(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3651505859 *, KeyValuePair_2_t947830983 , KeyValuePair_2_t947830983 , const MethodInfo*))Comparison_1_Invoke_m585221789_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m1959343620(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t3651505859 *, KeyValuePair_2_t947830983 , KeyValuePair_2_t947830983 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3618731798_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.String>>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m1327966977(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t3651505859 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m2981372591_gshared)(__this, ___result0, method)
