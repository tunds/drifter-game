﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct List_1_t722451806;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3103202094.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataPair4220460133.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SQLiteDatabase.SQLiteDB/DB_DataPair>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m858864750_gshared (Enumerator_t3103202094 * __this, List_1_t722451806 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m858864750(__this, ___l0, method) ((  void (*) (Enumerator_t3103202094 *, List_1_t722451806 *, const MethodInfo*))Enumerator__ctor_m858864750_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2869512996_gshared (Enumerator_t3103202094 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2869512996(__this, method) ((  void (*) (Enumerator_t3103202094 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2869512996_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3663765264_gshared (Enumerator_t3103202094 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3663765264(__this, method) ((  Il2CppObject * (*) (Enumerator_t3103202094 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3663765264_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLiteDatabase.SQLiteDB/DB_DataPair>::Dispose()
extern "C"  void Enumerator_Dispose_m1683572179_gshared (Enumerator_t3103202094 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1683572179(__this, method) ((  void (*) (Enumerator_t3103202094 *, const MethodInfo*))Enumerator_Dispose_m1683572179_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLiteDatabase.SQLiteDB/DB_DataPair>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3506430860_gshared (Enumerator_t3103202094 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3506430860(__this, method) ((  void (*) (Enumerator_t3103202094 *, const MethodInfo*))Enumerator_VerifyState_m3506430860_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SQLiteDatabase.SQLiteDB/DB_DataPair>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3937859216_gshared (Enumerator_t3103202094 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3937859216(__this, method) ((  bool (*) (Enumerator_t3103202094 *, const MethodInfo*))Enumerator_MoveNext_m3937859216_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Current()
extern "C"  DB_DataPair_t4220460133  Enumerator_get_Current_m2012668483_gshared (Enumerator_t3103202094 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2012668483(__this, method) ((  DB_DataPair_t4220460133  (*) (Enumerator_t3103202094 *, const MethodInfo*))Enumerator_get_Current_m2012668483_gshared)(__this, method)
