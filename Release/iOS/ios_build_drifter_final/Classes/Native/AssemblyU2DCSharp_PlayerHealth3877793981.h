﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Slider
struct Slider_t1468074762;
// UnityEngine.AudioClip
struct AudioClip_t3714538611;
// UnityEngine.AudioSource
struct AudioSource_t3628549054;
// PlayerMovement
struct PlayerMovement_t3827129040;
// PlayerEffects
struct PlayerEffects_t1618007745;
// GlobalObjectManager
struct GlobalObjectManager_t849077355;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerHealth
struct  PlayerHealth_t3877793981  : public MonoBehaviour_t3012272455
{
public:
	// System.Int32 PlayerHealth::startHealth
	int32_t ___startHealth_2;
	// System.Int32 PlayerHealth::currentHealth
	int32_t ___currentHealth_3;
	// UnityEngine.UI.Slider PlayerHealth::healthSldr
	Slider_t1468074762 * ___healthSldr_4;
	// UnityEngine.AudioClip PlayerHealth::audioClip
	AudioClip_t3714538611 * ___audioClip_5;
	// UnityEngine.AudioClip PlayerHealth::deathAudioClip
	AudioClip_t3714538611 * ___deathAudioClip_6;
	// UnityEngine.AudioSource PlayerHealth::audio
	AudioSource_t3628549054 * ___audio_7;
	// PlayerMovement PlayerHealth::playerMovement
	PlayerMovement_t3827129040 * ___playerMovement_8;
	// PlayerEffects PlayerHealth::playerEffects
	PlayerEffects_t1618007745 * ___playerEffects_9;
	// GlobalObjectManager PlayerHealth::persistentData
	GlobalObjectManager_t849077355 * ___persistentData_10;
	// System.Boolean PlayerHealth::hasDied
	bool ___hasDied_11;

public:
	inline static int32_t get_offset_of_startHealth_2() { return static_cast<int32_t>(offsetof(PlayerHealth_t3877793981, ___startHealth_2)); }
	inline int32_t get_startHealth_2() const { return ___startHealth_2; }
	inline int32_t* get_address_of_startHealth_2() { return &___startHealth_2; }
	inline void set_startHealth_2(int32_t value)
	{
		___startHealth_2 = value;
	}

	inline static int32_t get_offset_of_currentHealth_3() { return static_cast<int32_t>(offsetof(PlayerHealth_t3877793981, ___currentHealth_3)); }
	inline int32_t get_currentHealth_3() const { return ___currentHealth_3; }
	inline int32_t* get_address_of_currentHealth_3() { return &___currentHealth_3; }
	inline void set_currentHealth_3(int32_t value)
	{
		___currentHealth_3 = value;
	}

	inline static int32_t get_offset_of_healthSldr_4() { return static_cast<int32_t>(offsetof(PlayerHealth_t3877793981, ___healthSldr_4)); }
	inline Slider_t1468074762 * get_healthSldr_4() const { return ___healthSldr_4; }
	inline Slider_t1468074762 ** get_address_of_healthSldr_4() { return &___healthSldr_4; }
	inline void set_healthSldr_4(Slider_t1468074762 * value)
	{
		___healthSldr_4 = value;
		Il2CppCodeGenWriteBarrier(&___healthSldr_4, value);
	}

	inline static int32_t get_offset_of_audioClip_5() { return static_cast<int32_t>(offsetof(PlayerHealth_t3877793981, ___audioClip_5)); }
	inline AudioClip_t3714538611 * get_audioClip_5() const { return ___audioClip_5; }
	inline AudioClip_t3714538611 ** get_address_of_audioClip_5() { return &___audioClip_5; }
	inline void set_audioClip_5(AudioClip_t3714538611 * value)
	{
		___audioClip_5 = value;
		Il2CppCodeGenWriteBarrier(&___audioClip_5, value);
	}

	inline static int32_t get_offset_of_deathAudioClip_6() { return static_cast<int32_t>(offsetof(PlayerHealth_t3877793981, ___deathAudioClip_6)); }
	inline AudioClip_t3714538611 * get_deathAudioClip_6() const { return ___deathAudioClip_6; }
	inline AudioClip_t3714538611 ** get_address_of_deathAudioClip_6() { return &___deathAudioClip_6; }
	inline void set_deathAudioClip_6(AudioClip_t3714538611 * value)
	{
		___deathAudioClip_6 = value;
		Il2CppCodeGenWriteBarrier(&___deathAudioClip_6, value);
	}

	inline static int32_t get_offset_of_audio_7() { return static_cast<int32_t>(offsetof(PlayerHealth_t3877793981, ___audio_7)); }
	inline AudioSource_t3628549054 * get_audio_7() const { return ___audio_7; }
	inline AudioSource_t3628549054 ** get_address_of_audio_7() { return &___audio_7; }
	inline void set_audio_7(AudioSource_t3628549054 * value)
	{
		___audio_7 = value;
		Il2CppCodeGenWriteBarrier(&___audio_7, value);
	}

	inline static int32_t get_offset_of_playerMovement_8() { return static_cast<int32_t>(offsetof(PlayerHealth_t3877793981, ___playerMovement_8)); }
	inline PlayerMovement_t3827129040 * get_playerMovement_8() const { return ___playerMovement_8; }
	inline PlayerMovement_t3827129040 ** get_address_of_playerMovement_8() { return &___playerMovement_8; }
	inline void set_playerMovement_8(PlayerMovement_t3827129040 * value)
	{
		___playerMovement_8 = value;
		Il2CppCodeGenWriteBarrier(&___playerMovement_8, value);
	}

	inline static int32_t get_offset_of_playerEffects_9() { return static_cast<int32_t>(offsetof(PlayerHealth_t3877793981, ___playerEffects_9)); }
	inline PlayerEffects_t1618007745 * get_playerEffects_9() const { return ___playerEffects_9; }
	inline PlayerEffects_t1618007745 ** get_address_of_playerEffects_9() { return &___playerEffects_9; }
	inline void set_playerEffects_9(PlayerEffects_t1618007745 * value)
	{
		___playerEffects_9 = value;
		Il2CppCodeGenWriteBarrier(&___playerEffects_9, value);
	}

	inline static int32_t get_offset_of_persistentData_10() { return static_cast<int32_t>(offsetof(PlayerHealth_t3877793981, ___persistentData_10)); }
	inline GlobalObjectManager_t849077355 * get_persistentData_10() const { return ___persistentData_10; }
	inline GlobalObjectManager_t849077355 ** get_address_of_persistentData_10() { return &___persistentData_10; }
	inline void set_persistentData_10(GlobalObjectManager_t849077355 * value)
	{
		___persistentData_10 = value;
		Il2CppCodeGenWriteBarrier(&___persistentData_10, value);
	}

	inline static int32_t get_offset_of_hasDied_11() { return static_cast<int32_t>(offsetof(PlayerHealth_t3877793981, ___hasDied_11)); }
	inline bool get_hasDied_11() const { return ___hasDied_11; }
	inline bool* get_address_of_hasDied_11() { return &___hasDied_11; }
	inline void set_hasDied_11(bool value)
	{
		___hasDied_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
