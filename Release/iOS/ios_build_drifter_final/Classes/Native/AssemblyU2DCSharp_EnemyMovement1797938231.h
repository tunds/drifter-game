﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t3296505762;
// PlayerHealth
struct PlayerHealth_t3877793981;
// EnemyHealth
struct EnemyHealth_t1417584612;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyMovement
struct  EnemyMovement_t1797938231  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Transform EnemyMovement::playerPosition
	Transform_t284553113 * ___playerPosition_2;
	// UnityEngine.NavMeshAgent EnemyMovement::navigation
	NavMeshAgent_t3296505762 * ___navigation_3;
	// PlayerHealth EnemyMovement::playerHealth
	PlayerHealth_t3877793981 * ___playerHealth_4;
	// EnemyHealth EnemyMovement::enemyHealth
	EnemyHealth_t1417584612 * ___enemyHealth_5;

public:
	inline static int32_t get_offset_of_playerPosition_2() { return static_cast<int32_t>(offsetof(EnemyMovement_t1797938231, ___playerPosition_2)); }
	inline Transform_t284553113 * get_playerPosition_2() const { return ___playerPosition_2; }
	inline Transform_t284553113 ** get_address_of_playerPosition_2() { return &___playerPosition_2; }
	inline void set_playerPosition_2(Transform_t284553113 * value)
	{
		___playerPosition_2 = value;
		Il2CppCodeGenWriteBarrier(&___playerPosition_2, value);
	}

	inline static int32_t get_offset_of_navigation_3() { return static_cast<int32_t>(offsetof(EnemyMovement_t1797938231, ___navigation_3)); }
	inline NavMeshAgent_t3296505762 * get_navigation_3() const { return ___navigation_3; }
	inline NavMeshAgent_t3296505762 ** get_address_of_navigation_3() { return &___navigation_3; }
	inline void set_navigation_3(NavMeshAgent_t3296505762 * value)
	{
		___navigation_3 = value;
		Il2CppCodeGenWriteBarrier(&___navigation_3, value);
	}

	inline static int32_t get_offset_of_playerHealth_4() { return static_cast<int32_t>(offsetof(EnemyMovement_t1797938231, ___playerHealth_4)); }
	inline PlayerHealth_t3877793981 * get_playerHealth_4() const { return ___playerHealth_4; }
	inline PlayerHealth_t3877793981 ** get_address_of_playerHealth_4() { return &___playerHealth_4; }
	inline void set_playerHealth_4(PlayerHealth_t3877793981 * value)
	{
		___playerHealth_4 = value;
		Il2CppCodeGenWriteBarrier(&___playerHealth_4, value);
	}

	inline static int32_t get_offset_of_enemyHealth_5() { return static_cast<int32_t>(offsetof(EnemyMovement_t1797938231, ___enemyHealth_5)); }
	inline EnemyHealth_t1417584612 * get_enemyHealth_5() const { return ___enemyHealth_5; }
	inline EnemyHealth_t1417584612 ** get_address_of_enemyHealth_5() { return &___enemyHealth_5; }
	inline void set_enemyHealth_5(EnemyHealth_t1417584612 * value)
	{
		___enemyHealth_5 = value;
		Il2CppCodeGenWriteBarrier(&___enemyHealth_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
