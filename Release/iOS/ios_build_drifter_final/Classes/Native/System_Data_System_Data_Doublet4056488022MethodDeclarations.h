﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Doublet
struct Doublet_t4056488022;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void System.Data.Doublet::.ctor(System.Int32,System.String)
extern "C"  void Doublet__ctor_m3767502751 (Doublet_t4056488022 * __this, int32_t ___count0, String_t* ___columnname1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
