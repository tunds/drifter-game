﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1874265492(__this, ___l0, method) ((  void (*) (Enumerator_t1357546285 *, List_1_t3271763293 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2439504446(__this, method) ((  void (*) (Enumerator_t1357546285 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3040781876(__this, method) ((  Il2CppObject * (*) (Enumerator_t1357546285 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::Dispose()
#define Enumerator_Dispose_m1213151353(__this, method) ((  void (*) (Enumerator_t1357546285 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::VerifyState()
#define Enumerator_VerifyState_m231740210(__this, method) ((  void (*) (Enumerator_t1357546285 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::MoveNext()
#define Enumerator_MoveNext_m3042248686(__this, method) ((  bool (*) (Enumerator_t1357546285 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Current()
#define Enumerator_get_Current_m1119873099(__this, method) ((  Dictionary_2_t2474804324 * (*) (Enumerator_t1357546285 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
