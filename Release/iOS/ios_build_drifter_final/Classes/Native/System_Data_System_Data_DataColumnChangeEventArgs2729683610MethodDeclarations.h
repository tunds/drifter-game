﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.DataColumnChangeEventArgs
struct DataColumnChangeEventArgs_t2729683610;
// System.Data.DataRow
struct DataRow_t3654701923;
// System.Data.DataColumn
struct DataColumn_t3354469747;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_DataRow3654701923.h"
#include "System_Data_System_Data_DataColumn3354469747.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Data.DataColumnChangeEventArgs::.ctor(System.Data.DataRow,System.Data.DataColumn,System.Object)
extern "C"  void DataColumnChangeEventArgs__ctor_m3517710998 (DataColumnChangeEventArgs_t2729683610 * __this, DataRow_t3654701923 * ___row0, DataColumn_t3354469747 * ___column1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.DataColumnChangeEventArgs::.ctor()
extern "C"  void DataColumnChangeEventArgs__ctor_m3691399182 (DataColumnChangeEventArgs_t2729683610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Data.DataColumnChangeEventArgs::get_ProposedValue()
extern "C"  Il2CppObject * DataColumnChangeEventArgs_get_ProposedValue_m642842595 (DataColumnChangeEventArgs_t2729683610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.DataColumnChangeEventArgs::Initialize(System.Data.DataRow,System.Data.DataColumn,System.Object)
extern "C"  void DataColumnChangeEventArgs_Initialize_m1888852430 (DataColumnChangeEventArgs_t2729683610 * __this, DataRow_t3654701923 * ___row0, DataColumn_t3354469747 * ___column1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
