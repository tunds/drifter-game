﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbParameterCollection
struct DbParameterCollection_t3381130713;
// System.Object
struct Il2CppObject;
// System.Data.Common.DbParameter
struct DbParameter_t3306161371;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_String968488902.h"
#include "System_Data_System_Data_Common_DbParameter3306161371.h"

// System.Void System.Data.Common.DbParameterCollection::.ctor()
extern "C"  void DbParameterCollection__ctor_m4186716011 (DbParameterCollection_t3381130713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Data.Common.DbParameterCollection::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * DbParameterCollection_System_Collections_IList_get_Item_m2246162432 (DbParameterCollection_t3381130713 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbParameterCollection::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void DbParameterCollection_System_Collections_IList_set_Item_m619814999 (DbParameterCollection_t3381130713 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameter System.Data.Common.DbParameterCollection::get_Item(System.String)
extern "C"  DbParameter_t3306161371 * DbParameterCollection_get_Item_m3865959847 (DbParameterCollection_t3381130713 * __this, String_t* ___parameterName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameter System.Data.Common.DbParameterCollection::get_Item(System.Int32)
extern "C"  DbParameter_t3306161371 * DbParameterCollection_get_Item_m1079807500 (DbParameterCollection_t3381130713 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbParameterCollection::set_Item(System.Int32,System.Data.Common.DbParameter)
extern "C"  void DbParameterCollection_set_Item_m1393533375 (DbParameterCollection_t3381130713 * __this, int32_t ___index0, DbParameter_t3306161371 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
