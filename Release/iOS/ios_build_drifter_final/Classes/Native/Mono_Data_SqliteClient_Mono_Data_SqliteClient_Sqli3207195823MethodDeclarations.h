﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.SqliteClient.SqliteCommand
struct SqliteCommand_t3207195823;
// System.String
struct String_t;
// Mono.Data.SqliteClient.SqliteConnection
struct SqliteConnection_t2830494554;
// System.Data.IDbTransaction
struct IDbTransaction_t3098575722;
// System.Object
struct Il2CppObject;
// System.Data.Common.DbConnection
struct DbConnection_t462757452;
// Mono.Data.SqliteClient.SqliteParameterCollection
struct SqliteParameterCollection_t3807043211;
// System.Data.Common.DbParameterCollection
struct DbParameterCollection_t3381130713;
// System.Data.Common.DbTransaction
struct DbTransaction_t4162579344;
// System.Data.Common.DbParameter
struct DbParameter_t3306161371;
// Mono.Data.SqliteClient.SqliteDataReader
struct SqliteDataReader_t545175945;
// System.Data.Common.DbDataReader
struct DbDataReader_t2472406139;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli2830494554.h"
#include "System_Data_System_Data_CommandType753495736.h"
#include "System_Data_System_Data_Common_DbConnection462757452.h"
#include "System_Data_System_Data_Common_DbTransaction4162579344.h"
#include "System_Data_System_Data_UpdateRowSource1763489119.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_Data_System_Data_CommandBehavior326612048.h"

// System.Void Mono.Data.SqliteClient.SqliteCommand::.ctor(System.String,Mono.Data.SqliteClient.SqliteConnection)
extern "C"  void SqliteCommand__ctor_m4105589194 (SqliteCommand_t3207195823 * __this, String_t* ___sqlText0, SqliteConnection_t2830494554 * ___dbConn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteCommand::.ctor(System.String,Mono.Data.SqliteClient.SqliteConnection,System.Data.IDbTransaction)
extern "C"  void SqliteCommand__ctor_m2781458304 (SqliteCommand_t3207195823 * __this, String_t* ___sqlText0, SqliteConnection_t2830494554 * ___dbConn1, Il2CppObject * ___trans2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.SqliteClient.SqliteCommand::System.ICloneable.Clone()
extern "C"  Il2CppObject * SqliteCommand_System_ICloneable_Clone_m622759489 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.SqliteClient.SqliteCommand::get_CommandText()
extern "C"  String_t* SqliteCommand_get_CommandText_m3094117606 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteCommand::set_CommandText(System.String)
extern "C"  void SqliteCommand_set_CommandText_m3093197357 (SqliteCommand_t3207195823 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.SqliteCommand::get_CommandTimeout()
extern "C"  int32_t SqliteCommand_get_CommandTimeout_m3082381849 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteCommand::set_CommandTimeout(System.Int32)
extern "C"  void SqliteCommand_set_CommandTimeout_m1002795404 (SqliteCommand_t3207195823 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteCommand::set_CommandType(System.Data.CommandType)
extern "C"  void SqliteCommand_set_CommandType_m1638761424 (SqliteCommand_t3207195823 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbConnection Mono.Data.SqliteClient.SqliteCommand::get_DbConnection()
extern "C"  DbConnection_t462757452 * SqliteCommand_get_DbConnection_m955493062 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteCommand::set_DbConnection(System.Data.Common.DbConnection)
extern "C"  void SqliteCommand_set_DbConnection_m1449118263 (SqliteCommand_t3207195823 * __this, DbConnection_t462757452 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteConnection Mono.Data.SqliteClient.SqliteCommand::get_Connection()
extern "C"  SqliteConnection_t2830494554 * SqliteCommand_get_Connection_m1408967322 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteParameterCollection Mono.Data.SqliteClient.SqliteCommand::get_Parameters()
extern "C"  SqliteParameterCollection_t3807043211 * SqliteCommand_get_Parameters_m3305558975 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameterCollection Mono.Data.SqliteClient.SqliteCommand::get_DbParameterCollection()
extern "C"  DbParameterCollection_t3381130713 * SqliteCommand_get_DbParameterCollection_m4225734788 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbTransaction Mono.Data.SqliteClient.SqliteCommand::get_DbTransaction()
extern "C"  DbTransaction_t4162579344 * SqliteCommand_get_DbTransaction_m3542086770 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteCommand::set_DbTransaction(System.Data.Common.DbTransaction)
extern "C"  void SqliteCommand_set_DbTransaction_m630792475 (SqliteCommand_t3207195823 * __this, DbTransaction_t4162579344 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteCommand::get_DesignTimeVisible()
extern "C"  bool SqliteCommand_get_DesignTimeVisible_m3883816672 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteCommand::set_DesignTimeVisible(System.Boolean)
extern "C"  void SqliteCommand_set_DesignTimeVisible_m267100635 (SqliteCommand_t3207195823 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.UpdateRowSource Mono.Data.SqliteClient.SqliteCommand::get_UpdatedRowSource()
extern "C"  int32_t SqliteCommand_get_UpdatedRowSource_m2793172087 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteCommand::set_UpdatedRowSource(System.Data.UpdateRowSource)
extern "C"  void SqliteCommand_set_UpdatedRowSource_m2674694252 (SqliteCommand_t3207195823 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.SqliteCommand::NumChanges()
extern "C"  int32_t SqliteCommand_NumChanges_m4267508823 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteCommand::BindParameters3(System.IntPtr)
extern "C"  void SqliteCommand_BindParameters3_m193939214 (SqliteCommand_t3207195823 * __this, IntPtr_t ___pStmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteCommand::GetNextStatement(System.IntPtr,System.IntPtr&,System.IntPtr&)
extern "C"  void SqliteCommand_GetNextStatement_m3351897126 (SqliteCommand_t3207195823 * __this, IntPtr_t ___pzStart0, IntPtr_t* ___pzTail1, IntPtr_t* ___pStmt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteCommand::ExecuteStatement(System.IntPtr)
extern "C"  void SqliteCommand_ExecuteStatement_m3514726002 (SqliteCommand_t3207195823 * __this, IntPtr_t ___pStmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteCommand::ExecuteStatement(System.IntPtr,System.Int32&,System.IntPtr&,System.IntPtr&)
extern "C"  bool SqliteCommand_ExecuteStatement_m2763413839 (SqliteCommand_t3207195823 * __this, IntPtr_t ___pStmt0, int32_t* ___cols1, IntPtr_t* ___pazValue2, IntPtr_t* ___pazColName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.SqliteClient.SqliteCommand::BindParameters2()
extern "C"  String_t* SqliteCommand_BindParameters2_m3266475330 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteCommand::Prepare()
extern "C"  void SqliteCommand_Prepare_m1771868993 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameter Mono.Data.SqliteClient.SqliteCommand::CreateDbParameter()
extern "C"  DbParameter_t3306161371 * SqliteCommand_CreateDbParameter_m525919893 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.SqliteCommand::ExecuteNonQuery()
extern "C"  int32_t SqliteCommand_ExecuteNonQuery_m1920021240 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteDataReader Mono.Data.SqliteClient.SqliteCommand::ExecuteReader(System.Data.CommandBehavior)
extern "C"  SqliteDataReader_t545175945 * SqliteCommand_ExecuteReader_m1310918180 (SqliteCommand_t3207195823 * __this, int32_t ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbDataReader Mono.Data.SqliteClient.SqliteCommand::ExecuteDbDataReader(System.Data.CommandBehavior)
extern "C"  DbDataReader_t2472406139 * SqliteCommand_ExecuteDbDataReader_m3231897610 (SqliteCommand_t3207195823 * __this, int32_t ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.SqliteClient.SqliteDataReader Mono.Data.SqliteClient.SqliteCommand::ExecuteReader(System.Data.CommandBehavior,System.Boolean,System.Int32&)
extern "C"  SqliteDataReader_t545175945 * SqliteCommand_ExecuteReader_m1629572846 (SqliteCommand_t3207195823 * __this, int32_t ___behavior0, bool ___want_results1, int32_t* ___rows_affected2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.SqliteClient.SqliteCommand::GetError3()
extern "C"  String_t* SqliteCommand_GetError3_m1160471384 (SqliteCommand_t3207195823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
