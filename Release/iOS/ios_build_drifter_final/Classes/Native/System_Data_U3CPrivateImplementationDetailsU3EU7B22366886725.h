﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}/$ArrayType=168
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D168_t2366886725 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU3D168_t2366886725__padding[168];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}/$ArrayType=168
#pragma pack(push, tp, 1)
struct U24ArrayTypeU3D168_t2366886725_marshaled_pinvoke
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU3D168_t2366886725__padding[168];
	};
};
#pragma pack(pop, tp)
// Native definition for marshalling of: <PrivateImplementationDetails>{208ed9b8-43e9-4190-912c-9dd9f5304e9b}/$ArrayType=168
#pragma pack(push, tp, 1)
struct U24ArrayTypeU3D168_t2366886725_marshaled_com
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU3D168_t2366886725__padding[168];
	};
};
#pragma pack(pop, tp)
