﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "System_Data_System_Data_Common_DbParameter3306161371.h"
#include "System_Data_System_Data_DataRowVersion2975473339.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.Sqlite.SqliteParameter
struct  SqliteParameter_t3651135812  : public DbParameter_t3306161371
{
public:
	// System.Int32 Mono.Data.Sqlite.SqliteParameter::_dbType
	int32_t ____dbType_2;
	// System.Data.DataRowVersion Mono.Data.Sqlite.SqliteParameter::_rowVersion
	int32_t ____rowVersion_3;
	// System.Object Mono.Data.Sqlite.SqliteParameter::_objValue
	Il2CppObject * ____objValue_4;
	// System.String Mono.Data.Sqlite.SqliteParameter::_sourceColumn
	String_t* ____sourceColumn_5;
	// System.String Mono.Data.Sqlite.SqliteParameter::_parameterName
	String_t* ____parameterName_6;
	// System.Int32 Mono.Data.Sqlite.SqliteParameter::_dataSize
	int32_t ____dataSize_7;
	// System.Boolean Mono.Data.Sqlite.SqliteParameter::_nullable
	bool ____nullable_8;
	// System.Boolean Mono.Data.Sqlite.SqliteParameter::_nullMapping
	bool ____nullMapping_9;

public:
	inline static int32_t get_offset_of__dbType_2() { return static_cast<int32_t>(offsetof(SqliteParameter_t3651135812, ____dbType_2)); }
	inline int32_t get__dbType_2() const { return ____dbType_2; }
	inline int32_t* get_address_of__dbType_2() { return &____dbType_2; }
	inline void set__dbType_2(int32_t value)
	{
		____dbType_2 = value;
	}

	inline static int32_t get_offset_of__rowVersion_3() { return static_cast<int32_t>(offsetof(SqliteParameter_t3651135812, ____rowVersion_3)); }
	inline int32_t get__rowVersion_3() const { return ____rowVersion_3; }
	inline int32_t* get_address_of__rowVersion_3() { return &____rowVersion_3; }
	inline void set__rowVersion_3(int32_t value)
	{
		____rowVersion_3 = value;
	}

	inline static int32_t get_offset_of__objValue_4() { return static_cast<int32_t>(offsetof(SqliteParameter_t3651135812, ____objValue_4)); }
	inline Il2CppObject * get__objValue_4() const { return ____objValue_4; }
	inline Il2CppObject ** get_address_of__objValue_4() { return &____objValue_4; }
	inline void set__objValue_4(Il2CppObject * value)
	{
		____objValue_4 = value;
		Il2CppCodeGenWriteBarrier(&____objValue_4, value);
	}

	inline static int32_t get_offset_of__sourceColumn_5() { return static_cast<int32_t>(offsetof(SqliteParameter_t3651135812, ____sourceColumn_5)); }
	inline String_t* get__sourceColumn_5() const { return ____sourceColumn_5; }
	inline String_t** get_address_of__sourceColumn_5() { return &____sourceColumn_5; }
	inline void set__sourceColumn_5(String_t* value)
	{
		____sourceColumn_5 = value;
		Il2CppCodeGenWriteBarrier(&____sourceColumn_5, value);
	}

	inline static int32_t get_offset_of__parameterName_6() { return static_cast<int32_t>(offsetof(SqliteParameter_t3651135812, ____parameterName_6)); }
	inline String_t* get__parameterName_6() const { return ____parameterName_6; }
	inline String_t** get_address_of__parameterName_6() { return &____parameterName_6; }
	inline void set__parameterName_6(String_t* value)
	{
		____parameterName_6 = value;
		Il2CppCodeGenWriteBarrier(&____parameterName_6, value);
	}

	inline static int32_t get_offset_of__dataSize_7() { return static_cast<int32_t>(offsetof(SqliteParameter_t3651135812, ____dataSize_7)); }
	inline int32_t get__dataSize_7() const { return ____dataSize_7; }
	inline int32_t* get_address_of__dataSize_7() { return &____dataSize_7; }
	inline void set__dataSize_7(int32_t value)
	{
		____dataSize_7 = value;
	}

	inline static int32_t get_offset_of__nullable_8() { return static_cast<int32_t>(offsetof(SqliteParameter_t3651135812, ____nullable_8)); }
	inline bool get__nullable_8() const { return ____nullable_8; }
	inline bool* get_address_of__nullable_8() { return &____nullable_8; }
	inline void set__nullable_8(bool value)
	{
		____nullable_8 = value;
	}

	inline static int32_t get_offset_of__nullMapping_9() { return static_cast<int32_t>(offsetof(SqliteParameter_t3651135812, ____nullMapping_9)); }
	inline bool get__nullMapping_9() const { return ____nullMapping_9; }
	inline bool* get_address_of__nullMapping_9() { return &____nullMapping_9; }
	inline void set__nullMapping_9(bool value)
	{
		____nullMapping_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
