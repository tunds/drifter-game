﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Object,System.Object>
struct GetEnumeratorU3Ec__Iterator2_t4267822955;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Object,System.Object>::.ctor()
extern "C"  void GetEnumeratorU3Ec__Iterator2__ctor_m2607072436_gshared (GetEnumeratorU3Ec__Iterator2_t4267822955 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2__ctor_m2607072436(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator2_t4267822955 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2__ctor_m2607072436_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * GetEnumeratorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2205941918_gshared (GetEnumeratorU3Ec__Iterator2_t4267822955 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2205941918(__this, method) ((  Il2CppObject * (*) (GetEnumeratorU3Ec__Iterator2_t4267822955 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2205941918_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * GetEnumeratorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2572091442_gshared (GetEnumeratorU3Ec__Iterator2_t4267822955 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2572091442(__this, method) ((  Il2CppObject * (*) (GetEnumeratorU3Ec__Iterator2_t4267822955 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2572091442_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Object,System.Object>::MoveNext()
extern "C"  bool GetEnumeratorU3Ec__Iterator2_MoveNext_m3650407966_gshared (GetEnumeratorU3Ec__Iterator2_t4267822955 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_MoveNext_m3650407966(__this, method) ((  bool (*) (GetEnumeratorU3Ec__Iterator2_t4267822955 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_MoveNext_m3650407966_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Object,System.Object>::Dispose()
extern "C"  void GetEnumeratorU3Ec__Iterator2_Dispose_m1328960497_gshared (GetEnumeratorU3Ec__Iterator2_t4267822955 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_Dispose_m1328960497(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator2_t4267822955 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_Dispose_m1328960497_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Object,System.Object>::Reset()
extern "C"  void GetEnumeratorU3Ec__Iterator2_Reset_m253505377_gshared (GetEnumeratorU3Ec__Iterator2_t4267822955 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_Reset_m253505377(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator2_t4267822955 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_Reset_m253505377_gshared)(__this, method)
