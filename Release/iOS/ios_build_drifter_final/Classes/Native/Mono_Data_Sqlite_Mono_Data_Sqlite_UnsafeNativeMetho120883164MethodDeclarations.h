﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t58506160;
// Mono.Data.Sqlite.SQLiteCallback
struct SQLiteCallback_t2947340536;
// Mono.Data.Sqlite.SQLiteFinalCallback
struct SQLiteFinalCallback_t347793270;
// System.String
struct String_t;
// Mono.Data.Sqlite.SQLiteCollation
struct SQLiteCollation_t3373936932;
// Mono.Data.Sqlite.SQLiteUpdateCallback
struct SQLiteUpdateCallback_t2915972065;
// Mono.Data.Sqlite.SQLiteCommitCallback
struct SQLiteCommitCallback_t1317328463;
// Mono.Data.Sqlite.SQLiteRollbackCallback
struct SQLiteRollbackCallback_t4255779836;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteCallback2947340536.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteFinalCallba347793270.h"
#include "mscorlib_System_String968488902.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_TypeAffinity3864856329.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteCollation3373936932.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteUpdateCall2915972065.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteCommitCall1317328463.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteRollbackCa4255779836.h"

// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_close(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_close_m1563730316 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_create_function(System.IntPtr,System.Byte[],System.Int32,System.Int32,System.IntPtr,Mono.Data.Sqlite.SQLiteCallback,Mono.Data.Sqlite.SQLiteCallback,Mono.Data.Sqlite.SQLiteFinalCallback)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_create_function_m3043144898 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, ByteU5BU5D_t58506160* ___strName1, int32_t ___nArgs2, int32_t ___nType3, IntPtr_t ___pvUser4, SQLiteCallback_t2947340536 * ___func5, SQLiteCallback_t2947340536 * ___fstep6, SQLiteFinalCallback_t347793270 * ___ffinal7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_finalize(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_finalize_m3990639076 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_open_v2(System.Byte[],System.IntPtr&,System.Int32,System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_open_v2_m1996295791 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___utf8Filename0, IntPtr_t* ___db1, int32_t ___flags2, IntPtr_t ___vfs3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_open(System.Byte[],System.IntPtr&)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_open_m4000505649 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___utf8Filename0, IntPtr_t* ___db1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_open16(System.String,System.IntPtr&)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_open16_m2020967293 (Il2CppObject * __this /* static, unused */, String_t* ___fileName0, IntPtr_t* ___db1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_reset(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_reset_m814803285 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_parameter_name(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_bind_parameter_name_m2413036349 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_database_name(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_database_name_m3023975636 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_database_name16(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_database_name16_m1944976761 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_decltype(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_decltype_m2786715463 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_name(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_name_m121139694 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_name16(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_name16_m4048378899 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_origin_name(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_origin_name_m3154982089 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_origin_name16(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_origin_name16_m3288126510 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_table_name(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_table_name_m2907445503 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_table_name16(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_table_name16_m1628668644 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_text(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_text_m4279686544 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_text16(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_text16_m1797349173 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_errmsg(System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_errmsg_m2237771341 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_prepare(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr&,System.IntPtr&)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_prepare_m2062280908 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, IntPtr_t ___pSql1, int32_t ___nBytes2, IntPtr_t* ___stmt3, IntPtr_t* ___ptrRemain4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_table_column_metadata(System.IntPtr,System.Byte[],System.Byte[],System.Byte[],System.IntPtr&,System.IntPtr&,System.Int32&,System.Int32&,System.Int32&)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_table_column_metadata_m3713494693 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, ByteU5BU5D_t58506160* ___dbName1, ByteU5BU5D_t58506160* ___tblName2, ByteU5BU5D_t58506160* ___colName3, IntPtr_t* ___ptrDataType4, IntPtr_t* ___ptrCollSeq5, int32_t* ___notNull6, int32_t* ___primaryKey7, int32_t* ___autoInc8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_value_text(System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_value_text_m3936963118 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_value_text16(System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_value_text16_m1474704041 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_libversion()
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_libversion_m2276737374 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_changes(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_changes_m516844833 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_busy_timeout(System.IntPtr,System.Int32)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_busy_timeout_m3888644208 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, int32_t ___ms1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_blob(System.IntPtr,System.Int32,System.Byte[],System.Int32,System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_blob_m3024617552 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, ByteU5BU5D_t58506160* ___value2, int32_t ___nSize3, IntPtr_t ___nTransient4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_double(System.IntPtr,System.Int32,System.Double)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_double_m2925634338 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, double ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_int(System.IntPtr,System.Int32,System.Int32)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_int_m2910920277 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, int32_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_int64(System.IntPtr,System.Int32,System.Int64)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_int64_m183363672 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, int64_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_null(System.IntPtr,System.Int32)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_null_m2109187612 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_text(System.IntPtr,System.Int32,System.Byte[],System.Int32,System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_text_m526607552 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, ByteU5BU5D_t58506160* ___value2, int32_t ___nlen3, IntPtr_t ___pvReserved4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_parameter_count(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_parameter_count_m2475351403 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_count(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_column_count_m3744143004 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_step(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_step_m1525642486 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_double(System.IntPtr,System.Int32)
extern "C"  double UnsafeNativeMethods_sqlite3_column_double_m1614598086 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_int(System.IntPtr,System.Int32)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_column_int_m4207072411 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_int64(System.IntPtr,System.Int32)
extern "C"  int64_t UnsafeNativeMethods_sqlite3_column_int64_m1549194586 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_blob(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_column_blob_m274708480 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_bytes(System.IntPtr,System.Int32)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_column_bytes_m2417144087 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.TypeAffinity Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_column_type(System.IntPtr,System.Int32)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_column_type_m2106254026 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_create_collation(System.IntPtr,System.Byte[],System.Int32,System.IntPtr,Mono.Data.Sqlite.SQLiteCollation)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_create_collation_m1354437068 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, ByteU5BU5D_t58506160* ___strName1, int32_t ___nType2, IntPtr_t ___pvUser3, SQLiteCollation_t3373936932 * ___func4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_value_blob(System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_value_blob_m577843646 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_value_bytes(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_value_bytes_m739083271 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_value_double(System.IntPtr)
extern "C"  double UnsafeNativeMethods_sqlite3_value_double_m2293406172 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_value_int64(System.IntPtr)
extern "C"  int64_t UnsafeNativeMethods_sqlite3_value_int64_m687817670 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.TypeAffinity Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_value_type(System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_value_type_m109522926 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_blob(System.IntPtr,System.Byte[],System.Int32,System.IntPtr)
extern "C"  void UnsafeNativeMethods_sqlite3_result_blob_m2383137817 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, ByteU5BU5D_t58506160* ___value1, int32_t ___nSize2, IntPtr_t ___pvReserved3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_double(System.IntPtr,System.Double)
extern "C"  void UnsafeNativeMethods_sqlite3_result_double_m2497201553 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, double ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_error(System.IntPtr,System.Byte[],System.Int32)
extern "C"  void UnsafeNativeMethods_sqlite3_result_error_m2125588220 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, ByteU5BU5D_t58506160* ___strErr1, int32_t ___nLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_int64(System.IntPtr,System.Int64)
extern "C"  void UnsafeNativeMethods_sqlite3_result_int64_m1307348997 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_null(System.IntPtr)
extern "C"  void UnsafeNativeMethods_sqlite3_result_null_m771524127 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_text(System.IntPtr,System.Byte[],System.Int32,System.IntPtr)
extern "C"  void UnsafeNativeMethods_sqlite3_result_text_m504754601 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, ByteU5BU5D_t58506160* ___value1, int32_t ___nLen2, IntPtr_t ___pvReserved3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_aggregate_context(System.IntPtr,System.Int32)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_aggregate_context_m2858580041 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, int32_t ___nBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_bind_text16(System.IntPtr,System.Int32,System.String,System.Int32,System.IntPtr)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_bind_text16_m1325191778 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, String_t* ___value2, int32_t ___nlen3, IntPtr_t ___pvReserved4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_error16(System.IntPtr,System.String,System.Int32)
extern "C"  void UnsafeNativeMethods_sqlite3_result_error16_m309901704 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___strName1, int32_t ___nLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_result_text16(System.IntPtr,System.String,System.Int32,System.IntPtr)
extern "C"  void UnsafeNativeMethods_sqlite3_result_text16_m3033158517 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___strName1, int32_t ___nLen2, IntPtr_t ___pvReserved3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_key(System.IntPtr,System.Byte[],System.Int32)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_key_m2153074447 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, ByteU5BU5D_t58506160* ___key1, int32_t ___keylen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_update_hook(System.IntPtr,Mono.Data.Sqlite.SQLiteUpdateCallback,System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_update_hook_m2173926043 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, SQLiteUpdateCallback_t2915972065 * ___func1, IntPtr_t ___pvUser2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_commit_hook(System.IntPtr,Mono.Data.Sqlite.SQLiteCommitCallback,System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_commit_hook_m1545478363 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, SQLiteCommitCallback_t1317328463 * ___func1, IntPtr_t ___pvUser2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_rollback_hook(System.IntPtr,Mono.Data.Sqlite.SQLiteRollbackCallback,System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_rollback_hook_m3276455803 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, SQLiteRollbackCallback_t4255779836 * ___func1, IntPtr_t ___pvUser2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_next_stmt(System.IntPtr,System.IntPtr)
extern "C"  IntPtr_t UnsafeNativeMethods_sqlite3_next_stmt_m3556431383 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, IntPtr_t ___stmt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.UnsafeNativeMethods::sqlite3_exec(System.IntPtr,System.Byte[],System.IntPtr,System.IntPtr,System.IntPtr&)
extern "C"  int32_t UnsafeNativeMethods_sqlite3_exec_m2069248398 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, ByteU5BU5D_t58506160* ___strSql1, IntPtr_t ___pvCallback2, IntPtr_t ___pvParam3, IntPtr_t* ___errMsg4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
