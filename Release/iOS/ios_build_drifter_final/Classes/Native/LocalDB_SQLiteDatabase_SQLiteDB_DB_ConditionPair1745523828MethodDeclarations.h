﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct DB_ConditionPair_t1745523828;
struct DB_ConditionPair_t1745523828_marshaled_pinvoke;

extern "C" void DB_ConditionPair_t1745523828_marshal_pinvoke(const DB_ConditionPair_t1745523828& unmarshaled, DB_ConditionPair_t1745523828_marshaled_pinvoke& marshaled);
extern "C" void DB_ConditionPair_t1745523828_marshal_pinvoke_back(const DB_ConditionPair_t1745523828_marshaled_pinvoke& marshaled, DB_ConditionPair_t1745523828& unmarshaled);
extern "C" void DB_ConditionPair_t1745523828_marshal_pinvoke_cleanup(DB_ConditionPair_t1745523828_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct DB_ConditionPair_t1745523828;
struct DB_ConditionPair_t1745523828_marshaled_com;

extern "C" void DB_ConditionPair_t1745523828_marshal_com(const DB_ConditionPair_t1745523828& unmarshaled, DB_ConditionPair_t1745523828_marshaled_com& marshaled);
extern "C" void DB_ConditionPair_t1745523828_marshal_com_back(const DB_ConditionPair_t1745523828_marshaled_com& marshaled, DB_ConditionPair_t1745523828& unmarshaled);
extern "C" void DB_ConditionPair_t1745523828_marshal_com_cleanup(DB_ConditionPair_t1745523828_marshaled_com& marshaled);
