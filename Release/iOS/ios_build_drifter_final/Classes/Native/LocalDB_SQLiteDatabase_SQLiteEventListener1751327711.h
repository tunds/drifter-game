﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SQLiteDatabase.SQLiteEventListener/OnError
struct OnError_t314785609;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLiteDatabase.SQLiteEventListener
struct  SQLiteEventListener_t1751327711  : public Il2CppObject
{
public:

public:
};

struct SQLiteEventListener_t1751327711_StaticFields
{
public:
	// SQLiteDatabase.SQLiteEventListener/OnError SQLiteDatabase.SQLiteEventListener::onErrorInvoker
	OnError_t314785609 * ___onErrorInvoker_0;

public:
	inline static int32_t get_offset_of_onErrorInvoker_0() { return static_cast<int32_t>(offsetof(SQLiteEventListener_t1751327711_StaticFields, ___onErrorInvoker_0)); }
	inline OnError_t314785609 * get_onErrorInvoker_0() const { return ___onErrorInvoker_0; }
	inline OnError_t314785609 ** get_address_of_onErrorInvoker_0() { return &___onErrorInvoker_0; }
	inline void set_onErrorInvoker_0(OnError_t314785609 * value)
	{
		___onErrorInvoker_0 = value;
		Il2CppCodeGenWriteBarrier(&___onErrorInvoker_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
