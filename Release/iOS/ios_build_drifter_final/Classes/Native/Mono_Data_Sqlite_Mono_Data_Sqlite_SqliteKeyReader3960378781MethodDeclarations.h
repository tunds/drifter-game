﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteKeyReader
struct SqliteKeyReader_t3960378781;
// Mono.Data.Sqlite.SqliteConnection
struct SqliteConnection_t3853176977;
// Mono.Data.Sqlite.SqliteDataReader
struct SqliteDataReader_t1567858368;
// Mono.Data.Sqlite.SqliteStatement
struct SqliteStatement_t3906494218;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.Data.DataTable
struct DataTable_t2176726999;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection3853176977.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteDataReader1567858368.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteStatement3906494218.h"
#include "System_Data_System_Data_DataTable2176726999.h"

// System.Void Mono.Data.Sqlite.SqliteKeyReader::.ctor(Mono.Data.Sqlite.SqliteConnection,Mono.Data.Sqlite.SqliteDataReader,Mono.Data.Sqlite.SqliteStatement)
extern "C"  void SqliteKeyReader__ctor_m4040380232 (SqliteKeyReader_t3960378781 * __this, SqliteConnection_t3853176977 * ___cnn0, SqliteDataReader_t1567858368 * ___reader1, SqliteStatement_t3906494218 * ___stmt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteKeyReader::get_Count()
extern "C"  int32_t SqliteKeyReader_get_Count_m601543949 (SqliteKeyReader_t3960378781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteKeyReader::Sync(System.Int32)
extern "C"  void SqliteKeyReader_Sync_m2811630763 (SqliteKeyReader_t3960378781 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteKeyReader::Sync()
extern "C"  void SqliteKeyReader_Sync_m166270554 (SqliteKeyReader_t3960378781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteKeyReader::Reset()
extern "C"  void SqliteKeyReader_Reset_m3699050706 (SqliteKeyReader_t3960378781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteKeyReader::Dispose()
extern "C"  void SqliteKeyReader_Dispose_m1078236450 (SqliteKeyReader_t3960378781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteKeyReader::GetDataTypeName(System.Int32)
extern "C"  String_t* SqliteKeyReader_GetDataTypeName_m2232540598 (SqliteKeyReader_t3960378781 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Mono.Data.Sqlite.SqliteKeyReader::GetFieldType(System.Int32)
extern "C"  Type_t * SqliteKeyReader_GetFieldType_m3096553352 (SqliteKeyReader_t3960378781 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteKeyReader::GetName(System.Int32)
extern "C"  String_t* SqliteKeyReader_GetName_m1633112466 (SqliteKeyReader_t3960378781 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteKeyReader::GetBoolean(System.Int32)
extern "C"  bool SqliteKeyReader_GetBoolean_m3232741316 (SqliteKeyReader_t3960378781 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 Mono.Data.Sqlite.SqliteKeyReader::GetInt16(System.Int32)
extern "C"  int16_t SqliteKeyReader_GetInt16_m1481245060 (SqliteKeyReader_t3960378781 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteKeyReader::GetInt32(System.Int32)
extern "C"  int32_t SqliteKeyReader_GetInt32_m2713145668 (SqliteKeyReader_t3960378781 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.Data.Sqlite.SqliteKeyReader::GetInt64(System.Int32)
extern "C"  int64_t SqliteKeyReader_GetInt64_m1176458212 (SqliteKeyReader_t3960378781 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteKeyReader::GetString(System.Int32)
extern "C"  String_t* SqliteKeyReader_GetString_m2807728984 (SqliteKeyReader_t3960378781 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteKeyReader::GetValue(System.Int32)
extern "C"  Il2CppObject * SqliteKeyReader_GetValue_m1593935424 (SqliteKeyReader_t3960378781 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteKeyReader::IsDBNull(System.Int32)
extern "C"  bool SqliteKeyReader_IsDBNull_m4283187681 (SqliteKeyReader_t3960378781 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteKeyReader::AppendSchemaTable(System.Data.DataTable)
extern "C"  void SqliteKeyReader_AppendSchemaTable_m3169405213 (SqliteKeyReader_t3960378781 * __this, DataTable_t2176726999 * ___tbl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
