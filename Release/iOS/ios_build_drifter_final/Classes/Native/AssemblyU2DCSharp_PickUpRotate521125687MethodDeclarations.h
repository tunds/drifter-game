﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickUpRotate
struct PickUpRotate_t521125687;

#include "codegen/il2cpp-codegen.h"

// System.Void PickUpRotate::.ctor()
extern "C"  void PickUpRotate__ctor_m818307396 (PickUpRotate_t521125687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickUpRotate::FixedUpdate()
extern "C"  void PickUpRotate_FixedUpdate_m602618815 (PickUpRotate_t521125687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
