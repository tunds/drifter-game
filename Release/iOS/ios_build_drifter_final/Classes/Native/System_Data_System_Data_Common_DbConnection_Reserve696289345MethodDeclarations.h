﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.DataTable
struct DataTable_t2176726999;

#include "codegen/il2cpp-codegen.h"

// System.Data.DataTable System.Data.Common.DbConnection/ReservedWords::get_Instance()
extern "C"  DataTable_t2176726999 * ReservedWords_get_Instance_m1200617034 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbConnection/ReservedWords::.cctor()
extern "C"  void ReservedWords__cctor_m789000521 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
