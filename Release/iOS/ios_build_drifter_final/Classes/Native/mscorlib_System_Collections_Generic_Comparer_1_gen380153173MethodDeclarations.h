﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct Comparer_1_t380153173;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.Comparer`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::.ctor()
extern "C"  void Comparer_1__ctor_m2277133580_gshared (Comparer_1_t380153173 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m2277133580(__this, method) ((  void (*) (Comparer_1_t380153173 *, const MethodInfo*))Comparer_1__ctor_m2277133580_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::.cctor()
extern "C"  void Comparer_1__cctor_m1389568033_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m1389568033(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m1389568033_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m814766713_gshared (Comparer_1_t380153173 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m814766713(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t380153173 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m814766713_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Default()
extern "C"  Comparer_1_t380153173 * Comparer_1_get_Default_m2703612176_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m2703612176(__this /* static, unused */, method) ((  Comparer_1_t380153173 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m2703612176_gshared)(__this /* static, unused */, method)
