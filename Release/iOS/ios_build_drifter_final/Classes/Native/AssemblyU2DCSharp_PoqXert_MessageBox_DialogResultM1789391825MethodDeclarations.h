﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PoqXert.MessageBox.DialogResultMethod
struct DialogResultMethod_t1789391825;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_DialogResult3964159632.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PoqXert.MessageBox.DialogResultMethod::.ctor(System.Object,System.IntPtr)
extern "C"  void DialogResultMethod__ctor_m3207538458 (DialogResultMethod_t1789391825 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.DialogResultMethod::Invoke(System.Int32,PoqXert.MessageBox.DialogResult)
extern "C"  void DialogResultMethod_Invoke_m3390017135 (DialogResultMethod_t1789391825 * __this, int32_t ___id0, int32_t ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_DialogResultMethod_t1789391825(Il2CppObject* delegate, int32_t ___id0, int32_t ___result1);
// System.IAsyncResult PoqXert.MessageBox.DialogResultMethod::BeginInvoke(System.Int32,PoqXert.MessageBox.DialogResult,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DialogResultMethod_BeginInvoke_m283683788 (DialogResultMethod_t1789391825 * __this, int32_t ___id0, int32_t ___result1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.DialogResultMethod::EndInvoke(System.IAsyncResult)
extern "C"  void DialogResultMethod_EndInvoke_m219559466 (DialogResultMethod_t1789391825 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
