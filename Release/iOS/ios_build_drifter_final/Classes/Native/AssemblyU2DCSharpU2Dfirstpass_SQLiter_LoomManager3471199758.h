﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SQLiter.LoomManager/NullLoom
struct NullLoom_t2453031144;
// SQLiter.LoomManager/LoomDispatcher
struct LoomDispatcher_t1654289480;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLiter.LoomManager
struct  LoomManager_t3471199758  : public MonoBehaviour_t3012272455
{
public:

public:
};

struct LoomManager_t3471199758_StaticFields
{
public:
	// SQLiter.LoomManager/NullLoom SQLiter.LoomManager::_nullLoom
	NullLoom_t2453031144 * ____nullLoom_2;
	// SQLiter.LoomManager/LoomDispatcher SQLiter.LoomManager::_loom
	LoomDispatcher_t1654289480 * ____loom_3;

public:
	inline static int32_t get_offset_of__nullLoom_2() { return static_cast<int32_t>(offsetof(LoomManager_t3471199758_StaticFields, ____nullLoom_2)); }
	inline NullLoom_t2453031144 * get__nullLoom_2() const { return ____nullLoom_2; }
	inline NullLoom_t2453031144 ** get_address_of__nullLoom_2() { return &____nullLoom_2; }
	inline void set__nullLoom_2(NullLoom_t2453031144 * value)
	{
		____nullLoom_2 = value;
		Il2CppCodeGenWriteBarrier(&____nullLoom_2, value);
	}

	inline static int32_t get_offset_of__loom_3() { return static_cast<int32_t>(offsetof(LoomManager_t3471199758_StaticFields, ____loom_3)); }
	inline LoomDispatcher_t1654289480 * get__loom_3() const { return ____loom_3; }
	inline LoomDispatcher_t1654289480 ** get_address_of__loom_3() { return &____loom_3; }
	inline void set__loom_3(LoomDispatcher_t1654289480 * value)
	{
		____loom_3 = value;
		Il2CppCodeGenWriteBarrier(&____loom_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
