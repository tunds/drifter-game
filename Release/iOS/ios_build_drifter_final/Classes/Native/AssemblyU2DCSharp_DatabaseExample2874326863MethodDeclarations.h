﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DatabaseExample
struct DatabaseExample_t2874326863;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void DatabaseExample::.ctor()
extern "C"  void DatabaseExample__ctor_m4045787388 (DatabaseExample_t2874326863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseExample::OnEnable()
extern "C"  void DatabaseExample_OnEnable_m456426762 (DatabaseExample_t2874326863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseExample::OnDisable()
extern "C"  void DatabaseExample_OnDisable_m1705264995 (DatabaseExample_t2874326863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseExample::OnError(System.String)
extern "C"  void DatabaseExample_OnError_m320034527 (DatabaseExample_t2874326863 * __this, String_t* ___err0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseExample::Start()
extern "C"  void DatabaseExample_Start_m2992925180 (DatabaseExample_t2874326863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseExample::OnGUI()
extern "C"  void DatabaseExample_OnGUI_m3541186038 (DatabaseExample_t2874326863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseExample::CreateDB()
extern "C"  void DatabaseExample_CreateDB_m1707947330 (DatabaseExample_t2874326863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DatabaseExample::CreateTable()
extern "C"  bool DatabaseExample_CreateTable_m665722560 (DatabaseExample_t2874326863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseExample::AddRow(System.String,System.String)
extern "C"  void DatabaseExample_AddRow_m2730362077 (DatabaseExample_t2874326863 * __this, String_t* ___id0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseExample::UpdateRow(System.String,System.String)
extern "C"  void DatabaseExample_UpdateRow_m2923826995 (DatabaseExample_t2874326863 * __this, String_t* ___id0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseExample::DeleteRow(System.String)
extern "C"  void DatabaseExample_DeleteRow_m2634804121 (DatabaseExample_t2874326863 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseExample::Refresh()
extern "C"  void DatabaseExample_Refresh_m139107317 (DatabaseExample_t2874326863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseExample::OnApplicationQuit()
extern "C"  void DatabaseExample_OnApplicationQuit_m2364580986 (DatabaseExample_t2874326863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
