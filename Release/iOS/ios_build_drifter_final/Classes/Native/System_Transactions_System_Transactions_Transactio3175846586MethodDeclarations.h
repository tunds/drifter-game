﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Transactions.Transaction
struct Transaction_t3175846586;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Transactions.TransactionInformation
struct TransactionInformation_t3294574474;
// System.Transactions.Enlistment
struct Enlistment_t3082063553;
// System.Transactions.IEnlistmentNotification
struct IEnlistmentNotification_t1752309717;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;
// System.Transactions.TransactionScope
struct TransactionScope_t3316380210;
// System.Transactions.ISinglePhaseNotification
struct ISinglePhaseNotification_t2163935249;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "System_Transactions_System_Transactions_Transactio3175846586.h"
#include "System_Transactions_System_Transactions_Enlistment1495500821.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"
#include "System_Transactions_System_Transactions_Transactio3316380210.h"

// System.Void System.Transactions.Transaction::.ctor()
extern "C"  void Transaction__ctor_m651561089 (Transaction_t3175846586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Transaction_System_Runtime_Serialization_ISerializable_GetObjectData_m16758250 (Transaction_t3175846586 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Transactions.Transaction System.Transactions.Transaction::get_Current()
extern "C"  Transaction_t3175846586 * Transaction_get_Current_m308715390 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Transactions.Transaction System.Transactions.Transaction::get_CurrentInternal()
extern "C"  Transaction_t3175846586 * Transaction_get_CurrentInternal_m4103191067 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::set_CurrentInternal(System.Transactions.Transaction)
extern "C"  void Transaction_set_CurrentInternal_m529514518 (Il2CppObject * __this /* static, unused */, Transaction_t3175846586 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Transactions.TransactionInformation System.Transactions.Transaction::get_TransactionInformation()
extern "C"  TransactionInformation_t3294574474 * Transaction_get_TransactionInformation_m2830477433 (Transaction_t3175846586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::Dispose()
extern "C"  void Transaction_Dispose_m3278231678 (Transaction_t3175846586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Transactions.Enlistment System.Transactions.Transaction::EnlistVolatile(System.Transactions.IEnlistmentNotification,System.Transactions.EnlistmentOptions)
extern "C"  Enlistment_t3082063553 * Transaction_EnlistVolatile_m3347489508 (Transaction_t3175846586 * __this, Il2CppObject * ___notification0, int32_t ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Transactions.Enlistment System.Transactions.Transaction::EnlistVolatileInternal(System.Transactions.IEnlistmentNotification,System.Transactions.EnlistmentOptions)
extern "C"  Enlistment_t3082063553 * Transaction_EnlistVolatileInternal_m2333899751 (Transaction_t3175846586 * __this, Il2CppObject * ___notification0, int32_t ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.Transaction::Equals(System.Object)
extern "C"  bool Transaction_Equals_m790901490 (Transaction_t3175846586 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.Transaction::Equals(System.Transactions.Transaction)
extern "C"  bool Transaction_Equals_m3895543116 (Transaction_t3175846586 * __this, Transaction_t3175846586 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Transactions.Transaction::GetHashCode()
extern "C"  int32_t Transaction_GetHashCode_m1183983114 (Transaction_t3175846586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::Rollback()
extern "C"  void Transaction_Rollback_m3171304839 (Transaction_t3175846586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::Rollback(System.Exception)
extern "C"  void Transaction_Rollback_m490334263 (Transaction_t3175846586 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::Rollback(System.Exception,System.Transactions.IEnlistmentNotification)
extern "C"  void Transaction_Rollback_m1929892516 (Transaction_t3175846586 * __this, Exception_t1967233988 * ___ex0, Il2CppObject * ___enlisted1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::set_Aborted(System.Boolean)
extern "C"  void Transaction_set_Aborted_m4230882120 (Transaction_t3175846586 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Transactions.TransactionScope System.Transactions.Transaction::get_Scope()
extern "C"  TransactionScope_t3316380210 * Transaction_get_Scope_m2741316771 (Transaction_t3175846586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::set_Scope(System.Transactions.TransactionScope)
extern "C"  void Transaction_set_Scope_m1673949798 (Transaction_t3175846586 * __this, TransactionScope_t3316380210 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::CommitInternal()
extern "C"  void Transaction_CommitInternal_m578594359 (Transaction_t3175846586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::DoCommit()
extern "C"  void Transaction_DoCommit_m1880480965 (Transaction_t3175846586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::Complete()
extern "C"  void Transaction_Complete_m3112384156 (Transaction_t3175846586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::DoPreparePhase()
extern "C"  void Transaction_DoPreparePhase_m1564783426 (Transaction_t3175846586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::DoCommitPhase()
extern "C"  void Transaction_DoCommitPhase_m3294991960 (Transaction_t3175846586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::DoSingleCommit(System.Transactions.ISinglePhaseNotification)
extern "C"  void Transaction_DoSingleCommit_m3243763006 (Transaction_t3175846586 * __this, Il2CppObject * ___single0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::CheckAborted()
extern "C"  void Transaction_CheckAborted_m1722162826 (Transaction_t3175846586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.Transaction::EnsureIncompleteCurrentScope()
extern "C"  void Transaction_EnsureIncompleteCurrentScope_m144438170 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.Transaction::op_Equality(System.Transactions.Transaction,System.Transactions.Transaction)
extern "C"  bool Transaction_op_Equality_m825383295 (Il2CppObject * __this /* static, unused */, Transaction_t3175846586 * ___x0, Transaction_t3175846586 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.Transaction::op_Inequality(System.Transactions.Transaction,System.Transactions.Transaction)
extern "C"  bool Transaction_op_Inequality_m4227681530 (Il2CppObject * __this /* static, unused */, Transaction_t3175846586 * ___x0, Transaction_t3175846586 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
