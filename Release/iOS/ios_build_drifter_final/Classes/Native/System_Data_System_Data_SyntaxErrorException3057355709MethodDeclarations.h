﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.SyntaxErrorException
struct SyntaxErrorException_t3057355709;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void System.Data.SyntaxErrorException::.ctor()
extern "C"  void SyntaxErrorException__ctor_m4135207399 (SyntaxErrorException_t3057355709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.SyntaxErrorException::.ctor(System.String)
extern "C"  void SyntaxErrorException__ctor_m3581537403 (SyntaxErrorException_t3057355709 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.SyntaxErrorException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SyntaxErrorException__ctor_m2979986088 (SyntaxErrorException_t3057355709 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
