﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteKeyReader/KeyQuery
struct KeyQuery_t552866249;
// Mono.Data.Sqlite.SqliteConnection
struct SqliteConnection_t3853176977;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t2956870243;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection3853176977.h"
#include "mscorlib_System_String968488902.h"

// System.Void Mono.Data.Sqlite.SqliteKeyReader/KeyQuery::.ctor(Mono.Data.Sqlite.SqliteConnection,System.String,System.String,System.String[])
extern "C"  void KeyQuery__ctor_m1446771144 (KeyQuery_t552866249 * __this, SqliteConnection_t3853176977 * ___cnn0, String_t* ___database1, String_t* ___table2, StringU5BU5D_t2956870243* ___columns3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteKeyReader/KeyQuery::set_IsValid(System.Boolean)
extern "C"  void KeyQuery_set_IsValid_m3434526483 (KeyQuery_t552866249 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteKeyReader/KeyQuery::Sync(System.Int64)
extern "C"  void KeyQuery_Sync_m1717626536 (KeyQuery_t552866249 * __this, int64_t ___rowid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteKeyReader/KeyQuery::Dispose()
extern "C"  void KeyQuery_Dispose_m4002763558 (KeyQuery_t552866249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
