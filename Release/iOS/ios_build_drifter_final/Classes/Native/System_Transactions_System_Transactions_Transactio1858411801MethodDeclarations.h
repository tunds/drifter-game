﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Transactions.TransactionCompletedEventHandler
struct TransactionCompletedEventHandler_t1858411801;
// System.Object
struct Il2CppObject;
// System.Transactions.TransactionEventArgs
struct TransactionEventArgs_t2318060917;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_Transactions_System_Transactions_Transactio2318060917.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Transactions.TransactionCompletedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void TransactionCompletedEventHandler__ctor_m426193240 (TransactionCompletedEventHandler_t1858411801 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.TransactionCompletedEventHandler::Invoke(System.Object,System.Transactions.TransactionEventArgs)
extern "C"  void TransactionCompletedEventHandler_Invoke_m3537370257 (TransactionCompletedEventHandler_t1858411801 * __this, Il2CppObject * ___o0, TransactionEventArgs_t2318060917 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_TransactionCompletedEventHandler_t1858411801(Il2CppObject* delegate, Il2CppObject * ___o0, TransactionEventArgs_t2318060917 * ___e1);
// System.IAsyncResult System.Transactions.TransactionCompletedEventHandler::BeginInvoke(System.Object,System.Transactions.TransactionEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TransactionCompletedEventHandler_BeginInvoke_m4102746350 (TransactionCompletedEventHandler_t1858411801 * __this, Il2CppObject * ___o0, TransactionEventArgs_t2318060917 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.TransactionCompletedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void TransactionCompletedEventHandler_EndInvoke_m855183208 (TransactionCompletedEventHandler_t1858411801 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
