﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteParameterCollection
struct SqliteParameterCollection_t534758338;
// Mono.Data.Sqlite.SqliteCommand
struct SqliteCommand_t4229878246;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// Mono.Data.Sqlite.SqliteParameter
struct SqliteParameter_t3651135812;
// System.String
struct String_t;
// System.Array
struct Il2CppArray;
// System.Data.Common.DbParameter
struct DbParameter_t3306161371;
// Mono.Data.Sqlite.SqliteStatement
struct SqliteStatement_t3906494218;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteCommand4229878246.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteParameter3651135812.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Data_System_Data_Common_DbParameter3306161371.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteStatement3906494218.h"

// System.Void Mono.Data.Sqlite.SqliteParameterCollection::.ctor(Mono.Data.Sqlite.SqliteCommand)
extern "C"  void SqliteParameterCollection__ctor_m4126965866 (SqliteParameterCollection_t534758338 * __this, SqliteCommand_t4229878246 * ___cmd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteParameterCollection::get_IsSynchronized()
extern "C"  bool SqliteParameterCollection_get_IsSynchronized_m2702118157 (SqliteParameterCollection_t534758338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteParameterCollection::get_IsFixedSize()
extern "C"  bool SqliteParameterCollection_get_IsFixedSize_m1894817886 (SqliteParameterCollection_t534758338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteParameterCollection::get_IsReadOnly()
extern "C"  bool SqliteParameterCollection_get_IsReadOnly_m653679323 (SqliteParameterCollection_t534758338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteParameterCollection::get_SyncRoot()
extern "C"  Il2CppObject * SqliteParameterCollection_get_SyncRoot_m4117497919 (SqliteParameterCollection_t534758338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Mono.Data.Sqlite.SqliteParameterCollection::GetEnumerator()
extern "C"  Il2CppObject * SqliteParameterCollection_GetEnumerator_m2981568162 (SqliteParameterCollection_t534758338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteParameterCollection::Add(Mono.Data.Sqlite.SqliteParameter)
extern "C"  int32_t SqliteParameterCollection_Add_m53660911 (SqliteParameterCollection_t534758338 * __this, SqliteParameter_t3651135812 * ___parameter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteParameterCollection::Add(System.Object)
extern "C"  int32_t SqliteParameterCollection_Add_m658058929 (SqliteParameterCollection_t534758338 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteParameter Mono.Data.Sqlite.SqliteParameterCollection::AddWithValue(System.String,System.Object)
extern "C"  SqliteParameter_t3651135812 * SqliteParameterCollection_AddWithValue_m486664619 (SqliteParameterCollection_t534758338 * __this, String_t* ___parameterName0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameterCollection::AddRange(System.Array)
extern "C"  void SqliteParameterCollection_AddRange_m3686913318 (SqliteParameterCollection_t534758338 * __this, Il2CppArray * ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameterCollection::Clear()
extern "C"  void SqliteParameterCollection_Clear_m4001597387 (SqliteParameterCollection_t534758338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteParameterCollection::Contains(System.String)
extern "C"  bool SqliteParameterCollection_Contains_m1378426653 (SqliteParameterCollection_t534758338 * __this, String_t* ___parameterName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteParameterCollection::Contains(System.Object)
extern "C"  bool SqliteParameterCollection_Contains_m1600537007 (SqliteParameterCollection_t534758338 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameterCollection::CopyTo(System.Array,System.Int32)
extern "C"  void SqliteParameterCollection_CopyTo_m735931517 (SqliteParameterCollection_t534758338 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteParameterCollection::get_Count()
extern "C"  int32_t SqliteParameterCollection_get_Count_m494624776 (SqliteParameterCollection_t534758338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteParameter Mono.Data.Sqlite.SqliteParameterCollection::get_Item(System.Int32)
extern "C"  SqliteParameter_t3651135812 * SqliteParameterCollection_get_Item_m3350253192 (SqliteParameterCollection_t534758338 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameter Mono.Data.Sqlite.SqliteParameterCollection::GetParameter(System.Int32)
extern "C"  DbParameter_t3306161371 * SqliteParameterCollection_GetParameter_m15636166 (SqliteParameterCollection_t534758338 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteParameterCollection::IndexOf(System.String)
extern "C"  int32_t SqliteParameterCollection_IndexOf_m380862967 (SqliteParameterCollection_t534758338 * __this, String_t* ___parameterName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteParameterCollection::IndexOf(System.Object)
extern "C"  int32_t SqliteParameterCollection_IndexOf_m602973321 (SqliteParameterCollection_t534758338 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameterCollection::Insert(System.Int32,System.Object)
extern "C"  void SqliteParameterCollection_Insert_m3286760828 (SqliteParameterCollection_t534758338 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameterCollection::Remove(System.Object)
extern "C"  void SqliteParameterCollection_Remove_m282161708 (SqliteParameterCollection_t534758338 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameterCollection::RemoveAt(System.String)
extern "C"  void SqliteParameterCollection_RemoveAt_m2932546727 (SqliteParameterCollection_t534758338 * __this, String_t* ___parameterName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameterCollection::RemoveAt(System.Int32)
extern "C"  void SqliteParameterCollection_RemoveAt_m1742434060 (SqliteParameterCollection_t534758338 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameterCollection::SetParameter(System.Int32,System.Data.Common.DbParameter)
extern "C"  void SqliteParameterCollection_SetParameter_m3661558653 (SqliteParameterCollection_t534758338 * __this, int32_t ___index0, DbParameter_t3306161371 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameterCollection::Unbind()
extern "C"  void SqliteParameterCollection_Unbind_m2497728954 (SqliteParameterCollection_t534758338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteParameterCollection::MapParameters(Mono.Data.Sqlite.SqliteStatement)
extern "C"  void SqliteParameterCollection_MapParameters_m3071518410 (SqliteParameterCollection_t534758338 * __this, SqliteStatement_t3906494218 * ___activeStatement0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
