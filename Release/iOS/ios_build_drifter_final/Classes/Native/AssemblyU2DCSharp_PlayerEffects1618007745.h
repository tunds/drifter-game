﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.Animation
struct Animation_t350396337;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerEffects
struct  PlayerEffects_t1618007745  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GameObject PlayerEffects::hitEffect
	GameObject_t4012695102 * ___hitEffect_2;
	// UnityEngine.GameObject PlayerEffects::stunEffect
	GameObject_t4012695102 * ___stunEffect_3;
	// UnityEngine.GameObject PlayerEffects::normalAttackEffect
	GameObject_t4012695102 * ___normalAttackEffect_4;
	// UnityEngine.GameObject PlayerEffects::specialAttackEffect
	GameObject_t4012695102 * ___specialAttackEffect_5;
	// UnityEngine.Animation PlayerEffects::playerAnimation
	Animation_t350396337 * ___playerAnimation_6;

public:
	inline static int32_t get_offset_of_hitEffect_2() { return static_cast<int32_t>(offsetof(PlayerEffects_t1618007745, ___hitEffect_2)); }
	inline GameObject_t4012695102 * get_hitEffect_2() const { return ___hitEffect_2; }
	inline GameObject_t4012695102 ** get_address_of_hitEffect_2() { return &___hitEffect_2; }
	inline void set_hitEffect_2(GameObject_t4012695102 * value)
	{
		___hitEffect_2 = value;
		Il2CppCodeGenWriteBarrier(&___hitEffect_2, value);
	}

	inline static int32_t get_offset_of_stunEffect_3() { return static_cast<int32_t>(offsetof(PlayerEffects_t1618007745, ___stunEffect_3)); }
	inline GameObject_t4012695102 * get_stunEffect_3() const { return ___stunEffect_3; }
	inline GameObject_t4012695102 ** get_address_of_stunEffect_3() { return &___stunEffect_3; }
	inline void set_stunEffect_3(GameObject_t4012695102 * value)
	{
		___stunEffect_3 = value;
		Il2CppCodeGenWriteBarrier(&___stunEffect_3, value);
	}

	inline static int32_t get_offset_of_normalAttackEffect_4() { return static_cast<int32_t>(offsetof(PlayerEffects_t1618007745, ___normalAttackEffect_4)); }
	inline GameObject_t4012695102 * get_normalAttackEffect_4() const { return ___normalAttackEffect_4; }
	inline GameObject_t4012695102 ** get_address_of_normalAttackEffect_4() { return &___normalAttackEffect_4; }
	inline void set_normalAttackEffect_4(GameObject_t4012695102 * value)
	{
		___normalAttackEffect_4 = value;
		Il2CppCodeGenWriteBarrier(&___normalAttackEffect_4, value);
	}

	inline static int32_t get_offset_of_specialAttackEffect_5() { return static_cast<int32_t>(offsetof(PlayerEffects_t1618007745, ___specialAttackEffect_5)); }
	inline GameObject_t4012695102 * get_specialAttackEffect_5() const { return ___specialAttackEffect_5; }
	inline GameObject_t4012695102 ** get_address_of_specialAttackEffect_5() { return &___specialAttackEffect_5; }
	inline void set_specialAttackEffect_5(GameObject_t4012695102 * value)
	{
		___specialAttackEffect_5 = value;
		Il2CppCodeGenWriteBarrier(&___specialAttackEffect_5, value);
	}

	inline static int32_t get_offset_of_playerAnimation_6() { return static_cast<int32_t>(offsetof(PlayerEffects_t1618007745, ___playerAnimation_6)); }
	inline Animation_t350396337 * get_playerAnimation_6() const { return ___playerAnimation_6; }
	inline Animation_t350396337 ** get_address_of_playerAnimation_6() { return &___playerAnimation_6; }
	inline void set_playerAnimation_6(Animation_t350396337 * value)
	{
		___playerAnimation_6 = value;
		Il2CppCodeGenWriteBarrier(&___playerAnimation_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
