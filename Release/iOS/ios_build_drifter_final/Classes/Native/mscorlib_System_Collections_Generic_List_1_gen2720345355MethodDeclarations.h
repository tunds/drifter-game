﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::.ctor()
#define List_1__ctor_m968611477(__this, method) ((  void (*) (List_1_t2720345355 *, const MethodInfo*))List_1__ctor_m3048469268_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m2551716577(__this, ___collection0, method) ((  void (*) (List_1_t2720345355 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::.ctor(System.Int32)
#define List_1__ctor_m2575321903(__this, ___capacity0, method) ((  void (*) (List_1_t2720345355 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::.cctor()
#define List_1__cctor_m206845071(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1715150440(__this, method) ((  Il2CppObject* (*) (List_1_t2720345355 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m3568662566(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2720345355 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1596291125(__this, method) ((  Il2CppObject * (*) (List_1_t2720345355 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1493502888(__this, ___item0, method) ((  int32_t (*) (List_1_t2720345355 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m242486040(__this, ___item0, method) ((  bool (*) (List_1_t2720345355 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m2718379520(__this, ___item0, method) ((  int32_t (*) (List_1_t2720345355 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3201600627(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2720345355 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3312770517(__this, ___item0, method) ((  void (*) (List_1_t2720345355 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4131323161(__this, method) ((  bool (*) (List_1_t2720345355 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m4056569156(__this, method) ((  bool (*) (List_1_t2720345355 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m4038769014(__this, method) ((  Il2CppObject * (*) (List_1_t2720345355 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1254616199(__this, method) ((  bool (*) (List_1_t2720345355 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m3126879634(__this, method) ((  bool (*) (List_1_t2720345355 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3967165821(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2720345355 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2155571082(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2720345355 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::Add(T)
#define List_1_Add_m2542811617(__this, ___item0, method) ((  void (*) (List_1_t2720345355 *, MSGBoxStyle_t1923386386 *, const MethodInfo*))List_1_Add_m642669291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m4233932188(__this, ___newCount0, method) ((  void (*) (List_1_t2720345355 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m852464794(__this, ___collection0, method) ((  void (*) (List_1_t2720345355 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m113437018(__this, ___enumerable0, method) ((  void (*) (List_1_t2720345355 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1318327549(__this, ___collection0, method) ((  void (*) (List_1_t2720345355 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::AsReadOnly()
#define List_1_AsReadOnly_m3035800296(__this, method) ((  ReadOnlyCollection_1_t791564438 * (*) (List_1_t2720345355 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::Clear()
#define List_1_Clear_m3524439817(__this, method) ((  void (*) (List_1_t2720345355 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::Contains(T)
#define List_1_Contains_m3651846907(__this, ___item0, method) ((  bool (*) (List_1_t2720345355 *, MSGBoxStyle_t1923386386 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::CopyTo(T[])
#define List_1_CopyTo_m1523706630(__this, ___array0, method) ((  void (*) (List_1_t2720345355 *, MSGBoxStyleU5BU5D_t4110104423*, const MethodInfo*))List_1_CopyTo_m3016810556_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1396296593(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2720345355 *, MSGBoxStyleU5BU5D_t4110104423*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::Find(System.Predicate`1<T>)
#define List_1_Find_m3561842069(__this, ___match0, method) ((  MSGBoxStyle_t1923386386 * (*) (List_1_t2720345355 *, Predicate_1_t2494350284 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m1165346418(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2494350284 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m4223213391(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2720345355 *, int32_t, int32_t, Predicate_1_t2494350284 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::GetEnumerator()
#define List_1_GetEnumerator_m1087770296(__this, method) ((  Enumerator_t806128347  (*) (List_1_t2720345355 *, const MethodInfo*))List_1_GetEnumerator_m2326457258_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::IndexOf(T)
#define List_1_IndexOf_m1113399837(__this, ___item0, method) ((  int32_t (*) (List_1_t2720345355 *, MSGBoxStyle_t1923386386 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3042541800(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2720345355 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1142663777(__this, ___index0, method) ((  void (*) (List_1_t2720345355 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::Insert(System.Int32,T)
#define List_1_Insert_m1833407048(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2720345355 *, int32_t, MSGBoxStyle_t1923386386 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m3701549245(__this, ___collection0, method) ((  void (*) (List_1_t2720345355 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::Remove(T)
#define List_1_Remove_m387581366(__this, ___item0, method) ((  bool (*) (List_1_t2720345355 *, MSGBoxStyle_t1923386386 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2846934560(__this, ___match0, method) ((  int32_t (*) (List_1_t2720345355 *, Predicate_1_t2494350284 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m4002227214(__this, ___index0, method) ((  void (*) (List_1_t2720345355 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::Reverse()
#define List_1_Reverse_m4215121822(__this, method) ((  void (*) (List_1_t2720345355 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::Sort()
#define List_1_Sort_m3068783812(__this, method) ((  void (*) (List_1_t2720345355 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3914433952(__this, ___comparer0, method) ((  void (*) (List_1_t2720345355 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3807399447(__this, ___comparison0, method) ((  void (*) (List_1_t2720345355 *, Comparison_1_t332093966 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::ToArray()
#define List_1_ToArray_m1174659703(__this, method) ((  MSGBoxStyleU5BU5D_t4110104423* (*) (List_1_t2720345355 *, const MethodInfo*))List_1_ToArray_m238588755_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::TrimExcess()
#define List_1_TrimExcess_m3409799517(__this, method) ((  void (*) (List_1_t2720345355 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::get_Capacity()
#define List_1_get_Capacity_m2360656845(__this, method) ((  int32_t (*) (List_1_t2720345355 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1444121006(__this, ___value0, method) ((  void (*) (List_1_t2720345355 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::get_Count()
#define List_1_get_Count_m4267931390(__this, method) ((  int32_t (*) (List_1_t2720345355 *, const MethodInfo*))List_1_get_Count_m2599103100_gshared)(__this, method)
// T System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::get_Item(System.Int32)
#define List_1_get_Item_m4281281524(__this, ___index0, method) ((  MSGBoxStyle_t1923386386 * (*) (List_1_t2720345355 *, int32_t, const MethodInfo*))List_1_get_Item_m2771401372_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::set_Item(System.Int32,T)
#define List_1_set_Item_m2777178399(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2720345355 *, int32_t, MSGBoxStyle_t1923386386 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
