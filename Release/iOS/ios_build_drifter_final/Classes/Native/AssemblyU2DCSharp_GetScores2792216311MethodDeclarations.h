﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GetScores
struct GetScores_t2792216311;

#include "codegen/il2cpp-codegen.h"

// System.Void GetScores::.ctor()
extern "C"  void GetScores__ctor_m3636077652 (GetScores_t2792216311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetScores::Awake()
extern "C"  void GetScores_Awake_m3873682871 (GetScores_t2792216311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
