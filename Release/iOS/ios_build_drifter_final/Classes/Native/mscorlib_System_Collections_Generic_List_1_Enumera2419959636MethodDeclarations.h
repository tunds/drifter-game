﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteFunction>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1859215988(__this, ___l0, method) ((  void (*) (Enumerator_t2419959636 *, List_1_t39209348 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteFunction>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3334404446(__this, method) ((  void (*) (Enumerator_t2419959636 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteFunction>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3932120906(__this, method) ((  Il2CppObject * (*) (Enumerator_t2419959636 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteFunction>::Dispose()
#define Enumerator_Dispose_m3500592473(__this, method) ((  void (*) (Enumerator_t2419959636 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteFunction>::VerifyState()
#define Enumerator_VerifyState_m3297916946(__this, method) ((  void (*) (Enumerator_t2419959636 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteFunction>::MoveNext()
#define Enumerator_MoveNext_m1956916938(__this, method) ((  bool (*) (Enumerator_t2419959636 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Mono.Data.Sqlite.SqliteFunction>::get_Current()
#define Enumerator_get_Current_m4079775305(__this, method) ((  SqliteFunction_t3537217675 * (*) (Enumerator_t2419959636 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
