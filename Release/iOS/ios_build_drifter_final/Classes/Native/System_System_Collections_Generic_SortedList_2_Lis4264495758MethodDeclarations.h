﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Object,System.Object>
struct GetEnumeratorU3Ec__Iterator3_t4264495758;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Object,System.Object>::.ctor()
extern "C"  void GetEnumeratorU3Ec__Iterator3__ctor_m4039984291_gshared (GetEnumeratorU3Ec__Iterator3_t4264495758 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3__ctor_m4039984291(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator3_t4264495758 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3__ctor_m4039984291_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * GetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m656968015_gshared (GetEnumeratorU3Ec__Iterator3_t4264495758 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m656968015(__this, method) ((  Il2CppObject * (*) (GetEnumeratorU3Ec__Iterator3_t4264495758 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m656968015_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * GetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m2967276259_gshared (GetEnumeratorU3Ec__Iterator3_t4264495758 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m2967276259(__this, method) ((  Il2CppObject * (*) (GetEnumeratorU3Ec__Iterator3_t4264495758 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m2967276259_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Object,System.Object>::MoveNext()
extern "C"  bool GetEnumeratorU3Ec__Iterator3_MoveNext_m337293647_gshared (GetEnumeratorU3Ec__Iterator3_t4264495758 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_MoveNext_m337293647(__this, method) ((  bool (*) (GetEnumeratorU3Ec__Iterator3_t4264495758 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_MoveNext_m337293647_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Object,System.Object>::Dispose()
extern "C"  void GetEnumeratorU3Ec__Iterator3_Dispose_m3967718432_gshared (GetEnumeratorU3Ec__Iterator3_t4264495758 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_Dispose_m3967718432(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator3_t4264495758 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_Dispose_m3967718432_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Object,System.Object>::Reset()
extern "C"  void GetEnumeratorU3Ec__Iterator3_Reset_m1686417232_gshared (GetEnumeratorU3Ec__Iterator3_t4264495758 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_Reset_m1686417232(__this, method) ((  void (*) (GetEnumeratorU3Ec__Iterator3_t4264495758 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_Reset_m1686417232_gshared)(__this, method)
