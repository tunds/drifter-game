﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType4014882752.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Condition1501727354.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLiteDatabase.SQLiteDB/DB_ConditionPair
struct  DB_ConditionPair_t1745523828 
{
public:
	// System.String SQLiteDatabase.SQLiteDB/DB_ConditionPair::fieldName
	String_t* ___fieldName_0;
	// System.String SQLiteDatabase.SQLiteDB/DB_ConditionPair::value
	String_t* ___value_1;
	// SQLiteDatabase.SQLiteDB/DB_Condition SQLiteDatabase.SQLiteDB/DB_ConditionPair::condition
	int32_t ___condition_2;

public:
	inline static int32_t get_offset_of_fieldName_0() { return static_cast<int32_t>(offsetof(DB_ConditionPair_t1745523828, ___fieldName_0)); }
	inline String_t* get_fieldName_0() const { return ___fieldName_0; }
	inline String_t** get_address_of_fieldName_0() { return &___fieldName_0; }
	inline void set_fieldName_0(String_t* value)
	{
		___fieldName_0 = value;
		Il2CppCodeGenWriteBarrier(&___fieldName_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(DB_ConditionPair_t1745523828, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}

	inline static int32_t get_offset_of_condition_2() { return static_cast<int32_t>(offsetof(DB_ConditionPair_t1745523828, ___condition_2)); }
	inline int32_t get_condition_2() const { return ___condition_2; }
	inline int32_t* get_address_of_condition_2() { return &___condition_2; }
	inline void set_condition_2(int32_t value)
	{
		___condition_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: SQLiteDatabase.SQLiteDB/DB_ConditionPair
struct DB_ConditionPair_t1745523828_marshaled_pinvoke
{
	char* ___fieldName_0;
	char* ___value_1;
	int32_t ___condition_2;
};
// Native definition for marshalling of: SQLiteDatabase.SQLiteDB/DB_ConditionPair
struct DB_ConditionPair_t1745523828_marshaled_com
{
	uint16_t* ___fieldName_0;
	uint16_t* ___value_1;
	int32_t ___condition_2;
};
