﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.SqlExpressions.yyParser.yyUnexpectedEof
struct yyUnexpectedEof_t4221696488;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Data.SqlExpressions.yyParser.yyUnexpectedEof::.ctor()
extern "C"  void yyUnexpectedEof__ctor_m3755659148 (yyUnexpectedEof_t4221696488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
