﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// moveOBJ
struct moveOBJ_t1243551590;

#include "codegen/il2cpp-codegen.h"

// System.Void moveOBJ::.ctor()
extern "C"  void moveOBJ__ctor_m1230441157 (moveOBJ_t1243551590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void moveOBJ::Update()
extern "C"  void moveOBJ_Update_m1215832296 (moveOBJ_t1243551590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
