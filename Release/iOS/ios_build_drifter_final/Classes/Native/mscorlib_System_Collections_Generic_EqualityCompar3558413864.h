﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_Generic_EqualityCompar3654850783.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct  DefaultComparer_t3558413864  : public EqualityComparer_1_t3654850783
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
