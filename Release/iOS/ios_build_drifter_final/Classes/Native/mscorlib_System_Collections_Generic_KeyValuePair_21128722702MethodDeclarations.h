﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Mono.Data.Sqlite.SqliteConnectionPool/Pool>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m4067669783(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1128722702 *, String_t*, Pool_t2493500 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Mono.Data.Sqlite.SqliteConnectionPool/Pool>::get_Key()
#define KeyValuePair_2_get_Key_m1095257841(__this, method) ((  String_t* (*) (KeyValuePair_2_t1128722702 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Mono.Data.Sqlite.SqliteConnectionPool/Pool>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3647819954(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1128722702 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Mono.Data.Sqlite.SqliteConnectionPool/Pool>::get_Value()
#define KeyValuePair_2_get_Value_m3448233073(__this, method) ((  Pool_t2493500 * (*) (KeyValuePair_2_t1128722702 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Mono.Data.Sqlite.SqliteConnectionPool/Pool>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3228367794(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1128722702 *, Pool_t2493500 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Mono.Data.Sqlite.SqliteConnectionPool/Pool>::ToString()
#define KeyValuePair_2_ToString_m2053028912(__this, method) ((  String_t* (*) (KeyValuePair_2_t1128722702 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
