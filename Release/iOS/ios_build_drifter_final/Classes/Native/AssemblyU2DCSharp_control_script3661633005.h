﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t792326996;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// control_script
struct  control_script_t3661633005  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Animator control_script::anim
	Animator_t792326996 * ___anim_2;
	// System.Boolean control_script::boolper
	bool ___boolper_3;
	// System.Boolean control_script::boolper2
	bool ___boolper2_4;
	// System.Boolean control_script::boolper3
	bool ___boolper3_5;

public:
	inline static int32_t get_offset_of_anim_2() { return static_cast<int32_t>(offsetof(control_script_t3661633005, ___anim_2)); }
	inline Animator_t792326996 * get_anim_2() const { return ___anim_2; }
	inline Animator_t792326996 ** get_address_of_anim_2() { return &___anim_2; }
	inline void set_anim_2(Animator_t792326996 * value)
	{
		___anim_2 = value;
		Il2CppCodeGenWriteBarrier(&___anim_2, value);
	}

	inline static int32_t get_offset_of_boolper_3() { return static_cast<int32_t>(offsetof(control_script_t3661633005, ___boolper_3)); }
	inline bool get_boolper_3() const { return ___boolper_3; }
	inline bool* get_address_of_boolper_3() { return &___boolper_3; }
	inline void set_boolper_3(bool value)
	{
		___boolper_3 = value;
	}

	inline static int32_t get_offset_of_boolper2_4() { return static_cast<int32_t>(offsetof(control_script_t3661633005, ___boolper2_4)); }
	inline bool get_boolper2_4() const { return ___boolper2_4; }
	inline bool* get_address_of_boolper2_4() { return &___boolper2_4; }
	inline void set_boolper2_4(bool value)
	{
		___boolper2_4 = value;
	}

	inline static int32_t get_offset_of_boolper3_5() { return static_cast<int32_t>(offsetof(control_script_t3661633005, ___boolper3_5)); }
	inline bool get_boolper3_5() const { return ___boolper3_5; }
	inline bool* get_address_of_boolper3_5() { return &___boolper3_5; }
	inline void set_boolper3_5(bool value)
	{
		___boolper3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
