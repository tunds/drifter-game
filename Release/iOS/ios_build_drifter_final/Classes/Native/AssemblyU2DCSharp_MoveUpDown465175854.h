﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveUpDown
struct  MoveUpDown_t465175854  : public MonoBehaviour_t3012272455
{
public:
	// System.Boolean MoveUpDown::Move
	bool ___Move_2;
	// UnityEngine.Vector3 MoveUpDown::MoveVector
	Vector3_t3525329789  ___MoveVector_3;
	// System.Single MoveUpDown::MoveRange
	float ___MoveRange_4;
	// System.Single MoveUpDown::MoveSpeed
	float ___MoveSpeed_5;
	// UnityEngine.Vector3 MoveUpDown::startPosition
	Vector3_t3525329789  ___startPosition_6;

public:
	inline static int32_t get_offset_of_Move_2() { return static_cast<int32_t>(offsetof(MoveUpDown_t465175854, ___Move_2)); }
	inline bool get_Move_2() const { return ___Move_2; }
	inline bool* get_address_of_Move_2() { return &___Move_2; }
	inline void set_Move_2(bool value)
	{
		___Move_2 = value;
	}

	inline static int32_t get_offset_of_MoveVector_3() { return static_cast<int32_t>(offsetof(MoveUpDown_t465175854, ___MoveVector_3)); }
	inline Vector3_t3525329789  get_MoveVector_3() const { return ___MoveVector_3; }
	inline Vector3_t3525329789 * get_address_of_MoveVector_3() { return &___MoveVector_3; }
	inline void set_MoveVector_3(Vector3_t3525329789  value)
	{
		___MoveVector_3 = value;
	}

	inline static int32_t get_offset_of_MoveRange_4() { return static_cast<int32_t>(offsetof(MoveUpDown_t465175854, ___MoveRange_4)); }
	inline float get_MoveRange_4() const { return ___MoveRange_4; }
	inline float* get_address_of_MoveRange_4() { return &___MoveRange_4; }
	inline void set_MoveRange_4(float value)
	{
		___MoveRange_4 = value;
	}

	inline static int32_t get_offset_of_MoveSpeed_5() { return static_cast<int32_t>(offsetof(MoveUpDown_t465175854, ___MoveSpeed_5)); }
	inline float get_MoveSpeed_5() const { return ___MoveSpeed_5; }
	inline float* get_address_of_MoveSpeed_5() { return &___MoveSpeed_5; }
	inline void set_MoveSpeed_5(float value)
	{
		___MoveSpeed_5 = value;
	}

	inline static int32_t get_offset_of_startPosition_6() { return static_cast<int32_t>(offsetof(MoveUpDown_t465175854, ___startPosition_6)); }
	inline Vector3_t3525329789  get_startPosition_6() const { return ___startPosition_6; }
	inline Vector3_t3525329789 * get_address_of_startPosition_6() { return &___startPosition_6; }
	inline void set_startPosition_6(Vector3_t3525329789  value)
	{
		___startPosition_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
