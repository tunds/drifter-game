﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct Comparison_1_t3552548233;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K848873357.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Comparison`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m4249853150_gshared (Comparison_1_t3552548233 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m4249853150(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t3552548233 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m4249853150_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m146228954_gshared (Comparison_1_t3552548233 * __this, KeyInfo_t848873357  ___x0, KeyInfo_t848873357  ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m146228954(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3552548233 *, KeyInfo_t848873357 , KeyInfo_t848873357 , const MethodInfo*))Comparison_1_Invoke_m146228954_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m1103060515_gshared (Comparison_1_t3552548233 * __this, KeyInfo_t848873357  ___x0, KeyInfo_t848873357  ___y1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m1103060515(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t3552548233 *, KeyInfo_t848873357 , KeyInfo_t848873357 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m1103060515_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m645838674_gshared (Comparison_1_t3552548233 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m645838674(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t3552548233 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m645838674_gshared)(__this, ___result0, method)
