﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::.ctor()
#define List_1__ctor_m873482819(__this, method) ((  void (*) (List_1_t2105969720 *, const MethodInfo*))List_1__ctor_m3048469268_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m919767644(__this, ___collection0, method) ((  void (*) (List_1_t2105969720 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::.ctor(System.Int32)
#define List_1__ctor_m3726941972(__this, ___capacity0, method) ((  void (*) (List_1_t2105969720 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::.cctor()
#define List_1__cctor_m3715980746(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3747720077(__this, method) ((  Il2CppObject* (*) (List_1_t2105969720 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1970123873(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2105969720 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1565438128(__this, method) ((  Il2CppObject * (*) (List_1_t2105969720 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1928269197(__this, ___item0, method) ((  int32_t (*) (List_1_t2105969720 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2646902739(__this, ___item0, method) ((  bool (*) (List_1_t2105969720 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m3517166949(__this, ___item0, method) ((  int32_t (*) (List_1_t2105969720 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1219516248(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2105969720 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3937419984(__this, ___item0, method) ((  void (*) (List_1_t2105969720 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m724765716(__this, method) ((  bool (*) (List_1_t2105969720 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2569886377(__this, method) ((  bool (*) (List_1_t2105969720 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m612956891(__this, method) ((  Il2CppObject * (*) (List_1_t2105969720 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1282888066(__this, method) ((  bool (*) (List_1_t2105969720 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m218297655(__this, method) ((  bool (*) (List_1_t2105969720 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1706664354(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2105969720 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m42994991(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2105969720 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::Add(T)
#define List_1_Add_m3654841820(__this, ___item0, method) ((  void (*) (List_1_t2105969720 *, SqliteFunctionAttribute_t1309010751 *, const MethodInfo*))List_1_Add_m642669291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m3845266967(__this, ___newCount0, method) ((  void (*) (List_1_t2105969720 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1619810581(__this, ___collection0, method) ((  void (*) (List_1_t2105969720 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m880782805(__this, ___enumerable0, method) ((  void (*) (List_1_t2105969720 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m2957455266(__this, ___collection0, method) ((  void (*) (List_1_t2105969720 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::AsReadOnly()
#define List_1_AsReadOnly_m2172595491(__this, method) ((  ReadOnlyCollection_1_t177188803 * (*) (List_1_t2105969720 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::Clear()
#define List_1_Clear_m3637637742(__this, method) ((  void (*) (List_1_t2105969720 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::Contains(T)
#define List_1_Contains_m4219654496(__this, ___item0, method) ((  bool (*) (List_1_t2105969720 *, SqliteFunctionAttribute_t1309010751 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::CopyTo(T[])
#define List_1_CopyTo_m2680615915(__this, ___array0, method) ((  void (*) (List_1_t2105969720 *, SqliteFunctionAttributeU5BU5D_t494148838*, const MethodInfo*))List_1_CopyTo_m3016810556_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m2115444428(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2105969720 *, SqliteFunctionAttributeU5BU5D_t494148838*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::Find(System.Predicate`1<T>)
#define List_1_Find_m4191171002(__this, ___match0, method) ((  SqliteFunctionAttribute_t1309010751 * (*) (List_1_t2105969720 *, Predicate_1_t1879974649 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m533915031(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1879974649 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m2966645876(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2105969720 *, int32_t, int32_t, Predicate_1_t1879974649 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::GetEnumerator()
#define List_1_GetEnumerator_m4051659486(__this, method) ((  Enumerator_t191752712  (*) (List_1_t2105969720 *, const MethodInfo*))List_1_GetEnumerator_m2326457258_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::IndexOf(T)
#define List_1_IndexOf_m3269819288(__this, ___item0, method) ((  int32_t (*) (List_1_t2105969720 *, SqliteFunctionAttribute_t1309010751 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m251226403(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2105969720 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1861811612(__this, ___index0, method) ((  void (*) (List_1_t2105969720 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::Insert(System.Int32,T)
#define List_1_Insert_m1548122179(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2105969720 *, int32_t, SqliteFunctionAttribute_t1309010751 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2386475640(__this, ___collection0, method) ((  void (*) (List_1_t2105969720 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::Remove(T)
#define List_1_Remove_m3892078939(__this, ___item0, method) ((  bool (*) (List_1_t2105969720 *, SqliteFunctionAttribute_t1309010751 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2018836827(__this, ___match0, method) ((  int32_t (*) (List_1_t2105969720 *, Predicate_1_t1879974649 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m3716942345(__this, ___index0, method) ((  void (*) (List_1_t2105969720 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::Reverse()
#define List_1_Reverse_m1329178051(__this, method) ((  void (*) (List_1_t2105969720 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::Sort()
#define List_1_Sort_m4180814015(__this, method) ((  void (*) (List_1_t2105969720 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1803730949(__this, ___comparer0, method) ((  void (*) (List_1_t2105969720 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3962276818(__this, ___comparison0, method) ((  void (*) (List_1_t2105969720 *, Comparison_1_t4012685627 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::ToArray()
#define List_1_ToArray_m4022542428(__this, method) ((  SqliteFunctionAttributeU5BU5D_t494148838* (*) (List_1_t2105969720 *, const MethodInfo*))List_1_ToArray_m238588755_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::TrimExcess()
#define List_1_TrimExcess_m619281688(__this, method) ((  void (*) (List_1_t2105969720 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::get_Capacity()
#define List_1_get_Capacity_m1982719944(__this, method) ((  int32_t (*) (List_1_t2105969720 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1055455785(__this, ___value0, method) ((  void (*) (List_1_t2105969720 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::get_Count()
#define List_1_get_Count_m2397457635(__this, method) ((  int32_t (*) (List_1_t2105969720 *, const MethodInfo*))List_1_get_Count_m2599103100_gshared)(__this, method)
// T System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::get_Item(System.Int32)
#define List_1_get_Item_m3182206127(__this, ___index0, method) ((  SqliteFunctionAttribute_t1309010751 * (*) (List_1_t2105969720 *, int32_t, const MethodInfo*))List_1_get_Item_m2771401372_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteFunctionAttribute>::set_Item(System.Int32,T)
#define List_1_set_Item_m3496326234(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2105969720 *, int32_t, SqliteFunctionAttribute_t1309010751 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
