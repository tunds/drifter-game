﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2<System.Object,System.Object>
struct SortedList_2_t2672481741;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedList_2_Key3814447360.h"

// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C"  void KeyEnumerator__ctor_m3211575330_gshared (KeyEnumerator_t3814447360 * __this, SortedList_2_t2672481741 * ___l0, const MethodInfo* method);
#define KeyEnumerator__ctor_m3211575330(__this, ___l0, method) ((  void (*) (KeyEnumerator_t3814447360 *, SortedList_2_t2672481741 *, const MethodInfo*))KeyEnumerator__ctor_m3211575330_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void KeyEnumerator_System_Collections_IEnumerator_Reset_m1671842520_gshared (KeyEnumerator_t3814447360 * __this, const MethodInfo* method);
#define KeyEnumerator_System_Collections_IEnumerator_Reset_m1671842520(__this, method) ((  void (*) (KeyEnumerator_t3814447360 *, const MethodInfo*))KeyEnumerator_System_Collections_IEnumerator_Reset_m1671842520_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/KeyEnumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * KeyEnumerator_System_Collections_IEnumerator_get_Current_m3401402830_gshared (KeyEnumerator_t3814447360 * __this, const MethodInfo* method);
#define KeyEnumerator_System_Collections_IEnumerator_get_Current_m3401402830(__this, method) ((  Il2CppObject * (*) (KeyEnumerator_t3814447360 *, const MethodInfo*))KeyEnumerator_System_Collections_IEnumerator_get_Current_m3401402830_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Object,System.Object>::Dispose()
extern "C"  void KeyEnumerator_Dispose_m3990612383_gshared (KeyEnumerator_t3814447360 * __this, const MethodInfo* method);
#define KeyEnumerator_Dispose_m3990612383(__this, method) ((  void (*) (KeyEnumerator_t3814447360 *, const MethodInfo*))KeyEnumerator_Dispose_m3990612383_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/KeyEnumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool KeyEnumerator_MoveNext_m744157724_gshared (KeyEnumerator_t3814447360 * __this, const MethodInfo* method);
#define KeyEnumerator_MoveNext_m744157724(__this, method) ((  bool (*) (KeyEnumerator_t3814447360 *, const MethodInfo*))KeyEnumerator_MoveNext_m744157724_gshared)(__this, method)
// TKey System.Collections.Generic.SortedList`2/KeyEnumerator<System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * KeyEnumerator_get_Current_m2531132796_gshared (KeyEnumerator_t3814447360 * __this, const MethodInfo* method);
#define KeyEnumerator_get_Current_m2531132796(__this, method) ((  Il2CppObject * (*) (KeyEnumerator_t3814447360 *, const MethodInfo*))KeyEnumerator_get_Current_m2531132796_gshared)(__this, method)
