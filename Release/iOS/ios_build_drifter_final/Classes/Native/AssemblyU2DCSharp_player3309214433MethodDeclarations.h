﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// player
struct player_t3309214433;

#include "codegen/il2cpp-codegen.h"

// System.Void player::.ctor()
extern "C"  void player__ctor_m1868723930 (player_t3309214433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void player::Start()
extern "C"  void player_Start_m815861722 (player_t3309214433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void player::OnGUI()
extern "C"  void player_OnGUI_m1364122580 (player_t3309214433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void player::weaponSel()
extern "C"  void player_weaponSel_m420359606 (player_t3309214433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void player::disableFX()
extern "C"  void player_disableFX_m2175626194 (player_t3309214433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
