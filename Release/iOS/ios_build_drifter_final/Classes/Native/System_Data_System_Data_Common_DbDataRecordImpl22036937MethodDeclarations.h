﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbDataRecordImpl
struct DbDataRecordImpl_t22036937;
// System.Data.Common.SchemaInfo[]
struct SchemaInfoU5BU5D_t3721122278;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Data.Common.DbDataRecordImpl::.ctor(System.Data.Common.SchemaInfo[],System.Object[])
extern "C"  void DbDataRecordImpl__ctor_m2615395670 (DbDataRecordImpl_t22036937 * __this, SchemaInfoU5BU5D_t3721122278* ___schema0, ObjectU5BU5D_t11523773* ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.DbDataRecordImpl::get_FieldCount()
extern "C"  int32_t DbDataRecordImpl_get_FieldCount_m1576893829 (DbDataRecordImpl_t22036937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbDataRecordImpl::GetDataTypeName(System.Int32)
extern "C"  String_t* DbDataRecordImpl_GetDataTypeName_m2361999088 (DbDataRecordImpl_t22036937 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Data.Common.DbDataRecordImpl::GetFieldType(System.Int32)
extern "C"  Type_t * DbDataRecordImpl_GetFieldType_m3698343104 (DbDataRecordImpl_t22036937 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Data.Common.DbDataRecordImpl::GetInt16(System.Int32)
extern "C"  int16_t DbDataRecordImpl_GetInt16_m1863110268 (DbDataRecordImpl_t22036937 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.DbDataRecordImpl::GetInt32(System.Int32)
extern "C"  int32_t DbDataRecordImpl_GetInt32_m157892208 (DbDataRecordImpl_t22036937 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbDataRecordImpl::GetName(System.Int32)
extern "C"  String_t* DbDataRecordImpl_GetName_m1022921932 (DbDataRecordImpl_t22036937 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbDataRecordImpl::GetString(System.Int32)
extern "C"  String_t* DbDataRecordImpl_GetString_m530178066 (DbDataRecordImpl_t22036937 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Data.Common.DbDataRecordImpl::GetValue(System.Int32)
extern "C"  Il2CppObject * DbDataRecordImpl_GetValue_m3850045410 (DbDataRecordImpl_t22036937 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.DbDataRecordImpl::GetValues(System.Object[])
extern "C"  int32_t DbDataRecordImpl_GetValues_m2997264191 (DbDataRecordImpl_t22036937 * __this, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DbDataRecordImpl::IsDBNull(System.Int32)
extern "C"  bool DbDataRecordImpl_IsDBNull_m2297218177 (DbDataRecordImpl_t22036937 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
