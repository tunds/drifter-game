﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioClip
struct AudioClip_t3714538611;
// UnityEngine.UI.Slider
struct Slider_t1468074762;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.AudioSource
struct AudioSource_t3628549054;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1217738301;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerPickups
struct  PlayerPickups_t2873926454  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.AudioClip PlayerPickups::destroyAudio
	AudioClip_t3714538611 * ___destroyAudio_2;
	// UnityEngine.UI.Slider PlayerPickups::healthSlider
	Slider_t1468074762 * ___healthSlider_3;
	// UnityEngine.UI.Slider PlayerPickups::specialSlider
	Slider_t1468074762 * ___specialSlider_4;
	// UnityEngine.GameObject PlayerPickups::weapon
	GameObject_t4012695102 * ___weapon_5;
	// UnityEngine.GameObject PlayerPickups::oldWeapon
	GameObject_t4012695102 * ___oldWeapon_6;
	// UnityEngine.GameObject PlayerPickups::destroyedEffect
	GameObject_t4012695102 * ___destroyedEffect_7;
	// UnityEngine.AudioSource PlayerPickups::audio
	AudioSource_t3628549054 * ___audio_8;
	// UnityEngine.MeshRenderer PlayerPickups::weaponRenderer
	MeshRenderer_t1217738301 * ___weaponRenderer_9;
	// UnityEngine.MeshRenderer PlayerPickups::oldWeaponRenderer
	MeshRenderer_t1217738301 * ___oldWeaponRenderer_10;
	// System.Int32 PlayerPickups::numHits
	int32_t ___numHits_11;

public:
	inline static int32_t get_offset_of_destroyAudio_2() { return static_cast<int32_t>(offsetof(PlayerPickups_t2873926454, ___destroyAudio_2)); }
	inline AudioClip_t3714538611 * get_destroyAudio_2() const { return ___destroyAudio_2; }
	inline AudioClip_t3714538611 ** get_address_of_destroyAudio_2() { return &___destroyAudio_2; }
	inline void set_destroyAudio_2(AudioClip_t3714538611 * value)
	{
		___destroyAudio_2 = value;
		Il2CppCodeGenWriteBarrier(&___destroyAudio_2, value);
	}

	inline static int32_t get_offset_of_healthSlider_3() { return static_cast<int32_t>(offsetof(PlayerPickups_t2873926454, ___healthSlider_3)); }
	inline Slider_t1468074762 * get_healthSlider_3() const { return ___healthSlider_3; }
	inline Slider_t1468074762 ** get_address_of_healthSlider_3() { return &___healthSlider_3; }
	inline void set_healthSlider_3(Slider_t1468074762 * value)
	{
		___healthSlider_3 = value;
		Il2CppCodeGenWriteBarrier(&___healthSlider_3, value);
	}

	inline static int32_t get_offset_of_specialSlider_4() { return static_cast<int32_t>(offsetof(PlayerPickups_t2873926454, ___specialSlider_4)); }
	inline Slider_t1468074762 * get_specialSlider_4() const { return ___specialSlider_4; }
	inline Slider_t1468074762 ** get_address_of_specialSlider_4() { return &___specialSlider_4; }
	inline void set_specialSlider_4(Slider_t1468074762 * value)
	{
		___specialSlider_4 = value;
		Il2CppCodeGenWriteBarrier(&___specialSlider_4, value);
	}

	inline static int32_t get_offset_of_weapon_5() { return static_cast<int32_t>(offsetof(PlayerPickups_t2873926454, ___weapon_5)); }
	inline GameObject_t4012695102 * get_weapon_5() const { return ___weapon_5; }
	inline GameObject_t4012695102 ** get_address_of_weapon_5() { return &___weapon_5; }
	inline void set_weapon_5(GameObject_t4012695102 * value)
	{
		___weapon_5 = value;
		Il2CppCodeGenWriteBarrier(&___weapon_5, value);
	}

	inline static int32_t get_offset_of_oldWeapon_6() { return static_cast<int32_t>(offsetof(PlayerPickups_t2873926454, ___oldWeapon_6)); }
	inline GameObject_t4012695102 * get_oldWeapon_6() const { return ___oldWeapon_6; }
	inline GameObject_t4012695102 ** get_address_of_oldWeapon_6() { return &___oldWeapon_6; }
	inline void set_oldWeapon_6(GameObject_t4012695102 * value)
	{
		___oldWeapon_6 = value;
		Il2CppCodeGenWriteBarrier(&___oldWeapon_6, value);
	}

	inline static int32_t get_offset_of_destroyedEffect_7() { return static_cast<int32_t>(offsetof(PlayerPickups_t2873926454, ___destroyedEffect_7)); }
	inline GameObject_t4012695102 * get_destroyedEffect_7() const { return ___destroyedEffect_7; }
	inline GameObject_t4012695102 ** get_address_of_destroyedEffect_7() { return &___destroyedEffect_7; }
	inline void set_destroyedEffect_7(GameObject_t4012695102 * value)
	{
		___destroyedEffect_7 = value;
		Il2CppCodeGenWriteBarrier(&___destroyedEffect_7, value);
	}

	inline static int32_t get_offset_of_audio_8() { return static_cast<int32_t>(offsetof(PlayerPickups_t2873926454, ___audio_8)); }
	inline AudioSource_t3628549054 * get_audio_8() const { return ___audio_8; }
	inline AudioSource_t3628549054 ** get_address_of_audio_8() { return &___audio_8; }
	inline void set_audio_8(AudioSource_t3628549054 * value)
	{
		___audio_8 = value;
		Il2CppCodeGenWriteBarrier(&___audio_8, value);
	}

	inline static int32_t get_offset_of_weaponRenderer_9() { return static_cast<int32_t>(offsetof(PlayerPickups_t2873926454, ___weaponRenderer_9)); }
	inline MeshRenderer_t1217738301 * get_weaponRenderer_9() const { return ___weaponRenderer_9; }
	inline MeshRenderer_t1217738301 ** get_address_of_weaponRenderer_9() { return &___weaponRenderer_9; }
	inline void set_weaponRenderer_9(MeshRenderer_t1217738301 * value)
	{
		___weaponRenderer_9 = value;
		Il2CppCodeGenWriteBarrier(&___weaponRenderer_9, value);
	}

	inline static int32_t get_offset_of_oldWeaponRenderer_10() { return static_cast<int32_t>(offsetof(PlayerPickups_t2873926454, ___oldWeaponRenderer_10)); }
	inline MeshRenderer_t1217738301 * get_oldWeaponRenderer_10() const { return ___oldWeaponRenderer_10; }
	inline MeshRenderer_t1217738301 ** get_address_of_oldWeaponRenderer_10() { return &___oldWeaponRenderer_10; }
	inline void set_oldWeaponRenderer_10(MeshRenderer_t1217738301 * value)
	{
		___oldWeaponRenderer_10 = value;
		Il2CppCodeGenWriteBarrier(&___oldWeaponRenderer_10, value);
	}

	inline static int32_t get_offset_of_numHits_11() { return static_cast<int32_t>(offsetof(PlayerPickups_t2873926454, ___numHits_11)); }
	inline int32_t get_numHits_11() const { return ___numHits_11; }
	inline int32_t* get_address_of_numHits_11() { return &___numHits_11; }
	inline void set_numHits_11(int32_t value)
	{
		___numHits_11 = value;
	}
};

struct PlayerPickups_t2873926454_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayerPickups::<>f__switch$map0
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map0_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_12() { return static_cast<int32_t>(offsetof(PlayerPickups_t2873926454_StaticFields, ___U3CU3Ef__switchU24map0_12)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map0_12() const { return ___U3CU3Ef__switchU24map0_12; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map0_12() { return &___U3CU3Ef__switchU24map0_12; }
	inline void set_U3CU3Ef__switchU24map0_12(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map0_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
