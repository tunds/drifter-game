﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqlit175718547.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqliteClient.SqliteBusyException
struct  SqliteBusyException_t975985850  : public SqliteExecutionException_t175718547
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
