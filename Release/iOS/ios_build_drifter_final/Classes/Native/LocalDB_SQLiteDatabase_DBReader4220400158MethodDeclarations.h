﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLiteDatabase.DBReader
struct DBReader_t4220400158;
// Mono.Data.Sqlite.SqliteDataReader
struct SqliteDataReader_t1567858368;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteDataReader1567858368.h"
#include "mscorlib_System_String968488902.h"

// System.Void SQLiteDatabase.DBReader::.ctor(Mono.Data.Sqlite.SqliteDataReader)
extern "C"  void DBReader__ctor_m102303496 (DBReader_t4220400158 * __this, SqliteDataReader_t1567858368 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLiteDatabase.DBReader::Read()
extern "C"  bool DBReader_Read_m2141930664 (DBReader_t4220400158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLiteDatabase.DBReader::GetStringValue(System.String)
extern "C"  String_t* DBReader_GetStringValue_m1373122399 (DBReader_t4220400158 * __this, String_t* ___fieldName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLiteDatabase.DBReader::GetIntValue(System.String)
extern "C"  int32_t DBReader_GetIntValue_m2828894496 (DBReader_t4220400158 * __this, String_t* ___fieldName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiteDatabase.DBReader::Dispose()
extern "C"  void DBReader_Dispose_m400197349 (DBReader_t4220400158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
