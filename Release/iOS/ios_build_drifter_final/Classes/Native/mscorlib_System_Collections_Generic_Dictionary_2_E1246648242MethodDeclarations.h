﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enu38140093MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3512571448(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1246648242 *, Dictionary_2_t1479620301 *, const MethodInfo*))Enumerator__ctor_m3662731183_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m64266931(__this, method) ((  Il2CppObject * (*) (Enumerator_t1246648242 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1090819612_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m912492413(__this, method) ((  void (*) (Enumerator_t1246648242 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m446458278_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4124427124(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t1246648242 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2891646429_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1356713295(__this, method) ((  Il2CppObject * (*) (Enumerator_t1246648242 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3950628344_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3474822177(__this, method) ((  Il2CppObject * (*) (Enumerator_t1246648242 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m851185290_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::MoveNext()
#define Enumerator_MoveNext_m3177983661(__this, method) ((  bool (*) (Enumerator_t1246648242 *, const MethodInfo*))Enumerator_MoveNext_m2783827030_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::get_Current()
#define Enumerator_get_Current_m3574121824(__this, method) ((  KeyValuePair_2_t968151599  (*) (Enumerator_t1246648242 *, const MethodInfo*))Enumerator_get_Current_m366116006_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1399602038(__this, method) ((  int64_t (*) (Enumerator_t1246648242 *, const MethodInfo*))Enumerator_get_CurrentKey_m404890207_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3968109622(__this, method) ((  AggregateData_t2045614569 * (*) (Enumerator_t1246648242 *, const MethodInfo*))Enumerator_get_CurrentValue_m3979777247_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::Reset()
#define Enumerator_Reset_m1394105482(__this, method) ((  void (*) (Enumerator_t1246648242 *, const MethodInfo*))Enumerator_Reset_m1313692545_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::VerifyState()
#define Enumerator_VerifyState_m345152787(__this, method) ((  void (*) (Enumerator_t1246648242 *, const MethodInfo*))Enumerator_VerifyState_m1370769098_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3724218299(__this, method) ((  void (*) (Enumerator_t1246648242 *, const MethodInfo*))Enumerator_VerifyCurrent_m1499015090_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,Mono.Data.Sqlite.SqliteFunction/AggregateData>::Dispose()
#define Enumerator_Dispose_m2229000922(__this, method) ((  void (*) (Enumerator_t1246648242 *, const MethodInfo*))Enumerator_Dispose_m2261579793_gshared)(__this, method)
