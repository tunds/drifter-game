﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbCommandBuilder
struct DbCommandBuilder_t2534194750;
// System.String
struct String_t;
// System.Data.DataTable
struct DataTable_t2176726999;
// System.Data.DataRow
struct DataRow_t3654701923;
// System.Data.Common.DbCommand
struct DbCommand_t2323745021;
// System.Data.Common.DbParameter
struct DbParameter_t3306161371;
// System.Data.Common.DbDataAdapter
struct DbDataAdapter_t3684585719;
// System.Data.Common.RowUpdatingEventArgs
struct RowUpdatingEventArgs_t2786481031;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "System_Data_System_Data_DataTable2176726999.h"
#include "System_Data_System_Data_DataRow3654701923.h"
#include "System_Data_System_Data_Common_DbCommand2323745021.h"
#include "System_Data_System_Data_Common_DbDataAdapter3684585719.h"
#include "System_Data_System_Data_Common_RowUpdatingEventArg2786481031.h"

// System.Void System.Data.Common.DbCommandBuilder::.ctor()
extern "C"  void DbCommandBuilder__ctor_m1020735780 (DbCommandBuilder_t2534194750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::BuildCache(System.Boolean)
extern "C"  void DbCommandBuilder_BuildCache_m419923851 (DbCommandBuilder_t2534194750 * __this, bool ___closeConnection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbCommandBuilder::get_QuotedTableName()
extern "C"  String_t* DbCommandBuilder_get_QuotedTableName_m1340494253 (DbCommandBuilder_t2534194750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DbCommandBuilder::get_IsCommandGenerated()
extern "C"  bool DbCommandBuilder_get_IsCommandGenerated_m978825197 (DbCommandBuilder_t2534194750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbCommandBuilder::GetQuotedString(System.String)
extern "C"  String_t* DbCommandBuilder_GetQuotedString_m722348942 (DbCommandBuilder_t2534194750 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::BuildInformation(System.Data.DataTable)
extern "C"  void DbCommandBuilder_BuildInformation_m961283413 (DbCommandBuilder_t2534194750 * __this, DataTable_t2176726999 * ___schemaTable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DbCommandBuilder::IncludedInInsert(System.Data.DataRow)
extern "C"  bool DbCommandBuilder_IncludedInInsert_m3188795607 (DbCommandBuilder_t2534194750 * __this, DataRow_t3654701923 * ___schemaRow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DbCommandBuilder::IncludedInUpdate(System.Data.DataRow)
extern "C"  bool DbCommandBuilder_IncludedInUpdate_m1967332551 (DbCommandBuilder_t2534194750 * __this, DataRow_t3654701923 * ___schemaRow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DbCommandBuilder::IncludedInWhereClause(System.Data.DataRow)
extern "C"  bool DbCommandBuilder_IncludedInWhereClause_m1188938406 (DbCommandBuilder_t2534194750 * __this, DataRow_t3654701923 * ___schemaRow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::CreateDeleteCommand(System.Boolean)
extern "C"  DbCommand_t2323745021 * DbCommandBuilder_CreateDeleteCommand_m2175241729 (DbCommandBuilder_t2534194750 * __this, bool ___option0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::CreateInsertCommand(System.Boolean,System.Data.DataRow)
extern "C"  DbCommand_t2323745021 * DbCommandBuilder_CreateInsertCommand_m86459124 (DbCommandBuilder_t2534194750 * __this, bool ___option0, DataRow_t3654701923 * ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::CreateNewCommand(System.Data.Common.DbCommand&)
extern "C"  void DbCommandBuilder_CreateNewCommand_m1034183380 (DbCommandBuilder_t2534194750 * __this, DbCommand_t2323745021 ** ___command0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::CreateUpdateCommand(System.Boolean)
extern "C"  DbCommand_t2323745021 * DbCommandBuilder_CreateUpdateCommand_m3338877219 (DbCommandBuilder_t2534194750 * __this, bool ___option0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameter System.Data.Common.DbCommandBuilder::CreateParameter(System.Data.Common.DbCommand,System.Data.DataRow,System.Boolean)
extern "C"  DbParameter_t3306161371 * DbCommandBuilder_CreateParameter_m2182358710 (DbCommandBuilder_t2534194750 * __this, DbCommand_t2323745021 * ____dbCommand0, DataRow_t3654701923 * ___schemaRow1, bool ___whereClause2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameter System.Data.Common.DbCommandBuilder::CreateParameter(System.Data.Common.DbCommand,System.Int32,System.Data.DataRow)
extern "C"  DbParameter_t3306161371 * DbCommandBuilder_CreateParameter_m3710941618 (DbCommandBuilder_t2534194750 * __this, DbCommand_t2323745021 * ____dbCommand0, int32_t ___paramIndex1, DataRow_t3654701923 * ___schemaRow2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbDataAdapter System.Data.Common.DbCommandBuilder::get_DataAdapter()
extern "C"  DbDataAdapter_t3684585719 * DbCommandBuilder_get_DataAdapter_m2105466792 (DbCommandBuilder_t2534194750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::set_DataAdapter(System.Data.Common.DbDataAdapter)
extern "C"  void DbCommandBuilder_set_DataAdapter_m1581422881 (DbCommandBuilder_t2534194750 * __this, DbDataAdapter_t3684585719 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbCommandBuilder::get_QuotePrefix()
extern "C"  String_t* DbCommandBuilder_get_QuotePrefix_m3608610090 (DbCommandBuilder_t2534194750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::set_QuotePrefix(System.String)
extern "C"  void DbCommandBuilder_set_QuotePrefix_m1955710287 (DbCommandBuilder_t2534194750 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbCommandBuilder::get_QuoteSuffix()
extern "C"  String_t* DbCommandBuilder_get_QuoteSuffix_m2938246697 (DbCommandBuilder_t2534194750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::set_QuoteSuffix(System.String)
extern "C"  void DbCommandBuilder_set_QuoteSuffix_m518818480 (DbCommandBuilder_t2534194750 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::get_SourceCommand()
extern "C"  DbCommand_t2323745021 * DbCommandBuilder_get_SourceCommand_m2754335885 (DbCommandBuilder_t2534194750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::Dispose(System.Boolean)
extern "C"  void DbCommandBuilder_Dispose_m2810862616 (DbCommandBuilder_t2534194750 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::GetDeleteCommand()
extern "C"  DbCommand_t2323745021 * DbCommandBuilder_GetDeleteCommand_m2394146182 (DbCommandBuilder_t2534194750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::GetDeleteCommand(System.Boolean)
extern "C"  DbCommand_t2323745021 * DbCommandBuilder_GetDeleteCommand_m1031231613 (DbCommandBuilder_t2534194750 * __this, bool ___option0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::GetInsertCommand(System.Boolean,System.Data.DataRow)
extern "C"  DbCommand_t2323745021 * DbCommandBuilder_GetInsertCommand_m2403661680 (DbCommandBuilder_t2534194750 * __this, bool ___option0, DataRow_t3654701923 * ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::GetUpdateCommand()
extern "C"  DbCommand_t2323745021 * DbCommandBuilder_GetUpdateCommand_m2817438760 (DbCommandBuilder_t2534194750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand System.Data.Common.DbCommandBuilder::GetUpdateCommand(System.Boolean)
extern "C"  DbCommand_t2323745021 * DbCommandBuilder_GetUpdateCommand_m2194867103 (DbCommandBuilder_t2534194750 * __this, bool ___option0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbCommandBuilder::QuoteIdentifier(System.String)
extern "C"  String_t* DbCommandBuilder_QuoteIdentifier_m2419957400 (DbCommandBuilder_t2534194750 * __this, String_t* ___unquotedIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DbCommandBuilder::UnquoteIdentifier(System.String)
extern "C"  String_t* DbCommandBuilder_UnquoteIdentifier_m2651706481 (DbCommandBuilder_t2534194750 * __this, String_t* ___quotedIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::RowUpdatingHandler(System.Data.Common.RowUpdatingEventArgs)
extern "C"  void DbCommandBuilder_RowUpdatingHandler_m576872979 (DbCommandBuilder_t2534194750 * __this, RowUpdatingEventArgs_t2786481031 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbCommandBuilder::.cctor()
extern "C"  void DbCommandBuilder__cctor_m1095941897 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
