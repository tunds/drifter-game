﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct Comparer_1_t1303533693;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.Comparer`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor()
extern "C"  void Comparer_1__ctor_m323626736_gshared (Comparer_1_t1303533693 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m323626736(__this, method) ((  void (*) (Comparer_1_t1303533693 *, const MethodInfo*))Comparer_1__ctor_m323626736_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.cctor()
extern "C"  void Comparer_1__cctor_m960398013_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m960398013(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m960398013_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m3866307349_gshared (Comparer_1_t1303533693 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m3866307349(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t1303533693 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m3866307349_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Default()
extern "C"  Comparer_1_t1303533693 * Comparer_1_get_Default_m2512168472_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m2512168472(__this /* static, unused */, method) ((  Comparer_1_t1303533693 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m2512168472_gshared)(__this /* static, unused */, method)
