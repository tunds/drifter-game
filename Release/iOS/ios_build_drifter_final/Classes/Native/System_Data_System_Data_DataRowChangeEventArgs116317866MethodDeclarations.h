﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.DataRowChangeEventArgs
struct DataRowChangeEventArgs_t116317866;
// System.Data.DataRow
struct DataRow_t3654701923;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_DataRow3654701923.h"
#include "System_Data_System_Data_DataRowAction22198713.h"

// System.Void System.Data.DataRowChangeEventArgs::.ctor(System.Data.DataRow,System.Data.DataRowAction)
extern "C"  void DataRowChangeEventArgs__ctor_m1215614104 (DataRowChangeEventArgs_t116317866 * __this, DataRow_t3654701923 * ___row0, int32_t ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
