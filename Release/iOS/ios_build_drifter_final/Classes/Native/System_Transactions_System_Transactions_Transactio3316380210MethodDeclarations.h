﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Transactions.TransactionScope
struct TransactionScope_t3316380210;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Transactions.TransactionScope::.cctor()
extern "C"  void TransactionScope__cctor_m1305770556 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Transactions.TransactionScope::get_IsComplete()
extern "C"  bool TransactionScope_get_IsComplete_m2704364895 (TransactionScope_t3316380210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Transactions.TransactionScope::Dispose()
extern "C"  void TransactionScope_Dispose_m3782545294 (TransactionScope_t3316380210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
