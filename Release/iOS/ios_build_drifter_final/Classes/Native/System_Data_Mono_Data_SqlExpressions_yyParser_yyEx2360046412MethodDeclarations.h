﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.SqlExpressions.yyParser.yyException
struct yyException_t2360046412;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Mono.Data.SqlExpressions.yyParser.yyException::.ctor(System.String)
extern "C"  void yyException__ctor_m3312713562 (yyException_t2360046412 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
