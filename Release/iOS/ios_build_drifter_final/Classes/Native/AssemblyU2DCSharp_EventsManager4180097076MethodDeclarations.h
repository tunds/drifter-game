﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EventsManager
struct EventsManager_t4180097076;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_DialogResult3964159632.h"

// System.Void EventsManager::.ctor()
extern "C"  void EventsManager__ctor_m4179436471 (EventsManager_t4180097076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventsManager::Awake()
extern "C"  void EventsManager_Awake_m122074394 (EventsManager_t4180097076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventsManager::OnApplicationQuit()
extern "C"  void EventsManager_OnApplicationQuit_m2352133813 (EventsManager_t4180097076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventsManager::setName()
extern "C"  void EventsManager_setName_m3010993858 (EventsManager_t4180097076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventsManager::saveTime()
extern "C"  void EventsManager_saveTime_m3834172407 (EventsManager_t4180097076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventsManager::closeMessageBox(System.Int32,PoqXert.MessageBox.DialogResult)
extern "C"  void EventsManager_closeMessageBox_m1554315340 (EventsManager_t4180097076 * __this, int32_t ___id0, int32_t ___btn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventsManager::replayGame()
extern "C"  void EventsManager_replayGame_m3649958854 (EventsManager_t4180097076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventsManager::homeGame()
extern "C"  void EventsManager_homeGame_m3281303998 (EventsManager_t4180097076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
