﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbDataReader
struct DbDataReader_t2472406139;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Data.Common.DbDataReader::.ctor()
extern "C"  void DbDataReader__ctor_m251430663 (DbDataReader_t2472406139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.DbDataReader::get_VisibleFieldCount()
extern "C"  int32_t DbDataReader_get_VisibleFieldCount_m1786253351 (DbDataReader_t2472406139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbDataReader::Dispose()
extern "C"  void DbDataReader_Dispose_m1004981636 (DbDataReader_t2472406139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DbDataReader::Dispose(System.Boolean)
extern "C"  void DbDataReader_Dispose_m2097515515 (DbDataReader_t2472406139 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
