﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>
struct ShimEnumerator_t1701836840;
// System.Collections.Generic.Dictionary`2<System.Int64,System.Object>
struct Dictionary_2_t271112152;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m524822326_gshared (ShimEnumerator_t1701836840 * __this, Dictionary_2_t271112152 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m524822326(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1701836840 *, Dictionary_2_t271112152 *, const MethodInfo*))ShimEnumerator__ctor_m524822326_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3433290991_gshared (ShimEnumerator_t1701836840 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3433290991(__this, method) ((  bool (*) (ShimEnumerator_t1701836840 *, const MethodInfo*))ShimEnumerator_MoveNext_m3433290991_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t130027246  ShimEnumerator_get_Entry_m1365055003_gshared (ShimEnumerator_t1701836840 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1365055003(__this, method) ((  DictionaryEntry_t130027246  (*) (ShimEnumerator_t1701836840 *, const MethodInfo*))ShimEnumerator_get_Entry_m1365055003_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m729543450_gshared (ShimEnumerator_t1701836840 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m729543450(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1701836840 *, const MethodInfo*))ShimEnumerator_get_Key_m729543450_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2060022572_gshared (ShimEnumerator_t1701836840 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2060022572(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1701836840 *, const MethodInfo*))ShimEnumerator_get_Value_m2060022572_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1126459060_gshared (ShimEnumerator_t1701836840 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1126459060(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1701836840 *, const MethodInfo*))ShimEnumerator_get_Current_m1126459060_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2429177992_gshared (ShimEnumerator_t1701836840 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2429177992(__this, method) ((  void (*) (ShimEnumerator_t1701836840 *, const MethodInfo*))ShimEnumerator_Reset_m2429177992_gshared)(__this, method)
