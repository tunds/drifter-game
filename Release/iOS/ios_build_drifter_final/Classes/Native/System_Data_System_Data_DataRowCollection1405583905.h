﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Data.DataTable
struct DataTable_t2176726999;
// System.ComponentModel.ListChangedEventHandler
struct ListChangedEventHandler_t3535012143;

#include "System_Data_System_Data_InternalDataCollectionBase2754805833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRowCollection
struct  DataRowCollection_t1405583905  : public InternalDataCollectionBase_t2754805833
{
public:
	// System.Data.DataTable System.Data.DataRowCollection::table
	DataTable_t2176726999 * ___table_2;
	// System.ComponentModel.ListChangedEventHandler System.Data.DataRowCollection::ListChanged
	ListChangedEventHandler_t3535012143 * ___ListChanged_3;

public:
	inline static int32_t get_offset_of_table_2() { return static_cast<int32_t>(offsetof(DataRowCollection_t1405583905, ___table_2)); }
	inline DataTable_t2176726999 * get_table_2() const { return ___table_2; }
	inline DataTable_t2176726999 ** get_address_of_table_2() { return &___table_2; }
	inline void set_table_2(DataTable_t2176726999 * value)
	{
		___table_2 = value;
		Il2CppCodeGenWriteBarrier(&___table_2, value);
	}

	inline static int32_t get_offset_of_ListChanged_3() { return static_cast<int32_t>(offsetof(DataRowCollection_t1405583905, ___ListChanged_3)); }
	inline ListChangedEventHandler_t3535012143 * get_ListChanged_3() const { return ___ListChanged_3; }
	inline ListChangedEventHandler_t3535012143 ** get_address_of_ListChanged_3() { return &___ListChanged_3; }
	inline void set_ListChanged_3(ListChangedEventHandler_t3535012143 * value)
	{
		___ListChanged_3 = value;
		Il2CppCodeGenWriteBarrier(&___ListChanged_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
