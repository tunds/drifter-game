﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct DefaultComparer_t3558413864;

#include "codegen/il2cpp-codegen.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataPair4220460133.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_DataPair>::.ctor()
extern "C"  void DefaultComparer__ctor_m2922353989_gshared (DefaultComparer_t3558413864 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2922353989(__this, method) ((  void (*) (DefaultComparer_t3558413864 *, const MethodInfo*))DefaultComparer__ctor_m2922353989_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_DataPair>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1260820166_gshared (DefaultComparer_t3558413864 * __this, DB_DataPair_t4220460133  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1260820166(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3558413864 *, DB_DataPair_t4220460133 , const MethodInfo*))DefaultComparer_GetHashCode_m1260820166_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_DataPair>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3831141270_gshared (DefaultComparer_t3558413864 * __this, DB_DataPair_t4220460133  ___x0, DB_DataPair_t4220460133  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3831141270(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3558413864 *, DB_DataPair_t4220460133 , DB_DataPair_t4220460133 , const MethodInfo*))DefaultComparer_Equals_m3831141270_gshared)(__this, ___x0, ___y1, method)
