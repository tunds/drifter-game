﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct Comparison_1_t2189038549;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Field3780330969.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Comparison`1<SQLiteDatabase.SQLiteDB/DB_Field>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m663736658_gshared (Comparison_1_t2189038549 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m663736658(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t2189038549 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m663736658_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<SQLiteDatabase.SQLiteDB/DB_Field>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m311222502_gshared (Comparison_1_t2189038549 * __this, DB_Field_t3780330969  ___x0, DB_Field_t3780330969  ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m311222502(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t2189038549 *, DB_Field_t3780330969 , DB_Field_t3780330969 , const MethodInfo*))Comparison_1_Invoke_m311222502_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<SQLiteDatabase.SQLiteDB/DB_Field>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m2441077807_gshared (Comparison_1_t2189038549 * __this, DB_Field_t3780330969  ___x0, DB_Field_t3780330969  ___y1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m2441077807(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t2189038549 *, DB_Field_t3780330969 , DB_Field_t3780330969 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m2441077807_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<SQLiteDatabase.SQLiteDB/DB_Field>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m286583238_gshared (Comparison_1_t2189038549 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m286583238(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t2189038549 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m286583238_gshared)(__this, ___result0, method)
