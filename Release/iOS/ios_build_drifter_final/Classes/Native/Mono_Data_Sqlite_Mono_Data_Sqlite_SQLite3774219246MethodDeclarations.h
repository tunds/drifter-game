﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLite3
struct SQLite3_t774219246;
// System.String
struct String_t;
// Mono.Data.Sqlite.SqliteStatement
struct SqliteStatement_t3906494218;
// Mono.Data.Sqlite.SqliteConnection
struct SqliteConnection_t3853176977;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// Mono.Data.Sqlite.SQLiteCallback
struct SQLiteCallback_t2947340536;
// Mono.Data.Sqlite.SQLiteFinalCallback
struct SQLiteFinalCallback_t347793270;
// Mono.Data.Sqlite.SQLiteCollation
struct SQLiteCollation_t3373936932;
// Mono.Data.Sqlite.SQLiteUpdateCallback
struct SQLiteUpdateCallback_t2915972065;
// Mono.Data.Sqlite.SQLiteCommitCallback
struct SQLiteCommitCallback_t1317328463;
// Mono.Data.Sqlite.SQLiteRollbackCallback
struct SQLiteRollbackCallback_t4255779836;
// System.Object
struct Il2CppObject;
// Mono.Data.Sqlite.SQLiteType
struct SQLiteType_t3947843053;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteDateFormat4009100393.h"
#include "mscorlib_System_String968488902.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteOpenFlagsE1905737881.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteStatement3906494218.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteConnection3853176977.h"
#include "mscorlib_System_DateTime339033936.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_TypeAffinity3864856329.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteCallback2947340536.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteFinalCallba347793270.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteCollation3373936932.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteUpdateCall2915972065.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteCommitCall1317328463.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteRollbackCa4255779836.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteType3947843053.h"

// System.Void Mono.Data.Sqlite.SQLite3::.ctor(Mono.Data.Sqlite.SQLiteDateFormats)
extern "C"  void SQLite3__ctor_m2549972635 (SQLite3_t774219246 * __this, int32_t ___fmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::Dispose(System.Boolean)
extern "C"  void SQLite3_Dispose_m377519080 (SQLite3_t774219246 * __this, bool ___bDisposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::Close()
extern "C"  void SQLite3_Close_m1226872330 (SQLite3_t774219246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3::get_Version()
extern "C"  String_t* SQLite3_get_Version_m1617939774 (SQLite3_t774219246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3::get_SQLiteVersion()
extern "C"  String_t* SQLite3_get_SQLiteVersion_m1814918866 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SQLite3::get_Changes()
extern "C"  int32_t SQLite3_get_Changes_m2959797776 (SQLite3_t774219246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::Open(System.String,Mono.Data.Sqlite.SQLiteOpenFlagsEnum,System.Int32,System.Boolean)
extern "C"  void SQLite3_Open_m2989281179 (SQLite3_t774219246 * __this, String_t* ___strFilename0, int32_t ___flags1, int32_t ___maxPoolSize2, bool ___usePool3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::SetTimeout(System.Int32)
extern "C"  void SQLite3_SetTimeout_m2457430080 (SQLite3_t774219246 * __this, int32_t ___nTimeoutMS0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SQLite3::Step(Mono.Data.Sqlite.SqliteStatement)
extern "C"  bool SQLite3_Step_m916511940 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SQLite3::Reset(Mono.Data.Sqlite.SqliteStatement)
extern "C"  int32_t SQLite3_Reset_m4007877419 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3::SQLiteLastError()
extern "C"  String_t* SQLite3_SQLiteLastError_m1087507957 (SQLite3_t774219246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteStatement Mono.Data.Sqlite.SQLite3::Prepare(Mono.Data.Sqlite.SqliteConnection,System.String,Mono.Data.Sqlite.SqliteStatement,System.UInt32,System.String&)
extern "C"  SqliteStatement_t3906494218 * SQLite3_Prepare_m3135053027 (SQLite3_t774219246 * __this, SqliteConnection_t3853176977 * ___cnn0, String_t* ___strSql1, SqliteStatement_t3906494218 * ___previous2, uint32_t ___timeoutMS3, String_t** ___strRemain4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::Bind_Double(Mono.Data.Sqlite.SqliteStatement,System.Int32,System.Double)
extern "C"  void SQLite3_Bind_Double_m848997192 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, double ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::Bind_Int32(Mono.Data.Sqlite.SqliteStatement,System.Int32,System.Int32)
extern "C"  void SQLite3_Bind_Int32_m2691994242 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, int32_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::Bind_Int64(Mono.Data.Sqlite.SqliteStatement,System.Int32,System.Int64)
extern "C"  void SQLite3_Bind_Int64_m3524996578 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, int64_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::Bind_Text(Mono.Data.Sqlite.SqliteStatement,System.Int32,System.String)
extern "C"  void SQLite3_Bind_Text_m2608323180 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::Bind_DateTime(Mono.Data.Sqlite.SqliteStatement,System.Int32,System.DateTime)
extern "C"  void SQLite3_Bind_DateTime_m4201218164 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, DateTime_t339033936  ___dt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::Bind_Blob(Mono.Data.Sqlite.SqliteStatement,System.Int32,System.Byte[])
extern "C"  void SQLite3_Bind_Blob_m563177155 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, ByteU5BU5D_t58506160* ___blobData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::Bind_Null(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  void SQLite3_Bind_Null_m2854006422 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SQLite3::Bind_ParamCount(Mono.Data.Sqlite.SqliteStatement)
extern "C"  int32_t SQLite3_Bind_ParamCount_m3526498368 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3::Bind_ParamName(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  String_t* SQLite3_Bind_ParamName_m2490581064 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SQLite3::ColumnCount(Mono.Data.Sqlite.SqliteStatement)
extern "C"  int32_t SQLite3_ColumnCount_m2980938485 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3::ColumnName(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  String_t* SQLite3_ColumnName_m569071613 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.TypeAffinity Mono.Data.Sqlite.SQLite3::ColumnAffinity(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  int32_t SQLite3_ColumnAffinity_m3582649243 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3::ColumnType(Mono.Data.Sqlite.SqliteStatement,System.Int32,Mono.Data.Sqlite.TypeAffinity&)
extern "C"  String_t* SQLite3_ColumnType_m488632829 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, int32_t* ___nAffinity2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3::ColumnOriginalName(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  String_t* SQLite3_ColumnOriginalName_m1226319468 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3::ColumnDatabaseName(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  String_t* SQLite3_ColumnDatabaseName_m69255810 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3::ColumnTableName(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  String_t* SQLite3_ColumnTableName_m1574872735 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::ColumnMetaData(System.String,System.String,System.String,System.String&,System.String&,System.Boolean&,System.Boolean&,System.Boolean&)
extern "C"  void SQLite3_ColumnMetaData_m1683702674 (SQLite3_t774219246 * __this, String_t* ___dataBase0, String_t* ___table1, String_t* ___column2, String_t** ___dataType3, String_t** ___collateSequence4, bool* ___notNull5, bool* ___primaryKey6, bool* ___autoIncrement7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Mono.Data.Sqlite.SQLite3::GetDouble(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  double SQLite3_GetDouble_m2155694555 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SQLite3::GetInt32(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  int32_t SQLite3_GetInt32_m4035158445 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.Data.Sqlite.SQLite3::GetInt64(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  int64_t SQLite3_GetInt64_m2309718541 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3::GetText(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  String_t* SQLite3_GetText_m1958424511 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Data.Sqlite.SQLite3::GetDateTime(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  DateTime_t339033936  SQLite3_GetDateTime_m112368391 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.Data.Sqlite.SQLite3::GetBytes(Mono.Data.Sqlite.SqliteStatement,System.Int32,System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  int64_t SQLite3_GetBytes_m3132932363 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, int32_t ___nDataOffset2, ByteU5BU5D_t58506160* ___bDest3, int32_t ___nStart4, int32_t ___nLength5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SQLite3::IsNull(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  bool SQLite3_IsNull_m1312462126 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::CreateFunction(System.String,System.Int32,System.Boolean,Mono.Data.Sqlite.SQLiteCallback,Mono.Data.Sqlite.SQLiteCallback,Mono.Data.Sqlite.SQLiteFinalCallback)
extern "C"  void SQLite3_CreateFunction_m2875930648 (SQLite3_t774219246 * __this, String_t* ___strFunction0, int32_t ___nArgs1, bool ___needCollSeq2, SQLiteCallback_t2947340536 * ___func3, SQLiteCallback_t2947340536 * ___funcstep4, SQLiteFinalCallback_t347793270 * ___funcfinal5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::CreateCollation(System.String,Mono.Data.Sqlite.SQLiteCollation,Mono.Data.Sqlite.SQLiteCollation)
extern "C"  void SQLite3_CreateCollation_m2546531171 (SQLite3_t774219246 * __this, String_t* ___strCollation0, SQLiteCollation_t3373936932 * ___func1, SQLiteCollation_t3373936932 * ___func162, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.Data.Sqlite.SQLite3::GetParamValueBytes(System.IntPtr,System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  int64_t SQLite3_GetParamValueBytes_m1001488132 (SQLite3_t774219246 * __this, IntPtr_t ___p0, int32_t ___nDataOffset1, ByteU5BU5D_t58506160* ___bDest2, int32_t ___nStart3, int32_t ___nLength4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Mono.Data.Sqlite.SQLite3::GetParamValueDouble(System.IntPtr)
extern "C"  double SQLite3_GetParamValueDouble_m365744698 (SQLite3_t774219246 * __this, IntPtr_t ___ptr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.Data.Sqlite.SQLite3::GetParamValueInt64(System.IntPtr)
extern "C"  int64_t SQLite3_GetParamValueInt64_m4041453172 (SQLite3_t774219246 * __this, IntPtr_t ___ptr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3::GetParamValueText(System.IntPtr)
extern "C"  String_t* SQLite3_GetParamValueText_m572786462 (SQLite3_t774219246 * __this, IntPtr_t ___ptr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.TypeAffinity Mono.Data.Sqlite.SQLite3::GetParamValueType(System.IntPtr)
extern "C"  int32_t SQLite3_GetParamValueType_m713647030 (SQLite3_t774219246 * __this, IntPtr_t ___ptr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::ReturnBlob(System.IntPtr,System.Byte[])
extern "C"  void SQLite3_ReturnBlob_m3340522490 (SQLite3_t774219246 * __this, IntPtr_t ___context0, ByteU5BU5D_t58506160* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::ReturnDouble(System.IntPtr,System.Double)
extern "C"  void SQLite3_ReturnDouble_m175895423 (SQLite3_t774219246 * __this, IntPtr_t ___context0, double ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::ReturnError(System.IntPtr,System.String)
extern "C"  void SQLite3_ReturnError_m2220497222 (SQLite3_t774219246 * __this, IntPtr_t ___context0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::ReturnInt64(System.IntPtr,System.Int64)
extern "C"  void SQLite3_ReturnInt64_m920576371 (SQLite3_t774219246 * __this, IntPtr_t ___context0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::ReturnNull(System.IntPtr)
extern "C"  void SQLite3_ReturnNull_m660983821 (SQLite3_t774219246 * __this, IntPtr_t ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::ReturnText(System.IntPtr,System.String)
extern "C"  void SQLite3_ReturnText_m703629219 (SQLite3_t774219246 * __this, IntPtr_t ___context0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.SQLite3::AggregateContext(System.IntPtr)
extern "C"  IntPtr_t SQLite3_AggregateContext_m1321662079 (SQLite3_t774219246 * __this, IntPtr_t ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::SetPassword(System.Byte[])
extern "C"  void SQLite3_SetPassword_m107862266 (SQLite3_t774219246 * __this, ByteU5BU5D_t58506160* ___passwordBytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::SetUpdateHook(Mono.Data.Sqlite.SQLiteUpdateCallback)
extern "C"  void SQLite3_SetUpdateHook_m370193551 (SQLite3_t774219246 * __this, SQLiteUpdateCallback_t2915972065 * ___func0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::SetCommitHook(Mono.Data.Sqlite.SQLiteCommitCallback)
extern "C"  void SQLite3_SetCommitHook_m4102993587 (SQLite3_t774219246 * __this, SQLiteCommitCallback_t1317328463 * ___func0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::SetRollbackHook(Mono.Data.Sqlite.SQLiteRollbackCallback)
extern "C"  void SQLite3_SetRollbackHook_m761303641 (SQLite3_t774219246 * __this, SQLiteRollbackCallback_t4255779836 * ___func0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SQLite3::GetValue(Mono.Data.Sqlite.SqliteStatement,System.Int32,Mono.Data.Sqlite.SQLiteType)
extern "C"  Il2CppObject * SQLite3_GetValue_m1848920206 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, SQLiteType_t3947843053 * ___typ2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SQLite3::GetCursorForTable(Mono.Data.Sqlite.SqliteStatement,System.Int32,System.Int32)
extern "C"  int32_t SQLite3_GetCursorForTable_m3362877421 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___db1, int32_t ___rootPage2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.Data.Sqlite.SQLite3::GetRowIdForCursor(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  int64_t SQLite3_GetRowIdForCursor_m1965901596 (SQLite3_t774219246 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___cursor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3::GetIndexColumnExtendedInfo(System.String,System.String,System.String,System.Int32&,System.Int32&,System.String&)
extern "C"  void SQLite3_GetIndexColumnExtendedInfo_m1597046581 (SQLite3_t774219246 * __this, String_t* ___database0, String_t* ___index1, String_t* ___column2, int32_t* ___sortMode3, int32_t* ___onError4, String_t** ___collationSequence5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
