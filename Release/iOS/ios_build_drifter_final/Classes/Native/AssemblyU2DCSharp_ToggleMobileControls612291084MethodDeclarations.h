﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ToggleMobileControls
struct ToggleMobileControls_t612291084;

#include "codegen/il2cpp-codegen.h"

// System.Void ToggleMobileControls::.ctor()
extern "C"  void ToggleMobileControls__ctor_m3534165519 (ToggleMobileControls_t612291084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToggleMobileControls::Start()
extern "C"  void ToggleMobileControls_Start_m2481303311 (ToggleMobileControls_t612291084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToggleMobileControls::Update()
extern "C"  void ToggleMobileControls_Update_m3911810782 (ToggleMobileControls_t612291084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
