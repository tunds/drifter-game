﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct List_1_t722451806;
// System.Collections.Generic.IEnumerable`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct IEnumerable_1_t2797647193;
// System.Collections.Generic.IEnumerator`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct IEnumerator_1_t1408599285;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct ICollection_1_t391324223;
// System.Collections.ObjectModel.ReadOnlyCollection`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct ReadOnlyCollection_1_t3088638185;
// SQLiteDatabase.SQLiteDB/DB_DataPair[]
struct DB_DataPairU5BU5D_t2315727976;
// System.Predicate`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct Predicate_1_t496456735;
// System.Collections.Generic.IComparer`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct IComparer_1_t2625200246;
// System.Comparison`1<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct Comparison_1_t2629167713;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataPair4220460133.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3103202094.h"

// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::.ctor()
extern "C"  void List_1__ctor_m2069798182_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1__ctor_m2069798182(__this, method) ((  void (*) (List_1_t722451806 *, const MethodInfo*))List_1__ctor_m2069798182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3185957346_gshared (List_1_t722451806 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3185957346(__this, ___collection0, method) ((  void (*) (List_1_t722451806 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3185957346_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m150906958_gshared (List_1_t722451806 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m150906958(__this, ___capacity0, method) ((  void (*) (List_1_t722451806 *, int32_t, const MethodInfo*))List_1__ctor_m150906958_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::.cctor()
extern "C"  void List_1__cctor_m3444364496_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3444364496(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3444364496_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2123498383_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2123498383(__this, method) ((  Il2CppObject* (*) (List_1_t722451806 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2123498383_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1575248231_gshared (List_1_t722451806 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1575248231(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t722451806 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1575248231_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m986897826_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m986897826(__this, method) ((  Il2CppObject * (*) (List_1_t722451806 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m986897826_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m100038415_gshared (List_1_t722451806 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m100038415(__this, ___item0, method) ((  int32_t (*) (List_1_t722451806 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m100038415_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2294837661_gshared (List_1_t722451806 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2294837661(__this, ___item0, method) ((  bool (*) (List_1_t722451806 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2294837661_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1475775975_gshared (List_1_t722451806 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1475775975(__this, ___item0, method) ((  int32_t (*) (List_1_t722451806 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1475775975_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2404851346_gshared (List_1_t722451806 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2404851346(__this, ___index0, ___item1, method) ((  void (*) (List_1_t722451806 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2404851346_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2956215638_gshared (List_1_t722451806 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2956215638(__this, ___item0, method) ((  void (*) (List_1_t722451806 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2956215638_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m295485790_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m295485790(__this, method) ((  bool (*) (List_1_t722451806 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m295485790_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2509284511_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2509284511(__this, method) ((  bool (*) (List_1_t722451806 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2509284511_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2335649291_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2335649291(__this, method) ((  Il2CppObject * (*) (List_1_t722451806 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2335649291_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m469293772_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m469293772(__this, method) ((  bool (*) (List_1_t722451806 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m469293772_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3655735981_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3655735981(__this, method) ((  bool (*) (List_1_t722451806 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3655735981_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3651651794_gshared (List_1_t722451806 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3651651794(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t722451806 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3651651794_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m983690729_gshared (List_1_t722451806 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m983690729(__this, ___index0, ___value1, method) ((  void (*) (List_1_t722451806 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m983690729_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Add(T)
extern "C"  void List_1_Add_m1594226274_gshared (List_1_t722451806 * __this, DB_DataPair_t4220460133  ___item0, const MethodInfo* method);
#define List_1_Add_m1594226274(__this, ___item0, method) ((  void (*) (List_1_t722451806 *, DB_DataPair_t4220460133 , const MethodInfo*))List_1_Add_m1594226274_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m615522205_gshared (List_1_t722451806 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m615522205(__this, ___newCount0, method) ((  void (*) (List_1_t722451806 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m615522205_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3736257179_gshared (List_1_t722451806 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3736257179(__this, ___collection0, method) ((  void (*) (List_1_t722451806 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3736257179_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2997229403_gshared (List_1_t722451806 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2997229403(__this, ___enumerable0, method) ((  void (*) (List_1_t722451806 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2997229403_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2423941724_gshared (List_1_t722451806 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2423941724(__this, ___collection0, method) ((  void (*) (List_1_t722451806 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2423941724_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3088638185 * List_1_AsReadOnly_m610634445_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m610634445(__this, method) ((  ReadOnlyCollection_1_t3088638185 * (*) (List_1_t722451806 *, const MethodInfo*))List_1_AsReadOnly_m610634445_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Clear()
extern "C"  void List_1_Clear_m4183065256_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_Clear_m4183065256(__this, method) ((  void (*) (List_1_t722451806 *, const MethodInfo*))List_1_Clear_m4183065256_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Contains(T)
extern "C"  bool List_1_Contains_m4169124182_gshared (List_1_t722451806 * __this, DB_DataPair_t4220460133  ___item0, const MethodInfo* method);
#define List_1_Contains_m4169124182(__this, ___item0, method) ((  bool (*) (List_1_t722451806 *, DB_DataPair_t4220460133 , const MethodInfo*))List_1_Contains_m4169124182_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m2679297829_gshared (List_1_t722451806 * __this, DB_DataPairU5BU5D_t2315727976* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m2679297829(__this, ___array0, method) ((  void (*) (List_1_t722451806 *, DB_DataPairU5BU5D_t2315727976*, const MethodInfo*))List_1_CopyTo_m2679297829_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3390294482_gshared (List_1_t722451806 * __this, DB_DataPairU5BU5D_t2315727976* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3390294482(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t722451806 *, DB_DataPairU5BU5D_t2315727976*, int32_t, const MethodInfo*))List_1_CopyTo_m3390294482_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Find(System.Predicate`1<T>)
extern "C"  DB_DataPair_t4220460133  List_1_Find_m4228212246_gshared (List_1_t722451806 * __this, Predicate_1_t496456735 * ___match0, const MethodInfo* method);
#define List_1_Find_m4228212246(__this, ___match0, method) ((  DB_DataPair_t4220460133  (*) (List_1_t722451806 *, Predicate_1_t496456735 *, const MethodInfo*))List_1_Find_m4228212246_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1470054737_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t496456735 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1470054737(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t496456735 *, const MethodInfo*))List_1_CheckMatch_m1470054737_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2342193782_gshared (List_1_t722451806 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t496456735 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2342193782(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t722451806 *, int32_t, int32_t, Predicate_1_t496456735 *, const MethodInfo*))List_1_GetIndex_m2342193782_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::GetEnumerator()
extern "C"  Enumerator_t3103202094  List_1_GetEnumerator_m1866343315_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1866343315(__this, method) ((  Enumerator_t3103202094  (*) (List_1_t722451806 *, const MethodInfo*))List_1_GetEnumerator_m1866343315_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2130858454_gshared (List_1_t722451806 * __this, DB_DataPair_t4220460133  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2130858454(__this, ___item0, method) ((  int32_t (*) (List_1_t722451806 *, DB_DataPair_t4220460133 , const MethodInfo*))List_1_IndexOf_m2130858454_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3468013097_gshared (List_1_t722451806 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3468013097(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t722451806 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3468013097_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3136661666_gshared (List_1_t722451806 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3136661666(__this, ___index0, method) ((  void (*) (List_1_t722451806 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3136661666_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m257830089_gshared (List_1_t722451806 * __this, int32_t ___index0, DB_DataPair_t4220460133  ___item1, const MethodInfo* method);
#define List_1_Insert_m257830089(__this, ___index0, ___item1, method) ((  void (*) (List_1_t722451806 *, int32_t, DB_DataPair_t4220460133 , const MethodInfo*))List_1_Insert_m257830089_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m477158014_gshared (List_1_t722451806 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m477158014(__this, ___collection0, method) ((  void (*) (List_1_t722451806 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m477158014_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Remove(T)
extern "C"  bool List_1_Remove_m1956832977_gshared (List_1_t722451806 * __this, DB_DataPair_t4220460133  ___item0, const MethodInfo* method);
#define List_1_Remove_m1956832977(__this, ___item0, method) ((  bool (*) (List_1_t722451806 *, DB_DataPair_t4220460133 , const MethodInfo*))List_1_Remove_m1956832977_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1703965977_gshared (List_1_t722451806 * __this, Predicate_1_t496456735 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1703965977(__this, ___match0, method) ((  int32_t (*) (List_1_t722451806 *, Predicate_1_t496456735 *, const MethodInfo*))List_1_RemoveAll_m1703965977_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2426650255_gshared (List_1_t722451806 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2426650255(__this, ___index0, method) ((  void (*) (List_1_t722451806 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2426650255_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Reverse()
extern "C"  void List_1_Reverse_m1499008893_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_Reverse_m1499008893(__this, method) ((  void (*) (List_1_t722451806 *, const MethodInfo*))List_1_Reverse_m1499008893_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Sort()
extern "C"  void List_1_Sort_m2120198469_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_Sort_m2120198469(__this, method) ((  void (*) (List_1_t722451806 *, const MethodInfo*))List_1_Sort_m2120198469_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1451167295_gshared (List_1_t722451806 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1451167295(__this, ___comparer0, method) ((  void (*) (List_1_t722451806 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1451167295_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1143948248_gshared (List_1_t722451806 * __this, Comparison_1_t2629167713 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1143948248(__this, ___comparison0, method) ((  void (*) (List_1_t722451806 *, Comparison_1_t2629167713 *, const MethodInfo*))List_1_Sort_m1143948248_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::ToArray()
extern "C"  DB_DataPairU5BU5D_t2315727976* List_1_ToArray_m3792489532_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_ToArray_m3792489532(__this, method) ((  DB_DataPairU5BU5D_t2315727976* (*) (List_1_t722451806 *, const MethodInfo*))List_1_ToArray_m3792489532_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::TrimExcess()
extern "C"  void List_1_TrimExcess_m578421022_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m578421022(__this, method) ((  void (*) (List_1_t722451806 *, const MethodInfo*))List_1_TrimExcess_m578421022_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2405003014_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2405003014(__this, method) ((  int32_t (*) (List_1_t722451806 *, const MethodInfo*))List_1_get_Capacity_m2405003014_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2120678319_gshared (List_1_t722451806 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2120678319(__this, ___value0, method) ((  void (*) (List_1_t722451806 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2120678319_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Count()
extern "C"  int32_t List_1_get_Count_m1449410149_gshared (List_1_t722451806 * __this, const MethodInfo* method);
#define List_1_get_Count_m1449410149(__this, method) ((  int32_t (*) (List_1_t722451806 *, const MethodInfo*))List_1_get_Count_m1449410149_gshared)(__this, method)
// T System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::get_Item(System.Int32)
extern "C"  DB_DataPair_t4220460133  List_1_get_Item_m723984083_gshared (List_1_t722451806 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m723984083(__this, ___index0, method) ((  DB_DataPair_t4220460133  (*) (List_1_t722451806 *, int32_t, const MethodInfo*))List_1_get_Item_m723984083_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m476208992_gshared (List_1_t722451806 * __this, int32_t ___index0, DB_DataPair_t4220460133  ___value1, const MethodInfo* method);
#define List_1_set_Item_m476208992(__this, ___index0, ___value1, method) ((  void (*) (List_1_t722451806 *, int32_t, DB_DataPair_t4220460133 , const MethodInfo*))List_1_set_Item_m476208992_gshared)(__this, ___index0, ___value1, method)
