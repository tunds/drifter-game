﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Data_Mono_Data_SqlExpressions_BinaryOpExpre3970441148.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.ArithmeticOperation
struct  ArithmeticOperation_t78727519  : public BinaryOpExpression_t3970441148
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
