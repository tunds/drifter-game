﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FollowCharacter
struct FollowCharacter_t1365044824;

#include "codegen/il2cpp-codegen.h"

// System.Void FollowCharacter::.ctor()
extern "C"  void FollowCharacter__ctor_m2293595155 (FollowCharacter_t1365044824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FollowCharacter::Start()
extern "C"  void FollowCharacter_Start_m1240732947 (FollowCharacter_t1365044824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FollowCharacter::FixedUpdate()
extern "C"  void FollowCharacter_FixedUpdate_m3033055054 (FollowCharacter_t1365044824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
