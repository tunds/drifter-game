﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.DataColumnPropertyDescriptor
struct DataColumnPropertyDescriptor_t1327003831;
// System.String
struct String_t;
// System.Attribute[]
struct AttributeU5BU5D_t4076389004;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void System.Data.DataColumnPropertyDescriptor::.ctor(System.String,System.Int32,System.Attribute[])
extern "C"  void DataColumnPropertyDescriptor__ctor_m2045060901 (DataColumnPropertyDescriptor_t1327003831 * __this, String_t* ___name0, int32_t ___columnIndex1, AttributeU5BU5D_t4076389004* ___attrs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.DataColumnPropertyDescriptor::SetComponentType(System.Type)
extern "C"  void DataColumnPropertyDescriptor_SetComponentType_m2222443053 (DataColumnPropertyDescriptor_t1327003831 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.DataColumnPropertyDescriptor::SetPropertyType(System.Type)
extern "C"  void DataColumnPropertyDescriptor_SetPropertyType_m2553113917 (DataColumnPropertyDescriptor_t1327003831 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Data.DataColumnPropertyDescriptor::get_ComponentType()
extern "C"  Type_t * DataColumnPropertyDescriptor_get_ComponentType_m3322179231 (DataColumnPropertyDescriptor_t1327003831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Data.DataColumnPropertyDescriptor::get_PropertyType()
extern "C"  Type_t * DataColumnPropertyDescriptor_get_PropertyType_m503057801 (DataColumnPropertyDescriptor_t1327003831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
