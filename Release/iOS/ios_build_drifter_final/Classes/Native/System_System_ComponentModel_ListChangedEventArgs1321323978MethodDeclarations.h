﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.ListChangedEventArgs
struct ListChangedEventArgs_t1321323978;

#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_ListChangedType4034521881.h"

// System.Void System.ComponentModel.ListChangedEventArgs::.ctor(System.ComponentModel.ListChangedType,System.Int32,System.Int32)
extern "C"  void ListChangedEventArgs__ctor_m2269671341 (ListChangedEventArgs_t1321323978 * __this, int32_t ___listChangedType0, int32_t ___newIndex1, int32_t ___oldIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
