﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteStatementHandle
struct SqliteStatementHandle_t1861022482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteStatementH1861022482.h"

// System.Void Mono.Data.Sqlite.SqliteStatementHandle::.ctor(System.IntPtr)
extern "C"  void SqliteStatementHandle__ctor_m106939396 (SqliteStatementHandle_t1861022482 * __this, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteStatementHandle::.ctor()
extern "C"  void SqliteStatementHandle__ctor_m4074611280 (SqliteStatementHandle_t1861022482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteStatementHandle::ReleaseHandle()
extern "C"  bool SqliteStatementHandle_ReleaseHandle_m402485883 (SqliteStatementHandle_t1861022482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteStatementHandle::get_IsInvalid()
extern "C"  bool SqliteStatementHandle_get_IsInvalid_m1015969776 (SqliteStatementHandle_t1861022482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.Sqlite.SqliteStatementHandle::op_Implicit(Mono.Data.Sqlite.SqliteStatementHandle)
extern "C"  IntPtr_t SqliteStatementHandle_op_Implicit_m2604755386 (Il2CppObject * __this /* static, unused */, SqliteStatementHandle_t1861022482 * ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SqliteStatementHandle Mono.Data.Sqlite.SqliteStatementHandle::op_Implicit(System.IntPtr)
extern "C"  SqliteStatementHandle_t1861022482 * SqliteStatementHandle_op_Implicit_m1146378668 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
