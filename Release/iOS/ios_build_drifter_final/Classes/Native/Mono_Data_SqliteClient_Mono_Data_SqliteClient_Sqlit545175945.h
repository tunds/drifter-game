﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mono.Data.SqliteClient.SqliteCommand
struct SqliteCommand_t3207195823;
// System.Collections.ArrayList
struct ArrayList_t2121638921;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Collections.Hashtable
struct Hashtable_t3875263730;

#include "System_Data_System_Data_Common_DbDataReader2472406139.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqliteClient.SqliteDataReader
struct  SqliteDataReader_t545175945  : public DbDataReader_t2472406139
{
public:
	// Mono.Data.SqliteClient.SqliteCommand Mono.Data.SqliteClient.SqliteDataReader::command
	SqliteCommand_t3207195823 * ___command_1;
	// System.Collections.ArrayList Mono.Data.SqliteClient.SqliteDataReader::rows
	ArrayList_t2121638921 * ___rows_2;
	// System.String[] Mono.Data.SqliteClient.SqliteDataReader::columns
	StringU5BU5D_t2956870243* ___columns_3;
	// System.Collections.Hashtable Mono.Data.SqliteClient.SqliteDataReader::column_names_sens
	Hashtable_t3875263730 * ___column_names_sens_4;
	// System.Collections.Hashtable Mono.Data.SqliteClient.SqliteDataReader::column_names_insens
	Hashtable_t3875263730 * ___column_names_insens_5;
	// System.Int32 Mono.Data.SqliteClient.SqliteDataReader::current_row
	int32_t ___current_row_6;
	// System.Boolean Mono.Data.SqliteClient.SqliteDataReader::closed
	bool ___closed_7;
	// System.Boolean Mono.Data.SqliteClient.SqliteDataReader::reading
	bool ___reading_8;
	// System.Int32 Mono.Data.SqliteClient.SqliteDataReader::records_affected
	int32_t ___records_affected_9;
	// System.String[] Mono.Data.SqliteClient.SqliteDataReader::decltypes
	StringU5BU5D_t2956870243* ___decltypes_10;

public:
	inline static int32_t get_offset_of_command_1() { return static_cast<int32_t>(offsetof(SqliteDataReader_t545175945, ___command_1)); }
	inline SqliteCommand_t3207195823 * get_command_1() const { return ___command_1; }
	inline SqliteCommand_t3207195823 ** get_address_of_command_1() { return &___command_1; }
	inline void set_command_1(SqliteCommand_t3207195823 * value)
	{
		___command_1 = value;
		Il2CppCodeGenWriteBarrier(&___command_1, value);
	}

	inline static int32_t get_offset_of_rows_2() { return static_cast<int32_t>(offsetof(SqliteDataReader_t545175945, ___rows_2)); }
	inline ArrayList_t2121638921 * get_rows_2() const { return ___rows_2; }
	inline ArrayList_t2121638921 ** get_address_of_rows_2() { return &___rows_2; }
	inline void set_rows_2(ArrayList_t2121638921 * value)
	{
		___rows_2 = value;
		Il2CppCodeGenWriteBarrier(&___rows_2, value);
	}

	inline static int32_t get_offset_of_columns_3() { return static_cast<int32_t>(offsetof(SqliteDataReader_t545175945, ___columns_3)); }
	inline StringU5BU5D_t2956870243* get_columns_3() const { return ___columns_3; }
	inline StringU5BU5D_t2956870243** get_address_of_columns_3() { return &___columns_3; }
	inline void set_columns_3(StringU5BU5D_t2956870243* value)
	{
		___columns_3 = value;
		Il2CppCodeGenWriteBarrier(&___columns_3, value);
	}

	inline static int32_t get_offset_of_column_names_sens_4() { return static_cast<int32_t>(offsetof(SqliteDataReader_t545175945, ___column_names_sens_4)); }
	inline Hashtable_t3875263730 * get_column_names_sens_4() const { return ___column_names_sens_4; }
	inline Hashtable_t3875263730 ** get_address_of_column_names_sens_4() { return &___column_names_sens_4; }
	inline void set_column_names_sens_4(Hashtable_t3875263730 * value)
	{
		___column_names_sens_4 = value;
		Il2CppCodeGenWriteBarrier(&___column_names_sens_4, value);
	}

	inline static int32_t get_offset_of_column_names_insens_5() { return static_cast<int32_t>(offsetof(SqliteDataReader_t545175945, ___column_names_insens_5)); }
	inline Hashtable_t3875263730 * get_column_names_insens_5() const { return ___column_names_insens_5; }
	inline Hashtable_t3875263730 ** get_address_of_column_names_insens_5() { return &___column_names_insens_5; }
	inline void set_column_names_insens_5(Hashtable_t3875263730 * value)
	{
		___column_names_insens_5 = value;
		Il2CppCodeGenWriteBarrier(&___column_names_insens_5, value);
	}

	inline static int32_t get_offset_of_current_row_6() { return static_cast<int32_t>(offsetof(SqliteDataReader_t545175945, ___current_row_6)); }
	inline int32_t get_current_row_6() const { return ___current_row_6; }
	inline int32_t* get_address_of_current_row_6() { return &___current_row_6; }
	inline void set_current_row_6(int32_t value)
	{
		___current_row_6 = value;
	}

	inline static int32_t get_offset_of_closed_7() { return static_cast<int32_t>(offsetof(SqliteDataReader_t545175945, ___closed_7)); }
	inline bool get_closed_7() const { return ___closed_7; }
	inline bool* get_address_of_closed_7() { return &___closed_7; }
	inline void set_closed_7(bool value)
	{
		___closed_7 = value;
	}

	inline static int32_t get_offset_of_reading_8() { return static_cast<int32_t>(offsetof(SqliteDataReader_t545175945, ___reading_8)); }
	inline bool get_reading_8() const { return ___reading_8; }
	inline bool* get_address_of_reading_8() { return &___reading_8; }
	inline void set_reading_8(bool value)
	{
		___reading_8 = value;
	}

	inline static int32_t get_offset_of_records_affected_9() { return static_cast<int32_t>(offsetof(SqliteDataReader_t545175945, ___records_affected_9)); }
	inline int32_t get_records_affected_9() const { return ___records_affected_9; }
	inline int32_t* get_address_of_records_affected_9() { return &___records_affected_9; }
	inline void set_records_affected_9(int32_t value)
	{
		___records_affected_9 = value;
	}

	inline static int32_t get_offset_of_decltypes_10() { return static_cast<int32_t>(offsetof(SqliteDataReader_t545175945, ___decltypes_10)); }
	inline StringU5BU5D_t2956870243* get_decltypes_10() const { return ___decltypes_10; }
	inline StringU5BU5D_t2956870243** get_address_of_decltypes_10() { return &___decltypes_10; }
	inline void set_decltypes_10(StringU5BU5D_t2956870243* value)
	{
		___decltypes_10 = value;
		Il2CppCodeGenWriteBarrier(&___decltypes_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
