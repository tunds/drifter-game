﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BarrelColorManager
struct BarrelColorManager_t855775664;

#include "codegen/il2cpp-codegen.h"

// System.Void BarrelColorManager::.ctor()
extern "C"  void BarrelColorManager__ctor_m99470571 (BarrelColorManager_t855775664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrelColorManager::ChangeMaterial()
extern "C"  void BarrelColorManager_ChangeMaterial_m302238768 (BarrelColorManager_t855775664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrelColorManager::ChangeColor()
extern "C"  void BarrelColorManager_ChangeColor_m1633365212 (BarrelColorManager_t855775664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrelColorManager::ConfigReflectionProbe()
extern "C"  void BarrelColorManager_ConfigReflectionProbe_m148102124 (BarrelColorManager_t855775664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrelColorManager::Start()
extern "C"  void BarrelColorManager_Start_m3341575659 (BarrelColorManager_t855775664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrelColorManager::Update()
extern "C"  void BarrelColorManager_Update_m515482498 (BarrelColorManager_t855775664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
