﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.EnterpriseServices.ApplicationNameAttribute
struct ApplicationNameAttribute_t3723904587;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void System.EnterpriseServices.ApplicationNameAttribute::.ctor(System.String)
extern "C"  void ApplicationNameAttribute__ctor_m1566568328 (ApplicationNameAttribute_t3723904587 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
