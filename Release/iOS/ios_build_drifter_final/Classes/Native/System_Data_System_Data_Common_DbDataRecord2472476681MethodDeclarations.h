﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DbDataRecord
struct DbDataRecord_t2472476681;
// System.ComponentModel.PropertyDescriptorCollection
struct PropertyDescriptorCollection_t3591325611;
// System.Attribute[]
struct AttributeU5BU5D_t4076389004;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Data.Common.DbDataRecord::.ctor()
extern "C"  void DbDataRecord__ctor_m1950192441 (DbDataRecord_t2472476681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.PropertyDescriptorCollection System.Data.Common.DbDataRecord::System.ComponentModel.ICustomTypeDescriptor.GetProperties()
extern "C"  PropertyDescriptorCollection_t3591325611 * DbDataRecord_System_ComponentModel_ICustomTypeDescriptor_GetProperties_m2216974304 (DbDataRecord_t2472476681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.PropertyDescriptorCollection System.Data.Common.DbDataRecord::System.ComponentModel.ICustomTypeDescriptor.GetProperties(System.Attribute[])
extern "C"  PropertyDescriptorCollection_t3591325611 * DbDataRecord_System_ComponentModel_ICustomTypeDescriptor_GetProperties_m2155868737 (DbDataRecord_t2472476681 * __this, AttributeU5BU5D_t4076389004* ___attributes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
