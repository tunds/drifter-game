﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.BestFitMappingAttribute
struct BestFitMappingAttribute_t2249605341;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.BestFitMappingAttribute::.ctor(System.Boolean)
extern "C"  void BestFitMappingAttribute__ctor_m623189302 (BestFitMappingAttribute_t2249605341 * __this, bool ___BestFitMapping0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
