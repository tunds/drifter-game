﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DataAdapter
struct DataAdapter_t2395949141;
// System.Data.Common.DataTableMappingCollection
struct DataTableMappingCollection_t2256861304;
// System.Data.DataTable
struct DataTable_t2176726999;
// System.Data.IDataReader
struct IDataReader_t1445897193;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// System.Data.FillErrorEventArgs
struct FillErrorEventArgs_t1571131301;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Exception
struct Exception_t1967233988;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_MissingMappingAction2387118769.h"
#include "System_Data_System_Data_MissingSchemaAction2534824272.h"
#include "System_Data_System_Data_DataTable2176726999.h"
#include "System_Data_System_Data_SchemaType2091769422.h"
#include "System_Data_System_Data_Common_DataTableMappingCol2256861304.h"
#include "System_Data_System_Data_FillErrorEventArgs1571131301.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_String968488902.h"

// System.Void System.Data.Common.DataAdapter::.ctor()
extern "C"  void DataAdapter__ctor_m3645184431 (DataAdapter_t2395949141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DataAdapter::get_AcceptChangesDuringFill()
extern "C"  bool DataAdapter_get_AcceptChangesDuringFill_m1120299321 (DataAdapter_t2395949141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.MissingMappingAction System.Data.Common.DataAdapter::get_MissingMappingAction()
extern "C"  int32_t DataAdapter_get_MissingMappingAction_m638433484 (DataAdapter_t2395949141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.MissingSchemaAction System.Data.Common.DataAdapter::get_MissingSchemaAction()
extern "C"  int32_t DataAdapter_get_MissingSchemaAction_m3151945766 (DataAdapter_t2395949141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DataTableMappingCollection System.Data.Common.DataAdapter::get_TableMappings()
extern "C"  DataTableMappingCollection_t2256861304 * DataAdapter_get_TableMappings_m3344405610 (DataAdapter_t2395949141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DataAdapter::Dispose(System.Boolean)
extern "C"  void DataAdapter_Dispose_m2271465123 (DataAdapter_t2395949141 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.DataAdapter::FillInternal(System.Data.DataTable,System.Data.IDataReader)
extern "C"  int32_t DataAdapter_FillInternal_m638031157 (DataAdapter_t2395949141 * __this, DataTable_t2176726999 * ___dataTable0, Il2CppObject * ___dataReader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Data.Common.DataAdapter::BuildSchema(System.Data.IDataReader,System.Data.DataTable,System.Data.SchemaType)
extern "C"  Int32U5BU5D_t1809983122* DataAdapter_BuildSchema_m863984018 (DataAdapter_t2395949141 * __this, Il2CppObject * ___reader0, DataTable_t2176726999 * ___table1, int32_t ___schemaType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Data.Common.DataAdapter::BuildSchema(System.Data.IDataReader,System.Data.DataTable,System.Data.SchemaType,System.Data.MissingSchemaAction,System.Data.MissingMappingAction,System.Data.Common.DataTableMappingCollection)
extern "C"  Int32U5BU5D_t1809983122* DataAdapter_BuildSchema_m586202339 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___reader0, DataTable_t2176726999 * ___table1, int32_t ___schemaType2, int32_t ___missingSchAction3, int32_t ___missingMapAction4, DataTableMappingCollection_t2256861304 * ___dtMapping5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.Common.DataAdapter::FillTable(System.Data.DataTable,System.Data.IDataReader,System.Int32,System.Int32,System.Int32&)
extern "C"  bool DataAdapter_FillTable_m3370233973 (DataAdapter_t2395949141 * __this, DataTable_t2176726999 * ___dataTable0, Il2CppObject * ___dataReader1, int32_t ___startRecord2, int32_t ___maxRecords3, int32_t* ___counter4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DataAdapter::OnFillErrorInternal(System.Data.FillErrorEventArgs)
extern "C"  void DataAdapter_OnFillErrorInternal_m1783209401 (DataAdapter_t2395949141 * __this, FillErrorEventArgs_t1571131301 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.FillErrorEventArgs System.Data.Common.DataAdapter::CreateFillErrorEvent(System.Data.DataTable,System.Object[],System.Exception)
extern "C"  FillErrorEventArgs_t1571131301 * DataAdapter_CreateFillErrorEvent_m3023484921 (DataAdapter_t2395949141 * __this, DataTable_t2176726999 * ___dataTable0, ObjectU5BU5D_t11523773* ___values1, Exception_t1967233988 * ___e2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.DataAdapter::SetupSchema(System.Data.SchemaType,System.String)
extern "C"  String_t* DataAdapter_SetupSchema_m2796182052 (DataAdapter_t2395949141 * __this, int32_t ___schemaType0, String_t* ___sourceTableName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.DataAdapter::Fill(System.Data.DataTable,System.Data.IDataReader)
extern "C"  int32_t DataAdapter_Fill_m3875968690 (DataAdapter_t2395949141 * __this, DataTable_t2176726999 * ___dataTable0, Il2CppObject * ___dataReader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DataAdapter::OnFillError(System.Data.FillErrorEventArgs)
extern "C"  void DataAdapter_OnFillError_m605501660 (DataAdapter_t2395949141 * __this, FillErrorEventArgs_t1571131301 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
