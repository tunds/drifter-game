﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// player/AniClip
struct AniClip_t806476588;
// UnityEngine.Animation
struct Animation_t350396337;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// player
struct  player_t3309214433  : public MonoBehaviour_t3012272455
{
public:
	// player/AniClip player::ani
	AniClip_t806476588 * ___ani_2;
	// UnityEngine.Animation player::playerAni
	Animation_t350396337 * ___playerAni_3;
	// System.Int32 player::weaponId
	int32_t ___weaponId_4;
	// System.Boolean player::weaponVisible
	bool ___weaponVisible_5;
	// UnityEngine.GameObject player::weapon1
	GameObject_t4012695102 * ___weapon1_6;
	// UnityEngine.GameObject player::weapon2
	GameObject_t4012695102 * ___weapon2_7;
	// UnityEngine.GameObject player::weapon3
	GameObject_t4012695102 * ___weapon3_8;
	// UnityEngine.GameObject player::FXattack1
	GameObject_t4012695102 * ___FXattack1_9;
	// UnityEngine.GameObject player::FXattack2
	GameObject_t4012695102 * ___FXattack2_10;
	// UnityEngine.GameObject player::FXattack_weapon
	GameObject_t4012695102 * ___FXattack_weapon_11;
	// UnityEngine.GameObject player::FXskillA
	GameObject_t4012695102 * ___FXskillA_12;
	// UnityEngine.GameObject player::FXskillB
	GameObject_t4012695102 * ___FXskillB_13;
	// UnityEngine.GameObject player::FXskillC
	GameObject_t4012695102 * ___FXskillC_14;
	// UnityEngine.GameObject player::FXsturn
	GameObject_t4012695102 * ___FXsturn_15;
	// UnityEngine.GameObject player::FXhit
	GameObject_t4012695102 * ___FXhit_16;

public:
	inline static int32_t get_offset_of_ani_2() { return static_cast<int32_t>(offsetof(player_t3309214433, ___ani_2)); }
	inline AniClip_t806476588 * get_ani_2() const { return ___ani_2; }
	inline AniClip_t806476588 ** get_address_of_ani_2() { return &___ani_2; }
	inline void set_ani_2(AniClip_t806476588 * value)
	{
		___ani_2 = value;
		Il2CppCodeGenWriteBarrier(&___ani_2, value);
	}

	inline static int32_t get_offset_of_playerAni_3() { return static_cast<int32_t>(offsetof(player_t3309214433, ___playerAni_3)); }
	inline Animation_t350396337 * get_playerAni_3() const { return ___playerAni_3; }
	inline Animation_t350396337 ** get_address_of_playerAni_3() { return &___playerAni_3; }
	inline void set_playerAni_3(Animation_t350396337 * value)
	{
		___playerAni_3 = value;
		Il2CppCodeGenWriteBarrier(&___playerAni_3, value);
	}

	inline static int32_t get_offset_of_weaponId_4() { return static_cast<int32_t>(offsetof(player_t3309214433, ___weaponId_4)); }
	inline int32_t get_weaponId_4() const { return ___weaponId_4; }
	inline int32_t* get_address_of_weaponId_4() { return &___weaponId_4; }
	inline void set_weaponId_4(int32_t value)
	{
		___weaponId_4 = value;
	}

	inline static int32_t get_offset_of_weaponVisible_5() { return static_cast<int32_t>(offsetof(player_t3309214433, ___weaponVisible_5)); }
	inline bool get_weaponVisible_5() const { return ___weaponVisible_5; }
	inline bool* get_address_of_weaponVisible_5() { return &___weaponVisible_5; }
	inline void set_weaponVisible_5(bool value)
	{
		___weaponVisible_5 = value;
	}

	inline static int32_t get_offset_of_weapon1_6() { return static_cast<int32_t>(offsetof(player_t3309214433, ___weapon1_6)); }
	inline GameObject_t4012695102 * get_weapon1_6() const { return ___weapon1_6; }
	inline GameObject_t4012695102 ** get_address_of_weapon1_6() { return &___weapon1_6; }
	inline void set_weapon1_6(GameObject_t4012695102 * value)
	{
		___weapon1_6 = value;
		Il2CppCodeGenWriteBarrier(&___weapon1_6, value);
	}

	inline static int32_t get_offset_of_weapon2_7() { return static_cast<int32_t>(offsetof(player_t3309214433, ___weapon2_7)); }
	inline GameObject_t4012695102 * get_weapon2_7() const { return ___weapon2_7; }
	inline GameObject_t4012695102 ** get_address_of_weapon2_7() { return &___weapon2_7; }
	inline void set_weapon2_7(GameObject_t4012695102 * value)
	{
		___weapon2_7 = value;
		Il2CppCodeGenWriteBarrier(&___weapon2_7, value);
	}

	inline static int32_t get_offset_of_weapon3_8() { return static_cast<int32_t>(offsetof(player_t3309214433, ___weapon3_8)); }
	inline GameObject_t4012695102 * get_weapon3_8() const { return ___weapon3_8; }
	inline GameObject_t4012695102 ** get_address_of_weapon3_8() { return &___weapon3_8; }
	inline void set_weapon3_8(GameObject_t4012695102 * value)
	{
		___weapon3_8 = value;
		Il2CppCodeGenWriteBarrier(&___weapon3_8, value);
	}

	inline static int32_t get_offset_of_FXattack1_9() { return static_cast<int32_t>(offsetof(player_t3309214433, ___FXattack1_9)); }
	inline GameObject_t4012695102 * get_FXattack1_9() const { return ___FXattack1_9; }
	inline GameObject_t4012695102 ** get_address_of_FXattack1_9() { return &___FXattack1_9; }
	inline void set_FXattack1_9(GameObject_t4012695102 * value)
	{
		___FXattack1_9 = value;
		Il2CppCodeGenWriteBarrier(&___FXattack1_9, value);
	}

	inline static int32_t get_offset_of_FXattack2_10() { return static_cast<int32_t>(offsetof(player_t3309214433, ___FXattack2_10)); }
	inline GameObject_t4012695102 * get_FXattack2_10() const { return ___FXattack2_10; }
	inline GameObject_t4012695102 ** get_address_of_FXattack2_10() { return &___FXattack2_10; }
	inline void set_FXattack2_10(GameObject_t4012695102 * value)
	{
		___FXattack2_10 = value;
		Il2CppCodeGenWriteBarrier(&___FXattack2_10, value);
	}

	inline static int32_t get_offset_of_FXattack_weapon_11() { return static_cast<int32_t>(offsetof(player_t3309214433, ___FXattack_weapon_11)); }
	inline GameObject_t4012695102 * get_FXattack_weapon_11() const { return ___FXattack_weapon_11; }
	inline GameObject_t4012695102 ** get_address_of_FXattack_weapon_11() { return &___FXattack_weapon_11; }
	inline void set_FXattack_weapon_11(GameObject_t4012695102 * value)
	{
		___FXattack_weapon_11 = value;
		Il2CppCodeGenWriteBarrier(&___FXattack_weapon_11, value);
	}

	inline static int32_t get_offset_of_FXskillA_12() { return static_cast<int32_t>(offsetof(player_t3309214433, ___FXskillA_12)); }
	inline GameObject_t4012695102 * get_FXskillA_12() const { return ___FXskillA_12; }
	inline GameObject_t4012695102 ** get_address_of_FXskillA_12() { return &___FXskillA_12; }
	inline void set_FXskillA_12(GameObject_t4012695102 * value)
	{
		___FXskillA_12 = value;
		Il2CppCodeGenWriteBarrier(&___FXskillA_12, value);
	}

	inline static int32_t get_offset_of_FXskillB_13() { return static_cast<int32_t>(offsetof(player_t3309214433, ___FXskillB_13)); }
	inline GameObject_t4012695102 * get_FXskillB_13() const { return ___FXskillB_13; }
	inline GameObject_t4012695102 ** get_address_of_FXskillB_13() { return &___FXskillB_13; }
	inline void set_FXskillB_13(GameObject_t4012695102 * value)
	{
		___FXskillB_13 = value;
		Il2CppCodeGenWriteBarrier(&___FXskillB_13, value);
	}

	inline static int32_t get_offset_of_FXskillC_14() { return static_cast<int32_t>(offsetof(player_t3309214433, ___FXskillC_14)); }
	inline GameObject_t4012695102 * get_FXskillC_14() const { return ___FXskillC_14; }
	inline GameObject_t4012695102 ** get_address_of_FXskillC_14() { return &___FXskillC_14; }
	inline void set_FXskillC_14(GameObject_t4012695102 * value)
	{
		___FXskillC_14 = value;
		Il2CppCodeGenWriteBarrier(&___FXskillC_14, value);
	}

	inline static int32_t get_offset_of_FXsturn_15() { return static_cast<int32_t>(offsetof(player_t3309214433, ___FXsturn_15)); }
	inline GameObject_t4012695102 * get_FXsturn_15() const { return ___FXsturn_15; }
	inline GameObject_t4012695102 ** get_address_of_FXsturn_15() { return &___FXsturn_15; }
	inline void set_FXsturn_15(GameObject_t4012695102 * value)
	{
		___FXsturn_15 = value;
		Il2CppCodeGenWriteBarrier(&___FXsturn_15, value);
	}

	inline static int32_t get_offset_of_FXhit_16() { return static_cast<int32_t>(offsetof(player_t3309214433, ___FXhit_16)); }
	inline GameObject_t4012695102 * get_FXhit_16() const { return ___FXhit_16; }
	inline GameObject_t4012695102 ** get_address_of_FXhit_16() { return &___FXhit_16; }
	inline void set_FXhit_16(GameObject_t4012695102 * value)
	{
		___FXhit_16 = value;
		Il2CppCodeGenWriteBarrier(&___FXhit_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
