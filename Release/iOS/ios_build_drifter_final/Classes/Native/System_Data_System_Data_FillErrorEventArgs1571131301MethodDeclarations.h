﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.FillErrorEventArgs
struct FillErrorEventArgs_t1571131301;
// System.Data.DataTable
struct DataTable_t2176726999;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_DataTable2176726999.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void System.Data.FillErrorEventArgs::.ctor(System.Data.DataTable,System.Object[])
extern "C"  void FillErrorEventArgs__ctor_m2064609120 (FillErrorEventArgs_t1571131301 * __this, DataTable_t2176726999 * ___dataTable0, ObjectU5BU5D_t11523773* ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Data.FillErrorEventArgs::get_Continue()
extern "C"  bool FillErrorEventArgs_get_Continue_m579979423 (FillErrorEventArgs_t1571131301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.FillErrorEventArgs::set_Continue(System.Boolean)
extern "C"  void FillErrorEventArgs_set_Continue_m2916573056 (FillErrorEventArgs_t1571131301 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.FillErrorEventArgs::set_Errors(System.Exception)
extern "C"  void FillErrorEventArgs_set_Errors_m3041983101 (FillErrorEventArgs_t1571131301 * __this, Exception_t1967233988 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
