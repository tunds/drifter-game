﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// SQLiter.Example
struct Example_t2851665324;
// System.Data.IDbConnection
struct IDbConnection_t2001212056;
// System.Data.IDbCommand
struct IDbCommand_t2345198679;
// System.Data.IDataReader
struct IDataReader_t1445897193;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLiter.Example
struct  Example_t2851665324  : public MonoBehaviour_t3012272455
{
public:
	// System.Boolean SQLiter.Example::DebugMode
	bool ___DebugMode_7;
	// System.Data.IDbConnection SQLiter.Example::mConnection
	Il2CppObject * ___mConnection_9;
	// System.Data.IDbCommand SQLiter.Example::mCommand
	Il2CppObject * ___mCommand_10;
	// System.Data.IDataReader SQLiter.Example::mReader
	Il2CppObject * ___mReader_11;
	// System.String SQLiter.Example::mSQLString
	String_t* ___mSQLString_12;
	// System.Boolean SQLiter.Example::mCreateNewTable
	bool ___mCreateNewTable_13;

public:
	inline static int32_t get_offset_of_DebugMode_7() { return static_cast<int32_t>(offsetof(Example_t2851665324, ___DebugMode_7)); }
	inline bool get_DebugMode_7() const { return ___DebugMode_7; }
	inline bool* get_address_of_DebugMode_7() { return &___DebugMode_7; }
	inline void set_DebugMode_7(bool value)
	{
		___DebugMode_7 = value;
	}

	inline static int32_t get_offset_of_mConnection_9() { return static_cast<int32_t>(offsetof(Example_t2851665324, ___mConnection_9)); }
	inline Il2CppObject * get_mConnection_9() const { return ___mConnection_9; }
	inline Il2CppObject ** get_address_of_mConnection_9() { return &___mConnection_9; }
	inline void set_mConnection_9(Il2CppObject * value)
	{
		___mConnection_9 = value;
		Il2CppCodeGenWriteBarrier(&___mConnection_9, value);
	}

	inline static int32_t get_offset_of_mCommand_10() { return static_cast<int32_t>(offsetof(Example_t2851665324, ___mCommand_10)); }
	inline Il2CppObject * get_mCommand_10() const { return ___mCommand_10; }
	inline Il2CppObject ** get_address_of_mCommand_10() { return &___mCommand_10; }
	inline void set_mCommand_10(Il2CppObject * value)
	{
		___mCommand_10 = value;
		Il2CppCodeGenWriteBarrier(&___mCommand_10, value);
	}

	inline static int32_t get_offset_of_mReader_11() { return static_cast<int32_t>(offsetof(Example_t2851665324, ___mReader_11)); }
	inline Il2CppObject * get_mReader_11() const { return ___mReader_11; }
	inline Il2CppObject ** get_address_of_mReader_11() { return &___mReader_11; }
	inline void set_mReader_11(Il2CppObject * value)
	{
		___mReader_11 = value;
		Il2CppCodeGenWriteBarrier(&___mReader_11, value);
	}

	inline static int32_t get_offset_of_mSQLString_12() { return static_cast<int32_t>(offsetof(Example_t2851665324, ___mSQLString_12)); }
	inline String_t* get_mSQLString_12() const { return ___mSQLString_12; }
	inline String_t** get_address_of_mSQLString_12() { return &___mSQLString_12; }
	inline void set_mSQLString_12(String_t* value)
	{
		___mSQLString_12 = value;
		Il2CppCodeGenWriteBarrier(&___mSQLString_12, value);
	}

	inline static int32_t get_offset_of_mCreateNewTable_13() { return static_cast<int32_t>(offsetof(Example_t2851665324, ___mCreateNewTable_13)); }
	inline bool get_mCreateNewTable_13() const { return ___mCreateNewTable_13; }
	inline bool* get_address_of_mCreateNewTable_13() { return &___mCreateNewTable_13; }
	inline void set_mCreateNewTable_13(bool value)
	{
		___mCreateNewTable_13 = value;
	}
};

struct Example_t2851665324_StaticFields
{
public:
	// SQLiter.Example SQLiter.Example::Instance
	Example_t2851665324 * ___Instance_6;
	// System.String SQLiter.Example::SQL_DB_LOCATION
	String_t* ___SQL_DB_LOCATION_8;

public:
	inline static int32_t get_offset_of_Instance_6() { return static_cast<int32_t>(offsetof(Example_t2851665324_StaticFields, ___Instance_6)); }
	inline Example_t2851665324 * get_Instance_6() const { return ___Instance_6; }
	inline Example_t2851665324 ** get_address_of_Instance_6() { return &___Instance_6; }
	inline void set_Instance_6(Example_t2851665324 * value)
	{
		___Instance_6 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_6, value);
	}

	inline static int32_t get_offset_of_SQL_DB_LOCATION_8() { return static_cast<int32_t>(offsetof(Example_t2851665324_StaticFields, ___SQL_DB_LOCATION_8)); }
	inline String_t* get_SQL_DB_LOCATION_8() const { return ___SQL_DB_LOCATION_8; }
	inline String_t** get_address_of_SQL_DB_LOCATION_8() { return &___SQL_DB_LOCATION_8; }
	inline void set_SQL_DB_LOCATION_8(String_t* value)
	{
		___SQL_DB_LOCATION_8 = value;
		Il2CppCodeGenWriteBarrier(&___SQL_DB_LOCATION_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
