﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.SqlTypes.SqlTruncateException
struct SqlTruncateException_t449992971;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void System.Data.SqlTypes.SqlTruncateException::.ctor()
extern "C"  void SqlTruncateException__ctor_m3075892663 (SqlTruncateException_t449992971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.SqlTypes.SqlTruncateException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SqlTruncateException__ctor_m4183541368 (SqlTruncateException_t449992971 * __this, SerializationInfo_t2995724695 * ___si0, StreamingContext_t986364934  ___sc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.SqlTypes.SqlTruncateException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SqlTruncateException_System_Runtime_Serialization_ISerializable_GetObjectData_m458483188 (SqlTruncateException_t449992971 * __this, SerializationInfo_t2995724695 * ___si0, StreamingContext_t986364934  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
