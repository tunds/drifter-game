﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t2121638921;
// System.Collections.Hashtable
struct Hashtable_t3875263730;

#include "System_Data_System_Data_Common_DbParameterCollecti3381130713.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqliteClient.SqliteParameterCollection
struct  SqliteParameterCollection_t3807043211  : public DbParameterCollection_t3381130713
{
public:
	// System.Collections.ArrayList Mono.Data.SqliteClient.SqliteParameterCollection::numeric_param_list
	ArrayList_t2121638921 * ___numeric_param_list_1;
	// System.Collections.Hashtable Mono.Data.SqliteClient.SqliteParameterCollection::named_param_hash
	Hashtable_t3875263730 * ___named_param_hash_2;

public:
	inline static int32_t get_offset_of_numeric_param_list_1() { return static_cast<int32_t>(offsetof(SqliteParameterCollection_t3807043211, ___numeric_param_list_1)); }
	inline ArrayList_t2121638921 * get_numeric_param_list_1() const { return ___numeric_param_list_1; }
	inline ArrayList_t2121638921 ** get_address_of_numeric_param_list_1() { return &___numeric_param_list_1; }
	inline void set_numeric_param_list_1(ArrayList_t2121638921 * value)
	{
		___numeric_param_list_1 = value;
		Il2CppCodeGenWriteBarrier(&___numeric_param_list_1, value);
	}

	inline static int32_t get_offset_of_named_param_hash_2() { return static_cast<int32_t>(offsetof(SqliteParameterCollection_t3807043211, ___named_param_hash_2)); }
	inline Hashtable_t3875263730 * get_named_param_hash_2() const { return ___named_param_hash_2; }
	inline Hashtable_t3875263730 ** get_address_of_named_param_hash_2() { return &___named_param_hash_2; }
	inline void set_named_param_hash_2(Hashtable_t3875263730 * value)
	{
		___named_param_hash_2 = value;
		Il2CppCodeGenWriteBarrier(&___named_param_hash_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
