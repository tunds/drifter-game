﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.DateTimeDataContainer
struct DateTimeDataContainer_t2868619308;
// System.Data.ISafeDataRecord
struct ISafeDataRecord_t3927591524;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Data.Common.DateTimeDataContainer::.ctor()
extern "C"  void DateTimeDataContainer__ctor_m1307603576 (DateTimeDataContainer_t2868619308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DateTimeDataContainer::SetValueFromSafeDataRecord(System.Int32,System.Data.ISafeDataRecord,System.Int32)
extern "C"  void DateTimeDataContainer_SetValueFromSafeDataRecord_m1479653945 (DateTimeDataContainer_t2868619308 * __this, int32_t ___index0, Il2CppObject * ___record1, int32_t ___field2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.DateTimeDataContainer::SetValue(System.Int32,System.Object)
extern "C"  void DateTimeDataContainer_SetValue_m3645786970 (DateTimeDataContainer_t2868619308 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
