﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLiteDatabase.SQLiteDB/DB_DataPair
struct  DB_DataPair_t4220460133 
{
public:
	// System.String SQLiteDatabase.SQLiteDB/DB_DataPair::fieldName
	String_t* ___fieldName_0;
	// System.String SQLiteDatabase.SQLiteDB/DB_DataPair::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_fieldName_0() { return static_cast<int32_t>(offsetof(DB_DataPair_t4220460133, ___fieldName_0)); }
	inline String_t* get_fieldName_0() const { return ___fieldName_0; }
	inline String_t** get_address_of_fieldName_0() { return &___fieldName_0; }
	inline void set_fieldName_0(String_t* value)
	{
		___fieldName_0 = value;
		Il2CppCodeGenWriteBarrier(&___fieldName_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(DB_DataPair_t4220460133, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: SQLiteDatabase.SQLiteDB/DB_DataPair
struct DB_DataPair_t4220460133_marshaled_pinvoke
{
	char* ___fieldName_0;
	char* ___value_1;
};
// Native definition for marshalling of: SQLiteDatabase.SQLiteDB/DB_DataPair
struct DB_DataPair_t4220460133_marshaled_com
{
	uint16_t* ___fieldName_0;
	uint16_t* ___value_1;
};
