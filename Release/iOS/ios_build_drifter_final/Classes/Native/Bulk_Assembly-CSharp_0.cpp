﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// anims_controls
struct anims_controls_t1059809747;
// UnityEngine.Animator
struct Animator_t792326996;
// System.Object
struct Il2CppObject;
// BarrelColorManager
struct BarrelColorManager_t855775664;
// CloseManager
struct CloseManager_t3949459125;
// control_script
struct control_script_t3661633005;
// DatabaseExample
struct DatabaseExample_t2874326863;
// System.String
struct String_t;
// DatabaseManager
struct DatabaseManager_t737590674;
// EnemyAttack
struct EnemyAttack_t1231588304;
// UnityEngine.AudioSource
struct AudioSource_t3628549054;
// EnemyHealth
struct EnemyHealth_t1417584612;
// PlayerHealth
struct PlayerHealth_t3877793981;
// PlayerEffects
struct PlayerEffects_t1618007745;
// UnityEngine.Collider
struct Collider_t955670625;
// UnityEngine.CapsuleCollider[]
struct CapsuleColliderU5BU5D_t3130374427;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t3296505762;
// UnityEngine.Rigidbody
struct Rigidbody_t1972007546;
// EnemyManager
struct EnemyManager_t1035150117;
// EnemyMovement
struct EnemyMovement_t1797938231;
// EventsManager
struct EventsManager_t4180097076;
// UnityEngine.UI.Text
struct Text_t3286458198;
// GlobalObjectManager
struct GlobalObjectManager_t849077355;
// UnityEngine.UI.InputField
struct InputField_t2345609593;
// UnityEngine.UI.Button
struct Button_t990034267;
// FollowCharacter
struct FollowCharacter_t1365044824;
// GameTimer
struct GameTimer_t2994639443;
// GetScores
struct GetScores_t2792216311;
// UnityEngine.AudioListener
struct AudioListener_t1735598807;
// PlayerAttack
struct PlayerAttack_t3691797673;
// PlayerPickups
struct PlayerPickups_t2873926454;
// MenuManager
struct MenuManager_t3994435886;
// MouseLook
struct MouseLook_t2590096580;
// MoveLevel
struct MoveLevel_t3054455315;
// moveOBJ
struct moveOBJ_t1243551590;
// MoveUpDown
struct MoveUpDown_t465175854;
// PickupEffects
struct PickupEffects_t2342892230;
// PickUpManager
struct PickUpManager_t2721081681;
// PickUpRotate
struct PickUpRotate_t521125687;
// player
struct player_t3309214433;
// player/AniClip
struct AniClip_t806476588;
// UnityEngine.AudioClip
struct AudioClip_t3714538611;
// UnityEngine.Animation
struct Animation_t350396337;
// PlayerMovement
struct PlayerMovement_t3827129040;
// UnityEngine.CharacterController
struct CharacterController_t2029520850;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1217738301;
// UnityEngine.ParticleSystem
struct ParticleSystem_t56787138;
// PoqXert.MessageBox.DialogResultMethod
struct DialogResultMethod_t1789391825;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// PoqXert.MessageBox.MsgBox
struct MsgBox_t1468720533;
// PoqXert.MessageBox.MSGBoxStyle
struct MSGBoxStyle_t1923386386;
// UnityEngine.Sprite
struct Sprite_t4006040370;
// PXMSGExample
struct PXMSGExample_t104414929;
// ToggleMobileControls
struct ToggleMobileControls_t612291084;
// UnityEngine.UI.Image
struct Image_t3354615620;
// TurnOBJ
struct TurnOBJ_t699144986;
// turnSpaceBar
struct turnSpaceBar_t2231649482;
// turnView
struct turnView_t134260930;
// VolumeControl
struct VolumeControl_t2768303299;
// UnityEngine.UI.Slider
struct Slider_t1468074762;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_anims_controls1059809747.h"
#include "AssemblyU2DCSharp_anims_controls1059809747MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator792326996.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "UnityEngine_UnityEngine_Animator792326996MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Boolean211005341.h"
#include "AssemblyU2DCSharp_BarrelColorManager855775664.h"
#include "AssemblyU2DCSharp_BarrelColorManager855775664MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material1886596500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer1092684080MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material1886596500.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "UnityEngine_UnityEngine_Renderer1092684080.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_ReflectionProbe936962674MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ReflectionProbe936962674.h"
#include "AssemblyU2DCSharp_BarrelColorManager_ReflectMode2261419488.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeM1822783935.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "AssemblyU2DCSharp_BarrelColorManager_ReflectMode2261419488MethodDeclarations.h"
#include "AssemblyU2DCSharp_CloseManager3949459125.h"
#include "AssemblyU2DCSharp_CloseManager3949459125MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag1523288937MethodDeclarations.h"
#include "AssemblyU2DCSharp_control_script3661633005.h"
#include "AssemblyU2DCSharp_control_script3661633005MethodDeclarations.h"
#include "AssemblyU2DCSharp_DatabaseExample2874326863.h"
#include "AssemblyU2DCSharp_DatabaseExample2874326863MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB557922535MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB557922535.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "LocalDB_SQLiteDatabase_SQLiteEventListener_OnError314785609MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_SQLiteEventListener1751327711MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_SQLiteEventListener_OnError314785609.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_Rect1525428817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3994030297MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI1522956648MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Single958209021.h"
#include "LocalDB_SQLiteDatabase_DBSchema4247391996MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_DBSchema4247391996.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataType4220602565.h"
#include "mscorlib_System_Collections_Generic_List_1_gen722451806MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen722451806.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataPair4220460133.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_ConditionPair1745523828.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Condition1501727354.h"
#include "LocalDB_SQLiteDatabase_DBReader4220400158MethodDeclarations.h"
#include "LocalDB_SQLiteDatabase_DBReader4220400158.h"
#include "AssemblyU2DCSharp_DatabaseManager737590674.h"
#include "AssemblyU2DCSharp_DatabaseManager737590674MethodDeclarations.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "AssemblyU2DCSharp_EnemyAttack1231588304.h"
#include "AssemblyU2DCSharp_EnemyAttack1231588304MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource3628549054.h"
#include "AssemblyU2DCSharp_EnemyHealth1417584612.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "AssemblyU2DCSharp_PlayerHealth3877793981.h"
#include "AssemblyU2DCSharp_PlayerEffects1618007745.h"
#include "UnityEngine_UnityEngine_Time1525492538MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource3628549054MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip3714538611.h"
#include "AssemblyU2DCSharp_PlayerHealth3877793981MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"
#include "AssemblyU2DCSharp_PlayerEffects1618007745MethodDeclarations.h"
#include "AssemblyU2DCSharp_EnemyHealth1417584612MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_CapsuleCollider3138252142.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider1468074762.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider1468074762MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour3120504042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody1972007546MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider955670625MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NavMeshAgent3296505762.h"
#include "UnityEngine_UnityEngine_Rigidbody1972007546.h"
#include "AssemblyU2DCSharp_EnemyManager1035150117.h"
#include "AssemblyU2DCSharp_EnemyManager1035150117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3963434288MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "AssemblyU2DCSharp_EnemyMovement1797938231.h"
#include "AssemblyU2DCSharp_EnemyMovement1797938231MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NavMeshAgent3296505762MethodDeclarations.h"
#include "AssemblyU2DCSharp_EventsManager4180097076.h"
#include "AssemblyU2DCSharp_EventsManager4180097076MethodDeclarations.h"
#include "AssemblyU2DCSharp_GlobalObjectManager849077355.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField2345609593.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField2345609593MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable3621744255MethodDeclarations.h"
#include "mscorlib_System_Int642847414882MethodDeclarations.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_DialogResultM1789391825MethodDeclarations.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_MsgBox1468720533MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button990034267.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_DialogResult3964159632.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_DialogResultM1789391825.h"
#include "AssemblyU2DCSharp_FollowCharacter1365044824.h"
#include "AssemblyU2DCSharp_FollowCharacter1365044824MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameTimer2994639443.h"
#include "AssemblyU2DCSharp_GameTimer2994639443MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936.h"
#include "AssemblyU2DCSharp_GlobalObjectManager849077355MethodDeclarations.h"
#include "AssemblyU2DCSharp_GetScores2792216311.h"
#include "AssemblyU2DCSharp_GetScores2792216311MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera3533968274MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera3533968274.h"
#include "UnityEngine_UnityEngine_AudioListener1735598807.h"
#include "AssemblyU2DCSharp_PlayerAttack3691797673.h"
#include "AssemblyU2DCSharp_PlayerPickups2873926454.h"
#include "AssemblyU2DCSharp_MenuManager3994435886.h"
#include "AssemblyU2DCSharp_MenuManager3994435886MethodDeclarations.h"
#include "AssemblyU2DCSharp_MouseLook2590096580.h"
#include "AssemblyU2DCSharp_MouseLook2590096580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1593691127MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf1597001355MethodDeclarations.h"
#include "AssemblyU2DCSharp_MouseLook_RotationAxes1834209251.h"
#include "AssemblyU2DCSharp_MouseLook_RotationAxes1834209251MethodDeclarations.h"
#include "AssemblyU2DCSharp_MoveLevel3054455315.h"
#include "AssemblyU2DCSharp_MoveLevel3054455315MethodDeclarations.h"
#include "AssemblyU2DCSharp_moveOBJ1243551590.h"
#include "AssemblyU2DCSharp_moveOBJ1243551590MethodDeclarations.h"
#include "AssemblyU2DCSharp_MoveUpDown465175854.h"
#include "AssemblyU2DCSharp_MoveUpDown465175854MethodDeclarations.h"
#include "AssemblyU2DCSharp_PickupEffects2342892230.h"
#include "AssemblyU2DCSharp_PickupEffects2342892230MethodDeclarations.h"
#include "AssemblyU2DCSharp_PickUpManager2721081681.h"
#include "AssemblyU2DCSharp_PickUpManager2721081681MethodDeclarations.h"
#include "AssemblyU2DCSharp_PickUpRotate521125687.h"
#include "AssemblyU2DCSharp_PickUpRotate521125687MethodDeclarations.h"
#include "AssemblyU2DCSharp_player3309214433.h"
#include "AssemblyU2DCSharp_player3309214433MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation350396337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation350396337.h"
#include "AssemblyU2DCSharp_player_AniClip806476588.h"
#include "UnityEngine_UnityEngine_AnimationClip57566497.h"
#include "AssemblyU2DCSharp_player_AniClip806476588MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayerAttack3691797673MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LayerMask1862190090MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_PlayerPickups2873926454MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_CnInputMa3458767273MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2371581209.h"
#include "UnityEngine_UnityEngine_Ray1522967639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics2601443956MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit46221527MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray1522967639.h"
#include "UnityEngine_UnityEngine_RaycastHit46221527.h"
#include "UnityEngine_UnityEngine_PlayMode3466086244.h"
#include "AssemblyU2DCSharp_PlayerMovement3827129040.h"
#include "AssemblyU2DCSharp_PlayerMovement3827129040MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterController2029520850.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterController2029520850MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CollisionFlags165451490.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395.h"
#include "UnityEngine_UnityEngine_MeshRenderer1217738301.h"
#include "UnityEngine_UnityEngine_ParticleSystem56787138MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem56787138.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_DialogResult3964159632MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_MsgBox1468720533.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2720345355MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2720345355.h"
#include "UnityEngine_UnityEngine_Resources1543782994MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources1543782994.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_MsgBoxButtons1335831650.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_MSGBoxStyle1923386386.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_MSGBoxStyle1923386386MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic933884113MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image3354615620MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837.h"
#include "UnityEngine_UI_UnityEngine_UI_Image3354615620.h"
#include "UnityEngine_UnityEngine_Sprite4006040370.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis2055105.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_MsgBoxStyle2220920850.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystem409518532MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystem409518532.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_MsgBoxButtons1335831650MethodDeclarations.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_MsgBoxStyle2220920850MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject184905905MethodDeclarations.h"
#include "AssemblyU2DCSharp_PXMSGExample104414929.h"
#include "AssemblyU2DCSharp_PXMSGExample104414929MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1574985880.h"
#include "mscorlib_System_Enum2778772662MethodDeclarations.h"
#include "mscorlib_System_Enum2778772662.h"
#include "AssemblyU2DCSharp_ToggleMobileControls612291084.h"
#include "AssemblyU2DCSharp_ToggleMobileControls612291084MethodDeclarations.h"
#include "AssemblyU2DCSharp_TurnOBJ699144986.h"
#include "AssemblyU2DCSharp_TurnOBJ699144986MethodDeclarations.h"
#include "AssemblyU2DCSharp_turnSpaceBar2231649482.h"
#include "AssemblyU2DCSharp_turnSpaceBar2231649482MethodDeclarations.h"
#include "AssemblyU2DCSharp_turnView134260930.h"
#include "AssemblyU2DCSharp_turnView134260930MethodDeclarations.h"
#include "AssemblyU2DCSharp_VolumeControl2768303299.h"
#include "AssemblyU2DCSharp_VolumeControl2768303299MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioListener1735598807MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t792326996_m4147395588(__this, method) ((  Animator_t792326996 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m900797242(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Animator>()
#define Component_GetComponentInChildren_TisAnimator_t792326996_m1462315915(__this, method) ((  Animator_t792326996 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t3628549054_m3821406207(__this, method) ((  AudioSource_t3628549054 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<EnemyHealth>()
#define Component_GetComponent_TisEnemyHealth_t1417584612_m2559288533(__this, method) ((  EnemyHealth_t1417584612 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m3652735468(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PlayerHealth>()
#define GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008(__this, method) ((  PlayerHealth_t3877793981 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PlayerEffects>()
#define GameObject_GetComponent_TisPlayerEffects_t1618007745_m1848109824(__this, method) ((  PlayerEffects_t1618007745 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<EnemyAttack>()
#define Component_GetComponent_TisEnemyAttack_t1231588304_m2051088745(__this, method) ((  EnemyAttack_t1231588304 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponents_TisIl2CppObject_m4264249070_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponents_TisIl2CppObject_m4264249070(__this, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponents_TisIl2CppObject_m4264249070_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponents<UnityEngine.CapsuleCollider>()
#define Component_GetComponents_TisCapsuleCollider_t3138252142_m3419792738(__this, method) ((  CapsuleColliderU5BU5D_t3130374427* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponents_TisIl2CppObject_m4264249070_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.NavMeshAgent>()
#define Component_GetComponent_TisNavMeshAgent_t3296505762_m3159610649(__this, method) ((  NavMeshAgent_t3296505762 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t1972007546_m2213165263(__this, method) ((  Rigidbody_t1972007546 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<PlayerHealth>()
#define Component_GetComponent_TisPlayerHealth_t3877793981_m654738856(__this, method) ((  PlayerHealth_t3877793981 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t3286458198_m202917489(__this, method) ((  Text_t3286458198 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<GlobalObjectManager>()
#define GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422(__this, method) ((  GlobalObjectManager_t849077355 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.InputField>()
#define GameObject_GetComponent_TisInputField_t2345609593_m2575864366(__this, method) ((  InputField_t2345609593 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t990034267_m2812094092(__this, method) ((  Button_t990034267 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t3286458198_m404091797(__this, method) ((  Text_t3286458198 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioListener>()
#define Component_GetComponent_TisAudioListener_t1735598807_m1628673542(__this, method) ((  AudioListener_t1735598807 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PlayerAttack>()
#define GameObject_GetComponent_TisPlayerAttack_t3691797673_m1464963220(__this, method) ((  PlayerAttack_t3691797673 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PlayerPickups>()
#define GameObject_GetComponent_TisPlayerPickups_t2873926454_m3462254187(__this, method) ((  PlayerPickups_t2873926454 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<GameTimer>()
#define GameObject_GetComponent_TisGameTimer_t2994639443_m1318047534(__this, method) ((  GameTimer_t2994639443 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
#define GameObject_GetComponent_TisAudioSource_t3628549054_m1155306151(__this, method) ((  AudioSource_t3628549054 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<PlayerEffects>()
#define Component_GetComponent_TisPlayerEffects_t1618007745_m3926634072(__this, method) ((  PlayerEffects_t1618007745 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<PlayerPickups>()
#define Component_GetComponent_TisPlayerPickups_t2873926454_m1245811139(__this, method) ((  PlayerPickups_t2873926454 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animation>()
#define Component_GetComponent_TisAnimation_t350396337_m2546983788(__this, method) ((  Animation_t350396337 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<PlayerMovement>()
#define Component_GetComponent_TisPlayerMovement_t3827129040_m3500385589(__this, method) ((  PlayerMovement_t3827129040 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CharacterController>()
#define Component_GetComponent_TisCharacterController_t2029520850_m3352851511(__this, method) ((  CharacterController_t2029520850 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m782999868(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.MeshRenderer>()
#define GameObject_GetComponentInChildren_TisMeshRenderer_t1217738301_m3118202586(__this, method) ((  MeshRenderer_t1217738301 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.ParticleSystem>()
#define GameObject_GetComponent_TisParticleSystem_t56787138_m2450916872(__this, method) ((  ParticleSystem_t56787138 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
#define GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910(__this, method) ((  MeshRenderer_t1217738301 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  Il2CppObject * Resources_Load_TisIl2CppObject_m2208345422_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define Resources_Load_TisIl2CppObject_m2208345422(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2208345422_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Resources::Load<PoqXert.MessageBox.MsgBox>(System.String)
#define Resources_Load_TisMsgBox_t1468720533_m1631435255(__this /* static, unused */, p0, method) ((  MsgBox_t1468720533 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2208345422_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.UI.Text>()
#define Component_GetComponentInChildren_TisText_t3286458198_m582470501(__this, method) ((  Text_t3286458198 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3133387403(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<PoqXert.MessageBox.MsgBox>(!!0)
#define Object_Instantiate_TisMsgBox_t1468720533_m747618583(__this /* static, unused */, p0, method) ((  MsgBox_t1468720533 * (*) (Il2CppObject * /* static, unused */, MsgBox_t1468720533 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t3354615620_m2140199269(__this, method) ((  Image_t3354615620 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Slider>()
#define GameObject_GetComponent_TisSlider_t1468074762_m2004927101(__this, method) ((  Slider_t1468074762 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void anims_controls::.ctor()
extern "C"  void anims_controls__ctor_m2134425640 (anims_controls_t1059809747 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void anims_controls::Awake()
extern const MethodInfo* Component_GetComponent_TisAnimator_t792326996_m4147395588_MethodInfo_var;
extern const uint32_t anims_controls_Awake_m2372030859_MetadataUsageId;
extern "C"  void anims_controls_Awake_m2372030859 (anims_controls_t1059809747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (anims_controls_Awake_m2372030859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = Component_GetComponent_TisAnimator_t792326996_m4147395588(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t792326996_m4147395588_MethodInfo_var);
		__this->set_anim_2(L_0);
		return;
	}
}
// System.Void anims_controls::Att()
extern Il2CppCodeGenString* _stringLiteral1971575400;
extern const uint32_t anims_controls_Att_m4257138919_MetadataUsageId;
extern "C"  void anims_controls_Att_m4257138919 (anims_controls_t1059809747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (anims_controls_Att_m4257138919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetBool_m2336836203(L_0, _stringLiteral1971575400, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void anims_controls::KickOver()
extern Il2CppCodeGenString* _stringLiteral2020876186;
extern const uint32_t anims_controls_KickOver_m573535318_MetadataUsageId;
extern "C"  void anims_controls_KickOver_m573535318 (anims_controls_t1059809747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (anims_controls_KickOver_m573535318_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetBool_m2336836203(L_0, _stringLiteral2020876186, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void anims_controls::DeathOver()
extern Il2CppCodeGenString* _stringLiteral2056863594;
extern const uint32_t anims_controls_DeathOver_m2811472430_MetadataUsageId;
extern "C"  void anims_controls_DeathOver_m2811472430 (anims_controls_t1059809747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (anims_controls_DeathOver_m2811472430_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetBool_m2336836203(L_0, _stringLiteral2056863594, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void anims_controls::DeathOver2()
extern Il2CppCodeGenString* _stringLiteral3633229320;
extern const uint32_t anims_controls_DeathOver2_m1256309030_MetadataUsageId;
extern "C"  void anims_controls_DeathOver2_m1256309030 (anims_controls_t1059809747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (anims_controls_DeathOver2_m1256309030_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetBool_m2336836203(L_0, _stringLiteral3633229320, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void anims_controls::HitOver()
extern Il2CppCodeGenString* _stringLiteral3676816645;
extern const uint32_t anims_controls_HitOver_m3690841421_MetadataUsageId;
extern "C"  void anims_controls_HitOver_m3690841421 (anims_controls_t1059809747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (anims_controls_HitOver_m3690841421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetBool_m2336836203(L_0, _stringLiteral3676816645, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void anims_controls::DamageOver()
extern Il2CppCodeGenString* _stringLiteral3629874489;
extern const uint32_t anims_controls_DamageOver_m1274120191_MetadataUsageId;
extern "C"  void anims_controls_DamageOver_m1274120191 (anims_controls_t1059809747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (anims_controls_DamageOver_m1274120191_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetBool_m2336836203(L_0, _stringLiteral3629874489, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BarrelColorManager::.ctor()
extern "C"  void BarrelColorManager__ctor_m99470571 (BarrelColorManager_t855775664 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = Color_get_blue_m3657252170(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_BodyColor_3(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BarrelColorManager::ChangeMaterial()
extern Il2CppClass* Material_t1886596500_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2079703534;
extern Il2CppCodeGenString* _stringLiteral2453140955;
extern const uint32_t BarrelColorManager_ChangeMaterial_m302238768_MetadataUsageId;
extern "C"  void BarrelColorManager_ChangeMaterial_m302238768 (BarrelColorManager_t855775664 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BarrelColorManager_ChangeMaterial_m302238768_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t1886596500 * L_0 = __this->get_ReferenceMaterial_6();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0047;
		}
	}
	{
		Material_t1886596500 * L_2 = __this->get_ReferenceMaterial_6();
		Material_t1886596500 * L_3 = (Material_t1886596500 *)il2cpp_codegen_object_new(Material_t1886596500_il2cpp_TypeInfo_var);
		Material__ctor_m2546967560(L_3, L_2, /*hidden argument*/NULL);
		__this->set_RuntimeMaterial_7(L_3);
		Material_t1886596500 * L_4 = __this->get_RuntimeMaterial_7();
		NullCheck(L_4);
		Object_set_name_m1123518500(L_4, _stringLiteral2079703534, /*hidden argument*/NULL);
		Renderer_t1092684080 * L_5 = __this->get_BodyRenderer_2();
		Material_t1886596500 * L_6 = __this->get_RuntimeMaterial_7();
		NullCheck(L_5);
		Renderer_set_sharedMaterial_m1064371045(L_5, L_6, /*hidden argument*/NULL);
		goto IL_0051;
	}

IL_0047:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2453140955, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
// System.Void BarrelColorManager::ChangeColor()
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1608919640;
extern const uint32_t BarrelColorManager_ChangeColor_m1633365212_MetadataUsageId;
extern "C"  void BarrelColorManager_ChangeColor_m1633365212 (BarrelColorManager_t855775664 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BarrelColorManager_ChangeColor_m1633365212_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t1886596500 * L_0 = __this->get_RuntimeMaterial_7();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		Material_t1886596500 * L_2 = __this->get_RuntimeMaterial_7();
		Color_t1588175760  L_3 = __this->get_BodyColor_3();
		NullCheck(L_2);
		Material_set_color_m3296857020(L_2, L_3, /*hidden argument*/NULL);
		goto IL_0030;
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1608919640, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void BarrelColorManager::ConfigReflectionProbe()
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3574489850;
extern Il2CppCodeGenString* _stringLiteral2178219264;
extern Il2CppCodeGenString* _stringLiteral2349484511;
extern const uint32_t BarrelColorManager_ConfigReflectionProbe_m148102124_MetadataUsageId;
extern "C"  void BarrelColorManager_ConfigReflectionProbe_m148102124 (BarrelColorManager_t855775664 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BarrelColorManager_ConfigReflectionProbe_m148102124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReflectionProbe_t936962674 * L_0 = __this->get_ReflectProbe_5();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_2 = __this->get_ReflectionMode_4();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3574489850, /*hidden argument*/NULL);
		ReflectionProbe_t936962674 * L_3 = __this->get_ReflectProbe_5();
		NullCheck(L_3);
		ReflectionProbe_set_mode_m2850534931(L_3, 1, /*hidden argument*/NULL);
		goto IL_004d;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2178219264, /*hidden argument*/NULL);
		ReflectionProbe_t936962674 * L_4 = __this->get_ReflectProbe_5();
		NullCheck(L_4);
		ReflectionProbe_set_mode_m2850534931(L_4, 2, /*hidden argument*/NULL);
	}

IL_004d:
	{
		goto IL_005c;
	}

IL_0052:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2349484511, /*hidden argument*/NULL);
	}

IL_005c:
	{
		return;
	}
}
// System.Void BarrelColorManager::Start()
extern "C"  void BarrelColorManager_Start_m3341575659 (BarrelColorManager_t855775664 * __this, const MethodInfo* method)
{
	{
		BarrelColorManager_ConfigReflectionProbe_m148102124(__this, /*hidden argument*/NULL);
		BarrelColorManager_ChangeMaterial_m302238768(__this, /*hidden argument*/NULL);
		BarrelColorManager_ChangeColor_m1633365212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BarrelColorManager::Update()
extern "C"  void BarrelColorManager_Update_m515482498 (BarrelColorManager_t855775664 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		BarrelColorManager_ChangeColor_m1633365212(__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		return;
	}
}
// System.Void CloseManager::.ctor()
extern "C"  void CloseManager__ctor_m3403790214 (CloseManager_t3949459125 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CloseManager::goHome()
extern Il2CppCodeGenString* _stringLiteral2255103;
extern const uint32_t CloseManager_goHome_m3794559429_MetadataUsageId;
extern "C"  void CloseManager_goHome_m3794559429 (CloseManager_t3949459125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CloseManager_goHome_m3794559429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral2255103, /*hidden argument*/NULL);
		return;
	}
}
// System.Void control_script::.ctor()
extern "C"  void control_script__ctor_m1109070158 (control_script_t3661633005 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void control_script::Awake()
extern const MethodInfo* Component_GetComponentInChildren_TisAnimator_t792326996_m1462315915_MethodInfo_var;
extern const uint32_t control_script_Awake_m1346675377_MetadataUsageId;
extern "C"  void control_script_Awake_m1346675377 (control_script_t3661633005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (control_script_Awake_m1346675377_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = Component_GetComponentInChildren_TisAnimator_t792326996_m1462315915(__this, /*hidden argument*/Component_GetComponentInChildren_TisAnimator_t792326996_m1462315915_MethodInfo_var);
		__this->set_anim_2(L_0);
		return;
	}
}
// System.Void control_script::Walk()
extern Il2CppCodeGenString* _stringLiteral3114954259;
extern Il2CppCodeGenString* _stringLiteral100478209;
extern Il2CppCodeGenString* _stringLiteral2864008825;
extern Il2CppCodeGenString* _stringLiteral1971575400;
extern Il2CppCodeGenString* _stringLiteral2020876186;
extern Il2CppCodeGenString* _stringLiteral2056863594;
extern Il2CppCodeGenString* _stringLiteral3633229320;
extern Il2CppCodeGenString* _stringLiteral3676816645;
extern const uint32_t control_script_Walk_m791838143_MetadataUsageId;
extern "C"  void control_script_Walk_m791838143 (control_script_t3661633005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (control_script_Walk_m791838143_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		bool L_1 = Animator_GetBool_m436748612(L_0, _stringLiteral3114954259, /*hidden argument*/NULL);
		__this->set_boolper_3(L_1);
		Animator_t792326996 * L_2 = __this->get_anim_2();
		bool L_3 = __this->get_boolper_3();
		NullCheck(L_2);
		Animator_SetBool_m2336836203(L_2, _stringLiteral3114954259, (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Animator_t792326996 * L_4 = __this->get_anim_2();
		NullCheck(L_4);
		Animator_SetBool_m2336836203(L_4, _stringLiteral100478209, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_5 = __this->get_anim_2();
		NullCheck(L_5);
		Animator_SetBool_m2336836203(L_5, _stringLiteral2864008825, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_6 = __this->get_anim_2();
		NullCheck(L_6);
		Animator_SetBool_m2336836203(L_6, _stringLiteral1971575400, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_7 = __this->get_anim_2();
		NullCheck(L_7);
		Animator_SetBool_m2336836203(L_7, _stringLiteral2020876186, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_8 = __this->get_anim_2();
		NullCheck(L_8);
		Animator_SetBool_m2336836203(L_8, _stringLiteral2056863594, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_9 = __this->get_anim_2();
		NullCheck(L_9);
		Animator_SetBool_m2336836203(L_9, _stringLiteral3633229320, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_10 = __this->get_anim_2();
		NullCheck(L_10);
		Animator_SetBool_m2336836203(L_10, _stringLiteral3676816645, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void control_script::Run()
extern Il2CppCodeGenString* _stringLiteral100478209;
extern Il2CppCodeGenString* _stringLiteral3114954259;
extern Il2CppCodeGenString* _stringLiteral2864008825;
extern Il2CppCodeGenString* _stringLiteral1971575400;
extern Il2CppCodeGenString* _stringLiteral2020876186;
extern Il2CppCodeGenString* _stringLiteral2056863594;
extern Il2CppCodeGenString* _stringLiteral3633229320;
extern Il2CppCodeGenString* _stringLiteral3676816645;
extern const uint32_t control_script_Run_m1545541879_MetadataUsageId;
extern "C"  void control_script_Run_m1545541879 (control_script_t3661633005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (control_script_Run_m1545541879_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		bool L_1 = Animator_GetBool_m436748612(L_0, _stringLiteral100478209, /*hidden argument*/NULL);
		__this->set_boolper2_4(L_1);
		Animator_t792326996 * L_2 = __this->get_anim_2();
		bool L_3 = __this->get_boolper2_4();
		NullCheck(L_2);
		Animator_SetBool_m2336836203(L_2, _stringLiteral100478209, (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Animator_t792326996 * L_4 = __this->get_anim_2();
		NullCheck(L_4);
		Animator_SetBool_m2336836203(L_4, _stringLiteral3114954259, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_5 = __this->get_anim_2();
		NullCheck(L_5);
		Animator_SetBool_m2336836203(L_5, _stringLiteral2864008825, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_6 = __this->get_anim_2();
		NullCheck(L_6);
		Animator_SetBool_m2336836203(L_6, _stringLiteral1971575400, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_7 = __this->get_anim_2();
		NullCheck(L_7);
		Animator_SetBool_m2336836203(L_7, _stringLiteral2020876186, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_8 = __this->get_anim_2();
		NullCheck(L_8);
		Animator_SetBool_m2336836203(L_8, _stringLiteral2056863594, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_9 = __this->get_anim_2();
		NullCheck(L_9);
		Animator_SetBool_m2336836203(L_9, _stringLiteral3633229320, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_10 = __this->get_anim_2();
		NullCheck(L_10);
		Animator_SetBool_m2336836203(L_10, _stringLiteral3676816645, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void control_script::OtherIdle()
extern Il2CppCodeGenString* _stringLiteral2864008825;
extern Il2CppCodeGenString* _stringLiteral3114954259;
extern Il2CppCodeGenString* _stringLiteral100478209;
extern Il2CppCodeGenString* _stringLiteral1971575400;
extern Il2CppCodeGenString* _stringLiteral2020876186;
extern Il2CppCodeGenString* _stringLiteral2056863594;
extern Il2CppCodeGenString* _stringLiteral3633229320;
extern Il2CppCodeGenString* _stringLiteral3676816645;
extern const uint32_t control_script_OtherIdle_m1418871536_MetadataUsageId;
extern "C"  void control_script_OtherIdle_m1418871536 (control_script_t3661633005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (control_script_OtherIdle_m1418871536_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		bool L_1 = Animator_GetBool_m436748612(L_0, _stringLiteral2864008825, /*hidden argument*/NULL);
		__this->set_boolper3_5(L_1);
		Animator_t792326996 * L_2 = __this->get_anim_2();
		bool L_3 = __this->get_boolper3_5();
		NullCheck(L_2);
		Animator_SetBool_m2336836203(L_2, _stringLiteral2864008825, (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Animator_t792326996 * L_4 = __this->get_anim_2();
		NullCheck(L_4);
		Animator_SetBool_m2336836203(L_4, _stringLiteral3114954259, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_5 = __this->get_anim_2();
		NullCheck(L_5);
		Animator_SetBool_m2336836203(L_5, _stringLiteral100478209, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_6 = __this->get_anim_2();
		NullCheck(L_6);
		Animator_SetBool_m2336836203(L_6, _stringLiteral1971575400, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_7 = __this->get_anim_2();
		NullCheck(L_7);
		Animator_SetBool_m2336836203(L_7, _stringLiteral2020876186, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_8 = __this->get_anim_2();
		NullCheck(L_8);
		Animator_SetBool_m2336836203(L_8, _stringLiteral2056863594, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_9 = __this->get_anim_2();
		NullCheck(L_9);
		Animator_SetBool_m2336836203(L_9, _stringLiteral3633229320, (bool)0, /*hidden argument*/NULL);
		Animator_t792326996 * L_10 = __this->get_anim_2();
		NullCheck(L_10);
		Animator_SetBool_m2336836203(L_10, _stringLiteral3676816645, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void control_script::Attack()
extern Il2CppCodeGenString* _stringLiteral1971575400;
extern const uint32_t control_script_Attack_m964443454_MetadataUsageId;
extern "C"  void control_script_Attack_m964443454 (control_script_t3661633005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (control_script_Attack_m964443454_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetBool_m2336836203(L_0, _stringLiteral1971575400, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void control_script::LowKick()
extern Il2CppCodeGenString* _stringLiteral2020876186;
extern const uint32_t control_script_LowKick_m3339733222_MetadataUsageId;
extern "C"  void control_script_LowKick_m3339733222 (control_script_t3661633005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (control_script_LowKick_m3339733222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetBool_m2336836203(L_0, _stringLiteral2020876186, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void control_script::Death()
extern Il2CppCodeGenString* _stringLiteral2056863594;
extern const uint32_t control_script_Death_m3494132704_MetadataUsageId;
extern "C"  void control_script_Death_m3494132704 (control_script_t3661633005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (control_script_Death_m3494132704_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetBool_m2336836203(L_0, _stringLiteral2056863594, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void control_script::Death2()
extern Il2CppCodeGenString* _stringLiteral3633229320;
extern const uint32_t control_script_Death2_m943941044_MetadataUsageId;
extern "C"  void control_script_Death2_m943941044 (control_script_t3661633005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (control_script_Death2_m943941044_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetBool_m2336836203(L_0, _stringLiteral3633229320, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void control_script::Strike()
extern Il2CppCodeGenString* _stringLiteral3676816645;
extern const uint32_t control_script_Strike_m2220620840_MetadataUsageId;
extern "C"  void control_script_Strike_m2220620840 (control_script_t3661633005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (control_script_Strike_m2220620840_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetBool_m2336836203(L_0, _stringLiteral3676816645, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void control_script::Damage()
extern Il2CppCodeGenString* _stringLiteral3629874489;
extern const uint32_t control_script_Damage_m2014915749_MetadataUsageId;
extern "C"  void control_script_Damage_m2014915749 (control_script_t3661633005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (control_script_Damage_m2014915749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t792326996 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetBool_m2336836203(L_0, _stringLiteral3629874489, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void control_script::Update()
extern "C"  void control_script_Update_m1748298623 (control_script_t3661633005 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DatabaseExample::.ctor()
extern Il2CppClass* List_1_t1765447871_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2569629;
extern Il2CppCodeGenString* _stringLiteral209954730;
extern const uint32_t DatabaseExample__ctor_m4045787388_MetadataUsageId;
extern "C"  void DatabaseExample__ctor_m4045787388 (DatabaseExample_t2874326863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseExample__ctor_m4045787388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SQLiteDB_t557922535 * L_0 = SQLiteDB_get_Instance_m2094062401(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_db_2(L_0);
		List_1_t1765447871 * L_1 = (List_1_t1765447871 *)il2cpp_codegen_object_new(List_1_t1765447871_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_1, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_allIDs_3(L_1);
		List_1_t1765447871 * L_2 = (List_1_t1765447871 *)il2cpp_codegen_object_new(List_1_t1765447871_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_2, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_allNames_4(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__id_5(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__name_6(L_4);
		Vector2_t3525329788  L_5 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scrollPos_7(L_5);
		__this->set_btnName_SaveUpdate_8(_stringLiteral2569629);
		__this->set_btnName_CreateDeleteTable_9(_stringLiteral209954730);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DatabaseExample::OnEnable()
extern Il2CppClass* OnError_t314785609_il2cpp_TypeInfo_var;
extern const MethodInfo* DatabaseExample_OnError_m320034527_MethodInfo_var;
extern const uint32_t DatabaseExample_OnEnable_m456426762_MetadataUsageId;
extern "C"  void DatabaseExample_OnEnable_m456426762 (DatabaseExample_t2874326863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseExample_OnEnable_m456426762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)DatabaseExample_OnError_m320034527_MethodInfo_var);
		OnError_t314785609 * L_1 = (OnError_t314785609 *)il2cpp_codegen_object_new(OnError_t314785609_il2cpp_TypeInfo_var);
		OnError__ctor_m1086636545(L_1, __this, L_0, /*hidden argument*/NULL);
		SQLiteEventListener_add_onError_m1384741999(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DatabaseExample::OnDisable()
extern Il2CppClass* OnError_t314785609_il2cpp_TypeInfo_var;
extern const MethodInfo* DatabaseExample_OnError_m320034527_MethodInfo_var;
extern const uint32_t DatabaseExample_OnDisable_m1705264995_MetadataUsageId;
extern "C"  void DatabaseExample_OnDisable_m1705264995 (DatabaseExample_t2874326863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseExample_OnDisable_m1705264995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)DatabaseExample_OnError_m320034527_MethodInfo_var);
		OnError_t314785609 * L_1 = (OnError_t314785609 *)il2cpp_codegen_object_new(OnError_t314785609_il2cpp_TypeInfo_var);
		OnError__ctor_m1086636545(L_1, __this, L_0, /*hidden argument*/NULL);
		SQLiteEventListener_remove_onError_m1527352418(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DatabaseExample::OnError(System.String)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t DatabaseExample_OnError_m320034527_MetadataUsageId;
extern "C"  void DatabaseExample_OnError_m320034527 (DatabaseExample_t2874326863 * __this, String_t* ___err0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseExample_OnError_m320034527_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___err0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DatabaseExample::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3123515454;
extern const uint32_t DatabaseExample_Start_m2992925180_MetadataUsageId;
extern "C"  void DatabaseExample_Start_m2992925180 (DatabaseExample_t2874326863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseExample_Start_m2992925180_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SQLiteDB_t557922535 * L_0 = __this->get_db_2();
		String_t* L_1 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SQLiteDB_set_DBLocation_m3743704717(L_0, L_1, /*hidden argument*/NULL);
		SQLiteDB_t557922535 * L_2 = __this->get_db_2();
		NullCheck(L_2);
		String_t* L_3 = SQLiteDB_get_DBLocation_m603624094(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3123515454, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DatabaseExample::OnGUI()
extern Il2CppClass* GUI_t1522956648_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2481286909;
extern Il2CppCodeGenString* _stringLiteral2331;
extern Il2CppCodeGenString* _stringLiteral2420395;
extern Il2CppCodeGenString* _stringLiteral2569629;
extern Il2CppCodeGenString* _stringLiteral2539988201;
extern Il2CppCodeGenString* _stringLiteral2155050;
extern Il2CppCodeGenString* _stringLiteral2043376075;
extern Il2CppCodeGenString* _stringLiteral2887192571;
extern Il2CppCodeGenString* _stringLiteral82025960;
extern Il2CppCodeGenString* _stringLiteral2209090841;
extern Il2CppCodeGenString* _stringLiteral209954730;
extern Il2CppCodeGenString* _stringLiteral1645197043;
extern const uint32_t DatabaseExample_OnGUI_m3541186038_MetadataUsageId;
extern "C"  void DatabaseExample_OnGUI_m3541186038 (DatabaseExample_t2874326863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseExample_OnGUI_m3541186038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t1525428817  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Rect__ctor_m3291325233((&V_0), (0.0f), (0.0f), (100.0f), (20.0f), /*hidden argument*/NULL);
		SQLiteDB_t557922535 * L_0 = __this->get_db_2();
		NullCheck(L_0);
		bool L_1 = SQLiteDB_get_Exists_m3278641920(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_2 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t1525428817  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Rect__ctor_m3291325233(&L_4, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_2/(int32_t)2))-(int32_t)((int32_t)125)))))), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_3/(int32_t)2))-(int32_t)((int32_t)40)))))), (250.0f), (80.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_5 = GUI_Button_m885093907(NULL /*static, unused*/, L_4, _stringLiteral2481286909, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0065;
		}
	}
	{
		DatabaseExample_CreateDB_m1707947330(__this, /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}

IL_0066:
	{
		bool L_6 = __this->get_isTableCreated_10();
		if (!L_6)
		{
			goto IL_0182;
		}
	}
	{
		Rect_t1525428817  L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_Label_m1483857617(NULL /*static, unused*/, L_7, _stringLiteral2331, /*hidden argument*/NULL);
		Rect_set_x_m577970569((&V_0), (150.0f), /*hidden argument*/NULL);
		Rect_t1525428817  L_8 = V_0;
		GUI_Label_m1483857617(NULL /*static, unused*/, L_8, _stringLiteral2420395, /*hidden argument*/NULL);
		Rect_set_y_m67436392((&V_0), (30.0f), /*hidden argument*/NULL);
		Rect_set_x_m577970569((&V_0), (0.0f), /*hidden argument*/NULL);
		String_t* L_9 = __this->get_btnName_SaveUpdate_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_9, _stringLiteral2569629, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00d7;
		}
	}
	{
		Rect_t1525428817  L_11 = V_0;
		String_t* L_12 = __this->get__id_5();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		String_t* L_13 = GUI_TextField_m3177770189(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		__this->set__id_5(L_13);
		goto IL_00e4;
	}

IL_00d7:
	{
		Rect_t1525428817  L_14 = V_0;
		String_t* L_15 = __this->get__id_5();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_TextField_m3177770189(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
	}

IL_00e4:
	{
		Rect_set_x_m577970569((&V_0), (150.0f), /*hidden argument*/NULL);
		Rect_t1525428817  L_16 = V_0;
		String_t* L_17 = __this->get__name_6();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		String_t* L_18 = GUI_TextField_m3177770189(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		__this->set__name_6(L_18);
		float L_19 = Rect_get_x_m982385354((&V_0), /*hidden argument*/NULL);
		float L_20 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		Rect_set_x_m577970569((&V_0), ((float)((float)((float)((float)L_19+(float)L_20))+(float)(10.0f))), /*hidden argument*/NULL);
		Rect_t1525428817  L_21 = V_0;
		String_t* L_22 = __this->get_btnName_SaveUpdate_8();
		bool L_23 = GUI_Button_m885093907(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0182;
		}
	}
	{
		String_t* L_24 = __this->get_btnName_SaveUpdate_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_24, _stringLiteral2569629, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_015b;
		}
	}
	{
		String_t* L_26 = __this->get__id_5();
		String_t* L_27 = __this->get__name_6();
		DatabaseExample_AddRow_m2730362077(__this, L_26, L_27, /*hidden argument*/NULL);
		goto IL_0182;
	}

IL_015b:
	{
		String_t* L_28 = __this->get_btnName_SaveUpdate_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_29 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_28, _stringLiteral2539988201, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_0182;
		}
	}
	{
		String_t* L_30 = __this->get__id_5();
		String_t* L_31 = __this->get__name_6();
		DatabaseExample_UpdateRow_m2923826995(__this, L_30, L_31, /*hidden argument*/NULL);
	}

IL_0182:
	{
		Rect_set_x_m577970569((&V_0), (0.0f), /*hidden argument*/NULL);
		Rect_set_y_m67436392((&V_0), (60.0f), /*hidden argument*/NULL);
		int32_t L_32 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_width_m3771513595((&V_0), (((float)((float)L_32))), /*hidden argument*/NULL);
		int32_t L_33 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_height_m3398820332((&V_0), (((float)((float)((int32_t)((int32_t)L_33-(int32_t)((int32_t)120)))))), /*hidden argument*/NULL);
		Rect_t1525428817  L_34 = V_0;
		Vector2_t3525329788  L_35 = __this->get_scrollPos_7();
		float L_36 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		List_1_t1765447871 * L_37 = __this->get_allIDs_3();
		NullCheck(L_37);
		int32_t L_38 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_37);
		Rect_t1525428817  L_39;
		memset(&L_39, 0, sizeof(L_39));
		Rect__ctor_m3291325233(&L_39, (0.0f), (0.0f), ((float)((float)L_36-(float)(30.0f))), (((float)((float)((int32_t)((int32_t)L_38*(int32_t)((int32_t)30)))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		Vector2_t3525329788  L_40 = GUI_BeginScrollView_m2683764896(NULL /*static, unused*/, L_34, L_35, L_39, /*hidden argument*/NULL);
		__this->set_scrollPos_7(L_40);
		Rect_set_width_m3771513595((&V_0), (125.0f), /*hidden argument*/NULL);
		Rect_set_height_m3398820332((&V_0), (25.0f), /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_02ed;
	}

IL_0213:
	{
		Rect_set_x_m577970569((&V_0), (0.0f), /*hidden argument*/NULL);
		int32_t L_41 = V_1;
		Rect_set_y_m67436392((&V_0), (((float)((float)((int32_t)((int32_t)L_41*(int32_t)((int32_t)25)))))), /*hidden argument*/NULL);
		Rect_t1525428817  L_42 = V_0;
		List_1_t1765447871 * L_43 = __this->get_allIDs_3();
		int32_t L_44 = V_1;
		NullCheck(L_43);
		String_t* L_45 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_43, L_44);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_Label_m1483857617(NULL /*static, unused*/, L_42, L_45, /*hidden argument*/NULL);
		Rect_t1525428817 * L_46 = (&V_0);
		float L_47 = Rect_get_x_m982385354(L_46, /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_46, ((float)((float)L_47+(float)(130.0f))), /*hidden argument*/NULL);
		Rect_t1525428817  L_48 = V_0;
		List_1_t1765447871 * L_49 = __this->get_allNames_4();
		int32_t L_50 = V_1;
		NullCheck(L_49);
		String_t* L_51 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_49, L_50);
		GUI_Label_m1483857617(NULL /*static, unused*/, L_48, L_51, /*hidden argument*/NULL);
		Rect_t1525428817 * L_52 = (&V_0);
		float L_53 = Rect_get_x_m982385354(L_52, /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_52, ((float)((float)L_53+(float)(130.0f))), /*hidden argument*/NULL);
		Rect_t1525428817  L_54 = V_0;
		bool L_55 = GUI_Button_m885093907(NULL /*static, unused*/, L_54, _stringLiteral2155050, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_02b4;
		}
	}
	{
		List_1_t1765447871 * L_56 = __this->get_allIDs_3();
		int32_t L_57 = V_1;
		NullCheck(L_56);
		String_t* L_58 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_56, L_57);
		__this->set__id_5(L_58);
		List_1_t1765447871 * L_59 = __this->get_allNames_4();
		int32_t L_60 = V_1;
		NullCheck(L_59);
		String_t* L_61 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_59, L_60);
		__this->set__name_6(L_61);
		__this->set_btnName_SaveUpdate_8(_stringLiteral2539988201);
	}

IL_02b4:
	{
		Rect_t1525428817 * L_62 = (&V_0);
		float L_63 = Rect_get_x_m982385354(L_62, /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_62, ((float)((float)L_63+(float)(130.0f))), /*hidden argument*/NULL);
		Rect_t1525428817  L_64 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_65 = GUI_Button_m885093907(NULL /*static, unused*/, L_64, _stringLiteral2043376075, /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_02e9;
		}
	}
	{
		List_1_t1765447871 * L_66 = __this->get_allIDs_3();
		int32_t L_67 = V_1;
		NullCheck(L_66);
		String_t* L_68 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_66, L_67);
		DatabaseExample_DeleteRow_m2634804121(__this, L_68, /*hidden argument*/NULL);
	}

IL_02e9:
	{
		int32_t L_69 = V_1;
		V_1 = ((int32_t)((int32_t)L_69+(int32_t)1));
	}

IL_02ed:
	{
		int32_t L_70 = V_1;
		List_1_t1765447871 * L_71 = __this->get_allIDs_3();
		NullCheck(L_71);
		int32_t L_72 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_71);
		if ((((int32_t)L_70) < ((int32_t)L_72)))
		{
			goto IL_0213;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_EndScrollView_m3280556969(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_73 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_y_m67436392((&V_0), (((float)((float)((int32_t)((int32_t)L_73-(int32_t)((int32_t)25)))))), /*hidden argument*/NULL);
		Rect_set_x_m577970569((&V_0), (0.0f), /*hidden argument*/NULL);
		Rect_set_width_m3771513595((&V_0), (100.0f), /*hidden argument*/NULL);
		Rect_set_height_m3398820332((&V_0), (20.0f), /*hidden argument*/NULL);
		bool L_74 = __this->get_isTableCreated_10();
		GUI_set_enabled_m262604887(NULL /*static, unused*/, L_74, /*hidden argument*/NULL);
		Rect_t1525428817  L_75 = V_0;
		bool L_76 = GUI_Button_m885093907(NULL /*static, unused*/, L_75, _stringLiteral2887192571, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_0369;
		}
	}
	{
		SQLiteDB_t557922535 * L_77 = __this->get_db_2();
		NullCheck(L_77);
		SQLiteDB_ClearTable_m4150127616(L_77, _stringLiteral82025960, /*hidden argument*/NULL);
		DatabaseExample_Refresh_m139107317(__this, /*hidden argument*/NULL);
	}

IL_0369:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_set_enabled_m262604887(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		Rect_t1525428817 * L_78 = (&V_0);
		float L_79 = Rect_get_x_m982385354(L_78, /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_78, ((float)((float)L_79+(float)(110.0f))), /*hidden argument*/NULL);
		Rect_t1525428817  L_80 = V_0;
		String_t* L_81 = __this->get_btnName_CreateDeleteTable_9();
		bool L_82 = GUI_Button_m885093907(NULL /*static, unused*/, L_80, L_81, /*hidden argument*/NULL);
		if (!L_82)
		{
			goto IL_041c;
		}
	}
	{
		String_t* L_83 = __this->get_btnName_CreateDeleteTable_9();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_84 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_83, _stringLiteral2209090841, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_03ea;
		}
	}
	{
		SQLiteDB_t557922535 * L_85 = __this->get_db_2();
		NullCheck(L_85);
		bool L_86 = SQLiteDB_DeleteTable_m3319765176(L_85, _stringLiteral82025960, /*hidden argument*/NULL);
		if (!L_86)
		{
			goto IL_03ea;
		}
	}
	{
		List_1_t1765447871 * L_87 = __this->get_allIDs_3();
		NullCheck(L_87);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.String>::Clear() */, L_87);
		List_1_t1765447871 * L_88 = __this->get_allNames_4();
		NullCheck(L_88);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.String>::Clear() */, L_88);
		__this->set_btnName_CreateDeleteTable_9(_stringLiteral209954730);
		__this->set_isTableCreated_10((bool)0);
		goto IL_041c;
	}

IL_03ea:
	{
		String_t* L_89 = __this->get_btnName_CreateDeleteTable_9();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_90 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_89, _stringLiteral209954730, /*hidden argument*/NULL);
		if (!L_90)
		{
			goto IL_041c;
		}
	}
	{
		bool L_91 = DatabaseExample_CreateTable_m665722560(__this, /*hidden argument*/NULL);
		if (!L_91)
		{
			goto IL_041c;
		}
	}
	{
		__this->set_btnName_CreateDeleteTable_9(_stringLiteral2209090841);
		__this->set_isTableCreated_10((bool)1);
	}

IL_041c:
	{
		Rect_t1525428817 * L_92 = (&V_0);
		float L_93 = Rect_get_x_m982385354(L_92, /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_92, ((float)((float)L_93+(float)(110.0f))), /*hidden argument*/NULL);
		Rect_t1525428817  L_94 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_95 = GUI_Button_m885093907(NULL /*static, unused*/, L_94, _stringLiteral1645197043, /*hidden argument*/NULL);
		if (!L_95)
		{
			goto IL_0477;
		}
	}
	{
		SQLiteDB_t557922535 * L_96 = __this->get_db_2();
		NullCheck(L_96);
		bool L_97 = SQLiteDB_DeleteDatabase_m2858673313(L_96, /*hidden argument*/NULL);
		if (!L_97)
		{
			goto IL_0477;
		}
	}
	{
		List_1_t1765447871 * L_98 = __this->get_allIDs_3();
		NullCheck(L_98);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.String>::Clear() */, L_98);
		List_1_t1765447871 * L_99 = __this->get_allNames_4();
		NullCheck(L_99);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.String>::Clear() */, L_99);
		__this->set_btnName_CreateDeleteTable_9(_stringLiteral209954730);
		__this->set_isTableCreated_10((bool)0);
	}

IL_0477:
	{
		return;
	}
}
// System.Void DatabaseExample::CreateDB()
extern Il2CppCodeGenString* _stringLiteral4101224549;
extern const uint32_t DatabaseExample_CreateDB_m1707947330_MetadataUsageId;
extern "C"  void DatabaseExample_CreateDB_m1707947330 (DatabaseExample_t2874326863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseExample_CreateDB_m1707947330_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SQLiteDB_t557922535 * L_0 = __this->get_db_2();
		NullCheck(L_0);
		bool L_1 = SQLiteDB_get_Exists_m3278641920(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		SQLiteDB_t557922535 * L_2 = __this->get_db_2();
		NullCheck(L_2);
		SQLiteDB_ConnectToDefaultDatabase_m3847386093(L_2, _stringLiteral4101224549, (bool)0, /*hidden argument*/NULL);
		goto IL_0039;
	}

IL_0027:
	{
		SQLiteDB_t557922535 * L_3 = __this->get_db_2();
		NullCheck(L_3);
		SQLiteDB_ConnectToDefaultDatabase_m3847386093(L_3, _stringLiteral4101224549, (bool)1, /*hidden argument*/NULL);
	}

IL_0039:
	{
		DatabaseExample_Refresh_m139107317(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DatabaseExample::CreateTable()
extern Il2CppClass* DBSchema_t4247391996_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral82025960;
extern Il2CppCodeGenString* _stringLiteral2363;
extern Il2CppCodeGenString* _stringLiteral2420395;
extern const uint32_t DatabaseExample_CreateTable_m665722560_MetadataUsageId;
extern "C"  bool DatabaseExample_CreateTable_m665722560 (DatabaseExample_t2874326863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseExample_CreateTable_m665722560_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DBSchema_t4247391996 * V_0 = NULL;
	{
		SQLiteDB_t557922535 * L_0 = __this->get_db_2();
		NullCheck(L_0);
		bool L_1 = SQLiteDB_get_Exists_m3278641920(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0049;
		}
	}
	{
		DBSchema_t4247391996 * L_2 = (DBSchema_t4247391996 *)il2cpp_codegen_object_new(DBSchema_t4247391996_il2cpp_TypeInfo_var);
		DBSchema__ctor_m2925071800(L_2, _stringLiteral82025960, /*hidden argument*/NULL);
		V_0 = L_2;
		DBSchema_t4247391996 * L_3 = V_0;
		NullCheck(L_3);
		DBSchema_AddField_m776412250(L_3, _stringLiteral2363, 1, 0, (bool)0, (bool)1, (bool)1, /*hidden argument*/NULL);
		DBSchema_t4247391996 * L_4 = V_0;
		NullCheck(L_4);
		DBSchema_AddField_m776412250(L_4, _stringLiteral2420395, 4, ((int32_t)100), (bool)0, (bool)0, (bool)0, /*hidden argument*/NULL);
		SQLiteDB_t557922535 * L_5 = __this->get_db_2();
		DBSchema_t4247391996 * L_6 = V_0;
		NullCheck(L_5);
		bool L_7 = SQLiteDB_CreateTable_m3808471283(L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0049:
	{
		return (bool)0;
	}
}
// System.Void DatabaseExample::AddRow(System.String,System.String)
extern Il2CppClass* List_1_t722451806_il2cpp_TypeInfo_var;
extern Il2CppClass* DB_DataPair_t4220460133_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2069798182_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2363;
extern Il2CppCodeGenString* _stringLiteral2420395;
extern Il2CppCodeGenString* _stringLiteral82025960;
extern Il2CppCodeGenString* _stringLiteral4161151226;
extern const uint32_t DatabaseExample_AddRow_m2730362077_MetadataUsageId;
extern "C"  void DatabaseExample_AddRow_m2730362077 (DatabaseExample_t2874326863 * __this, String_t* ___id0, String_t* ___name1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseExample_AddRow_m2730362077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t722451806 * V_0 = NULL;
	DB_DataPair_t4220460133  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		List_1_t722451806 * L_0 = (List_1_t722451806 *)il2cpp_codegen_object_new(List_1_t722451806_il2cpp_TypeInfo_var);
		List_1__ctor_m2069798182(L_0, /*hidden argument*/List_1__ctor_m2069798182_MethodInfo_var);
		V_0 = L_0;
		Initobj (DB_DataPair_t4220460133_il2cpp_TypeInfo_var, (&V_1));
		(&V_1)->set_fieldName_0(_stringLiteral2363);
		String_t* L_1 = ___id0;
		(&V_1)->set_value_1(L_1);
		List_1_t722451806 * L_2 = V_0;
		DB_DataPair_t4220460133  L_3 = V_1;
		NullCheck(L_2);
		VirtActionInvoker1< DB_DataPair_t4220460133  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Add(!0) */, L_2, L_3);
		(&V_1)->set_fieldName_0(_stringLiteral2420395);
		String_t* L_4 = ___name1;
		(&V_1)->set_value_1(L_4);
		List_1_t722451806 * L_5 = V_0;
		DB_DataPair_t4220460133  L_6 = V_1;
		NullCheck(L_5);
		VirtActionInvoker1< DB_DataPair_t4220460133  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Add(!0) */, L_5, L_6);
		SQLiteDB_t557922535 * L_7 = __this->get_db_2();
		List_1_t722451806 * L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = SQLiteDB_Insert_m4013404553(L_7, _stringLiteral82025960, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0083;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral4161151226, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__name_6(L_11);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__id_5(L_12);
		DatabaseExample_Refresh_m139107317(__this, /*hidden argument*/NULL);
	}

IL_0083:
	{
		return;
	}
}
// System.Void DatabaseExample::UpdateRow(System.String,System.String)
extern Il2CppClass* List_1_t722451806_il2cpp_TypeInfo_var;
extern Il2CppClass* DB_DataPair_t4220460133_il2cpp_TypeInfo_var;
extern Il2CppClass* DB_ConditionPair_t1745523828_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2069798182_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2420395;
extern Il2CppCodeGenString* _stringLiteral2363;
extern Il2CppCodeGenString* _stringLiteral82025960;
extern Il2CppCodeGenString* _stringLiteral1065078165;
extern const uint32_t DatabaseExample_UpdateRow_m2923826995_MetadataUsageId;
extern "C"  void DatabaseExample_UpdateRow_m2923826995 (DatabaseExample_t2874326863 * __this, String_t* ___id0, String_t* ___name1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseExample_UpdateRow_m2923826995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t722451806 * V_0 = NULL;
	DB_DataPair_t4220460133  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DB_ConditionPair_t1745523828  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t V_3 = 0;
	{
		List_1_t722451806 * L_0 = (List_1_t722451806 *)il2cpp_codegen_object_new(List_1_t722451806_il2cpp_TypeInfo_var);
		List_1__ctor_m2069798182(L_0, /*hidden argument*/List_1__ctor_m2069798182_MethodInfo_var);
		V_0 = L_0;
		Initobj (DB_DataPair_t4220460133_il2cpp_TypeInfo_var, (&V_1));
		(&V_1)->set_fieldName_0(_stringLiteral2420395);
		String_t* L_1 = ___name1;
		(&V_1)->set_value_1(L_1);
		List_1_t722451806 * L_2 = V_0;
		DB_DataPair_t4220460133  L_3 = V_1;
		NullCheck(L_2);
		VirtActionInvoker1< DB_DataPair_t4220460133  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Add(!0) */, L_2, L_3);
		Initobj (DB_ConditionPair_t1745523828_il2cpp_TypeInfo_var, (&V_2));
		(&V_2)->set_fieldName_0(_stringLiteral2363);
		String_t* L_4 = ___id0;
		(&V_2)->set_value_1(L_4);
		(&V_2)->set_condition_2(3);
		SQLiteDB_t557922535 * L_5 = __this->get_db_2();
		List_1_t722451806 * L_6 = V_0;
		DB_ConditionPair_t1745523828  L_7 = V_2;
		NullCheck(L_5);
		int32_t L_8 = SQLiteDB_Update_m4162022779(L_5, _stringLiteral82025960, L_6, L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		int32_t L_9 = V_3;
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0098;
		}
	}
	{
		int32_t L_10 = V_3;
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m389863537(NULL /*static, unused*/, L_12, _stringLiteral1065078165, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__name_6(L_14);
		String_t* L_15 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set__id_5(L_15);
		DatabaseExample_Refresh_m139107317(__this, /*hidden argument*/NULL);
	}

IL_0098:
	{
		return;
	}
}
// System.Void DatabaseExample::DeleteRow(System.String)
extern Il2CppClass* DB_ConditionPair_t1745523828_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2363;
extern Il2CppCodeGenString* _stringLiteral82025960;
extern Il2CppCodeGenString* _stringLiteral562194935;
extern const uint32_t DatabaseExample_DeleteRow_m2634804121_MetadataUsageId;
extern "C"  void DatabaseExample_DeleteRow_m2634804121 (DatabaseExample_t2874326863 * __this, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseExample_DeleteRow_m2634804121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DB_ConditionPair_t1745523828  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Initobj (DB_ConditionPair_t1745523828_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_fieldName_0(_stringLiteral2363);
		String_t* L_0 = ___id0;
		(&V_0)->set_value_1(L_0);
		(&V_0)->set_condition_2(3);
		SQLiteDB_t557922535 * L_1 = __this->get_db_2();
		DB_ConditionPair_t1745523828  L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = SQLiteDB_DeleteRow_m1399588258(L_1, _stringLiteral82025960, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_5 = V_1;
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m389863537(NULL /*static, unused*/, L_7, _stringLiteral562194935, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		DatabaseExample_Refresh_m139107317(__this, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void DatabaseExample::Refresh()
extern Il2CppCodeGenString* _stringLiteral82025960;
extern Il2CppCodeGenString* _stringLiteral2363;
extern Il2CppCodeGenString* _stringLiteral2420395;
extern Il2CppCodeGenString* _stringLiteral2569629;
extern const uint32_t DatabaseExample_Refresh_m139107317_MetadataUsageId;
extern "C"  void DatabaseExample_Refresh_m139107317 (DatabaseExample_t2874326863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseExample_Refresh_m139107317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DBReader_t4220400158 * V_0 = NULL;
	{
		List_1_t1765447871 * L_0 = __this->get_allIDs_3();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.String>::Clear() */, L_0);
		List_1_t1765447871 * L_1 = __this->get_allNames_4();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.String>::Clear() */, L_1);
		SQLiteDB_t557922535 * L_2 = __this->get_db_2();
		NullCheck(L_2);
		DBReader_t4220400158 * L_3 = SQLiteDB_GetAllData_m261559315(L_2, _stringLiteral82025960, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_005f;
	}

IL_002c:
	{
		List_1_t1765447871 * L_4 = __this->get_allIDs_3();
		DBReader_t4220400158 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = DBReader_GetStringValue_m1373122399(L_5, _stringLiteral2363, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_4, L_6);
		List_1_t1765447871 * L_7 = __this->get_allNames_4();
		DBReader_t4220400158 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = DBReader_GetStringValue_m1373122399(L_8, _stringLiteral2420395, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_7, L_9);
		__this->set_isTableCreated_10((bool)1);
	}

IL_005f:
	{
		DBReader_t4220400158 * L_10 = V_0;
		if (!L_10)
		{
			goto IL_0070;
		}
	}
	{
		DBReader_t4220400158 * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = DBReader_Read_m2141930664(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_002c;
		}
	}

IL_0070:
	{
		__this->set_btnName_SaveUpdate_8(_stringLiteral2569629);
		return;
	}
}
// System.Void DatabaseExample::OnApplicationQuit()
extern "C"  void DatabaseExample_OnApplicationQuit_m2364580986 (DatabaseExample_t2874326863 * __this, const MethodInfo* method)
{
	{
		SQLiteDB_t557922535 * L_0 = __this->get_db_2();
		NullCheck(L_0);
		SQLiteDB_Dispose_m2829228156(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DatabaseManager::.ctor()
extern "C"  void DatabaseManager__ctor_m1163406873 (DatabaseManager_t737590674 * __this, const MethodInfo* method)
{
	{
		SQLiteDB_t557922535 * L_0 = SQLiteDB_get_Instance_m2094062401(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_db_2(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DatabaseManager::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3123515454;
extern const uint32_t DatabaseManager_Start_m110544665_MetadataUsageId;
extern "C"  void DatabaseManager_Start_m110544665 (DatabaseManager_t737590674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseManager_Start_m110544665_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SQLiteDB_t557922535 * L_0 = __this->get_db_2();
		String_t* L_1 = Application_get_persistentDataPath_m2554537447(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SQLiteDB_set_DBLocation_m3743704717(L_0, L_1, /*hidden argument*/NULL);
		SQLiteDB_t557922535 * L_2 = __this->get_db_2();
		NullCheck(L_2);
		String_t* L_3 = SQLiteDB_get_DBLocation_m603624094(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3123515454, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		DatabaseManager_CreateDB_m1991173893(__this, /*hidden argument*/NULL);
		DatabaseManager_GetScoresData_m1846108248(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DatabaseManager::OnEnable()
extern Il2CppClass* OnError_t314785609_il2cpp_TypeInfo_var;
extern const MethodInfo* DatabaseManager_OnError_m532773922_MethodInfo_var;
extern const uint32_t DatabaseManager_OnEnable_m739653325_MetadataUsageId;
extern "C"  void DatabaseManager_OnEnable_m739653325 (DatabaseManager_t737590674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseManager_OnEnable_m739653325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)DatabaseManager_OnError_m532773922_MethodInfo_var);
		OnError_t314785609 * L_1 = (OnError_t314785609 *)il2cpp_codegen_object_new(OnError_t314785609_il2cpp_TypeInfo_var);
		OnError__ctor_m1086636545(L_1, __this, L_0, /*hidden argument*/NULL);
		SQLiteEventListener_add_onError_m1384741999(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DatabaseManager::OnDisable()
extern Il2CppClass* OnError_t314785609_il2cpp_TypeInfo_var;
extern const MethodInfo* DatabaseManager_OnError_m532773922_MethodInfo_var;
extern const uint32_t DatabaseManager_OnDisable_m1895353856_MetadataUsageId;
extern "C"  void DatabaseManager_OnDisable_m1895353856 (DatabaseManager_t737590674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseManager_OnDisable_m1895353856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)DatabaseManager_OnError_m532773922_MethodInfo_var);
		OnError_t314785609 * L_1 = (OnError_t314785609 *)il2cpp_codegen_object_new(OnError_t314785609_il2cpp_TypeInfo_var);
		OnError__ctor_m1086636545(L_1, __this, L_0, /*hidden argument*/NULL);
		SQLiteEventListener_remove_onError_m1527352418(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DatabaseManager::OnError(System.String)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t DatabaseManager_OnError_m532773922_MetadataUsageId;
extern "C"  void DatabaseManager_OnError_m532773922 (DatabaseManager_t737590674 * __this, String_t* ___err0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseManager_OnError_m532773922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___err0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DatabaseManager::CreateDB()
extern Il2CppCodeGenString* _stringLiteral267720400;
extern Il2CppCodeGenString* _stringLiteral2471067713;
extern const uint32_t DatabaseManager_CreateDB_m1991173893_MetadataUsageId;
extern "C"  void DatabaseManager_CreateDB_m1991173893 (DatabaseManager_t737590674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseManager_CreateDB_m1991173893_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SQLiteDB_t557922535 * L_0 = __this->get_db_2();
		NullCheck(L_0);
		bool L_1 = SQLiteDB_get_Exists_m3278641920(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		SQLiteDB_t557922535 * L_2 = __this->get_db_2();
		NullCheck(L_2);
		SQLiteDB_ConnectToDefaultDatabase_m3847386093(L_2, _stringLiteral267720400, (bool)0, /*hidden argument*/NULL);
		goto IL_0039;
	}

IL_0027:
	{
		SQLiteDB_t557922535 * L_3 = __this->get_db_2();
		NullCheck(L_3);
		SQLiteDB_ConnectToDefaultDatabase_m3847386093(L_3, _stringLiteral267720400, (bool)1, /*hidden argument*/NULL);
	}

IL_0039:
	{
		SQLiteDB_t557922535 * L_4 = __this->get_db_2();
		NullCheck(L_4);
		bool L_5 = SQLiteDB_IsTableExists_m4049229627(L_4, _stringLiteral2471067713, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0055;
		}
	}
	{
		DatabaseManager_CreateTable_m2952491549(__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Boolean DatabaseManager::CreateTable()
extern Il2CppClass* DBSchema_t4247391996_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2471067713;
extern Il2CppCodeGenString* _stringLiteral2420395;
extern Il2CppCodeGenString* _stringLiteral1417172811;
extern const uint32_t DatabaseManager_CreateTable_m2952491549_MetadataUsageId;
extern "C"  bool DatabaseManager_CreateTable_m2952491549 (DatabaseManager_t737590674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseManager_CreateTable_m2952491549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DBSchema_t4247391996 * V_0 = NULL;
	{
		SQLiteDB_t557922535 * L_0 = __this->get_db_2();
		NullCheck(L_0);
		bool L_1 = SQLiteDB_get_Exists_m3278641920(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004a;
		}
	}
	{
		DBSchema_t4247391996 * L_2 = (DBSchema_t4247391996 *)il2cpp_codegen_object_new(DBSchema_t4247391996_il2cpp_TypeInfo_var);
		DBSchema__ctor_m2925071800(L_2, _stringLiteral2471067713, /*hidden argument*/NULL);
		V_0 = L_2;
		DBSchema_t4247391996 * L_3 = V_0;
		NullCheck(L_3);
		DBSchema_AddField_m776412250(L_3, _stringLiteral2420395, 4, ((int32_t)100), (bool)0, (bool)0, (bool)0, /*hidden argument*/NULL);
		DBSchema_t4247391996 * L_4 = V_0;
		NullCheck(L_4);
		DBSchema_AddField_m776412250(L_4, _stringLiteral1417172811, 1, ((int32_t)100), (bool)0, (bool)0, (bool)0, /*hidden argument*/NULL);
		SQLiteDB_t557922535 * L_5 = __this->get_db_2();
		DBSchema_t4247391996 * L_6 = V_0;
		NullCheck(L_5);
		bool L_7 = SQLiteDB_CreateTable_m3808471283(L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_004a:
	{
		return (bool)0;
	}
}
// System.Void DatabaseManager::AddNewScore(System.String,System.Int32)
extern Il2CppClass* List_1_t722451806_il2cpp_TypeInfo_var;
extern Il2CppClass* DB_DataPair_t4220460133_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2069798182_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2420395;
extern Il2CppCodeGenString* _stringLiteral1417172811;
extern Il2CppCodeGenString* _stringLiteral2471067713;
extern Il2CppCodeGenString* _stringLiteral4161151226;
extern const uint32_t DatabaseManager_AddNewScore_m941688607_MetadataUsageId;
extern "C"  void DatabaseManager_AddNewScore_m941688607 (DatabaseManager_t737590674 * __this, String_t* ___name0, int32_t ___time1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseManager_AddNewScore_m941688607_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t722451806 * V_0 = NULL;
	DB_DataPair_t4220460133  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		List_1_t722451806 * L_0 = (List_1_t722451806 *)il2cpp_codegen_object_new(List_1_t722451806_il2cpp_TypeInfo_var);
		List_1__ctor_m2069798182(L_0, /*hidden argument*/List_1__ctor_m2069798182_MethodInfo_var);
		V_0 = L_0;
		Initobj (DB_DataPair_t4220460133_il2cpp_TypeInfo_var, (&V_1));
		(&V_1)->set_fieldName_0(_stringLiteral2420395);
		String_t* L_1 = ___name0;
		(&V_1)->set_value_1(L_1);
		List_1_t722451806 * L_2 = V_0;
		DB_DataPair_t4220460133  L_3 = V_1;
		NullCheck(L_2);
		VirtActionInvoker1< DB_DataPair_t4220460133  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Add(!0) */, L_2, L_3);
		(&V_1)->set_fieldName_0(_stringLiteral1417172811);
		String_t* L_4 = Int32_ToString_m1286526384((&___time1), /*hidden argument*/NULL);
		(&V_1)->set_value_1(L_4);
		List_1_t722451806 * L_5 = V_0;
		DB_DataPair_t4220460133  L_6 = V_1;
		NullCheck(L_5);
		VirtActionInvoker1< DB_DataPair_t4220460133  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Add(!0) */, L_5, L_6);
		SQLiteDB_t557922535 * L_7 = __this->get_db_2();
		List_1_t722451806 * L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = SQLiteDB_Insert_m4013404553(L_7, _stringLiteral2471067713, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_006d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral4161151226, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
// System.Void DatabaseManager::GetScoresData()
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2312675008;
extern Il2CppCodeGenString* _stringLiteral2420395;
extern Il2CppCodeGenString* _stringLiteral1417172811;
extern Il2CppCodeGenString* _stringLiteral2505164499;
extern Il2CppCodeGenString* _stringLiteral3448946361;
extern const uint32_t DatabaseManager_GetScoresData_m1846108248_MetadataUsageId;
extern "C"  void DatabaseManager_GetScoresData_m1846108248 (DatabaseManager_t737590674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DatabaseManager_GetScoresData_m1846108248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DBReader_t4220400158 * V_0 = NULL;
	int32_t V_1 = 0;
	TimeSpan_t763862892  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		TextU5BU5D_t4157799059* L_0 = __this->get_scores_4();
		if (!L_0)
		{
			goto IL_00b9;
		}
	}
	{
		TextU5BU5D_t4157799059* L_1 = __this->get_names_3();
		if (!L_1)
		{
			goto IL_00b9;
		}
	}
	{
		SQLiteDB_t557922535 * L_2 = __this->get_db_2();
		NullCheck(L_2);
		DBReader_t4220400158 * L_3 = SQLiteDB_Select_m2209825900(L_2, _stringLiteral2312675008, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_00a1;
	}

IL_002e:
	{
		TextU5BU5D_t4157799059* L_4 = __this->get_names_3();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		DBReader_t4220400158 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = DBReader_GetStringValue_m1373122399(L_7, _stringLiteral2420395, /*hidden argument*/NULL);
		NullCheck(((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6))));
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6))), L_8);
		DBReader_t4220400158 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = DBReader_GetIntValue_m2828894496(L_9, _stringLiteral1417172811, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_11 = TimeSpan_FromTicks_m2483091767(NULL /*static, unused*/, (((int64_t)((int64_t)L_10))), /*hidden argument*/NULL);
		V_2 = L_11;
		TextU5BU5D_t4157799059* L_12 = __this->get_scores_4();
		int32_t L_13 = V_1;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		int32_t L_15 = TimeSpan_get_Hours_m1664362814((&V_2), /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_16);
		int32_t L_18 = TimeSpan_get_Minutes_m1876674446((&V_2), /*hidden argument*/NULL);
		int32_t L_19 = L_18;
		Il2CppObject * L_20 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_19);
		int32_t L_21 = TimeSpan_get_Seconds_m4185591086((&V_2), /*hidden argument*/NULL);
		int32_t L_22 = L_21;
		Il2CppObject * L_23 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_22);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral3448946361, L_17, L_20, L_23, /*hidden argument*/NULL);
		String_t* L_25 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2505164499, L_24, /*hidden argument*/NULL);
		NullCheck(((L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14))));
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, ((L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14))), L_25);
		int32_t L_26 = V_1;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00a1:
	{
		DBReader_t4220400158 * L_27 = V_0;
		if (!L_27)
		{
			goto IL_00b9;
		}
	}
	{
		DBReader_t4220400158 * L_28 = V_0;
		NullCheck(L_28);
		bool L_29 = DBReader_Read_m2141930664(L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00b9;
		}
	}
	{
		int32_t L_30 = V_1;
		if ((((int32_t)L_30) < ((int32_t)3)))
		{
			goto IL_002e;
		}
	}

IL_00b9:
	{
		return;
	}
}
// System.Void DatabaseManager::OnApplicationQuit()
extern "C"  void DatabaseManager_OnApplicationQuit_m3294051863 (DatabaseManager_t737590674 * __this, const MethodInfo* method)
{
	{
		SQLiteDB_t557922535 * L_0 = __this->get_db_2();
		NullCheck(L_0);
		SQLiteDB_Dispose_m2829228156(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyAttack::.ctor()
extern Il2CppCodeGenString* _stringLiteral96920;
extern const uint32_t EnemyAttack__ctor_m2061187483_MetadataUsageId;
extern "C"  void EnemyAttack__ctor_m2061187483 (EnemyAttack_t1231588304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyAttack__ctor_m2061187483_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_timeBetweenAttacks_2((2.0f));
		__this->set_attackDamage_3(((int32_t)10));
		int32_t L_0 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, _stringLiteral96920, /*hidden argument*/NULL);
		__this->set_atkHash_5(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyAttack::Awake()
extern const MethodInfo* Component_GetComponent_TisAudioSource_t3628549054_m3821406207_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimator_t792326996_m4147395588_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisEnemyHealth_t1417584612_m2559288533_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerEffects_t1618007745_m1848109824_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t EnemyAttack_Awake_m2298792702_MetadataUsageId;
extern "C"  void EnemyAttack_Awake_m2298792702 (EnemyAttack_t1231588304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyAttack_Awake_m2298792702_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t3628549054 * L_0 = Component_GetComponent_TisAudioSource_t3628549054_m3821406207(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3628549054_m3821406207_MethodInfo_var);
		__this->set_audio_8(L_0);
		Animator_t792326996 * L_1 = Component_GetComponent_TisAnimator_t792326996_m4147395588(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t792326996_m4147395588_MethodInfo_var);
		__this->set_anim_7(L_1);
		EnemyHealth_t1417584612 * L_2 = Component_GetComponent_TisEnemyHealth_t1417584612_m2559288533(__this, /*hidden argument*/Component_GetComponent_TisEnemyHealth_t1417584612_m2559288533_MethodInfo_var);
		__this->set_enemyHealth_11(L_2);
		GameObject_t4012695102 * L_3 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2393081601, /*hidden argument*/NULL);
		NullCheck(L_3);
		PlayerHealth_t3877793981 * L_4 = GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008(L_3, /*hidden argument*/GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008_MethodInfo_var);
		__this->set_playerHealth_10(L_4);
		GameObject_t4012695102 * L_5 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2393081601, /*hidden argument*/NULL);
		NullCheck(L_5);
		PlayerEffects_t1618007745 * L_6 = GameObject_GetComponent_TisPlayerEffects_t1618007745_m1848109824(L_5, /*hidden argument*/GameObject_GetComponent_TisPlayerEffects_t1618007745_m1848109824_MethodInfo_var);
		__this->set_playerEffects_12(L_6);
		return;
	}
}
// System.Void EnemyAttack::Update()
extern "C"  void EnemyAttack_Update_m1199164626 (EnemyAttack_t1231588304 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_timer_4();
		float L_1 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timer_4(((float)((float)L_0+(float)L_1)));
		float L_2 = __this->get_timer_4();
		float L_3 = __this->get_timeBetweenAttacks_2();
		if ((!(((float)L_2) >= ((float)L_3))))
		{
			goto IL_005a;
		}
	}
	{
		bool L_4 = __this->get_plyrInRange_6();
		if (!L_4)
		{
			goto IL_005a;
		}
	}
	{
		AudioSource_t3628549054 * L_5 = __this->get_audio_8();
		NullCheck(L_5);
		bool L_6 = AudioSource_get_isPlaying_m4213444423(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		AudioSource_t3628549054 * L_7 = __this->get_audio_8();
		AudioClip_t3714538611 * L_8 = __this->get_eatingAudio_9();
		NullCheck(L_7);
		AudioSource_PlayOneShot_m823779350(L_7, L_8, (0.4f), /*hidden argument*/NULL);
	}

IL_0054:
	{
		EnemyAttack_Attack_m415309457(__this, /*hidden argument*/NULL);
	}

IL_005a:
	{
		return;
	}
}
// System.Void EnemyAttack::Attack()
extern "C"  void EnemyAttack_Attack_m415309457 (EnemyAttack_t1231588304 * __this, const MethodInfo* method)
{
	{
		__this->set_timer_4((0.0f));
		PlayerHealth_t3877793981 * L_0 = __this->get_playerHealth_10();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_currentHealth_3();
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		PlayerHealth_t3877793981 * L_2 = __this->get_playerHealth_10();
		int32_t L_3 = __this->get_attackDamage_3();
		NullCheck(L_2);
		PlayerHealth_TookDamage_m4294364413(L_2, L_3, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void EnemyAttack::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t EnemyAttack_OnTriggerEnter_m2539862973_MetadataUsageId;
extern "C"  void EnemyAttack_OnTriggerEnter_m2539862973 (EnemyAttack_t1231588304 * __this, Collider_t955670625 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyAttack_OnTriggerEnter_m2539862973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t955670625 * L_0 = ___other0;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m305486283(L_0, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0044;
		}
	}
	{
		EnemyHealth_t1417584612 * L_2 = __this->get_enemyHealth_11();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_currentHealth_6();
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0044;
		}
	}
	{
		PlayerEffects_t1618007745 * L_4 = __this->get_playerEffects_12();
		NullCheck(L_4);
		PlayerEffects_showHitEffect_m751940815(L_4, /*hidden argument*/NULL);
		Animator_t792326996 * L_5 = __this->get_anim_7();
		int32_t L_6 = __this->get_atkHash_5();
		NullCheck(L_5);
		Animator_SetTrigger_m2911354149(L_5, L_6, /*hidden argument*/NULL);
		__this->set_plyrInRange_6((bool)1);
	}

IL_0044:
	{
		return;
	}
}
// System.Void EnemyAttack::OnTriggerExit(UnityEngine.Collider)
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t EnemyAttack_OnTriggerExit_m701685317_MetadataUsageId;
extern "C"  void EnemyAttack_OnTriggerExit_m701685317 (EnemyAttack_t1231588304 * __this, Collider_t955670625 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyAttack_OnTriggerExit_m701685317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t955670625 * L_0 = ___other0;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m305486283(L_0, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		EnemyHealth_t1417584612 * L_2 = __this->get_enemyHealth_11();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_currentHealth_6();
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0039;
		}
	}
	{
		PlayerEffects_t1618007745 * L_4 = __this->get_playerEffects_12();
		NullCheck(L_4);
		GameObject_t4012695102 * L_5 = L_4->get_hitEffect_2();
		NullCheck(L_5);
		GameObject_SetActive_m3538205401(L_5, (bool)0, /*hidden argument*/NULL);
		__this->set_plyrInRange_6((bool)0);
	}

IL_0039:
	{
		return;
	}
}
// System.Void EnemyHealth::.ctor()
extern Il2CppCodeGenString* _stringLiteral3244309516;
extern const uint32_t EnemyHealth__ctor_m1528331271_MetadataUsageId;
extern "C"  void EnemyHealth__ctor_m1528331271 (EnemyHealth_t1417584612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyHealth__ctor_m1528331271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_startHealth_5(((int32_t)100));
		__this->set_sinkSpeed_7((2.5f));
		int32_t L_0 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, _stringLiteral3244309516, /*hidden argument*/NULL);
		__this->set_healthHash_14(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyHealth::Awake()
extern const MethodInfo* Component_GetComponent_TisAudioSource_t3628549054_m3821406207_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisEnemyAttack_t1231588304_m2051088745_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimator_t792326996_m4147395588_MethodInfo_var;
extern const MethodInfo* Component_GetComponents_TisCapsuleCollider_t3138252142_m3419792738_MethodInfo_var;
extern const uint32_t EnemyHealth_Awake_m1765936490_MetadataUsageId;
extern "C"  void EnemyHealth_Awake_m1765936490 (EnemyHealth_t1417584612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyHealth_Awake_m1765936490_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t3628549054 * L_0 = Component_GetComponent_TisAudioSource_t3628549054_m3821406207(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3628549054_m3821406207_MethodInfo_var);
		__this->set_audio_10(L_0);
		EnemyAttack_t1231588304 * L_1 = Component_GetComponent_TisEnemyAttack_t1231588304_m2051088745(__this, /*hidden argument*/Component_GetComponent_TisEnemyAttack_t1231588304_m2051088745_MethodInfo_var);
		__this->set_enemyAtk_12(L_1);
		Animator_t792326996 * L_2 = Component_GetComponent_TisAnimator_t792326996_m4147395588(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t792326996_m4147395588_MethodInfo_var);
		__this->set_anim_13(L_2);
		CapsuleColliderU5BU5D_t3130374427* L_3 = Component_GetComponents_TisCapsuleCollider_t3138252142_m3419792738(__this, /*hidden argument*/Component_GetComponents_TisCapsuleCollider_t3138252142_m3419792738_MethodInfo_var);
		__this->set_capCollider_11(L_3);
		int32_t L_4 = __this->get_startHealth_5();
		__this->set_currentHealth_6(L_4);
		return;
	}
}
// System.Void EnemyHealth::Update()
extern "C"  void EnemyHealth_Update_m1860491238 (EnemyHealth_t1417584612 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isDead_3();
		if (!L_0)
		{
			goto IL_0062;
		}
	}
	{
		float L_1 = __this->get_time_2();
		float L_2 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_time_2(((float)((float)L_1+(float)L_2)));
		float L_3 = __this->get_time_2();
		if ((!(((float)L_3) > ((float)(4.0f)))))
		{
			goto IL_0062;
		}
	}
	{
		bool L_4 = __this->get_isSinking_4();
		if (!L_4)
		{
			goto IL_0062;
		}
	}
	{
		Transform_t284553113 * L_5 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_7 = Vector3_op_UnaryNegation_m3293197314(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		float L_8 = __this->get_sinkSpeed_7();
		Vector3_t3525329789  L_9 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		float L_10 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_11 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_Translate_m2849099360(L_5, L_11, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void EnemyHealth::HitDamage(System.Int32)
extern "C"  void EnemyHealth_HitDamage_m819917400 (EnemyHealth_t1417584612 * __this, int32_t ___amount0, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isDead_3();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		int32_t L_1 = __this->get_currentHealth_6();
		int32_t L_2 = ___amount0;
		__this->set_currentHealth_6(((int32_t)((int32_t)L_1-(int32_t)L_2)));
		Slider_t1468074762 * L_3 = __this->get_healthSlider_8();
		int32_t L_4 = __this->get_currentHealth_6();
		NullCheck(L_3);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_3, (((float)((float)L_4))));
		int32_t L_5 = __this->get_currentHealth_6();
		if ((((int32_t)L_5) > ((int32_t)0)))
		{
			goto IL_0064;
		}
	}
	{
		AudioSource_t3628549054 * L_6 = __this->get_audio_10();
		NullCheck(L_6);
		bool L_7 = AudioSource_get_isPlaying_m4213444423(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_005e;
		}
	}
	{
		AudioSource_t3628549054 * L_8 = __this->get_audio_10();
		AudioClip_t3714538611 * L_9 = __this->get_deathAudio_9();
		NullCheck(L_8);
		AudioSource_PlayOneShot_m823779350(L_8, L_9, (0.4f), /*hidden argument*/NULL);
	}

IL_005e:
	{
		EnemyHealth_Death_m3913393817(__this, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void EnemyHealth::Death()
extern const MethodInfo* Component_GetComponent_TisNavMeshAgent_t3296505762_m3159610649_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t1972007546_m2213165263_MethodInfo_var;
extern const uint32_t EnemyHealth_Death_m3913393817_MetadataUsageId;
extern "C"  void EnemyHealth_Death_m3913393817 (EnemyHealth_t1417584612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyHealth_Death_m3913393817_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EnemyAttack_t1231588304 * L_0 = __this->get_enemyAtk_12();
		NullCheck(L_0);
		Behaviour_set_enabled_m2046806933(L_0, (bool)0, /*hidden argument*/NULL);
		NavMeshAgent_t3296505762 * L_1 = Component_GetComponent_TisNavMeshAgent_t3296505762_m3159610649(__this, /*hidden argument*/Component_GetComponent_TisNavMeshAgent_t3296505762_m3159610649_MethodInfo_var);
		NullCheck(L_1);
		Behaviour_set_enabled_m2046806933(L_1, (bool)0, /*hidden argument*/NULL);
		Rigidbody_t1972007546 * L_2 = Component_GetComponent_TisRigidbody_t1972007546_m2213165263(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t1972007546_m2213165263_MethodInfo_var);
		NullCheck(L_2);
		Rigidbody_set_isKinematic_m294703295(L_2, (bool)1, /*hidden argument*/NULL);
		Animator_t792326996 * L_3 = __this->get_anim_13();
		int32_t L_4 = __this->get_healthHash_14();
		NullCheck(L_3);
		Animator_SetTrigger_m2911354149(L_3, L_4, /*hidden argument*/NULL);
		__this->set_isDead_3((bool)1);
		__this->set_isSinking_4((bool)1);
		CapsuleColliderU5BU5D_t3130374427* L_5 = __this->get_capCollider_11();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		int32_t L_6 = 0;
		NullCheck(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6))));
		Collider_set_isTrigger_m2057864191(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6))), (bool)1, /*hidden argument*/NULL);
		CapsuleColliderU5BU5D_t3130374427* L_7 = __this->get_capCollider_11();
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		int32_t L_8 = 1;
		NullCheck(((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		Collider_set_isTrigger_m2057864191(((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_9 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_9, (10.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyManager::.ctor()
extern "C"  void EnemyManager__ctor_m1552517398 (EnemyManager_t1035150117 * __this, const MethodInfo* method)
{
	{
		__this->set_spawnTime_4((2.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyManager::Start()
extern Il2CppCodeGenString* _stringLiteral1630653710;
extern const uint32_t EnemyManager_Start_m499655190_MetadataUsageId;
extern "C"  void EnemyManager_Start_m499655190 (EnemyManager_t1035150117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyManager_Start_m499655190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_spawnTime_4();
		float L_1 = __this->get_spawnTime_4();
		MonoBehaviour_InvokeRepeating_m1115468640(__this, _stringLiteral1630653710, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyManager::SpawnItem()
extern "C"  void EnemyManager_SpawnItem_m261633634 (EnemyManager_t1035150117 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Slider_t1468074762 * L_0 = __this->get_healthSldr_2();
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_0);
		if ((!(((float)L_1) <= ((float)(0.0f)))))
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		TransformU5BU5D_t3681339876* L_2 = __this->get_spawnPoints_5();
		NullCheck(L_2);
		int32_t L_3 = Random_Range_m75452833(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t4012695102 * L_4 = __this->get_item_3();
		TransformU5BU5D_t3681339876* L_5 = __this->get_spawnPoints_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		Vector3_t3525329789  L_8 = Transform_get_position_m2211398607(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), /*hidden argument*/NULL);
		TransformU5BU5D_t3681339876* L_9 = __this->get_spawnPoints_5();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		NullCheck(((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		Quaternion_t1891715979  L_12 = Transform_get_rotation_m11483428(((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_4, L_8, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyMovement::.ctor()
extern "C"  void EnemyMovement__ctor_m1262408468 (EnemyMovement_t1797938231 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyMovement::Awake()
extern const MethodInfo* Component_GetComponent_TisPlayerHealth_t3877793981_m654738856_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisEnemyHealth_t1417584612_m2559288533_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisNavMeshAgent_t3296505762_m3159610649_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t EnemyMovement_Awake_m1500013687_MetadataUsageId;
extern "C"  void EnemyMovement_Awake_m1500013687 (EnemyMovement_t1797938231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnemyMovement_Awake_m1500013687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2393081601, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t284553113 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		__this->set_playerPosition_2(L_1);
		Transform_t284553113 * L_2 = __this->get_playerPosition_2();
		NullCheck(L_2);
		PlayerHealth_t3877793981 * L_3 = Component_GetComponent_TisPlayerHealth_t3877793981_m654738856(L_2, /*hidden argument*/Component_GetComponent_TisPlayerHealth_t3877793981_m654738856_MethodInfo_var);
		__this->set_playerHealth_4(L_3);
		EnemyHealth_t1417584612 * L_4 = Component_GetComponent_TisEnemyHealth_t1417584612_m2559288533(__this, /*hidden argument*/Component_GetComponent_TisEnemyHealth_t1417584612_m2559288533_MethodInfo_var);
		__this->set_enemyHealth_5(L_4);
		NavMeshAgent_t3296505762 * L_5 = Component_GetComponent_TisNavMeshAgent_t3296505762_m3159610649(__this, /*hidden argument*/Component_GetComponent_TisNavMeshAgent_t3296505762_m3159610649_MethodInfo_var);
		__this->set_navigation_3(L_5);
		return;
	}
}
// System.Void EnemyMovement::Update()
extern "C"  void EnemyMovement_Update_m2206818937 (EnemyMovement_t1797938231 * __this, const MethodInfo* method)
{
	{
		EnemyHealth_t1417584612 * L_0 = __this->get_enemyHealth_5();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_currentHealth_6();
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003e;
		}
	}
	{
		PlayerHealth_t3877793981 * L_2 = __this->get_playerHealth_4();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_currentHealth_3();
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_003e;
		}
	}
	{
		NavMeshAgent_t3296505762 * L_4 = __this->get_navigation_3();
		Transform_t284553113 * L_5 = __this->get_playerPosition_2();
		NullCheck(L_5);
		Vector3_t3525329789  L_6 = Transform_get_position_m2211398607(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		NavMeshAgent_SetDestination_m1934812347(L_4, L_6, /*hidden argument*/NULL);
		goto IL_004a;
	}

IL_003e:
	{
		NavMeshAgent_t3296505762 * L_7 = __this->get_navigation_3();
		NullCheck(L_7);
		Behaviour_set_enabled_m2046806933(L_7, (bool)0, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void EventsManager::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t EventsManager__ctor_m4179436471_MetadataUsageId;
extern "C"  void EventsManager__ctor_m4179436471 (EventsManager_t4180097076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventsManager__ctor_m4179436471_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_name_6(L_0);
		SQLiteDB_t557922535 * L_1 = SQLiteDB_get_Instance_m2094062401(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_db_7(L_1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EventsManager::Awake()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t3286458198_m202917489_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisInputField_t2345609593_m2575864366_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3471646982;
extern Il2CppCodeGenString* _stringLiteral2127272705;
extern Il2CppCodeGenString* _stringLiteral3356722032;
extern Il2CppCodeGenString* _stringLiteral3448946361;
extern const uint32_t EventsManager_Awake_m122074394_MetadataUsageId;
extern "C"  void EventsManager_Awake_m122074394 (EventsManager_t4180097076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventsManager_Awake_m122074394_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3471646982, /*hidden argument*/NULL);
		NullCheck(L_0);
		Text_t3286458198 * L_1 = GameObject_GetComponent_TisText_t3286458198_m202917489(L_0, /*hidden argument*/GameObject_GetComponent_TisText_t3286458198_m202917489_MethodInfo_var);
		__this->set_time_5(L_1);
		GameObject_t4012695102 * L_2 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2127272705, /*hidden argument*/NULL);
		NullCheck(L_2);
		GlobalObjectManager_t849077355 * L_3 = GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422(L_2, /*hidden argument*/GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422_MethodInfo_var);
		__this->set_persistentData_3(L_3);
		GameObject_t4012695102 * L_4 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3356722032, /*hidden argument*/NULL);
		NullCheck(L_4);
		InputField_t2345609593 * L_5 = GameObject_GetComponent_TisInputField_t2345609593_m2575864366(L_4, /*hidden argument*/GameObject_GetComponent_TisInputField_t2345609593_m2575864366_MethodInfo_var);
		__this->set_input_4(L_5);
		Text_t3286458198 * L_6 = __this->get_time_5();
		GlobalObjectManager_t849077355 * L_7 = __this->get_persistentData_3();
		NullCheck(L_7);
		TimeSpan_t763862892 * L_8 = L_7->get_address_of_elapsed_6();
		int32_t L_9 = TimeSpan_get_Hours_m1664362814(L_8, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_10);
		GlobalObjectManager_t849077355 * L_12 = __this->get_persistentData_3();
		NullCheck(L_12);
		TimeSpan_t763862892 * L_13 = L_12->get_address_of_elapsed_6();
		int32_t L_14 = TimeSpan_get_Minutes_m1876674446(L_13, /*hidden argument*/NULL);
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_15);
		GlobalObjectManager_t849077355 * L_17 = __this->get_persistentData_3();
		NullCheck(L_17);
		TimeSpan_t763862892 * L_18 = L_17->get_address_of_elapsed_6();
		int32_t L_19 = TimeSpan_get_Seconds_m4185591086(L_18, /*hidden argument*/NULL);
		int32_t L_20 = L_19;
		Il2CppObject * L_21 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral3448946361, L_11, L_16, L_21, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_22);
		GlobalObjectManager_t849077355 * L_23 = __this->get_persistentData_3();
		NullCheck(L_23);
		L_23->set_isLoaded_7((bool)1);
		GlobalObjectManager_t849077355 * L_24 = __this->get_persistentData_3();
		NullCheck(L_24);
		L_24->set_isDead_8((bool)0);
		return;
	}
}
// System.Void EventsManager::OnApplicationQuit()
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2650163027;
extern const uint32_t EventsManager_OnApplicationQuit_m2352133813_MetadataUsageId;
extern "C"  void EventsManager_OnApplicationQuit_m2352133813 (EventsManager_t4180097076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventsManager_OnApplicationQuit_m2352133813_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2650163027, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EventsManager::setName()
extern "C"  void EventsManager_setName_m3010993858 (EventsManager_t4180097076 * __this, const MethodInfo* method)
{
	{
		InputField_t2345609593 * L_0 = __this->get_input_4();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m3972300732(L_0, /*hidden argument*/NULL);
		__this->set_name_6(L_1);
		return;
	}
}
// System.Void EventsManager::saveTime()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t722451806_il2cpp_TypeInfo_var;
extern Il2CppClass* DB_DataPair_t4220460133_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* DialogResultMethod_t1789391825_il2cpp_TypeInfo_var;
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t990034267_m2812094092_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2069798182_MethodInfo_var;
extern const MethodInfo* EventsManager_closeMessageBox_m1554315340_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2569629;
extern Il2CppCodeGenString* _stringLiteral2420395;
extern Il2CppCodeGenString* _stringLiteral1417172811;
extern Il2CppCodeGenString* _stringLiteral2471067713;
extern Il2CppCodeGenString* _stringLiteral4161151226;
extern Il2CppCodeGenString* _stringLiteral2544471936;
extern Il2CppCodeGenString* _stringLiteral2524;
extern const uint32_t EventsManager_saveTime_m3834172407_MetadataUsageId;
extern "C"  void EventsManager_saveTime_m3834172407 (EventsManager_t4180097076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventsManager_saveTime_m3834172407_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Button_t990034267 * V_0 = NULL;
	List_1_t722451806 * V_1 = NULL;
	DB_DataPair_t4220460133  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t V_3 = 0;
	int64_t V_4 = 0;
	{
		String_t* L_0 = __this->get_name_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_2 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00b5;
		}
	}
	{
		GameObject_t4012695102 * L_3 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2569629, /*hidden argument*/NULL);
		NullCheck(L_3);
		Button_t990034267 * L_4 = GameObject_GetComponent_TisButton_t990034267_m2812094092(L_3, /*hidden argument*/GameObject_GetComponent_TisButton_t990034267_m2812094092_MethodInfo_var);
		V_0 = L_4;
		Button_t990034267 * L_5 = V_0;
		NullCheck(L_5);
		Selectable_set_interactable_m2686686419(L_5, (bool)0, /*hidden argument*/NULL);
		List_1_t722451806 * L_6 = (List_1_t722451806 *)il2cpp_codegen_object_new(List_1_t722451806_il2cpp_TypeInfo_var);
		List_1__ctor_m2069798182(L_6, /*hidden argument*/List_1__ctor_m2069798182_MethodInfo_var);
		V_1 = L_6;
		Initobj (DB_DataPair_t4220460133_il2cpp_TypeInfo_var, (&V_2));
		(&V_2)->set_fieldName_0(_stringLiteral2420395);
		String_t* L_7 = __this->get_name_6();
		(&V_2)->set_value_1(L_7);
		List_1_t722451806 * L_8 = V_1;
		DB_DataPair_t4220460133  L_9 = V_2;
		NullCheck(L_8);
		VirtActionInvoker1< DB_DataPair_t4220460133  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Add(!0) */, L_8, L_9);
		(&V_2)->set_fieldName_0(_stringLiteral1417172811);
		GlobalObjectManager_t849077355 * L_10 = __this->get_persistentData_3();
		NullCheck(L_10);
		TimeSpan_t763862892 * L_11 = L_10->get_address_of_elapsed_6();
		int64_t L_12 = TimeSpan_get_Ticks_m315930342(L_11, /*hidden argument*/NULL);
		V_4 = L_12;
		String_t* L_13 = Int64_ToString_m3478011791((&V_4), /*hidden argument*/NULL);
		(&V_2)->set_value_1(L_13);
		List_1_t722451806 * L_14 = V_1;
		DB_DataPair_t4220460133  L_15 = V_2;
		NullCheck(L_14);
		VirtActionInvoker1< DB_DataPair_t4220460133  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_DataPair>::Add(!0) */, L_14, L_15);
		SQLiteDB_t557922535 * L_16 = __this->get_db_7();
		List_1_t722451806 * L_17 = V_1;
		NullCheck(L_16);
		int32_t L_18 = SQLiteDB_Insert_m4013404553(L_16, _stringLiteral2471067713, L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		int32_t L_19 = V_3;
		if ((((int32_t)L_19) <= ((int32_t)0)))
		{
			goto IL_00b0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral4161151226, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		goto IL_00dc;
	}

IL_00b5:
	{
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)EventsManager_closeMessageBox_m1554315340_MethodInfo_var);
		DialogResultMethod_t1789391825 * L_21 = (DialogResultMethod_t1789391825 *)il2cpp_codegen_object_new(DialogResultMethod_t1789391825_il2cpp_TypeInfo_var);
		DialogResultMethod__ctor_m3207538458(L_21, __this, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_23 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_Show_m3994277758(NULL /*static, unused*/, 0, _stringLiteral2544471936, L_21, (bool)1, _stringLiteral2524, L_22, L_23, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		return;
	}
}
// System.Void EventsManager::closeMessageBox(System.Int32,PoqXert.MessageBox.DialogResult)
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern const uint32_t EventsManager_closeMessageBox_m1554315340_MetadataUsageId;
extern "C"  void EventsManager_closeMessageBox_m1554315340 (EventsManager_t4180097076 * __this, int32_t ___id0, int32_t ___btn1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventsManager_closeMessageBox_m1554315340_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_Close_m1432979546(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EventsManager::replayGame()
extern Il2CppCodeGenString* _stringLiteral2390489;
extern const uint32_t EventsManager_replayGame_m3649958854_MetadataUsageId;
extern "C"  void EventsManager_replayGame_m3649958854 (EventsManager_t4180097076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventsManager_replayGame_m3649958854_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral2390489, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EventsManager::homeGame()
extern Il2CppCodeGenString* _stringLiteral2255103;
extern const uint32_t EventsManager_homeGame_m3281303998_MetadataUsageId;
extern "C"  void EventsManager_homeGame_m3281303998 (EventsManager_t4180097076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventsManager_homeGame_m3281303998_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral2255103, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FollowCharacter::.ctor()
extern "C"  void FollowCharacter__ctor_m2293595155 (FollowCharacter_t1365044824 * __this, const MethodInfo* method)
{
	{
		__this->set_smoothing_3((5.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FollowCharacter::Start()
extern "C"  void FollowCharacter_Start_m1240732947 (FollowCharacter_t1365044824 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		Transform_t284553113 * L_2 = __this->get_target_2();
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		Vector3_t3525329789  L_4 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		__this->set_offset_4(L_4);
		return;
	}
}
// System.Void FollowCharacter::FixedUpdate()
extern "C"  void FollowCharacter_FixedUpdate_m3033055054 (FollowCharacter_t1365044824 * __this, const MethodInfo* method)
{
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t284553113 * L_0 = __this->get_target_2();
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = __this->get_offset_4();
		Vector3_t3525329789  L_3 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Transform_t284553113 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_5 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t3525329789  L_6 = Transform_get_position_m2211398607(L_5, /*hidden argument*/NULL);
		Vector3_t3525329789  L_7 = V_0;
		float L_8 = __this->get_smoothing_3();
		float L_9 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_10 = Vector3_Lerp_m650470329(NULL /*static, unused*/, L_6, L_7, ((float)((float)L_8*(float)L_9)), /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_position_m3111394108(L_4, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameTimer::.ctor()
extern "C"  void GameTimer__ctor_m3467669624 (GameTimer_t2994639443 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameTimer::Start()
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral2127272705;
extern const uint32_t GameTimer_Start_m2414807416_MetadataUsageId;
extern "C"  void GameTimer_Start_m2414807416 (GameTimer_t2994639443 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameTimer_Start_m2414807416_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2393081601, /*hidden argument*/NULL);
		NullCheck(L_0);
		PlayerHealth_t3877793981 * L_1 = GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008(L_0, /*hidden argument*/GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008_MethodInfo_var);
		__this->set_playerHealth_5(L_1);
		GameObject_t4012695102 * L_2 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2127272705, /*hidden argument*/NULL);
		NullCheck(L_2);
		GlobalObjectManager_t849077355 * L_3 = GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422(L_2, /*hidden argument*/GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422_MethodInfo_var);
		__this->set_persistentData_6(L_3);
		GlobalObjectManager_t849077355 * L_4 = __this->get_persistentData_6();
		NullCheck(L_4);
		bool L_5 = L_4->get_isLoaded_7();
		if (!L_5)
		{
			goto IL_0051;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_6 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_2(L_6);
		GlobalObjectManager_t849077355 * L_7 = __this->get_persistentData_6();
		NullCheck(L_7);
		L_7->set_isLoaded_7((bool)0);
	}

IL_0051:
	{
		return;
	}
}
// System.Void GameTimer::Update()
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisText_t3286458198_m404091797_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3448946361;
extern Il2CppCodeGenString* _stringLiteral2945850434;
extern const uint32_t GameTimer_Update_m1850438037_MetadataUsageId;
extern "C"  void GameTimer_Update_m1850438037 (GameTimer_t2994639443 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameTimer_Update_m1850438037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		GlobalObjectManager_t849077355 * L_0 = __this->get_persistentData_6();
		NullCheck(L_0);
		bool L_1 = L_0->get_isDead_8();
		if (L_1)
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_2 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t339033936  L_3 = __this->get_startTime_2();
		TimeSpan_t763862892  L_4 = DateTime_op_Subtraction_m3612355463(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_elapsedTime_3(L_4);
		TimeSpan_t763862892 * L_5 = __this->get_address_of_elapsedTime_3();
		int32_t L_6 = TimeSpan_get_Hours_m1664362814(L_5, /*hidden argument*/NULL);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_7);
		TimeSpan_t763862892 * L_9 = __this->get_address_of_elapsedTime_3();
		int32_t L_10 = TimeSpan_get_Minutes_m1876674446(L_9, /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_11);
		TimeSpan_t763862892 * L_13 = __this->get_address_of_elapsedTime_3();
		int32_t L_14 = TimeSpan_get_Seconds_m4185591086(L_13, /*hidden argument*/NULL);
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral3448946361, L_8, L_12, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		Text_t3286458198 * L_18 = Component_GetComponent_TisText_t3286458198_m404091797(__this, /*hidden argument*/Component_GetComponent_TisText_t3286458198_m404091797_MethodInfo_var);
		String_t* L_19 = V_0;
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_19);
		goto IL_00a9;
	}

IL_0072:
	{
		float L_20 = __this->get_time_4();
		float L_21 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_time_4(((float)((float)L_20+(float)L_21)));
		float L_22 = __this->get_time_4();
		if ((!(((float)L_22) > ((float)(3.0f)))))
		{
			goto IL_00a9;
		}
	}
	{
		GlobalObjectManager_t849077355 * L_23 = __this->get_persistentData_6();
		NullCheck(L_23);
		GlobalObjectManager_SaveData_m3896486251(L_23, /*hidden argument*/NULL);
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral2945850434, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		return;
	}
}
// System.Void GetScores::.ctor()
extern "C"  void GetScores__ctor_m3636077652 (GetScores_t2792216311 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GetScores::Awake()
extern "C"  void GetScores_Awake_m3873682871 (GetScores_t2792216311 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GlobalObjectManager::.ctor()
extern "C"  void GlobalObjectManager__ctor_m2975135584 (GlobalObjectManager_t849077355 * __this, const MethodInfo* method)
{
	{
		__this->set_isLoaded_7((bool)1);
		__this->set_volume_10((1.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GlobalObjectManager::Awake()
extern Il2CppClass* GlobalObjectManager_t849077355_il2cpp_TypeInfo_var;
extern const uint32_t GlobalObjectManager_Awake_m3212740803_MetadataUsageId;
extern "C"  void GlobalObjectManager_Awake_m3212740803 (GlobalObjectManager_t849077355 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlobalObjectManager_Awake_m3212740803_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GlobalObjectManager_t849077355 * L_0 = ((GlobalObjectManager_t849077355_StaticFields*)GlobalObjectManager_t849077355_il2cpp_TypeInfo_var->static_fields)->get_Instance_12();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		GameObject_t4012695102 * L_2 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m4064482788(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		((GlobalObjectManager_t849077355_StaticFields*)GlobalObjectManager_t849077355_il2cpp_TypeInfo_var->static_fields)->set_Instance_12(__this);
		goto IL_0041;
	}

IL_0026:
	{
		GlobalObjectManager_t849077355 * L_3 = ((GlobalObjectManager_t849077355_StaticFields*)GlobalObjectManager_t849077355_il2cpp_TypeInfo_var->static_fields)->get_Instance_12();
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, __this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0041;
		}
	}
	{
		GameObject_t4012695102 * L_5 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void GlobalObjectManager::OnLevelWasLoaded()
extern Il2CppClass* GlobalObjectManager_t849077355_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioListener_t1735598807_m1628673542_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerAttack_t3691797673_m1464963220_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerPickups_t2873926454_m3462254187_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGameTimer_t2994639443_m1318047534_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral80811813;
extern const uint32_t GlobalObjectManager_OnLevelWasLoaded_m3732057101_MetadataUsageId;
extern "C"  void GlobalObjectManager_OnLevelWasLoaded_m3732057101 (GlobalObjectManager_t849077355 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlobalObjectManager_OnLevelWasLoaded_m3732057101_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	GameObject_t4012695102 * V_1 = NULL;
	{
		Camera_t3533968274 * L_0 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioListener_t1735598807 * L_1 = Component_GetComponent_TisAudioListener_t1735598807_m1628673542(L_0, /*hidden argument*/Component_GetComponent_TisAudioListener_t1735598807_m1628673542_MethodInfo_var);
		__this->set_listener_11(L_1);
		GameObject_t4012695102 * L_2 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2393081601, /*hidden argument*/NULL);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00da;
		}
	}
	{
		GameObject_t4012695102 * L_4 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral80811813, /*hidden argument*/NULL);
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00da;
		}
	}
	{
		GameObject_t4012695102 * L_6 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2393081601, /*hidden argument*/NULL);
		V_0 = L_6;
		GameObject_t4012695102 * L_7 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral80811813, /*hidden argument*/NULL);
		V_1 = L_7;
		GameObject_t4012695102 * L_8 = V_0;
		NullCheck(L_8);
		PlayerHealth_t3877793981 * L_9 = GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008(L_8, /*hidden argument*/GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008_MethodInfo_var);
		GlobalObjectManager_t849077355 * L_10 = ((GlobalObjectManager_t849077355_StaticFields*)GlobalObjectManager_t849077355_il2cpp_TypeInfo_var->static_fields)->get_Instance_12();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_currentHealth_2();
		NullCheck(L_9);
		L_9->set_currentHealth_3(L_11);
		GameObject_t4012695102 * L_12 = V_0;
		NullCheck(L_12);
		PlayerHealth_t3877793981 * L_13 = GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008(L_12, /*hidden argument*/GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008_MethodInfo_var);
		NullCheck(L_13);
		Slider_t1468074762 * L_14 = L_13->get_healthSldr_4();
		GlobalObjectManager_t849077355 * L_15 = ((GlobalObjectManager_t849077355_StaticFields*)GlobalObjectManager_t849077355_il2cpp_TypeInfo_var->static_fields)->get_Instance_12();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_currentHealth_2();
		NullCheck(L_14);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_14, (((float)((float)L_16))));
		GameObject_t4012695102 * L_17 = V_0;
		NullCheck(L_17);
		PlayerAttack_t3691797673 * L_18 = GameObject_GetComponent_TisPlayerAttack_t3691797673_m1464963220(L_17, /*hidden argument*/GameObject_GetComponent_TisPlayerAttack_t3691797673_m1464963220_MethodInfo_var);
		NullCheck(L_18);
		Slider_t1468074762 * L_19 = L_18->get_specialAtkSlider_5();
		GlobalObjectManager_t849077355 * L_20 = ((GlobalObjectManager_t849077355_StaticFields*)GlobalObjectManager_t849077355_il2cpp_TypeInfo_var->static_fields)->get_Instance_12();
		NullCheck(L_20);
		int32_t L_21 = L_20->get_special_3();
		NullCheck(L_19);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_19, (((float)((float)L_21))));
		GameObject_t4012695102 * L_22 = V_0;
		NullCheck(L_22);
		PlayerPickups_t2873926454 * L_23 = GameObject_GetComponent_TisPlayerPickups_t2873926454_m3462254187(L_22, /*hidden argument*/GameObject_GetComponent_TisPlayerPickups_t2873926454_m3462254187_MethodInfo_var);
		GlobalObjectManager_t849077355 * L_24 = ((GlobalObjectManager_t849077355_StaticFields*)GlobalObjectManager_t849077355_il2cpp_TypeInfo_var->static_fields)->get_Instance_12();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_numHits_4();
		NullCheck(L_23);
		L_23->set_numHits_11(L_25);
		GameObject_t4012695102 * L_26 = V_1;
		NullCheck(L_26);
		GameTimer_t2994639443 * L_27 = GameObject_GetComponent_TisGameTimer_t2994639443_m1318047534(L_26, /*hidden argument*/GameObject_GetComponent_TisGameTimer_t2994639443_m1318047534_MethodInfo_var);
		GlobalObjectManager_t849077355 * L_28 = ((GlobalObjectManager_t849077355_StaticFields*)GlobalObjectManager_t849077355_il2cpp_TypeInfo_var->static_fields)->get_Instance_12();
		NullCheck(L_28);
		DateTime_t339033936  L_29 = L_28->get_start_5();
		NullCheck(L_27);
		L_27->set_startTime_2(L_29);
		GameObject_t4012695102 * L_30 = V_1;
		NullCheck(L_30);
		GameTimer_t2994639443 * L_31 = GameObject_GetComponent_TisGameTimer_t2994639443_m1318047534(L_30, /*hidden argument*/GameObject_GetComponent_TisGameTimer_t2994639443_m1318047534_MethodInfo_var);
		GlobalObjectManager_t849077355 * L_32 = ((GlobalObjectManager_t849077355_StaticFields*)GlobalObjectManager_t849077355_il2cpp_TypeInfo_var->static_fields)->get_Instance_12();
		NullCheck(L_32);
		TimeSpan_t763862892  L_33 = L_32->get_elapsed_6();
		NullCheck(L_31);
		L_31->set_elapsedTime_3(L_33);
	}

IL_00da:
	{
		return;
	}
}
// System.Void GlobalObjectManager::SaveData()
extern Il2CppClass* GlobalObjectManager_t849077355_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerAttack_t3691797673_m1464963220_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerPickups_t2873926454_m3462254187_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGameTimer_t2994639443_m1318047534_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral80811813;
extern const uint32_t GlobalObjectManager_SaveData_m3896486251_MetadataUsageId;
extern "C"  void GlobalObjectManager_SaveData_m3896486251 (GlobalObjectManager_t849077355 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlobalObjectManager_SaveData_m3896486251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	GameObject_t4012695102 * V_1 = NULL;
	{
		GameObject_t4012695102 * L_0 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2393081601, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t4012695102 * L_1 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral80811813, /*hidden argument*/NULL);
		V_1 = L_1;
		GlobalObjectManager_t849077355 * L_2 = ((GlobalObjectManager_t849077355_StaticFields*)GlobalObjectManager_t849077355_il2cpp_TypeInfo_var->static_fields)->get_Instance_12();
		GameObject_t4012695102 * L_3 = V_0;
		NullCheck(L_3);
		PlayerHealth_t3877793981 * L_4 = GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008(L_3, /*hidden argument*/GameObject_GetComponent_TisPlayerHealth_t3877793981_m1973163008_MethodInfo_var);
		NullCheck(L_4);
		int32_t L_5 = L_4->get_currentHealth_3();
		NullCheck(L_2);
		L_2->set_currentHealth_2(L_5);
		GlobalObjectManager_t849077355 * L_6 = ((GlobalObjectManager_t849077355_StaticFields*)GlobalObjectManager_t849077355_il2cpp_TypeInfo_var->static_fields)->get_Instance_12();
		GameObject_t4012695102 * L_7 = V_0;
		NullCheck(L_7);
		PlayerAttack_t3691797673 * L_8 = GameObject_GetComponent_TisPlayerAttack_t3691797673_m1464963220(L_7, /*hidden argument*/GameObject_GetComponent_TisPlayerAttack_t3691797673_m1464963220_MethodInfo_var);
		NullCheck(L_8);
		Slider_t1468074762 * L_9 = L_8->get_specialAtkSlider_5();
		NullCheck(L_9);
		float L_10 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_9);
		NullCheck(L_6);
		L_6->set_special_3((((int32_t)((int32_t)L_10))));
		GlobalObjectManager_t849077355 * L_11 = ((GlobalObjectManager_t849077355_StaticFields*)GlobalObjectManager_t849077355_il2cpp_TypeInfo_var->static_fields)->get_Instance_12();
		GameObject_t4012695102 * L_12 = V_0;
		NullCheck(L_12);
		PlayerPickups_t2873926454 * L_13 = GameObject_GetComponent_TisPlayerPickups_t2873926454_m3462254187(L_12, /*hidden argument*/GameObject_GetComponent_TisPlayerPickups_t2873926454_m3462254187_MethodInfo_var);
		NullCheck(L_13);
		int32_t L_14 = L_13->get_numHits_11();
		NullCheck(L_11);
		L_11->set_numHits_4(L_14);
		GlobalObjectManager_t849077355 * L_15 = ((GlobalObjectManager_t849077355_StaticFields*)GlobalObjectManager_t849077355_il2cpp_TypeInfo_var->static_fields)->get_Instance_12();
		GameObject_t4012695102 * L_16 = V_1;
		NullCheck(L_16);
		GameTimer_t2994639443 * L_17 = GameObject_GetComponent_TisGameTimer_t2994639443_m1318047534(L_16, /*hidden argument*/GameObject_GetComponent_TisGameTimer_t2994639443_m1318047534_MethodInfo_var);
		NullCheck(L_17);
		DateTime_t339033936  L_18 = L_17->get_startTime_2();
		NullCheck(L_15);
		L_15->set_start_5(L_18);
		GlobalObjectManager_t849077355 * L_19 = ((GlobalObjectManager_t849077355_StaticFields*)GlobalObjectManager_t849077355_il2cpp_TypeInfo_var->static_fields)->get_Instance_12();
		GameObject_t4012695102 * L_20 = V_1;
		NullCheck(L_20);
		GameTimer_t2994639443 * L_21 = GameObject_GetComponent_TisGameTimer_t2994639443_m1318047534(L_20, /*hidden argument*/GameObject_GetComponent_TisGameTimer_t2994639443_m1318047534_MethodInfo_var);
		NullCheck(L_21);
		TimeSpan_t763862892  L_22 = L_21->get_elapsedTime_3();
		NullCheck(L_19);
		L_19->set_elapsed_6(L_22);
		return;
	}
}
// System.Void MenuManager::.ctor()
extern "C"  void MenuManager__ctor_m1757457917 (MenuManager_t3994435886 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuManager::Awake()
extern const MethodInfo* GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t3628549054_m1155306151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2127272705;
extern Il2CppCodeGenString* _stringLiteral1982636638;
extern const uint32_t MenuManager_Awake_m1995063136_MetadataUsageId;
extern "C"  void MenuManager_Awake_m1995063136 (MenuManager_t3994435886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuManager_Awake_m1995063136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2127272705, /*hidden argument*/NULL);
		NullCheck(L_0);
		GlobalObjectManager_t849077355 * L_1 = GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422(L_0, /*hidden argument*/GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422_MethodInfo_var);
		__this->set_persistentData_2(L_1);
		GameObject_t4012695102 * L_2 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral1982636638, /*hidden argument*/NULL);
		NullCheck(L_2);
		AudioSource_t3628549054 * L_3 = GameObject_GetComponent_TisAudioSource_t3628549054_m1155306151(L_2, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3628549054_m1155306151_MethodInfo_var);
		__this->set_gameBGSound_3(L_3);
		AudioSource_t3628549054 * L_4 = __this->get_gameBGSound_3();
		NullCheck(L_4);
		bool L_5 = AudioSource_get_isPlaying_m4213444423(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_006c;
		}
	}
	{
		AudioSource_t3628549054 * L_6 = __this->get_gameBGSound_3();
		NullCheck(L_6);
		AudioSource_Stop_m1454243038(L_6, /*hidden argument*/NULL);
		AudioSource_t3628549054 * L_7 = __this->get_gameBGSound_3();
		GlobalObjectManager_t849077355 * L_8 = __this->get_persistentData_2();
		NullCheck(L_8);
		float L_9 = L_8->get_startAudioPosition_9();
		NullCheck(L_7);
		AudioSource_set_timeSamples_m3782237206(L_7, (((int32_t)((int32_t)L_9))), /*hidden argument*/NULL);
		AudioSource_t3628549054 * L_10 = __this->get_gameBGSound_3();
		NullCheck(L_10);
		AudioSource_PlayDelayed_m2357889495(L_10, (0.05f), /*hidden argument*/NULL);
	}

IL_006c:
	{
		return;
	}
}
// System.Void MenuManager::Update()
extern "C"  void MenuManager_Update_m373482672 (MenuManager_t3994435886 * __this, const MethodInfo* method)
{
	{
		GlobalObjectManager_t849077355 * L_0 = __this->get_persistentData_2();
		AudioSource_t3628549054 * L_1 = __this->get_gameBGSound_3();
		NullCheck(L_1);
		int32_t L_2 = AudioSource_get_timeSamples_m1590595257(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_startAudioPosition_9((((float)((float)L_2))));
		return;
	}
}
// System.Void MenuManager::goGame()
extern Il2CppCodeGenString* _stringLiteral2390489;
extern const uint32_t MenuManager_goGame_m4256307329_MetadataUsageId;
extern "C"  void MenuManager_goGame_m4256307329 (MenuManager_t3994435886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuManager_goGame_m4256307329_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral2390489, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuManager::goSettings()
extern Il2CppCodeGenString* _stringLiteral1499275331;
extern const uint32_t MenuManager_goSettings_m1443924338_MetadataUsageId;
extern "C"  void MenuManager_goSettings_m1443924338 (MenuManager_t3994435886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuManager_goSettings_m1443924338_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral1499275331, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuManager::goLeaderboards()
extern Il2CppCodeGenString* _stringLiteral4256763741;
extern const uint32_t MenuManager_goLeaderboards_m1539624485_MetadataUsageId;
extern "C"  void MenuManager_goLeaderboards_m1539624485 (MenuManager_t3994435886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuManager_goLeaderboards_m1539624485_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral4256763741, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuManager::goInstructions()
extern Il2CppCodeGenString* _stringLiteral921696709;
extern const uint32_t MenuManager_goInstructions_m2482561908_MetadataUsageId;
extern "C"  void MenuManager_goInstructions_m2482561908 (MenuManager_t3994435886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuManager_goInstructions_m2482561908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral921696709, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuManager::goHome()
extern Il2CppCodeGenString* _stringLiteral2255103;
extern const uint32_t MenuManager_goHome_m2898478_MetadataUsageId;
extern "C"  void MenuManager_goHome_m2898478 (MenuManager_t3994435886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MenuManager_goHome_m2898478_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral2255103, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MouseLook::.ctor()
extern "C"  void MouseLook__ctor_m4146558247 (MouseLook_t2590096580 * __this, const MethodInfo* method)
{
	{
		__this->set_sensitivityX_3((15.0f));
		__this->set_sensitivityY_4((15.0f));
		__this->set_minimumX_5((-360.0f));
		__this->set_maximumX_6((360.0f));
		__this->set_minimumY_7((-60.0f));
		__this->set_maximumY_8((60.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MouseLook::Update()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2907718525;
extern Il2CppCodeGenString* _stringLiteral2907718526;
extern const uint32_t MouseLook_Update_m1421148870_MetadataUsageId;
extern "C"  void MouseLook_Update_m1421148870 (MouseLook_t2590096580 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MouseLook_Update_m1421148870_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = __this->get_axes_2();
		if (L_0)
		{
			goto IL_008e;
		}
	}
	{
		Transform_t284553113 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_localEulerAngles_m3489183428(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = (&V_1)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		float L_4 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718525, /*hidden argument*/NULL);
		float L_5 = __this->get_sensitivityX_3();
		V_0 = ((float)((float)L_3+(float)((float)((float)L_4*(float)L_5))));
		float L_6 = __this->get_rotationY_9();
		float L_7 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718526, /*hidden argument*/NULL);
		float L_8 = __this->get_sensitivityY_4();
		__this->set_rotationY_9(((float)((float)L_6+(float)((float)((float)L_7*(float)L_8)))));
		float L_9 = __this->get_rotationY_9();
		float L_10 = __this->get_minimumY_7();
		float L_11 = __this->get_maximumY_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		__this->set_rotationY_9(L_12);
		Transform_t284553113 * L_13 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		float L_14 = __this->get_rotationY_9();
		float L_15 = V_0;
		Vector3_t3525329789  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m2926210380(&L_16, ((-L_14)), L_15, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localEulerAngles_m3898859559(L_13, L_16, /*hidden argument*/NULL);
		goto IL_012f;
	}

IL_008e:
	{
		int32_t L_17 = __this->get_axes_2();
		if ((!(((uint32_t)L_17) == ((uint32_t)1))))
		{
			goto IL_00c5;
		}
	}
	{
		Transform_t284553113 * L_18 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		float L_19 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718525, /*hidden argument*/NULL);
		float L_20 = __this->get_sensitivityX_3();
		NullCheck(L_18);
		Transform_Rotate_m3498734243(L_18, (0.0f), ((float)((float)L_19*(float)L_20)), (0.0f), /*hidden argument*/NULL);
		goto IL_012f;
	}

IL_00c5:
	{
		float L_21 = __this->get_rotationY_9();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		float L_22 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718526, /*hidden argument*/NULL);
		float L_23 = __this->get_sensitivityY_4();
		__this->set_rotationY_9(((float)((float)L_21+(float)((float)((float)L_22*(float)L_23)))));
		float L_24 = __this->get_rotationY_9();
		float L_25 = __this->get_minimumY_7();
		float L_26 = __this->get_maximumY_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_27 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_24, L_25, L_26, /*hidden argument*/NULL);
		__this->set_rotationY_9(L_27);
		Transform_t284553113 * L_28 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		float L_29 = __this->get_rotationY_9();
		Transform_t284553113 * L_30 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_t3525329789  L_31 = Transform_get_localEulerAngles_m3489183428(L_30, /*hidden argument*/NULL);
		V_2 = L_31;
		float L_32 = (&V_2)->get_y_2();
		Vector3_t3525329789  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector3__ctor_m2926210380(&L_33, ((-L_29)), L_32, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_set_localEulerAngles_m3898859559(L_28, L_33, /*hidden argument*/NULL);
	}

IL_012f:
	{
		return;
	}
}
// System.Void MouseLook::Start()
extern const MethodInfo* Component_GetComponent_TisRigidbody_t1972007546_m2213165263_MethodInfo_var;
extern const uint32_t MouseLook_Start_m3093696039_MetadataUsageId;
extern "C"  void MouseLook_Start_m3093696039 (MouseLook_t2590096580 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MouseLook_Start_m3093696039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rigidbody_t1972007546 * L_0 = Component_GetComponent_TisRigidbody_t1972007546_m2213165263(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t1972007546_m2213165263_MethodInfo_var);
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Rigidbody_t1972007546 * L_2 = Component_GetComponent_TisRigidbody_t1972007546_m2213165263(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t1972007546_m2213165263_MethodInfo_var);
		NullCheck(L_2);
		Rigidbody_set_freezeRotation_m3989473889(L_2, (bool)1, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void MoveLevel::.ctor()
extern "C"  void MoveLevel__ctor_m2215801016 (MoveLevel_t3054455315 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoveLevel::OnTriggerEnter(UnityEngine.Collider)
extern const MethodInfo* GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral2127272705;
extern const uint32_t MoveLevel_OnTriggerEnter_m972740032_MetadataUsageId;
extern "C"  void MoveLevel_OnTriggerEnter_m972740032 (MoveLevel_t3054455315 * __this, Collider_t955670625 * ___collider0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MoveLevel_OnTriggerEnter_m972740032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t955670625 * L_0 = ___collider0;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m305486283(L_0, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		GameObject_t4012695102 * L_2 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2127272705, /*hidden argument*/NULL);
		NullCheck(L_2);
		GlobalObjectManager_t849077355 * L_3 = GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422(L_2, /*hidden argument*/GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422_MethodInfo_var);
		__this->set_persistentData_3(L_3);
		GlobalObjectManager_t849077355 * L_4 = __this->get_persistentData_3();
		NullCheck(L_4);
		GlobalObjectManager_SaveData_m3896486251(L_4, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_levelName_2();
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_003b:
	{
		return;
	}
}
// System.Void moveOBJ::.ctor()
extern "C"  void moveOBJ__ctor_m1230441157 (moveOBJ_t1243551590 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void moveOBJ::Update()
extern "C"  void moveOBJ_Update_m1215832296 (moveOBJ_t1243551590 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_Xtansform_2();
		float L_2 = __this->get_Ytansform_3();
		float L_3 = __this->get_Ztansform_4();
		NullCheck(L_0);
		Transform_Translate_m2900276092(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoveUpDown::.ctor()
extern "C"  void MoveUpDown__ctor_m1706657581 (MoveUpDown_t465175854 * __this, const MethodInfo* method)
{
	{
		__this->set_Move_2((bool)1);
		Vector3_t3525329789  L_0 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_MoveVector_3(L_0);
		__this->set_MoveRange_4((2.0f));
		__this->set_MoveSpeed_5((0.5f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoveUpDown::Start()
extern "C"  void MoveUpDown_Start_m653795373 (MoveUpDown_t465175854 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		__this->set_startPosition_6(L_1);
		return;
	}
}
// System.Void MoveUpDown::Update()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t MoveUpDown_Update_m3093639552_MetadataUsageId;
extern "C"  void MoveUpDown_Update_m3093639552 (MoveUpDown_t465175854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MoveUpDown_Update_m3093639552_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_Move_2();
		if (!L_0)
		{
			goto IL_0044;
		}
	}
	{
		Transform_t284553113 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = __this->get_startPosition_6();
		Vector3_t3525329789  L_3 = __this->get_MoveVector_3();
		float L_4 = __this->get_MoveRange_4();
		float L_5 = Time_get_timeSinceLevelLoad_m441028310(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = __this->get_MoveSpeed_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_7 = sinf(((float)((float)L_5*(float)L_6)));
		Vector3_t3525329789  L_8 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_3, ((float)((float)L_4*(float)L_7)), /*hidden argument*/NULL);
		Vector3_t3525329789  L_9 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_2, L_8, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_m3111394108(L_1, L_9, /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Void PickupEffects::.ctor()
extern "C"  void PickupEffects__ctor_m2787798373 (PickupEffects_t2342892230 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickupEffects::Start()
extern const MethodInfo* Component_GetComponent_TisAudioSource_t3628549054_m3821406207_MethodInfo_var;
extern const uint32_t PickupEffects_Start_m1734936165_MetadataUsageId;
extern "C"  void PickupEffects_Start_m1734936165 (PickupEffects_t2342892230 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PickupEffects_Start_m1734936165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t3628549054 * L_0 = Component_GetComponent_TisAudioSource_t3628549054_m3821406207(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3628549054_m3821406207_MethodInfo_var);
		__this->set_audio_2(L_0);
		return;
	}
}
// System.Void PickupEffects::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t PickupEffects_OnTriggerEnter_m820270131_MetadataUsageId;
extern "C"  void PickupEffects_OnTriggerEnter_m820270131 (PickupEffects_t2342892230 * __this, Collider_t955670625 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PickupEffects_OnTriggerEnter_m820270131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t955670625 * L_0 = ___hit0;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m305486283(L_0, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		AudioSource_t3628549054 * L_2 = __this->get_audio_2();
		AudioSource_t3628549054 * L_3 = __this->get_audio_2();
		NullCheck(L_3);
		AudioClip_t3714538611 * L_4 = AudioSource_get_clip_m2410835857(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AudioSource_PlayOneShot_m823779350(L_2, L_4, (0.4f), /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void PickUpManager::.ctor()
extern "C"  void PickUpManager__ctor_m623698746 (PickUpManager_t2721081681 * __this, const MethodInfo* method)
{
	{
		__this->set_spawnTime_4((2.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickUpManager::Start()
extern Il2CppCodeGenString* _stringLiteral1630653710;
extern const uint32_t PickUpManager_Start_m3865803834_MetadataUsageId;
extern "C"  void PickUpManager_Start_m3865803834 (PickUpManager_t2721081681 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PickUpManager_Start_m3865803834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_spawnTime_4();
		float L_1 = __this->get_spawnTime_4();
		MonoBehaviour_InvokeRepeating_m1115468640(__this, _stringLiteral1630653710, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickUpManager::SpawnItem()
extern "C"  void PickUpManager_SpawnItem_m3304709766 (PickUpManager_t2721081681 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		GameObject_t4012695102 * L_0 = __this->get_item_3();
		NullCheck(L_0);
		String_t* L_1 = GameObject_get_tag_m211612200(L_0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_2 = GameObject_FindWithTag_m3162815092(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_3 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		return;
	}

IL_001b:
	{
		Slider_t1468074762 * L_4 = __this->get_healthSldr_2();
		NullCheck(L_4);
		float L_5 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_4);
		if ((!(((float)L_5) <= ((float)(0.0f)))))
		{
			goto IL_0031;
		}
	}
	{
		return;
	}

IL_0031:
	{
		TransformU5BU5D_t3681339876* L_6 = __this->get_spawnPoints_5();
		NullCheck(L_6);
		int32_t L_7 = Random_Range_m75452833(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_7;
		GameObject_t4012695102 * L_8 = __this->get_item_3();
		TransformU5BU5D_t3681339876* L_9 = __this->get_spawnPoints_5();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		NullCheck(((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		Vector3_t3525329789  L_12 = Transform_get_position_m2211398607(((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_13 = __this->get_item_3();
		NullCheck(L_13);
		Transform_t284553113 * L_14 = GameObject_get_transform_m1278640159(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Quaternion_t1891715979  L_15 = Transform_get_rotation_m11483428(L_14, /*hidden argument*/NULL);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_8, L_12, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickUpRotate::.ctor()
extern "C"  void PickUpRotate__ctor_m818307396 (PickUpRotate_t521125687 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickUpRotate::FixedUpdate()
extern "C"  void PickUpRotate_FixedUpdate_m602618815 (PickUpRotate_t521125687 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_xRotation_2();
		float L_2 = __this->get_yRotation_3();
		float L_3 = __this->get_zRotation_4();
		Vector3_t3525329789  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m2926210380(&L_4, L_1, L_2, L_3, /*hidden argument*/NULL);
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Rotate_m637363399(L_0, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void player::.ctor()
extern "C"  void player__ctor_m1868723930 (player_t3309214433 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void player::Start()
extern "C"  void player_Start_m815861722 (player_t3309214433 * __this, const MethodInfo* method)
{
	{
		player_disableFX_m2175626194(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void player::OnGUI()
extern Il2CppClass* GUI_t1522956648_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2274292;
extern Il2CppCodeGenString* _stringLiteral989295305;
extern Il2CppCodeGenString* _stringLiteral989295306;
extern Il2CppCodeGenString* _stringLiteral3811972626;
extern Il2CppCodeGenString* _stringLiteral3811972627;
extern Il2CppCodeGenString* _stringLiteral3811972628;
extern Il2CppCodeGenString* _stringLiteral2587866;
extern Il2CppCodeGenString* _stringLiteral2688489;
extern Il2CppCodeGenString* _stringLiteral72563;
extern Il2CppCodeGenString* _stringLiteral1829962841;
extern Il2CppCodeGenString* _stringLiteral2478215502;
extern Il2CppCodeGenString* _stringLiteral1223718605;
extern Il2CppCodeGenString* _stringLiteral3314384088;
extern Il2CppCodeGenString* _stringLiteral1223328149;
extern Il2CppCodeGenString* _stringLiteral1223328150;
extern Il2CppCodeGenString* _stringLiteral1223328151;
extern const uint32_t player_OnGUI_m1364122580_MetadataUsageId;
extern "C"  void player_OnGUI_m1364122580 (player_t3309214433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (player_OnGUI_m1364122580_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t1525428817  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Rect__ctor_m3291325233(&L_0, (10.0f), (10.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_1 = GUI_Button_m885093907(NULL /*static, unused*/, L_0, _stringLiteral2274292, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005a;
		}
	}
	{
		player_disableFX_m2175626194(__this, /*hidden argument*/NULL);
		Animation_t350396337 * L_2 = __this->get_playerAni_3();
		AniClip_t806476588 * L_3 = __this->get_ani_2();
		NullCheck(L_3);
		AnimationClip_t57566497 * L_4 = L_3->get_idle_0();
		NullCheck(L_4);
		String_t* L_5 = Object_get_name_m3709440845(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		Animation_CrossFade_m2600902456(L_2, L_5, (0.1f), /*hidden argument*/NULL);
		__this->set_weaponVisible_5((bool)0);
		goto IL_0511;
	}

IL_005a:
	{
		Rect_t1525428817  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m3291325233(&L_6, (10.0f), (50.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_7 = GUI_Button_m885093907(NULL /*static, unused*/, L_6, _stringLiteral989295305, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00c0;
		}
	}
	{
		player_disableFX_m2175626194(__this, /*hidden argument*/NULL);
		Animation_t350396337 * L_8 = __this->get_playerAni_3();
		AniClip_t806476588 * L_9 = __this->get_ani_2();
		NullCheck(L_9);
		AnimationClip_t57566497 * L_10 = L_9->get_attack1_2();
		NullCheck(L_10);
		String_t* L_11 = Object_get_name_m3709440845(L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		Animation_CrossFade_m2600902456(L_8, L_11, (0.1f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_12 = __this->get_FXattack1_9();
		NullCheck(L_12);
		GameObject_SetActive_m3538205401(L_12, (bool)1, /*hidden argument*/NULL);
		__this->set_weaponVisible_5((bool)0);
		goto IL_0511;
	}

IL_00c0:
	{
		Rect_t1525428817  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Rect__ctor_m3291325233(&L_13, (10.0f), (80.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_14 = GUI_Button_m885093907(NULL /*static, unused*/, L_13, _stringLiteral989295306, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0126;
		}
	}
	{
		player_disableFX_m2175626194(__this, /*hidden argument*/NULL);
		Animation_t350396337 * L_15 = __this->get_playerAni_3();
		AniClip_t806476588 * L_16 = __this->get_ani_2();
		NullCheck(L_16);
		AnimationClip_t57566497 * L_17 = L_16->get_attack2_3();
		NullCheck(L_17);
		String_t* L_18 = Object_get_name_m3709440845(L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		Animation_CrossFade_m2600902456(L_15, L_18, (0.1f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_19 = __this->get_FXattack2_10();
		NullCheck(L_19);
		GameObject_SetActive_m3538205401(L_19, (bool)1, /*hidden argument*/NULL);
		__this->set_weaponVisible_5((bool)0);
		goto IL_0511;
	}

IL_0126:
	{
		Rect_t1525428817  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Rect__ctor_m3291325233(&L_20, (10.0f), (120.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_21 = GUI_Button_m885093907(NULL /*static, unused*/, L_20, _stringLiteral3811972626, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_018c;
		}
	}
	{
		player_disableFX_m2175626194(__this, /*hidden argument*/NULL);
		Animation_t350396337 * L_22 = __this->get_playerAni_3();
		AniClip_t806476588 * L_23 = __this->get_ani_2();
		NullCheck(L_23);
		AnimationClip_t57566497 * L_24 = L_23->get_skillA_4();
		NullCheck(L_24);
		String_t* L_25 = Object_get_name_m3709440845(L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		Animation_CrossFade_m2600902456(L_22, L_25, (0.1f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_26 = __this->get_FXskillA_12();
		NullCheck(L_26);
		GameObject_SetActive_m3538205401(L_26, (bool)1, /*hidden argument*/NULL);
		__this->set_weaponVisible_5((bool)0);
		goto IL_0511;
	}

IL_018c:
	{
		Rect_t1525428817  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Rect__ctor_m3291325233(&L_27, (10.0f), (150.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_28 = GUI_Button_m885093907(NULL /*static, unused*/, L_27, _stringLiteral3811972627, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_01f2;
		}
	}
	{
		player_disableFX_m2175626194(__this, /*hidden argument*/NULL);
		Animation_t350396337 * L_29 = __this->get_playerAni_3();
		AniClip_t806476588 * L_30 = __this->get_ani_2();
		NullCheck(L_30);
		AnimationClip_t57566497 * L_31 = L_30->get_skillB_5();
		NullCheck(L_31);
		String_t* L_32 = Object_get_name_m3709440845(L_31, /*hidden argument*/NULL);
		NullCheck(L_29);
		Animation_CrossFade_m2600902456(L_29, L_32, (0.1f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_33 = __this->get_FXskillB_13();
		NullCheck(L_33);
		GameObject_SetActive_m3538205401(L_33, (bool)1, /*hidden argument*/NULL);
		__this->set_weaponVisible_5((bool)0);
		goto IL_0511;
	}

IL_01f2:
	{
		Rect_t1525428817  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Rect__ctor_m3291325233(&L_34, (10.0f), (180.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_35 = GUI_Button_m885093907(NULL /*static, unused*/, L_34, _stringLiteral3811972628, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0258;
		}
	}
	{
		player_disableFX_m2175626194(__this, /*hidden argument*/NULL);
		Animation_t350396337 * L_36 = __this->get_playerAni_3();
		AniClip_t806476588 * L_37 = __this->get_ani_2();
		NullCheck(L_37);
		AnimationClip_t57566497 * L_38 = L_37->get_skillC_6();
		NullCheck(L_38);
		String_t* L_39 = Object_get_name_m3709440845(L_38, /*hidden argument*/NULL);
		NullCheck(L_36);
		Animation_CrossFade_m2600902456(L_36, L_39, (0.1f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_40 = __this->get_FXskillC_14();
		NullCheck(L_40);
		GameObject_SetActive_m3538205401(L_40, (bool)1, /*hidden argument*/NULL);
		__this->set_weaponVisible_5((bool)0);
		goto IL_0511;
	}

IL_0258:
	{
		Rect_t1525428817  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Rect__ctor_m3291325233(&L_41, (10.0f), (220.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_42 = GUI_Button_m885093907(NULL /*static, unused*/, L_41, _stringLiteral2587866, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_02be;
		}
	}
	{
		player_disableFX_m2175626194(__this, /*hidden argument*/NULL);
		Animation_t350396337 * L_43 = __this->get_playerAni_3();
		AniClip_t806476588 * L_44 = __this->get_ani_2();
		NullCheck(L_44);
		AnimationClip_t57566497 * L_45 = L_44->get_stun_7();
		NullCheck(L_45);
		String_t* L_46 = Object_get_name_m3709440845(L_45, /*hidden argument*/NULL);
		NullCheck(L_43);
		Animation_CrossFade_m2600902456(L_43, L_46, (0.1f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_47 = __this->get_FXsturn_15();
		NullCheck(L_47);
		GameObject_SetActive_m3538205401(L_47, (bool)1, /*hidden argument*/NULL);
		__this->set_weaponVisible_5((bool)0);
		goto IL_0511;
	}

IL_02be:
	{
		Rect_t1525428817  L_48;
		memset(&L_48, 0, sizeof(L_48));
		Rect__ctor_m3291325233(&L_48, (10.0f), (250.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_49 = GUI_Button_m885093907(NULL /*static, unused*/, L_48, _stringLiteral2688489, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_0318;
		}
	}
	{
		player_disableFX_m2175626194(__this, /*hidden argument*/NULL);
		Animation_t350396337 * L_50 = __this->get_playerAni_3();
		AniClip_t806476588 * L_51 = __this->get_ani_2();
		NullCheck(L_51);
		AnimationClip_t57566497 * L_52 = L_51->get_walk_1();
		NullCheck(L_52);
		String_t* L_53 = Object_get_name_m3709440845(L_52, /*hidden argument*/NULL);
		NullCheck(L_50);
		Animation_CrossFade_m2600902456(L_50, L_53, (0.1f), /*hidden argument*/NULL);
		__this->set_weaponVisible_5((bool)0);
		goto IL_0511;
	}

IL_0318:
	{
		Rect_t1525428817  L_54;
		memset(&L_54, 0, sizeof(L_54));
		Rect__ctor_m3291325233(&L_54, (10.0f), (280.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_55 = GUI_Button_m885093907(NULL /*static, unused*/, L_54, _stringLiteral72563, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_037e;
		}
	}
	{
		player_disableFX_m2175626194(__this, /*hidden argument*/NULL);
		Animation_t350396337 * L_56 = __this->get_playerAni_3();
		AniClip_t806476588 * L_57 = __this->get_ani_2();
		NullCheck(L_57);
		AnimationClip_t57566497 * L_58 = L_57->get_hit_8();
		NullCheck(L_58);
		String_t* L_59 = Object_get_name_m3709440845(L_58, /*hidden argument*/NULL);
		NullCheck(L_56);
		Animation_CrossFade_m2600902456(L_56, L_59, (0.1f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_60 = __this->get_FXhit_16();
		NullCheck(L_60);
		GameObject_SetActive_m3538205401(L_60, (bool)1, /*hidden argument*/NULL);
		__this->set_weaponVisible_5((bool)0);
		goto IL_0511;
	}

IL_037e:
	{
		Rect_t1525428817  L_61;
		memset(&L_61, 0, sizeof(L_61));
		Rect__ctor_m3291325233(&L_61, (10.0f), (320.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_62 = GUI_Button_m885093907(NULL /*static, unused*/, L_61, _stringLiteral1829962841, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_03de;
		}
	}
	{
		player_disableFX_m2175626194(__this, /*hidden argument*/NULL);
		__this->set_weaponVisible_5((bool)1);
		player_weaponSel_m420359606(__this, /*hidden argument*/NULL);
		Animation_t350396337 * L_63 = __this->get_playerAni_3();
		AniClip_t806476588 * L_64 = __this->get_ani_2();
		NullCheck(L_64);
		AnimationClip_t57566497 * L_65 = L_64->get_idle_weapon_9();
		NullCheck(L_65);
		String_t* L_66 = Object_get_name_m3709440845(L_65, /*hidden argument*/NULL);
		NullCheck(L_63);
		Animation_CrossFade_m2600902456(L_63, L_66, (0.1f), /*hidden argument*/NULL);
		goto IL_0511;
	}

IL_03de:
	{
		Rect_t1525428817  L_67;
		memset(&L_67, 0, sizeof(L_67));
		Rect__ctor_m3291325233(&L_67, (10.0f), (350.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_68 = GUI_Button_m885093907(NULL /*static, unused*/, L_67, _stringLiteral2478215502, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_043e;
		}
	}
	{
		player_disableFX_m2175626194(__this, /*hidden argument*/NULL);
		__this->set_weaponVisible_5((bool)1);
		player_weaponSel_m420359606(__this, /*hidden argument*/NULL);
		Animation_t350396337 * L_69 = __this->get_playerAni_3();
		AniClip_t806476588 * L_70 = __this->get_ani_2();
		NullCheck(L_70);
		AnimationClip_t57566497 * L_71 = L_70->get_walk_weapon_10();
		NullCheck(L_71);
		String_t* L_72 = Object_get_name_m3709440845(L_71, /*hidden argument*/NULL);
		NullCheck(L_69);
		Animation_CrossFade_m2600902456(L_69, L_72, (0.1f), /*hidden argument*/NULL);
		goto IL_0511;
	}

IL_043e:
	{
		Rect_t1525428817  L_73;
		memset(&L_73, 0, sizeof(L_73));
		Rect__ctor_m3291325233(&L_73, (10.0f), (380.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_74 = GUI_Button_m885093907(NULL /*static, unused*/, L_73, _stringLiteral1223718605, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_04aa;
		}
	}
	{
		player_disableFX_m2175626194(__this, /*hidden argument*/NULL);
		__this->set_weaponVisible_5((bool)1);
		player_weaponSel_m420359606(__this, /*hidden argument*/NULL);
		Animation_t350396337 * L_75 = __this->get_playerAni_3();
		AniClip_t806476588 * L_76 = __this->get_ani_2();
		NullCheck(L_76);
		AnimationClip_t57566497 * L_77 = L_76->get_attack_weapon_11();
		NullCheck(L_77);
		String_t* L_78 = Object_get_name_m3709440845(L_77, /*hidden argument*/NULL);
		NullCheck(L_75);
		Animation_CrossFade_m2600902456(L_75, L_78, (0.1f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_79 = __this->get_FXattack_weapon_11();
		NullCheck(L_79);
		GameObject_SetActive_m3538205401(L_79, (bool)1, /*hidden argument*/NULL);
		goto IL_0511;
	}

IL_04aa:
	{
		Rect_t1525428817  L_80;
		memset(&L_80, 0, sizeof(L_80));
		Rect__ctor_m3291325233(&L_80, (10.0f), (410.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_81 = GUI_Button_m885093907(NULL /*static, unused*/, L_80, _stringLiteral3314384088, /*hidden argument*/NULL);
		if (!L_81)
		{
			goto IL_0511;
		}
	}
	{
		player_disableFX_m2175626194(__this, /*hidden argument*/NULL);
		__this->set_weaponVisible_5((bool)1);
		player_weaponSel_m420359606(__this, /*hidden argument*/NULL);
		Animation_t350396337 * L_82 = __this->get_playerAni_3();
		AniClip_t806476588 * L_83 = __this->get_ani_2();
		NullCheck(L_83);
		AnimationClip_t57566497 * L_84 = L_83->get_hit_weapon_12();
		NullCheck(L_84);
		String_t* L_85 = Object_get_name_m3709440845(L_84, /*hidden argument*/NULL);
		NullCheck(L_82);
		Animation_CrossFade_m2600902456(L_82, L_85, (0.1f), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_86 = __this->get_FXhit_16();
		NullCheck(L_86);
		GameObject_SetActive_m3538205401(L_86, (bool)1, /*hidden argument*/NULL);
	}

IL_0511:
	{
		Rect_t1525428817  L_87;
		memset(&L_87, 0, sizeof(L_87));
		Rect__ctor_m3291325233(&L_87, (160.0f), (320.0f), (80.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_88 = GUI_Button_m885093907(NULL /*static, unused*/, L_87, _stringLiteral1223328149, /*hidden argument*/NULL);
		if (!L_88)
		{
			goto IL_054b;
		}
	}
	{
		__this->set_weaponId_4(0);
		player_weaponSel_m420359606(__this, /*hidden argument*/NULL);
		goto IL_05ba;
	}

IL_054b:
	{
		Rect_t1525428817  L_89;
		memset(&L_89, 0, sizeof(L_89));
		Rect__ctor_m3291325233(&L_89, (160.0f), (350.0f), (80.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_90 = GUI_Button_m885093907(NULL /*static, unused*/, L_89, _stringLiteral1223328150, /*hidden argument*/NULL);
		if (!L_90)
		{
			goto IL_0585;
		}
	}
	{
		__this->set_weaponId_4(1);
		player_weaponSel_m420359606(__this, /*hidden argument*/NULL);
		goto IL_05ba;
	}

IL_0585:
	{
		Rect_t1525428817  L_91;
		memset(&L_91, 0, sizeof(L_91));
		Rect__ctor_m3291325233(&L_91, (160.0f), (380.0f), (80.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_92 = GUI_Button_m885093907(NULL /*static, unused*/, L_91, _stringLiteral1223328151, /*hidden argument*/NULL);
		if (!L_92)
		{
			goto IL_05ba;
		}
	}
	{
		__this->set_weaponId_4(2);
		player_weaponSel_m420359606(__this, /*hidden argument*/NULL);
	}

IL_05ba:
	{
		return;
	}
}
// System.Void player::weaponSel()
extern "C"  void player_weaponSel_m420359606 (player_t3309214433 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_weaponVisible_5();
		if (!L_0)
		{
			goto IL_00a4;
		}
	}
	{
		int32_t L_1 = __this->get_weaponId_4();
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2 == 0)
		{
			goto IL_0029;
		}
		if (L_2 == 1)
		{
			goto IL_0052;
		}
		if (L_2 == 2)
		{
			goto IL_007b;
		}
	}
	{
		goto IL_00a4;
	}

IL_0029:
	{
		GameObject_t4012695102 * L_3 = __this->get_weapon1_6();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_4 = __this->get_weapon2_7();
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_5 = __this->get_weapon3_8();
		NullCheck(L_5);
		GameObject_SetActive_m3538205401(L_5, (bool)0, /*hidden argument*/NULL);
		goto IL_00a4;
	}

IL_0052:
	{
		GameObject_t4012695102 * L_6 = __this->get_weapon1_6();
		NullCheck(L_6);
		GameObject_SetActive_m3538205401(L_6, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_7 = __this->get_weapon2_7();
		NullCheck(L_7);
		GameObject_SetActive_m3538205401(L_7, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_8 = __this->get_weapon3_8();
		NullCheck(L_8);
		GameObject_SetActive_m3538205401(L_8, (bool)0, /*hidden argument*/NULL);
		goto IL_00a4;
	}

IL_007b:
	{
		GameObject_t4012695102 * L_9 = __this->get_weapon1_6();
		NullCheck(L_9);
		GameObject_SetActive_m3538205401(L_9, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_10 = __this->get_weapon2_7();
		NullCheck(L_10);
		GameObject_SetActive_m3538205401(L_10, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_11 = __this->get_weapon3_8();
		NullCheck(L_11);
		GameObject_SetActive_m3538205401(L_11, (bool)1, /*hidden argument*/NULL);
		goto IL_00a4;
	}

IL_00a4:
	{
		return;
	}
}
// System.Void player::disableFX()
extern "C"  void player_disableFX_m2175626194 (player_t3309214433 * __this, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = __this->get_weapon1_6();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_1 = __this->get_weapon2_7();
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_2 = __this->get_weapon3_8();
		NullCheck(L_2);
		GameObject_SetActive_m3538205401(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_3 = __this->get_FXattack1_9();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_4 = __this->get_FXattack2_10();
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_5 = __this->get_FXsturn_15();
		NullCheck(L_5);
		GameObject_SetActive_m3538205401(L_5, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_6 = __this->get_FXattack_weapon_11();
		NullCheck(L_6);
		GameObject_SetActive_m3538205401(L_6, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_7 = __this->get_FXskillA_12();
		NullCheck(L_7);
		GameObject_SetActive_m3538205401(L_7, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_8 = __this->get_FXskillB_13();
		NullCheck(L_8);
		GameObject_SetActive_m3538205401(L_8, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_9 = __this->get_FXskillC_14();
		NullCheck(L_9);
		GameObject_SetActive_m3538205401(L_9, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_10 = __this->get_FXhit_16();
		NullCheck(L_10);
		GameObject_SetActive_m3538205401(L_10, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void player/AniClip::.ctor()
extern "C"  void AniClip__ctor_m2105141341 (AniClip_t806476588 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerAttack::.ctor()
extern "C"  void PlayerAttack__ctor_m576635922 (PlayerAttack_t3691797673 * __this, const MethodInfo* method)
{
	{
		__this->set_damagePerHit_6(((int32_t)20));
		__this->set_timeBetweenAttacks_7((1.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerAttack::Awake()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t3628549054_m3821406207_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisPlayerEffects_t1618007745_m3926634072_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisPlayerPickups_t2873926454_m1245811139_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2591932237;
extern const uint32_t PlayerAttack_Awake_m814241141_MetadataUsageId;
extern "C"  void PlayerAttack_Awake_m814241141 (PlayerAttack_t3691797673 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerAttack_Awake_m814241141_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t3628549054 * L_0 = Component_GetComponent_TisAudioSource_t3628549054_m3821406207(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3628549054_m3821406207_MethodInfo_var);
		__this->set_audio_2(L_0);
		StringU5BU5D_t2956870243* L_1 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, _stringLiteral2591932237);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2591932237);
		int32_t L_2 = LayerMask_GetMask_m500679236(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->set_hitableMask_10(L_2);
		PlayerEffects_t1618007745 * L_3 = Component_GetComponent_TisPlayerEffects_t1618007745_m3926634072(__this, /*hidden argument*/Component_GetComponent_TisPlayerEffects_t1618007745_m3926634072_MethodInfo_var);
		__this->set_playerEffects_14(L_3);
		PlayerPickups_t2873926454 * L_4 = Component_GetComponent_TisPlayerPickups_t2873926454_m1245811139(__this, /*hidden argument*/Component_GetComponent_TisPlayerPickups_t2873926454_m1245811139_MethodInfo_var);
		__this->set_playerPickups_15(L_4);
		return;
	}
}
// System.Void PlayerAttack::Update()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1971575400;
extern Il2CppCodeGenString* _stringLiteral755082602;
extern Il2CppCodeGenString* _stringLiteral1191894995;
extern Il2CppCodeGenString* _stringLiteral989295305;
extern Il2CppCodeGenString* _stringLiteral3688700993;
extern Il2CppCodeGenString* _stringLiteral3811974580;
extern const uint32_t PlayerAttack_Update_m2422706491_MetadataUsageId;
extern "C"  void PlayerAttack_Update_m2422706491 (PlayerAttack_t3691797673 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerAttack_Update_m2422706491_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_timer_8();
		float L_1 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timer_8(((float)((float)L_0+(float)L_1)));
		PlayerPickups_t2873926454 * L_2 = __this->get_playerPickups_15();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_numHits_11();
		if ((((int32_t)L_3) > ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		PlayerPickups_t2873926454 * L_4 = __this->get_playerPickups_15();
		NullCheck(L_4);
		PlayerPickups_DestroyWeapon_m1172587337(L_4, /*hidden argument*/NULL);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_5 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)97), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		float L_6 = __this->get_timer_8();
		float L_7 = __this->get_timeBetweenAttacks_7();
		if ((((float)L_6) >= ((float)L_7)))
		{
			goto IL_006b;
		}
	}

IL_004b:
	{
		bool L_8 = CnInputManager_GetButtonDown_m3486119458(NULL /*static, unused*/, _stringLiteral1971575400, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00dd;
		}
	}
	{
		float L_9 = __this->get_timer_8();
		float L_10 = __this->get_timeBetweenAttacks_7();
		if ((!(((float)L_9) >= ((float)L_10))))
		{
			goto IL_00dd;
		}
	}

IL_006b:
	{
		__this->set_isAttacking_11((bool)1);
		AudioClip_t3714538611 * L_11 = __this->get_normalClip_4();
		PlayerAttack_PlayAtkSound_m399909350(__this, L_11, /*hidden argument*/NULL);
		PlayerPickups_t2873926454 * L_12 = __this->get_playerPickups_15();
		NullCheck(L_12);
		GameObject_t4012695102 * L_13 = L_12->get_weapon_5();
		NullCheck(L_13);
		String_t* L_14 = GameObject_get_tag_m211612200(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_14, _stringLiteral755082602, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00ba;
		}
	}
	{
		__this->set_damagePerHit_6(((int32_t)50));
		PlayerEffects_t1618007745 * L_16 = __this->get_playerEffects_14();
		NullCheck(L_16);
		PlayerEffects_showNormalAttackEffect_m3980383595(L_16, _stringLiteral1191894995, /*hidden argument*/NULL);
		goto IL_00d2;
	}

IL_00ba:
	{
		__this->set_damagePerHit_6(((int32_t)20));
		PlayerEffects_t1618007745 * L_17 = __this->get_playerEffects_14();
		NullCheck(L_17);
		PlayerEffects_showNormalAttackEffect_m3980383595(L_17, _stringLiteral989295305, /*hidden argument*/NULL);
	}

IL_00d2:
	{
		PlayerAttack_Attack_m1638851322(__this, /*hidden argument*/NULL);
		goto IL_018c;
	}

IL_00dd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_18 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)115), /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_010f;
		}
	}
	{
		float L_19 = __this->get_timer_8();
		float L_20 = __this->get_timeBetweenAttacks_7();
		if ((!(((float)L_19) >= ((float)L_20))))
		{
			goto IL_010f;
		}
	}
	{
		Slider_t1468074762 * L_21 = __this->get_specialAtkSlider_5();
		NullCheck(L_21);
		float L_22 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_21);
		if ((((float)L_22) > ((float)(0.0f))))
		{
			goto IL_0144;
		}
	}

IL_010f:
	{
		bool L_23 = CnInputManager_GetButtonDown_m3486119458(NULL /*static, unused*/, _stringLiteral3688700993, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_018c;
		}
	}
	{
		float L_24 = __this->get_timer_8();
		float L_25 = __this->get_timeBetweenAttacks_7();
		if ((!(((float)L_24) >= ((float)L_25))))
		{
			goto IL_018c;
		}
	}
	{
		Slider_t1468074762 * L_26 = __this->get_specialAtkSlider_5();
		NullCheck(L_26);
		float L_27 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_26);
		if ((!(((float)L_27) > ((float)(0.0f)))))
		{
			goto IL_018c;
		}
	}

IL_0144:
	{
		__this->set_isAttacking_11((bool)1);
		AudioClip_t3714538611 * L_28 = __this->get_specialClip_3();
		PlayerAttack_PlayAtkSound_m399909350(__this, L_28, /*hidden argument*/NULL);
		__this->set_damagePerHit_6(((int32_t)100));
		PlayerEffects_t1618007745 * L_29 = __this->get_playerEffects_14();
		NullCheck(L_29);
		PlayerEffects_showSpecialAttackEffect_m2043161349(L_29, _stringLiteral3811974580, /*hidden argument*/NULL);
		Slider_t1468074762 * L_30 = __this->get_specialAtkSlider_5();
		Slider_t1468074762 * L_31 = L_30;
		NullCheck(L_31);
		float L_32 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_31);
		NullCheck(L_31);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_31, ((float)((float)L_32-(float)(20.0f))));
		PlayerAttack_Attack_m1638851322(__this, /*hidden argument*/NULL);
	}

IL_018c:
	{
		bool L_33 = __this->get_isAttacking_11();
		if (!L_33)
		{
			goto IL_01ed;
		}
	}
	{
		float L_34 = __this->get_atkEffectTimer_9();
		float L_35 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_atkEffectTimer_9(((float)((float)L_34+(float)L_35)));
		float L_36 = __this->get_atkEffectTimer_9();
		if ((!(((float)L_36) > ((float)(1.0f)))))
		{
			goto IL_01ed;
		}
	}
	{
		__this->set_atkEffectTimer_9((0.0f));
		__this->set_isAttacking_11((bool)0);
		PlayerEffects_t1618007745 * L_37 = __this->get_playerEffects_14();
		NullCheck(L_37);
		GameObject_t4012695102 * L_38 = L_37->get_normalAttackEffect_4();
		NullCheck(L_38);
		GameObject_SetActive_m3538205401(L_38, (bool)0, /*hidden argument*/NULL);
		PlayerEffects_t1618007745 * L_39 = __this->get_playerEffects_14();
		NullCheck(L_39);
		GameObject_t4012695102 * L_40 = L_39->get_specialAttackEffect_5();
		NullCheck(L_40);
		GameObject_SetActive_m3538205401(L_40, (bool)0, /*hidden argument*/NULL);
	}

IL_01ed:
	{
		return;
	}
}
// System.Void PlayerAttack::PlayAtkSound(UnityEngine.AudioClip)
extern "C"  void PlayerAttack_PlayAtkSound_m399909350 (PlayerAttack_t3691797673 * __this, AudioClip_t3714538611 * ___clip0, const MethodInfo* method)
{
	{
		AudioSource_t3628549054 * L_0 = __this->get_audio_2();
		AudioClip_t3714538611 * L_1 = ___clip0;
		NullCheck(L_0);
		AudioSource_PlayOneShot_m823779350(L_0, L_1, (0.5f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerAttack::Attack()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisEnemyHealth_t1417584612_m2559288533_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral755082602;
extern const uint32_t PlayerAttack_Attack_m1638851322_MetadataUsageId;
extern "C"  void PlayerAttack_Attack_m1638851322 (PlayerAttack_t3691797673 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerAttack_Attack_m1638851322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EnemyHealth_t1417584612 * V_0 = NULL;
	{
		PlayerPickups_t2873926454 * L_0 = __this->get_playerPickups_15();
		NullCheck(L_0);
		GameObject_t4012695102 * L_1 = L_0->get_weapon_5();
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m211612200(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_2, _stringLiteral755082602, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		PlayerPickups_t2873926454 * L_4 = __this->get_playerPickups_15();
		PlayerPickups_t2873926454 * L_5 = L_4;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_numHits_11();
		NullCheck(L_5);
		L_5->set_numHits_11(((int32_t)((int32_t)L_6-(int32_t)1)));
	}

IL_0032:
	{
		__this->set_timer_8((0.0f));
		Ray_t1522967639 * L_7 = __this->get_address_of_shootRay_12();
		Transform_t284553113 * L_8 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t3525329789  L_9 = Transform_get_position_m2211398607(L_8, /*hidden argument*/NULL);
		Ray_set_origin_m1191170081(L_7, L_9, /*hidden argument*/NULL);
		Ray_t1522967639 * L_10 = __this->get_address_of_shootRay_12();
		Transform_t284553113 * L_11 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t3525329789  L_12 = Transform_get_forward_m877665793(L_11, /*hidden argument*/NULL);
		Ray_set_direction_m2672750186(L_10, L_12, /*hidden argument*/NULL);
		Ray_t1522967639  L_13 = __this->get_shootRay_12();
		RaycastHit_t46221527 * L_14 = __this->get_address_of_shootHit_13();
		int32_t L_15 = __this->get_hitableMask_10();
		bool L_16 = Physics_Raycast_m1600345803(NULL /*static, unused*/, L_13, L_14, (100.0f), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c8;
		}
	}
	{
		RaycastHit_t46221527 * L_17 = __this->get_address_of_shootHit_13();
		float L_18 = RaycastHit_get_distance_m800944203(L_17, /*hidden argument*/NULL);
		if ((!(((float)L_18) < ((float)(4.0f)))))
		{
			goto IL_00c8;
		}
	}
	{
		RaycastHit_t46221527 * L_19 = __this->get_address_of_shootHit_13();
		Collider_t955670625 * L_20 = RaycastHit_get_collider_m3116882274(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		EnemyHealth_t1417584612 * L_21 = Component_GetComponent_TisEnemyHealth_t1417584612_m2559288533(L_20, /*hidden argument*/Component_GetComponent_TisEnemyHealth_t1417584612_m2559288533_MethodInfo_var);
		V_0 = L_21;
		EnemyHealth_t1417584612 * L_22 = V_0;
		bool L_23 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_22, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00c8;
		}
	}
	{
		EnemyHealth_t1417584612 * L_24 = V_0;
		int32_t L_25 = __this->get_damagePerHit_6();
		NullCheck(L_24);
		EnemyHealth_HitDamage_m819917400(L_24, L_25, /*hidden argument*/NULL);
	}

IL_00c8:
	{
		return;
	}
}
// System.Void PlayerEffects::.ctor()
extern "C"  void PlayerEffects__ctor_m2486735818 (PlayerEffects_t1618007745 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerEffects::Awake()
extern const MethodInfo* Component_GetComponent_TisAnimation_t350396337_m2546983788_MethodInfo_var;
extern const uint32_t PlayerEffects_Awake_m2724341037_MetadataUsageId;
extern "C"  void PlayerEffects_Awake_m2724341037 (PlayerEffects_t1618007745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerEffects_Awake_m2724341037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = __this->get_hitEffect_2();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_1 = __this->get_stunEffect_3();
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_2 = __this->get_normalAttackEffect_4();
		NullCheck(L_2);
		GameObject_SetActive_m3538205401(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_3 = __this->get_specialAttackEffect_5();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)0, /*hidden argument*/NULL);
		Animation_t350396337 * L_4 = Component_GetComponent_TisAnimation_t350396337_m2546983788(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t350396337_m2546983788_MethodInfo_var);
		__this->set_playerAnimation_6(L_4);
		return;
	}
}
// System.Void PlayerEffects::showIdleEffect(System.String)
extern "C"  void PlayerEffects_showIdleEffect_m3987524294 (PlayerEffects_t1618007745 * __this, String_t* ___typeOfIdle0, const MethodInfo* method)
{
	{
		Animation_t350396337 * L_0 = __this->get_playerAnimation_6();
		String_t* L_1 = ___typeOfIdle0;
		NullCheck(L_0);
		bool L_2 = Animation_IsPlaying_m1471515685(L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		Animation_t350396337 * L_3 = __this->get_playerAnimation_6();
		String_t* L_4 = ___typeOfIdle0;
		NullCheck(L_3);
		Animation_Play_m1881292147(L_3, L_4, 4, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void PlayerEffects::showWalkingEffect(System.String)
extern "C"  void PlayerEffects_showWalkingEffect_m3667812717 (PlayerEffects_t1618007745 * __this, String_t* ___typeOfWalk0, const MethodInfo* method)
{
	{
		Animation_t350396337 * L_0 = __this->get_playerAnimation_6();
		String_t* L_1 = ___typeOfWalk0;
		NullCheck(L_0);
		bool L_2 = Animation_IsPlaying_m1471515685(L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		Animation_t350396337 * L_3 = __this->get_playerAnimation_6();
		String_t* L_4 = ___typeOfWalk0;
		NullCheck(L_3);
		Animation_Play_m1881292147(L_3, L_4, 4, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void PlayerEffects::showStunEffect()
extern Il2CppCodeGenString* _stringLiteral2587866;
extern const uint32_t PlayerEffects_showStunEffect_m2027784354_MetadataUsageId;
extern "C"  void PlayerEffects_showStunEffect_m2027784354 (PlayerEffects_t1618007745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerEffects_showStunEffect_m2027784354_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = __this->get_stunEffect_3();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		Transform_t284553113 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m2926210380(&L_2, (0.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_Rotate_m637363399(L_1, L_2, /*hidden argument*/NULL);
		Animation_t350396337 * L_3 = __this->get_playerAnimation_6();
		NullCheck(L_3);
		Animation_Play_m900498501(L_3, _stringLiteral2587866, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerEffects::showHitEffect()
extern "C"  void PlayerEffects_showHitEffect_m751940815 (PlayerEffects_t1618007745 * __this, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = __this->get_hitEffect_2();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerEffects::showNormalAttackEffect(System.String)
extern "C"  void PlayerEffects_showNormalAttackEffect_m3980383595 (PlayerEffects_t1618007745 * __this, String_t* ___typeOfAttack0, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = __this->get_normalAttackEffect_4();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		Animation_t350396337 * L_1 = __this->get_playerAnimation_6();
		String_t* L_2 = ___typeOfAttack0;
		NullCheck(L_1);
		Animation_Play_m1881292147(L_1, L_2, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerEffects::showSpecialAttackEffect(System.String)
extern "C"  void PlayerEffects_showSpecialAttackEffect_m2043161349 (PlayerEffects_t1618007745 * __this, String_t* ___typeOfAttack0, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = __this->get_specialAttackEffect_5();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		Animation_t350396337 * L_1 = __this->get_playerAnimation_6();
		String_t* L_2 = ___typeOfAttack0;
		NullCheck(L_1);
		Animation_Play_m1881292147(L_1, L_2, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerHealth::.ctor()
extern "C"  void PlayerHealth__ctor_m43779710 (PlayerHealth_t3877793981 * __this, const MethodInfo* method)
{
	{
		__this->set_startHealth_2(((int32_t)100));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerHealth::Start()
extern const MethodInfo* Component_GetComponent_TisAudioSource_t3628549054_m3821406207_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisPlayerMovement_t3827129040_m3500385589_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisPlayerEffects_t1618007745_m3926634072_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2127272705;
extern const uint32_t PlayerHealth_Start_m3285884798_MetadataUsageId;
extern "C"  void PlayerHealth_Start_m3285884798 (PlayerHealth_t3877793981 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerHealth_Start_m3285884798_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t3628549054 * L_0 = Component_GetComponent_TisAudioSource_t3628549054_m3821406207(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3628549054_m3821406207_MethodInfo_var);
		__this->set_audio_7(L_0);
		PlayerMovement_t3827129040 * L_1 = Component_GetComponent_TisPlayerMovement_t3827129040_m3500385589(__this, /*hidden argument*/Component_GetComponent_TisPlayerMovement_t3827129040_m3500385589_MethodInfo_var);
		__this->set_playerMovement_8(L_1);
		PlayerEffects_t1618007745 * L_2 = Component_GetComponent_TisPlayerEffects_t1618007745_m3926634072(__this, /*hidden argument*/Component_GetComponent_TisPlayerEffects_t1618007745_m3926634072_MethodInfo_var);
		__this->set_playerEffects_9(L_2);
		GameObject_t4012695102 * L_3 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2127272705, /*hidden argument*/NULL);
		NullCheck(L_3);
		GlobalObjectManager_t849077355 * L_4 = GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422(L_3, /*hidden argument*/GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422_MethodInfo_var);
		__this->set_persistentData_10(L_4);
		GlobalObjectManager_t849077355 * L_5 = __this->get_persistentData_10();
		NullCheck(L_5);
		bool L_6 = L_5->get_isLoaded_7();
		if (!L_6)
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_7 = __this->get_startHealth_2();
		__this->set_currentHealth_3(L_7);
		Slider_t1468074762 * L_8 = __this->get_healthSldr_4();
		int32_t L_9 = __this->get_currentHealth_3();
		NullCheck(L_8);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_8, (((float)((float)L_9))));
	}

IL_0067:
	{
		return;
	}
}
// System.Void PlayerHealth::TookDamage(System.Int32)
extern "C"  void PlayerHealth_TookDamage_m4294364413 (PlayerHealth_t3877793981 * __this, int32_t ___amount0, const MethodInfo* method)
{
	{
		AudioSource_t3628549054 * L_0 = __this->get_audio_7();
		NullCheck(L_0);
		bool L_1 = AudioSource_get_isPlaying_m4213444423(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		AudioSource_t3628549054 * L_2 = __this->get_audio_7();
		AudioClip_t3714538611 * L_3 = __this->get_audioClip_5();
		NullCheck(L_2);
		AudioSource_PlayOneShot_m823779350(L_2, L_3, (1.0f), /*hidden argument*/NULL);
	}

IL_0026:
	{
		int32_t L_4 = __this->get_currentHealth_3();
		int32_t L_5 = ___amount0;
		__this->set_currentHealth_3(((int32_t)((int32_t)L_4-(int32_t)L_5)));
		Slider_t1468074762 * L_6 = __this->get_healthSldr_4();
		int32_t L_7 = __this->get_currentHealth_3();
		NullCheck(L_6);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_6, (((float)((float)L_7))));
		int32_t L_8 = __this->get_currentHealth_3();
		if ((((int32_t)L_8) > ((int32_t)0)))
		{
			goto IL_0079;
		}
	}
	{
		bool L_9 = __this->get_hasDied_11();
		if (L_9)
		{
			goto IL_0079;
		}
	}
	{
		AudioSource_t3628549054 * L_10 = __this->get_audio_7();
		AudioClip_t3714538611 * L_11 = __this->get_deathAudioClip_6();
		NullCheck(L_10);
		AudioSource_PlayOneShot_m823779350(L_10, L_11, (1.0f), /*hidden argument*/NULL);
		PlayerHealth_Die_m284211932(__this, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void PlayerHealth::Die()
extern "C"  void PlayerHealth_Die_m284211932 (PlayerHealth_t3877793981 * __this, const MethodInfo* method)
{
	{
		__this->set_hasDied_11((bool)1);
		GlobalObjectManager_t849077355 * L_0 = __this->get_persistentData_10();
		NullCheck(L_0);
		L_0->set_isDead_8((bool)1);
		PlayerMovement_t3827129040 * L_1 = __this->get_playerMovement_8();
		NullCheck(L_1);
		Behaviour_set_enabled_m2046806933(L_1, (bool)0, /*hidden argument*/NULL);
		PlayerEffects_t1618007745 * L_2 = __this->get_playerEffects_9();
		NullCheck(L_2);
		PlayerEffects_showStunEffect_m2027784354(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerMovement::.ctor()
extern "C"  void PlayerMovement__ctor_m537500619 (PlayerMovement_t3827129040 * __this, const MethodInfo* method)
{
	{
		__this->set_speed_2((4.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerMovement::Awake()
extern const MethodInfo* Component_GetComponent_TisAnimation_t350396337_m2546983788_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCharacterController_t2029520850_m3352851511_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisPlayerEffects_t1618007745_m3926634072_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisPlayerPickups_t2873926454_m1245811139_MethodInfo_var;
extern const uint32_t PlayerMovement_Awake_m775105838_MetadataUsageId;
extern "C"  void PlayerMovement_Awake_m775105838 (PlayerMovement_t3827129040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerMovement_Awake_m775105838_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animation_t350396337 * L_0 = Component_GetComponent_TisAnimation_t350396337_m2546983788(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t350396337_m2546983788_MethodInfo_var);
		__this->set_playerAnimation_6(L_0);
		CharacterController_t2029520850 * L_1 = Component_GetComponent_TisCharacterController_t2029520850_m3352851511(__this, /*hidden argument*/Component_GetComponent_TisCharacterController_t2029520850_m3352851511_MethodInfo_var);
		__this->set_playerCharacterController_7(L_1);
		PlayerEffects_t1618007745 * L_2 = Component_GetComponent_TisPlayerEffects_t1618007745_m3926634072(__this, /*hidden argument*/Component_GetComponent_TisPlayerEffects_t1618007745_m3926634072_MethodInfo_var);
		__this->set_playerEffects_9(L_2);
		PlayerPickups_t2873926454 * L_3 = Component_GetComponent_TisPlayerPickups_t2873926454_m1245811139(__this, /*hidden argument*/Component_GetComponent_TisPlayerPickups_t2873926454_m1245811139_MethodInfo_var);
		__this->set_playerPickups_10(L_3);
		return;
	}
}
// System.Void PlayerMovement::FixedUpdate()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1971575400;
extern const uint32_t PlayerMovement_FixedUpdate_m1673803014_MetadataUsageId;
extern "C"  void PlayerMovement_FixedUpdate_m1673803014 (PlayerMovement_t3827129040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerMovement_FixedUpdate_m1673803014_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)97), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		bool L_1 = CnInputManager_GetButtonDown_m3486119458(NULL /*static, unused*/, _stringLiteral1971575400, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0021;
		}
	}

IL_001b:
	{
		PlayerMovement_Move_m2717087658(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void PlayerMovement::Move()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3381094468;
extern Il2CppCodeGenString* _stringLiteral2375469974;
extern Il2CppCodeGenString* _stringLiteral755082602;
extern Il2CppCodeGenString* _stringLiteral1191894995;
extern Il2CppCodeGenString* _stringLiteral3705303239;
extern Il2CppCodeGenString* _stringLiteral3811974580;
extern Il2CppCodeGenString* _stringLiteral989295305;
extern Il2CppCodeGenString* _stringLiteral2274292;
extern Il2CppCodeGenString* _stringLiteral2479288626;
extern Il2CppCodeGenString* _stringLiteral2688489;
extern const uint32_t PlayerMovement_Move_m2717087658_MetadataUsageId;
extern "C"  void PlayerMovement_Move_m2717087658 (PlayerMovement_t3827129040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerMovement_Move_m2717087658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Quaternion_t1891715979  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = CnInputManager_GetAxisRaw_m692810837(NULL /*static, unused*/, _stringLiteral3381094468, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_00ef;
		}
	}
	{
		float L_1 = CnInputManager_GetAxisRaw_m692810837(NULL /*static, unused*/, _stringLiteral2375469974, /*hidden argument*/NULL);
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_00ef;
		}
	}
	{
		PlayerPickups_t2873926454 * L_2 = __this->get_playerPickups_10();
		NullCheck(L_2);
		GameObject_t4012695102 * L_3 = L_2->get_weapon_5();
		NullCheck(L_3);
		String_t* L_4 = GameObject_get_tag_m211612200(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_4, _stringLiteral755082602, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_009b;
		}
	}
	{
		Animation_t350396337 * L_6 = __this->get_playerAnimation_6();
		NullCheck(L_6);
		bool L_7 = Animation_IsPlaying_m1471515685(L_6, _stringLiteral1191894995, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0096;
		}
	}
	{
		Animation_t350396337 * L_8 = __this->get_playerAnimation_6();
		NullCheck(L_8);
		bool L_9 = Animation_IsPlaying_m1471515685(L_8, _stringLiteral3705303239, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0096;
		}
	}
	{
		Animation_t350396337 * L_10 = __this->get_playerAnimation_6();
		NullCheck(L_10);
		bool L_11 = Animation_IsPlaying_m1471515685(L_10, _stringLiteral3811974580, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0096;
		}
	}
	{
		PlayerEffects_t1618007745 * L_12 = __this->get_playerEffects_9();
		NullCheck(L_12);
		PlayerEffects_showIdleEffect_m3987524294(L_12, _stringLiteral3705303239, /*hidden argument*/NULL);
	}

IL_0096:
	{
		goto IL_00ea;
	}

IL_009b:
	{
		Animation_t350396337 * L_13 = __this->get_playerAnimation_6();
		NullCheck(L_13);
		bool L_14 = Animation_IsPlaying_m1471515685(L_13, _stringLiteral989295305, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_00ea;
		}
	}
	{
		Animation_t350396337 * L_15 = __this->get_playerAnimation_6();
		NullCheck(L_15);
		bool L_16 = Animation_IsPlaying_m1471515685(L_15, _stringLiteral2274292, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00ea;
		}
	}
	{
		Animation_t350396337 * L_17 = __this->get_playerAnimation_6();
		NullCheck(L_17);
		bool L_18 = Animation_IsPlaying_m1471515685(L_17, _stringLiteral3811974580, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_00ea;
		}
	}
	{
		PlayerEffects_t1618007745 * L_19 = __this->get_playerEffects_9();
		NullCheck(L_19);
		PlayerEffects_showIdleEffect_m3987524294(L_19, _stringLiteral2274292, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		goto IL_01b1;
	}

IL_00ef:
	{
		PlayerPickups_t2873926454 * L_20 = __this->get_playerPickups_10();
		NullCheck(L_20);
		GameObject_t4012695102 * L_21 = L_20->get_weapon_5();
		NullCheck(L_21);
		String_t* L_22 = GameObject_get_tag_m211612200(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_22, _stringLiteral755082602, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0162;
		}
	}
	{
		Animation_t350396337 * L_24 = __this->get_playerAnimation_6();
		NullCheck(L_24);
		bool L_25 = Animation_IsPlaying_m1471515685(L_24, _stringLiteral1191894995, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_015d;
		}
	}
	{
		Animation_t350396337 * L_26 = __this->get_playerAnimation_6();
		NullCheck(L_26);
		bool L_27 = Animation_IsPlaying_m1471515685(L_26, _stringLiteral2479288626, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_015d;
		}
	}
	{
		Animation_t350396337 * L_28 = __this->get_playerAnimation_6();
		NullCheck(L_28);
		bool L_29 = Animation_IsPlaying_m1471515685(L_28, _stringLiteral3811974580, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_015d;
		}
	}
	{
		PlayerEffects_t1618007745 * L_30 = __this->get_playerEffects_9();
		NullCheck(L_30);
		PlayerEffects_showWalkingEffect_m3667812717(L_30, _stringLiteral2479288626, /*hidden argument*/NULL);
	}

IL_015d:
	{
		goto IL_01b1;
	}

IL_0162:
	{
		Animation_t350396337 * L_31 = __this->get_playerAnimation_6();
		NullCheck(L_31);
		bool L_32 = Animation_IsPlaying_m1471515685(L_31, _stringLiteral989295305, /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_01b1;
		}
	}
	{
		Animation_t350396337 * L_33 = __this->get_playerAnimation_6();
		NullCheck(L_33);
		bool L_34 = Animation_IsPlaying_m1471515685(L_33, _stringLiteral2688489, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_01b1;
		}
	}
	{
		Animation_t350396337 * L_35 = __this->get_playerAnimation_6();
		NullCheck(L_35);
		bool L_36 = Animation_IsPlaying_m1471515685(L_35, _stringLiteral3811974580, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_01b1;
		}
	}
	{
		PlayerEffects_t1618007745 * L_37 = __this->get_playerEffects_9();
		NullCheck(L_37);
		PlayerEffects_showWalkingEffect_m3667812717(L_37, _stringLiteral2688489, /*hidden argument*/NULL);
	}

IL_01b1:
	{
		float L_38 = CnInputManager_GetAxis_m2810903397(NULL /*static, unused*/, _stringLiteral2375469974, /*hidden argument*/NULL);
		float L_39 = CnInputManager_GetAxis_m2810903397(NULL /*static, unused*/, _stringLiteral3381094468, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_38/(float)L_39));
		float L_40 = CnInputManager_GetAxis_m2810903397(NULL /*static, unused*/, _stringLiteral3381094468, /*hidden argument*/NULL);
		float L_41 = CnInputManager_GetAxis_m2810903397(NULL /*static, unused*/, _stringLiteral2375469974, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_1), ((float)((float)L_40+(float)(0.001f))), (0.0f), L_41, /*hidden argument*/NULL);
		Vector3_t3525329789  L_42 = V_1;
		Quaternion_t1891715979  L_43 = Quaternion_LookRotation_m1257501645(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		V_2 = L_43;
		float L_44 = V_0;
		if ((((float)L_44) >= ((float)(0.1f))))
		{
			goto IL_020a;
		}
	}
	{
		float L_45 = V_0;
		if ((!(((float)L_45) <= ((float)(0.1f)))))
		{
			goto IL_0231;
		}
	}

IL_020a:
	{
		Transform_t284553113 * L_46 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_47 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		Quaternion_t1891715979  L_48 = Transform_get_rotation_m11483428(L_47, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_49 = V_2;
		float L_50 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_51 = Quaternion_Slerp_m844700366(NULL /*static, unused*/, L_48, L_49, ((float)((float)(20.0f)*(float)L_50)), /*hidden argument*/NULL);
		NullCheck(L_46);
		Transform_set_rotation_m1525803229(L_46, L_51, /*hidden argument*/NULL);
	}

IL_0231:
	{
		CharacterController_t2029520850 * L_52 = __this->get_playerCharacterController_7();
		Vector3_t3525329789  L_53 = V_1;
		float L_54 = __this->get_speed_2();
		Vector3_t3525329789  L_55 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		float L_56 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_57 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		NullCheck(L_52);
		CharacterController_Move_m3043020731(L_52, L_57, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPickups::.ctor()
extern "C"  void PlayerPickups__ctor_m2071132917 (PlayerPickups_t2873926454 * __this, const MethodInfo* method)
{
	{
		__this->set_numHits_11(((int32_t)30));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPickups::Start()
extern const MethodInfo* Component_GetComponent_TisAudioSource_t3628549054_m3821406207_MethodInfo_var;
extern const uint32_t PlayerPickups_Start_m1018270709_MetadataUsageId;
extern "C"  void PlayerPickups_Start_m1018270709 (PlayerPickups_t2873926454 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPickups_Start_m1018270709_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t3628549054 * L_0 = Component_GetComponent_TisAudioSource_t3628549054_m3821406207(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3628549054_m3821406207_MethodInfo_var);
		__this->set_audio_8(L_0);
		return;
	}
}
// System.Void PlayerPickups::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* PlayerPickups_t2873926454_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2157571708;
extern Il2CppCodeGenString* _stringLiteral3951155353;
extern Il2CppCodeGenString* _stringLiteral1028781042;
extern Il2CppCodeGenString* _stringLiteral4129016078;
extern Il2CppCodeGenString* _stringLiteral1827865969;
extern Il2CppCodeGenString* _stringLiteral1372847986;
extern Il2CppCodeGenString* _stringLiteral2140409230;
extern Il2CppCodeGenString* _stringLiteral3904006641;
extern const uint32_t PlayerPickups_OnTriggerEnter_m1011052707_MetadataUsageId;
extern "C"  void PlayerPickups_OnTriggerEnter_m1011052707 (PlayerPickups_t2873926454 * __this, Collider_t955670625 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPickups_OnTriggerEnter_m1011052707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t190145395 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		Collider_t955670625 * L_0 = ___hit0;
		NullCheck(L_0);
		GameObject_t4012695102 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m211612200(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		if (!L_3)
		{
			goto IL_0133;
		}
	}
	{
		Dictionary_2_t190145395 * L_4 = ((PlayerPickups_t2873926454_StaticFields*)PlayerPickups_t2873926454_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_12();
		if (L_4)
		{
			goto IL_0065;
		}
	}
	{
		Dictionary_2_t190145395 * L_5 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_5, 5, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_1 = L_5;
		Dictionary_2_t190145395 * L_6 = V_1;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_6, _stringLiteral2157571708, 0);
		Dictionary_2_t190145395 * L_7 = V_1;
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_7, _stringLiteral3951155353, 1);
		Dictionary_2_t190145395 * L_8 = V_1;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_8, _stringLiteral1028781042, 2);
		Dictionary_2_t190145395 * L_9 = V_1;
		NullCheck(L_9);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_9, _stringLiteral4129016078, 3);
		Dictionary_2_t190145395 * L_10 = V_1;
		NullCheck(L_10);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_10, _stringLiteral1827865969, 4);
		Dictionary_2_t190145395 * L_11 = V_1;
		((PlayerPickups_t2873926454_StaticFields*)PlayerPickups_t2873926454_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map0_12(L_11);
	}

IL_0065:
	{
		Dictionary_2_t190145395 * L_12 = ((PlayerPickups_t2873926454_StaticFields*)PlayerPickups_t2873926454_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_12();
		String_t* L_13 = V_0;
		NullCheck(L_12);
		bool L_14 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(35 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_12, L_13, (&V_2));
		if (!L_14)
		{
			goto IL_0133;
		}
	}
	{
		int32_t L_15 = V_2;
		if (L_15 == 0)
		{
			goto IL_0096;
		}
		if (L_15 == 1)
		{
			goto IL_00b9;
		}
		if (L_15 == 2)
		{
			goto IL_00dc;
		}
		if (L_15 == 3)
		{
			goto IL_00f9;
		}
		if (L_15 == 4)
		{
			goto IL_0116;
		}
	}
	{
		goto IL_0133;
	}

IL_0096:
	{
		Slider_t1468074762 * L_16 = __this->get_healthSlider_3();
		Slider_t1468074762 * L_17 = L_16;
		NullCheck(L_17);
		float L_18 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_17);
		NullCheck(L_17);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_17, ((float)((float)L_18+(float)(20.0f))));
		Collider_t955670625 * L_19 = ___hit0;
		PlayerPickups_RemoveObject_m688797319(__this, L_19, /*hidden argument*/NULL);
		goto IL_0133;
	}

IL_00b9:
	{
		Slider_t1468074762 * L_20 = __this->get_specialSlider_4();
		Slider_t1468074762 * L_21 = L_20;
		NullCheck(L_21);
		float L_22 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_21);
		NullCheck(L_21);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_21, ((float)((float)L_22+(float)(20.0f))));
		Collider_t955670625 * L_23 = ___hit0;
		PlayerPickups_RemoveObject_m688797319(__this, L_23, /*hidden argument*/NULL);
		goto IL_0133;
	}

IL_00dc:
	{
		GameObject_t4012695102 * L_24 = __this->get_destroyedEffect_7();
		NullCheck(L_24);
		GameObject_SetActive_m3538205401(L_24, (bool)0, /*hidden argument*/NULL);
		Collider_t955670625 * L_25 = ___hit0;
		PlayerPickups_AssignWeapon_m2718080467(__this, _stringLiteral1372847986, L_25, /*hidden argument*/NULL);
		goto IL_0133;
	}

IL_00f9:
	{
		GameObject_t4012695102 * L_26 = __this->get_destroyedEffect_7();
		NullCheck(L_26);
		GameObject_SetActive_m3538205401(L_26, (bool)0, /*hidden argument*/NULL);
		Collider_t955670625 * L_27 = ___hit0;
		PlayerPickups_AssignWeapon_m2718080467(__this, _stringLiteral2140409230, L_27, /*hidden argument*/NULL);
		goto IL_0133;
	}

IL_0116:
	{
		GameObject_t4012695102 * L_28 = __this->get_destroyedEffect_7();
		NullCheck(L_28);
		GameObject_SetActive_m3538205401(L_28, (bool)0, /*hidden argument*/NULL);
		Collider_t955670625 * L_29 = ___hit0;
		PlayerPickups_AssignWeapon_m2718080467(__this, _stringLiteral3904006641, L_29, /*hidden argument*/NULL);
		goto IL_0133;
	}

IL_0133:
	{
		return;
	}
}
// System.Void PlayerPickups::RemoveObject(UnityEngine.Collider)
extern const MethodInfo* GameObject_GetComponentInChildren_TisMeshRenderer_t1217738301_m3118202586_MethodInfo_var;
extern const uint32_t PlayerPickups_RemoveObject_m688797319_MetadataUsageId;
extern "C"  void PlayerPickups_RemoveObject_m688797319 (PlayerPickups_t2873926454 * __this, Collider_t955670625 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPickups_RemoveObject_m688797319_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t955670625 * L_0 = ___hit0;
		NullCheck(L_0);
		GameObject_t4012695102 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t284553113 * L_2 = GameObject_get_transform_m1278640159(L_1, /*hidden argument*/NULL);
		Vector3_t3525329789  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, (0.0f), (-5.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m3111394108(L_2, L_3, /*hidden argument*/NULL);
		Collider_t955670625 * L_4 = ___hit0;
		NullCheck(L_4);
		GameObject_t4012695102 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		MeshRenderer_t1217738301 * L_6 = GameObject_GetComponentInChildren_TisMeshRenderer_t1217738301_m3118202586(L_5, /*hidden argument*/GameObject_GetComponentInChildren_TisMeshRenderer_t1217738301_m3118202586_MethodInfo_var);
		NullCheck(L_6);
		Renderer_set_enabled_m2514140131(L_6, (bool)0, /*hidden argument*/NULL);
		Collider_t955670625 * L_7 = ___hit0;
		NullCheck(L_7);
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899(L_7, /*hidden argument*/NULL);
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_8, (2.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPickups::AssignWeapon(System.String,UnityEngine.Collider)
extern const MethodInfo* GameObject_GetComponentInChildren_TisMeshRenderer_t1217738301_m3118202586_MethodInfo_var;
extern const uint32_t PlayerPickups_AssignWeapon_m2718080467_MetadataUsageId;
extern "C"  void PlayerPickups_AssignWeapon_m2718080467 (PlayerPickups_t2873926454 * __this, String_t* ___tag0, Collider_t955670625 * ___hit1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPickups_AssignWeapon_m2718080467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t955670625 * L_0 = ___hit1;
		NullCheck(L_0);
		GameObject_t4012695102 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t284553113 * L_2 = GameObject_get_transform_m1278640159(L_1, /*hidden argument*/NULL);
		Vector3_t3525329789  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, (0.0f), (-5.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m3111394108(L_2, L_3, /*hidden argument*/NULL);
		Collider_t955670625 * L_4 = ___hit1;
		NullCheck(L_4);
		GameObject_t4012695102 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		MeshRenderer_t1217738301 * L_6 = GameObject_GetComponentInChildren_TisMeshRenderer_t1217738301_m3118202586(L_5, /*hidden argument*/GameObject_GetComponentInChildren_TisMeshRenderer_t1217738301_m3118202586_MethodInfo_var);
		NullCheck(L_6);
		Renderer_set_enabled_m2514140131(L_6, (bool)0, /*hidden argument*/NULL);
		Collider_t955670625 * L_7 = ___hit1;
		NullCheck(L_7);
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899(L_7, /*hidden argument*/NULL);
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_8, (2.0f), /*hidden argument*/NULL);
		String_t* L_9 = ___tag0;
		PlayerPickups_ToggleWeapon_m2520552739(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPickups::DestroyWeapon()
extern const MethodInfo* GameObject_GetComponent_TisParticleSystem_t56787138_m2450916872_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral755082602;
extern const uint32_t PlayerPickups_DestroyWeapon_m1172587337_MetadataUsageId;
extern "C"  void PlayerPickups_DestroyWeapon_m1172587337 (PlayerPickups_t2873926454 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPickups_DestroyWeapon_m1172587337_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ParticleSystem_t56787138 * V_0 = NULL;
	{
		GameObject_t4012695102 * L_0 = __this->get_destroyedEffect_7();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_1 = __this->get_destroyedEffect_7();
		NullCheck(L_1);
		ParticleSystem_t56787138 * L_2 = GameObject_GetComponent_TisParticleSystem_t56787138_m2450916872(L_1, /*hidden argument*/GameObject_GetComponent_TisParticleSystem_t56787138_m2450916872_MethodInfo_var);
		V_0 = L_2;
		ParticleSystem_t56787138 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = ParticleSystem_get_isPlaying_m184625675(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0039;
		}
	}
	{
		AudioSource_t3628549054 * L_5 = __this->get_audio_8();
		AudioClip_t3714538611 * L_6 = __this->get_destroyAudio_2();
		NullCheck(L_5);
		AudioSource_PlayOneShot_m823779350(L_5, L_6, (1.0f), /*hidden argument*/NULL);
	}

IL_0039:
	{
		ParticleSystem_t56787138 * L_7 = V_0;
		NullCheck(L_7);
		ParticleSystem_Play_m1237476160(L_7, /*hidden argument*/NULL);
		__this->set_numHits_11(3);
		PlayerPickups_ToggleWeapon_m2520552739(__this, _stringLiteral755082602, /*hidden argument*/NULL);
		__this->set_numHits_11(((int32_t)30));
		return;
	}
}
// System.Void PlayerPickups::ToggleWeapon(System.String)
extern const MethodInfo* GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910_MethodInfo_var;
extern const uint32_t PlayerPickups_ToggleWeapon_m2520552739_MetadataUsageId;
extern "C"  void PlayerPickups_ToggleWeapon_m2520552739 (PlayerPickups_t2873926454 * __this, String_t* ___tag0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPickups_ToggleWeapon_m2520552739_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = __this->get_weapon_5();
		__this->set_oldWeapon_6(L_0);
		GameObject_t4012695102 * L_1 = __this->get_oldWeapon_6();
		NullCheck(L_1);
		MeshRenderer_t1217738301 * L_2 = GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910(L_1, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910_MethodInfo_var);
		__this->set_oldWeaponRenderer_10(L_2);
		MeshRenderer_t1217738301 * L_3 = __this->get_oldWeaponRenderer_10();
		NullCheck(L_3);
		Renderer_set_enabled_m2514140131(L_3, (bool)0, /*hidden argument*/NULL);
		String_t* L_4 = ___tag0;
		GameObject_t4012695102 * L_5 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_weapon_5(L_5);
		GameObject_t4012695102 * L_6 = __this->get_weapon_5();
		NullCheck(L_6);
		MeshRenderer_t1217738301 * L_7 = GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910(L_6, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910_MethodInfo_var);
		__this->set_weaponRenderer_9(L_7);
		MeshRenderer_t1217738301 * L_8 = __this->get_weaponRenderer_9();
		NullCheck(L_8);
		Renderer_set_enabled_m2514140131(L_8, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PoqXert.MessageBox.DialogResultMethod::.ctor(System.Object,System.IntPtr)
extern "C"  void DialogResultMethod__ctor_m3207538458 (DialogResultMethod_t1789391825 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PoqXert.MessageBox.DialogResultMethod::Invoke(System.Int32,PoqXert.MessageBox.DialogResult)
extern "C"  void DialogResultMethod_Invoke_m3390017135 (DialogResultMethod_t1789391825 * __this, int32_t ___id0, int32_t ___result1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DialogResultMethod_Invoke_m3390017135((DialogResultMethod_t1789391825 *)__this->get_prev_9(),___id0, ___result1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___id0, int32_t ___result1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___id0, ___result1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___id0, int32_t ___result1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___id0, ___result1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_DialogResultMethod_t1789391825(Il2CppObject* delegate, int32_t ___id0, int32_t ___result1)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___id0' to native representation

	// Marshaling of parameter '___result1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___id0, ___result1);

	// Marshaling cleanup of parameter '___id0' native representation

	// Marshaling cleanup of parameter '___result1' native representation

}
// System.IAsyncResult PoqXert.MessageBox.DialogResultMethod::BeginInvoke(System.Int32,PoqXert.MessageBox.DialogResult,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* DialogResult_t3964159632_il2cpp_TypeInfo_var;
extern const uint32_t DialogResultMethod_BeginInvoke_m283683788_MetadataUsageId;
extern "C"  Il2CppObject * DialogResultMethod_BeginInvoke_m283683788 (DialogResultMethod_t1789391825 * __this, int32_t ___id0, int32_t ___result1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DialogResultMethod_BeginInvoke_m283683788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___id0);
	__d_args[1] = Box(DialogResult_t3964159632_il2cpp_TypeInfo_var, &___result1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void PoqXert.MessageBox.DialogResultMethod::EndInvoke(System.IAsyncResult)
extern "C"  void DialogResultMethod_EndInvoke_m219559466 (DialogResultMethod_t1789391825 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void PoqXert.MessageBox.MsgBox::.ctor()
extern Il2CppClass* List_1_t2720345355_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m968611477_MethodInfo_var;
extern const uint32_t MsgBox__ctor_m4017087300_MetadataUsageId;
extern "C"  void MsgBox__ctor_m4017087300 (MsgBox_t1468720533 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MsgBox__ctor_m4017087300_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2720345355 * L_0 = (List_1_t2720345355 *)il2cpp_codegen_object_new(List_1_t2720345355_il2cpp_TypeInfo_var);
		List_1__ctor_m968611477(L_0, /*hidden argument*/List_1__ctor_m968611477_MethodInfo_var);
		__this->set__customStyles_17(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PoqXert.MessageBox.MsgBox::.cctor()
extern "C"  void MsgBox__cctor_m3788525801 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// PoqXert.MessageBox.MsgBox PoqXert.MessageBox.MsgBox::get_prefab()
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern const MethodInfo* Resources_Load_TisMsgBox_t1468720533_m1631435255_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2242590891;
extern const uint32_t MsgBox_get_prefab_m2374698177_MetadataUsageId;
extern "C"  MsgBox_t1468720533 * MsgBox_get_prefab_m2374698177 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MsgBox_get_prefab_m2374698177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_0 = ((MsgBox_t1468720533_StaticFields*)MsgBox_t1468720533_il2cpp_TypeInfo_var->static_fields)->get__prefab_18();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		MsgBox_t1468720533 * L_2 = Resources_Load_TisMsgBox_t1468720533_m1631435255(NULL /*static, unused*/, _stringLiteral2242590891, /*hidden argument*/Resources_Load_TisMsgBox_t1468720533_m1631435255_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		((MsgBox_t1468720533_StaticFields*)MsgBox_t1468720533_il2cpp_TypeInfo_var->static_fields)->set__prefab_18(L_2);
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_3 = ((MsgBox_t1468720533_StaticFields*)MsgBox_t1468720533_il2cpp_TypeInfo_var->static_fields)->get__prefab_18();
		return L_3;
	}
}
// PoqXert.MessageBox.MsgBox PoqXert.MessageBox.MsgBox::get_instance()
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern const uint32_t MsgBox_get_instance_m3950348434_MetadataUsageId;
extern "C"  MsgBox_t1468720533 * MsgBox_get_instance_m3950348434 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MsgBox_get_instance_m3950348434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_0 = ((MsgBox_t1468720533_StaticFields*)MsgBox_t1468720533_il2cpp_TypeInfo_var->static_fields)->get__lastMsgBox_19();
		return L_0;
	}
}
// System.Boolean PoqXert.MessageBox.MsgBox::get_isOpen()
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern const uint32_t MsgBox_get_isOpen_m2684764489_MetadataUsageId;
extern "C"  bool MsgBox_get_isOpen_m2684764489 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MsgBox_get_isOpen_m2684764489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_0 = ((MsgBox_t1468720533_StaticFields*)MsgBox_t1468720533_il2cpp_TypeInfo_var->static_fields)->get__lastMsgBox_19();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean PoqXert.MessageBox.MsgBox::get_isModal()
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern const uint32_t MsgBox_get_isModal_m4113405872_MetadataUsageId;
extern "C"  bool MsgBox_get_isModal_m4113405872 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MsgBox_get_isModal_m4113405872_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		bool L_0 = MsgBox_get_isOpen_m2684764489(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_1 = ((MsgBox_t1468720533_StaticFields*)MsgBox_t1468720533_il2cpp_TypeInfo_var->static_fields)->get__lastMsgBox_19();
		NullCheck(L_1);
		GameObject_t4012695102 * L_2 = L_1->get__blockPanel_3();
		NullCheck(L_2);
		bool L_3 = GameObject_get_activeSelf_m3858025161(L_2, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Void PoqXert.MessageBox.MsgBox::ButtonClickEvent(System.Int32)
extern "C"  void MsgBox_ButtonClickEvent_m2153297941 (MsgBox_t1468720533 * __this, int32_t ___btn0, const MethodInfo* method)
{
	{
		DialogResultMethod_t1789391825 * L_0 = __this->get_calledMethod_21();
		int32_t L_1 = __this->get_messageID_20();
		int32_t L_2 = ___btn0;
		NullCheck(L_0);
		DialogResultMethod_Invoke_m3390017135(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PoqXert.MessageBox.MsgBox::BuildMessageBox(System.Int32,System.String,System.String,PoqXert.MessageBox.MsgBoxButtons,PoqXert.MessageBox.MSGBoxStyle,PoqXert.MessageBox.DialogResultMethod,System.Boolean,System.String,System.String,System.String)
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisText_t3286458198_m582470501_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2556;
extern Il2CppCodeGenString* _stringLiteral2011110042;
extern Il2CppCodeGenString* _stringLiteral2529;
extern Il2CppCodeGenString* _stringLiteral88775;
extern const uint32_t MsgBox_BuildMessageBox_m96384347_MetadataUsageId;
extern "C"  void MsgBox_BuildMessageBox_m96384347 (MsgBox_t1468720533 * __this, int32_t ___id0, String_t* ___mess1, String_t* ___caption2, int32_t ___btns3, MSGBoxStyle_t1923386386 * ___style4, DialogResultMethod_t1789391825 * ___method5, bool ___modal6, String_t* ___btnText07, String_t* ___btnText18, String_t* ___btnText29, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MsgBox_BuildMessageBox_m96384347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RectTransform_t3317474837 * V_1 = NULL;
	Text_t3286458198 * V_2 = NULL;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t V_5 = 0;
	Text_t3286458198 * G_B4_0 = NULL;
	Text_t3286458198 * G_B3_0 = NULL;
	String_t* G_B5_0 = NULL;
	Text_t3286458198 * G_B5_1 = NULL;
	Text_t3286458198 * G_B8_0 = NULL;
	Text_t3286458198 * G_B7_0 = NULL;
	String_t* G_B9_0 = NULL;
	Text_t3286458198 * G_B9_1 = NULL;
	Text_t3286458198 * G_B11_0 = NULL;
	Text_t3286458198 * G_B10_0 = NULL;
	String_t* G_B12_0 = NULL;
	Text_t3286458198 * G_B12_1 = NULL;
	Text_t3286458198 * G_B15_0 = NULL;
	Text_t3286458198 * G_B14_0 = NULL;
	String_t* G_B16_0 = NULL;
	Text_t3286458198 * G_B16_1 = NULL;
	Text_t3286458198 * G_B18_0 = NULL;
	Text_t3286458198 * G_B17_0 = NULL;
	String_t* G_B19_0 = NULL;
	Text_t3286458198 * G_B19_1 = NULL;
	Text_t3286458198 * G_B22_0 = NULL;
	Text_t3286458198 * G_B21_0 = NULL;
	String_t* G_B23_0 = NULL;
	Text_t3286458198 * G_B23_1 = NULL;
	Text_t3286458198 * G_B25_0 = NULL;
	Text_t3286458198 * G_B24_0 = NULL;
	String_t* G_B26_0 = NULL;
	Text_t3286458198 * G_B26_1 = NULL;
	Text_t3286458198 * G_B28_0 = NULL;
	Text_t3286458198 * G_B27_0 = NULL;
	String_t* G_B29_0 = NULL;
	Text_t3286458198 * G_B29_1 = NULL;
	{
		int32_t L_0 = ___id0;
		__this->set_messageID_20(L_0);
		GameObject_t4012695102 * L_1 = __this->get__blockPanel_3();
		bool L_2 = ___modal6;
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, L_2, /*hidden argument*/NULL);
		DialogResultMethod_t1789391825 * L_3 = ___method5;
		__this->set_calledMethod_21(L_3);
		Text_t3286458198 * L_4 = __this->get__captionText_4();
		String_t* L_5 = ___caption2;
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_5);
		Text_t3286458198 * L_6 = __this->get__mainText_5();
		String_t* L_7 = ___mess1;
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_7);
		Image_t3354615620 * L_8 = __this->get__captionImg_6();
		MSGBoxStyle_t1923386386 * L_9 = ___style4;
		NullCheck(L_9);
		Color_t1588175760  L_10 = MSGBoxStyle_get_captionColor_m334910080(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Graphic_set_color_m1311501487(L_8, L_10, /*hidden argument*/NULL);
		Image_t3354615620 * L_11 = __this->get__backgroundImg_7();
		MSGBoxStyle_t1923386386 * L_12 = ___style4;
		NullCheck(L_12);
		Color_t1588175760  L_13 = MSGBoxStyle_get_backgroundColor_m3445433268(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Graphic_set_color_m1311501487(L_11, L_13, /*hidden argument*/NULL);
		Image_t3354615620 * L_14 = __this->get__btnYesImg_9();
		MSGBoxStyle_t1923386386 * L_15 = ___style4;
		NullCheck(L_15);
		Color_t1588175760  L_16 = MSGBoxStyle_get_btnYesColor_m1144113751(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Graphic_set_color_m1311501487(L_14, L_16, /*hidden argument*/NULL);
		Image_t3354615620 * L_17 = __this->get__btnNoImg_10();
		MSGBoxStyle_t1923386386 * L_18 = ___style4;
		NullCheck(L_18);
		Color_t1588175760  L_19 = MSGBoxStyle_get_btnNoColor_m437349321(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Graphic_set_color_m1311501487(L_17, L_19, /*hidden argument*/NULL);
		Image_t3354615620 * L_20 = __this->get__btnCancelImg_11();
		MSGBoxStyle_t1923386386 * L_21 = ___style4;
		NullCheck(L_21);
		Color_t1588175760  L_22 = MSGBoxStyle_get_btnCancelColor_m4250514928(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		Graphic_set_color_m1311501487(L_20, L_22, /*hidden argument*/NULL);
		Image_t3354615620 * L_23 = __this->get__iconImg_8();
		MSGBoxStyle_t1923386386 * L_24 = ___style4;
		NullCheck(L_24);
		Sprite_t4006040370 * L_25 = MSGBoxStyle_get_icon_m2425158486(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		Image_set_sprite_m572551402(L_23, L_25, /*hidden argument*/NULL);
		Text_t3286458198 * L_26 = __this->get__captionText_4();
		NullCheck(L_26);
		float L_27 = VirtFuncInvoker0< float >::Invoke(69 /* System.Single UnityEngine.UI.Text::get_preferredWidth() */, L_26);
		Text_t3286458198 * L_28 = __this->get__mainText_5();
		NullCheck(L_28);
		float L_29 = VirtFuncInvoker0< float >::Invoke(69 /* System.Single UnityEngine.UI.Text::get_preferredWidth() */, L_28);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_30 = Mathf_Max_m3923796455(NULL /*static, unused*/, L_27, ((float)((float)(150.0f)+(float)L_29)), /*hidden argument*/NULL);
		int32_t L_31 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_32 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_30, (256.0f), (((float)((float)L_31))), /*hidden argument*/NULL);
		Text_t3286458198 * L_33 = __this->get__mainText_5();
		NullCheck(L_33);
		float L_34 = VirtFuncInvoker0< float >::Invoke(72 /* System.Single UnityEngine.UI.Text::get_preferredHeight() */, L_33);
		int32_t L_35 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_36 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_34, (256.0f), (((float)((float)L_35))), /*hidden argument*/NULL);
		Vector2__ctor_m1517109030((&V_0), L_32, L_36, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_37 = __this->get__mainPanel_2();
		float L_38 = (&V_0)->get_x_1();
		NullCheck(L_37);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_37, 0, L_38, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_39 = __this->get__mainPanel_2();
		float L_40 = (&V_0)->get_y_2();
		NullCheck(L_39);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_39, 1, L_40, /*hidden argument*/NULL);
		int32_t L_41 = ___btns3;
		V_5 = L_41;
		int32_t L_42 = V_5;
		if (L_42 == 0)
		{
			goto IL_0139;
		}
		if (L_42 == 1)
		{
			goto IL_01f4;
		}
		if (L_42 == 2)
		{
			goto IL_0337;
		}
		if (L_42 == 3)
		{
			goto IL_047d;
		}
	}
	{
		goto IL_064d;
	}

IL_0139:
	{
		Image_t3354615620 * L_43 = __this->get__btnYesImg_9();
		NullCheck(L_43);
		RectTransform_t3317474837 * L_44 = Graphic_get_rectTransform_m4017371950(L_43, /*hidden argument*/NULL);
		V_1 = L_44;
		RectTransform_t3317474837 * L_45 = V_1;
		Vector2_t3525329788  L_46;
		memset(&L_46, 0, sizeof(L_46));
		Vector2__ctor_m1517109030(&L_46, (-10.0f), (10.0f), /*hidden argument*/NULL);
		NullCheck(L_45);
		RectTransform_set_anchoredPosition_m1498949997(L_45, L_46, /*hidden argument*/NULL);
		Image_t3354615620 * L_47 = __this->get__btnYesImg_9();
		NullCheck(L_47);
		Text_t3286458198 * L_48 = Component_GetComponentInChildren_TisText_t3286458198_m582470501(L_47, /*hidden argument*/Component_GetComponentInChildren_TisText_t3286458198_m582470501_MethodInfo_var);
		V_2 = L_48;
		Text_t3286458198 * L_49 = V_2;
		String_t* L_50 = ___btnText07;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_51 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_52 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_50, L_51, /*hidden argument*/NULL);
		G_B3_0 = L_49;
		if (!L_52)
		{
			G_B4_0 = L_49;
			goto IL_0182;
		}
	}
	{
		G_B5_0 = _stringLiteral2556;
		G_B5_1 = G_B3_0;
		goto IL_0184;
	}

IL_0182:
	{
		String_t* L_53 = ___btnText07;
		G_B5_0 = L_53;
		G_B5_1 = G_B4_0;
	}

IL_0184:
	{
		NullCheck(G_B5_1);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B5_1, G_B5_0);
		RectTransform_t3317474837 * L_54 = V_1;
		Text_t3286458198 * L_55 = V_2;
		NullCheck(L_55);
		float L_56 = VirtFuncInvoker0< float >::Invoke(69 /* System.Single UnityEngine.UI.Text::get_preferredWidth() */, L_55);
		float L_57 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_58 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, ((float)((float)L_56+(float)(20.0f))), (70.0f), ((float)((float)((float)((float)L_57/(float)(2.0f)))-(float)(30.0f))), /*hidden argument*/NULL);
		NullCheck(L_54);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_54, 0, L_58, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_59 = V_1;
		float L_60 = (&V_0)->get_y_2();
		NullCheck(L_59);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_59, 1, ((float)((float)L_60*(float)(0.2f))), /*hidden argument*/NULL);
		Image_t3354615620 * L_61 = __this->get__btnNoImg_10();
		NullCheck(L_61);
		GameObject_t4012695102 * L_62 = Component_get_gameObject_m1170635899(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		GameObject_SetActive_m3538205401(L_62, (bool)0, /*hidden argument*/NULL);
		Image_t3354615620 * L_63 = __this->get__btnCancelImg_11();
		NullCheck(L_63);
		GameObject_t4012695102 * L_64 = Component_get_gameObject_m1170635899(L_63, /*hidden argument*/NULL);
		NullCheck(L_64);
		GameObject_SetActive_m3538205401(L_64, (bool)0, /*hidden argument*/NULL);
		goto IL_0652;
	}

IL_01f4:
	{
		Image_t3354615620 * L_65 = __this->get__btnCancelImg_11();
		NullCheck(L_65);
		Text_t3286458198 * L_66 = Component_GetComponentInChildren_TisText_t3286458198_m582470501(L_65, /*hidden argument*/Component_GetComponentInChildren_TisText_t3286458198_m582470501_MethodInfo_var);
		V_2 = L_66;
		Text_t3286458198 * L_67 = V_2;
		String_t* L_68 = ___btnText29;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_69 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_70 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_68, L_69, /*hidden argument*/NULL);
		G_B7_0 = L_67;
		if (!L_70)
		{
			G_B8_0 = L_67;
			goto IL_021c;
		}
	}
	{
		G_B9_0 = _stringLiteral2011110042;
		G_B9_1 = G_B7_0;
		goto IL_021e;
	}

IL_021c:
	{
		String_t* L_71 = ___btnText29;
		G_B9_0 = L_71;
		G_B9_1 = G_B8_0;
	}

IL_021e:
	{
		NullCheck(G_B9_1);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B9_1, G_B9_0);
		Image_t3354615620 * L_72 = __this->get__btnCancelImg_11();
		NullCheck(L_72);
		RectTransform_t3317474837 * L_73 = Graphic_get_rectTransform_m4017371950(L_72, /*hidden argument*/NULL);
		V_1 = L_73;
		Text_t3286458198 * L_74 = V_2;
		NullCheck(L_74);
		float L_75 = VirtFuncInvoker0< float >::Invoke(69 /* System.Single UnityEngine.UI.Text::get_preferredWidth() */, L_74);
		float L_76 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_77 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, ((float)((float)L_75+(float)(20.0f))), (70.0f), ((float)((float)((float)((float)L_76/(float)(2.0f)))-(float)(30.0f))), /*hidden argument*/NULL);
		V_3 = L_77;
		RectTransform_t3317474837 * L_78 = V_1;
		float L_79 = V_3;
		NullCheck(L_78);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_78, 0, L_79, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_80 = V_1;
		float L_81 = (&V_0)->get_y_2();
		NullCheck(L_80);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_80, 1, ((float)((float)L_81*(float)(0.2f))), /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_82 = V_1;
		Vector2_t3525329788  L_83;
		memset(&L_83, 0, sizeof(L_83));
		Vector2__ctor_m1517109030(&L_83, (-10.0f), (10.0f), /*hidden argument*/NULL);
		NullCheck(L_82);
		RectTransform_set_anchoredPosition_m1498949997(L_82, L_83, /*hidden argument*/NULL);
		Image_t3354615620 * L_84 = __this->get__btnYesImg_9();
		NullCheck(L_84);
		RectTransform_t3317474837 * L_85 = Graphic_get_rectTransform_m4017371950(L_84, /*hidden argument*/NULL);
		V_1 = L_85;
		RectTransform_t3317474837 * L_86 = V_1;
		float L_87 = V_3;
		Vector2_t3525329788  L_88;
		memset(&L_88, 0, sizeof(L_88));
		Vector2__ctor_m1517109030(&L_88, ((-((float)((float)(20.0f)+(float)L_87)))), (10.0f), /*hidden argument*/NULL);
		NullCheck(L_86);
		RectTransform_set_anchoredPosition_m1498949997(L_86, L_88, /*hidden argument*/NULL);
		Image_t3354615620 * L_89 = __this->get__btnYesImg_9();
		NullCheck(L_89);
		Text_t3286458198 * L_90 = Component_GetComponentInChildren_TisText_t3286458198_m582470501(L_89, /*hidden argument*/Component_GetComponentInChildren_TisText_t3286458198_m582470501_MethodInfo_var);
		V_2 = L_90;
		Text_t3286458198 * L_91 = V_2;
		String_t* L_92 = ___btnText07;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_93 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_94 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_92, L_93, /*hidden argument*/NULL);
		G_B10_0 = L_91;
		if (!L_94)
		{
			G_B11_0 = L_91;
			goto IL_02d6;
		}
	}
	{
		G_B12_0 = _stringLiteral2556;
		G_B12_1 = G_B10_0;
		goto IL_02d8;
	}

IL_02d6:
	{
		String_t* L_95 = ___btnText07;
		G_B12_0 = L_95;
		G_B12_1 = G_B11_0;
	}

IL_02d8:
	{
		NullCheck(G_B12_1);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B12_1, G_B12_0);
		RectTransform_t3317474837 * L_96 = V_1;
		Text_t3286458198 * L_97 = V_2;
		NullCheck(L_97);
		float L_98 = VirtFuncInvoker0< float >::Invoke(69 /* System.Single UnityEngine.UI.Text::get_preferredWidth() */, L_97);
		float L_99 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_100 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, ((float)((float)L_98+(float)(20.0f))), (70.0f), ((float)((float)((float)((float)L_99/(float)(2.0f)))-(float)(30.0f))), /*hidden argument*/NULL);
		NullCheck(L_96);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_96, 0, L_100, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_101 = V_1;
		float L_102 = (&V_0)->get_y_2();
		NullCheck(L_101);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_101, 1, ((float)((float)L_102*(float)(0.2f))), /*hidden argument*/NULL);
		Image_t3354615620 * L_103 = __this->get__btnNoImg_10();
		NullCheck(L_103);
		GameObject_t4012695102 * L_104 = Component_get_gameObject_m1170635899(L_103, /*hidden argument*/NULL);
		NullCheck(L_104);
		GameObject_SetActive_m3538205401(L_104, (bool)0, /*hidden argument*/NULL);
		goto IL_0652;
	}

IL_0337:
	{
		Image_t3354615620 * L_105 = __this->get__btnNoImg_10();
		NullCheck(L_105);
		Text_t3286458198 * L_106 = Component_GetComponentInChildren_TisText_t3286458198_m582470501(L_105, /*hidden argument*/Component_GetComponentInChildren_TisText_t3286458198_m582470501_MethodInfo_var);
		V_2 = L_106;
		Text_t3286458198 * L_107 = V_2;
		String_t* L_108 = ___btnText18;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_109 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_110 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_108, L_109, /*hidden argument*/NULL);
		G_B14_0 = L_107;
		if (!L_110)
		{
			G_B15_0 = L_107;
			goto IL_035f;
		}
	}
	{
		G_B16_0 = _stringLiteral2529;
		G_B16_1 = G_B14_0;
		goto IL_0361;
	}

IL_035f:
	{
		String_t* L_111 = ___btnText18;
		G_B16_0 = L_111;
		G_B16_1 = G_B15_0;
	}

IL_0361:
	{
		NullCheck(G_B16_1);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B16_1, G_B16_0);
		Image_t3354615620 * L_112 = __this->get__btnNoImg_10();
		NullCheck(L_112);
		RectTransform_t3317474837 * L_113 = Graphic_get_rectTransform_m4017371950(L_112, /*hidden argument*/NULL);
		V_1 = L_113;
		Text_t3286458198 * L_114 = V_2;
		NullCheck(L_114);
		float L_115 = VirtFuncInvoker0< float >::Invoke(69 /* System.Single UnityEngine.UI.Text::get_preferredWidth() */, L_114);
		float L_116 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_117 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, ((float)((float)L_115+(float)(20.0f))), (70.0f), ((float)((float)((float)((float)L_116/(float)(2.0f)))-(float)(30.0f))), /*hidden argument*/NULL);
		V_4 = L_117;
		RectTransform_t3317474837 * L_118 = V_1;
		float L_119 = V_4;
		NullCheck(L_118);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_118, 0, L_119, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_120 = V_1;
		float L_121 = (&V_0)->get_y_2();
		NullCheck(L_120);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_120, 1, ((float)((float)L_121*(float)(0.2f))), /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_122 = V_1;
		Vector2_t3525329788  L_123;
		memset(&L_123, 0, sizeof(L_123));
		Vector2__ctor_m1517109030(&L_123, (-10.0f), (10.0f), /*hidden argument*/NULL);
		NullCheck(L_122);
		RectTransform_set_anchoredPosition_m1498949997(L_122, L_123, /*hidden argument*/NULL);
		Image_t3354615620 * L_124 = __this->get__btnYesImg_9();
		NullCheck(L_124);
		RectTransform_t3317474837 * L_125 = Graphic_get_rectTransform_m4017371950(L_124, /*hidden argument*/NULL);
		V_1 = L_125;
		RectTransform_t3317474837 * L_126 = V_1;
		float L_127 = V_4;
		Vector2_t3525329788  L_128;
		memset(&L_128, 0, sizeof(L_128));
		Vector2__ctor_m1517109030(&L_128, ((-((float)((float)(20.0f)+(float)L_127)))), (10.0f), /*hidden argument*/NULL);
		NullCheck(L_126);
		RectTransform_set_anchoredPosition_m1498949997(L_126, L_128, /*hidden argument*/NULL);
		Image_t3354615620 * L_129 = __this->get__btnYesImg_9();
		NullCheck(L_129);
		Text_t3286458198 * L_130 = Component_GetComponentInChildren_TisText_t3286458198_m582470501(L_129, /*hidden argument*/Component_GetComponentInChildren_TisText_t3286458198_m582470501_MethodInfo_var);
		V_2 = L_130;
		Text_t3286458198 * L_131 = V_2;
		String_t* L_132 = ___btnText07;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_133 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_134 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_132, L_133, /*hidden argument*/NULL);
		G_B17_0 = L_131;
		if (!L_134)
		{
			G_B18_0 = L_131;
			goto IL_041c;
		}
	}
	{
		G_B19_0 = _stringLiteral88775;
		G_B19_1 = G_B17_0;
		goto IL_041e;
	}

IL_041c:
	{
		String_t* L_135 = ___btnText07;
		G_B19_0 = L_135;
		G_B19_1 = G_B18_0;
	}

IL_041e:
	{
		NullCheck(G_B19_1);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B19_1, G_B19_0);
		RectTransform_t3317474837 * L_136 = V_1;
		Text_t3286458198 * L_137 = V_2;
		NullCheck(L_137);
		float L_138 = VirtFuncInvoker0< float >::Invoke(69 /* System.Single UnityEngine.UI.Text::get_preferredWidth() */, L_137);
		float L_139 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_140 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, ((float)((float)L_138+(float)(20.0f))), (70.0f), ((float)((float)((float)((float)L_139/(float)(2.0f)))-(float)(30.0f))), /*hidden argument*/NULL);
		NullCheck(L_136);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_136, 0, L_140, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_141 = V_1;
		float L_142 = (&V_0)->get_y_2();
		NullCheck(L_141);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_141, 1, ((float)((float)L_142*(float)(0.2f))), /*hidden argument*/NULL);
		Image_t3354615620 * L_143 = __this->get__btnCancelImg_11();
		NullCheck(L_143);
		GameObject_t4012695102 * L_144 = Component_get_gameObject_m1170635899(L_143, /*hidden argument*/NULL);
		NullCheck(L_144);
		GameObject_SetActive_m3538205401(L_144, (bool)0, /*hidden argument*/NULL);
		goto IL_0652;
	}

IL_047d:
	{
		Image_t3354615620 * L_145 = __this->get__btnCancelImg_11();
		NullCheck(L_145);
		Text_t3286458198 * L_146 = Component_GetComponentInChildren_TisText_t3286458198_m582470501(L_145, /*hidden argument*/Component_GetComponentInChildren_TisText_t3286458198_m582470501_MethodInfo_var);
		V_2 = L_146;
		Text_t3286458198 * L_147 = V_2;
		String_t* L_148 = ___btnText29;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_149 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_150 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_148, L_149, /*hidden argument*/NULL);
		G_B21_0 = L_147;
		if (!L_150)
		{
			G_B22_0 = L_147;
			goto IL_04a5;
		}
	}
	{
		G_B23_0 = _stringLiteral2011110042;
		G_B23_1 = G_B21_0;
		goto IL_04a7;
	}

IL_04a5:
	{
		String_t* L_151 = ___btnText29;
		G_B23_0 = L_151;
		G_B23_1 = G_B22_0;
	}

IL_04a7:
	{
		NullCheck(G_B23_1);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B23_1, G_B23_0);
		Image_t3354615620 * L_152 = __this->get__btnCancelImg_11();
		NullCheck(L_152);
		RectTransform_t3317474837 * L_153 = Graphic_get_rectTransform_m4017371950(L_152, /*hidden argument*/NULL);
		V_1 = L_153;
		Text_t3286458198 * L_154 = V_2;
		NullCheck(L_154);
		float L_155 = VirtFuncInvoker0< float >::Invoke(69 /* System.Single UnityEngine.UI.Text::get_preferredWidth() */, L_154);
		float L_156 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_157 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, ((float)((float)L_155+(float)(20.0f))), (35.0f), ((float)((float)((float)((float)L_156/(float)(3.0f)))-(float)(30.0f))), /*hidden argument*/NULL);
		V_3 = L_157;
		RectTransform_t3317474837 * L_158 = V_1;
		float L_159 = V_3;
		NullCheck(L_158);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_158, 0, L_159, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_160 = V_1;
		float L_161 = (&V_0)->get_y_2();
		NullCheck(L_160);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_160, 1, ((float)((float)L_161*(float)(0.2f))), /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_162 = V_1;
		Vector2_t3525329788  L_163;
		memset(&L_163, 0, sizeof(L_163));
		Vector2__ctor_m1517109030(&L_163, (-10.0f), (10.0f), /*hidden argument*/NULL);
		NullCheck(L_162);
		RectTransform_set_anchoredPosition_m1498949997(L_162, L_163, /*hidden argument*/NULL);
		Image_t3354615620 * L_164 = __this->get__btnNoImg_10();
		NullCheck(L_164);
		Text_t3286458198 * L_165 = Component_GetComponentInChildren_TisText_t3286458198_m582470501(L_164, /*hidden argument*/Component_GetComponentInChildren_TisText_t3286458198_m582470501_MethodInfo_var);
		V_2 = L_165;
		Text_t3286458198 * L_166 = V_2;
		String_t* L_167 = ___btnText18;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_168 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_169 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_167, L_168, /*hidden argument*/NULL);
		G_B24_0 = L_166;
		if (!L_169)
		{
			G_B25_0 = L_166;
			goto IL_053b;
		}
	}
	{
		G_B26_0 = _stringLiteral2529;
		G_B26_1 = G_B24_0;
		goto IL_053d;
	}

IL_053b:
	{
		String_t* L_170 = ___btnText18;
		G_B26_0 = L_170;
		G_B26_1 = G_B25_0;
	}

IL_053d:
	{
		NullCheck(G_B26_1);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B26_1, G_B26_0);
		Image_t3354615620 * L_171 = __this->get__btnNoImg_10();
		NullCheck(L_171);
		RectTransform_t3317474837 * L_172 = Graphic_get_rectTransform_m4017371950(L_171, /*hidden argument*/NULL);
		V_1 = L_172;
		Text_t3286458198 * L_173 = V_2;
		NullCheck(L_173);
		float L_174 = VirtFuncInvoker0< float >::Invoke(69 /* System.Single UnityEngine.UI.Text::get_preferredWidth() */, L_173);
		float L_175 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_176 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, ((float)((float)L_174+(float)(20.0f))), (35.0f), ((float)((float)((float)((float)L_175/(float)(3.0f)))-(float)(30.0f))), /*hidden argument*/NULL);
		V_4 = L_176;
		RectTransform_t3317474837 * L_177 = V_1;
		float L_178 = V_4;
		NullCheck(L_177);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_177, 0, L_178, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_179 = V_1;
		float L_180 = (&V_0)->get_y_2();
		NullCheck(L_179);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_179, 1, ((float)((float)L_180*(float)(0.2f))), /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_181 = V_1;
		float L_182 = V_3;
		Vector2_t3525329788  L_183;
		memset(&L_183, 0, sizeof(L_183));
		Vector2__ctor_m1517109030(&L_183, ((-((float)((float)(20.0f)+(float)L_182)))), (10.0f), /*hidden argument*/NULL);
		NullCheck(L_181);
		RectTransform_set_anchoredPosition_m1498949997(L_181, L_183, /*hidden argument*/NULL);
		Image_t3354615620 * L_184 = __this->get__btnYesImg_9();
		NullCheck(L_184);
		RectTransform_t3317474837 * L_185 = Graphic_get_rectTransform_m4017371950(L_184, /*hidden argument*/NULL);
		V_1 = L_185;
		RectTransform_t3317474837 * L_186 = V_1;
		float L_187 = V_3;
		float L_188 = V_4;
		Vector2_t3525329788  L_189;
		memset(&L_189, 0, sizeof(L_189));
		Vector2__ctor_m1517109030(&L_189, ((-((float)((float)((float)((float)(30.0f)+(float)L_187))+(float)L_188)))), (10.0f), /*hidden argument*/NULL);
		NullCheck(L_186);
		RectTransform_set_anchoredPosition_m1498949997(L_186, L_189, /*hidden argument*/NULL);
		Image_t3354615620 * L_190 = __this->get__btnYesImg_9();
		NullCheck(L_190);
		Text_t3286458198 * L_191 = Component_GetComponentInChildren_TisText_t3286458198_m582470501(L_190, /*hidden argument*/Component_GetComponentInChildren_TisText_t3286458198_m582470501_MethodInfo_var);
		V_2 = L_191;
		Text_t3286458198 * L_192 = V_2;
		String_t* L_193 = ___btnText07;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_194 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_195 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_193, L_194, /*hidden argument*/NULL);
		G_B27_0 = L_192;
		if (!L_195)
		{
			G_B28_0 = L_192;
			goto IL_05fd;
		}
	}
	{
		G_B29_0 = _stringLiteral2556;
		G_B29_1 = G_B27_0;
		goto IL_05ff;
	}

IL_05fd:
	{
		String_t* L_196 = ___btnText07;
		G_B29_0 = L_196;
		G_B29_1 = G_B28_0;
	}

IL_05ff:
	{
		NullCheck(G_B29_1);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B29_1, G_B29_0);
		RectTransform_t3317474837 * L_197 = V_1;
		Text_t3286458198 * L_198 = V_2;
		NullCheck(L_198);
		float L_199 = VirtFuncInvoker0< float >::Invoke(69 /* System.Single UnityEngine.UI.Text::get_preferredWidth() */, L_198);
		float L_200 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_201 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, ((float)((float)L_199+(float)(20.0f))), (35.0f), ((float)((float)((float)((float)L_200/(float)(3.0f)))-(float)(30.0f))), /*hidden argument*/NULL);
		NullCheck(L_197);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_197, 0, L_201, /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_202 = V_1;
		float L_203 = (&V_0)->get_y_2();
		NullCheck(L_202);
		RectTransform_SetSizeWithCurrentAnchors_m4019722691(L_202, 1, ((float)((float)L_203*(float)(0.2f))), /*hidden argument*/NULL);
		goto IL_0652;
	}

IL_064d:
	{
		goto IL_0652;
	}

IL_0652:
	{
		return;
	}
}
// PoqXert.MessageBox.MSGBoxStyle PoqXert.MessageBox.MsgBox::GetStyle(PoqXert.MessageBox.MsgBoxStyle)
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern const uint32_t MsgBox_GetStyle_m1286816734_MetadataUsageId;
extern "C"  MSGBoxStyle_t1923386386 * MsgBox_GetStyle_m1286816734 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MsgBox_GetStyle_m1286816734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___id0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0028;
		}
		if (L_1 == 2)
		{
			goto IL_0033;
		}
		if (L_1 == 3)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0049;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_2 = MsgBox_get_prefab_m2374698177(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		MSGBoxStyle_t1923386386 * L_3 = L_2->get__informationStyle_13();
		return L_3;
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_4 = MsgBox_get_prefab_m2374698177(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		MSGBoxStyle_t1923386386 * L_5 = L_4->get__questionStyle_14();
		return L_5;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_6 = MsgBox_get_prefab_m2374698177(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		MSGBoxStyle_t1923386386 * L_7 = L_6->get__warningStyle_15();
		return L_7;
	}

IL_003e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_8 = MsgBox_get_prefab_m2374698177(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		MSGBoxStyle_t1923386386 * L_9 = L_8->get__errorStyle_16();
		return L_9;
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_10 = MsgBox_get_prefab_m2374698177(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		MSGBoxStyle_t1923386386 * L_11 = L_10->get__informationStyle_13();
		return L_11;
	}
}
// System.Void PoqXert.MessageBox.MsgBox::Show(System.Int32,System.String,PoqXert.MessageBox.DialogResultMethod,System.Boolean,System.String,System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern const uint32_t MsgBox_Show_m3994277758_MetadataUsageId;
extern "C"  void MsgBox_Show_m3994277758 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___message1, DialogResultMethod_t1789391825 * ___method2, bool ___modal3, String_t* ___btnText04, String_t* ___btnText15, String_t* ___btnText26, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MsgBox_Show_m3994277758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id0;
		String_t* L_1 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		DialogResultMethod_t1789391825 * L_3 = ___method2;
		bool L_4 = ___modal3;
		String_t* L_5 = ___btnText04;
		String_t* L_6 = ___btnText15;
		String_t* L_7 = ___btnText26;
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_Show_m3109697522(NULL /*static, unused*/, L_0, L_1, L_2, 0, 0, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PoqXert.MessageBox.MsgBox::Show(System.Int32,System.String,System.String,PoqXert.MessageBox.DialogResultMethod,System.Boolean,System.String,System.String,System.String)
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern const uint32_t MsgBox_Show_m3721224514_MetadataUsageId;
extern "C"  void MsgBox_Show_m3721224514 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___message1, String_t* ___caption2, DialogResultMethod_t1789391825 * ___method3, bool ___modal4, String_t* ___btnText05, String_t* ___btnText16, String_t* ___btnText27, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MsgBox_Show_m3721224514_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id0;
		String_t* L_1 = ___message1;
		String_t* L_2 = ___caption2;
		DialogResultMethod_t1789391825 * L_3 = ___method3;
		bool L_4 = ___modal4;
		String_t* L_5 = ___btnText05;
		String_t* L_6 = ___btnText16;
		String_t* L_7 = ___btnText27;
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_Show_m3109697522(NULL /*static, unused*/, L_0, L_1, L_2, 0, 0, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PoqXert.MessageBox.MsgBox::Show(System.Int32,System.String,System.String,PoqXert.MessageBox.MsgBoxButtons,PoqXert.MessageBox.DialogResultMethod,System.Boolean,System.String,System.String,System.String)
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern const uint32_t MsgBox_Show_m34694978_MetadataUsageId;
extern "C"  void MsgBox_Show_m34694978 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___message1, String_t* ___caption2, int32_t ___buttons3, DialogResultMethod_t1789391825 * ___method4, bool ___modal5, String_t* ___btnText06, String_t* ___btnText17, String_t* ___btnText28, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MsgBox_Show_m34694978_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id0;
		String_t* L_1 = ___message1;
		String_t* L_2 = ___caption2;
		int32_t L_3 = ___buttons3;
		DialogResultMethod_t1789391825 * L_4 = ___method4;
		bool L_5 = ___modal5;
		String_t* L_6 = ___btnText06;
		String_t* L_7 = ___btnText17;
		String_t* L_8 = ___btnText28;
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_Show_m3109697522(NULL /*static, unused*/, L_0, L_1, L_2, L_3, 0, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PoqXert.MessageBox.MsgBox::Show(System.Int32,System.String,System.String,PoqXert.MessageBox.MsgBoxButtons,PoqXert.MessageBox.MsgBoxStyle,PoqXert.MessageBox.DialogResultMethod,System.Boolean,System.String,System.String,System.String)
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern const uint32_t MsgBox_Show_m3109697522_MetadataUsageId;
extern "C"  void MsgBox_Show_m3109697522 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___message1, String_t* ___caption2, int32_t ___buttons3, int32_t ___style4, DialogResultMethod_t1789391825 * ___method5, bool ___modal6, String_t* ___btnText07, String_t* ___btnText18, String_t* ___btnText29, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MsgBox_Show_m3109697522_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id0;
		String_t* L_1 = ___message1;
		String_t* L_2 = ___caption2;
		int32_t L_3 = ___buttons3;
		int32_t L_4 = ___style4;
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MSGBoxStyle_t1923386386 * L_5 = MsgBox_GetStyle_m1286816734(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		DialogResultMethod_t1789391825 * L_6 = ___method5;
		bool L_7 = ___modal6;
		String_t* L_8 = ___btnText07;
		String_t* L_9 = ___btnText18;
		String_t* L_10 = ___btnText29;
		MsgBox_Show_m2597204978(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_5, L_6, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PoqXert.MessageBox.MsgBox::Show(System.Int32,System.String,System.String,PoqXert.MessageBox.MsgBoxButtons,PoqXert.MessageBox.MSGBoxStyle,PoqXert.MessageBox.DialogResultMethod,System.Boolean,System.String,System.String,System.String)
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern Il2CppClass* EventSystem_t409518532_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisMsgBox_t1468720533_m747618583_MethodInfo_var;
extern const uint32_t MsgBox_Show_m2597204978_MetadataUsageId;
extern "C"  void MsgBox_Show_m2597204978 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___message1, String_t* ___caption2, int32_t ___buttons3, MSGBoxStyle_t1923386386 * ___style4, DialogResultMethod_t1789391825 * ___method5, bool ___modal6, String_t* ___btnText07, String_t* ___btnText18, String_t* ___btnText29, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MsgBox_Show_m2597204978_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_Close_m1432979546(NULL /*static, unused*/, /*hidden argument*/NULL);
		MsgBox_t1468720533 * L_0 = MsgBox_get_prefab_m2374698177(NULL /*static, unused*/, /*hidden argument*/NULL);
		MsgBox_t1468720533 * L_1 = Object_Instantiate_TisMsgBox_t1468720533_m747618583(NULL /*static, unused*/, L_0, /*hidden argument*/Object_Instantiate_TisMsgBox_t1468720533_m747618583_MethodInfo_var);
		((MsgBox_t1468720533_StaticFields*)MsgBox_t1468720533_il2cpp_TypeInfo_var->static_fields)->set__lastMsgBox_19(L_1);
		MsgBox_t1468720533 * L_2 = ((MsgBox_t1468720533_StaticFields*)MsgBox_t1468720533_il2cpp_TypeInfo_var->static_fields)->get__lastMsgBox_19();
		int32_t L_3 = ___id0;
		String_t* L_4 = ___message1;
		String_t* L_5 = ___caption2;
		int32_t L_6 = ___buttons3;
		MSGBoxStyle_t1923386386 * L_7 = ___style4;
		DialogResultMethod_t1789391825 * L_8 = ___method5;
		bool L_9 = ___modal6;
		String_t* L_10 = ___btnText07;
		String_t* L_11 = ___btnText18;
		String_t* L_12 = ___btnText29;
		NullCheck(L_2);
		MsgBox_BuildMessageBox_m96384347(L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(EventSystem_t409518532_il2cpp_TypeInfo_var);
		EventSystem_t409518532 * L_13 = EventSystem_get_current_m3483537871(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_14 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_13, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_004e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_15 = ((MsgBox_t1468720533_StaticFields*)MsgBox_t1468720533_il2cpp_TypeInfo_var->static_fields)->get__lastMsgBox_19();
		NullCheck(L_15);
		GameObject_t4012695102 * L_16 = L_15->get__eventer_12();
		NullCheck(L_16);
		GameObject_SetActive_m3538205401(L_16, (bool)1, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Void PoqXert.MessageBox.MsgBox::Show(System.Int32,System.String,System.String,PoqXert.MessageBox.MsgBoxButtons,System.Int32,PoqXert.MessageBox.DialogResultMethod,System.Boolean,System.String,System.String,System.String)
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2253546971;
extern const uint32_t MsgBox_Show_m2815667799_MetadataUsageId;
extern "C"  void MsgBox_Show_m2815667799 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___message1, String_t* ___caption2, int32_t ___buttons3, int32_t ___customStyleID4, DialogResultMethod_t1789391825 * ___method5, bool ___modal6, String_t* ___btnText07, String_t* ___btnText18, String_t* ___btnText29, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MsgBox_Show_m2815667799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___customStyleID4;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = ___customStyleID4;
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_2 = MsgBox_get_prefab_m2374698177(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		List_1_t2720345355 * L_3 = L_2->get__customStyles_17();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::get_Count() */, L_3);
		if ((((int32_t)L_1) < ((int32_t)L_4)))
		{
			goto IL_0029;
		}
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral2253546971, /*hidden argument*/NULL);
		return;
	}

IL_0029:
	{
		int32_t L_5 = ___id0;
		String_t* L_6 = ___message1;
		String_t* L_7 = ___caption2;
		int32_t L_8 = ___buttons3;
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_9 = MsgBox_get_prefab_m2374698177(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		List_1_t2720345355 * L_10 = L_9->get__customStyles_17();
		int32_t L_11 = ___customStyleID4;
		NullCheck(L_10);
		MSGBoxStyle_t1923386386 * L_12 = VirtFuncInvoker1< MSGBoxStyle_t1923386386 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>::get_Item(System.Int32) */, L_10, L_11);
		DialogResultMethod_t1789391825 * L_13 = ___method5;
		bool L_14 = ___modal6;
		String_t* L_15 = ___btnText07;
		String_t* L_16 = ___btnText18;
		String_t* L_17 = ___btnText29;
		MsgBox_Show_m2597204978(NULL /*static, unused*/, L_5, L_6, L_7, L_8, L_12, L_13, L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PoqXert.MessageBox.MsgBox::Close()
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern const uint32_t MsgBox_Close_m1432979546_MetadataUsageId;
extern "C"  void MsgBox_Close_m1432979546 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MsgBox_Close_m1432979546_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_0 = ((MsgBox_t1468720533_StaticFields*)MsgBox_t1468720533_il2cpp_TypeInfo_var->static_fields)->get__lastMsgBox_19();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_2 = ((MsgBox_t1468720533_StaticFields*)MsgBox_t1468720533_il2cpp_TypeInfo_var->static_fields)->get__lastMsgBox_19();
		NullCheck(L_2);
		GameObject_t4012695102 * L_3 = Component_get_gameObject_m1170635899(L_2, /*hidden argument*/NULL);
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void PoqXert.MessageBox.MSGBoxStyle::.ctor()
extern "C"  void MSGBoxStyle__ctor_m1731184625 (MSGBoxStyle_t1923386386 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = Color_get_blue_m3657252170(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__captionColor_2(L_0);
		Color_t1588175760  L_1 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__backgroundColor_3(L_1);
		Color_t1588175760  L_2 = Color_get_green_m2005284533(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__btnYesColor_4(L_2);
		Color_t1588175760  L_3 = Color_get_blue_m3657252170(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__btnNoColor_5(L_3);
		Color_t1588175760  L_4 = Color_get_red_m4288945411(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__btnCancelColor_6(L_4);
		ScriptableObject__ctor_m1827087273(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::get_captionColor()
extern "C"  Color_t1588175760  MSGBoxStyle_get_captionColor_m334910080 (MSGBoxStyle_t1923386386 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = __this->get__captionColor_2();
		return L_0;
	}
}
// System.Void PoqXert.MessageBox.MSGBoxStyle::set_captionColor(UnityEngine.Color)
extern "C"  void MSGBoxStyle_set_captionColor_m234508889 (MSGBoxStyle_t1923386386 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = ___value0;
		__this->set__captionColor_2(L_0);
		return;
	}
}
// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::get_backgroundColor()
extern "C"  Color_t1588175760  MSGBoxStyle_get_backgroundColor_m3445433268 (MSGBoxStyle_t1923386386 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = __this->get__backgroundColor_3();
		return L_0;
	}
}
// System.Void PoqXert.MessageBox.MSGBoxStyle::set_backgroundColor(UnityEngine.Color)
extern "C"  void MSGBoxStyle_set_backgroundColor_m2284919135 (MSGBoxStyle_t1923386386 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = ___value0;
		__this->set__backgroundColor_3(L_0);
		return;
	}
}
// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::get_btnYesColor()
extern "C"  Color_t1588175760  MSGBoxStyle_get_btnYesColor_m1144113751 (MSGBoxStyle_t1923386386 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = __this->get__btnYesColor_4();
		return L_0;
	}
}
// System.Void PoqXert.MessageBox.MSGBoxStyle::set_btnYesColor(UnityEngine.Color)
extern "C"  void MSGBoxStyle_set_btnYesColor_m4268583836 (MSGBoxStyle_t1923386386 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = ___value0;
		__this->set__btnYesColor_4(L_0);
		return;
	}
}
// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::get_btnNoColor()
extern "C"  Color_t1588175760  MSGBoxStyle_get_btnNoColor_m437349321 (MSGBoxStyle_t1923386386 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = __this->get__btnNoColor_5();
		return L_0;
	}
}
// System.Void PoqXert.MessageBox.MSGBoxStyle::set_btnNoColor(UnityEngine.Color)
extern "C"  void MSGBoxStyle_set_btnNoColor_m3380012272 (MSGBoxStyle_t1923386386 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = ___value0;
		__this->set__btnNoColor_5(L_0);
		return;
	}
}
// UnityEngine.Color PoqXert.MessageBox.MSGBoxStyle::get_btnCancelColor()
extern "C"  Color_t1588175760  MSGBoxStyle_get_btnCancelColor_m4250514928 (MSGBoxStyle_t1923386386 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = __this->get__btnCancelColor_6();
		return L_0;
	}
}
// System.Void PoqXert.MessageBox.MSGBoxStyle::set_btnCancelColor(UnityEngine.Color)
extern "C"  void MSGBoxStyle_set_btnCancelColor_m62151721 (MSGBoxStyle_t1923386386 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = ___value0;
		__this->set__btnCancelColor_6(L_0);
		return;
	}
}
// UnityEngine.Sprite PoqXert.MessageBox.MSGBoxStyle::get_icon()
extern "C"  Sprite_t4006040370 * MSGBoxStyle_get_icon_m2425158486 (MSGBoxStyle_t1923386386 * __this, const MethodInfo* method)
{
	{
		Sprite_t4006040370 * L_0 = __this->get__icon_7();
		return L_0;
	}
}
// System.Void PoqXert.MessageBox.MSGBoxStyle::set_icon(UnityEngine.Sprite)
extern "C"  void MSGBoxStyle_set_icon_m2791749517 (MSGBoxStyle_t1923386386 * __this, Sprite_t4006040370 * ___value0, const MethodInfo* method)
{
	{
		Sprite_t4006040370 * L_0 = ___value0;
		__this->set__icon_7(L_0);
		return;
	}
}
// System.Void PXMSGExample::.ctor()
extern "C"  void PXMSGExample__ctor_m2869546730 (PXMSGExample_t104414929 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PXMSGExample::Update()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimePlatform_t1574985880_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DialogResultMethod_t1789391825_il2cpp_TypeInfo_var;
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern const MethodInfo* PXMSGExample_Method_m3687707510_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2919985350;
extern Il2CppCodeGenString* _stringLiteral1939328147;
extern Il2CppCodeGenString* _stringLiteral3856372476;
extern Il2CppCodeGenString* _stringLiteral2528879;
extern Il2CppCodeGenString* _stringLiteral1427071199;
extern Il2CppCodeGenString* _stringLiteral2403596963;
extern Il2CppCodeGenString* _stringLiteral2443895749;
extern Il2CppCodeGenString* _stringLiteral2011110042;
extern Il2CppCodeGenString* _stringLiteral294053489;
extern Il2CppCodeGenString* _stringLiteral67232232;
extern Il2CppCodeGenString* _stringLiteral65203672;
extern Il2CppCodeGenString* _stringLiteral950830637;
extern Il2CppCodeGenString* _stringLiteral1325975414;
extern Il2CppCodeGenString* _stringLiteral2569629;
extern Il2CppCodeGenString* _stringLiteral2799618605;
extern Il2CppCodeGenString* _stringLiteral2838376053;
extern Il2CppCodeGenString* _stringLiteral2217226694;
extern Il2CppCodeGenString* _stringLiteral88775;
extern Il2CppCodeGenString* _stringLiteral2529;
extern Il2CppCodeGenString* _stringLiteral74120264;
extern const uint32_t PXMSGExample_Update_m488497507_MetadataUsageId;
extern "C"  void PXMSGExample_Update_m488497507 (PXMSGExample_t104414929 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PXMSGExample_Update_m488497507_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)49), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)105), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005d;
		}
	}

IL_0018:
	{
		int32_t L_2 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(RuntimePlatform_t1574985880_il2cpp_TypeInfo_var, &L_3);
		NullCheck((Enum_t2778772662 *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2778772662 *)L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2919985350, L_5, /*hidden argument*/NULL);
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)PXMSGExample_Method_m3687707510_MethodInfo_var);
		DialogResultMethod_t1789391825 * L_8 = (DialogResultMethod_t1789391825 *)il2cpp_codegen_object_new(DialogResultMethod_t1789391825_il2cpp_TypeInfo_var);
		DialogResultMethod__ctor_m3207538458(L_8, __this, L_7, /*hidden argument*/NULL);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_Show_m3721224514(NULL /*static, unused*/, 0, L_6, _stringLiteral1939328147, L_8, (bool)0, L_9, L_10, L_11, /*hidden argument*/NULL);
		goto IL_01c3;
	}

IL_005d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_12 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)50), /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0075;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_13 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)113), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00a8;
		}
	}

IL_0075:
	{
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)PXMSGExample_Method_m3687707510_MethodInfo_var);
		DialogResultMethod_t1789391825 * L_15 = (DialogResultMethod_t1789391825 *)il2cpp_codegen_object_new(DialogResultMethod_t1789391825_il2cpp_TypeInfo_var);
		DialogResultMethod__ctor_m3207538458(L_15, __this, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_17 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_Show_m3109697522(NULL /*static, unused*/, 1, _stringLiteral3856372476, _stringLiteral2528879, 2, 1, L_15, (bool)0, L_16, L_17, L_18, /*hidden argument*/NULL);
		goto IL_01c3;
	}

IL_00a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_19 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)51), /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00c0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_20 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)119), /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00f3;
		}
	}

IL_00c0:
	{
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)PXMSGExample_Method_m3687707510_MethodInfo_var);
		DialogResultMethod_t1789391825 * L_22 = (DialogResultMethod_t1789391825 *)il2cpp_codegen_object_new(DialogResultMethod_t1789391825_il2cpp_TypeInfo_var);
		DialogResultMethod__ctor_m3207538458(L_22, __this, L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_Show_m3109697522(NULL /*static, unused*/, 2, _stringLiteral1427071199, _stringLiteral2403596963, 2, 2, L_22, (bool)0, _stringLiteral2443895749, _stringLiteral2011110042, L_23, /*hidden argument*/NULL);
		goto IL_01c3;
	}

IL_00f3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_24 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)52), /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_010b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_25 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)101), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_013e;
		}
	}

IL_010b:
	{
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)PXMSGExample_Method_m3687707510_MethodInfo_var);
		DialogResultMethod_t1789391825 * L_27 = (DialogResultMethod_t1789391825 *)il2cpp_codegen_object_new(DialogResultMethod_t1789391825_il2cpp_TypeInfo_var);
		DialogResultMethod__ctor_m3207538458(L_27, __this, L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_29 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_Show_m3109697522(NULL /*static, unused*/, 3, _stringLiteral294053489, _stringLiteral67232232, 0, 3, L_27, (bool)0, _stringLiteral65203672, L_28, L_29, /*hidden argument*/NULL);
		goto IL_01c3;
	}

IL_013e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_30 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)53), /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_0156;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_31 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)99), /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0189;
		}
	}

IL_0156:
	{
		IntPtr_t L_32;
		L_32.set_m_value_0((void*)(void*)PXMSGExample_Method_m3687707510_MethodInfo_var);
		DialogResultMethod_t1789391825 * L_33 = (DialogResultMethod_t1789391825 *)il2cpp_codegen_object_new(DialogResultMethod_t1789391825_il2cpp_TypeInfo_var);
		DialogResultMethod__ctor_m3207538458(L_33, __this, L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_Show_m2815667799(NULL /*static, unused*/, 4, _stringLiteral950830637, _stringLiteral1325975414, 3, 0, L_33, (bool)1, _stringLiteral2569629, _stringLiteral2799618605, _stringLiteral2011110042, /*hidden argument*/NULL);
		goto IL_01c3;
	}

IL_0189:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_34 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)109), /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_01c3;
		}
	}
	{
		IntPtr_t L_35;
		L_35.set_m_value_0((void*)(void*)PXMSGExample_Method_m3687707510_MethodInfo_var);
		DialogResultMethod_t1789391825 * L_36 = (DialogResultMethod_t1789391825 *)il2cpp_codegen_object_new(DialogResultMethod_t1789391825_il2cpp_TypeInfo_var);
		DialogResultMethod__ctor_m3207538458(L_36, __this, L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_Show_m3109697522(NULL /*static, unused*/, 0, _stringLiteral2838376053, _stringLiteral2217226694, 3, 1, L_36, (bool)0, _stringLiteral88775, _stringLiteral2529, _stringLiteral74120264, /*hidden argument*/NULL);
	}

IL_01c3:
	{
		return;
	}
}
// System.Void PXMSGExample::Method(System.Int32,PoqXert.MessageBox.DialogResult)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* DialogResult_t3964159632_il2cpp_TypeInfo_var;
extern Il2CppClass* MsgBox_t1468720533_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3113707034;
extern Il2CppCodeGenString* _stringLiteral1247637016;
extern Il2CppCodeGenString* _stringLiteral1063425;
extern const uint32_t PXMSGExample_Method_m3687707510_MetadataUsageId;
extern "C"  void PXMSGExample_Method_m3687707510 (PXMSGExample_t104414929 * __this, int32_t ___id0, int32_t ___btn1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PXMSGExample_Method_m3687707510_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3113707034);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3113707034);
		ObjectU5BU5D_t11523773* L_1 = L_0;
		int32_t L_2 = ___id0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t11523773* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral1247637016);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1247637016);
		ObjectU5BU5D_t11523773* L_6 = L_5;
		int32_t L_7 = ___btn1;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(DialogResult_t3964159632_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral1063425);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1063425);
		ObjectU5BU5D_t11523773* L_11 = L_10;
		IL2CPP_RUNTIME_CLASS_INIT(MsgBox_t1468720533_il2cpp_TypeInfo_var);
		MsgBox_t1468720533 * L_12 = MsgBox_get_instance_m3950348434(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = Object_get_name_m3709440845(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_13);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ToggleMobileControls::.ctor()
extern "C"  void ToggleMobileControls__ctor_m3534165519 (ToggleMobileControls_t612291084 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2252924356(&L_0, (1.0f), (1.0f), (1.0f), (0.5f), /*hidden argument*/NULL);
		__this->set_disabledColor_7(L_0);
		Color_t1588175760  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Color__ctor_m2252924356(&L_1, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_enabledColor_8(L_1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ToggleMobileControls::Start()
extern "C"  void ToggleMobileControls_Start_m2481303311 (ToggleMobileControls_t612291084 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)11))))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = Application_get_platform_m2918632856(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)8))))
		{
			goto IL_001e;
		}
	}

IL_0017:
	{
		__this->set_isDevice_6((bool)1);
	}

IL_001e:
	{
		bool L_2 = __this->get_isDevice_6();
		if (L_2)
		{
			goto IL_004d;
		}
	}
	{
		GameObject_t4012695102 * L_3 = __this->get_joystick_2();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_4 = __this->get_attackButton_3();
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_5 = __this->get_specialAttackButton_4();
		NullCheck(L_5);
		GameObject_SetActive_m3538205401(L_5, (bool)0, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return;
	}
}
// System.Void ToggleMobileControls::Update()
extern const MethodInfo* GameObject_GetComponent_TisImage_t3354615620_m2140199269_MethodInfo_var;
extern const uint32_t ToggleMobileControls_Update_m3911810782_MetadataUsageId;
extern "C"  void ToggleMobileControls_Update_m3911810782 (ToggleMobileControls_t612291084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ToggleMobileControls_Update_m3911810782_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Slider_t1468074762 * L_0 = __this->get_specialSldr_5();
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_0);
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_0030;
		}
	}
	{
		GameObject_t4012695102 * L_2 = __this->get_specialAttackButton_4();
		NullCheck(L_2);
		Image_t3354615620 * L_3 = GameObject_GetComponent_TisImage_t3354615620_m2140199269(L_2, /*hidden argument*/GameObject_GetComponent_TisImage_t3354615620_m2140199269_MethodInfo_var);
		Color_t1588175760  L_4 = __this->get_disabledColor_7();
		NullCheck(L_3);
		Graphic_set_color_m1311501487(L_3, L_4, /*hidden argument*/NULL);
		goto IL_0046;
	}

IL_0030:
	{
		GameObject_t4012695102 * L_5 = __this->get_specialAttackButton_4();
		NullCheck(L_5);
		Image_t3354615620 * L_6 = GameObject_GetComponent_TisImage_t3354615620_m2140199269(L_5, /*hidden argument*/GameObject_GetComponent_TisImage_t3354615620_m2140199269_MethodInfo_var);
		Color_t1588175760  L_7 = __this->get_enabledColor_8();
		NullCheck(L_6);
		Graphic_set_color_m1311501487(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// System.Void TurnOBJ::.ctor()
extern "C"  void TurnOBJ__ctor_m945877393 (TurnOBJ_t699144986 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TurnOBJ::Update()
extern "C"  void TurnOBJ_Update_m984290204 (TurnOBJ_t699144986 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_Xangle_2();
		float L_2 = __this->get_Yangle_3();
		float L_3 = __this->get_Zangle_4();
		NullCheck(L_0);
		Transform_Rotate_m3498734243(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void turnSpaceBar::.ctor()
extern "C"  void turnSpaceBar__ctor_m2873323793 (turnSpaceBar_t2231649482 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void turnSpaceBar::Update()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern const uint32_t turnSpaceBar_Update_m605586460_MetadataUsageId;
extern "C"  void turnSpaceBar_Update_m605586460 (turnSpaceBar_t2231649482 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (turnSpaceBar_Update_m605586460_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		Transform_t284553113 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_3 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_2, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_Rotate_m637363399(L_1, L_3, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void turnView::.ctor()
extern "C"  void turnView__ctor_m1382338585 (turnView_t134260930 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void turnView::Start()
extern "C"  void turnView_Start_m329476377 (turnView_t134260930 * __this, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = __this->get_chr_skin1_2();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_1 = __this->get_chr_skin2_3();
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void turnView::Update()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern const uint32_t turnView_Update_m1629685268_MetadataUsageId;
extern "C"  void turnView_Update_m1629685268 (turnView_t134260930 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (turnView_Update_m1629685268_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		Transform_t284553113 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_3 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_2, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_Rotate_m637363399(L_1, L_3, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void turnView::OnGUI()
extern Il2CppClass* GUI_t1522956648_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004822901;
extern Il2CppCodeGenString* _stringLiteral2004822902;
extern const uint32_t turnView_OnGUI_m877737235_MetadataUsageId;
extern "C"  void turnView_OnGUI_m877737235 (turnView_t134260930 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (turnView_OnGUI_m877737235_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t1525428817  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Rect__ctor_m3291325233(&L_0, (400.0f), (10.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_1 = GUI_Button_m885093907(NULL /*static, unused*/, L_0, _stringLiteral2004822901, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0045;
		}
	}
	{
		GameObject_t4012695102 * L_2 = __this->get_chr_skin1_2();
		NullCheck(L_2);
		GameObject_SetActive_m3538205401(L_2, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_3 = __this->get_chr_skin2_3();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)0, /*hidden argument*/NULL);
		goto IL_0085;
	}

IL_0045:
	{
		Rect_t1525428817  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Rect__ctor_m3291325233(&L_4, (400.0f), (50.0f), (150.0f), (30.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_5 = GUI_Button_m885093907(NULL /*static, unused*/, L_4, _stringLiteral2004822902, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0085;
		}
	}
	{
		GameObject_t4012695102 * L_6 = __this->get_chr_skin1_2();
		NullCheck(L_6);
		GameObject_SetActive_m3538205401(L_6, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_7 = __this->get_chr_skin2_3();
		NullCheck(L_7);
		GameObject_SetActive_m3538205401(L_7, (bool)1, /*hidden argument*/NULL);
	}

IL_0085:
	{
		return;
	}
}
// System.Void VolumeControl::.ctor()
extern "C"  void VolumeControl__ctor_m4118616072 (VolumeControl_t2768303299 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VolumeControl::Start()
extern const MethodInfo* GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2127272705;
extern const uint32_t VolumeControl_Start_m3065753864_MetadataUsageId;
extern "C"  void VolumeControl_Start_m3065753864 (VolumeControl_t2768303299 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VolumeControl_Start_m3065753864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2127272705, /*hidden argument*/NULL);
		NullCheck(L_0);
		GlobalObjectManager_t849077355 * L_1 = GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422(L_0, /*hidden argument*/GameObject_GetComponent_TisGlobalObjectManager_t849077355_m1854358422_MethodInfo_var);
		__this->set_persistentData_2(L_1);
		Slider_t1468074762 * L_2 = __this->get_volumeSldr_3();
		GlobalObjectManager_t849077355 * L_3 = __this->get_persistentData_2();
		NullCheck(L_3);
		float L_4 = L_3->get_volume_10();
		NullCheck(L_2);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_2, L_4);
		return;
	}
}
// System.Void VolumeControl::changeVolume()
extern const MethodInfo* GameObject_GetComponent_TisSlider_t1468074762_m2004927101_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2567951162;
extern const uint32_t VolumeControl_changeVolume_m3120926086_MetadataUsageId;
extern "C"  void VolumeControl_changeVolume_m3120926086 (VolumeControl_t2768303299 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VolumeControl_changeVolume_m3120926086_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GlobalObjectManager_t849077355 * L_0 = __this->get_persistentData_2();
		GameObject_t4012695102 * L_1 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2567951162, /*hidden argument*/NULL);
		NullCheck(L_1);
		Slider_t1468074762 * L_2 = GameObject_GetComponent_TisSlider_t1468074762_m2004927101(L_1, /*hidden argument*/GameObject_GetComponent_TisSlider_t1468074762_m2004927101_MethodInfo_var);
		NullCheck(L_2);
		float L_3 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_2);
		NullCheck(L_0);
		L_0->set_volume_10(L_3);
		GlobalObjectManager_t849077355 * L_4 = __this->get_persistentData_2();
		NullCheck(L_4);
		float L_5 = L_4->get_volume_10();
		AudioListener_set_volume_m1072709503(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
