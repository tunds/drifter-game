﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnemyAttack
struct EnemyAttack_t1231588304;
// UnityEngine.Collider
struct Collider_t955670625;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"

// System.Void EnemyAttack::.ctor()
extern "C"  void EnemyAttack__ctor_m2061187483 (EnemyAttack_t1231588304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyAttack::Awake()
extern "C"  void EnemyAttack_Awake_m2298792702 (EnemyAttack_t1231588304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyAttack::Update()
extern "C"  void EnemyAttack_Update_m1199164626 (EnemyAttack_t1231588304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyAttack::Attack()
extern "C"  void EnemyAttack_Attack_m415309457 (EnemyAttack_t1231588304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyAttack::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void EnemyAttack_OnTriggerEnter_m2539862973 (EnemyAttack_t1231588304 * __this, Collider_t955670625 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyAttack::OnTriggerExit(UnityEngine.Collider)
extern "C"  void EnemyAttack_OnTriggerExit_m701685317 (EnemyAttack_t1231588304 * __this, Collider_t955670625 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
