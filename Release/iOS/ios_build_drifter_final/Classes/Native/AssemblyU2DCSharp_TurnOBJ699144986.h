﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnOBJ
struct  TurnOBJ_t699144986  : public MonoBehaviour_t3012272455
{
public:
	// System.Single TurnOBJ::Xangle
	float ___Xangle_2;
	// System.Single TurnOBJ::Yangle
	float ___Yangle_3;
	// System.Single TurnOBJ::Zangle
	float ___Zangle_4;

public:
	inline static int32_t get_offset_of_Xangle_2() { return static_cast<int32_t>(offsetof(TurnOBJ_t699144986, ___Xangle_2)); }
	inline float get_Xangle_2() const { return ___Xangle_2; }
	inline float* get_address_of_Xangle_2() { return &___Xangle_2; }
	inline void set_Xangle_2(float value)
	{
		___Xangle_2 = value;
	}

	inline static int32_t get_offset_of_Yangle_3() { return static_cast<int32_t>(offsetof(TurnOBJ_t699144986, ___Yangle_3)); }
	inline float get_Yangle_3() const { return ___Yangle_3; }
	inline float* get_address_of_Yangle_3() { return &___Yangle_3; }
	inline void set_Yangle_3(float value)
	{
		___Yangle_3 = value;
	}

	inline static int32_t get_offset_of_Zangle_4() { return static_cast<int32_t>(offsetof(TurnOBJ_t699144986, ___Zangle_4)); }
	inline float get_Zangle_4() const { return ___Zangle_4; }
	inline float* get_address_of_Zangle_4() { return &___Zangle_4; }
	inline void set_Zangle_4(float value)
	{
		___Zangle_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
