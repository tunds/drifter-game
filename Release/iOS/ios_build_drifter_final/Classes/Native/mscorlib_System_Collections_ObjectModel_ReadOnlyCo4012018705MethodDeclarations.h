﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct ReadOnlyCollection_1_t4012018705;
// System.Collections.Generic.IList`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct IList_1_t3015365671;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// Mono.Data.Sqlite.SqliteKeyReader/KeyInfo[]
struct KeyInfoU5BU5D_t1118564256;
// System.Collections.Generic.IEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct IEnumerator_1_t2331979805;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K848873357.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2912891749_gshared (ReadOnlyCollection_1_t4012018705 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2912891749(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t4012018705 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2912891749_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2420189903_gshared (ReadOnlyCollection_1_t4012018705 * __this, KeyInfo_t848873357  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2420189903(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t4012018705 *, KeyInfo_t848873357 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2420189903_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4018133979_gshared (ReadOnlyCollection_1_t4012018705 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4018133979(__this, method) ((  void (*) (ReadOnlyCollection_1_t4012018705 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4018133979_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2126362358_gshared (ReadOnlyCollection_1_t4012018705 * __this, int32_t ___index0, KeyInfo_t848873357  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2126362358(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t4012018705 *, int32_t, KeyInfo_t848873357 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2126362358_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m80850056_gshared (ReadOnlyCollection_1_t4012018705 * __this, KeyInfo_t848873357  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m80850056(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t4012018705 *, KeyInfo_t848873357 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m80850056_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m215228_gshared (ReadOnlyCollection_1_t4012018705 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m215228(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4012018705 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m215228_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  KeyInfo_t848873357  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3724789922_gshared (ReadOnlyCollection_1_t4012018705 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3724789922(__this, ___index0, method) ((  KeyInfo_t848873357  (*) (ReadOnlyCollection_1_t4012018705 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3724789922_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m839389773_gshared (ReadOnlyCollection_1_t4012018705 * __this, int32_t ___index0, KeyInfo_t848873357  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m839389773(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4012018705 *, int32_t, KeyInfo_t848873357 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m839389773_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m80504839_gshared (ReadOnlyCollection_1_t4012018705 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m80504839(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4012018705 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m80504839_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m899834260_gshared (ReadOnlyCollection_1_t4012018705 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m899834260(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4012018705 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m899834260_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1088683555_gshared (ReadOnlyCollection_1_t4012018705 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1088683555(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4012018705 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1088683555_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m124508026_gshared (ReadOnlyCollection_1_t4012018705 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m124508026(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4012018705 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m124508026_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2019213474_gshared (ReadOnlyCollection_1_t4012018705 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2019213474(__this, method) ((  void (*) (ReadOnlyCollection_1_t4012018705 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2019213474_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m730834822_gshared (ReadOnlyCollection_1_t4012018705 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m730834822(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4012018705 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m730834822_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3852452050_gshared (ReadOnlyCollection_1_t4012018705 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3852452050(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4012018705 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3852452050_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3302684229_gshared (ReadOnlyCollection_1_t4012018705 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3302684229(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4012018705 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3302684229_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2364335811_gshared (ReadOnlyCollection_1_t4012018705 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2364335811(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t4012018705 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2364335811_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1865321813_gshared (ReadOnlyCollection_1_t4012018705 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1865321813(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4012018705 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1865321813_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1105934870_gshared (ReadOnlyCollection_1_t4012018705 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1105934870(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4012018705 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1105934870_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2287356232_gshared (ReadOnlyCollection_1_t4012018705 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2287356232(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4012018705 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2287356232_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3660903285_gshared (ReadOnlyCollection_1_t4012018705 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3660903285(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4012018705 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3660903285_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m987744484_gshared (ReadOnlyCollection_1_t4012018705 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m987744484(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4012018705 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m987744484_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m191695055_gshared (ReadOnlyCollection_1_t4012018705 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m191695055(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4012018705 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m191695055_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m512664796_gshared (ReadOnlyCollection_1_t4012018705 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m512664796(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4012018705 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m512664796_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1658741197_gshared (ReadOnlyCollection_1_t4012018705 * __this, KeyInfo_t848873357  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1658741197(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4012018705 *, KeyInfo_t848873357 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1658741197_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m766458623_gshared (ReadOnlyCollection_1_t4012018705 * __this, KeyInfoU5BU5D_t1118564256* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m766458623(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4012018705 *, KeyInfoU5BU5D_t1118564256*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m766458623_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2673357092_gshared (ReadOnlyCollection_1_t4012018705 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2673357092(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t4012018705 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2673357092_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1664601099_gshared (ReadOnlyCollection_1_t4012018705 * __this, KeyInfo_t848873357  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1664601099(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4012018705 *, KeyInfo_t848873357 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1664601099_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m4175301328_gshared (ReadOnlyCollection_1_t4012018705 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m4175301328(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t4012018705 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m4175301328_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Item(System.Int32)
extern "C"  KeyInfo_t848873357  ReadOnlyCollection_1_get_Item_m2170491746_gshared (ReadOnlyCollection_1_t4012018705 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2170491746(__this, ___index0, method) ((  KeyInfo_t848873357  (*) (ReadOnlyCollection_1_t4012018705 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2170491746_gshared)(__this, ___index0, method)
