﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2<System.Object,System.Object>
struct SortedList_2_t2672481741;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t3536813829;
// System.Object
struct Il2CppObject;
// System.Collections.ICollection
struct ICollection_t3761522009;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t346249057;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t501095600;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1541724277;
// System.Array
struct Il2CppArray;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_System_Collections_Generic_SortedList_2_Enu2625762892.h"

// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::.ctor()
extern "C"  void SortedList_2__ctor_m1200808462_gshared (SortedList_2_t2672481741 * __this, const MethodInfo* method);
#define SortedList_2__ctor_m1200808462(__this, method) ((  void (*) (SortedList_2_t2672481741 *, const MethodInfo*))SortedList_2__ctor_m1200808462_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::.ctor(System.Int32,System.Collections.Generic.IComparer`1<TKey>)
extern "C"  void SortedList_2__ctor_m3482398936_gshared (SortedList_2_t2672481741 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define SortedList_2__ctor_m3482398936(__this, ___capacity0, ___comparer1, method) ((  void (*) (SortedList_2_t2672481741 *, int32_t, Il2CppObject*, const MethodInfo*))SortedList_2__ctor_m3482398936_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
extern "C"  void SortedList_2__ctor_m1206323807_gshared (SortedList_2_t2672481741 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define SortedList_2__ctor_m1206323807(__this, ___comparer0, method) ((  void (*) (SortedList_2_t2672481741 *, Il2CppObject*, const MethodInfo*))SortedList_2__ctor_m1206323807_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::.cctor()
extern "C"  void SortedList_2__cctor_m2383227743_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define SortedList_2__cctor_m2383227743(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SortedList_2__cctor_m2383227743_gshared)(__this /* static, unused */, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool SortedList_2_System_Collections_ICollection_get_IsSynchronized_m3304970012_gshared (SortedList_2_t2672481741 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_ICollection_get_IsSynchronized_m3304970012(__this, method) ((  bool (*) (SortedList_2_t2672481741 *, const MethodInfo*))SortedList_2_System_Collections_ICollection_get_IsSynchronized_m3304970012_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * SortedList_2_System_Collections_ICollection_get_SyncRoot_m637579548_gshared (SortedList_2_t2672481741 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_ICollection_get_SyncRoot_m637579548(__this, method) ((  Il2CppObject * (*) (SortedList_2_t2672481741 *, const MethodInfo*))SortedList_2_System_Collections_ICollection_get_SyncRoot_m637579548_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * SortedList_2_System_Collections_IDictionary_get_Item_m2850762042_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_Item_m2850762042(__this, ___key0, method) ((  Il2CppObject * (*) (SortedList_2_t2672481741 *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Item_m2850762042_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void SortedList_2_System_Collections_IDictionary_set_Item_m745689567_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_set_Item_m745689567(__this, ___key0, ___value1, method) ((  void (*) (SortedList_2_t2672481741 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_set_Item_m745689567_gshared)(__this, ___key0, ___value1, method)
// System.Collections.ICollection System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * SortedList_2_System_Collections_IDictionary_get_Keys_m3098418752_gshared (SortedList_2_t2672481741 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_Keys_m3098418752(__this, method) ((  Il2CppObject * (*) (SortedList_2_t2672481741 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Keys_m3098418752_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * SortedList_2_System_Collections_IDictionary_get_Values_m3535639342_gshared (SortedList_2_t2672481741 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_Values_m3535639342(__this, method) ((  Il2CppObject * (*) (SortedList_2_t2672481741 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Values_m3535639342_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1752790112_gshared (SortedList_2_t2672481741 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1752790112(__this, method) ((  bool (*) (SortedList_2_t2672481741 *, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1752790112_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Clear()
extern "C"  void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m3644810574_gshared (SortedList_2_t2672481741 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m3644810574(__this, method) ((  void (*) (SortedList_2_t2672481741 *, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m3644810574_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m606997847_gshared (SortedList_2_t2672481741 * __this, KeyValuePair_2U5BU5D_t346249057* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m606997847(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedList_2_t2672481741 *, KeyValuePair_2U5BU5D_t346249057*, int32_t, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m606997847_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m394306611_gshared (SortedList_2_t2672481741 * __this, KeyValuePair_2_t3312956448  ___keyValuePair0, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m394306611(__this, ___keyValuePair0, method) ((  void (*) (SortedList_2_t2672481741 *, KeyValuePair_2_t3312956448 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m394306611_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m594949339_gshared (SortedList_2_t2672481741 * __this, KeyValuePair_2_t3312956448  ___keyValuePair0, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m594949339(__this, ___keyValuePair0, method) ((  bool (*) (SortedList_2_t2672481741 *, KeyValuePair_2_t3312956448 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m594949339_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m4046575296_gshared (SortedList_2_t2672481741 * __this, KeyValuePair_2_t3312956448  ___keyValuePair0, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m4046575296(__this, ___keyValuePair0, method) ((  bool (*) (SortedList_2_t2672481741 *, KeyValuePair_2_t3312956448 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m4046575296_gshared)(__this, ___keyValuePair0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m359432642_gshared (SortedList_2_t2672481741 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m359432642(__this, method) ((  Il2CppObject* (*) (SortedList_2_t2672481741 *, const MethodInfo*))SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m359432642_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SortedList_2_System_Collections_IEnumerable_GetEnumerator_m2853032453_gshared (SortedList_2_t2672481741 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IEnumerable_GetEnumerator_m2853032453(__this, method) ((  Il2CppObject * (*) (SortedList_2_t2672481741 *, const MethodInfo*))SortedList_2_System_Collections_IEnumerable_GetEnumerator_m2853032453_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void SortedList_2_System_Collections_IDictionary_Add_m3797024018_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_Add_m3797024018(__this, ___key0, ___value1, method) ((  void (*) (SortedList_2_t2672481741 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Add_m3797024018_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool SortedList_2_System_Collections_IDictionary_Contains_m1188651832_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_Contains_m1188651832(__this, ___key0, method) ((  bool (*) (SortedList_2_t2672481741 *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Contains_m1188651832_gshared)(__this, ___key0, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * SortedList_2_System_Collections_IDictionary_GetEnumerator_m1174133021_gshared (SortedList_2_t2672481741 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_GetEnumerator_m1174133021(__this, method) ((  Il2CppObject * (*) (SortedList_2_t2672481741 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_GetEnumerator_m1174133021_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void SortedList_2_System_Collections_IDictionary_Remove_m485124637_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_Remove_m485124637(__this, ___key0, method) ((  void (*) (SortedList_2_t2672481741 *, Il2CppObject *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Remove_m485124637_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void SortedList_2_System_Collections_ICollection_CopyTo_m2539372278_gshared (SortedList_2_t2672481741 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define SortedList_2_System_Collections_ICollection_CopyTo_m2539372278(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedList_2_t2672481741 *, Il2CppArray *, int32_t, const MethodInfo*))SortedList_2_System_Collections_ICollection_CopyTo_m2539372278_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t SortedList_2_get_Count_m3178594530_gshared (SortedList_2_t2672481741 * __this, const MethodInfo* method);
#define SortedList_2_get_Count_m3178594530(__this, method) ((  int32_t (*) (SortedList_2_t2672481741 *, const MethodInfo*))SortedList_2_get_Count_m3178594530_gshared)(__this, method)
// TValue System.Collections.Generic.SortedList`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * SortedList_2_get_Item_m541476809_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedList_2_get_Item_m541476809(__this, ___key0, method) ((  Il2CppObject * (*) (SortedList_2_t2672481741 *, Il2CppObject *, const MethodInfo*))SortedList_2_get_Item_m541476809_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void SortedList_2_set_Item_m500393806_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedList_2_set_Item_m500393806(__this, ___key0, ___value1, method) ((  void (*) (SortedList_2_t2672481741 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedList_2_set_Item_m500393806_gshared)(__this, ___key0, ___value1, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Object,System.Object>::get_Capacity()
extern "C"  int32_t SortedList_2_get_Capacity_m2699149161_gshared (SortedList_2_t2672481741 * __this, const MethodInfo* method);
#define SortedList_2_get_Capacity_m2699149161(__this, method) ((  int32_t (*) (SortedList_2_t2672481741 *, const MethodInfo*))SortedList_2_get_Capacity_m2699149161_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void SortedList_2_Add_m3130442343_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedList_2_Add_m3130442343(__this, ___key0, ___value1, method) ((  void (*) (SortedList_2_t2672481741 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedList_2_Add_m3130442343_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* SortedList_2_GetEnumerator_m3467844521_gshared (SortedList_2_t2672481741 * __this, const MethodInfo* method);
#define SortedList_2_GetEnumerator_m3467844521(__this, method) ((  Il2CppObject* (*) (SortedList_2_t2672481741 *, const MethodInfo*))SortedList_2_GetEnumerator_m3467844521_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::Clear()
extern "C"  void SortedList_2_Clear_m2901909049_gshared (SortedList_2_t2672481741 * __this, const MethodInfo* method);
#define SortedList_2_Clear_m2901909049(__this, method) ((  void (*) (SortedList_2_t2672481741 *, const MethodInfo*))SortedList_2_Clear_m2901909049_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::RemoveAt(System.Int32)
extern "C"  void SortedList_2_RemoveAt_m3903188702_gshared (SortedList_2_t2672481741 * __this, int32_t ___index0, const MethodInfo* method);
#define SortedList_2_RemoveAt_m3903188702(__this, ___index0, method) ((  void (*) (SortedList_2_t2672481741 *, int32_t, const MethodInfo*))SortedList_2_RemoveAt_m3903188702_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Object,System.Object>::IndexOfKey(TKey)
extern "C"  int32_t SortedList_2_IndexOfKey_m2478029361_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedList_2_IndexOfKey_m2478029361(__this, ___key0, method) ((  int32_t (*) (SortedList_2_t2672481741 *, Il2CppObject *, const MethodInfo*))SortedList_2_IndexOfKey_m2478029361_gshared)(__this, ___key0, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Object,System.Object>::IndexOfValue(TValue)
extern "C"  int32_t SortedList_2_IndexOfValue_m3058994609_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SortedList_2_IndexOfValue_m3058994609(__this, ___value0, method) ((  int32_t (*) (SortedList_2_t2672481741 *, Il2CppObject *, const MethodInfo*))SortedList_2_IndexOfValue_m3058994609_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool SortedList_2_TryGetValue_m1119161636_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define SortedList_2_TryGetValue_m1119161636(__this, ___key0, ___value1, method) ((  bool (*) (SortedList_2_t2672481741 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))SortedList_2_TryGetValue_m1119161636_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::EnsureCapacity(System.Int32,System.Int32)
extern "C"  void SortedList_2_EnsureCapacity_m3344536216_gshared (SortedList_2_t2672481741 * __this, int32_t ___n0, int32_t ___free1, const MethodInfo* method);
#define SortedList_2_EnsureCapacity_m3344536216(__this, ___n0, ___free1, method) ((  void (*) (SortedList_2_t2672481741 *, int32_t, int32_t, const MethodInfo*))SortedList_2_EnsureCapacity_m3344536216_gshared)(__this, ___n0, ___free1, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::PutImpl(TKey,TValue,System.Boolean)
extern "C"  void SortedList_2_PutImpl_m4084697316_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, bool ___overwrite2, const MethodInfo* method);
#define SortedList_2_PutImpl_m4084697316(__this, ___key0, ___value1, ___overwrite2, method) ((  void (*) (SortedList_2_t2672481741 *, Il2CppObject *, Il2CppObject *, bool, const MethodInfo*))SortedList_2_PutImpl_m4084697316_gshared)(__this, ___key0, ___value1, ___overwrite2, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::Init(System.Collections.Generic.IComparer`1<TKey>,System.Int32,System.Boolean)
extern "C"  void SortedList_2_Init_m673053725_gshared (SortedList_2_t2672481741 * __this, Il2CppObject* ___comparer0, int32_t ___capacity1, bool ___forceSize2, const MethodInfo* method);
#define SortedList_2_Init_m673053725(__this, ___comparer0, ___capacity1, ___forceSize2, method) ((  void (*) (SortedList_2_t2672481741 *, Il2CppObject*, int32_t, bool, const MethodInfo*))SortedList_2_Init_m673053725_gshared)(__this, ___comparer0, ___capacity1, ___forceSize2, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::CopyToArray(System.Array,System.Int32,System.Collections.Generic.SortedList`2/EnumeratorMode<TKey,TValue>)
extern "C"  void SortedList_2_CopyToArray_m1964302598_gshared (SortedList_2_t2672481741 * __this, Il2CppArray * ___arr0, int32_t ___i1, int32_t ___mode2, const MethodInfo* method);
#define SortedList_2_CopyToArray_m1964302598(__this, ___arr0, ___i1, ___mode2, method) ((  void (*) (SortedList_2_t2672481741 *, Il2CppArray *, int32_t, int32_t, const MethodInfo*))SortedList_2_CopyToArray_m1964302598_gshared)(__this, ___arr0, ___i1, ___mode2, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Object,System.Object>::Find(TKey)
extern "C"  int32_t SortedList_2_Find_m2098903572_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedList_2_Find_m2098903572(__this, ___key0, method) ((  int32_t (*) (SortedList_2_t2672481741 *, Il2CppObject *, const MethodInfo*))SortedList_2_Find_m2098903572_gshared)(__this, ___key0, method)
// TKey System.Collections.Generic.SortedList`2<System.Object,System.Object>::ToKey(System.Object)
extern "C"  Il2CppObject * SortedList_2_ToKey_m1200255192_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedList_2_ToKey_m1200255192(__this, ___key0, method) ((  Il2CppObject * (*) (SortedList_2_t2672481741 *, Il2CppObject *, const MethodInfo*))SortedList_2_ToKey_m1200255192_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.SortedList`2<System.Object,System.Object>::ToValue(System.Object)
extern "C"  Il2CppObject * SortedList_2_ToValue_m490284596_gshared (SortedList_2_t2672481741 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SortedList_2_ToValue_m490284596(__this, ___value0, method) ((  Il2CppObject * (*) (SortedList_2_t2672481741 *, Il2CppObject *, const MethodInfo*))SortedList_2_ToValue_m490284596_gshared)(__this, ___value0, method)
// TKey System.Collections.Generic.SortedList`2<System.Object,System.Object>::KeyAt(System.Int32)
extern "C"  Il2CppObject * SortedList_2_KeyAt_m1318479387_gshared (SortedList_2_t2672481741 * __this, int32_t ___index0, const MethodInfo* method);
#define SortedList_2_KeyAt_m1318479387(__this, ___index0, method) ((  Il2CppObject * (*) (SortedList_2_t2672481741 *, int32_t, const MethodInfo*))SortedList_2_KeyAt_m1318479387_gshared)(__this, ___index0, method)
// TValue System.Collections.Generic.SortedList`2<System.Object,System.Object>::ValueAt(System.Int32)
extern "C"  Il2CppObject * SortedList_2_ValueAt_m935040511_gshared (SortedList_2_t2672481741 * __this, int32_t ___index0, const MethodInfo* method);
#define SortedList_2_ValueAt_m935040511(__this, ___index0, method) ((  Il2CppObject * (*) (SortedList_2_t2672481741 *, int32_t, const MethodInfo*))SortedList_2_ValueAt_m935040511_gshared)(__this, ___index0, method)
