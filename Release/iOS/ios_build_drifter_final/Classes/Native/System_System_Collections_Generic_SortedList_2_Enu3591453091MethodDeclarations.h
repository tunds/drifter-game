﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>
struct Enumerator_t3591453094;
// System.Collections.Generic.SortedList`2<System.Object,System.Object>
struct SortedList_2_t2672481741;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedList_2_Enu2625762892.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"

// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>,System.Collections.Generic.SortedList`2/EnumeratorMode<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4182471073_gshared (Enumerator_t3591453094 * __this, SortedList_2_t2672481741 * ___host0, int32_t ___mode1, const MethodInfo* method);
#define Enumerator__ctor_m4182471073(__this, ___host0, ___mode1, method) ((  void (*) (Enumerator_t3591453094 *, SortedList_2_t2672481741 *, int32_t, const MethodInfo*))Enumerator__ctor_m4182471073_gshared)(__this, ___host0, ___mode1, method)
// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::.cctor()
extern "C"  void Enumerator__cctor_m1049821430_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Enumerator__cctor_m1049821430(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Enumerator__cctor_m1049821430_gshared)(__this /* static, unused */, method)
// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m2960648260_gshared (Enumerator_t3591453094 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2960648260(__this, method) ((  void (*) (Enumerator_t3591453094 *, const MethodInfo*))Enumerator_Reset_m2960648260_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1453053403_gshared (Enumerator_t3591453094 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1453053403(__this, method) ((  bool (*) (Enumerator_t3591453094 *, const MethodInfo*))Enumerator_MoveNext_m1453053403_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t130027246  Enumerator_get_Entry_m615092953_gshared (Enumerator_t3591453094 * __this, const MethodInfo* method);
#define Enumerator_get_Entry_m615092953(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t3591453094 *, const MethodInfo*))Enumerator_get_Entry_m615092953_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * Enumerator_get_Key_m740741856_gshared (Enumerator_t3591453094 * __this, const MethodInfo* method);
#define Enumerator_get_Key_m740741856(__this, method) ((  Il2CppObject * (*) (Enumerator_t3591453094 *, const MethodInfo*))Enumerator_get_Key_m740741856_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * Enumerator_get_Value_m4231756146_gshared (Enumerator_t3591453094 * __this, const MethodInfo* method);
#define Enumerator_get_Value_m4231756146(__this, method) ((  Il2CppObject * (*) (Enumerator_t3591453094 *, const MethodInfo*))Enumerator_get_Value_m4231756146_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m808317818_gshared (Enumerator_t3591453094 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m808317818(__this, method) ((  Il2CppObject * (*) (Enumerator_t3591453094 *, const MethodInfo*))Enumerator_get_Current_m808317818_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Object,System.Object>::Clone()
extern "C"  Il2CppObject * Enumerator_Clone_m1212221127_gshared (Enumerator_t3591453094 * __this, const MethodInfo* method);
#define Enumerator_Clone_m1212221127(__this, method) ((  Il2CppObject * (*) (Enumerator_t3591453094 *, const MethodInfo*))Enumerator_Clone_m1212221127_gshared)(__this, method)
