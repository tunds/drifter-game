﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.EnterpriseServices.ApplicationIDAttribute
struct ApplicationIDAttribute_t1175707099;
// System.String
struct String_t;
// System.EnterpriseServices.ApplicationNameAttribute
struct ApplicationNameAttribute_t3723904587;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_EnterpriseServices_U3CModuleU3E86524790.h"
#include "System_EnterpriseServices_U3CModuleU3E86524790MethodDeclarations.h"
#include "System_EnterpriseServices_System_EnterpriseService1175707099.h"
#include "System_EnterpriseServices_System_EnterpriseService1175707099MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Attribute498693649MethodDeclarations.h"
#include "mscorlib_System_Guid2778838590MethodDeclarations.h"
#include "mscorlib_System_Guid2778838590.h"
#include "System_EnterpriseServices_System_EnterpriseService3723904587.h"
#include "System_EnterpriseServices_System_EnterpriseService3723904587MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.EnterpriseServices.ApplicationIDAttribute::.ctor(System.String)
extern "C"  void ApplicationIDAttribute__ctor_m2767770904 (ApplicationIDAttribute_t1175707099 * __this, String_t* ___guid0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___guid0;
		Guid_t2778838590  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Guid__ctor_m1994687478(&L_1, L_0, /*hidden argument*/NULL);
		__this->set_guid_0(L_1);
		return;
	}
}
// System.Void System.EnterpriseServices.ApplicationNameAttribute::.ctor(System.String)
extern "C"  void ApplicationNameAttribute__ctor_m1566568328 (ApplicationNameAttribute_t3723904587 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_name_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
