﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.ResourceManager
struct ResourceManager_t1361280801;
// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t1882292308;
// System.Collections.Hashtable
struct Hashtable_t3875263730;
// System.Globalization.CultureInfo
struct CultureInfo_t3603717042;
// System.IO.Stream
struct Stream_t219029575;
// System.Resources.ResourceSet
struct ResourceSet_t3790468310;
// System.Version
struct Version_t497901645;
// System.Resources.MissingManifestResourceException
struct MissingManifestResourceException_t1089815118;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Reflection_Assembly1882292308.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042.h"

// System.Void System.Resources.ResourceManager::.ctor()
extern "C"  void ResourceManager__ctor_m1041657915 (ResourceManager_t1361280801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceManager::.ctor(System.String,System.Reflection.Assembly)
extern "C"  void ResourceManager__ctor_m2505354837 (ResourceManager_t1361280801 * __this, String_t* ___baseName0, Assembly_t1882292308 * ___assembly1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceManager::.cctor()
extern "C"  void ResourceManager__cctor_m1744528082 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable System.Resources.ResourceManager::GetResourceSets(System.Reflection.Assembly,System.String)
extern "C"  Hashtable_t3875263730 * ResourceManager_GetResourceSets_m3015585469 (Il2CppObject * __this /* static, unused */, Assembly_t1882292308 * ___assembly0, String_t* ___basename1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.ResourceManager::GetString(System.String,System.Globalization.CultureInfo)
extern "C"  String_t* ResourceManager_GetString_m2820303697 (ResourceManager_t1361280801 * __this, String_t* ___name0, CultureInfo_t3603717042 * ___culture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.ResourceManager::GetResourceFileName(System.Globalization.CultureInfo)
extern "C"  String_t* ResourceManager_GetResourceFileName_m2700287441 (ResourceManager_t1361280801 * __this, CultureInfo_t3603717042 * ___culture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.ResourceManager::GetResourceFilePath(System.Globalization.CultureInfo)
extern "C"  String_t* ResourceManager_GetResourceFilePath_m3319603435 (ResourceManager_t1361280801 * __this, CultureInfo_t3603717042 * ___culture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Resources.ResourceManager::GetManifestResourceStreamNoCase(System.Reflection.Assembly,System.String)
extern "C"  Stream_t219029575 * ResourceManager_GetManifestResourceStreamNoCase_m1178585581 (ResourceManager_t1361280801 * __this, Assembly_t1882292308 * ___ass0, String_t* ___fn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.ResourceSet System.Resources.ResourceManager::InternalGetResourceSet(System.Globalization.CultureInfo,System.Boolean,System.Boolean)
extern "C"  ResourceSet_t3790468310 * ResourceManager_InternalGetResourceSet_m21708179 (ResourceManager_t1361280801 * __this, CultureInfo_t3603717042 * ___culture0, bool ___createIfNotExists1, bool ___tryParents2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Resources.ResourceManager::GetNeutralResourcesLanguage(System.Reflection.Assembly)
extern "C"  CultureInfo_t3603717042 * ResourceManager_GetNeutralResourcesLanguage_m2512021728 (Il2CppObject * __this /* static, unused */, Assembly_t1882292308 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Version System.Resources.ResourceManager::GetSatelliteContractVersion(System.Reflection.Assembly)
extern "C"  Version_t497901645 * ResourceManager_GetSatelliteContractVersion_m1327211774 (Il2CppObject * __this /* static, unused */, Assembly_t1882292308 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.MissingManifestResourceException System.Resources.ResourceManager::AssemblyResourceMissing(System.String)
extern "C"  MissingManifestResourceException_t1089815118 * ResourceManager_AssemblyResourceMissing_m421195990 (ResourceManager_t1361280801 * __this, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.ResourceManager::GetManifestResourceName(System.String)
extern "C"  String_t* ResourceManager_GetManifestResourceName_m4074426734 (ResourceManager_t1361280801 * __this, String_t* ___fn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
