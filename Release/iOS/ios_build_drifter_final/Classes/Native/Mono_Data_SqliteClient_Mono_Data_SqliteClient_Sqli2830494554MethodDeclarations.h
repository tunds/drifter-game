﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.SqliteClient.SqliteConnection
struct SqliteConnection_t2830494554;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Text.Encoding
struct Encoding_t180559927;
// System.Data.Common.DbTransaction
struct DbTransaction_t4162579344;
// System.Data.Common.DbCommand
struct DbCommand_t2323745021;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "System_Data_System_Data_ConnectionState270608870.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_Data_System_Data_IsolationLevel3729656617.h"

// System.Void Mono.Data.SqliteClient.SqliteConnection::.ctor()
extern "C"  void SqliteConnection__ctor_m4080992609 (SqliteConnection_t2830494554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteConnection::.ctor(System.String)
extern "C"  void SqliteConnection__ctor_m3562382145 (SqliteConnection_t2830494554 * __this, String_t* ___connstring0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.SqliteClient.SqliteConnection::System.ICloneable.Clone()
extern "C"  Il2CppObject * SqliteConnection_System_ICloneable_Clone_m1703326480 (SqliteConnection_t2830494554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteConnection::Dispose(System.Boolean)
extern "C"  void SqliteConnection_Dispose_m3972797013 (SqliteConnection_t2830494554 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.SqliteClient.SqliteConnection::get_ConnectionString()
extern "C"  String_t* SqliteConnection_get_ConnectionString_m3335658776 (SqliteConnection_t2830494554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteConnection::set_ConnectionString(System.String)
extern "C"  void SqliteConnection_set_ConnectionString_m2467624915 (SqliteConnection_t2830494554 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.ConnectionState Mono.Data.SqliteClient.SqliteConnection::get_State()
extern "C"  int32_t SqliteConnection_get_State_m2282736236 (SqliteConnection_t2830494554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding Mono.Data.SqliteClient.SqliteConnection::get_Encoding()
extern "C"  Encoding_t180559927 * SqliteConnection_get_Encoding_m1059375315 (SqliteConnection_t2830494554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.SqliteClient.SqliteConnection::get_Version()
extern "C"  int32_t SqliteConnection_get_Version_m2626162688 (SqliteConnection_t2830494554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Mono.Data.SqliteClient.SqliteConnection::get_Handle()
extern "C"  IntPtr_t SqliteConnection_get_Handle_m3026649183 (SqliteConnection_t2830494554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteConnection::SetConnectionString(System.String)
extern "C"  void SqliteConnection_SetConnectionString_m3238925874 (SqliteConnection_t2830494554 * __this, String_t* ___connstring0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteConnection::StartExec()
extern "C"  void SqliteConnection_StartExec_m3816599250 (SqliteConnection_t2830494554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteConnection::EndExec()
extern "C"  void SqliteConnection_EndExec_m1387854859 (SqliteConnection_t2830494554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbTransaction Mono.Data.SqliteClient.SqliteConnection::BeginDbTransaction(System.Data.IsolationLevel)
extern "C"  DbTransaction_t4162579344 * SqliteConnection_BeginDbTransaction_m673728864 (SqliteConnection_t2830494554 * __this, int32_t ___il0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteConnection::Close()
extern "C"  void SqliteConnection_Close_m1496884855 (SqliteConnection_t2830494554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand Mono.Data.SqliteClient.SqliteConnection::CreateDbCommand()
extern "C"  DbCommand_t2323745021 * SqliteConnection_CreateDbCommand_m2954565122 (SqliteConnection_t2830494554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteConnection::Open()
extern "C"  void SqliteConnection_Open_m3720361869 (SqliteConnection_t2830494554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
