﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3687821472.h"
#include "mscorlib_System_Array2840145358.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Field3780330969.h"

// System.Void System.Array/InternalEnumerator`1<SQLiteDatabase.SQLiteDB/DB_Field>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2705732104_gshared (InternalEnumerator_1_t3687821472 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2705732104(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3687821472 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2705732104_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1647670936_gshared (InternalEnumerator_1_t3687821472 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1647670936(__this, method) ((  void (*) (InternalEnumerator_1_t3687821472 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1647670936_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3685681860_gshared (InternalEnumerator_1_t3687821472 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3685681860(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3687821472 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3685681860_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SQLiteDatabase.SQLiteDB/DB_Field>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1389438431_gshared (InternalEnumerator_1_t3687821472 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1389438431(__this, method) ((  void (*) (InternalEnumerator_1_t3687821472 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1389438431_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SQLiteDatabase.SQLiteDB/DB_Field>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2017934468_gshared (InternalEnumerator_1_t3687821472 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2017934468(__this, method) ((  bool (*) (InternalEnumerator_1_t3687821472 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2017934468_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SQLiteDatabase.SQLiteDB/DB_Field>::get_Current()
extern "C"  DB_Field_t3780330969  InternalEnumerator_1_get_Current_m3325494927_gshared (InternalEnumerator_1_t3687821472 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3325494927(__this, method) ((  DB_Field_t3780330969  (*) (InternalEnumerator_1_t3687821472 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3325494927_gshared)(__this, method)
