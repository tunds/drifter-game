﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterMotor/$SubtractNewPlatformVelocity$1/$
struct U24_t36;
// CharacterMotor
struct CharacterMotor_t3633310444;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_CharacterMotor3633310444.h"

// System.Void CharacterMotor/$SubtractNewPlatformVelocity$1/$::.ctor(CharacterMotor)
extern "C"  void U24__ctor_m3380355821 (U24_t36 * __this, CharacterMotor_t3633310444 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CharacterMotor/$SubtractNewPlatformVelocity$1/$::MoveNext()
extern "C"  bool U24_MoveNext_m3451948657 (U24_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
