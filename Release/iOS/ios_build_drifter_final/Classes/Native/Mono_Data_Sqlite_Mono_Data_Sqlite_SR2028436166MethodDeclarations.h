﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.ResourceManager
struct ResourceManager_t1361280801;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Resources.ResourceManager Mono.Data.Sqlite.SR::get_ResourceManager()
extern "C"  ResourceManager_t1361280801 * SR_get_ResourceManager_m1466818036 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SR::get_DataTypes()
extern "C"  String_t* SR_get_DataTypes_m2351531571 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SR::get_Keywords()
extern "C"  String_t* SR_get_Keywords_m1356220360 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SR::get_MetaDataCollections()
extern "C"  String_t* SR_get_MetaDataCollections_m420173962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
