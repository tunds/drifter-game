﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Copy._2D.Platformer2DUserControl
struct Platformer2DUserControl_t3438281707;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::.ctor()
extern "C"  void Platformer2DUserControl__ctor_m2694631724 (Platformer2DUserControl_t3438281707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::Awake()
extern "C"  void Platformer2DUserControl_Awake_m2932236943 (Platformer2DUserControl_t3438281707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::Update()
extern "C"  void Platformer2DUserControl_Update_m3656066913 (Platformer2DUserControl_t3438281707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::FixedUpdate()
extern "C"  void Platformer2DUserControl_FixedUpdate_m2677546407 (Platformer2DUserControl_t3438281707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
