﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1904240784.h"
#include "mscorlib_System_Array2840145358.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteTypeNames1996750281.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SQLiteTypeNames>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3247463978_gshared (InternalEnumerator_1_t1904240784 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3247463978(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1904240784 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3247463978_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SQLiteTypeNames>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3456254774_gshared (InternalEnumerator_1_t1904240784 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3456254774(__this, method) ((  void (*) (InternalEnumerator_1_t1904240784 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3456254774_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SQLiteTypeNames>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1540057314_gshared (InternalEnumerator_1_t1904240784 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1540057314(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1904240784 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1540057314_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SQLiteTypeNames>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4223382657_gshared (InternalEnumerator_1_t1904240784 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4223382657(__this, method) ((  void (*) (InternalEnumerator_1_t1904240784 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4223382657_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SQLiteTypeNames>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3970859554_gshared (InternalEnumerator_1_t1904240784 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3970859554(__this, method) ((  bool (*) (InternalEnumerator_1_t1904240784 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3970859554_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Data.Sqlite.SQLiteTypeNames>::get_Current()
extern "C"  SQLiteTypeNames_t1996750281  InternalEnumerator_1_get_Current_m3289740337_gshared (InternalEnumerator_1_t1904240784 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3289740337(__this, method) ((  SQLiteTypeNames_t1996750281  (*) (InternalEnumerator_1_t1904240784 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3289740337_gshared)(__this, method)
