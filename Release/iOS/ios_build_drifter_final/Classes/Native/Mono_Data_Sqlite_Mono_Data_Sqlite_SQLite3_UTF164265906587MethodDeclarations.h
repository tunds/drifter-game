﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLite3_UTF16
struct SQLite3_UTF16_t4265906587;
// System.String
struct String_t;
// Mono.Data.Sqlite.SqliteStatement
struct SqliteStatement_t3906494218;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteDateFormat4009100393.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SQLiteOpenFlagsE1905737881.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteStatement3906494218.h"
#include "mscorlib_System_DateTime339033936.h"

// System.Void Mono.Data.Sqlite.SQLite3_UTF16::.ctor(Mono.Data.Sqlite.SQLiteDateFormats)
extern "C"  void SQLite3_UTF16__ctor_m1230403278 (SQLite3_UTF16_t4265906587 * __this, int32_t ___fmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3_UTF16::ToString(System.IntPtr,System.Int32)
extern "C"  String_t* SQLite3_UTF16_ToString_m124785167 (SQLite3_UTF16_t4265906587 * __this, IntPtr_t ___b0, int32_t ___nbytelen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3_UTF16::UTF16ToString(System.IntPtr,System.Int32)
extern "C"  String_t* SQLite3_UTF16_UTF16ToString_m2538215389 (Il2CppObject * __this /* static, unused */, IntPtr_t ___b0, int32_t ___nbytelen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3_UTF16::Open(System.String,Mono.Data.Sqlite.SQLiteOpenFlagsEnum,System.Int32,System.Boolean)
extern "C"  void SQLite3_UTF16_Open_m1963054472 (SQLite3_UTF16_t4265906587 * __this, String_t* ___strFilename0, int32_t ___flags1, int32_t ___maxPoolSize2, bool ___usePool3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3_UTF16::Bind_DateTime(Mono.Data.Sqlite.SqliteStatement,System.Int32,System.DateTime)
extern "C"  void SQLite3_UTF16_Bind_DateTime_m1900397409 (SQLite3_UTF16_t4265906587 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, DateTime_t339033936  ___dt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3_UTF16::Bind_Text(Mono.Data.Sqlite.SqliteStatement,System.Int32,System.String)
extern "C"  void SQLite3_UTF16_Bind_Text_m4056421145 (SQLite3_UTF16_t4265906587 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Data.Sqlite.SQLite3_UTF16::GetDateTime(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  DateTime_t339033936  SQLite3_UTF16_GetDateTime_m2297571444 (SQLite3_UTF16_t4265906587 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3_UTF16::ColumnName(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  String_t* SQLite3_UTF16_ColumnName_m2614978672 (SQLite3_UTF16_t4265906587 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3_UTF16::GetText(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  String_t* SQLite3_UTF16_GetText_m4066978860 (SQLite3_UTF16_t4265906587 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3_UTF16::ColumnOriginalName(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  String_t* SQLite3_UTF16_ColumnOriginalName_m2461537247 (SQLite3_UTF16_t4265906587 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3_UTF16::ColumnDatabaseName(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  String_t* SQLite3_UTF16_ColumnDatabaseName_m1304473589 (SQLite3_UTF16_t4265906587 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3_UTF16::ColumnTableName(Mono.Data.Sqlite.SqliteStatement,System.Int32)
extern "C"  String_t* SQLite3_UTF16_ColumnTableName_m1559488012 (SQLite3_UTF16_t4265906587 * __this, SqliteStatement_t3906494218 * ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SQLite3_UTF16::GetParamValueText(System.IntPtr)
extern "C"  String_t* SQLite3_UTF16_GetParamValueText_m1361777483 (SQLite3_UTF16_t4265906587 * __this, IntPtr_t ___ptr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3_UTF16::ReturnError(System.IntPtr,System.String)
extern "C"  void SQLite3_UTF16_ReturnError_m515362483 (SQLite3_UTF16_t4265906587 * __this, IntPtr_t ___context0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLite3_UTF16::ReturnText(System.IntPtr,System.String)
extern "C"  void SQLite3_UTF16_ReturnText_m2172645526 (SQLite3_UTF16_t4265906587 * __this, IntPtr_t ___context0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
