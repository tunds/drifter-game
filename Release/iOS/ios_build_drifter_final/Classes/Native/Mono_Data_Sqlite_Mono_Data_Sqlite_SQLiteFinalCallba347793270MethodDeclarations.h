﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteFinalCallback
struct SQLiteFinalCallback_t347793270;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void Mono.Data.Sqlite.SQLiteFinalCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void SQLiteFinalCallback__ctor_m389493814 (SQLiteFinalCallback_t347793270 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteFinalCallback::Invoke(System.IntPtr)
extern "C"  void SQLiteFinalCallback_Invoke_m11077188 (SQLiteFinalCallback_t347793270 * __this, IntPtr_t ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_SQLiteFinalCallback_t347793270(Il2CppObject* delegate, IntPtr_t ___context0);
// System.IAsyncResult Mono.Data.Sqlite.SQLiteFinalCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SQLiteFinalCallback_BeginInvoke_m3590744037 (SQLiteFinalCallback_t347793270 * __this, IntPtr_t ___context0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteFinalCallback::EndInvoke(System.IAsyncResult)
extern "C"  void SQLiteFinalCallback_EndInvoke_m461975878 (SQLiteFinalCallback_t347793270 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
