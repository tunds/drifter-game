﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityStandardAssets.Water.WaterBase
struct WaterBase_t2511321362;
// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.Camera
struct Camera_t3533968274;
// UnityStandardAssets.Water.WaterBasic
struct WaterBasic_t354635297;
// UnityEngine.Renderer
struct Renderer_t1092684080;
// System.Object
struct Il2CppObject;
// UnityStandardAssets.Water.WaterTile
struct WaterTile_t2511865071;
// UnityStandardAssets.Water.PlanarReflection
struct PlanarReflection_t991663;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1789603642.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1789603642MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2511321362.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2511321362MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_W799476914.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_Material1886596500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader3998140498MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemInfo4158905322MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera3533968274MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material1886596500.h"
#include "UnityEngine_UnityEngine_Shader3998140498.h"
#include "mscorlib_System_Int322847414787.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat3253344041.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_Camera3533968274.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "UnityEngine_UnityEngine_DepthTextureMode2168306440.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_W354635297.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_W354635297MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer1092684080MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time1525492538MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector43525329790MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf1597001355MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer1092684080.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"
#include "mscorlib_System_Single958209021.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_W799476914MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2511865071.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2511865071MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Wate991663.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Wate991663MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t1092684080_m1748555263(__this, method) ((  Renderer_t1092684080 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Water.PlanarReflection>()
#define Component_GetComponent_TisPlanarReflection_t991663_m4268723140(__this, method) ((  PlanarReflection_t991663 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Water.WaterBase>()
#define Component_GetComponent_TisWaterBase_t2511321362_m2122905197(__this, method) ((  WaterBase_t2511321362 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Water.WaterBase::.ctor()
extern "C"  void WaterBase__ctor_m3514809247 (WaterBase_t2511321362 * __this, const MethodInfo* method)
{
	{
		__this->set_waterQuality_3(2);
		__this->set_edgeBlend_4((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterBase::UpdateShader()
extern Il2CppCodeGenString* _stringLiteral2908883890;
extern Il2CppCodeGenString* _stringLiteral4276054492;
extern const uint32_t WaterBase_UpdateShader_m2829669459_MetadataUsageId;
extern "C"  void WaterBase_UpdateShader_m2829669459 (WaterBase_t2511321362 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WaterBase_UpdateShader_m2829669459_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_waterQuality_3();
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0026;
		}
	}
	{
		Material_t1886596500 * L_1 = __this->get_sharedMaterial_2();
		NullCheck(L_1);
		Shader_t3998140498 * L_2 = Material_get_shader_m2881845503(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Shader_set_maximumLOD_m2797626507(L_2, ((int32_t)501), /*hidden argument*/NULL);
		goto IL_0061;
	}

IL_0026:
	{
		int32_t L_3 = __this->get_waterQuality_3();
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_004c;
		}
	}
	{
		Material_t1886596500 * L_4 = __this->get_sharedMaterial_2();
		NullCheck(L_4);
		Shader_t3998140498 * L_5 = Material_get_shader_m2881845503(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Shader_set_maximumLOD_m2797626507(L_5, ((int32_t)301), /*hidden argument*/NULL);
		goto IL_0061;
	}

IL_004c:
	{
		Material_t1886596500 * L_6 = __this->get_sharedMaterial_2();
		NullCheck(L_6);
		Shader_t3998140498 * L_7 = Material_get_shader_m2881845503(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Shader_set_maximumLOD_m2797626507(L_7, ((int32_t)201), /*hidden argument*/NULL);
	}

IL_0061:
	{
		bool L_8 = SystemInfo_SupportsRenderTextureFormat_m1773213581(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0073;
		}
	}
	{
		__this->set_edgeBlend_4((bool)0);
	}

IL_0073:
	{
		bool L_9 = __this->get_edgeBlend_4();
		if (!L_9)
		{
			goto IL_00b8;
		}
	}
	{
		Shader_EnableKeyword_m944528214(NULL /*static, unused*/, _stringLiteral2908883890, /*hidden argument*/NULL);
		Shader_DisableKeyword_m2163321765(NULL /*static, unused*/, _stringLiteral4276054492, /*hidden argument*/NULL);
		Camera_t3533968274 * L_10 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_11 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00b3;
		}
	}
	{
		Camera_t3533968274 * L_12 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t3533968274 * L_13 = L_12;
		NullCheck(L_13);
		int32_t L_14 = Camera_get_depthTextureMode_m2117446653(L_13, /*hidden argument*/NULL);
		NullCheck(L_13);
		Camera_set_depthTextureMode_m2368326786(L_13, ((int32_t)((int32_t)L_14|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_00b3:
	{
		goto IL_00cc;
	}

IL_00b8:
	{
		Shader_EnableKeyword_m944528214(NULL /*static, unused*/, _stringLiteral4276054492, /*hidden argument*/NULL);
		Shader_DisableKeyword_m2163321765(NULL /*static, unused*/, _stringLiteral2908883890, /*hidden argument*/NULL);
	}

IL_00cc:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterBase::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern "C"  void WaterBase_WaterTileBeingRendered_m4064990337 (WaterBase_t2511321362 * __this, Transform_t284553113 * ___tr0, Camera_t3533968274 * ___currentCam1, const MethodInfo* method)
{
	{
		Camera_t3533968274 * L_0 = ___currentCam1;
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		bool L_2 = __this->get_edgeBlend_4();
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		Camera_t3533968274 * L_3 = ___currentCam1;
		Camera_t3533968274 * L_4 = L_3;
		NullCheck(L_4);
		int32_t L_5 = Camera_get_depthTextureMode_m2117446653(L_4, /*hidden argument*/NULL);
		NullCheck(L_4);
		Camera_set_depthTextureMode_m2368326786(L_4, ((int32_t)((int32_t)L_5|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterBase::Update()
extern "C"  void WaterBase_Update_m3311766350 (WaterBase_t2511321362 * __this, const MethodInfo* method)
{
	{
		Material_t1886596500 * L_0 = __this->get_sharedMaterial_2();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		WaterBase_UpdateShader_m2829669459(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterBasic::.ctor()
extern "C"  void WaterBasic__ctor_m2683650792 (WaterBasic_t354635297 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterBasic::Update()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t1092684080_m1748555263_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3537014958;
extern Il2CppCodeGenString* _stringLiteral2047710258;
extern Il2CppCodeGenString* _stringLiteral3237885611;
extern const uint32_t WaterBasic_Update_m3315658021_MetadataUsageId;
extern "C"  void WaterBasic_Update_m3315658021 (WaterBasic_t354635297 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WaterBasic_Update_m3315658021_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Renderer_t1092684080 * V_0 = NULL;
	Material_t1886596500 * V_1 = NULL;
	Vector4_t3525329790  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Vector4_t3525329790  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector4_t3525329790  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		Renderer_t1092684080 * L_0 = Component_GetComponent_TisRenderer_t1092684080_m1748555263(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t1092684080_m1748555263_MethodInfo_var);
		V_0 = L_0;
		Renderer_t1092684080 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Renderer_t1092684080 * L_3 = V_0;
		NullCheck(L_3);
		Material_t1886596500 * L_4 = Renderer_get_sharedMaterial_m835478880(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		Material_t1886596500 * L_5 = V_1;
		bool L_6 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0026;
		}
	}
	{
		return;
	}

IL_0026:
	{
		Material_t1886596500 * L_7 = V_1;
		NullCheck(L_7);
		Vector4_t3525329790  L_8 = Material_GetVector_m4092100414(L_7, _stringLiteral3537014958, /*hidden argument*/NULL);
		V_2 = L_8;
		Material_t1886596500 * L_9 = V_1;
		NullCheck(L_9);
		float L_10 = Material_GetFloat_m2541456626(L_9, _stringLiteral2047710258, /*hidden argument*/NULL);
		V_3 = L_10;
		float L_11 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = ((float)((float)L_11/(float)(20.0f)));
		Vector4_t3525329790  L_12 = V_2;
		float L_13 = V_4;
		float L_14 = V_3;
		Vector4_t3525329790  L_15 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_12, ((float)((float)L_13*(float)L_14)), /*hidden argument*/NULL);
		V_5 = L_15;
		float L_16 = (&V_5)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_17 = Mathf_Repeat_m3424250200(NULL /*static, unused*/, L_16, (1.0f), /*hidden argument*/NULL);
		float L_18 = (&V_5)->get_y_2();
		float L_19 = Mathf_Repeat_m3424250200(NULL /*static, unused*/, L_18, (1.0f), /*hidden argument*/NULL);
		float L_20 = (&V_5)->get_z_3();
		float L_21 = Mathf_Repeat_m3424250200(NULL /*static, unused*/, L_20, (1.0f), /*hidden argument*/NULL);
		float L_22 = (&V_5)->get_w_4();
		float L_23 = Mathf_Repeat_m3424250200(NULL /*static, unused*/, L_22, (1.0f), /*hidden argument*/NULL);
		Vector4__ctor_m2441427762((&V_6), L_17, L_19, L_21, L_23, /*hidden argument*/NULL);
		Material_t1886596500 * L_24 = V_1;
		Vector4_t3525329790  L_25 = V_6;
		NullCheck(L_24);
		Material_SetVector_m3505096203(L_24, _stringLiteral3237885611, L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterTile::.ctor()
extern "C"  void WaterTile__ctor_m3254941794 (WaterTile_t2511865071 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterTile::Start()
extern "C"  void WaterTile_Start_m2202079586 (WaterTile_t2511865071 * __this, const MethodInfo* method)
{
	{
		WaterTile_AcquireComponents_m393236620(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterTile::AcquireComponents()
extern const MethodInfo* Component_GetComponent_TisPlanarReflection_t991663_m4268723140_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisWaterBase_t2511321362_m2122905197_MethodInfo_var;
extern const uint32_t WaterTile_AcquireComponents_m393236620_MetadataUsageId;
extern "C"  void WaterTile_AcquireComponents_m393236620 (WaterTile_t2511865071 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WaterTile_AcquireComponents_m393236620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlanarReflection_t991663 * L_0 = __this->get_reflection_2();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0051;
		}
	}
	{
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t284553113 * L_3 = Transform_get_parent_m2236876972(L_2, /*hidden argument*/NULL);
		bool L_4 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		Transform_t284553113 * L_5 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t284553113 * L_6 = Transform_get_parent_m2236876972(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		PlanarReflection_t991663 * L_7 = Component_GetComponent_TisPlanarReflection_t991663_m4268723140(L_6, /*hidden argument*/Component_GetComponent_TisPlanarReflection_t991663_m4268723140_MethodInfo_var);
		__this->set_reflection_2(L_7);
		goto IL_0051;
	}

IL_0040:
	{
		Transform_t284553113 * L_8 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		PlanarReflection_t991663 * L_9 = Component_GetComponent_TisPlanarReflection_t991663_m4268723140(L_8, /*hidden argument*/Component_GetComponent_TisPlanarReflection_t991663_m4268723140_MethodInfo_var);
		__this->set_reflection_2(L_9);
	}

IL_0051:
	{
		WaterBase_t2511321362 * L_10 = __this->get_waterBase_3();
		bool L_11 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_00a2;
		}
	}
	{
		Transform_t284553113 * L_12 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t284553113 * L_13 = Transform_get_parent_m2236876972(L_12, /*hidden argument*/NULL);
		bool L_14 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0091;
		}
	}
	{
		Transform_t284553113 * L_15 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t284553113 * L_16 = Transform_get_parent_m2236876972(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		WaterBase_t2511321362 * L_17 = Component_GetComponent_TisWaterBase_t2511321362_m2122905197(L_16, /*hidden argument*/Component_GetComponent_TisWaterBase_t2511321362_m2122905197_MethodInfo_var);
		__this->set_waterBase_3(L_17);
		goto IL_00a2;
	}

IL_0091:
	{
		Transform_t284553113 * L_18 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		WaterBase_t2511321362 * L_19 = Component_GetComponent_TisWaterBase_t2511321362_m2122905197(L_18, /*hidden argument*/Component_GetComponent_TisWaterBase_t2511321362_m2122905197_MethodInfo_var);
		__this->set_waterBase_3(L_19);
	}

IL_00a2:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterTile::OnWillRenderObject()
extern "C"  void WaterTile_OnWillRenderObject_m3418736328 (WaterTile_t2511865071 * __this, const MethodInfo* method)
{
	{
		PlanarReflection_t991663 * L_0 = __this->get_reflection_2();
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		PlanarReflection_t991663 * L_2 = __this->get_reflection_2();
		Transform_t284553113 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Camera_t3533968274 * L_4 = Camera_get_current_m475592003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlanarReflection_WaterTileBeingRendered_m728320614(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		WaterBase_t2511321362 * L_5 = __this->get_waterBase_3();
		bool L_6 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004c;
		}
	}
	{
		WaterBase_t2511321362 * L_7 = __this->get_waterBase_3();
		Transform_t284553113 * L_8 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Camera_t3533968274 * L_9 = Camera_get_current_m475592003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		WaterBase_WaterTileBeingRendered_m4064990337(L_7, L_8, L_9, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
