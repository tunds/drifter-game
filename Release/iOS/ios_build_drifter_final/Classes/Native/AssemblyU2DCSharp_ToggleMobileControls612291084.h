﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.UI.Slider
struct Slider_t1468074762;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToggleMobileControls
struct  ToggleMobileControls_t612291084  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GameObject ToggleMobileControls::joystick
	GameObject_t4012695102 * ___joystick_2;
	// UnityEngine.GameObject ToggleMobileControls::attackButton
	GameObject_t4012695102 * ___attackButton_3;
	// UnityEngine.GameObject ToggleMobileControls::specialAttackButton
	GameObject_t4012695102 * ___specialAttackButton_4;
	// UnityEngine.UI.Slider ToggleMobileControls::specialSldr
	Slider_t1468074762 * ___specialSldr_5;
	// System.Boolean ToggleMobileControls::isDevice
	bool ___isDevice_6;
	// UnityEngine.Color ToggleMobileControls::disabledColor
	Color_t1588175760  ___disabledColor_7;
	// UnityEngine.Color ToggleMobileControls::enabledColor
	Color_t1588175760  ___enabledColor_8;

public:
	inline static int32_t get_offset_of_joystick_2() { return static_cast<int32_t>(offsetof(ToggleMobileControls_t612291084, ___joystick_2)); }
	inline GameObject_t4012695102 * get_joystick_2() const { return ___joystick_2; }
	inline GameObject_t4012695102 ** get_address_of_joystick_2() { return &___joystick_2; }
	inline void set_joystick_2(GameObject_t4012695102 * value)
	{
		___joystick_2 = value;
		Il2CppCodeGenWriteBarrier(&___joystick_2, value);
	}

	inline static int32_t get_offset_of_attackButton_3() { return static_cast<int32_t>(offsetof(ToggleMobileControls_t612291084, ___attackButton_3)); }
	inline GameObject_t4012695102 * get_attackButton_3() const { return ___attackButton_3; }
	inline GameObject_t4012695102 ** get_address_of_attackButton_3() { return &___attackButton_3; }
	inline void set_attackButton_3(GameObject_t4012695102 * value)
	{
		___attackButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___attackButton_3, value);
	}

	inline static int32_t get_offset_of_specialAttackButton_4() { return static_cast<int32_t>(offsetof(ToggleMobileControls_t612291084, ___specialAttackButton_4)); }
	inline GameObject_t4012695102 * get_specialAttackButton_4() const { return ___specialAttackButton_4; }
	inline GameObject_t4012695102 ** get_address_of_specialAttackButton_4() { return &___specialAttackButton_4; }
	inline void set_specialAttackButton_4(GameObject_t4012695102 * value)
	{
		___specialAttackButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___specialAttackButton_4, value);
	}

	inline static int32_t get_offset_of_specialSldr_5() { return static_cast<int32_t>(offsetof(ToggleMobileControls_t612291084, ___specialSldr_5)); }
	inline Slider_t1468074762 * get_specialSldr_5() const { return ___specialSldr_5; }
	inline Slider_t1468074762 ** get_address_of_specialSldr_5() { return &___specialSldr_5; }
	inline void set_specialSldr_5(Slider_t1468074762 * value)
	{
		___specialSldr_5 = value;
		Il2CppCodeGenWriteBarrier(&___specialSldr_5, value);
	}

	inline static int32_t get_offset_of_isDevice_6() { return static_cast<int32_t>(offsetof(ToggleMobileControls_t612291084, ___isDevice_6)); }
	inline bool get_isDevice_6() const { return ___isDevice_6; }
	inline bool* get_address_of_isDevice_6() { return &___isDevice_6; }
	inline void set_isDevice_6(bool value)
	{
		___isDevice_6 = value;
	}

	inline static int32_t get_offset_of_disabledColor_7() { return static_cast<int32_t>(offsetof(ToggleMobileControls_t612291084, ___disabledColor_7)); }
	inline Color_t1588175760  get_disabledColor_7() const { return ___disabledColor_7; }
	inline Color_t1588175760 * get_address_of_disabledColor_7() { return &___disabledColor_7; }
	inline void set_disabledColor_7(Color_t1588175760  value)
	{
		___disabledColor_7 = value;
	}

	inline static int32_t get_offset_of_enabledColor_8() { return static_cast<int32_t>(offsetof(ToggleMobileControls_t612291084, ___enabledColor_8)); }
	inline Color_t1588175760  get_enabledColor_8() const { return ___enabledColor_8; }
	inline Color_t1588175760 * get_address_of_enabledColor_8() { return &___enabledColor_8; }
	inline void set_enabledColor_8(Color_t1588175760  value)
	{
		___enabledColor_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
