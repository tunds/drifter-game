﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct List_1_t1645832326;
// System.Collections.Generic.IEnumerable`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct IEnumerable_1_t3721027713;
// System.Collections.Generic.IEnumerator`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct IEnumerator_1_t2331979805;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct ICollection_1_t1314704743;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct ReadOnlyCollection_1_t4012018705;
// Mono.Data.Sqlite.SqliteKeyReader/KeyInfo[]
struct KeyInfoU5BU5D_t1118564256;
// System.Predicate`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct Predicate_1_t1419837255;
// System.Collections.Generic.IComparer`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct IComparer_1_t3548580766;
// System.Comparison`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>
struct Comparison_1_t3552548233;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteKeyReader_K848873357.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4026582614.h"

// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor()
extern "C"  void List_1__ctor_m1544486943_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1__ctor_m1544486943(__this, method) ((  void (*) (List_1_t1645832326 *, const MethodInfo*))List_1__ctor_m1544486943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3854516992_gshared (List_1_t1645832326 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3854516992(__this, ___collection0, method) ((  void (*) (List_1_t1645832326 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3854516992_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m2505403120_gshared (List_1_t1645832326 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m2505403120(__this, ___capacity0, method) ((  void (*) (List_1_t1645832326 *, int32_t, const MethodInfo*))List_1__ctor_m2505403120_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::.cctor()
extern "C"  void List_1__cctor_m3042272110_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3042272110(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3042272110_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m863416937_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m863416937(__this, method) ((  Il2CppObject* (*) (List_1_t1645832326 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m863416937_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3695738373_gshared (List_1_t1645832326 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3695738373(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1645832326 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3695738373_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1553554260_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1553554260(__this, method) ((  Il2CppObject * (*) (List_1_t1645832326 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1553554260_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2159084905_gshared (List_1_t1645832326 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2159084905(__this, ___item0, method) ((  int32_t (*) (List_1_t1645832326 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2159084905_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m4111407479_gshared (List_1_t1645832326 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m4111407479(__this, ___item0, method) ((  bool (*) (List_1_t1645832326 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m4111407479_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3148767041_gshared (List_1_t1645832326 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3148767041(__this, ___item0, method) ((  int32_t (*) (List_1_t1645832326 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3148767041_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3953071924_gshared (List_1_t1645832326 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3953071924(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1645832326 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3953071924_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3925536116_gshared (List_1_t1645832326 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3925536116(__this, ___item0, method) ((  void (*) (List_1_t1645832326 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3925536116_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1505681592_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1505681592(__this, method) ((  bool (*) (List_1_t1645832326 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1505681592_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3352802949_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3352802949(__this, method) ((  bool (*) (List_1_t1645832326 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3352802949_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3383520183_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3383520183(__this, method) ((  Il2CppObject * (*) (List_1_t1645832326 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3383520183_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1290333734_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1290333734(__this, method) ((  bool (*) (List_1_t1645832326 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1290333734_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1465463827_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1465463827(__this, method) ((  bool (*) (List_1_t1645832326 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1465463827_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1338264446_gshared (List_1_t1645832326 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1338264446(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1645832326 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1338264446_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2764981771_gshared (List_1_t1645832326 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2764981771(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1645832326 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2764981771_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Add(T)
extern "C"  void List_1_Add_m1182635136_gshared (List_1_t1645832326 * __this, KeyInfo_t848873357  ___item0, const MethodInfo* method);
#define List_1_Add_m1182635136(__this, ___item0, method) ((  void (*) (List_1_t1645832326 *, KeyInfo_t848873357 , const MethodInfo*))List_1_Add_m1182635136_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2350482107_gshared (List_1_t1645832326 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2350482107(__this, ___newCount0, method) ((  void (*) (List_1_t1645832326 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2350482107_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2400726457_gshared (List_1_t1645832326 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2400726457(__this, ___collection0, method) ((  void (*) (List_1_t1645832326 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2400726457_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1661698681_gshared (List_1_t1645832326 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1661698681(__this, ___enumerable0, method) ((  void (*) (List_1_t1645832326 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1661698681_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3721004158_gshared (List_1_t1645832326 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3721004158(__this, ___collection0, method) ((  void (*) (List_1_t1645832326 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3721004158_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t4012018705 * List_1_AsReadOnly_m4241735879_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m4241735879(__this, method) ((  ReadOnlyCollection_1_t4012018705 * (*) (List_1_t1645832326 *, const MethodInfo*))List_1_AsReadOnly_m4241735879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Clear()
extern "C"  void List_1_Clear_m13674570_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_Clear_m13674570(__this, method) ((  void (*) (List_1_t1645832326 *, const MethodInfo*))List_1_Clear_m13674570_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Contains(T)
extern "C"  bool List_1_Contains_m4147853628_gshared (List_1_t1645832326 * __this, KeyInfo_t848873357  ___item0, const MethodInfo* method);
#define List_1_Contains_m4147853628(__this, ___item0, method) ((  bool (*) (List_1_t1645832326 *, KeyInfo_t848873357 , const MethodInfo*))List_1_Contains_m4147853628_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m531844452_gshared (List_1_t1645832326 * __this, KeyInfoU5BU5D_t1118564256* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m531844452(__this, ___array0, method) ((  void (*) (List_1_t1645832326 *, KeyInfoU5BU5D_t1118564256*, const MethodInfo*))List_1_CopyTo_m531844452_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2752994416_gshared (List_1_t1645832326 * __this, KeyInfoU5BU5D_t1118564256* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2752994416(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1645832326 *, KeyInfoU5BU5D_t1118564256*, int32_t, const MethodInfo*))List_1_CopyTo_m2752994416_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Find(System.Predicate`1<T>)
extern "C"  KeyInfo_t848873357  List_1_Find_m802513302_gshared (List_1_t1645832326 * __this, Predicate_1_t1419837255 * ___match0, const MethodInfo* method);
#define List_1_Find_m802513302(__this, ___match0, method) ((  KeyInfo_t848873357  (*) (List_1_t1645832326 *, Predicate_1_t1419837255 *, const MethodInfo*))List_1_Find_m802513302_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3100885107_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1419837255 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3100885107(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1419837255 *, const MethodInfo*))List_1_CheckMatch_m3100885107_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m82342736_gshared (List_1_t1645832326 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1419837255 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m82342736(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1645832326 *, int32_t, int32_t, Predicate_1_t1419837255 *, const MethodInfo*))List_1_GetIndex_m82342736_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::GetEnumerator()
extern "C"  Enumerator_t4026582614  List_1_GetEnumerator_m2600467577_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2600467577(__this, method) ((  Enumerator_t4026582614  (*) (List_1_t1645832326 *, const MethodInfo*))List_1_GetEnumerator_m2600467577_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m80914492_gshared (List_1_t1645832326 * __this, KeyInfo_t848873357  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m80914492(__this, ___item0, method) ((  int32_t (*) (List_1_t1645832326 *, KeyInfo_t848873357 , const MethodInfo*))List_1_IndexOf_m80914492_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2135147207_gshared (List_1_t1645832326 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2135147207(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1645832326 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2135147207_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2499361600_gshared (List_1_t1645832326 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2499361600(__this, ___index0, method) ((  void (*) (List_1_t1645832326 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2499361600_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1942081255_gshared (List_1_t1645832326 * __this, int32_t ___index0, KeyInfo_t848873357  ___item1, const MethodInfo* method);
#define List_1_Insert_m1942081255(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1645832326 *, int32_t, KeyInfo_t848873357 , const MethodInfo*))List_1_Insert_m1942081255_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1227355676_gshared (List_1_t1645832326 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1227355676(__this, ___collection0, method) ((  void (*) (List_1_t1645832326 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1227355676_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Remove(T)
extern "C"  bool List_1_Remove_m186980407_gshared (List_1_t1645832326 * __this, KeyInfo_t848873357  ___item0, const MethodInfo* method);
#define List_1_Remove_m186980407(__this, ___item0, method) ((  bool (*) (List_1_t1645832326 *, KeyInfo_t848873357 , const MethodInfo*))List_1_Remove_m186980407_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3902757631_gshared (List_1_t1645832326 * __this, Predicate_1_t1419837255 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3902757631(__this, ___match0, method) ((  int32_t (*) (List_1_t1645832326 *, Predicate_1_t1419837255 *, const MethodInfo*))List_1_RemoveAll_m3902757631_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m4110901421_gshared (List_1_t1645832326 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m4110901421(__this, ___index0, method) ((  void (*) (List_1_t1645832326 *, int32_t, const MethodInfo*))List_1_RemoveAt_m4110901421_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Reverse()
extern "C"  void List_1_Reverse_m1919046815_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_Reverse_m1919046815(__this, method) ((  void (*) (List_1_t1645832326 *, const MethodInfo*))List_1_Reverse_m1919046815_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Sort()
extern "C"  void List_1_Sort_m1708607331_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_Sort_m1708607331(__this, method) ((  void (*) (List_1_t1645832326 *, const MethodInfo*))List_1_Sort_m1708607331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1435331041_gshared (List_1_t1645832326 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1435331041(__this, ___comparer0, method) ((  void (*) (List_1_t1645832326 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1435331041_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1993103222_gshared (List_1_t1645832326 * __this, Comparison_1_t3552548233 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1993103222(__this, ___comparison0, method) ((  void (*) (List_1_t1645832326 *, Comparison_1_t3552548233 *, const MethodInfo*))List_1_Sort_m1993103222_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::ToArray()
extern "C"  KeyInfoU5BU5D_t1118564256* List_1_ToArray_m317443896_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_ToArray_m317443896(__this, method) ((  KeyInfoU5BU5D_t1118564256* (*) (List_1_t1645832326 *, const MethodInfo*))List_1_ToArray_m317443896_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2688422076_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2688422076(__this, method) ((  void (*) (List_1_t1645832326 *, const MethodInfo*))List_1_TrimExcess_m2688422076_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1856774764_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1856774764(__this, method) ((  int32_t (*) (List_1_t1645832326 *, const MethodInfo*))List_1_get_Capacity_m1856774764_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3855638221_gshared (List_1_t1645832326 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3855638221(__this, ___value0, method) ((  void (*) (List_1_t1645832326 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3855638221_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Count()
extern "C"  int32_t List_1_get_Count_m2325656767_gshared (List_1_t1645832326 * __this, const MethodInfo* method);
#define List_1_get_Count_m2325656767(__this, method) ((  int32_t (*) (List_1_t1645832326 *, const MethodInfo*))List_1_get_Count_m2325656767_gshared)(__this, method)
// T System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::get_Item(System.Int32)
extern "C"  KeyInfo_t848873357  List_1_get_Item_m3576165203_gshared (List_1_t1645832326 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3576165203(__this, ___index0, method) ((  KeyInfo_t848873357  (*) (List_1_t1645832326 *, int32_t, const MethodInfo*))List_1_get_Item_m3576165203_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mono.Data.Sqlite.SqliteKeyReader/KeyInfo>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m4133876222_gshared (List_1_t1645832326 * __this, int32_t ___index0, KeyInfo_t848873357  ___value1, const MethodInfo* method);
#define List_1_set_Item_m4133876222(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1645832326 *, int32_t, KeyInfo_t848873357 , const MethodInfo*))List_1_set_Item_m4133876222_gshared)(__this, ___index0, ___value1, method)
