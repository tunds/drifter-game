﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_Field>
struct DefaultComparer_t3118284700;

#include "codegen/il2cpp-codegen.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Field3780330969.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_Field>::.ctor()
extern "C"  void DefaultComparer__ctor_m523764067_gshared (DefaultComparer_t3118284700 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m523764067(__this, method) ((  void (*) (DefaultComparer_t3118284700 *, const MethodInfo*))DefaultComparer__ctor_m523764067_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_Field>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4067321904_gshared (DefaultComparer_t3118284700 * __this, DB_Field_t3780330969  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m4067321904(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3118284700 *, DB_Field_t3780330969 , const MethodInfo*))DefaultComparer_GetHashCode_m4067321904_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_Field>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3152543736_gshared (DefaultComparer_t3118284700 * __this, DB_Field_t3780330969  ___x0, DB_Field_t3780330969  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3152543736(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3118284700 *, DB_Field_t3780330969 , DB_Field_t3780330969 , const MethodInfo*))DefaultComparer_Equals_m3152543736_gshared)(__this, ___x0, ___y1, method)
