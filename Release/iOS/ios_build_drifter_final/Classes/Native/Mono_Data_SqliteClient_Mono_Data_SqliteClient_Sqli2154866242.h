﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mono.Data.SqliteClient.SqliteConnection
struct SqliteConnection_t2830494554;

#include "System_Data_System_Data_Common_DbTransaction4162579344.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqliteClient.SqliteTransaction
struct  SqliteTransaction_t2154866242  : public DbTransaction_t4162579344
{
public:
	// Mono.Data.SqliteClient.SqliteConnection Mono.Data.SqliteClient.SqliteTransaction::_connection
	SqliteConnection_t2830494554 * ____connection_1;
	// System.Boolean Mono.Data.SqliteClient.SqliteTransaction::_open
	bool ____open_2;

public:
	inline static int32_t get_offset_of__connection_1() { return static_cast<int32_t>(offsetof(SqliteTransaction_t2154866242, ____connection_1)); }
	inline SqliteConnection_t2830494554 * get__connection_1() const { return ____connection_1; }
	inline SqliteConnection_t2830494554 ** get_address_of__connection_1() { return &____connection_1; }
	inline void set__connection_1(SqliteConnection_t2830494554 * value)
	{
		____connection_1 = value;
		Il2CppCodeGenWriteBarrier(&____connection_1, value);
	}

	inline static int32_t get_offset_of__open_2() { return static_cast<int32_t>(offsetof(SqliteTransaction_t2154866242, ____open_2)); }
	inline bool get__open_2() const { return ____open_2; }
	inline bool* get_address_of__open_2() { return &____open_2; }
	inline void set__open_2(bool value)
	{
		____open_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
