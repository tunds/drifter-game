﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.VersionNotFoundException
struct VersionNotFoundException_t1168984187;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void System.Data.VersionNotFoundException::.ctor()
extern "C"  void VersionNotFoundException__ctor_m2616865769 (VersionNotFoundException_t1168984187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.VersionNotFoundException::.ctor(System.String)
extern "C"  void VersionNotFoundException__ctor_m3266235833 (VersionNotFoundException_t1168984187 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.VersionNotFoundException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void VersionNotFoundException__ctor_m2204542378 (VersionNotFoundException_t1168984187 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
