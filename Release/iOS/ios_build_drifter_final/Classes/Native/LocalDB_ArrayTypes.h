﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array2840145358.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Field3780330969.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataPair4220460133.h"

#pragma once
// SQLiteDatabase.SQLiteDB/DB_Field[]
struct DB_FieldU5BU5D_t1717179044  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DB_Field_t3780330969  m_Items[1];

public:
	inline DB_Field_t3780330969  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DB_Field_t3780330969 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DB_Field_t3780330969  value)
	{
		m_Items[index] = value;
	}
};
// SQLiteDatabase.SQLiteDB/DB_DataPair[]
struct DB_DataPairU5BU5D_t2315727976  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DB_DataPair_t4220460133  m_Items[1];

public:
	inline DB_DataPair_t4220460133  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DB_DataPair_t4220460133 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DB_DataPair_t4220460133  value)
	{
		m_Items[index] = value;
	}
};
