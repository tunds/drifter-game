﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteUpdateEventHandler
struct SQLiteUpdateEventHandler_t1851894892;
// System.Object
struct Il2CppObject;
// Mono.Data.Sqlite.UpdateEventArgs
struct UpdateEventArgs_t2267790357;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_UpdateEventArgs2267790357.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void Mono.Data.Sqlite.SQLiteUpdateEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SQLiteUpdateEventHandler__ctor_m3530115420 (SQLiteUpdateEventHandler_t1851894892 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteUpdateEventHandler::Invoke(System.Object,Mono.Data.Sqlite.UpdateEventArgs)
extern "C"  void SQLiteUpdateEventHandler_Invoke_m2122769443 (SQLiteUpdateEventHandler_t1851894892 * __this, Il2CppObject * ___sender0, UpdateEventArgs_t2267790357 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_SQLiteUpdateEventHandler_t1851894892(Il2CppObject* delegate, Il2CppObject * ___sender0, UpdateEventArgs_t2267790357 * ___e1);
// System.IAsyncResult Mono.Data.Sqlite.SQLiteUpdateEventHandler::BeginInvoke(System.Object,Mono.Data.Sqlite.UpdateEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SQLiteUpdateEventHandler_BeginInvoke_m532948508 (SQLiteUpdateEventHandler_t1851894892 * __this, Il2CppObject * ___sender0, UpdateEventArgs_t2267790357 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteUpdateEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void SQLiteUpdateEventHandler_EndInvoke_m4189047148 (SQLiteUpdateEventHandler_t1851894892 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
