﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int64,System.Object>
struct Dictionary_2_t271112152;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key38140093.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1820995741_gshared (Enumerator_t38140093 * __this, Dictionary_2_t271112152 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1820995741(__this, ___host0, method) ((  void (*) (Enumerator_t38140093 *, Dictionary_2_t271112152 *, const MethodInfo*))Enumerator__ctor_m1820995741_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1902046766_gshared (Enumerator_t38140093 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1902046766(__this, method) ((  Il2CppObject * (*) (Enumerator_t38140093 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1902046766_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2481035256_gshared (Enumerator_t38140093 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2481035256(__this, method) ((  void (*) (Enumerator_t38140093 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2481035256_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3422783615_gshared (Enumerator_t38140093 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3422783615(__this, method) ((  void (*) (Enumerator_t38140093 *, const MethodInfo*))Enumerator_Dispose_m3422783615_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1073825320_gshared (Enumerator_t38140093 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1073825320(__this, method) ((  bool (*) (Enumerator_t38140093 *, const MethodInfo*))Enumerator_MoveNext_m1073825320_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::get_Current()
extern "C"  int64_t Enumerator_get_Current_m617781232_gshared (Enumerator_t38140093 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m617781232(__this, method) ((  int64_t (*) (Enumerator_t38140093 *, const MethodInfo*))Enumerator_get_Current_m617781232_gshared)(__this, method)
