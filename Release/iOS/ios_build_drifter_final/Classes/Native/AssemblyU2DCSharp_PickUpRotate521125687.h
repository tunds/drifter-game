﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickUpRotate
struct  PickUpRotate_t521125687  : public MonoBehaviour_t3012272455
{
public:
	// System.Single PickUpRotate::xRotation
	float ___xRotation_2;
	// System.Single PickUpRotate::yRotation
	float ___yRotation_3;
	// System.Single PickUpRotate::zRotation
	float ___zRotation_4;

public:
	inline static int32_t get_offset_of_xRotation_2() { return static_cast<int32_t>(offsetof(PickUpRotate_t521125687, ___xRotation_2)); }
	inline float get_xRotation_2() const { return ___xRotation_2; }
	inline float* get_address_of_xRotation_2() { return &___xRotation_2; }
	inline void set_xRotation_2(float value)
	{
		___xRotation_2 = value;
	}

	inline static int32_t get_offset_of_yRotation_3() { return static_cast<int32_t>(offsetof(PickUpRotate_t521125687, ___yRotation_3)); }
	inline float get_yRotation_3() const { return ___yRotation_3; }
	inline float* get_address_of_yRotation_3() { return &___yRotation_3; }
	inline void set_yRotation_3(float value)
	{
		___yRotation_3 = value;
	}

	inline static int32_t get_offset_of_zRotation_4() { return static_cast<int32_t>(offsetof(PickUpRotate_t521125687, ___zRotation_4)); }
	inline float get_zRotation_4() const { return ___zRotation_4; }
	inline float* get_address_of_zRotation_4() { return &___zRotation_4; }
	inline void set_zRotation_4(float value)
	{
		___zRotation_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
