﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mono.Data.SqliteClient.SqliteConnection
struct SqliteConnection_t2830494554;
// System.Data.IDbTransaction
struct IDbTransaction_t3098575722;
// System.String
struct String_t;
// Mono.Data.SqliteClient.SqliteParameterCollection
struct SqliteParameterCollection_t3807043211;

#include "System_Data_System_Data_Common_DbCommand2323745021.h"
#include "System_Data_System_Data_CommandType753495736.h"
#include "System_Data_System_Data_UpdateRowSource1763489119.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqliteClient.SqliteCommand
struct  SqliteCommand_t3207195823  : public DbCommand_t2323745021
{
public:
	// Mono.Data.SqliteClient.SqliteConnection Mono.Data.SqliteClient.SqliteCommand::parent_conn
	SqliteConnection_t2830494554 * ___parent_conn_4;
	// System.Data.IDbTransaction Mono.Data.SqliteClient.SqliteCommand::transaction
	Il2CppObject * ___transaction_5;
	// System.String Mono.Data.SqliteClient.SqliteCommand::sql
	String_t* ___sql_6;
	// System.Int32 Mono.Data.SqliteClient.SqliteCommand::timeout
	int32_t ___timeout_7;
	// System.Data.CommandType Mono.Data.SqliteClient.SqliteCommand::type
	int32_t ___type_8;
	// System.Data.UpdateRowSource Mono.Data.SqliteClient.SqliteCommand::upd_row_source
	int32_t ___upd_row_source_9;
	// Mono.Data.SqliteClient.SqliteParameterCollection Mono.Data.SqliteClient.SqliteCommand::sql_params
	SqliteParameterCollection_t3807043211 * ___sql_params_10;
	// System.Boolean Mono.Data.SqliteClient.SqliteCommand::prepared
	bool ___prepared_11;
	// System.Boolean Mono.Data.SqliteClient.SqliteCommand::_designTimeVisible
	bool ____designTimeVisible_12;

public:
	inline static int32_t get_offset_of_parent_conn_4() { return static_cast<int32_t>(offsetof(SqliteCommand_t3207195823, ___parent_conn_4)); }
	inline SqliteConnection_t2830494554 * get_parent_conn_4() const { return ___parent_conn_4; }
	inline SqliteConnection_t2830494554 ** get_address_of_parent_conn_4() { return &___parent_conn_4; }
	inline void set_parent_conn_4(SqliteConnection_t2830494554 * value)
	{
		___parent_conn_4 = value;
		Il2CppCodeGenWriteBarrier(&___parent_conn_4, value);
	}

	inline static int32_t get_offset_of_transaction_5() { return static_cast<int32_t>(offsetof(SqliteCommand_t3207195823, ___transaction_5)); }
	inline Il2CppObject * get_transaction_5() const { return ___transaction_5; }
	inline Il2CppObject ** get_address_of_transaction_5() { return &___transaction_5; }
	inline void set_transaction_5(Il2CppObject * value)
	{
		___transaction_5 = value;
		Il2CppCodeGenWriteBarrier(&___transaction_5, value);
	}

	inline static int32_t get_offset_of_sql_6() { return static_cast<int32_t>(offsetof(SqliteCommand_t3207195823, ___sql_6)); }
	inline String_t* get_sql_6() const { return ___sql_6; }
	inline String_t** get_address_of_sql_6() { return &___sql_6; }
	inline void set_sql_6(String_t* value)
	{
		___sql_6 = value;
		Il2CppCodeGenWriteBarrier(&___sql_6, value);
	}

	inline static int32_t get_offset_of_timeout_7() { return static_cast<int32_t>(offsetof(SqliteCommand_t3207195823, ___timeout_7)); }
	inline int32_t get_timeout_7() const { return ___timeout_7; }
	inline int32_t* get_address_of_timeout_7() { return &___timeout_7; }
	inline void set_timeout_7(int32_t value)
	{
		___timeout_7 = value;
	}

	inline static int32_t get_offset_of_type_8() { return static_cast<int32_t>(offsetof(SqliteCommand_t3207195823, ___type_8)); }
	inline int32_t get_type_8() const { return ___type_8; }
	inline int32_t* get_address_of_type_8() { return &___type_8; }
	inline void set_type_8(int32_t value)
	{
		___type_8 = value;
	}

	inline static int32_t get_offset_of_upd_row_source_9() { return static_cast<int32_t>(offsetof(SqliteCommand_t3207195823, ___upd_row_source_9)); }
	inline int32_t get_upd_row_source_9() const { return ___upd_row_source_9; }
	inline int32_t* get_address_of_upd_row_source_9() { return &___upd_row_source_9; }
	inline void set_upd_row_source_9(int32_t value)
	{
		___upd_row_source_9 = value;
	}

	inline static int32_t get_offset_of_sql_params_10() { return static_cast<int32_t>(offsetof(SqliteCommand_t3207195823, ___sql_params_10)); }
	inline SqliteParameterCollection_t3807043211 * get_sql_params_10() const { return ___sql_params_10; }
	inline SqliteParameterCollection_t3807043211 ** get_address_of_sql_params_10() { return &___sql_params_10; }
	inline void set_sql_params_10(SqliteParameterCollection_t3807043211 * value)
	{
		___sql_params_10 = value;
		Il2CppCodeGenWriteBarrier(&___sql_params_10, value);
	}

	inline static int32_t get_offset_of_prepared_11() { return static_cast<int32_t>(offsetof(SqliteCommand_t3207195823, ___prepared_11)); }
	inline bool get_prepared_11() const { return ___prepared_11; }
	inline bool* get_address_of_prepared_11() { return &___prepared_11; }
	inline void set_prepared_11(bool value)
	{
		___prepared_11 = value;
	}

	inline static int32_t get_offset_of__designTimeVisible_12() { return static_cast<int32_t>(offsetof(SqliteCommand_t3207195823, ____designTimeVisible_12)); }
	inline bool get__designTimeVisible_12() const { return ____designTimeVisible_12; }
	inline bool* get_address_of__designTimeVisible_12() { return &____designTimeVisible_12; }
	inline void set__designTimeVisible_12(bool value)
	{
		____designTimeVisible_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
