﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.IConvertible
struct IConvertible_t4194222097;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_TypeCode2164429820.h"

// System.Boolean Mono.Data.SqlExpressions.Numeric::IsNumeric(System.Object)
extern "C"  bool Numeric_IsNumeric_m654073676 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IConvertible Mono.Data.SqlExpressions.Numeric::Unify(System.IConvertible)
extern "C"  Il2CppObject * Numeric_Unify_m1453043581 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TypeCode Mono.Data.SqlExpressions.Numeric::ToSameType(System.IConvertible&,System.IConvertible&)
extern "C"  int32_t Numeric_ToSameType_m5691979 (Il2CppObject * __this /* static, unused */, Il2CppObject ** ___o10, Il2CppObject ** ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IConvertible Mono.Data.SqlExpressions.Numeric::Add(System.IConvertible,System.IConvertible)
extern "C"  Il2CppObject * Numeric_Add_m368185008 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IConvertible Mono.Data.SqlExpressions.Numeric::Subtract(System.IConvertible,System.IConvertible)
extern "C"  Il2CppObject * Numeric_Subtract_m161190409 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IConvertible Mono.Data.SqlExpressions.Numeric::Multiply(System.IConvertible,System.IConvertible)
extern "C"  Il2CppObject * Numeric_Multiply_m3550591225 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IConvertible Mono.Data.SqlExpressions.Numeric::Divide(System.IConvertible,System.IConvertible)
extern "C"  Il2CppObject * Numeric_Divide_m3493307332 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IConvertible Mono.Data.SqlExpressions.Numeric::Modulo(System.IConvertible,System.IConvertible)
extern "C"  Il2CppObject * Numeric_Modulo_m35068519 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IConvertible Mono.Data.SqlExpressions.Numeric::Negative(System.IConvertible)
extern "C"  Il2CppObject * Numeric_Negative_m3349617975 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IConvertible Mono.Data.SqlExpressions.Numeric::Min(System.IConvertible,System.IConvertible)
extern "C"  Il2CppObject * Numeric_Min_m4248976415 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IConvertible Mono.Data.SqlExpressions.Numeric::Max(System.IConvertible,System.IConvertible)
extern "C"  Il2CppObject * Numeric_Max_m489347405 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
