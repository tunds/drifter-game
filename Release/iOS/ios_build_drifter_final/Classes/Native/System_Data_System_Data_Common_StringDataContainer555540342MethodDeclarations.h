﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.StringDataContainer
struct StringDataContainer_t555540342;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Data.ISafeDataRecord
struct ISafeDataRecord_t3927591524;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Data.Common.StringDataContainer::.ctor()
extern "C"  void StringDataContainer__ctor_m3625105070 (StringDataContainer_t555540342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.StringDataContainer::SetValue(System.Int32,System.String)
extern "C"  void StringDataContainer_SetValue_m1328650322 (StringDataContainer_t555540342 * __this, int32_t ___index0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.StringDataContainer::SetValue(System.Int32,System.Object)
extern "C"  void StringDataContainer_SetValue_m1550760676 (StringDataContainer_t555540342 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.StringDataContainer::SetValueFromSafeDataRecord(System.Int32,System.Data.ISafeDataRecord,System.Int32)
extern "C"  void StringDataContainer_SetValueFromSafeDataRecord_m971768559 (StringDataContainer_t555540342 * __this, int32_t ___index0, Il2CppObject * ___record1, int32_t ___field2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.StringDataContainer::DoCompareValues(System.Int32,System.Int32)
extern "C"  int32_t StringDataContainer_DoCompareValues_m3382193762 (StringDataContainer_t555540342 * __this, int32_t ___index10, int32_t ___index21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
