﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.SqliteClient.SqliteParameter
struct SqliteParameter_t2628453389;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Data_System_Data_DbType2586775211.h"
#include "System_Data_System_Data_ParameterDirection608485289.h"
#include "mscorlib_System_String968488902.h"
#include "System_Data_System_Data_DataRowVersion2975473339.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Mono.Data.SqliteClient.SqliteParameter::.ctor()
extern "C"  void SqliteParameter__ctor_m1693193598 (SqliteParameter_t2628453389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DbType Mono.Data.SqliteClient.SqliteParameter::get_DbType()
extern "C"  int32_t SqliteParameter_get_DbType_m194818123 (SqliteParameter_t2628453389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_DbType(System.Data.DbType)
extern "C"  void SqliteParameter_set_DbType_m734555902 (SqliteParameter_t2628453389 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.ParameterDirection Mono.Data.SqliteClient.SqliteParameter::get_Direction()
extern "C"  int32_t SqliteParameter_get_Direction_m1968364812 (SqliteParameter_t2628453389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_Direction(System.Data.ParameterDirection)
extern "C"  void SqliteParameter_set_Direction_m3141793699 (SqliteParameter_t2628453389 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.SqliteClient.SqliteParameter::get_IsNullable()
extern "C"  bool SqliteParameter_get_IsNullable_m1709788274 (SqliteParameter_t2628453389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_IsNullable(System.Boolean)
extern "C"  void SqliteParameter_set_IsNullable_m2697584485 (SqliteParameter_t2628453389 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.SqliteClient.SqliteParameter::get_ParameterName()
extern "C"  String_t* SqliteParameter_get_ParameterName_m3143765892 (SqliteParameter_t2628453389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_ParameterName(System.String)
extern "C"  void SqliteParameter_set_ParameterName_m1446956239 (SqliteParameter_t2628453389 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_Size(System.Int32)
extern "C"  void SqliteParameter_set_Size_m931788565 (SqliteParameter_t2628453389 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.SqliteClient.SqliteParameter::get_SourceColumn()
extern "C"  String_t* SqliteParameter_get_SourceColumn_m599646339 (SqliteParameter_t2628453389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_SourceColumn(System.String)
extern "C"  void SqliteParameter_set_SourceColumn_m1936352878 (SqliteParameter_t2628453389 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_SourceColumnNullMapping(System.Boolean)
extern "C"  void SqliteParameter_set_SourceColumnNullMapping_m4236372780 (SqliteParameter_t2628453389 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataRowVersion Mono.Data.SqliteClient.SqliteParameter::get_SourceVersion()
extern "C"  int32_t SqliteParameter_get_SourceVersion_m1707108188 (SqliteParameter_t2628453389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_SourceVersion(System.Data.DataRowVersion)
extern "C"  void SqliteParameter_set_SourceVersion_m3470416783 (SqliteParameter_t2628453389 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value()
extern "C"  Il2CppObject * SqliteParameter_get_Value_m4012789519 (SqliteParameter_t2628453389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_Value(System.Object)
extern "C"  void SqliteParameter_set_Value_m3711129636 (SqliteParameter_t2628453389 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
