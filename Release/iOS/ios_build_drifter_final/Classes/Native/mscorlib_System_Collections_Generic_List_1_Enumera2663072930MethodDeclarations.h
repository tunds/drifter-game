﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<SQLiteDatabase.SQLiteDB/DB_Field>
struct List_1_t282322642;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2663072930.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_Field3780330969.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SQLiteDatabase.SQLiteDB/DB_Field>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m987373706_gshared (Enumerator_t2663072930 * __this, List_1_t282322642 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m987373706(__this, ___l0, method) ((  void (*) (Enumerator_t2663072930 *, List_1_t282322642 *, const MethodInfo*))Enumerator__ctor_m987373706_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m569460104_gshared (Enumerator_t2663072930 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m569460104(__this, method) ((  void (*) (Enumerator_t2663072930 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m569460104_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SQLiteDatabase.SQLiteDB/DB_Field>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m558465150_gshared (Enumerator_t2663072930 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m558465150(__this, method) ((  Il2CppObject * (*) (Enumerator_t2663072930 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m558465150_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLiteDatabase.SQLiteDB/DB_Field>::Dispose()
extern "C"  void Enumerator_Dispose_m3149394159_gshared (Enumerator_t2663072930 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3149394159(__this, method) ((  void (*) (Enumerator_t2663072930 *, const MethodInfo*))Enumerator_Dispose_m3149394159_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLiteDatabase.SQLiteDB/DB_Field>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3030098088_gshared (Enumerator_t2663072930 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3030098088(__this, method) ((  void (*) (Enumerator_t2663072930 *, const MethodInfo*))Enumerator_VerifyState_m3030098088_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SQLiteDatabase.SQLiteDB/DB_Field>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m241301304_gshared (Enumerator_t2663072930 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m241301304(__this, method) ((  bool (*) (Enumerator_t2663072930 *, const MethodInfo*))Enumerator_MoveNext_m241301304_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SQLiteDatabase.SQLiteDB/DB_Field>::get_Current()
extern "C"  DB_Field_t3780330969  Enumerator_get_Current_m2527326785_gshared (Enumerator_t2663072930 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2527326785(__this, method) ((  DB_Field_t3780330969  (*) (Enumerator_t2663072930 *, const MethodInfo*))Enumerator_get_Current_m2527326785_gshared)(__this, method)
