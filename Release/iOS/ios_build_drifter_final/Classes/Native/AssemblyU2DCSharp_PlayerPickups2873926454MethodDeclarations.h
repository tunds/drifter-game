﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerPickups
struct PlayerPickups_t2873926454;
// UnityEngine.Collider
struct Collider_t955670625;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayerPickups::.ctor()
extern "C"  void PlayerPickups__ctor_m2071132917 (PlayerPickups_t2873926454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPickups::Start()
extern "C"  void PlayerPickups_Start_m1018270709 (PlayerPickups_t2873926454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPickups::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void PlayerPickups_OnTriggerEnter_m1011052707 (PlayerPickups_t2873926454 * __this, Collider_t955670625 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPickups::RemoveObject(UnityEngine.Collider)
extern "C"  void PlayerPickups_RemoveObject_m688797319 (PlayerPickups_t2873926454 * __this, Collider_t955670625 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPickups::AssignWeapon(System.String,UnityEngine.Collider)
extern "C"  void PlayerPickups_AssignWeapon_m2718080467 (PlayerPickups_t2873926454 * __this, String_t* ___tag0, Collider_t955670625 * ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPickups::DestroyWeapon()
extern "C"  void PlayerPickups_DestroyWeapon_m1172587337 (PlayerPickups_t2873926454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPickups::ToggleWeapon(System.String)
extern "C"  void PlayerPickups_ToggleWeapon_m2520552739 (PlayerPickups_t2873926454 * __this, String_t* ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
