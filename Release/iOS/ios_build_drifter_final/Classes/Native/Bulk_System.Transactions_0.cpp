﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.MonoTODOAttribute
struct MonoTODOAttribute_t1287393900;
// System.String
struct String_t;
// System.Transactions.Enlistment
struct Enlistment_t3082063553;
// System.Transactions.PreparingEnlistment
struct PreparingEnlistment_t3015517605;
// System.Transactions.Transaction
struct Transaction_t3175846586;
// System.Transactions.IEnlistmentNotification
struct IEnlistmentNotification_t1752309717;
// System.Exception
struct Exception_t1967233988;
// System.Transactions.SinglePhaseEnlistment
struct SinglePhaseEnlistment_t3022234868;
// System.Transactions.ISinglePhaseNotification
struct ISinglePhaseNotification_t2163935249;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Transactions.TransactionInformation
struct TransactionInformation_t3294574474;
// System.Object
struct Il2CppObject;
// System.Transactions.TransactionScope
struct TransactionScope_t3316380210;
// System.Transactions.Transaction/AsyncCommit
struct AsyncCommit_t802550451;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Transactions.TransactionAbortedException
struct TransactionAbortedException_t2212577306;
// System.Transactions.TransactionCompletedEventHandler
struct TransactionCompletedEventHandler_t1858411801;
// System.Transactions.TransactionEventArgs
struct TransactionEventArgs_t2318060917;
// System.Transactions.TransactionException
struct TransactionException_t3768828717;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Transactions_U3CModuleU3E86524790.h"
#include "System_Transactions_U3CModuleU3E86524790MethodDeclarations.h"
#include "System_Transactions_System_MonoTODOAttribute1287393896.h"
#include "System_Transactions_System_MonoTODOAttribute1287393896MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Attribute498693649MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "System_Transactions_System_Transactions_Enlistment3082063553.h"
#include "System_Transactions_System_Transactions_Enlistment3082063553MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "System_Transactions_System_Transactions_Enlistment1495500821.h"
#include "System_Transactions_System_Transactions_Enlistment1495500821MethodDeclarations.h"
#include "System_Transactions_System_Transactions_IsolationL2549522802.h"
#include "System_Transactions_System_Transactions_IsolationL2549522802MethodDeclarations.h"
#include "System_Transactions_System_Transactions_PreparingE3015517605.h"
#include "System_Transactions_System_Transactions_PreparingE3015517605MethodDeclarations.h"
#include "System_Transactions_System_Transactions_Transactio3175846586.h"
#include "mscorlib_System_Exception1967233988.h"
#include "System_Transactions_System_Transactions_Transactio3175846586MethodDeclarations.h"
#include "System_Transactions_System_Transactions_SinglePhas3022234868.h"
#include "System_Transactions_System_Transactions_SinglePhas3022234868MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList2121638921MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2549268686MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2960894218MethodDeclarations.h"
#include "System_Transactions_System_Transactions_Transactio3294574474MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2549268686.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2960894218.h"
#include "System_Transactions_System_Transactions_Transactio3294574474.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "mscorlib_System_NotImplementedException1091014741MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException1091014741.h"
#include "System_Transactions_System_Transactions_Transactio1562036300.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Int322847414787.h"
#include "System_Transactions_System_Transactions_Transactio3768828717MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat635051678MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat635051678.h"
#include "System_Transactions_System_Transactions_Transactio3768828717.h"
#include "System_Transactions_System_Transactions_Transactio3316380210.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "System_Transactions_System_Transactions_Transactio2212577306MethodDeclarations.h"
#include "System_Transactions_System_Transactions_Transactio2212577306.h"
#include "System_Transactions_System_Transactions_Transactio3316380210MethodDeclarations.h"
#include "System_Transactions_System_Transactions_Transaction802550451.h"
#include "System_Transactions_System_Transactions_Transaction802550451MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "System_Transactions_System_Transactions_Transactio1858411801.h"
#include "System_Transactions_System_Transactions_Transactio1858411801MethodDeclarations.h"
#include "System_Transactions_System_Transactions_Transactio2318060917.h"
#include "System_Transactions_System_Transactions_Transactio2318060917MethodDeclarations.h"
#include "mscorlib_System_SystemException3155420757MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936MethodDeclarations.h"
#include "mscorlib_System_Guid2778838590MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Guid2778838590.h"
#include "System_Transactions_System_Transactions_Transactio1434080011.h"
#include "System_Transactions_System_Transactions_Transactio1434080011MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "System_Transactions_System_Transactions_Transactio3644312060.h"
#include "System_Transactions_System_Transactions_Transactio3644312060MethodDeclarations.h"
#include "System_Transactions_System_Transactions_Transactio1562036300MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.MonoTODOAttribute::.ctor()
extern "C"  void MonoTODOAttribute__ctor_m3438007885 (MonoTODOAttribute_t1287393900 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.MonoTODOAttribute::.ctor(System.String)
extern "C"  void MonoTODOAttribute__ctor_m4120325589 (MonoTODOAttribute_t1287393900 * __this, String_t* ___comment0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___comment0;
		__this->set_comment_0(L_0);
		return;
	}
}
// System.Void System.Transactions.Enlistment::.ctor()
extern "C"  void Enlistment__ctor_m1854573282 (Enlistment_t3082063553 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_done_0((bool)0);
		return;
	}
}
// System.Void System.Transactions.Enlistment::Done()
extern "C"  void Enlistment_Done_m423463204 (Enlistment_t3082063553 * __this, const MethodInfo* method)
{
	{
		__this->set_done_0((bool)1);
		return;
	}
}
// System.Void System.Transactions.PreparingEnlistment::.ctor(System.Transactions.Transaction,System.Transactions.IEnlistmentNotification)
extern "C"  void PreparingEnlistment__ctor_m3145960357 (PreparingEnlistment_t3015517605 * __this, Transaction_t3175846586 * ___tx0, Il2CppObject * ___enlisted1, const MethodInfo* method)
{
	{
		Enlistment__ctor_m1854573282(__this, /*hidden argument*/NULL);
		Transaction_t3175846586 * L_0 = ___tx0;
		__this->set_tx_2(L_0);
		Il2CppObject * L_1 = ___enlisted1;
		__this->set_enlisted_3(L_1);
		return;
	}
}
// System.Void System.Transactions.PreparingEnlistment::ForceRollback()
extern "C"  void PreparingEnlistment_ForceRollback_m1708279619 (PreparingEnlistment_t3015517605 * __this, const MethodInfo* method)
{
	{
		PreparingEnlistment_ForceRollback_m2493706739(__this, (Exception_t1967233988 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.PreparingEnlistment::ForceRollback(System.Exception)
extern "C"  void PreparingEnlistment_ForceRollback_m2493706739 (PreparingEnlistment_t3015517605 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method)
{
	{
		Transaction_t3175846586 * L_0 = __this->get_tx_2();
		Exception_t1967233988 * L_1 = ___ex0;
		Il2CppObject * L_2 = __this->get_enlisted_3();
		NullCheck(L_0);
		Transaction_Rollback_m1929892516(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.PreparingEnlistment::Prepared()
extern "C"  void PreparingEnlistment_Prepared_m4090146571 (PreparingEnlistment_t3015517605 * __this, const MethodInfo* method)
{
	{
		__this->set_prepared_1((bool)1);
		return;
	}
}
// System.Boolean System.Transactions.PreparingEnlistment::get_IsPrepared()
extern "C"  bool PreparingEnlistment_get_IsPrepared_m4211467806 (PreparingEnlistment_t3015517605 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_prepared_1();
		return L_0;
	}
}
// System.Void System.Transactions.SinglePhaseEnlistment::.ctor(System.Transactions.Transaction,System.Transactions.ISinglePhaseNotification)
extern "C"  void SinglePhaseEnlistment__ctor_m44442160 (SinglePhaseEnlistment_t3022234868 * __this, Transaction_t3175846586 * ___tx0, Il2CppObject * ___enlisted1, const MethodInfo* method)
{
	{
		Enlistment__ctor_m1854573282(__this, /*hidden argument*/NULL);
		Transaction_t3175846586 * L_0 = ___tx0;
		__this->set_tx_1(L_0);
		Il2CppObject * L_1 = ___enlisted1;
		__this->set_enlisted_2(L_1);
		return;
	}
}
// System.Void System.Transactions.Transaction::.ctor()
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2549268686_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2960894218_il2cpp_TypeInfo_var;
extern Il2CppClass* TransactionInformation_t3294574474_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2946007076_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m183005972_MethodInfo_var;
extern const uint32_t Transaction__ctor_m651561089_MetadataUsageId;
extern "C"  void Transaction__ctor_m651561089 (Transaction_t3175846586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transaction__ctor_m651561089_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		__this->set_dependents_3(L_0);
		List_1_t2549268686 * L_1 = (List_1_t2549268686 *)il2cpp_codegen_object_new(List_1_t2549268686_il2cpp_TypeInfo_var);
		List_1__ctor_m2946007076(L_1, /*hidden argument*/List_1__ctor_m2946007076_MethodInfo_var);
		__this->set_volatiles_4(L_1);
		List_1_t2960894218 * L_2 = (List_1_t2960894218 *)il2cpp_codegen_object_new(List_1_t2960894218_il2cpp_TypeInfo_var);
		List_1__ctor_m183005972(L_2, /*hidden argument*/List_1__ctor_m183005972_MethodInfo_var);
		__this->set_durables_5(L_2);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		TransactionInformation_t3294574474 * L_3 = (TransactionInformation_t3294574474 *)il2cpp_codegen_object_new(TransactionInformation_t3294574474_il2cpp_TypeInfo_var);
		TransactionInformation__ctor_m49122873(L_3, /*hidden argument*/NULL);
		__this->set_info_2(L_3);
		__this->set_level_1(0);
		return;
	}
}
// System.Void System.Transactions.Transaction::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_System_Runtime_Serialization_ISerializable_GetObjectData_m16758250_MetadataUsageId;
extern "C"  void Transaction_System_Runtime_Serialization_ISerializable_GetObjectData_m16758250 (Transaction_t3175846586 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transaction_System_Runtime_Serialization_ISerializable_GetObjectData_m16758250_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Transactions.Transaction System.Transactions.Transaction::get_Current()
extern "C"  Transaction_t3175846586 * Transaction_get_Current_m308715390 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Transaction_EnsureIncompleteCurrentScope_m144438170(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transaction_t3175846586 * L_0 = Transaction_get_CurrentInternal_m4103191067(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Transactions.Transaction System.Transactions.Transaction::get_CurrentInternal()
extern Il2CppClass* Transaction_t3175846586_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_get_CurrentInternal_m4103191067_MetadataUsageId;
extern "C"  Transaction_t3175846586 * Transaction_get_CurrentInternal_m4103191067 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transaction_get_CurrentInternal_m4103191067_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transaction_t3175846586 * L_0 = ((Transaction_t3175846586_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Transaction_t3175846586_il2cpp_TypeInfo_var))->get_ambient_0();
		return L_0;
	}
}
// System.Void System.Transactions.Transaction::set_CurrentInternal(System.Transactions.Transaction)
extern Il2CppClass* Transaction_t3175846586_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_set_CurrentInternal_m529514518_MetadataUsageId;
extern "C"  void Transaction_set_CurrentInternal_m529514518 (Il2CppObject * __this /* static, unused */, Transaction_t3175846586 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transaction_set_CurrentInternal_m529514518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transaction_t3175846586 * L_0 = ___value0;
		((Transaction_t3175846586_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Transaction_t3175846586_il2cpp_TypeInfo_var))->set_ambient_0(L_0);
		return;
	}
}
// System.Transactions.TransactionInformation System.Transactions.Transaction::get_TransactionInformation()
extern "C"  TransactionInformation_t3294574474 * Transaction_get_TransactionInformation_m2830477433 (Transaction_t3175846586 * __this, const MethodInfo* method)
{
	{
		Transaction_EnsureIncompleteCurrentScope_m144438170(NULL /*static, unused*/, /*hidden argument*/NULL);
		TransactionInformation_t3294574474 * L_0 = __this->get_info_2();
		return L_0;
	}
}
// System.Void System.Transactions.Transaction::Dispose()
extern "C"  void Transaction_Dispose_m3278231678 (Transaction_t3175846586 * __this, const MethodInfo* method)
{
	{
		TransactionInformation_t3294574474 * L_0 = Transaction_get_TransactionInformation_m2830477433(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = TransactionInformation_get_Status_m4243117603(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		Transaction_Rollback_m3171304839(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Transactions.Enlistment System.Transactions.Transaction::EnlistVolatile(System.Transactions.IEnlistmentNotification,System.Transactions.EnlistmentOptions)
extern "C"  Enlistment_t3082063553 * Transaction_EnlistVolatile_m3347489508 (Transaction_t3175846586 * __this, Il2CppObject * ___notification0, int32_t ___options1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___notification0;
		int32_t L_1 = ___options1;
		Enlistment_t3082063553 * L_2 = Transaction_EnlistVolatileInternal_m2333899751(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Transactions.Enlistment System.Transactions.Transaction::EnlistVolatileInternal(System.Transactions.IEnlistmentNotification,System.Transactions.EnlistmentOptions)
extern Il2CppClass* Enlistment_t3082063553_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_EnlistVolatileInternal_m2333899751_MetadataUsageId;
extern "C"  Enlistment_t3082063553 * Transaction_EnlistVolatileInternal_m2333899751 (Transaction_t3175846586 * __this, Il2CppObject * ___notification0, int32_t ___options1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transaction_EnlistVolatileInternal_m2333899751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transaction_EnsureIncompleteCurrentScope_m144438170(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t2549268686 * L_0 = __this->get_volatiles_4();
		Il2CppObject * L_1 = ___notification0;
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Transactions.IEnlistmentNotification>::Add(!0) */, L_0, L_1);
		Enlistment_t3082063553 * L_2 = (Enlistment_t3082063553 *)il2cpp_codegen_object_new(Enlistment_t3082063553_il2cpp_TypeInfo_var);
		Enlistment__ctor_m1854573282(L_2, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Transactions.Transaction::Equals(System.Object)
extern Il2CppClass* Transaction_t3175846586_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_Equals_m790901490_MetadataUsageId;
extern "C"  bool Transaction_Equals_m790901490 (Transaction_t3175846586 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transaction_Equals_m790901490_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		bool L_1 = Transaction_Equals_m3895543116(__this, ((Transaction_t3175846586 *)IsInstClass(L_0, Transaction_t3175846586_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Transactions.Transaction::Equals(System.Transactions.Transaction)
extern "C"  bool Transaction_Equals_m3895543116 (Transaction_t3175846586 * __this, Transaction_t3175846586 * ___t0, const MethodInfo* method)
{
	int32_t G_B7_0 = 0;
	{
		Transaction_t3175846586 * L_0 = ___t0;
		bool L_1 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)1;
	}

IL_000e:
	{
		Transaction_t3175846586 * L_2 = ___t0;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_2, NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (bool)0;
	}

IL_001c:
	{
		int32_t L_4 = __this->get_level_1();
		Transaction_t3175846586 * L_5 = ___t0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_level_1();
		if ((!(((uint32_t)L_4) == ((uint32_t)L_6))))
		{
			goto IL_003d;
		}
	}
	{
		TransactionInformation_t3294574474 * L_7 = __this->get_info_2();
		Transaction_t3175846586 * L_8 = ___t0;
		NullCheck(L_8);
		TransactionInformation_t3294574474 * L_9 = L_8->get_info_2();
		G_B7_0 = ((((Il2CppObject*)(TransactionInformation_t3294574474 *)L_7) == ((Il2CppObject*)(TransactionInformation_t3294574474 *)L_9))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B7_0 = 0;
	}

IL_003e:
	{
		return (bool)G_B7_0;
	}
}
// System.Int32 System.Transactions.Transaction::GetHashCode()
extern "C"  int32_t Transaction_GetHashCode_m1183983114 (Transaction_t3175846586 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_level_1();
		TransactionInformation_t3294574474 * L_1 = __this->get_info_2();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_1);
		ArrayList_t2121638921 * L_3 = __this->get_dependents_3();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_3);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0^(int32_t)L_2))^(int32_t)L_4));
	}
}
// System.Void System.Transactions.Transaction::Rollback()
extern "C"  void Transaction_Rollback_m3171304839 (Transaction_t3175846586 * __this, const MethodInfo* method)
{
	{
		Transaction_Rollback_m490334263(__this, (Exception_t1967233988 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::Rollback(System.Exception)
extern "C"  void Transaction_Rollback_m490334263 (Transaction_t3175846586 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method)
{
	{
		Transaction_EnsureIncompleteCurrentScope_m144438170(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t1967233988 * L_0 = ___ex0;
		Transaction_Rollback_m1929892516(__this, L_0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::Rollback(System.Exception,System.Transactions.IEnlistmentNotification)
extern Il2CppClass* TransactionException_t3768828717_il2cpp_TypeInfo_var;
extern Il2CppClass* Enlistment_t3082063553_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnlistmentNotification_t1752309717_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t635051678_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2574341403_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m431675431_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1026347209_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2656236324;
extern const uint32_t Transaction_Rollback_m1929892516_MetadataUsageId;
extern "C"  void Transaction_Rollback_m1929892516 (Transaction_t3175846586 * __this, Exception_t1967233988 * ___ex0, Il2CppObject * ___enlisted1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transaction_Rollback_m1929892516_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Enlistment_t3082063553 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Enumerator_t635051678  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_aborted_9();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		TransactionInformation_t3294574474 * L_1 = __this->get_info_2();
		NullCheck(L_1);
		int32_t L_2 = TransactionInformation_get_Status_m4243117603(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0028;
		}
	}
	{
		TransactionException_t3768828717 * L_3 = (TransactionException_t3768828717 *)il2cpp_codegen_object_new(TransactionException_t3768828717_il2cpp_TypeInfo_var);
		TransactionException__ctor_m2617484236(L_3, _stringLiteral2656236324, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		Exception_t1967233988 * L_4 = ___ex0;
		__this->set_innerException_11(L_4);
		Enlistment_t3082063553 * L_5 = (Enlistment_t3082063553 *)il2cpp_codegen_object_new(Enlistment_t3082063553_il2cpp_TypeInfo_var);
		Enlistment__ctor_m1854573282(L_5, /*hidden argument*/NULL);
		V_0 = L_5;
		List_1_t2549268686 * L_6 = __this->get_volatiles_4();
		NullCheck(L_6);
		Enumerator_t635051678  L_7 = List_1_GetEnumerator_m2574341403(L_6, /*hidden argument*/List_1_GetEnumerator_m2574341403_MethodInfo_var);
		V_2 = L_7;
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005c;
		}

IL_0046:
		{
			Il2CppObject * L_8 = Enumerator_get_Current_m431675431((&V_2), /*hidden argument*/Enumerator_get_Current_m431675431_MethodInfo_var);
			V_1 = L_8;
			Il2CppObject * L_9 = V_1;
			Il2CppObject * L_10 = ___enlisted1;
			if ((((Il2CppObject*)(Il2CppObject *)L_9) == ((Il2CppObject*)(Il2CppObject *)L_10)))
			{
				goto IL_005c;
			}
		}

IL_0055:
		{
			Il2CppObject * L_11 = V_1;
			Enlistment_t3082063553 * L_12 = V_0;
			NullCheck(L_11);
			InterfaceActionInvoker1< Enlistment_t3082063553 * >::Invoke(2 /* System.Void System.Transactions.IEnlistmentNotification::Rollback(System.Transactions.Enlistment) */, IEnlistmentNotification_t1752309717_il2cpp_TypeInfo_var, L_11, L_12);
		}

IL_005c:
		{
			bool L_13 = Enumerator_MoveNext_m1026347209((&V_2), /*hidden argument*/Enumerator_MoveNext_m1026347209_MethodInfo_var);
			if (L_13)
			{
				goto IL_0046;
			}
		}

IL_0068:
		{
			IL2CPP_LEAVE(0x79, FINALLY_006d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006d;
	}

FINALLY_006d:
	{ // begin finally (depth: 1)
		Enumerator_t635051678  L_14 = V_2;
		Enumerator_t635051678  L_15 = L_14;
		Il2CppObject * L_16 = Box(Enumerator_t635051678_il2cpp_TypeInfo_var, &L_15);
		NullCheck((Il2CppObject *)L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_16);
		IL2CPP_END_FINALLY(109)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(109)
	{
		IL2CPP_JUMP_TBL(0x79, IL_0079)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0079:
	{
		List_1_t2960894218 * L_17 = __this->get_durables_5();
		NullCheck(L_17);
		int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Transactions.ISinglePhaseNotification>::get_Count() */, L_17);
		if ((((int32_t)L_18) <= ((int32_t)0)))
		{
			goto IL_00ae;
		}
	}
	{
		List_1_t2960894218 * L_19 = __this->get_durables_5();
		NullCheck(L_19);
		Il2CppObject * L_20 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Transactions.ISinglePhaseNotification>::get_Item(System.Int32) */, L_19, 0);
		Il2CppObject * L_21 = ___enlisted1;
		if ((((Il2CppObject*)(Il2CppObject *)L_20) == ((Il2CppObject*)(Il2CppObject *)L_21)))
		{
			goto IL_00ae;
		}
	}
	{
		List_1_t2960894218 * L_22 = __this->get_durables_5();
		NullCheck(L_22);
		Il2CppObject * L_23 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Transactions.ISinglePhaseNotification>::get_Item(System.Int32) */, L_22, 0);
		Enlistment_t3082063553 * L_24 = V_0;
		NullCheck(L_23);
		InterfaceActionInvoker1< Enlistment_t3082063553 * >::Invoke(2 /* System.Void System.Transactions.IEnlistmentNotification::Rollback(System.Transactions.Enlistment) */, IEnlistmentNotification_t1752309717_il2cpp_TypeInfo_var, L_23, L_24);
	}

IL_00ae:
	{
		Transaction_set_Aborted_m4230882120(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::set_Aborted(System.Boolean)
extern "C"  void Transaction_set_Aborted_m4230882120 (Transaction_t3175846586 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_aborted_9(L_0);
		bool L_1 = __this->get_aborted_9();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		TransactionInformation_t3294574474 * L_2 = __this->get_info_2();
		NullCheck(L_2);
		TransactionInformation_set_Status_m2073488130(L_2, 2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Transactions.TransactionScope System.Transactions.Transaction::get_Scope()
extern "C"  TransactionScope_t3316380210 * Transaction_get_Scope_m2741316771 (Transaction_t3175846586 * __this, const MethodInfo* method)
{
	{
		TransactionScope_t3316380210 * L_0 = __this->get_scope_10();
		return L_0;
	}
}
// System.Void System.Transactions.Transaction::set_Scope(System.Transactions.TransactionScope)
extern "C"  void Transaction_set_Scope_m1673949798 (Transaction_t3175846586 * __this, TransactionScope_t3316380210 * ___value0, const MethodInfo* method)
{
	{
		TransactionScope_t3316380210 * L_0 = ___value0;
		__this->set_scope_10(L_0);
		return;
	}
}
// System.Void System.Transactions.Transaction::CommitInternal()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1494946805;
extern const uint32_t Transaction_CommitInternal_m578594359_MetadataUsageId;
extern "C"  void Transaction_CommitInternal_m578594359 (Transaction_t3175846586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transaction_CommitInternal_m578594359_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_committed_8();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_committing_7();
		if (!L_1)
		{
			goto IL_0021;
		}
	}

IL_0016:
	{
		InvalidOperationException_t2420574324 * L_2 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_2, _stringLiteral1494946805, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0021:
	{
		__this->set_committing_7((bool)1);
		Transaction_DoCommit_m1880480965(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::DoCommit()
extern Il2CppClass* ISinglePhaseNotification_t2163935249_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_DoCommit_m1880480965_MetadataUsageId;
extern "C"  void Transaction_DoCommit_m1880480965 (Transaction_t3175846586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transaction_DoCommit_m1880480965_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		TransactionScope_t3316380210 * L_0 = Transaction_get_Scope_m2741316771(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Transaction_Rollback_m1929892516(__this, (Exception_t1967233988 *)NULL, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		Transaction_CheckAborted_m1722162826(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		List_1_t2549268686 * L_1 = __this->get_volatiles_4();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Transactions.IEnlistmentNotification>::get_Count() */, L_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0060;
		}
	}
	{
		List_1_t2960894218 * L_3 = __this->get_durables_5();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Transactions.ISinglePhaseNotification>::get_Count() */, L_3);
		if (L_4)
		{
			goto IL_0060;
		}
	}
	{
		List_1_t2549268686 * L_5 = __this->get_volatiles_4();
		NullCheck(L_5);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Transactions.IEnlistmentNotification>::get_Item(System.Int32) */, L_5, 0);
		V_0 = ((Il2CppObject *)IsInst(L_6, ISinglePhaseNotification_t2163935249_il2cpp_TypeInfo_var));
		Il2CppObject * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0060;
		}
	}
	{
		Il2CppObject * L_8 = V_0;
		Transaction_DoSingleCommit_m3243763006(__this, L_8, /*hidden argument*/NULL);
		Transaction_Complete_m3112384156(__this, /*hidden argument*/NULL);
		return;
	}

IL_0060:
	{
		List_1_t2549268686 * L_9 = __this->get_volatiles_4();
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Transactions.IEnlistmentNotification>::get_Count() */, L_9);
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0077;
		}
	}
	{
		Transaction_DoPreparePhase_m1564783426(__this, /*hidden argument*/NULL);
	}

IL_0077:
	{
		List_1_t2960894218 * L_11 = __this->get_durables_5();
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Transactions.ISinglePhaseNotification>::get_Count() */, L_11);
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_009a;
		}
	}
	{
		List_1_t2960894218 * L_13 = __this->get_durables_5();
		NullCheck(L_13);
		Il2CppObject * L_14 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Transactions.ISinglePhaseNotification>::get_Item(System.Int32) */, L_13, 0);
		Transaction_DoSingleCommit_m3243763006(__this, L_14, /*hidden argument*/NULL);
	}

IL_009a:
	{
		List_1_t2549268686 * L_15 = __this->get_volatiles_4();
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Transactions.IEnlistmentNotification>::get_Count() */, L_15);
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_00b1;
		}
	}
	{
		Transaction_DoCommitPhase_m3294991960(__this, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		Transaction_Complete_m3112384156(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::Complete()
extern "C"  void Transaction_Complete_m3112384156 (Transaction_t3175846586 * __this, const MethodInfo* method)
{
	{
		__this->set_committing_7((bool)0);
		__this->set_committed_8((bool)1);
		bool L_0 = __this->get_aborted_9();
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		TransactionInformation_t3294574474 * L_1 = __this->get_info_2();
		NullCheck(L_1);
		TransactionInformation_set_Status_m2073488130(L_1, 1, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void System.Transactions.Transaction::DoPreparePhase()
extern Il2CppClass* PreparingEnlistment_t3015517605_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnlistmentNotification_t1752309717_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t635051678_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2574341403_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m431675431_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1026347209_MethodInfo_var;
extern const uint32_t Transaction_DoPreparePhase_m1564783426_MetadataUsageId;
extern "C"  void Transaction_DoPreparePhase_m1564783426 (Transaction_t3175846586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transaction_DoPreparePhase_m1564783426_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PreparingEnlistment_t3015517605 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Enumerator_t635051678  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2549268686 * L_0 = __this->get_volatiles_4();
		NullCheck(L_0);
		Enumerator_t635051678  L_1 = List_1_GetEnumerator_m2574341403(L_0, /*hidden argument*/List_1_GetEnumerator_m2574341403_MethodInfo_var);
		V_2 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003f;
		}

IL_0011:
		{
			Il2CppObject * L_2 = Enumerator_get_Current_m431675431((&V_2), /*hidden argument*/Enumerator_get_Current_m431675431_MethodInfo_var);
			V_1 = L_2;
			Il2CppObject * L_3 = V_1;
			PreparingEnlistment_t3015517605 * L_4 = (PreparingEnlistment_t3015517605 *)il2cpp_codegen_object_new(PreparingEnlistment_t3015517605_il2cpp_TypeInfo_var);
			PreparingEnlistment__ctor_m3145960357(L_4, __this, L_3, /*hidden argument*/NULL);
			V_0 = L_4;
			Il2CppObject * L_5 = V_1;
			PreparingEnlistment_t3015517605 * L_6 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker1< PreparingEnlistment_t3015517605 * >::Invoke(1 /* System.Void System.Transactions.IEnlistmentNotification::Prepare(System.Transactions.PreparingEnlistment) */, IEnlistmentNotification_t1752309717_il2cpp_TypeInfo_var, L_5, L_6);
			PreparingEnlistment_t3015517605 * L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = PreparingEnlistment_get_IsPrepared_m4211467806(L_7, /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_003f;
			}
		}

IL_0033:
		{
			Transaction_set_Aborted_m4230882120(__this, (bool)1, /*hidden argument*/NULL);
			goto IL_004b;
		}

IL_003f:
		{
			bool L_9 = Enumerator_MoveNext_m1026347209((&V_2), /*hidden argument*/Enumerator_MoveNext_m1026347209_MethodInfo_var);
			if (L_9)
			{
				goto IL_0011;
			}
		}

IL_004b:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_0050);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		Enumerator_t635051678  L_10 = V_2;
		Enumerator_t635051678  L_11 = L_10;
		Il2CppObject * L_12 = Box(Enumerator_t635051678_il2cpp_TypeInfo_var, &L_11);
		NullCheck((Il2CppObject *)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
		IL2CPP_END_FINALLY(80)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005c:
	{
		Transaction_CheckAborted_m1722162826(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::DoCommitPhase()
extern Il2CppClass* Enlistment_t3082063553_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnlistmentNotification_t1752309717_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t635051678_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2574341403_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m431675431_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1026347209_MethodInfo_var;
extern const uint32_t Transaction_DoCommitPhase_m3294991960_MetadataUsageId;
extern "C"  void Transaction_DoCommitPhase_m3294991960 (Transaction_t3175846586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transaction_DoCommitPhase_m3294991960_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Enumerator_t635051678  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enlistment_t3082063553 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2549268686 * L_0 = __this->get_volatiles_4();
		NullCheck(L_0);
		Enumerator_t635051678  L_1 = List_1_GetEnumerator_m2574341403(L_0, /*hidden argument*/List_1_GetEnumerator_m2574341403_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			Il2CppObject * L_2 = Enumerator_get_Current_m431675431((&V_1), /*hidden argument*/Enumerator_get_Current_m431675431_MethodInfo_var);
			V_0 = L_2;
			Enlistment_t3082063553 * L_3 = (Enlistment_t3082063553 *)il2cpp_codegen_object_new(Enlistment_t3082063553_il2cpp_TypeInfo_var);
			Enlistment__ctor_m1854573282(L_3, /*hidden argument*/NULL);
			V_2 = L_3;
			Il2CppObject * L_4 = V_0;
			Enlistment_t3082063553 * L_5 = V_2;
			NullCheck(L_4);
			InterfaceActionInvoker1< Enlistment_t3082063553 * >::Invoke(0 /* System.Void System.Transactions.IEnlistmentNotification::Commit(System.Transactions.Enlistment) */, IEnlistmentNotification_t1752309717_il2cpp_TypeInfo_var, L_4, L_5);
		}

IL_0026:
		{
			bool L_6 = Enumerator_MoveNext_m1026347209((&V_1), /*hidden argument*/Enumerator_MoveNext_m1026347209_MethodInfo_var);
			if (L_6)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_t635051678  L_7 = V_1;
		Enumerator_t635051678  L_8 = L_7;
		Il2CppObject * L_9 = Box(Enumerator_t635051678_il2cpp_TypeInfo_var, &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void System.Transactions.Transaction::DoSingleCommit(System.Transactions.ISinglePhaseNotification)
extern Il2CppClass* SinglePhaseEnlistment_t3022234868_il2cpp_TypeInfo_var;
extern Il2CppClass* ISinglePhaseNotification_t2163935249_il2cpp_TypeInfo_var;
extern const uint32_t Transaction_DoSingleCommit_m3243763006_MetadataUsageId;
extern "C"  void Transaction_DoSingleCommit_m3243763006 (Transaction_t3175846586 * __this, Il2CppObject * ___single0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transaction_DoSingleCommit_m3243763006_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SinglePhaseEnlistment_t3022234868 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___single0;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		Il2CppObject * L_1 = ___single0;
		SinglePhaseEnlistment_t3022234868 * L_2 = (SinglePhaseEnlistment_t3022234868 *)il2cpp_codegen_object_new(SinglePhaseEnlistment_t3022234868_il2cpp_TypeInfo_var);
		SinglePhaseEnlistment__ctor_m44442160(L_2, __this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Il2CppObject * L_3 = ___single0;
		SinglePhaseEnlistment_t3022234868 * L_4 = V_0;
		NullCheck(L_3);
		InterfaceActionInvoker1< SinglePhaseEnlistment_t3022234868 * >::Invoke(0 /* System.Void System.Transactions.ISinglePhaseNotification::SinglePhaseCommit(System.Transactions.SinglePhaseEnlistment) */, ISinglePhaseNotification_t2163935249_il2cpp_TypeInfo_var, L_3, L_4);
		Transaction_CheckAborted_m1722162826(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.Transaction::CheckAborted()
extern Il2CppClass* TransactionAbortedException_t2212577306_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1828721127;
extern const uint32_t Transaction_CheckAborted_m1722162826_MetadataUsageId;
extern "C"  void Transaction_CheckAborted_m1722162826 (Transaction_t3175846586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transaction_CheckAborted_m1722162826_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_aborted_9();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Exception_t1967233988 * L_1 = __this->get_innerException_11();
		TransactionAbortedException_t2212577306 * L_2 = (TransactionAbortedException_t2212577306 *)il2cpp_codegen_object_new(TransactionAbortedException_t2212577306_il2cpp_TypeInfo_var);
		TransactionAbortedException__ctor_m3301525973(L_2, _stringLiteral1828721127, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		return;
	}
}
// System.Void System.Transactions.Transaction::EnsureIncompleteCurrentScope()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral987091203;
extern const uint32_t Transaction_EnsureIncompleteCurrentScope_m144438170_MetadataUsageId;
extern "C"  void Transaction_EnsureIncompleteCurrentScope_m144438170 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transaction_EnsureIncompleteCurrentScope_m144438170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transaction_t3175846586 * L_0 = Transaction_get_CurrentInternal_m4103191067(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Transaction_op_Equality_m825383295(NULL /*static, unused*/, L_0, (Transaction_t3175846586 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Transaction_t3175846586 * L_2 = Transaction_get_CurrentInternal_m4103191067(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		TransactionScope_t3316380210 * L_3 = Transaction_get_Scope_m2741316771(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		Transaction_t3175846586 * L_4 = Transaction_get_CurrentInternal_m4103191067(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		TransactionScope_t3316380210 * L_5 = Transaction_get_Scope_m2741316771(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = TransactionScope_get_IsComplete_m2704364895(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_7 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_7, _stringLiteral987091203, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean System.Transactions.Transaction::op_Equality(System.Transactions.Transaction,System.Transactions.Transaction)
extern "C"  bool Transaction_op_Equality_m825383295 (Il2CppObject * __this /* static, unused */, Transaction_t3175846586 * ___x0, Transaction_t3175846586 * ___y1, const MethodInfo* method)
{
	{
		Transaction_t3175846586 * L_0 = ___x0;
		bool L_1 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_0, NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		Transaction_t3175846586 * L_2 = ___y1;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_2, NULL, /*hidden argument*/NULL);
		return L_3;
	}

IL_0014:
	{
		Transaction_t3175846586 * L_4 = ___x0;
		Transaction_t3175846586 * L_5 = ___y1;
		NullCheck(L_4);
		bool L_6 = Transaction_Equals_m3895543116(L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean System.Transactions.Transaction::op_Inequality(System.Transactions.Transaction,System.Transactions.Transaction)
extern "C"  bool Transaction_op_Inequality_m4227681530 (Il2CppObject * __this /* static, unused */, Transaction_t3175846586 * ___x0, Transaction_t3175846586 * ___y1, const MethodInfo* method)
{
	{
		Transaction_t3175846586 * L_0 = ___x0;
		Transaction_t3175846586 * L_1 = ___y1;
		bool L_2 = Transaction_op_Equality_m825383295(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Transactions.Transaction/AsyncCommit::.ctor(System.Object,System.IntPtr)
extern "C"  void AsyncCommit__ctor_m433227045 (AsyncCommit_t802550451 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Transactions.Transaction/AsyncCommit::Invoke()
extern "C"  void AsyncCommit_Invoke_m4102084031 (AsyncCommit_t802550451 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AsyncCommit_Invoke_m4102084031((AsyncCommit_t802550451 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_AsyncCommit_t802550451(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult System.Transactions.Transaction/AsyncCommit::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AsyncCommit_BeginInvoke_m3788276972 (AsyncCommit_t802550451 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void System.Transactions.Transaction/AsyncCommit::EndInvoke(System.IAsyncResult)
extern "C"  void AsyncCommit_EndInvoke_m1956139445 (AsyncCommit_t802550451 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Transactions.TransactionAbortedException::.ctor()
extern "C"  void TransactionAbortedException__ctor_m3591106337 (TransactionAbortedException_t2212577306 * __this, const MethodInfo* method)
{
	{
		TransactionException__ctor_m1094974198(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionAbortedException::.ctor(System.String,System.Exception)
extern "C"  void TransactionAbortedException__ctor_m3301525973 (TransactionAbortedException_t2212577306 * __this, String_t* ___message0, Exception_t1967233988 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1967233988 * L_1 = ___innerException1;
		TransactionException__ctor_m608875242(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionAbortedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void TransactionAbortedException__ctor_m1275469026 (TransactionAbortedException_t2212577306 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info0;
		StreamingContext_t986364934  L_1 = ___context1;
		TransactionException__ctor_m3888881463(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionCompletedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void TransactionCompletedEventHandler__ctor_m426193240 (TransactionCompletedEventHandler_t1858411801 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Transactions.TransactionCompletedEventHandler::Invoke(System.Object,System.Transactions.TransactionEventArgs)
extern "C"  void TransactionCompletedEventHandler_Invoke_m3537370257 (TransactionCompletedEventHandler_t1858411801 * __this, Il2CppObject * ___o0, TransactionEventArgs_t2318060917 * ___e1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TransactionCompletedEventHandler_Invoke_m3537370257((TransactionCompletedEventHandler_t1858411801 *)__this->get_prev_9(),___o0, ___e1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___o0, TransactionEventArgs_t2318060917 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___o0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___o0, TransactionEventArgs_t2318060917 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___o0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, TransactionEventArgs_t2318060917 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___o0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_TransactionCompletedEventHandler_t1858411801(Il2CppObject* delegate, Il2CppObject * ___o0, TransactionEventArgs_t2318060917 * ___e1)
{
	// Marshaling of parameter '___o0' to native representation
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Transactions.TransactionCompletedEventHandler::BeginInvoke(System.Object,System.Transactions.TransactionEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TransactionCompletedEventHandler_BeginInvoke_m4102746350 (TransactionCompletedEventHandler_t1858411801 * __this, Il2CppObject * ___o0, TransactionEventArgs_t2318060917 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___o0;
	__d_args[1] = ___e1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Transactions.TransactionCompletedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void TransactionCompletedEventHandler_EndInvoke_m855183208 (TransactionCompletedEventHandler_t1858411801 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Transactions.TransactionException::.ctor()
extern "C"  void TransactionException__ctor_m1094974198 (TransactionException_t3768828717 * __this, const MethodInfo* method)
{
	{
		SystemException__ctor_m677430257(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionException::.ctor(System.String)
extern "C"  void TransactionException__ctor_m2617484236 (TransactionException_t3768828717 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		SystemException__ctor_m3697314481(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionException::.ctor(System.String,System.Exception)
extern "C"  void TransactionException__ctor_m608875242 (TransactionException_t3768828717 * __this, String_t* ___message0, Exception_t1967233988 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1967233988 * L_1 = ___innerException1;
		SystemException__ctor_m253088421(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void TransactionException__ctor_m3888881463 (TransactionException_t3768828717 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t2995724695 * L_0 = ___info0;
		StreamingContext_t986364934  L_1 = ___context1;
		SystemException__ctor_m2083527090(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Transactions.TransactionInformation::.ctor()
extern Il2CppClass* Guid_t2778838590_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1847;
extern const uint32_t TransactionInformation__ctor_m49122873_MetadataUsageId;
extern "C"  void TransactionInformation__ctor_m49122873 (TransactionInformation_t3294574474 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransactionInformation__ctor_m49122873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t339033936  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Guid_t2778838590  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t2778838590_il2cpp_TypeInfo_var);
		Guid_t2778838590  L_0 = ((Guid_t2778838590_StaticFields*)Guid_t2778838590_il2cpp_TypeInfo_var->static_fields)->get_Empty_11();
		__this->set_dtcId_1(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_status_3(0);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_1 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		DateTime_t339033936  L_2 = DateTime_ToUniversalTime_m691668206((&V_0), /*hidden argument*/NULL);
		__this->set_creation_time_2(L_2);
		Guid_t2778838590  L_3 = Guid_NewGuid_m3560729310(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = Guid_ToString_m2528531937((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m138640077(NULL /*static, unused*/, L_4, _stringLiteral1847, /*hidden argument*/NULL);
		__this->set_local_id_0(L_5);
		return;
	}
}
// System.Transactions.TransactionStatus System.Transactions.TransactionInformation::get_Status()
extern "C"  int32_t TransactionInformation_get_Status_m4243117603 (TransactionInformation_t3294574474 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_status_3();
		return L_0;
	}
}
// System.Void System.Transactions.TransactionInformation::set_Status(System.Transactions.TransactionStatus)
extern "C"  void TransactionInformation_set_Status_m2073488130 (TransactionInformation_t3294574474 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_status_3(L_0);
		return;
	}
}
// System.Void System.Transactions.TransactionManager::.cctor()
extern Il2CppClass* TransactionManager_t1434080011_il2cpp_TypeInfo_var;
extern const uint32_t TransactionManager__cctor_m1121997013_MetadataUsageId;
extern "C"  void TransactionManager__cctor_m1121997013 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransactionManager__cctor_m1121997013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TimeSpan_t763862892  L_0;
		memset(&L_0, 0, sizeof(L_0));
		TimeSpan__ctor_m4160332047(&L_0, 0, 1, 0, /*hidden argument*/NULL);
		((TransactionManager_t1434080011_StaticFields*)TransactionManager_t1434080011_il2cpp_TypeInfo_var->static_fields)->set_defaultTimeout_0(L_0);
		TimeSpan_t763862892  L_1;
		memset(&L_1, 0, sizeof(L_1));
		TimeSpan__ctor_m4160332047(&L_1, 0, ((int32_t)10), 0, /*hidden argument*/NULL);
		((TransactionManager_t1434080011_StaticFields*)TransactionManager_t1434080011_il2cpp_TypeInfo_var->static_fields)->set_maxTimeout_1(L_1);
		return;
	}
}
// System.TimeSpan System.Transactions.TransactionManager::get_DefaultTimeout()
extern Il2CppClass* TransactionManager_t1434080011_il2cpp_TypeInfo_var;
extern const uint32_t TransactionManager_get_DefaultTimeout_m2612090002_MetadataUsageId;
extern "C"  TimeSpan_t763862892  TransactionManager_get_DefaultTimeout_m2612090002 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransactionManager_get_DefaultTimeout_m2612090002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TransactionManager_t1434080011_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_0 = ((TransactionManager_t1434080011_StaticFields*)TransactionManager_t1434080011_il2cpp_TypeInfo_var->static_fields)->get_defaultTimeout_0();
		return L_0;
	}
}
// System.Void System.Transactions.TransactionOptions::.ctor(System.Transactions.IsolationLevel,System.TimeSpan)
extern "C"  void TransactionOptions__ctor_m379171565 (TransactionOptions_t3644312060 * __this, int32_t ___level0, TimeSpan_t763862892  ___timeout1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___level0;
		__this->set_level_0(L_0);
		TimeSpan_t763862892  L_1 = ___timeout1;
		__this->set_timeout_1(L_1);
		return;
	}
}
// System.Boolean System.Transactions.TransactionOptions::Equals(System.Object)
extern Il2CppClass* TransactionOptions_t3644312060_il2cpp_TypeInfo_var;
extern const uint32_t TransactionOptions_Equals_m1860415864_MetadataUsageId;
extern "C"  bool TransactionOptions_Equals_m1860415864 (TransactionOptions_t3644312060 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransactionOptions_Equals_m1860415864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (((Il2CppObject *)IsInstSealed(L_0, TransactionOptions_t3644312060_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		bool L_2 = TransactionOptions_op_Equality_m4085403305(NULL /*static, unused*/, (*(TransactionOptions_t3644312060 *)__this), ((*(TransactionOptions_t3644312060 *)((TransactionOptions_t3644312060 *)UnBox (L_1, TransactionOptions_t3644312060_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 System.Transactions.TransactionOptions::GetHashCode()
extern "C"  int32_t TransactionOptions_GetHashCode_m578159260 (TransactionOptions_t3644312060 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_level_0();
		TimeSpan_t763862892 * L_1 = __this->get_address_of_timeout_1();
		int32_t L_2 = TimeSpan_GetHashCode_m3188156777(L_1, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0^(int32_t)L_2));
	}
}
// System.Boolean System.Transactions.TransactionOptions::op_Equality(System.Transactions.TransactionOptions,System.Transactions.TransactionOptions)
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t TransactionOptions_op_Equality_m4085403305_MetadataUsageId;
extern "C"  bool TransactionOptions_op_Equality_m4085403305 (Il2CppObject * __this /* static, unused */, TransactionOptions_t3644312060  ___o10, TransactionOptions_t3644312060  ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransactionOptions_op_Equality_m4085403305_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (&___o10)->get_level_0();
		int32_t L_1 = (&___o21)->get_level_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0028;
		}
	}
	{
		TimeSpan_t763862892  L_2 = (&___o10)->get_timeout_1();
		TimeSpan_t763862892  L_3 = (&___o21)->get_timeout_1();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		bool L_4 = TimeSpan_op_Equality_m2213378780(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_0029;
	}

IL_0028:
	{
		G_B3_0 = 0;
	}

IL_0029:
	{
		return (bool)G_B3_0;
	}
}
// Conversion methods for marshalling of: System.Transactions.TransactionOptions
extern "C" void TransactionOptions_t3644312060_marshal_pinvoke(const TransactionOptions_t3644312060& unmarshaled, TransactionOptions_t3644312060_marshaled_pinvoke& marshaled)
{
	marshaled.___level_0 = unmarshaled.get_level_0();
	TimeSpan_t763862892_marshal_pinvoke(unmarshaled.get_timeout_1(), marshaled.___timeout_1);
}
extern "C" void TransactionOptions_t3644312060_marshal_pinvoke_back(const TransactionOptions_t3644312060_marshaled_pinvoke& marshaled, TransactionOptions_t3644312060& unmarshaled)
{
	int32_t unmarshaled_level_temp = 0;
	unmarshaled_level_temp = marshaled.___level_0;
	unmarshaled.set_level_0(unmarshaled_level_temp);
	TimeSpan_t763862892  unmarshaled_timeout_temp;
	memset(&unmarshaled_timeout_temp, 0, sizeof(unmarshaled_timeout_temp));
	TimeSpan_t763862892_marshal_pinvoke_back(marshaled.___timeout_1, unmarshaled_timeout_temp);
	unmarshaled.set_timeout_1(unmarshaled_timeout_temp);
}
// Conversion method for clean up from marshalling of: System.Transactions.TransactionOptions
extern "C" void TransactionOptions_t3644312060_marshal_pinvoke_cleanup(TransactionOptions_t3644312060_marshaled_pinvoke& marshaled)
{
	TimeSpan_t763862892_marshal_pinvoke_cleanup(marshaled.___timeout_1);
}
// Conversion methods for marshalling of: System.Transactions.TransactionOptions
extern "C" void TransactionOptions_t3644312060_marshal_com(const TransactionOptions_t3644312060& unmarshaled, TransactionOptions_t3644312060_marshaled_com& marshaled)
{
	marshaled.___level_0 = unmarshaled.get_level_0();
	TimeSpan_t763862892_marshal_com(unmarshaled.get_timeout_1(), marshaled.___timeout_1);
}
extern "C" void TransactionOptions_t3644312060_marshal_com_back(const TransactionOptions_t3644312060_marshaled_com& marshaled, TransactionOptions_t3644312060& unmarshaled)
{
	int32_t unmarshaled_level_temp = 0;
	unmarshaled_level_temp = marshaled.___level_0;
	unmarshaled.set_level_0(unmarshaled_level_temp);
	TimeSpan_t763862892  unmarshaled_timeout_temp;
	memset(&unmarshaled_timeout_temp, 0, sizeof(unmarshaled_timeout_temp));
	TimeSpan_t763862892_marshal_com_back(marshaled.___timeout_1, unmarshaled_timeout_temp);
	unmarshaled.set_timeout_1(unmarshaled_timeout_temp);
}
// Conversion method for clean up from marshalling of: System.Transactions.TransactionOptions
extern "C" void TransactionOptions_t3644312060_marshal_com_cleanup(TransactionOptions_t3644312060_marshaled_com& marshaled)
{
	TimeSpan_t763862892_marshal_com_cleanup(marshaled.___timeout_1);
}
// System.Void System.Transactions.TransactionScope::.cctor()
extern Il2CppClass* TransactionManager_t1434080011_il2cpp_TypeInfo_var;
extern Il2CppClass* TransactionScope_t3316380210_il2cpp_TypeInfo_var;
extern const uint32_t TransactionScope__cctor_m1305770556_MetadataUsageId;
extern "C"  void TransactionScope__cctor_m1305770556 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransactionScope__cctor_m1305770556_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TransactionManager_t1434080011_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_0 = TransactionManager_get_DefaultTimeout_m2612090002(NULL /*static, unused*/, /*hidden argument*/NULL);
		TransactionOptions_t3644312060  L_1;
		memset(&L_1, 0, sizeof(L_1));
		TransactionOptions__ctor_m379171565(&L_1, 0, L_0, /*hidden argument*/NULL);
		((TransactionScope_t3316380210_StaticFields*)TransactionScope_t3316380210_il2cpp_TypeInfo_var->static_fields)->set_defaultOptions_0(L_1);
		return;
	}
}
// System.Boolean System.Transactions.TransactionScope::get_IsComplete()
extern "C"  bool TransactionScope_get_IsComplete_m2704364895 (TransactionScope_t3316380210 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_completed_6();
		return L_0;
	}
}
// System.Void System.Transactions.TransactionScope::Dispose()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2938034291;
extern Il2CppCodeGenString* _stringLiteral1292509715;
extern const uint32_t TransactionScope_Dispose_m3782545294_MetadataUsageId;
extern "C"  void TransactionScope_Dispose_m3782545294 (TransactionScope_t3316380210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransactionScope_Dispose_m3782545294_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_disposed_5();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->set_disposed_5((bool)1);
		TransactionScope_t3316380210 * L_1 = __this->get_parentScope_3();
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		TransactionScope_t3316380210 * L_2 = __this->get_parentScope_3();
		TransactionScope_t3316380210 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_nested_4();
		NullCheck(L_3);
		L_3->set_nested_4(((int32_t)((int32_t)L_4-(int32_t)1)));
	}

IL_0031:
	{
		int32_t L_5 = __this->get_nested_4();
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0053;
		}
	}
	{
		Transaction_t3175846586 * L_6 = __this->get_transaction_1();
		NullCheck(L_6);
		Transaction_Rollback_m3171304839(L_6, /*hidden argument*/NULL);
		InvalidOperationException_t2420574324 * L_7 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_7, _stringLiteral2938034291, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0053:
	{
		Transaction_t3175846586 * L_8 = Transaction_get_CurrentInternal_m4103191067(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transaction_t3175846586 * L_9 = __this->get_transaction_1();
		bool L_10 = Transaction_op_Inequality_m4227681530(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00a9;
		}
	}
	{
		Transaction_t3175846586 * L_11 = __this->get_transaction_1();
		bool L_12 = Transaction_op_Inequality_m4227681530(NULL /*static, unused*/, L_11, (Transaction_t3175846586 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		Transaction_t3175846586 * L_13 = __this->get_transaction_1();
		NullCheck(L_13);
		Transaction_Rollback_m3171304839(L_13, /*hidden argument*/NULL);
	}

IL_0084:
	{
		Transaction_t3175846586 * L_14 = Transaction_get_CurrentInternal_m4103191067(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_15 = Transaction_op_Inequality_m4227681530(NULL /*static, unused*/, L_14, (Transaction_t3175846586 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_009e;
		}
	}
	{
		Transaction_t3175846586 * L_16 = Transaction_get_CurrentInternal_m4103191067(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transaction_Rollback_m3171304839(L_16, /*hidden argument*/NULL);
	}

IL_009e:
	{
		InvalidOperationException_t2420574324 * L_17 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_17, _stringLiteral1292509715, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_00a9:
	{
		Transaction_t3175846586 * L_18 = Transaction_get_CurrentInternal_m4103191067(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transaction_t3175846586 * L_19 = __this->get_oldTransaction_2();
		bool L_20 = Transaction_op_Equality_m825383295(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00e0;
		}
	}
	{
		Transaction_t3175846586 * L_21 = __this->get_oldTransaction_2();
		bool L_22 = Transaction_op_Inequality_m4227681530(NULL /*static, unused*/, L_21, (Transaction_t3175846586 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00e0;
		}
	}
	{
		Transaction_t3175846586 * L_23 = __this->get_oldTransaction_2();
		TransactionScope_t3316380210 * L_24 = __this->get_parentScope_3();
		NullCheck(L_23);
		Transaction_set_Scope_m1673949798(L_23, L_24, /*hidden argument*/NULL);
	}

IL_00e0:
	{
		Transaction_t3175846586 * L_25 = __this->get_oldTransaction_2();
		Transaction_set_CurrentInternal_m529514518(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		Transaction_t3175846586 * L_26 = __this->get_transaction_1();
		bool L_27 = Transaction_op_Equality_m825383295(NULL /*static, unused*/, L_26, (Transaction_t3175846586 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00fd;
		}
	}
	{
		return;
	}

IL_00fd:
	{
		Transaction_t3175846586 * L_28 = __this->get_transaction_1();
		NullCheck(L_28);
		Transaction_set_Scope_m1673949798(L_28, (TransactionScope_t3316380210 *)NULL, /*hidden argument*/NULL);
		bool L_29 = TransactionScope_get_IsComplete_m2704364895(__this, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_0120;
		}
	}
	{
		Transaction_t3175846586 * L_30 = __this->get_transaction_1();
		NullCheck(L_30);
		Transaction_Rollback_m3171304839(L_30, /*hidden argument*/NULL);
		return;
	}

IL_0120:
	{
		bool L_31 = __this->get_isRoot_7();
		if (L_31)
		{
			goto IL_012c;
		}
	}
	{
		return;
	}

IL_012c:
	{
		Transaction_t3175846586 * L_32 = __this->get_transaction_1();
		NullCheck(L_32);
		Transaction_CommitInternal_m578594359(L_32, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
