﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AnimationClip
struct AnimationClip_t57566497;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// player/AniClip
struct  AniClip_t806476588  : public Il2CppObject
{
public:
	// UnityEngine.AnimationClip player/AniClip::idle
	AnimationClip_t57566497 * ___idle_0;
	// UnityEngine.AnimationClip player/AniClip::walk
	AnimationClip_t57566497 * ___walk_1;
	// UnityEngine.AnimationClip player/AniClip::attack1
	AnimationClip_t57566497 * ___attack1_2;
	// UnityEngine.AnimationClip player/AniClip::attack2
	AnimationClip_t57566497 * ___attack2_3;
	// UnityEngine.AnimationClip player/AniClip::skillA
	AnimationClip_t57566497 * ___skillA_4;
	// UnityEngine.AnimationClip player/AniClip::skillB
	AnimationClip_t57566497 * ___skillB_5;
	// UnityEngine.AnimationClip player/AniClip::skillC
	AnimationClip_t57566497 * ___skillC_6;
	// UnityEngine.AnimationClip player/AniClip::stun
	AnimationClip_t57566497 * ___stun_7;
	// UnityEngine.AnimationClip player/AniClip::hit
	AnimationClip_t57566497 * ___hit_8;
	// UnityEngine.AnimationClip player/AniClip::idle_weapon
	AnimationClip_t57566497 * ___idle_weapon_9;
	// UnityEngine.AnimationClip player/AniClip::walk_weapon
	AnimationClip_t57566497 * ___walk_weapon_10;
	// UnityEngine.AnimationClip player/AniClip::attack_weapon
	AnimationClip_t57566497 * ___attack_weapon_11;
	// UnityEngine.AnimationClip player/AniClip::hit_weapon
	AnimationClip_t57566497 * ___hit_weapon_12;

public:
	inline static int32_t get_offset_of_idle_0() { return static_cast<int32_t>(offsetof(AniClip_t806476588, ___idle_0)); }
	inline AnimationClip_t57566497 * get_idle_0() const { return ___idle_0; }
	inline AnimationClip_t57566497 ** get_address_of_idle_0() { return &___idle_0; }
	inline void set_idle_0(AnimationClip_t57566497 * value)
	{
		___idle_0 = value;
		Il2CppCodeGenWriteBarrier(&___idle_0, value);
	}

	inline static int32_t get_offset_of_walk_1() { return static_cast<int32_t>(offsetof(AniClip_t806476588, ___walk_1)); }
	inline AnimationClip_t57566497 * get_walk_1() const { return ___walk_1; }
	inline AnimationClip_t57566497 ** get_address_of_walk_1() { return &___walk_1; }
	inline void set_walk_1(AnimationClip_t57566497 * value)
	{
		___walk_1 = value;
		Il2CppCodeGenWriteBarrier(&___walk_1, value);
	}

	inline static int32_t get_offset_of_attack1_2() { return static_cast<int32_t>(offsetof(AniClip_t806476588, ___attack1_2)); }
	inline AnimationClip_t57566497 * get_attack1_2() const { return ___attack1_2; }
	inline AnimationClip_t57566497 ** get_address_of_attack1_2() { return &___attack1_2; }
	inline void set_attack1_2(AnimationClip_t57566497 * value)
	{
		___attack1_2 = value;
		Il2CppCodeGenWriteBarrier(&___attack1_2, value);
	}

	inline static int32_t get_offset_of_attack2_3() { return static_cast<int32_t>(offsetof(AniClip_t806476588, ___attack2_3)); }
	inline AnimationClip_t57566497 * get_attack2_3() const { return ___attack2_3; }
	inline AnimationClip_t57566497 ** get_address_of_attack2_3() { return &___attack2_3; }
	inline void set_attack2_3(AnimationClip_t57566497 * value)
	{
		___attack2_3 = value;
		Il2CppCodeGenWriteBarrier(&___attack2_3, value);
	}

	inline static int32_t get_offset_of_skillA_4() { return static_cast<int32_t>(offsetof(AniClip_t806476588, ___skillA_4)); }
	inline AnimationClip_t57566497 * get_skillA_4() const { return ___skillA_4; }
	inline AnimationClip_t57566497 ** get_address_of_skillA_4() { return &___skillA_4; }
	inline void set_skillA_4(AnimationClip_t57566497 * value)
	{
		___skillA_4 = value;
		Il2CppCodeGenWriteBarrier(&___skillA_4, value);
	}

	inline static int32_t get_offset_of_skillB_5() { return static_cast<int32_t>(offsetof(AniClip_t806476588, ___skillB_5)); }
	inline AnimationClip_t57566497 * get_skillB_5() const { return ___skillB_5; }
	inline AnimationClip_t57566497 ** get_address_of_skillB_5() { return &___skillB_5; }
	inline void set_skillB_5(AnimationClip_t57566497 * value)
	{
		___skillB_5 = value;
		Il2CppCodeGenWriteBarrier(&___skillB_5, value);
	}

	inline static int32_t get_offset_of_skillC_6() { return static_cast<int32_t>(offsetof(AniClip_t806476588, ___skillC_6)); }
	inline AnimationClip_t57566497 * get_skillC_6() const { return ___skillC_6; }
	inline AnimationClip_t57566497 ** get_address_of_skillC_6() { return &___skillC_6; }
	inline void set_skillC_6(AnimationClip_t57566497 * value)
	{
		___skillC_6 = value;
		Il2CppCodeGenWriteBarrier(&___skillC_6, value);
	}

	inline static int32_t get_offset_of_stun_7() { return static_cast<int32_t>(offsetof(AniClip_t806476588, ___stun_7)); }
	inline AnimationClip_t57566497 * get_stun_7() const { return ___stun_7; }
	inline AnimationClip_t57566497 ** get_address_of_stun_7() { return &___stun_7; }
	inline void set_stun_7(AnimationClip_t57566497 * value)
	{
		___stun_7 = value;
		Il2CppCodeGenWriteBarrier(&___stun_7, value);
	}

	inline static int32_t get_offset_of_hit_8() { return static_cast<int32_t>(offsetof(AniClip_t806476588, ___hit_8)); }
	inline AnimationClip_t57566497 * get_hit_8() const { return ___hit_8; }
	inline AnimationClip_t57566497 ** get_address_of_hit_8() { return &___hit_8; }
	inline void set_hit_8(AnimationClip_t57566497 * value)
	{
		___hit_8 = value;
		Il2CppCodeGenWriteBarrier(&___hit_8, value);
	}

	inline static int32_t get_offset_of_idle_weapon_9() { return static_cast<int32_t>(offsetof(AniClip_t806476588, ___idle_weapon_9)); }
	inline AnimationClip_t57566497 * get_idle_weapon_9() const { return ___idle_weapon_9; }
	inline AnimationClip_t57566497 ** get_address_of_idle_weapon_9() { return &___idle_weapon_9; }
	inline void set_idle_weapon_9(AnimationClip_t57566497 * value)
	{
		___idle_weapon_9 = value;
		Il2CppCodeGenWriteBarrier(&___idle_weapon_9, value);
	}

	inline static int32_t get_offset_of_walk_weapon_10() { return static_cast<int32_t>(offsetof(AniClip_t806476588, ___walk_weapon_10)); }
	inline AnimationClip_t57566497 * get_walk_weapon_10() const { return ___walk_weapon_10; }
	inline AnimationClip_t57566497 ** get_address_of_walk_weapon_10() { return &___walk_weapon_10; }
	inline void set_walk_weapon_10(AnimationClip_t57566497 * value)
	{
		___walk_weapon_10 = value;
		Il2CppCodeGenWriteBarrier(&___walk_weapon_10, value);
	}

	inline static int32_t get_offset_of_attack_weapon_11() { return static_cast<int32_t>(offsetof(AniClip_t806476588, ___attack_weapon_11)); }
	inline AnimationClip_t57566497 * get_attack_weapon_11() const { return ___attack_weapon_11; }
	inline AnimationClip_t57566497 ** get_address_of_attack_weapon_11() { return &___attack_weapon_11; }
	inline void set_attack_weapon_11(AnimationClip_t57566497 * value)
	{
		___attack_weapon_11 = value;
		Il2CppCodeGenWriteBarrier(&___attack_weapon_11, value);
	}

	inline static int32_t get_offset_of_hit_weapon_12() { return static_cast<int32_t>(offsetof(AniClip_t806476588, ___hit_weapon_12)); }
	inline AnimationClip_t57566497 * get_hit_weapon_12() const { return ___hit_weapon_12; }
	inline AnimationClip_t57566497 ** get_address_of_hit_weapon_12() { return &___hit_weapon_12; }
	inline void set_hit_weapon_12(AnimationClip_t57566497 * value)
	{
		___hit_weapon_12 = value;
		Il2CppCodeGenWriteBarrier(&___hit_weapon_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
