﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t3628549054;
// UnityEngine.AudioClip
struct AudioClip_t3714538611;
// UnityEngine.UI.Slider
struct Slider_t1468074762;
// PlayerEffects
struct PlayerEffects_t1618007745;
// PlayerPickups
struct PlayerPickups_t2873926454;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Ray1522967639.h"
#include "UnityEngine_UnityEngine_RaycastHit46221527.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerAttack
struct  PlayerAttack_t3691797673  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.AudioSource PlayerAttack::audio
	AudioSource_t3628549054 * ___audio_2;
	// UnityEngine.AudioClip PlayerAttack::specialClip
	AudioClip_t3714538611 * ___specialClip_3;
	// UnityEngine.AudioClip PlayerAttack::normalClip
	AudioClip_t3714538611 * ___normalClip_4;
	// UnityEngine.UI.Slider PlayerAttack::specialAtkSlider
	Slider_t1468074762 * ___specialAtkSlider_5;
	// System.Int32 PlayerAttack::damagePerHit
	int32_t ___damagePerHit_6;
	// System.Single PlayerAttack::timeBetweenAttacks
	float ___timeBetweenAttacks_7;
	// System.Single PlayerAttack::timer
	float ___timer_8;
	// System.Single PlayerAttack::atkEffectTimer
	float ___atkEffectTimer_9;
	// System.Int32 PlayerAttack::hitableMask
	int32_t ___hitableMask_10;
	// System.Boolean PlayerAttack::isAttacking
	bool ___isAttacking_11;
	// UnityEngine.Ray PlayerAttack::shootRay
	Ray_t1522967639  ___shootRay_12;
	// UnityEngine.RaycastHit PlayerAttack::shootHit
	RaycastHit_t46221527  ___shootHit_13;
	// PlayerEffects PlayerAttack::playerEffects
	PlayerEffects_t1618007745 * ___playerEffects_14;
	// PlayerPickups PlayerAttack::playerPickups
	PlayerPickups_t2873926454 * ___playerPickups_15;

public:
	inline static int32_t get_offset_of_audio_2() { return static_cast<int32_t>(offsetof(PlayerAttack_t3691797673, ___audio_2)); }
	inline AudioSource_t3628549054 * get_audio_2() const { return ___audio_2; }
	inline AudioSource_t3628549054 ** get_address_of_audio_2() { return &___audio_2; }
	inline void set_audio_2(AudioSource_t3628549054 * value)
	{
		___audio_2 = value;
		Il2CppCodeGenWriteBarrier(&___audio_2, value);
	}

	inline static int32_t get_offset_of_specialClip_3() { return static_cast<int32_t>(offsetof(PlayerAttack_t3691797673, ___specialClip_3)); }
	inline AudioClip_t3714538611 * get_specialClip_3() const { return ___specialClip_3; }
	inline AudioClip_t3714538611 ** get_address_of_specialClip_3() { return &___specialClip_3; }
	inline void set_specialClip_3(AudioClip_t3714538611 * value)
	{
		___specialClip_3 = value;
		Il2CppCodeGenWriteBarrier(&___specialClip_3, value);
	}

	inline static int32_t get_offset_of_normalClip_4() { return static_cast<int32_t>(offsetof(PlayerAttack_t3691797673, ___normalClip_4)); }
	inline AudioClip_t3714538611 * get_normalClip_4() const { return ___normalClip_4; }
	inline AudioClip_t3714538611 ** get_address_of_normalClip_4() { return &___normalClip_4; }
	inline void set_normalClip_4(AudioClip_t3714538611 * value)
	{
		___normalClip_4 = value;
		Il2CppCodeGenWriteBarrier(&___normalClip_4, value);
	}

	inline static int32_t get_offset_of_specialAtkSlider_5() { return static_cast<int32_t>(offsetof(PlayerAttack_t3691797673, ___specialAtkSlider_5)); }
	inline Slider_t1468074762 * get_specialAtkSlider_5() const { return ___specialAtkSlider_5; }
	inline Slider_t1468074762 ** get_address_of_specialAtkSlider_5() { return &___specialAtkSlider_5; }
	inline void set_specialAtkSlider_5(Slider_t1468074762 * value)
	{
		___specialAtkSlider_5 = value;
		Il2CppCodeGenWriteBarrier(&___specialAtkSlider_5, value);
	}

	inline static int32_t get_offset_of_damagePerHit_6() { return static_cast<int32_t>(offsetof(PlayerAttack_t3691797673, ___damagePerHit_6)); }
	inline int32_t get_damagePerHit_6() const { return ___damagePerHit_6; }
	inline int32_t* get_address_of_damagePerHit_6() { return &___damagePerHit_6; }
	inline void set_damagePerHit_6(int32_t value)
	{
		___damagePerHit_6 = value;
	}

	inline static int32_t get_offset_of_timeBetweenAttacks_7() { return static_cast<int32_t>(offsetof(PlayerAttack_t3691797673, ___timeBetweenAttacks_7)); }
	inline float get_timeBetweenAttacks_7() const { return ___timeBetweenAttacks_7; }
	inline float* get_address_of_timeBetweenAttacks_7() { return &___timeBetweenAttacks_7; }
	inline void set_timeBetweenAttacks_7(float value)
	{
		___timeBetweenAttacks_7 = value;
	}

	inline static int32_t get_offset_of_timer_8() { return static_cast<int32_t>(offsetof(PlayerAttack_t3691797673, ___timer_8)); }
	inline float get_timer_8() const { return ___timer_8; }
	inline float* get_address_of_timer_8() { return &___timer_8; }
	inline void set_timer_8(float value)
	{
		___timer_8 = value;
	}

	inline static int32_t get_offset_of_atkEffectTimer_9() { return static_cast<int32_t>(offsetof(PlayerAttack_t3691797673, ___atkEffectTimer_9)); }
	inline float get_atkEffectTimer_9() const { return ___atkEffectTimer_9; }
	inline float* get_address_of_atkEffectTimer_9() { return &___atkEffectTimer_9; }
	inline void set_atkEffectTimer_9(float value)
	{
		___atkEffectTimer_9 = value;
	}

	inline static int32_t get_offset_of_hitableMask_10() { return static_cast<int32_t>(offsetof(PlayerAttack_t3691797673, ___hitableMask_10)); }
	inline int32_t get_hitableMask_10() const { return ___hitableMask_10; }
	inline int32_t* get_address_of_hitableMask_10() { return &___hitableMask_10; }
	inline void set_hitableMask_10(int32_t value)
	{
		___hitableMask_10 = value;
	}

	inline static int32_t get_offset_of_isAttacking_11() { return static_cast<int32_t>(offsetof(PlayerAttack_t3691797673, ___isAttacking_11)); }
	inline bool get_isAttacking_11() const { return ___isAttacking_11; }
	inline bool* get_address_of_isAttacking_11() { return &___isAttacking_11; }
	inline void set_isAttacking_11(bool value)
	{
		___isAttacking_11 = value;
	}

	inline static int32_t get_offset_of_shootRay_12() { return static_cast<int32_t>(offsetof(PlayerAttack_t3691797673, ___shootRay_12)); }
	inline Ray_t1522967639  get_shootRay_12() const { return ___shootRay_12; }
	inline Ray_t1522967639 * get_address_of_shootRay_12() { return &___shootRay_12; }
	inline void set_shootRay_12(Ray_t1522967639  value)
	{
		___shootRay_12 = value;
	}

	inline static int32_t get_offset_of_shootHit_13() { return static_cast<int32_t>(offsetof(PlayerAttack_t3691797673, ___shootHit_13)); }
	inline RaycastHit_t46221527  get_shootHit_13() const { return ___shootHit_13; }
	inline RaycastHit_t46221527 * get_address_of_shootHit_13() { return &___shootHit_13; }
	inline void set_shootHit_13(RaycastHit_t46221527  value)
	{
		___shootHit_13 = value;
	}

	inline static int32_t get_offset_of_playerEffects_14() { return static_cast<int32_t>(offsetof(PlayerAttack_t3691797673, ___playerEffects_14)); }
	inline PlayerEffects_t1618007745 * get_playerEffects_14() const { return ___playerEffects_14; }
	inline PlayerEffects_t1618007745 ** get_address_of_playerEffects_14() { return &___playerEffects_14; }
	inline void set_playerEffects_14(PlayerEffects_t1618007745 * value)
	{
		___playerEffects_14 = value;
		Il2CppCodeGenWriteBarrier(&___playerEffects_14, value);
	}

	inline static int32_t get_offset_of_playerPickups_15() { return static_cast<int32_t>(offsetof(PlayerAttack_t3691797673, ___playerPickups_15)); }
	inline PlayerPickups_t2873926454 * get_playerPickups_15() const { return ___playerPickups_15; }
	inline PlayerPickups_t2873926454 ** get_address_of_playerPickups_15() { return &___playerPickups_15; }
	inline void set_playerPickups_15(PlayerPickups_t2873926454 * value)
	{
		___playerPickups_15 = value;
		Il2CppCodeGenWriteBarrier(&___playerPickups_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
