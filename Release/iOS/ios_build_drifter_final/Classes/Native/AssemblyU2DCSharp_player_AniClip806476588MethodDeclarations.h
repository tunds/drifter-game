﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// player/AniClip
struct AniClip_t806476588;

#include "codegen/il2cpp-codegen.h"

// System.Void player/AniClip::.ctor()
extern "C"  void AniClip__ctor_m2105141341 (AniClip_t806476588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
