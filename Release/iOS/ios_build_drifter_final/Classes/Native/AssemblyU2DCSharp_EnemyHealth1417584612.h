﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Slider
struct Slider_t1468074762;
// UnityEngine.AudioClip
struct AudioClip_t3714538611;
// UnityEngine.AudioSource
struct AudioSource_t3628549054;
// UnityEngine.CapsuleCollider[]
struct CapsuleColliderU5BU5D_t3130374427;
// EnemyAttack
struct EnemyAttack_t1231588304;
// UnityEngine.Animator
struct Animator_t792326996;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyHealth
struct  EnemyHealth_t1417584612  : public MonoBehaviour_t3012272455
{
public:
	// System.Single EnemyHealth::time
	float ___time_2;
	// System.Boolean EnemyHealth::isDead
	bool ___isDead_3;
	// System.Boolean EnemyHealth::isSinking
	bool ___isSinking_4;
	// System.Int32 EnemyHealth::startHealth
	int32_t ___startHealth_5;
	// System.Int32 EnemyHealth::currentHealth
	int32_t ___currentHealth_6;
	// System.Single EnemyHealth::sinkSpeed
	float ___sinkSpeed_7;
	// UnityEngine.UI.Slider EnemyHealth::healthSlider
	Slider_t1468074762 * ___healthSlider_8;
	// UnityEngine.AudioClip EnemyHealth::deathAudio
	AudioClip_t3714538611 * ___deathAudio_9;
	// UnityEngine.AudioSource EnemyHealth::audio
	AudioSource_t3628549054 * ___audio_10;
	// UnityEngine.CapsuleCollider[] EnemyHealth::capCollider
	CapsuleColliderU5BU5D_t3130374427* ___capCollider_11;
	// EnemyAttack EnemyHealth::enemyAtk
	EnemyAttack_t1231588304 * ___enemyAtk_12;
	// UnityEngine.Animator EnemyHealth::anim
	Animator_t792326996 * ___anim_13;
	// System.Int32 EnemyHealth::healthHash
	int32_t ___healthHash_14;

public:
	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(EnemyHealth_t1417584612, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_isDead_3() { return static_cast<int32_t>(offsetof(EnemyHealth_t1417584612, ___isDead_3)); }
	inline bool get_isDead_3() const { return ___isDead_3; }
	inline bool* get_address_of_isDead_3() { return &___isDead_3; }
	inline void set_isDead_3(bool value)
	{
		___isDead_3 = value;
	}

	inline static int32_t get_offset_of_isSinking_4() { return static_cast<int32_t>(offsetof(EnemyHealth_t1417584612, ___isSinking_4)); }
	inline bool get_isSinking_4() const { return ___isSinking_4; }
	inline bool* get_address_of_isSinking_4() { return &___isSinking_4; }
	inline void set_isSinking_4(bool value)
	{
		___isSinking_4 = value;
	}

	inline static int32_t get_offset_of_startHealth_5() { return static_cast<int32_t>(offsetof(EnemyHealth_t1417584612, ___startHealth_5)); }
	inline int32_t get_startHealth_5() const { return ___startHealth_5; }
	inline int32_t* get_address_of_startHealth_5() { return &___startHealth_5; }
	inline void set_startHealth_5(int32_t value)
	{
		___startHealth_5 = value;
	}

	inline static int32_t get_offset_of_currentHealth_6() { return static_cast<int32_t>(offsetof(EnemyHealth_t1417584612, ___currentHealth_6)); }
	inline int32_t get_currentHealth_6() const { return ___currentHealth_6; }
	inline int32_t* get_address_of_currentHealth_6() { return &___currentHealth_6; }
	inline void set_currentHealth_6(int32_t value)
	{
		___currentHealth_6 = value;
	}

	inline static int32_t get_offset_of_sinkSpeed_7() { return static_cast<int32_t>(offsetof(EnemyHealth_t1417584612, ___sinkSpeed_7)); }
	inline float get_sinkSpeed_7() const { return ___sinkSpeed_7; }
	inline float* get_address_of_sinkSpeed_7() { return &___sinkSpeed_7; }
	inline void set_sinkSpeed_7(float value)
	{
		___sinkSpeed_7 = value;
	}

	inline static int32_t get_offset_of_healthSlider_8() { return static_cast<int32_t>(offsetof(EnemyHealth_t1417584612, ___healthSlider_8)); }
	inline Slider_t1468074762 * get_healthSlider_8() const { return ___healthSlider_8; }
	inline Slider_t1468074762 ** get_address_of_healthSlider_8() { return &___healthSlider_8; }
	inline void set_healthSlider_8(Slider_t1468074762 * value)
	{
		___healthSlider_8 = value;
		Il2CppCodeGenWriteBarrier(&___healthSlider_8, value);
	}

	inline static int32_t get_offset_of_deathAudio_9() { return static_cast<int32_t>(offsetof(EnemyHealth_t1417584612, ___deathAudio_9)); }
	inline AudioClip_t3714538611 * get_deathAudio_9() const { return ___deathAudio_9; }
	inline AudioClip_t3714538611 ** get_address_of_deathAudio_9() { return &___deathAudio_9; }
	inline void set_deathAudio_9(AudioClip_t3714538611 * value)
	{
		___deathAudio_9 = value;
		Il2CppCodeGenWriteBarrier(&___deathAudio_9, value);
	}

	inline static int32_t get_offset_of_audio_10() { return static_cast<int32_t>(offsetof(EnemyHealth_t1417584612, ___audio_10)); }
	inline AudioSource_t3628549054 * get_audio_10() const { return ___audio_10; }
	inline AudioSource_t3628549054 ** get_address_of_audio_10() { return &___audio_10; }
	inline void set_audio_10(AudioSource_t3628549054 * value)
	{
		___audio_10 = value;
		Il2CppCodeGenWriteBarrier(&___audio_10, value);
	}

	inline static int32_t get_offset_of_capCollider_11() { return static_cast<int32_t>(offsetof(EnemyHealth_t1417584612, ___capCollider_11)); }
	inline CapsuleColliderU5BU5D_t3130374427* get_capCollider_11() const { return ___capCollider_11; }
	inline CapsuleColliderU5BU5D_t3130374427** get_address_of_capCollider_11() { return &___capCollider_11; }
	inline void set_capCollider_11(CapsuleColliderU5BU5D_t3130374427* value)
	{
		___capCollider_11 = value;
		Il2CppCodeGenWriteBarrier(&___capCollider_11, value);
	}

	inline static int32_t get_offset_of_enemyAtk_12() { return static_cast<int32_t>(offsetof(EnemyHealth_t1417584612, ___enemyAtk_12)); }
	inline EnemyAttack_t1231588304 * get_enemyAtk_12() const { return ___enemyAtk_12; }
	inline EnemyAttack_t1231588304 ** get_address_of_enemyAtk_12() { return &___enemyAtk_12; }
	inline void set_enemyAtk_12(EnemyAttack_t1231588304 * value)
	{
		___enemyAtk_12 = value;
		Il2CppCodeGenWriteBarrier(&___enemyAtk_12, value);
	}

	inline static int32_t get_offset_of_anim_13() { return static_cast<int32_t>(offsetof(EnemyHealth_t1417584612, ___anim_13)); }
	inline Animator_t792326996 * get_anim_13() const { return ___anim_13; }
	inline Animator_t792326996 ** get_address_of_anim_13() { return &___anim_13; }
	inline void set_anim_13(Animator_t792326996 * value)
	{
		___anim_13 = value;
		Il2CppCodeGenWriteBarrier(&___anim_13, value);
	}

	inline static int32_t get_offset_of_healthHash_14() { return static_cast<int32_t>(offsetof(EnemyHealth_t1417584612, ___healthHash_14)); }
	inline int32_t get_healthHash_14() const { return ___healthHash_14; }
	inline int32_t* get_address_of_healthHash_14() { return &___healthHash_14; }
	inline void set_healthHash_14(int32_t value)
	{
		___healthHash_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
