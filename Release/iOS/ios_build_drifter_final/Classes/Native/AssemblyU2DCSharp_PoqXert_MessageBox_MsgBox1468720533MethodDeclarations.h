﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PoqXert.MessageBox.MsgBox
struct MsgBox_t1468720533;
// System.String
struct String_t;
// PoqXert.MessageBox.MSGBoxStyle
struct MSGBoxStyle_t1923386386;
// PoqXert.MessageBox.DialogResultMethod
struct DialogResultMethod_t1789391825;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_MsgBoxButtons1335831650.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_MSGBoxStyle1923386386.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_DialogResultM1789391825.h"
#include "AssemblyU2DCSharp_PoqXert_MessageBox_MsgBoxStyle2220920850.h"

// System.Void PoqXert.MessageBox.MsgBox::.ctor()
extern "C"  void MsgBox__ctor_m4017087300 (MsgBox_t1468720533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MsgBox::.cctor()
extern "C"  void MsgBox__cctor_m3788525801 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PoqXert.MessageBox.MsgBox PoqXert.MessageBox.MsgBox::get_prefab()
extern "C"  MsgBox_t1468720533 * MsgBox_get_prefab_m2374698177 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PoqXert.MessageBox.MsgBox PoqXert.MessageBox.MsgBox::get_instance()
extern "C"  MsgBox_t1468720533 * MsgBox_get_instance_m3950348434 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PoqXert.MessageBox.MsgBox::get_isOpen()
extern "C"  bool MsgBox_get_isOpen_m2684764489 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PoqXert.MessageBox.MsgBox::get_isModal()
extern "C"  bool MsgBox_get_isModal_m4113405872 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MsgBox::ButtonClickEvent(System.Int32)
extern "C"  void MsgBox_ButtonClickEvent_m2153297941 (MsgBox_t1468720533 * __this, int32_t ___btn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MsgBox::BuildMessageBox(System.Int32,System.String,System.String,PoqXert.MessageBox.MsgBoxButtons,PoqXert.MessageBox.MSGBoxStyle,PoqXert.MessageBox.DialogResultMethod,System.Boolean,System.String,System.String,System.String)
extern "C"  void MsgBox_BuildMessageBox_m96384347 (MsgBox_t1468720533 * __this, int32_t ___id0, String_t* ___mess1, String_t* ___caption2, int32_t ___btns3, MSGBoxStyle_t1923386386 * ___style4, DialogResultMethod_t1789391825 * ___method5, bool ___modal6, String_t* ___btnText07, String_t* ___btnText18, String_t* ___btnText29, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PoqXert.MessageBox.MSGBoxStyle PoqXert.MessageBox.MsgBox::GetStyle(PoqXert.MessageBox.MsgBoxStyle)
extern "C"  MSGBoxStyle_t1923386386 * MsgBox_GetStyle_m1286816734 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MsgBox::Show(System.Int32,System.String,PoqXert.MessageBox.DialogResultMethod,System.Boolean,System.String,System.String,System.String)
extern "C"  void MsgBox_Show_m3994277758 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___message1, DialogResultMethod_t1789391825 * ___method2, bool ___modal3, String_t* ___btnText04, String_t* ___btnText15, String_t* ___btnText26, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MsgBox::Show(System.Int32,System.String,System.String,PoqXert.MessageBox.DialogResultMethod,System.Boolean,System.String,System.String,System.String)
extern "C"  void MsgBox_Show_m3721224514 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___message1, String_t* ___caption2, DialogResultMethod_t1789391825 * ___method3, bool ___modal4, String_t* ___btnText05, String_t* ___btnText16, String_t* ___btnText27, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MsgBox::Show(System.Int32,System.String,System.String,PoqXert.MessageBox.MsgBoxButtons,PoqXert.MessageBox.DialogResultMethod,System.Boolean,System.String,System.String,System.String)
extern "C"  void MsgBox_Show_m34694978 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___message1, String_t* ___caption2, int32_t ___buttons3, DialogResultMethod_t1789391825 * ___method4, bool ___modal5, String_t* ___btnText06, String_t* ___btnText17, String_t* ___btnText28, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MsgBox::Show(System.Int32,System.String,System.String,PoqXert.MessageBox.MsgBoxButtons,PoqXert.MessageBox.MsgBoxStyle,PoqXert.MessageBox.DialogResultMethod,System.Boolean,System.String,System.String,System.String)
extern "C"  void MsgBox_Show_m3109697522 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___message1, String_t* ___caption2, int32_t ___buttons3, int32_t ___style4, DialogResultMethod_t1789391825 * ___method5, bool ___modal6, String_t* ___btnText07, String_t* ___btnText18, String_t* ___btnText29, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MsgBox::Show(System.Int32,System.String,System.String,PoqXert.MessageBox.MsgBoxButtons,PoqXert.MessageBox.MSGBoxStyle,PoqXert.MessageBox.DialogResultMethod,System.Boolean,System.String,System.String,System.String)
extern "C"  void MsgBox_Show_m2597204978 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___message1, String_t* ___caption2, int32_t ___buttons3, MSGBoxStyle_t1923386386 * ___style4, DialogResultMethod_t1789391825 * ___method5, bool ___modal6, String_t* ___btnText07, String_t* ___btnText18, String_t* ___btnText29, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MsgBox::Show(System.Int32,System.String,System.String,PoqXert.MessageBox.MsgBoxButtons,System.Int32,PoqXert.MessageBox.DialogResultMethod,System.Boolean,System.String,System.String,System.String)
extern "C"  void MsgBox_Show_m2815667799 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___message1, String_t* ___caption2, int32_t ___buttons3, int32_t ___customStyleID4, DialogResultMethod_t1789391825 * ___method5, bool ___modal6, String_t* ___btnText07, String_t* ___btnText18, String_t* ___btnText29, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PoqXert.MessageBox.MsgBox::Close()
extern "C"  void MsgBox_Close_m1432979546 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
