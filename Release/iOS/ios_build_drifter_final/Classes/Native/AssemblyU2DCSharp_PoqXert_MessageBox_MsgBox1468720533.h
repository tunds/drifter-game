﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t3317474837;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.UI.Text
struct Text_t3286458198;
// UnityEngine.UI.Image
struct Image_t3354615620;
// PoqXert.MessageBox.MSGBoxStyle
struct MSGBoxStyle_t1923386386;
// System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle>
struct List_1_t2720345355;
// PoqXert.MessageBox.MsgBox
struct MsgBox_t1468720533;
// PoqXert.MessageBox.DialogResultMethod
struct DialogResultMethod_t1789391825;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PoqXert.MessageBox.MsgBox
struct  MsgBox_t1468720533  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.RectTransform PoqXert.MessageBox.MsgBox::_mainPanel
	RectTransform_t3317474837 * ____mainPanel_2;
	// UnityEngine.GameObject PoqXert.MessageBox.MsgBox::_blockPanel
	GameObject_t4012695102 * ____blockPanel_3;
	// UnityEngine.UI.Text PoqXert.MessageBox.MsgBox::_captionText
	Text_t3286458198 * ____captionText_4;
	// UnityEngine.UI.Text PoqXert.MessageBox.MsgBox::_mainText
	Text_t3286458198 * ____mainText_5;
	// UnityEngine.UI.Image PoqXert.MessageBox.MsgBox::_captionImg
	Image_t3354615620 * ____captionImg_6;
	// UnityEngine.UI.Image PoqXert.MessageBox.MsgBox::_backgroundImg
	Image_t3354615620 * ____backgroundImg_7;
	// UnityEngine.UI.Image PoqXert.MessageBox.MsgBox::_iconImg
	Image_t3354615620 * ____iconImg_8;
	// UnityEngine.UI.Image PoqXert.MessageBox.MsgBox::_btnYesImg
	Image_t3354615620 * ____btnYesImg_9;
	// UnityEngine.UI.Image PoqXert.MessageBox.MsgBox::_btnNoImg
	Image_t3354615620 * ____btnNoImg_10;
	// UnityEngine.UI.Image PoqXert.MessageBox.MsgBox::_btnCancelImg
	Image_t3354615620 * ____btnCancelImg_11;
	// UnityEngine.GameObject PoqXert.MessageBox.MsgBox::_eventer
	GameObject_t4012695102 * ____eventer_12;
	// PoqXert.MessageBox.MSGBoxStyle PoqXert.MessageBox.MsgBox::_informationStyle
	MSGBoxStyle_t1923386386 * ____informationStyle_13;
	// PoqXert.MessageBox.MSGBoxStyle PoqXert.MessageBox.MsgBox::_questionStyle
	MSGBoxStyle_t1923386386 * ____questionStyle_14;
	// PoqXert.MessageBox.MSGBoxStyle PoqXert.MessageBox.MsgBox::_warningStyle
	MSGBoxStyle_t1923386386 * ____warningStyle_15;
	// PoqXert.MessageBox.MSGBoxStyle PoqXert.MessageBox.MsgBox::_errorStyle
	MSGBoxStyle_t1923386386 * ____errorStyle_16;
	// System.Collections.Generic.List`1<PoqXert.MessageBox.MSGBoxStyle> PoqXert.MessageBox.MsgBox::_customStyles
	List_1_t2720345355 * ____customStyles_17;
	// System.Int32 PoqXert.MessageBox.MsgBox::messageID
	int32_t ___messageID_20;
	// PoqXert.MessageBox.DialogResultMethod PoqXert.MessageBox.MsgBox::calledMethod
	DialogResultMethod_t1789391825 * ___calledMethod_21;

public:
	inline static int32_t get_offset_of__mainPanel_2() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____mainPanel_2)); }
	inline RectTransform_t3317474837 * get__mainPanel_2() const { return ____mainPanel_2; }
	inline RectTransform_t3317474837 ** get_address_of__mainPanel_2() { return &____mainPanel_2; }
	inline void set__mainPanel_2(RectTransform_t3317474837 * value)
	{
		____mainPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&____mainPanel_2, value);
	}

	inline static int32_t get_offset_of__blockPanel_3() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____blockPanel_3)); }
	inline GameObject_t4012695102 * get__blockPanel_3() const { return ____blockPanel_3; }
	inline GameObject_t4012695102 ** get_address_of__blockPanel_3() { return &____blockPanel_3; }
	inline void set__blockPanel_3(GameObject_t4012695102 * value)
	{
		____blockPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&____blockPanel_3, value);
	}

	inline static int32_t get_offset_of__captionText_4() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____captionText_4)); }
	inline Text_t3286458198 * get__captionText_4() const { return ____captionText_4; }
	inline Text_t3286458198 ** get_address_of__captionText_4() { return &____captionText_4; }
	inline void set__captionText_4(Text_t3286458198 * value)
	{
		____captionText_4 = value;
		Il2CppCodeGenWriteBarrier(&____captionText_4, value);
	}

	inline static int32_t get_offset_of__mainText_5() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____mainText_5)); }
	inline Text_t3286458198 * get__mainText_5() const { return ____mainText_5; }
	inline Text_t3286458198 ** get_address_of__mainText_5() { return &____mainText_5; }
	inline void set__mainText_5(Text_t3286458198 * value)
	{
		____mainText_5 = value;
		Il2CppCodeGenWriteBarrier(&____mainText_5, value);
	}

	inline static int32_t get_offset_of__captionImg_6() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____captionImg_6)); }
	inline Image_t3354615620 * get__captionImg_6() const { return ____captionImg_6; }
	inline Image_t3354615620 ** get_address_of__captionImg_6() { return &____captionImg_6; }
	inline void set__captionImg_6(Image_t3354615620 * value)
	{
		____captionImg_6 = value;
		Il2CppCodeGenWriteBarrier(&____captionImg_6, value);
	}

	inline static int32_t get_offset_of__backgroundImg_7() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____backgroundImg_7)); }
	inline Image_t3354615620 * get__backgroundImg_7() const { return ____backgroundImg_7; }
	inline Image_t3354615620 ** get_address_of__backgroundImg_7() { return &____backgroundImg_7; }
	inline void set__backgroundImg_7(Image_t3354615620 * value)
	{
		____backgroundImg_7 = value;
		Il2CppCodeGenWriteBarrier(&____backgroundImg_7, value);
	}

	inline static int32_t get_offset_of__iconImg_8() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____iconImg_8)); }
	inline Image_t3354615620 * get__iconImg_8() const { return ____iconImg_8; }
	inline Image_t3354615620 ** get_address_of__iconImg_8() { return &____iconImg_8; }
	inline void set__iconImg_8(Image_t3354615620 * value)
	{
		____iconImg_8 = value;
		Il2CppCodeGenWriteBarrier(&____iconImg_8, value);
	}

	inline static int32_t get_offset_of__btnYesImg_9() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____btnYesImg_9)); }
	inline Image_t3354615620 * get__btnYesImg_9() const { return ____btnYesImg_9; }
	inline Image_t3354615620 ** get_address_of__btnYesImg_9() { return &____btnYesImg_9; }
	inline void set__btnYesImg_9(Image_t3354615620 * value)
	{
		____btnYesImg_9 = value;
		Il2CppCodeGenWriteBarrier(&____btnYesImg_9, value);
	}

	inline static int32_t get_offset_of__btnNoImg_10() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____btnNoImg_10)); }
	inline Image_t3354615620 * get__btnNoImg_10() const { return ____btnNoImg_10; }
	inline Image_t3354615620 ** get_address_of__btnNoImg_10() { return &____btnNoImg_10; }
	inline void set__btnNoImg_10(Image_t3354615620 * value)
	{
		____btnNoImg_10 = value;
		Il2CppCodeGenWriteBarrier(&____btnNoImg_10, value);
	}

	inline static int32_t get_offset_of__btnCancelImg_11() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____btnCancelImg_11)); }
	inline Image_t3354615620 * get__btnCancelImg_11() const { return ____btnCancelImg_11; }
	inline Image_t3354615620 ** get_address_of__btnCancelImg_11() { return &____btnCancelImg_11; }
	inline void set__btnCancelImg_11(Image_t3354615620 * value)
	{
		____btnCancelImg_11 = value;
		Il2CppCodeGenWriteBarrier(&____btnCancelImg_11, value);
	}

	inline static int32_t get_offset_of__eventer_12() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____eventer_12)); }
	inline GameObject_t4012695102 * get__eventer_12() const { return ____eventer_12; }
	inline GameObject_t4012695102 ** get_address_of__eventer_12() { return &____eventer_12; }
	inline void set__eventer_12(GameObject_t4012695102 * value)
	{
		____eventer_12 = value;
		Il2CppCodeGenWriteBarrier(&____eventer_12, value);
	}

	inline static int32_t get_offset_of__informationStyle_13() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____informationStyle_13)); }
	inline MSGBoxStyle_t1923386386 * get__informationStyle_13() const { return ____informationStyle_13; }
	inline MSGBoxStyle_t1923386386 ** get_address_of__informationStyle_13() { return &____informationStyle_13; }
	inline void set__informationStyle_13(MSGBoxStyle_t1923386386 * value)
	{
		____informationStyle_13 = value;
		Il2CppCodeGenWriteBarrier(&____informationStyle_13, value);
	}

	inline static int32_t get_offset_of__questionStyle_14() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____questionStyle_14)); }
	inline MSGBoxStyle_t1923386386 * get__questionStyle_14() const { return ____questionStyle_14; }
	inline MSGBoxStyle_t1923386386 ** get_address_of__questionStyle_14() { return &____questionStyle_14; }
	inline void set__questionStyle_14(MSGBoxStyle_t1923386386 * value)
	{
		____questionStyle_14 = value;
		Il2CppCodeGenWriteBarrier(&____questionStyle_14, value);
	}

	inline static int32_t get_offset_of__warningStyle_15() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____warningStyle_15)); }
	inline MSGBoxStyle_t1923386386 * get__warningStyle_15() const { return ____warningStyle_15; }
	inline MSGBoxStyle_t1923386386 ** get_address_of__warningStyle_15() { return &____warningStyle_15; }
	inline void set__warningStyle_15(MSGBoxStyle_t1923386386 * value)
	{
		____warningStyle_15 = value;
		Il2CppCodeGenWriteBarrier(&____warningStyle_15, value);
	}

	inline static int32_t get_offset_of__errorStyle_16() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____errorStyle_16)); }
	inline MSGBoxStyle_t1923386386 * get__errorStyle_16() const { return ____errorStyle_16; }
	inline MSGBoxStyle_t1923386386 ** get_address_of__errorStyle_16() { return &____errorStyle_16; }
	inline void set__errorStyle_16(MSGBoxStyle_t1923386386 * value)
	{
		____errorStyle_16 = value;
		Il2CppCodeGenWriteBarrier(&____errorStyle_16, value);
	}

	inline static int32_t get_offset_of__customStyles_17() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ____customStyles_17)); }
	inline List_1_t2720345355 * get__customStyles_17() const { return ____customStyles_17; }
	inline List_1_t2720345355 ** get_address_of__customStyles_17() { return &____customStyles_17; }
	inline void set__customStyles_17(List_1_t2720345355 * value)
	{
		____customStyles_17 = value;
		Il2CppCodeGenWriteBarrier(&____customStyles_17, value);
	}

	inline static int32_t get_offset_of_messageID_20() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ___messageID_20)); }
	inline int32_t get_messageID_20() const { return ___messageID_20; }
	inline int32_t* get_address_of_messageID_20() { return &___messageID_20; }
	inline void set_messageID_20(int32_t value)
	{
		___messageID_20 = value;
	}

	inline static int32_t get_offset_of_calledMethod_21() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533, ___calledMethod_21)); }
	inline DialogResultMethod_t1789391825 * get_calledMethod_21() const { return ___calledMethod_21; }
	inline DialogResultMethod_t1789391825 ** get_address_of_calledMethod_21() { return &___calledMethod_21; }
	inline void set_calledMethod_21(DialogResultMethod_t1789391825 * value)
	{
		___calledMethod_21 = value;
		Il2CppCodeGenWriteBarrier(&___calledMethod_21, value);
	}
};

struct MsgBox_t1468720533_StaticFields
{
public:
	// PoqXert.MessageBox.MsgBox PoqXert.MessageBox.MsgBox::_prefab
	MsgBox_t1468720533 * ____prefab_18;
	// PoqXert.MessageBox.MsgBox PoqXert.MessageBox.MsgBox::_lastMsgBox
	MsgBox_t1468720533 * ____lastMsgBox_19;

public:
	inline static int32_t get_offset_of__prefab_18() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533_StaticFields, ____prefab_18)); }
	inline MsgBox_t1468720533 * get__prefab_18() const { return ____prefab_18; }
	inline MsgBox_t1468720533 ** get_address_of__prefab_18() { return &____prefab_18; }
	inline void set__prefab_18(MsgBox_t1468720533 * value)
	{
		____prefab_18 = value;
		Il2CppCodeGenWriteBarrier(&____prefab_18, value);
	}

	inline static int32_t get_offset_of__lastMsgBox_19() { return static_cast<int32_t>(offsetof(MsgBox_t1468720533_StaticFields, ____lastMsgBox_19)); }
	inline MsgBox_t1468720533 * get__lastMsgBox_19() const { return ____lastMsgBox_19; }
	inline MsgBox_t1468720533 ** get_address_of__lastMsgBox_19() { return &____lastMsgBox_19; }
	inline void set__lastMsgBox_19(MsgBox_t1468720533 * value)
	{
		____lastMsgBox_19 = value;
		Il2CppCodeGenWriteBarrier(&____lastMsgBox_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
