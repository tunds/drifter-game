﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SQLiteUpdateCallback
struct SQLiteUpdateCallback_t2915972065;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void Mono.Data.Sqlite.SQLiteUpdateCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void SQLiteUpdateCallback__ctor_m4011771473 (SQLiteUpdateCallback_t2915972065 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteUpdateCallback::Invoke(System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.Int64)
extern "C"  void SQLiteUpdateCallback_Invoke_m3808899022 (SQLiteUpdateCallback_t2915972065 * __this, IntPtr_t ___puser0, int32_t ___type1, IntPtr_t ___database2, IntPtr_t ___table3, int64_t ___rowid4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_SQLiteUpdateCallback_t2915972065(Il2CppObject* delegate, IntPtr_t ___puser0, int32_t ___type1, IntPtr_t ___database2, IntPtr_t ___table3, int64_t ___rowid4);
// System.IAsyncResult Mono.Data.Sqlite.SQLiteUpdateCallback::BeginInvoke(System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.Int64,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SQLiteUpdateCallback_BeginInvoke_m3421891597 (SQLiteUpdateCallback_t2915972065 * __this, IntPtr_t ___puser0, int32_t ___type1, IntPtr_t ___database2, IntPtr_t ___table3, int64_t ___rowid4, AsyncCallback_t1363551830 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SQLiteUpdateCallback::EndInvoke(System.IAsyncResult)
extern "C"  void SQLiteUpdateCallback_EndInvoke_m908855521 (SQLiteUpdateCallback_t2915972065 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
