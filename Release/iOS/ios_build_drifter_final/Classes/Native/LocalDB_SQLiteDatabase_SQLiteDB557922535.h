﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SQLiteDatabase.SQLiteDB
struct SQLiteDB_t557922535;
// System.String
struct String_t;
// Mono.Data.Sqlite.SqliteConnection
struct SqliteConnection_t3853176977;
// Mono.Data.Sqlite.SqliteDataReader
struct SqliteDataReader_t1567858368;
// Mono.Data.Sqlite.SqliteCommand
struct SqliteCommand_t4229878246;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLiteDatabase.SQLiteDB
struct  SQLiteDB_t557922535  : public Il2CppObject
{
public:
	// System.String SQLiteDatabase.SQLiteDB::_dbName
	String_t* ____dbName_1;
	// System.String SQLiteDatabase.SQLiteDB::_dbPath
	String_t* ____dbPath_2;
	// System.Boolean SQLiteDatabase.SQLiteDB::_isOverWrite
	bool ____isOverWrite_3;
	// Mono.Data.Sqlite.SqliteConnection SQLiteDatabase.SQLiteDB::connection
	SqliteConnection_t3853176977 * ___connection_4;
	// Mono.Data.Sqlite.SqliteDataReader SQLiteDatabase.SQLiteDB::reader
	SqliteDataReader_t1567858368 * ___reader_5;
	// Mono.Data.Sqlite.SqliteCommand SQLiteDatabase.SQLiteDB::command
	SqliteCommand_t4229878246 * ___command_6;

public:
	inline static int32_t get_offset_of__dbName_1() { return static_cast<int32_t>(offsetof(SQLiteDB_t557922535, ____dbName_1)); }
	inline String_t* get__dbName_1() const { return ____dbName_1; }
	inline String_t** get_address_of__dbName_1() { return &____dbName_1; }
	inline void set__dbName_1(String_t* value)
	{
		____dbName_1 = value;
		Il2CppCodeGenWriteBarrier(&____dbName_1, value);
	}

	inline static int32_t get_offset_of__dbPath_2() { return static_cast<int32_t>(offsetof(SQLiteDB_t557922535, ____dbPath_2)); }
	inline String_t* get__dbPath_2() const { return ____dbPath_2; }
	inline String_t** get_address_of__dbPath_2() { return &____dbPath_2; }
	inline void set__dbPath_2(String_t* value)
	{
		____dbPath_2 = value;
		Il2CppCodeGenWriteBarrier(&____dbPath_2, value);
	}

	inline static int32_t get_offset_of__isOverWrite_3() { return static_cast<int32_t>(offsetof(SQLiteDB_t557922535, ____isOverWrite_3)); }
	inline bool get__isOverWrite_3() const { return ____isOverWrite_3; }
	inline bool* get_address_of__isOverWrite_3() { return &____isOverWrite_3; }
	inline void set__isOverWrite_3(bool value)
	{
		____isOverWrite_3 = value;
	}

	inline static int32_t get_offset_of_connection_4() { return static_cast<int32_t>(offsetof(SQLiteDB_t557922535, ___connection_4)); }
	inline SqliteConnection_t3853176977 * get_connection_4() const { return ___connection_4; }
	inline SqliteConnection_t3853176977 ** get_address_of_connection_4() { return &___connection_4; }
	inline void set_connection_4(SqliteConnection_t3853176977 * value)
	{
		___connection_4 = value;
		Il2CppCodeGenWriteBarrier(&___connection_4, value);
	}

	inline static int32_t get_offset_of_reader_5() { return static_cast<int32_t>(offsetof(SQLiteDB_t557922535, ___reader_5)); }
	inline SqliteDataReader_t1567858368 * get_reader_5() const { return ___reader_5; }
	inline SqliteDataReader_t1567858368 ** get_address_of_reader_5() { return &___reader_5; }
	inline void set_reader_5(SqliteDataReader_t1567858368 * value)
	{
		___reader_5 = value;
		Il2CppCodeGenWriteBarrier(&___reader_5, value);
	}

	inline static int32_t get_offset_of_command_6() { return static_cast<int32_t>(offsetof(SQLiteDB_t557922535, ___command_6)); }
	inline SqliteCommand_t4229878246 * get_command_6() const { return ___command_6; }
	inline SqliteCommand_t4229878246 ** get_address_of_command_6() { return &___command_6; }
	inline void set_command_6(SqliteCommand_t4229878246 * value)
	{
		___command_6 = value;
		Il2CppCodeGenWriteBarrier(&___command_6, value);
	}
};

struct SQLiteDB_t557922535_StaticFields
{
public:
	// SQLiteDatabase.SQLiteDB SQLiteDatabase.SQLiteDB::_instance
	SQLiteDB_t557922535 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(SQLiteDB_t557922535_StaticFields, ____instance_0)); }
	inline SQLiteDB_t557922535 * get__instance_0() const { return ____instance_0; }
	inline SQLiteDB_t557922535 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(SQLiteDB_t557922535 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
