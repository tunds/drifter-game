﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_DataPair>
struct DefaultComparer_t3558413865;

#include "codegen/il2cpp-codegen.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataPair4220460133.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_DataPair>::.ctor()
extern "C"  void DefaultComparer__ctor_m3767862315_gshared (DefaultComparer_t3558413865 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3767862315(__this, method) ((  void (*) (DefaultComparer_t3558413865 *, const MethodInfo*))DefaultComparer__ctor_m3767862315_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<SQLiteDatabase.SQLiteDB/DB_DataPair>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3206024676_gshared (DefaultComparer_t3558413865 * __this, DB_DataPair_t4220460133  ___x0, DB_DataPair_t4220460133  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3206024676(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3558413865 *, DB_DataPair_t4220460133 , DB_DataPair_t4220460133 , const MethodInfo*))DefaultComparer_Compare_m3206024676_gshared)(__this, ___x0, ___y1, method)
