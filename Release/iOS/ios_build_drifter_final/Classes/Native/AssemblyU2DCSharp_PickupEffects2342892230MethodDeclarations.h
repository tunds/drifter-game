﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickupEffects
struct PickupEffects_t2342892230;
// UnityEngine.Collider
struct Collider_t955670625;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"

// System.Void PickupEffects::.ctor()
extern "C"  void PickupEffects__ctor_m2787798373 (PickupEffects_t2342892230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupEffects::Start()
extern "C"  void PickupEffects_Start_m1734936165 (PickupEffects_t2342892230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickupEffects::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void PickupEffects_OnTriggerEnter_m820270131 (PickupEffects_t2342892230 * __this, Collider_t955670625 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
