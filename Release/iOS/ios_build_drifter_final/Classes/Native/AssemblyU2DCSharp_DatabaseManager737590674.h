﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SQLiteDatabase.SQLiteDB
struct SQLiteDB_t557922535;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4157799059;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DatabaseManager
struct  DatabaseManager_t737590674  : public MonoBehaviour_t3012272455
{
public:
	// SQLiteDatabase.SQLiteDB DatabaseManager::db
	SQLiteDB_t557922535 * ___db_2;
	// UnityEngine.UI.Text[] DatabaseManager::names
	TextU5BU5D_t4157799059* ___names_3;
	// UnityEngine.UI.Text[] DatabaseManager::scores
	TextU5BU5D_t4157799059* ___scores_4;

public:
	inline static int32_t get_offset_of_db_2() { return static_cast<int32_t>(offsetof(DatabaseManager_t737590674, ___db_2)); }
	inline SQLiteDB_t557922535 * get_db_2() const { return ___db_2; }
	inline SQLiteDB_t557922535 ** get_address_of_db_2() { return &___db_2; }
	inline void set_db_2(SQLiteDB_t557922535 * value)
	{
		___db_2 = value;
		Il2CppCodeGenWriteBarrier(&___db_2, value);
	}

	inline static int32_t get_offset_of_names_3() { return static_cast<int32_t>(offsetof(DatabaseManager_t737590674, ___names_3)); }
	inline TextU5BU5D_t4157799059* get_names_3() const { return ___names_3; }
	inline TextU5BU5D_t4157799059** get_address_of_names_3() { return &___names_3; }
	inline void set_names_3(TextU5BU5D_t4157799059* value)
	{
		___names_3 = value;
		Il2CppCodeGenWriteBarrier(&___names_3, value);
	}

	inline static int32_t get_offset_of_scores_4() { return static_cast<int32_t>(offsetof(DatabaseManager_t737590674, ___scores_4)); }
	inline TextU5BU5D_t4157799059* get_scores_4() const { return ___scores_4; }
	inline TextU5BU5D_t4157799059** get_address_of_scores_4() { return &___scores_4; }
	inline void set_scores_4(TextU5BU5D_t4157799059* value)
	{
		___scores_4 = value;
		Il2CppCodeGenWriteBarrier(&___scores_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
