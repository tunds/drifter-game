﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.UInt32DataContainer
struct UInt32DataContainer_t2920381806;
// System.Object
struct Il2CppObject;
// System.Data.ISafeDataRecord
struct ISafeDataRecord_t3927591524;
// System.Data.Common.DataContainer
struct DataContainer_t1942492167;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Data_System_Data_Common_DataContainer1942492167.h"

// System.Void System.Data.Common.UInt32DataContainer::.ctor()
extern "C"  void UInt32DataContainer__ctor_m1327779766 (UInt32DataContainer_t2920381806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Data.Common.UInt32DataContainer::GetValue(System.Int32)
extern "C"  Il2CppObject * UInt32DataContainer_GetValue_m2662378959 (UInt32DataContainer_t2920381806 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.UInt32DataContainer::ZeroOut(System.Int32)
extern "C"  void UInt32DataContainer_ZeroOut_m2940500075 (UInt32DataContainer_t2920381806 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.UInt32DataContainer::SetValue(System.Int32,System.Object)
extern "C"  void UInt32DataContainer_SetValue_m3807715036 (UInt32DataContainer_t2920381806 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.UInt32DataContainer::SetValueFromSafeDataRecord(System.Int32,System.Data.ISafeDataRecord,System.Int32)
extern "C"  void UInt32DataContainer_SetValueFromSafeDataRecord_m2092675575 (UInt32DataContainer_t2920381806 * __this, int32_t ___index0, Il2CppObject * ___record1, int32_t ___field2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.UInt32DataContainer::DoCopyValue(System.Data.Common.DataContainer,System.Int32,System.Int32)
extern "C"  void UInt32DataContainer_DoCopyValue_m3436959884 (UInt32DataContainer_t2920381806 * __this, DataContainer_t1942492167 * ___from0, int32_t ___from_index1, int32_t ___to_index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Data.Common.UInt32DataContainer::DoCompareValues(System.Int32,System.Int32)
extern "C"  int32_t UInt32DataContainer_DoCompareValues_m462534746 (UInt32DataContainer_t2920381806 * __this, int32_t ___index10, int32_t ___index21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.UInt32DataContainer::Resize(System.Int32)
extern "C"  void UInt32DataContainer_Resize_m3545133459 (UInt32DataContainer_t2920381806 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Data.Common.UInt32DataContainer::GetInt64(System.Int32)
extern "C"  int64_t UInt32DataContainer_GetInt64_m1190119483 (UInt32DataContainer_t2920381806 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
