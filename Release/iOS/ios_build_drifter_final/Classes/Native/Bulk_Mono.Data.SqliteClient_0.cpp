﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Text.Encoding
struct Encoding_t180559927;
// Mono.Data.SqliteClient.SqliteBusyException
struct SqliteBusyException_t975985850;
// Mono.Data.SqliteClient.SqliteCommand
struct SqliteCommand_t3207195823;
// Mono.Data.SqliteClient.SqliteConnection
struct SqliteConnection_t2830494554;
// System.Data.IDbTransaction
struct IDbTransaction_t3098575722;
// System.Object
struct Il2CppObject;
// System.Data.Common.DbConnection
struct DbConnection_t462757452;
// Mono.Data.SqliteClient.SqliteParameterCollection
struct SqliteParameterCollection_t3807043211;
// System.Data.Common.DbParameterCollection
struct DbParameterCollection_t3381130713;
// System.Data.Common.DbTransaction
struct DbTransaction_t4162579344;
// System.Data.Common.DbParameter
struct DbParameter_t3306161371;
// Mono.Data.SqliteClient.SqliteDataReader
struct SqliteDataReader_t545175945;
// System.Data.Common.DbDataReader
struct DbDataReader_t2472406139;
// System.Data.Common.DbCommand
struct DbCommand_t2323745021;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Data.DataTable
struct DataTable_t2176726999;
// System.Type
struct Type_t;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// Mono.Data.SqliteClient.SqliteExecutionException
struct SqliteExecutionException_t175718547;
// Mono.Data.SqliteClient.SqliteParameter
struct SqliteParameter_t2628453389;
// System.Array
struct Il2CppArray;
// Mono.Data.SqliteClient.SqliteSyntaxException
struct SqliteSyntaxException_t416288016;
// Mono.Data.SqliteClient.SqliteTransaction
struct SqliteTransaction_t2154866242;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "Mono_Data_SqliteClient_U3CModuleU3E86524790.h"
#include "Mono_Data_SqliteClient_U3CModuleU3E86524790MethodDeclarations.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli3489650524.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli3489650524MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Void2779279689.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqlit661380204.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_Double534516614.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_Text_Encoding180559927.h"
#include "mscorlib_System_Runtime_InteropServices_Marshal3977632096MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020MethodDeclarations.h"
#include "mscorlib_System_OutOfMemoryException2442386302MethodDeclarations.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Text_Encoding180559927MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "mscorlib_System_OutOfMemoryException2442386302.h"
#include "mscorlib_System_SByte2855346064.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqlit975985850.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqlit975985850MethodDeclarations.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqlit175718547MethodDeclarations.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli3207195823.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli3207195823MethodDeclarations.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli2830494554.h"
#include "System_Data_System_Data_Common_DbCommand2323745021MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Data_System_Data_CommandType753495736.h"
#include "System_Data_System_Data_Common_DbConnection462757452.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli3807043211.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli3807043211MethodDeclarations.h"
#include "System_Data_System_Data_Common_DbParameterCollecti3381130713.h"
#include "System_Data_System_Data_Common_DbTransaction4162579344.h"
#include "System_Data_System_Data_UpdateRowSource1763489119.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli2830494554MethodDeclarations.h"
#include "System_Data_System_Data_Common_DbParameterCollecti3381130713MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Enum2778772662MethodDeclarations.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936MethodDeclarations.h"
#include "mscorlib_System_ApplicationException3809199252MethodDeclarations.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli2628453389.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_DateTime339033936.h"
#include "System_Data_System_Data_Common_DbParameter3306161371.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli2628453389MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "mscorlib_System_Int162847414729.h"
#include "mscorlib_System_UInt16985925268.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_ApplicationException3809199252.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqlit416288016MethodDeclarations.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqlit416288016.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqlit175718547.h"
#include "mscorlib_System_Char2778706699MethodDeclarations.h"
#include "mscorlib_System_Convert1097883944MethodDeclarations.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqlit545175945.h"
#include "System_Data_System_Data_CommandBehavior326612048.h"
#include "System_Data_System_Data_Common_DbDataReader2472406139.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqlit545175945MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"
#include "System_Data_System_Data_Common_DbConnection462757452MethodDeclarations.h"
#include "System_Data_System_Data_ConnectionState270608870.h"
#include "System_System_ComponentModel_Component553679750MethodDeclarations.h"
#include "System_System_ComponentModel_Component553679750.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395MethodDeclarations.h"
#include "mscorlib_System_AppDomain1551247802MethodDeclarations.h"
#include "mscorlib_System_AppDomainSetup516720365MethodDeclarations.h"
#include "mscorlib_System_IO_Path2029632748MethodDeclarations.h"
#include "mscorlib_System_AppDomainSetup516720365.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042.h"
#include "mscorlib_System_StringComparison1653470895.h"
#include "mscorlib_System_AppDomain1551247802.h"
#include "mscorlib_System_IO_Path2029632748.h"
#include "System_Data_System_Data_IsolationLevel3729656617.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli2154866242MethodDeclarations.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqli2154866242.h"
#include "System_Data_System_Data_Common_DbCommand2323745021.h"
#include "mscorlib_System_DllNotFoundException3753030609.h"
#include "mscorlib_System_EntryPointNotFoundException140015479.h"
#include "System_Data_System_Data_Common_DbDataReader2472406139MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList2121638921MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable3875263730MethodDeclarations.h"
#include "mscorlib_System_StringComparer4058118931MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "mscorlib_System_StringComparer4058118931.h"
#include "System_Data_System_Data_Common_DbEnumerator317749042MethodDeclarations.h"
#include "System_Data_System_Data_Common_DbEnumerator317749042.h"
#include "System_Data_System_Data_DataTable2176726999.h"
#include "System_Data_System_Data_DataTable2176726999MethodDeclarations.h"
#include "System_Data_System_Data_DataColumnCollection3528392753MethodDeclarations.h"
#include "System_Data_System_Data_DataRow3654701923MethodDeclarations.h"
#include "System_Data_System_Data_DataRowCollection1405583905MethodDeclarations.h"
#include "System_Data_System_Data_DataRow3654701923.h"
#include "System_Data_System_Data_DataColumnCollection3528392753.h"
#include "System_Data_System_Data_DataColumn3354469747.h"
#include "System_Data_System_Data_DataRowCollection1405583905.h"
#include "mscorlib_System_Math2778998461MethodDeclarations.h"
#include "mscorlib_System_DBNull491814586.h"
#include "mscorlib_System_DBNull491814586MethodDeclarations.h"
#include "Mono_Data_SqliteClient_Mono_Data_SqliteClient_Sqlit661380204MethodDeclarations.h"
#include "System_Data_System_Data_Common_DbParameter3306161371MethodDeclarations.h"
#include "System_Data_System_Data_DbType2586775211.h"
#include "System_Data_System_Data_ParameterDirection608485289.h"
#include "System_Data_System_Data_DataRowVersion2975473339.h"
#include "mscorlib_System_InvalidCastException922874574MethodDeclarations.h"
#include "mscorlib_System_InvalidCastException922874574.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3760259642MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3760259642.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "System_Data_System_Data_DuplicateNameException46464140MethodDeclarations.h"
#include "System_Data_System_Data_DuplicateNameException46464140.h"
#include "System_Data_System_Data_Common_DbTransaction4162579344MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.IntPtr Mono.Data.SqliteClient.Sqlite::sqlite_open(System.String,System.Int32,System.IntPtr&)
extern "C"  IntPtr_t Sqlite_sqlite_open_m1087827322 (Il2CppObject * __this /* static, unused */, String_t* ___dbname0, int32_t ___db_mode1, IntPtr_t* ___errstr2, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, intptr_t*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(char*) + sizeof(int32_t) + sizeof(intptr_t*);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite", "sqlite_open", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite_open'"));
		}
	}

	// Marshaling of parameter '___dbname0' to native representation
	char* ____dbname0_marshaled = NULL;
	____dbname0_marshaled = il2cpp_codegen_marshal_string(___dbname0);

	// Marshaling of parameter '___db_mode1' to native representation

	// Marshaling of parameter '___errstr2' to native representation
	intptr_t ____errstr2_empty = 0;
	intptr_t* ____errstr2_marshaled = &____errstr2_empty;

	// Native function invocation and marshaling of return value back from native representation
	intptr_t _return_value = _il2cpp_pinvoke_func(____dbname0_marshaled, ___db_mode1, ____errstr2_marshaled);
	IntPtr_t __return_value_unmarshaled;
	__return_value_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)_return_value));

	// Marshaling cleanup of parameter '___dbname0' native representation
	il2cpp_codegen_marshal_free(____dbname0_marshaled);
	____dbname0_marshaled = NULL;

	// Marshaling cleanup of parameter '___db_mode1' native representation

	// Marshaling of parameter '___errstr2' back from native representation
	IntPtr_t ____errstr2_result_dereferenced;
	memset(&____errstr2_result_dereferenced, 0, sizeof(____errstr2_result_dereferenced));
	IntPtr_t* ____errstr2_result = &____errstr2_result_dereferenced;
	IntPtr_t ____errstr2_result_temp;
	____errstr2_result_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)*____errstr2_marshaled));
	*____errstr2_result = ____errstr2_result_temp;
	*___errstr2 = *____errstr2_result;

	// Marshaling cleanup of parameter '___errstr2' native representation

	return __return_value_unmarshaled;
}
// System.Void Mono.Data.SqliteClient.Sqlite::sqlite_close(System.IntPtr)
extern "C"  void Sqlite_sqlite_close_m1774242346 (Il2CppObject * __this /* static, unused */, IntPtr_t ___sqlite_handle0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite", "sqlite_close", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite_close'"));
		}
	}

	// Marshaling of parameter '___sqlite_handle0' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___sqlite_handle0.get_m_value_0()));

	// Marshaling cleanup of parameter '___sqlite_handle0' native representation

}
// System.Int32 Mono.Data.SqliteClient.Sqlite::sqlite_changes(System.IntPtr)
extern "C"  int32_t Sqlite_sqlite_changes_m2880385937 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite", "sqlite_changes", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite_changes'"));
		}
	}

	// Marshaling of parameter '___handle0' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___handle0.get_m_value_0()));

	// Marshaling cleanup of parameter '___handle0' native representation

	return _return_value;
}
// System.Void Mono.Data.SqliteClient.Sqlite::sqliteFree(System.IntPtr)
extern "C"  void Sqlite_sqliteFree_m3453836343 (Il2CppObject * __this /* static, unused */, IntPtr_t ___ptr0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite", "sqliteFree", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqliteFree'"));
		}
	}

	// Marshaling of parameter '___ptr0' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___ptr0.get_m_value_0()));

	// Marshaling cleanup of parameter '___ptr0' native representation

}
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite_compile(System.IntPtr,System.IntPtr,System.IntPtr&,System.IntPtr&,System.IntPtr&)
extern "C"  int32_t Sqlite_sqlite_compile_m3875735358 (Il2CppObject * __this /* static, unused */, IntPtr_t ___sqlite_handle0, IntPtr_t ___zSql1, IntPtr_t* ___pzTail2, IntPtr_t* ___pVm3, IntPtr_t* ___errstr4, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, intptr_t*, intptr_t*, intptr_t*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(IntPtr_t) + sizeof(intptr_t*) + sizeof(intptr_t*) + sizeof(intptr_t*);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite", "sqlite_compile", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite_compile'"));
		}
	}

	// Marshaling of parameter '___sqlite_handle0' to native representation

	// Marshaling of parameter '___zSql1' to native representation

	// Marshaling of parameter '___pzTail2' to native representation
	intptr_t ____pzTail2_empty = 0;
	intptr_t* ____pzTail2_marshaled = &____pzTail2_empty;

	// Marshaling of parameter '___pVm3' to native representation
	intptr_t ____pVm3_empty = 0;
	intptr_t* ____pVm3_marshaled = &____pVm3_empty;

	// Marshaling of parameter '___errstr4' to native representation
	intptr_t ____errstr4_empty = 0;
	intptr_t* ____errstr4_marshaled = &____errstr4_empty;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___sqlite_handle0.get_m_value_0()), reinterpret_cast<intptr_t>(___zSql1.get_m_value_0()), ____pzTail2_marshaled, ____pVm3_marshaled, ____errstr4_marshaled);

	// Marshaling cleanup of parameter '___sqlite_handle0' native representation

	// Marshaling cleanup of parameter '___zSql1' native representation

	// Marshaling of parameter '___pzTail2' back from native representation
	IntPtr_t ____pzTail2_result_dereferenced;
	memset(&____pzTail2_result_dereferenced, 0, sizeof(____pzTail2_result_dereferenced));
	IntPtr_t* ____pzTail2_result = &____pzTail2_result_dereferenced;
	IntPtr_t ____pzTail2_result_temp;
	____pzTail2_result_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)*____pzTail2_marshaled));
	*____pzTail2_result = ____pzTail2_result_temp;
	*___pzTail2 = *____pzTail2_result;

	// Marshaling cleanup of parameter '___pzTail2' native representation

	// Marshaling of parameter '___pVm3' back from native representation
	IntPtr_t ____pVm3_result_dereferenced;
	memset(&____pVm3_result_dereferenced, 0, sizeof(____pVm3_result_dereferenced));
	IntPtr_t* ____pVm3_result = &____pVm3_result_dereferenced;
	IntPtr_t ____pVm3_result_temp;
	____pVm3_result_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)*____pVm3_marshaled));
	*____pVm3_result = ____pVm3_result_temp;
	*___pVm3 = *____pVm3_result;

	// Marshaling cleanup of parameter '___pVm3' native representation

	// Marshaling of parameter '___errstr4' back from native representation
	IntPtr_t ____errstr4_result_dereferenced;
	memset(&____errstr4_result_dereferenced, 0, sizeof(____errstr4_result_dereferenced));
	IntPtr_t* ____errstr4_result = &____errstr4_result_dereferenced;
	IntPtr_t ____errstr4_result_temp;
	____errstr4_result_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)*____errstr4_marshaled));
	*____errstr4_result = ____errstr4_result_temp;
	*___errstr4 = *____errstr4_result;

	// Marshaling cleanup of parameter '___errstr4' native representation

	return _return_value;
}
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite_step(System.IntPtr,System.Int32&,System.IntPtr&,System.IntPtr&)
extern "C"  int32_t Sqlite_sqlite_step_m3605130666 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t* ___pN1, IntPtr_t* ___pazValue2, IntPtr_t* ___pazColName3, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t*, intptr_t*, intptr_t*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t*) + sizeof(intptr_t*) + sizeof(intptr_t*);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite", "sqlite_step", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite_step'"));
		}
	}

	// Marshaling of parameter '___pVm0' to native representation

	// Marshaling of parameter '___pN1' to native representation
	int32_t ____pN1_empty = 0;
	int32_t* ____pN1_marshaled = &____pN1_empty;

	// Marshaling of parameter '___pazValue2' to native representation
	intptr_t ____pazValue2_empty = 0;
	intptr_t* ____pazValue2_marshaled = &____pazValue2_empty;

	// Marshaling of parameter '___pazColName3' to native representation
	intptr_t ____pazColName3_empty = 0;
	intptr_t* ____pazColName3_marshaled = &____pazColName3_empty;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pVm0.get_m_value_0()), ____pN1_marshaled, ____pazValue2_marshaled, ____pazColName3_marshaled);

	// Marshaling cleanup of parameter '___pVm0' native representation

	// Marshaling of parameter '___pN1' back from native representation
	int32_t ____pN1_result_dereferenced = 0;
	int32_t* ____pN1_result = &____pN1_result_dereferenced;
	*____pN1_result = *____pN1_marshaled;
	*___pN1 = *____pN1_result;

	// Marshaling cleanup of parameter '___pN1' native representation

	// Marshaling of parameter '___pazValue2' back from native representation
	IntPtr_t ____pazValue2_result_dereferenced;
	memset(&____pazValue2_result_dereferenced, 0, sizeof(____pazValue2_result_dereferenced));
	IntPtr_t* ____pazValue2_result = &____pazValue2_result_dereferenced;
	IntPtr_t ____pazValue2_result_temp;
	____pazValue2_result_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)*____pazValue2_marshaled));
	*____pazValue2_result = ____pazValue2_result_temp;
	*___pazValue2 = *____pazValue2_result;

	// Marshaling cleanup of parameter '___pazValue2' native representation

	// Marshaling of parameter '___pazColName3' back from native representation
	IntPtr_t ____pazColName3_result_dereferenced;
	memset(&____pazColName3_result_dereferenced, 0, sizeof(____pazColName3_result_dereferenced));
	IntPtr_t* ____pazColName3_result = &____pazColName3_result_dereferenced;
	IntPtr_t ____pazColName3_result_temp;
	____pazColName3_result_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)*____pazColName3_marshaled));
	*____pazColName3_result = ____pazColName3_result_temp;
	*___pazColName3 = *____pazColName3_result;

	// Marshaling cleanup of parameter '___pazColName3' native representation

	return _return_value;
}
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite_finalize(System.IntPtr,System.IntPtr&)
extern "C"  int32_t Sqlite_sqlite_finalize_m2315478619 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, IntPtr_t* ___pzErrMsg1, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(intptr_t*);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite", "sqlite_finalize", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite_finalize'"));
		}
	}

	// Marshaling of parameter '___pVm0' to native representation

	// Marshaling of parameter '___pzErrMsg1' to native representation
	intptr_t ____pzErrMsg1_empty = 0;
	intptr_t* ____pzErrMsg1_marshaled = &____pzErrMsg1_empty;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pVm0.get_m_value_0()), ____pzErrMsg1_marshaled);

	// Marshaling cleanup of parameter '___pVm0' native representation

	// Marshaling of parameter '___pzErrMsg1' back from native representation
	IntPtr_t ____pzErrMsg1_result_dereferenced;
	memset(&____pzErrMsg1_result_dereferenced, 0, sizeof(____pzErrMsg1_result_dereferenced));
	IntPtr_t* ____pzErrMsg1_result = &____pzErrMsg1_result_dereferenced;
	IntPtr_t ____pzErrMsg1_result_temp;
	____pzErrMsg1_result_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)*____pzErrMsg1_marshaled));
	*____pzErrMsg1_result = ____pzErrMsg1_result_temp;
	*___pzErrMsg1 = *____pzErrMsg1_result;

	// Marshaling cleanup of parameter '___pzErrMsg1' native representation

	return _return_value;
}
// System.Void Mono.Data.SqliteClient.Sqlite::sqlite_busy_timeout(System.IntPtr,System.Int32)
extern "C"  void Sqlite_sqlite_busy_timeout_m3809979918 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, int32_t ___ms1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite", "sqlite_busy_timeout", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite_busy_timeout'"));
		}
	}

	// Marshaling of parameter '___handle0' to native representation

	// Marshaling of parameter '___ms1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___handle0.get_m_value_0()), ___ms1);

	// Marshaling cleanup of parameter '___handle0' native representation

	// Marshaling cleanup of parameter '___ms1' native representation

}
// System.Int32 Mono.Data.SqliteClient.Sqlite::sqlite3_open16(System.String,System.IntPtr&)
extern "C"  int32_t Sqlite_sqlite3_open16_m3110095766 (Il2CppObject * __this /* static, unused */, String_t* ___dbname0, IntPtr_t* ___handle1, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint16_t*, intptr_t*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(char*) + sizeof(intptr_t*);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_open16", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_open16'"));
		}
	}

	// Marshaling of parameter '___dbname0' to native representation
	uint16_t* ____dbname0_marshaled = NULL;
	____dbname0_marshaled = il2cpp_codegen_marshal_wstring(___dbname0);

	// Marshaling of parameter '___handle1' to native representation
	intptr_t ____handle1_empty = 0;
	intptr_t* ____handle1_marshaled = &____handle1_empty;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____dbname0_marshaled, ____handle1_marshaled);

	// Marshaling cleanup of parameter '___dbname0' native representation
	il2cpp_codegen_marshal_free(____dbname0_marshaled);
	____dbname0_marshaled = NULL;

	// Marshaling of parameter '___handle1' back from native representation
	IntPtr_t ____handle1_result_dereferenced;
	memset(&____handle1_result_dereferenced, 0, sizeof(____handle1_result_dereferenced));
	IntPtr_t* ____handle1_result = &____handle1_result_dereferenced;
	IntPtr_t ____handle1_result_temp;
	____handle1_result_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)*____handle1_marshaled));
	*____handle1_result = ____handle1_result_temp;
	*___handle1 = *____handle1_result;

	// Marshaling cleanup of parameter '___handle1' native representation

	return _return_value;
}
// System.Void Mono.Data.SqliteClient.Sqlite::sqlite3_close(System.IntPtr)
extern "C"  void Sqlite_sqlite3_close_m3896572727 (Il2CppObject * __this /* static, unused */, IntPtr_t ___sqlite_handle0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_close", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_close'"));
		}
	}

	// Marshaling of parameter '___sqlite_handle0' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___sqlite_handle0.get_m_value_0()));

	// Marshaling cleanup of parameter '___sqlite_handle0' native representation

}
// System.IntPtr Mono.Data.SqliteClient.Sqlite::sqlite3_errmsg16(System.IntPtr)
extern "C"  IntPtr_t Sqlite_sqlite3_errmsg16_m2962662443 (Il2CppObject * __this /* static, unused */, IntPtr_t ___sqlite_handle0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_errmsg16", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_errmsg16'"));
		}
	}

	// Marshaling of parameter '___sqlite_handle0' to native representation

	// Native function invocation and marshaling of return value back from native representation
	intptr_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___sqlite_handle0.get_m_value_0()));
	IntPtr_t __return_value_unmarshaled;
	__return_value_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)_return_value));

	// Marshaling cleanup of parameter '___sqlite_handle0' native representation

	return __return_value_unmarshaled;
}
// System.Int32 Mono.Data.SqliteClient.Sqlite::sqlite3_changes(System.IntPtr)
extern "C"  int32_t Sqlite_sqlite3_changes_m4244137210 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_changes", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_changes'"));
		}
	}

	// Marshaling of parameter '___handle0' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___handle0.get_m_value_0()));

	// Marshaling cleanup of parameter '___handle0' native representation

	return _return_value;
}
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_prepare16(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr&,System.IntPtr&)
extern "C"  int32_t Sqlite_sqlite3_prepare16_m2592742857 (Il2CppObject * __this /* static, unused */, IntPtr_t ___sqlite_handle0, IntPtr_t ___zSql1, int32_t ___zSqllen2, IntPtr_t* ___pVm3, IntPtr_t* ___pzTail4, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, int32_t, intptr_t*, intptr_t*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(IntPtr_t) + sizeof(int32_t) + sizeof(intptr_t*) + sizeof(intptr_t*);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_prepare16", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_prepare16'"));
		}
	}

	// Marshaling of parameter '___sqlite_handle0' to native representation

	// Marshaling of parameter '___zSql1' to native representation

	// Marshaling of parameter '___zSqllen2' to native representation

	// Marshaling of parameter '___pVm3' to native representation
	intptr_t ____pVm3_empty = 0;
	intptr_t* ____pVm3_marshaled = &____pVm3_empty;

	// Marshaling of parameter '___pzTail4' to native representation
	intptr_t ____pzTail4_empty = 0;
	intptr_t* ____pzTail4_marshaled = &____pzTail4_empty;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___sqlite_handle0.get_m_value_0()), reinterpret_cast<intptr_t>(___zSql1.get_m_value_0()), ___zSqllen2, ____pVm3_marshaled, ____pzTail4_marshaled);

	// Marshaling cleanup of parameter '___sqlite_handle0' native representation

	// Marshaling cleanup of parameter '___zSql1' native representation

	// Marshaling cleanup of parameter '___zSqllen2' native representation

	// Marshaling of parameter '___pVm3' back from native representation
	IntPtr_t ____pVm3_result_dereferenced;
	memset(&____pVm3_result_dereferenced, 0, sizeof(____pVm3_result_dereferenced));
	IntPtr_t* ____pVm3_result = &____pVm3_result_dereferenced;
	IntPtr_t ____pVm3_result_temp;
	____pVm3_result_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)*____pVm3_marshaled));
	*____pVm3_result = ____pVm3_result_temp;
	*___pVm3 = *____pVm3_result;

	// Marshaling cleanup of parameter '___pVm3' native representation

	// Marshaling of parameter '___pzTail4' back from native representation
	IntPtr_t ____pzTail4_result_dereferenced;
	memset(&____pzTail4_result_dereferenced, 0, sizeof(____pzTail4_result_dereferenced));
	IntPtr_t* ____pzTail4_result = &____pzTail4_result_dereferenced;
	IntPtr_t ____pzTail4_result_temp;
	____pzTail4_result_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)*____pzTail4_marshaled));
	*____pzTail4_result = ____pzTail4_result_temp;
	*___pzTail4 = *____pzTail4_result;

	// Marshaling cleanup of parameter '___pzTail4' native representation

	return _return_value;
}
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_step(System.IntPtr)
extern "C"  int32_t Sqlite_sqlite3_step_m2034772046 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_step", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_step'"));
		}
	}

	// Marshaling of parameter '___pVm0' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pVm0.get_m_value_0()));

	// Marshaling cleanup of parameter '___pVm0' native representation

	return _return_value;
}
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_finalize(System.IntPtr)
extern "C"  int32_t Sqlite_sqlite3_finalize_m4286290236 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_finalize", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_finalize'"));
		}
	}

	// Marshaling of parameter '___pVm0' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pVm0.get_m_value_0()));

	// Marshaling cleanup of parameter '___pVm0' native representation

	return _return_value;
}
// System.IntPtr Mono.Data.SqliteClient.Sqlite::sqlite3_column_name16(System.IntPtr,System.Int32)
extern "C"  IntPtr_t Sqlite_sqlite3_column_name16_m3464827510 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_column_name16", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_column_name16'"));
		}
	}

	// Marshaling of parameter '___pVm0' to native representation

	// Marshaling of parameter '___col1' to native representation

	// Native function invocation and marshaling of return value back from native representation
	intptr_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pVm0.get_m_value_0()), ___col1);
	IntPtr_t __return_value_unmarshaled;
	__return_value_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)_return_value));

	// Marshaling cleanup of parameter '___pVm0' native representation

	// Marshaling cleanup of parameter '___col1' native representation

	return __return_value_unmarshaled;
}
// System.IntPtr Mono.Data.SqliteClient.Sqlite::sqlite3_column_text16(System.IntPtr,System.Int32)
extern "C"  IntPtr_t Sqlite_sqlite3_column_text16_m1213797784 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_column_text16", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_column_text16'"));
		}
	}

	// Marshaling of parameter '___pVm0' to native representation

	// Marshaling of parameter '___col1' to native representation

	// Native function invocation and marshaling of return value back from native representation
	intptr_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pVm0.get_m_value_0()), ___col1);
	IntPtr_t __return_value_unmarshaled;
	__return_value_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)_return_value));

	// Marshaling cleanup of parameter '___pVm0' native representation

	// Marshaling cleanup of parameter '___col1' native representation

	return __return_value_unmarshaled;
}
// System.IntPtr Mono.Data.SqliteClient.Sqlite::sqlite3_column_blob(System.IntPtr,System.Int32)
extern "C"  IntPtr_t Sqlite_sqlite3_column_blob_m667396899 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_column_blob", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_column_blob'"));
		}
	}

	// Marshaling of parameter '___pVm0' to native representation

	// Marshaling of parameter '___col1' to native representation

	// Native function invocation and marshaling of return value back from native representation
	intptr_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pVm0.get_m_value_0()), ___col1);
	IntPtr_t __return_value_unmarshaled;
	__return_value_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)_return_value));

	// Marshaling cleanup of parameter '___pVm0' native representation

	// Marshaling cleanup of parameter '___col1' native representation

	return __return_value_unmarshaled;
}
// System.Int32 Mono.Data.SqliteClient.Sqlite::sqlite3_column_bytes16(System.IntPtr,System.Int32)
extern "C"  int32_t Sqlite_sqlite3_column_bytes16_m516228693 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_column_bytes16", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_column_bytes16'"));
		}
	}

	// Marshaling of parameter '___pVm0' to native representation

	// Marshaling of parameter '___col1' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pVm0.get_m_value_0()), ___col1);

	// Marshaling cleanup of parameter '___pVm0' native representation

	// Marshaling cleanup of parameter '___col1' native representation

	return _return_value;
}
// System.Int32 Mono.Data.SqliteClient.Sqlite::sqlite3_column_count(System.IntPtr)
extern "C"  int32_t Sqlite_sqlite3_column_count_m1643060835 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_column_count", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_column_count'"));
		}
	}

	// Marshaling of parameter '___pVm0' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pVm0.get_m_value_0()));

	// Marshaling cleanup of parameter '___pVm0' native representation

	return _return_value;
}
// System.Int32 Mono.Data.SqliteClient.Sqlite::sqlite3_column_type(System.IntPtr,System.Int32)
extern "C"  int32_t Sqlite_sqlite3_column_type_m3242015453 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_column_type", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_column_type'"));
		}
	}

	// Marshaling of parameter '___pVm0' to native representation

	// Marshaling of parameter '___col1' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pVm0.get_m_value_0()), ___col1);

	// Marshaling cleanup of parameter '___pVm0' native representation

	// Marshaling cleanup of parameter '___col1' native representation

	return _return_value;
}
// System.Int64 Mono.Data.SqliteClient.Sqlite::sqlite3_column_int64(System.IntPtr,System.Int32)
extern "C"  int64_t Sqlite_sqlite3_column_int64_m3912717969 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_column_int64", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_column_int64'"));
		}
	}

	// Marshaling of parameter '___pVm0' to native representation

	// Marshaling of parameter '___col1' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int64_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pVm0.get_m_value_0()), ___col1);

	// Marshaling cleanup of parameter '___pVm0' native representation

	// Marshaling cleanup of parameter '___col1' native representation

	return _return_value;
}
// System.Double Mono.Data.SqliteClient.Sqlite::sqlite3_column_double(System.IntPtr,System.Int32)
extern "C"  double Sqlite_sqlite3_column_double_m1464837061 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method)
{
	typedef double (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_column_double", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_column_double'"));
		}
	}

	// Marshaling of parameter '___pVm0' to native representation

	// Marshaling of parameter '___col1' to native representation

	// Native function invocation and marshaling of return value back from native representation
	double _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pVm0.get_m_value_0()), ___col1);

	// Marshaling cleanup of parameter '___pVm0' native representation

	// Marshaling cleanup of parameter '___col1' native representation

	return _return_value;
}
// System.IntPtr Mono.Data.SqliteClient.Sqlite::sqlite3_column_decltype16(System.IntPtr,System.Int32)
extern "C"  IntPtr_t Sqlite_sqlite3_column_decltype16_m3515264015 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pVm0, int32_t ___col1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_column_decltype16", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_column_decltype16'"));
		}
	}

	// Marshaling of parameter '___pVm0' to native representation

	// Marshaling of parameter '___col1' to native representation

	// Native function invocation and marshaling of return value back from native representation
	intptr_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pVm0.get_m_value_0()), ___col1);
	IntPtr_t __return_value_unmarshaled;
	__return_value_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)_return_value));

	// Marshaling cleanup of parameter '___pVm0' native representation

	// Marshaling cleanup of parameter '___col1' native representation

	return __return_value_unmarshaled;
}
// System.Int32 Mono.Data.SqliteClient.Sqlite::sqlite3_bind_parameter_count(System.IntPtr)
extern "C"  int32_t Sqlite_sqlite3_bind_parameter_count_m847916594 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_bind_parameter_count", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_bind_parameter_count'"));
		}
	}

	// Marshaling of parameter '___pStmt0' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pStmt0.get_m_value_0()));

	// Marshaling cleanup of parameter '___pStmt0' native representation

	return _return_value;
}
// System.IntPtr Mono.Data.SqliteClient.Sqlite::sqlite3_bind_parameter_name(System.IntPtr,System.Int32)
extern "C"  IntPtr_t Sqlite_sqlite3_bind_parameter_name_m2331255648 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, int32_t ___n1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_bind_parameter_name", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_bind_parameter_name'"));
		}
	}

	// Marshaling of parameter '___pStmt0' to native representation

	// Marshaling of parameter '___n1' to native representation

	// Native function invocation and marshaling of return value back from native representation
	intptr_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pStmt0.get_m_value_0()), ___n1);
	IntPtr_t __return_value_unmarshaled;
	__return_value_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)_return_value));

	// Marshaling cleanup of parameter '___pStmt0' native representation

	// Marshaling cleanup of parameter '___n1' native representation

	return __return_value_unmarshaled;
}
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_bind_blob(System.IntPtr,System.Int32,System.Byte[],System.Int32,System.IntPtr)
extern "C"  int32_t Sqlite_sqlite3_bind_blob_m2003916280 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, int32_t ___n1, ByteU5BU5D_t58506160* ___blob2, int32_t ___length3, IntPtr_t ___freetype4, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t, uint8_t*, int32_t, intptr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t) + sizeof(void*) + sizeof(int32_t) + sizeof(IntPtr_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_bind_blob", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_bind_blob'"));
		}
	}

	// Marshaling of parameter '___pStmt0' to native representation

	// Marshaling of parameter '___n1' to native representation

	// Marshaling of parameter '___blob2' to native representation
	uint8_t* ____blob2_marshaled = NULL;
	____blob2_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___blob2);

	// Marshaling of parameter '___length3' to native representation

	// Marshaling of parameter '___freetype4' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pStmt0.get_m_value_0()), ___n1, ____blob2_marshaled, ___length3, reinterpret_cast<intptr_t>(___freetype4.get_m_value_0()));

	// Marshaling cleanup of parameter '___pStmt0' native representation

	// Marshaling cleanup of parameter '___n1' native representation

	// Marshaling cleanup of parameter '___blob2' native representation

	// Marshaling cleanup of parameter '___length3' native representation

	// Marshaling cleanup of parameter '___freetype4' native representation

	return _return_value;
}
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_bind_double(System.IntPtr,System.Int32,System.Double)
extern "C"  int32_t Sqlite_sqlite3_bind_double_m525303930 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, int32_t ___n1, double ___value2, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t, double);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t) + sizeof(double);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_bind_double", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_bind_double'"));
		}
	}

	// Marshaling of parameter '___pStmt0' to native representation

	// Marshaling of parameter '___n1' to native representation

	// Marshaling of parameter '___value2' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pStmt0.get_m_value_0()), ___n1, ___value2);

	// Marshaling cleanup of parameter '___pStmt0' native representation

	// Marshaling cleanup of parameter '___n1' native representation

	// Marshaling cleanup of parameter '___value2' native representation

	return _return_value;
}
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_bind_int(System.IntPtr,System.Int32,System.Int32)
extern "C"  int32_t Sqlite_sqlite3_bind_int_m367224749 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, int32_t ___n1, int32_t ___value2, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t) + sizeof(int32_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_bind_int", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_bind_int'"));
		}
	}

	// Marshaling of parameter '___pStmt0' to native representation

	// Marshaling of parameter '___n1' to native representation

	// Marshaling of parameter '___value2' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pStmt0.get_m_value_0()), ___n1, ___value2);

	// Marshaling cleanup of parameter '___pStmt0' native representation

	// Marshaling cleanup of parameter '___n1' native representation

	// Marshaling cleanup of parameter '___value2' native representation

	return _return_value;
}
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_bind_int64(System.IntPtr,System.Int32,System.Int64)
extern "C"  int32_t Sqlite_sqlite3_bind_int64_m3823319984 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, int32_t ___n1, int64_t ___value2, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t, int64_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t) + sizeof(int64_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_bind_int64", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_bind_int64'"));
		}
	}

	// Marshaling of parameter '___pStmt0' to native representation

	// Marshaling of parameter '___n1' to native representation

	// Marshaling of parameter '___value2' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pStmt0.get_m_value_0()), ___n1, ___value2);

	// Marshaling cleanup of parameter '___pStmt0' native representation

	// Marshaling cleanup of parameter '___n1' native representation

	// Marshaling cleanup of parameter '___value2' native representation

	return _return_value;
}
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_bind_null(System.IntPtr,System.Int32)
extern "C"  int32_t Sqlite_sqlite3_bind_null_m2331718516 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, int32_t ___n1, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_bind_null", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_bind_null'"));
		}
	}

	// Marshaling of parameter '___pStmt0' to native representation

	// Marshaling of parameter '___n1' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pStmt0.get_m_value_0()), ___n1);

	// Marshaling cleanup of parameter '___pStmt0' native representation

	// Marshaling cleanup of parameter '___n1' native representation

	return _return_value;
}
// Mono.Data.SqliteClient.SqliteError Mono.Data.SqliteClient.Sqlite::sqlite3_bind_text16(System.IntPtr,System.Int32,System.String,System.Int32,System.IntPtr)
extern "C"  int32_t Sqlite_sqlite3_bind_text16_m3978780170 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pStmt0, int32_t ___n1, String_t* ___value2, int32_t ___length3, IntPtr_t ___freetype4, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t, uint16_t*, int32_t, intptr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t) + sizeof(char*) + sizeof(int32_t) + sizeof(IntPtr_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_bind_text16", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_bind_text16'"));
		}
	}

	// Marshaling of parameter '___pStmt0' to native representation

	// Marshaling of parameter '___n1' to native representation

	// Marshaling of parameter '___value2' to native representation
	uint16_t* ____value2_marshaled = NULL;
	____value2_marshaled = il2cpp_codegen_marshal_wstring(___value2);

	// Marshaling of parameter '___length3' to native representation

	// Marshaling of parameter '___freetype4' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___pStmt0.get_m_value_0()), ___n1, ____value2_marshaled, ___length3, reinterpret_cast<intptr_t>(___freetype4.get_m_value_0()));

	// Marshaling cleanup of parameter '___pStmt0' native representation

	// Marshaling cleanup of parameter '___n1' native representation

	// Marshaling cleanup of parameter '___value2' native representation
	il2cpp_codegen_marshal_free(____value2_marshaled);
	____value2_marshaled = NULL;

	// Marshaling cleanup of parameter '___length3' native representation

	// Marshaling cleanup of parameter '___freetype4' native representation

	return _return_value;
}
// System.Void Mono.Data.SqliteClient.Sqlite::sqlite3_busy_timeout(System.IntPtr,System.Int32)
extern "C"  void Sqlite_sqlite3_busy_timeout_m207214235 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, int32_t ___ms1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(int32_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("sqlite3", "sqlite3_busy_timeout", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'sqlite3_busy_timeout'"));
		}
	}

	// Marshaling of parameter '___handle0' to native representation

	// Marshaling of parameter '___ms1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___handle0.get_m_value_0()), ___ms1);

	// Marshaling cleanup of parameter '___handle0' native representation

	// Marshaling cleanup of parameter '___ms1' native representation

}
// System.IntPtr Mono.Data.SqliteClient.Sqlite::StringToHeap(System.String,System.Text.Encoding)
extern Il2CppClass* Marshal_t3977632096_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* OutOfMemoryException_t2442386302_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral498889709;
extern const uint32_t Sqlite_StringToHeap_m3048525239_MetadataUsageId;
extern "C"  IntPtr_t Sqlite_StringToHeap_m3048525239 (Il2CppObject * __this /* static, unused */, String_t* ___s0, Encoding_t180559927 * ___encoding1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sqlite_StringToHeap_m3048525239_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	CharU5BU5D_t3416858730* V_1 = NULL;
	ByteU5BU5D_t58506160* V_2 = NULL;
	int32_t V_3 = 0;
	IntPtr_t V_4;
	memset(&V_4, 0, sizeof(V_4));
	bool V_5 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Encoding_t180559927 * L_0 = ___encoding1;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		IntPtr_t L_2 = Marshal_StringToHGlobalAnsi_m3986935227(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_000d:
	{
		Encoding_t180559927 * L_3 = ___encoding1;
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(18 /* System.Int32 System.Text.Encoding::GetMaxByteCount(System.Int32) */, L_3, 1);
		V_0 = L_4;
		String_t* L_5 = ___s0;
		NullCheck(L_5);
		CharU5BU5D_t3416858730* L_6 = String_ToCharArray_m1208288742(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Encoding_t180559927 * L_7 = ___encoding1;
		CharU5BU5D_t3416858730* L_8 = V_1;
		NullCheck(L_7);
		int32_t L_9 = VirtFuncInvoker1< int32_t, CharU5BU5D_t3416858730* >::Invoke(7 /* System.Int32 System.Text.Encoding::GetByteCount(System.Char[]) */, L_7, L_8);
		int32_t L_10 = V_0;
		V_2 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_9+(int32_t)L_10))));
		Encoding_t180559927 * L_11 = ___encoding1;
		CharU5BU5D_t3416858730* L_12 = V_1;
		CharU5BU5D_t3416858730* L_13 = V_1;
		NullCheck(L_13);
		ByteU5BU5D_t58506160* L_14 = V_2;
		NullCheck(L_11);
		int32_t L_15 = VirtFuncInvoker5< int32_t, CharU5BU5D_t3416858730*, int32_t, int32_t, ByteU5BU5D_t58506160*, int32_t >::Invoke(8 /* System.Int32 System.Text.Encoding::GetBytes(System.Char[],System.Int32,System.Int32,System.Byte[],System.Int32) */, L_11, L_12, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))), L_14, 0);
		V_3 = L_15;
		int32_t L_16 = V_3;
		ByteU5BU5D_t58506160* L_17 = V_2;
		NullCheck(L_17);
		int32_t L_18 = V_0;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length))))-(int32_t)L_18)))))
		{
			goto IL_004f;
		}
	}
	{
		NotSupportedException_t1374155497 * L_19 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_19, _stringLiteral498889709, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_004f:
	{
		ByteU5BU5D_t58506160* L_20 = V_2;
		NullCheck(L_20);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		IntPtr_t L_21 = Marshal_AllocHGlobal_m2766529508(NULL /*static, unused*/, (((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))), /*hidden argument*/NULL);
		V_4 = L_21;
		IntPtr_t L_22 = V_4;
		IntPtr_t L_23 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_24 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0070;
		}
	}
	{
		OutOfMemoryException_t2442386302 * L_25 = (OutOfMemoryException_t2442386302 *)il2cpp_codegen_object_new(OutOfMemoryException_t2442386302_il2cpp_TypeInfo_var);
		OutOfMemoryException__ctor_m3970465100(L_25, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25);
	}

IL_0070:
	{
		V_5 = (bool)0;
	}

IL_0073:
	try
	{ // begin try (depth: 1)
		ByteU5BU5D_t58506160* L_26 = V_2;
		IntPtr_t L_27 = V_4;
		ByteU5BU5D_t58506160* L_28 = V_2;
		NullCheck(L_28);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		Marshal_Copy_m2999277726(NULL /*static, unused*/, L_26, 0, L_27, (((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))), /*hidden argument*/NULL);
		V_5 = (bool)1;
		IL2CPP_LEAVE(0x96, FINALLY_0087);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0087;
	}

FINALLY_0087:
	{ // begin finally (depth: 1)
		{
			bool L_29 = V_5;
			if (L_29)
			{
				goto IL_0095;
			}
		}

IL_008e:
		{
			IntPtr_t L_30 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
			Marshal_FreeHGlobal_m1353395547(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		}

IL_0095:
		{
			IL2CPP_END_FINALLY(135)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(135)
	{
		IL2CPP_JUMP_TBL(0x96, IL_0096)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0096:
	{
		IntPtr_t L_31 = V_4;
		return L_31;
	}
}
// System.String Mono.Data.SqliteClient.Sqlite::HeapToString(System.IntPtr,System.Text.Encoding)
extern Il2CppClass* Marshal_t3977632096_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Sqlite_HeapToString_m2398139703_MetadataUsageId;
extern "C"  String_t* Sqlite_HeapToString_m2398139703 (Il2CppObject * __this /* static, unused */, IntPtr_t ___p0, Encoding_t180559927 * ___encoding1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sqlite_HeapToString_m2398139703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	{
		Encoding_t180559927 * L_0 = ___encoding1;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		IntPtr_t L_1 = ___p0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		String_t* L_2 = Marshal_PtrToStringAnsi_m1193920512(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_000d:
	{
		IntPtr_t L_3 = ___p0;
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_5 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_001f;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_001f:
	{
		V_0 = 0;
		goto IL_002a;
	}

IL_0026:
	{
		int32_t L_6 = V_0;
		if (((int64_t)L_6 + (int64_t)1 < (int64_t)kIl2CppInt32Min) || ((int64_t)L_6 + (int64_t)1 > (int64_t)kIl2CppInt32Max))
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_002a:
	{
		IntPtr_t L_7 = ___p0;
		int32_t L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		uint8_t L_9 = Marshal_ReadByte_m3705434677(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0026;
		}
	}
	{
		IntPtr_t L_10 = ___p0;
		void* L_11 = IntPtr_op_Explicit_m2322222010(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		Encoding_t180559927 * L_13 = ___encoding1;
		String_t* L_14 = String_CreateString_m416250615(NULL, (int8_t*)(int8_t*)L_11, 0, L_12, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		String_t* L_15 = V_1;
		NullCheck(L_15);
		int32_t L_16 = String_get_Length_m2979997331(L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_0055;
	}

IL_0051:
	{
		int32_t L_17 = V_0;
		V_0 = ((int32_t)((int32_t)L_17-(int32_t)1));
	}

IL_0055:
	{
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) <= ((int32_t)0)))
		{
			goto IL_006a;
		}
	}
	{
		String_t* L_19 = V_1;
		int32_t L_20 = V_0;
		NullCheck(L_19);
		uint16_t L_21 = String_get_Chars_m3015341861(L_19, ((int32_t)((int32_t)L_20-(int32_t)1)), /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0051;
		}
	}

IL_006a:
	{
		int32_t L_22 = V_0;
		String_t* L_23 = V_1;
		NullCheck(L_23);
		int32_t L_24 = String_get_Length_m2979997331(L_23, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)L_24))))
		{
			goto IL_0078;
		}
	}
	{
		String_t* L_25 = V_1;
		return L_25;
	}

IL_0078:
	{
		String_t* L_26 = V_1;
		int32_t L_27 = V_0;
		NullCheck(L_26);
		String_t* L_28 = String_Substring_m675079568(L_26, 0, L_27, /*hidden argument*/NULL);
		return L_28;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteBusyException::.ctor()
extern Il2CppCodeGenString* _stringLiteral1694755556;
extern const uint32_t SqliteBusyException__ctor_m2561528689_MetadataUsageId;
extern "C"  void SqliteBusyException__ctor_m2561528689 (SqliteBusyException_t975985850 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteBusyException__ctor_m2561528689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SqliteBusyException__ctor_m4276661041(__this, _stringLiteral1694755556, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteBusyException::.ctor(System.String)
extern "C"  void SqliteBusyException__ctor_m4276661041 (SqliteBusyException_t975985850 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		SqliteExecutionException__ctor_m2226364346(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteCommand::.ctor(System.String,Mono.Data.SqliteClient.SqliteConnection)
extern "C"  void SqliteCommand__ctor_m4105589194 (SqliteCommand_t3207195823 * __this, String_t* ___sqlText0, SqliteConnection_t2830494554 * ___dbConn1, const MethodInfo* method)
{
	{
		__this->set__designTimeVisible_12((bool)1);
		DbCommand__ctor_m2980138567(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___sqlText0;
		__this->set_sql_6(L_0);
		SqliteConnection_t2830494554 * L_1 = ___dbConn1;
		__this->set_parent_conn_4(L_1);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteCommand::.ctor(System.String,Mono.Data.SqliteClient.SqliteConnection,System.Data.IDbTransaction)
extern "C"  void SqliteCommand__ctor_m2781458304 (SqliteCommand_t3207195823 * __this, String_t* ___sqlText0, SqliteConnection_t2830494554 * ___dbConn1, Il2CppObject * ___trans2, const MethodInfo* method)
{
	{
		__this->set__designTimeVisible_12((bool)1);
		DbCommand__ctor_m2980138567(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___sqlText0;
		__this->set_sql_6(L_0);
		SqliteConnection_t2830494554 * L_1 = ___dbConn1;
		__this->set_parent_conn_4(L_1);
		Il2CppObject * L_2 = ___trans2;
		__this->set_transaction_5(L_2);
		return;
	}
}
// System.Object Mono.Data.SqliteClient.SqliteCommand::System.ICloneable.Clone()
extern Il2CppClass* SqliteCommand_t3207195823_il2cpp_TypeInfo_var;
extern const uint32_t SqliteCommand_System_ICloneable_Clone_m622759489_MetadataUsageId;
extern "C"  Il2CppObject * SqliteCommand_System_ICloneable_Clone_m622759489 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteCommand_System_ICloneable_Clone_m622759489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_sql_6();
		SqliteConnection_t2830494554 * L_1 = __this->get_parent_conn_4();
		Il2CppObject * L_2 = __this->get_transaction_5();
		SqliteCommand_t3207195823 * L_3 = (SqliteCommand_t3207195823 *)il2cpp_codegen_object_new(SqliteCommand_t3207195823_il2cpp_TypeInfo_var);
		SqliteCommand__ctor_m2781458304(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String Mono.Data.SqliteClient.SqliteCommand::get_CommandText()
extern "C"  String_t* SqliteCommand_get_CommandText_m3094117606 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_sql_6();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteCommand::set_CommandText(System.String)
extern "C"  void SqliteCommand_set_CommandText_m3093197357 (SqliteCommand_t3207195823 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_sql_6(L_0);
		__this->set_prepared_11((bool)0);
		return;
	}
}
// System.Int32 Mono.Data.SqliteClient.SqliteCommand::get_CommandTimeout()
extern "C"  int32_t SqliteCommand_get_CommandTimeout_m3082381849 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_timeout_7();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteCommand::set_CommandTimeout(System.Int32)
extern "C"  void SqliteCommand_set_CommandTimeout_m1002795404 (SqliteCommand_t3207195823 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_timeout_7(L_0);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteCommand::set_CommandType(System.Data.CommandType)
extern "C"  void SqliteCommand_set_CommandType_m1638761424 (SqliteCommand_t3207195823 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_type_8(L_0);
		return;
	}
}
// System.Data.Common.DbConnection Mono.Data.SqliteClient.SqliteCommand::get_DbConnection()
extern "C"  DbConnection_t462757452 * SqliteCommand_get_DbConnection_m955493062 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	{
		SqliteConnection_t2830494554 * L_0 = __this->get_parent_conn_4();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteCommand::set_DbConnection(System.Data.Common.DbConnection)
extern Il2CppClass* SqliteConnection_t2830494554_il2cpp_TypeInfo_var;
extern const uint32_t SqliteCommand_set_DbConnection_m1449118263_MetadataUsageId;
extern "C"  void SqliteCommand_set_DbConnection_m1449118263 (SqliteCommand_t3207195823 * __this, DbConnection_t462757452 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteCommand_set_DbConnection_m1449118263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DbConnection_t462757452 * L_0 = ___value0;
		__this->set_parent_conn_4(((SqliteConnection_t2830494554 *)CastclassClass(L_0, SqliteConnection_t2830494554_il2cpp_TypeInfo_var)));
		return;
	}
}
// Mono.Data.SqliteClient.SqliteConnection Mono.Data.SqliteClient.SqliteCommand::get_Connection()
extern "C"  SqliteConnection_t2830494554 * SqliteCommand_get_Connection_m1408967322 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	{
		SqliteConnection_t2830494554 * L_0 = __this->get_parent_conn_4();
		return L_0;
	}
}
// Mono.Data.SqliteClient.SqliteParameterCollection Mono.Data.SqliteClient.SqliteCommand::get_Parameters()
extern Il2CppClass* SqliteParameterCollection_t3807043211_il2cpp_TypeInfo_var;
extern const uint32_t SqliteCommand_get_Parameters_m3305558975_MetadataUsageId;
extern "C"  SqliteParameterCollection_t3807043211 * SqliteCommand_get_Parameters_m3305558975 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteCommand_get_Parameters_m3305558975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SqliteParameterCollection_t3807043211 * L_0 = __this->get_sql_params_10();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		SqliteParameterCollection_t3807043211 * L_1 = (SqliteParameterCollection_t3807043211 *)il2cpp_codegen_object_new(SqliteParameterCollection_t3807043211_il2cpp_TypeInfo_var);
		SqliteParameterCollection__ctor_m2482897600(L_1, /*hidden argument*/NULL);
		__this->set_sql_params_10(L_1);
	}

IL_0016:
	{
		SqliteParameterCollection_t3807043211 * L_2 = __this->get_sql_params_10();
		return L_2;
	}
}
// System.Data.Common.DbParameterCollection Mono.Data.SqliteClient.SqliteCommand::get_DbParameterCollection()
extern "C"  DbParameterCollection_t3381130713 * SqliteCommand_get_DbParameterCollection_m4225734788 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	{
		SqliteParameterCollection_t3807043211 * L_0 = SqliteCommand_get_Parameters_m3305558975(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Data.Common.DbTransaction Mono.Data.SqliteClient.SqliteCommand::get_DbTransaction()
extern Il2CppClass* DbTransaction_t4162579344_il2cpp_TypeInfo_var;
extern const uint32_t SqliteCommand_get_DbTransaction_m3542086770_MetadataUsageId;
extern "C"  DbTransaction_t4162579344 * SqliteCommand_get_DbTransaction_m3542086770 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteCommand_get_DbTransaction_m3542086770_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_transaction_5();
		return ((DbTransaction_t4162579344 *)CastclassClass(L_0, DbTransaction_t4162579344_il2cpp_TypeInfo_var));
	}
}
// System.Void Mono.Data.SqliteClient.SqliteCommand::set_DbTransaction(System.Data.Common.DbTransaction)
extern "C"  void SqliteCommand_set_DbTransaction_m630792475 (SqliteCommand_t3207195823 * __this, DbTransaction_t4162579344 * ___value0, const MethodInfo* method)
{
	{
		DbTransaction_t4162579344 * L_0 = ___value0;
		__this->set_transaction_5(L_0);
		return;
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteCommand::get_DesignTimeVisible()
extern "C"  bool SqliteCommand_get_DesignTimeVisible_m3883816672 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__designTimeVisible_12();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteCommand::set_DesignTimeVisible(System.Boolean)
extern "C"  void SqliteCommand_set_DesignTimeVisible_m267100635 (SqliteCommand_t3207195823 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set__designTimeVisible_12(L_0);
		return;
	}
}
// System.Data.UpdateRowSource Mono.Data.SqliteClient.SqliteCommand::get_UpdatedRowSource()
extern "C"  int32_t SqliteCommand_get_UpdatedRowSource_m2793172087 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_upd_row_source_9();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteCommand::set_UpdatedRowSource(System.Data.UpdateRowSource)
extern "C"  void SqliteCommand_set_UpdatedRowSource_m2674694252 (SqliteCommand_t3207195823 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_upd_row_source_9(L_0);
		return;
	}
}
// System.Int32 Mono.Data.SqliteClient.SqliteCommand::NumChanges()
extern "C"  int32_t SqliteCommand_NumChanges_m4267508823 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	{
		SqliteConnection_t2830494554 * L_0 = __this->get_parent_conn_4();
		NullCheck(L_0);
		int32_t L_1 = SqliteConnection_get_Version_m2626162688(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0022;
		}
	}
	{
		SqliteConnection_t2830494554 * L_2 = __this->get_parent_conn_4();
		NullCheck(L_2);
		IntPtr_t L_3 = SqliteConnection_get_Handle_m3026649183(L_2, /*hidden argument*/NULL);
		int32_t L_4 = Sqlite_sqlite3_changes_m4244137210(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0022:
	{
		SqliteConnection_t2830494554 * L_5 = __this->get_parent_conn_4();
		NullCheck(L_5);
		IntPtr_t L_6 = SqliteConnection_get_Handle_m3026649183(L_5, /*hidden argument*/NULL);
		int32_t L_7 = Sqlite_sqlite_changes_m2880385937(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteCommand::BindParameters3(System.IntPtr)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* DBNull_t491814586_0_0_0_var;
extern const Il2CppType* Boolean_t211005341_0_0_0_var;
extern const Il2CppType* Byte_t2778693821_0_0_0_var;
extern const Il2CppType* Char_t2778706699_0_0_0_var;
extern const Il2CppType* Int16_t2847414729_0_0_0_var;
extern const Il2CppType* Int32_t2847414787_0_0_0_var;
extern const Il2CppType* SByte_t2855346064_0_0_0_var;
extern const Il2CppType* UInt16_t985925268_0_0_0_var;
extern const Il2CppType* DateTime_t339033936_0_0_0_var;
extern const Il2CppType* Double_t534516614_0_0_0_var;
extern const Il2CppType* Single_t958209021_0_0_0_var;
extern const Il2CppType* UInt32_t985925326_0_0_0_var;
extern const Il2CppType* Int64_t2847414882_0_0_0_var;
extern const Il2CppType* ByteU5BU5D_t58506160_0_0_0_var;
extern Il2CppClass* Encoding_t180559927_il2cpp_TypeInfo_var;
extern Il2CppClass* SqliteParameter_t2628453389_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2778772662_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t2778693821_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t2847414729_il2cpp_TypeInfo_var;
extern Il2CppClass* SByte_t2855346064_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t985925268_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t985925326_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern Il2CppClass* ApplicationException_t3809199252_il2cpp_TypeInfo_var;
extern Il2CppClass* SqliteError_t661380204_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2469021117;
extern Il2CppCodeGenString* _stringLiteral2978728532;
extern const uint32_t SqliteCommand_BindParameters3_m193939214_MetadataUsageId;
extern "C"  void SqliteCommand_BindParameters3_m193939214 (SqliteCommand_t3207195823 * __this, IntPtr_t ___pStmt0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteCommand_BindParameters3_m193939214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	SqliteParameter_t2628453389 * V_3 = NULL;
	Type_t * V_4 = NULL;
	int32_t V_5 = 0;
	String_t* V_6 = NULL;
	bool V_7 = false;
	DateTime_t339033936  V_8;
	memset(&V_8, 0, sizeof(V_8));
	int32_t G_B19_0 = 0;
	IntPtr_t G_B19_1;
	memset(&G_B19_1, 0, sizeof(G_B19_1));
	int32_t G_B18_0 = 0;
	IntPtr_t G_B18_1;
	memset(&G_B18_1, 0, sizeof(G_B18_1));
	int32_t G_B20_0 = 0;
	int32_t G_B20_1 = 0;
	IntPtr_t G_B20_2;
	memset(&G_B20_2, 0, sizeof(G_B20_2));
	{
		SqliteParameterCollection_t3807043211 * L_0 = __this->get_sql_params_10();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		SqliteParameterCollection_t3807043211 * L_1 = __this->get_sql_params_10();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(22 /* System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::get_Count() */, L_1);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		return;
	}

IL_001d:
	{
		IntPtr_t L_3 = ___pStmt0;
		int32_t L_4 = Sqlite_sqlite3_bind_parameter_count_m847916594(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		V_1 = 1;
		goto IL_03ec;
	}

IL_002b:
	{
		IntPtr_t L_5 = ___pStmt0;
		int32_t L_6 = V_1;
		IntPtr_t L_7 = Sqlite_sqlite3_bind_parameter_name_m2331255648(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_8 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_9 = Sqlite_HeapToString_m2398139703(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		V_3 = (SqliteParameter_t2628453389 *)NULL;
		String_t* L_10 = V_2;
		if (!L_10)
		{
			goto IL_005c;
		}
	}
	{
		SqliteParameterCollection_t3807043211 * L_11 = __this->get_sql_params_10();
		String_t* L_12 = V_2;
		NullCheck(L_11);
		DbParameter_t3306161371 * L_13 = DbParameterCollection_get_Item_m3865959847(L_11, L_12, /*hidden argument*/NULL);
		V_3 = ((SqliteParameter_t2628453389 *)IsInstClass(L_13, SqliteParameter_t2628453389_il2cpp_TypeInfo_var));
		goto IL_0070;
	}

IL_005c:
	{
		SqliteParameterCollection_t3807043211 * L_14 = __this->get_sql_params_10();
		int32_t L_15 = V_1;
		NullCheck(L_14);
		DbParameter_t3306161371 * L_16 = DbParameterCollection_get_Item_m1079807500(L_14, ((int32_t)((int32_t)L_15-(int32_t)1)), /*hidden argument*/NULL);
		V_3 = ((SqliteParameter_t2628453389 *)IsInstClass(L_16, SqliteParameter_t2628453389_il2cpp_TypeInfo_var));
	}

IL_0070:
	{
		SqliteParameter_t2628453389 * L_17 = V_3;
		NullCheck(L_17);
		Il2CppObject * L_18 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_17);
		if (L_18)
		{
			goto IL_0088;
		}
	}
	{
		IntPtr_t L_19 = ___pStmt0;
		int32_t L_20 = V_1;
		Sqlite_sqlite3_bind_null_m2331718516(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		goto IL_03e8;
	}

IL_0088:
	{
		SqliteParameter_t2628453389 * L_21 = V_3;
		NullCheck(L_21);
		Il2CppObject * L_22 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_21);
		NullCheck(L_22);
		Type_t * L_23 = Object_GetType_m2022236990(L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		Type_t * L_24 = V_4;
		NullCheck(L_24);
		bool L_25 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Type::get_IsEnum() */, L_24);
		if (!L_25)
		{
			goto IL_00aa;
		}
	}
	{
		Type_t * L_26 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2778772662_il2cpp_TypeInfo_var);
		Type_t * L_27 = Enum_GetUnderlyingType_m2468052512(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		V_4 = L_27;
	}

IL_00aa:
	{
		Type_t * L_28 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_29 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_28);
		bool L_30 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_28, L_29);
		if (!L_30)
		{
			goto IL_00e4;
		}
	}
	{
		SqliteParameter_t2628453389 * L_31 = V_3;
		NullCheck(L_31);
		Il2CppObject * L_32 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_31);
		V_6 = ((String_t*)CastclassSealed(L_32, String_t_il2cpp_TypeInfo_var));
		IntPtr_t L_33 = ___pStmt0;
		int32_t L_34 = V_1;
		String_t* L_35 = V_6;
		IntPtr_t L_36 = IntPtr_op_Explicit_m2174167756(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		int32_t L_37 = Sqlite_sqlite3_bind_text16_m3978780170(NULL /*static, unused*/, L_33, L_34, L_35, (-1), L_36, /*hidden argument*/NULL);
		V_5 = L_37;
		goto IL_03ca;
	}

IL_00e4:
	{
		Type_t * L_38 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_39 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DBNull_t491814586_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_38);
		bool L_40 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_38, L_39);
		if (!L_40)
		{
			goto IL_0108;
		}
	}
	{
		IntPtr_t L_41 = ___pStmt0;
		int32_t L_42 = V_1;
		int32_t L_43 = Sqlite_sqlite3_bind_null_m2331718516(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		V_5 = L_43;
		goto IL_03ca;
	}

IL_0108:
	{
		Type_t * L_44 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t211005341_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_44);
		bool L_46 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_44, L_45);
		if (!L_46)
		{
			goto IL_0147;
		}
	}
	{
		SqliteParameter_t2628453389 * L_47 = V_3;
		NullCheck(L_47);
		Il2CppObject * L_48 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_47);
		V_7 = ((*(bool*)((bool*)UnBox (L_48, Boolean_t211005341_il2cpp_TypeInfo_var))));
		IntPtr_t L_49 = ___pStmt0;
		int32_t L_50 = V_1;
		bool L_51 = V_7;
		G_B18_0 = L_50;
		G_B18_1 = L_49;
		if (!L_51)
		{
			G_B19_0 = L_50;
			G_B19_1 = L_49;
			goto IL_013a;
		}
	}
	{
		G_B20_0 = 1;
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		goto IL_013b;
	}

IL_013a:
	{
		G_B20_0 = 0;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
	}

IL_013b:
	{
		int32_t L_52 = Sqlite_sqlite3_bind_int_m367224749(NULL /*static, unused*/, G_B20_2, G_B20_1, G_B20_0, /*hidden argument*/NULL);
		V_5 = L_52;
		goto IL_03ca;
	}

IL_0147:
	{
		Type_t * L_53 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_54 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Byte_t2778693821_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_53);
		bool L_55 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_53, L_54);
		if (!L_55)
		{
			goto IL_0176;
		}
	}
	{
		IntPtr_t L_56 = ___pStmt0;
		int32_t L_57 = V_1;
		SqliteParameter_t2628453389 * L_58 = V_3;
		NullCheck(L_58);
		Il2CppObject * L_59 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_58);
		int32_t L_60 = Sqlite_sqlite3_bind_int_m367224749(NULL /*static, unused*/, L_56, L_57, ((*(uint8_t*)((uint8_t*)UnBox (L_59, Byte_t2778693821_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		V_5 = L_60;
		goto IL_03ca;
	}

IL_0176:
	{
		Type_t * L_61 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_62 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Char_t2778706699_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_61);
		bool L_63 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_61, L_62);
		if (!L_63)
		{
			goto IL_01a5;
		}
	}
	{
		IntPtr_t L_64 = ___pStmt0;
		int32_t L_65 = V_1;
		SqliteParameter_t2628453389 * L_66 = V_3;
		NullCheck(L_66);
		Il2CppObject * L_67 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_66);
		int32_t L_68 = Sqlite_sqlite3_bind_int_m367224749(NULL /*static, unused*/, L_64, L_65, ((*(uint16_t*)((uint16_t*)UnBox (L_67, Char_t2778706699_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		V_5 = L_68;
		goto IL_03ca;
	}

IL_01a5:
	{
		Type_t * L_69 = V_4;
		NullCheck(L_69);
		bool L_70 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Type::get_IsEnum() */, L_69);
		if (!L_70)
		{
			goto IL_01ca;
		}
	}
	{
		IntPtr_t L_71 = ___pStmt0;
		int32_t L_72 = V_1;
		SqliteParameter_t2628453389 * L_73 = V_3;
		NullCheck(L_73);
		Il2CppObject * L_74 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_73);
		int32_t L_75 = Sqlite_sqlite3_bind_int_m367224749(NULL /*static, unused*/, L_71, L_72, ((*(int32_t*)((int32_t*)UnBox (L_74, Int32_t2847414787_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		V_5 = L_75;
		goto IL_03ca;
	}

IL_01ca:
	{
		Type_t * L_76 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_77 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int16_t2847414729_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_76);
		bool L_78 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_76, L_77);
		if (!L_78)
		{
			goto IL_01f9;
		}
	}
	{
		IntPtr_t L_79 = ___pStmt0;
		int32_t L_80 = V_1;
		SqliteParameter_t2628453389 * L_81 = V_3;
		NullCheck(L_81);
		Il2CppObject * L_82 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_81);
		int32_t L_83 = Sqlite_sqlite3_bind_int_m367224749(NULL /*static, unused*/, L_79, L_80, ((*(int16_t*)((int16_t*)UnBox (L_82, Int16_t2847414729_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		V_5 = L_83;
		goto IL_03ca;
	}

IL_01f9:
	{
		Type_t * L_84 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_85 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t2847414787_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_84);
		bool L_86 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_84, L_85);
		if (!L_86)
		{
			goto IL_0228;
		}
	}
	{
		IntPtr_t L_87 = ___pStmt0;
		int32_t L_88 = V_1;
		SqliteParameter_t2628453389 * L_89 = V_3;
		NullCheck(L_89);
		Il2CppObject * L_90 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_89);
		int32_t L_91 = Sqlite_sqlite3_bind_int_m367224749(NULL /*static, unused*/, L_87, L_88, ((*(int32_t*)((int32_t*)UnBox (L_90, Int32_t2847414787_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		V_5 = L_91;
		goto IL_03ca;
	}

IL_0228:
	{
		Type_t * L_92 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_93 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(SByte_t2855346064_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_92);
		bool L_94 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_92, L_93);
		if (!L_94)
		{
			goto IL_0258;
		}
	}
	{
		IntPtr_t L_95 = ___pStmt0;
		int32_t L_96 = V_1;
		SqliteParameter_t2628453389 * L_97 = V_3;
		NullCheck(L_97);
		Il2CppObject * L_98 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_97);
		int32_t L_99 = Sqlite_sqlite3_bind_int_m367224749(NULL /*static, unused*/, L_95, L_96, (((int32_t)((int32_t)((*(int8_t*)((int8_t*)UnBox (L_98, SByte_t2855346064_il2cpp_TypeInfo_var))))))), /*hidden argument*/NULL);
		V_5 = L_99;
		goto IL_03ca;
	}

IL_0258:
	{
		Type_t * L_100 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_101 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt16_t985925268_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_100);
		bool L_102 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_100, L_101);
		if (!L_102)
		{
			goto IL_0287;
		}
	}
	{
		IntPtr_t L_103 = ___pStmt0;
		int32_t L_104 = V_1;
		SqliteParameter_t2628453389 * L_105 = V_3;
		NullCheck(L_105);
		Il2CppObject * L_106 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_105);
		int32_t L_107 = Sqlite_sqlite3_bind_int_m367224749(NULL /*static, unused*/, L_103, L_104, ((*(uint16_t*)((uint16_t*)UnBox (L_106, UInt16_t985925268_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		V_5 = L_107;
		goto IL_03ca;
	}

IL_0287:
	{
		Type_t * L_108 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_109 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DateTime_t339033936_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_108);
		bool L_110 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_108, L_109);
		if (!L_110)
		{
			goto IL_02bf;
		}
	}
	{
		SqliteParameter_t2628453389 * L_111 = V_3;
		NullCheck(L_111);
		Il2CppObject * L_112 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_111);
		V_8 = ((*(DateTime_t339033936 *)((DateTime_t339033936 *)UnBox (L_112, DateTime_t339033936_il2cpp_TypeInfo_var))));
		IntPtr_t L_113 = ___pStmt0;
		int32_t L_114 = V_1;
		int64_t L_115 = DateTime_ToFileTime_m3189666065((&V_8), /*hidden argument*/NULL);
		int32_t L_116 = Sqlite_sqlite3_bind_int64_m3823319984(NULL /*static, unused*/, L_113, L_114, L_115, /*hidden argument*/NULL);
		V_5 = L_116;
		goto IL_03ca;
	}

IL_02bf:
	{
		Type_t * L_117 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_118 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Double_t534516614_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_117);
		bool L_119 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_117, L_118);
		if (!L_119)
		{
			goto IL_02ee;
		}
	}
	{
		IntPtr_t L_120 = ___pStmt0;
		int32_t L_121 = V_1;
		SqliteParameter_t2628453389 * L_122 = V_3;
		NullCheck(L_122);
		Il2CppObject * L_123 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_122);
		int32_t L_124 = Sqlite_sqlite3_bind_double_m525303930(NULL /*static, unused*/, L_120, L_121, ((*(double*)((double*)UnBox (L_123, Double_t534516614_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		V_5 = L_124;
		goto IL_03ca;
	}

IL_02ee:
	{
		Type_t * L_125 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_126 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Single_t958209021_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_125);
		bool L_127 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_125, L_126);
		if (!L_127)
		{
			goto IL_031e;
		}
	}
	{
		IntPtr_t L_128 = ___pStmt0;
		int32_t L_129 = V_1;
		SqliteParameter_t2628453389 * L_130 = V_3;
		NullCheck(L_130);
		Il2CppObject * L_131 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_130);
		int32_t L_132 = Sqlite_sqlite3_bind_double_m525303930(NULL /*static, unused*/, L_128, L_129, (((double)((double)((*(float*)((float*)UnBox (L_131, Single_t958209021_il2cpp_TypeInfo_var))))))), /*hidden argument*/NULL);
		V_5 = L_132;
		goto IL_03ca;
	}

IL_031e:
	{
		Type_t * L_133 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_134 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt32_t985925326_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_133);
		bool L_135 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_133, L_134);
		if (!L_135)
		{
			goto IL_034e;
		}
	}
	{
		IntPtr_t L_136 = ___pStmt0;
		int32_t L_137 = V_1;
		SqliteParameter_t2628453389 * L_138 = V_3;
		NullCheck(L_138);
		Il2CppObject * L_139 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_138);
		int32_t L_140 = Sqlite_sqlite3_bind_int64_m3823319984(NULL /*static, unused*/, L_136, L_137, (((int64_t)((uint64_t)((*(uint32_t*)((uint32_t*)UnBox (L_139, UInt32_t985925326_il2cpp_TypeInfo_var))))))), /*hidden argument*/NULL);
		V_5 = L_140;
		goto IL_03ca;
	}

IL_034e:
	{
		Type_t * L_141 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_142 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int64_t2847414882_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_141);
		bool L_143 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_141, L_142);
		if (!L_143)
		{
			goto IL_037d;
		}
	}
	{
		IntPtr_t L_144 = ___pStmt0;
		int32_t L_145 = V_1;
		SqliteParameter_t2628453389 * L_146 = V_3;
		NullCheck(L_146);
		Il2CppObject * L_147 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_146);
		int32_t L_148 = Sqlite_sqlite3_bind_int64_m3823319984(NULL /*static, unused*/, L_144, L_145, ((*(int64_t*)((int64_t*)UnBox (L_147, Int64_t2847414882_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		V_5 = L_148;
		goto IL_03ca;
	}

IL_037d:
	{
		Type_t * L_149 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_150 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ByteU5BU5D_t58506160_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_149);
		bool L_151 = VirtFuncInvoker1< bool, Type_t * >::Invoke(37 /* System.Boolean System.Type::Equals(System.Type) */, L_149, L_150);
		if (!L_151)
		{
			goto IL_03bf;
		}
	}
	{
		IntPtr_t L_152 = ___pStmt0;
		int32_t L_153 = V_1;
		SqliteParameter_t2628453389 * L_154 = V_3;
		NullCheck(L_154);
		Il2CppObject * L_155 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_154);
		SqliteParameter_t2628453389 * L_156 = V_3;
		NullCheck(L_156);
		Il2CppObject * L_157 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_156);
		NullCheck(((ByteU5BU5D_t58506160*)Castclass(L_157, ByteU5BU5D_t58506160_il2cpp_TypeInfo_var)));
		IntPtr_t L_158 = IntPtr_op_Explicit_m2174167756(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		int32_t L_159 = Sqlite_sqlite3_bind_blob_m2003916280(NULL /*static, unused*/, L_152, L_153, ((ByteU5BU5D_t58506160*)Castclass(L_155, ByteU5BU5D_t58506160_il2cpp_TypeInfo_var)), (((int32_t)((int32_t)(((Il2CppArray *)((ByteU5BU5D_t58506160*)Castclass(L_157, ByteU5BU5D_t58506160_il2cpp_TypeInfo_var)))->max_length)))), L_158, /*hidden argument*/NULL);
		V_5 = L_159;
		goto IL_03ca;
	}

IL_03bf:
	{
		ApplicationException_t3809199252 * L_160 = (ApplicationException_t3809199252 *)il2cpp_codegen_object_new(ApplicationException_t3809199252_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m1727689164(L_160, _stringLiteral2469021117, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_160);
	}

IL_03ca:
	{
		int32_t L_161 = V_5;
		if (!L_161)
		{
			goto IL_03e8;
		}
	}
	{
		int32_t L_162 = V_5;
		int32_t L_163 = L_162;
		Il2CppObject * L_164 = Box(SqliteError_t661380204_il2cpp_TypeInfo_var, &L_163);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_165 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2978728532, L_164, /*hidden argument*/NULL);
		ApplicationException_t3809199252 * L_166 = (ApplicationException_t3809199252 *)il2cpp_codegen_object_new(ApplicationException_t3809199252_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m1727689164(L_166, L_165, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_166);
	}

IL_03e8:
	{
		int32_t L_167 = V_1;
		V_1 = ((int32_t)((int32_t)L_167+(int32_t)1));
	}

IL_03ec:
	{
		int32_t L_168 = V_1;
		int32_t L_169 = V_0;
		if ((((int32_t)L_168) <= ((int32_t)L_169)))
		{
			goto IL_002b;
		}
	}
	{
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteCommand::GetNextStatement(System.IntPtr,System.IntPtr&,System.IntPtr&)
extern Il2CppClass* SqliteSyntaxException_t416288016_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Marshal_t3977632096_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral234992306;
extern const uint32_t SqliteCommand_GetNextStatement_m3351897126_MetadataUsageId;
extern "C"  void SqliteCommand_GetNextStatement_m3351897126 (SqliteCommand_t3207195823 * __this, IntPtr_t ___pzStart0, IntPtr_t* ___pzTail1, IntPtr_t* ___pStmt2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteCommand_GetNextStatement_m3351897126_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	{
		SqliteConnection_t2830494554 * L_0 = __this->get_parent_conn_4();
		NullCheck(L_0);
		int32_t L_1 = SqliteConnection_get_Version_m2626162688(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_003d;
		}
	}
	{
		SqliteConnection_t2830494554 * L_2 = __this->get_parent_conn_4();
		NullCheck(L_2);
		IntPtr_t L_3 = SqliteConnection_get_Handle_m3026649183(L_2, /*hidden argument*/NULL);
		IntPtr_t L_4 = ___pzStart0;
		IntPtr_t* L_5 = ___pStmt2;
		IntPtr_t* L_6 = ___pzTail1;
		int32_t L_7 = Sqlite_sqlite3_prepare16_m2592742857(NULL /*static, unused*/, L_3, L_4, (-1), L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		int32_t L_8 = V_0;
		if (!L_8)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_9 = SqliteCommand_GetError3_m1160471384(__this, /*hidden argument*/NULL);
		SqliteSyntaxException_t416288016 * L_10 = (SqliteSyntaxException_t416288016 *)il2cpp_codegen_object_new(SqliteSyntaxException_t416288016_il2cpp_TypeInfo_var);
		SqliteSyntaxException__ctor_m294290695(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0038:
	{
		goto IL_0083;
	}

IL_003d:
	{
		SqliteConnection_t2830494554 * L_11 = __this->get_parent_conn_4();
		NullCheck(L_11);
		IntPtr_t L_12 = SqliteConnection_get_Handle_m3026649183(L_11, /*hidden argument*/NULL);
		IntPtr_t L_13 = ___pzStart0;
		IntPtr_t* L_14 = ___pzTail1;
		IntPtr_t* L_15 = ___pStmt2;
		int32_t L_16 = Sqlite_sqlite_compile_m3875735358(NULL /*static, unused*/, L_12, L_13, L_14, L_15, (&V_1), /*hidden argument*/NULL);
		V_2 = L_16;
		int32_t L_17 = V_2;
		if (!L_17)
		{
			goto IL_0083;
		}
	}
	{
		V_3 = _stringLiteral234992306;
		IntPtr_t L_18 = V_1;
		IntPtr_t L_19 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_20 = IntPtr_op_Inequality_m10053967(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_007c;
		}
	}
	{
		IntPtr_t L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		String_t* L_22 = Marshal_PtrToStringAnsi_m1193920512(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		IntPtr_t L_23 = V_1;
		Sqlite_sqliteFree_m3453836343(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
	}

IL_007c:
	{
		String_t* L_24 = V_3;
		SqliteSyntaxException_t416288016 * L_25 = (SqliteSyntaxException_t416288016 *)il2cpp_codegen_object_new(SqliteSyntaxException_t416288016_il2cpp_TypeInfo_var);
		SqliteSyntaxException__ctor_m294290695(L_25, L_24, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25);
	}

IL_0083:
	{
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteCommand::ExecuteStatement(System.IntPtr)
extern "C"  void SqliteCommand_ExecuteStatement_m3514726002 (SqliteCommand_t3207195823 * __this, IntPtr_t ___pStmt0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IntPtr_t L_0 = ___pStmt0;
		SqliteCommand_ExecuteStatement_m2763413839(__this, L_0, (&V_0), (&V_1), (&V_2), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteCommand::ExecuteStatement(System.IntPtr,System.Int32&,System.IntPtr&,System.IntPtr&)
extern Il2CppClass* SqliteExecutionException_t175718547_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* SqliteBusyException_t975985850_il2cpp_TypeInfo_var;
extern const uint32_t SqliteCommand_ExecuteStatement_m2763413839_MetadataUsageId;
extern "C"  bool SqliteCommand_ExecuteStatement_m2763413839 (SqliteCommand_t3207195823 * __this, IntPtr_t ___pStmt0, int32_t* ___cols1, IntPtr_t* ___pazValue2, IntPtr_t* ___pazColName3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteCommand_ExecuteStatement_m2763413839_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		SqliteConnection_t2830494554 * L_0 = __this->get_parent_conn_4();
		NullCheck(L_0);
		int32_t L_1 = SqliteConnection_get_Version_m2626162688(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0047;
		}
	}
	{
		IntPtr_t L_2 = ___pStmt0;
		int32_t L_3 = Sqlite_sqlite3_step_m2034772046(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_5 = SqliteCommand_GetError3_m1160471384(__this, /*hidden argument*/NULL);
		SqliteExecutionException_t175718547 * L_6 = (SqliteExecutionException_t175718547 *)il2cpp_codegen_object_new(SqliteExecutionException_t175718547_il2cpp_TypeInfo_var);
		SqliteExecutionException__ctor_m2226364346(L_6, L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002b:
	{
		IntPtr_t* L_7 = ___pazValue2;
		IntPtr_t L_8 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		*(L_7) = L_8;
		IntPtr_t* L_9 = ___pazColName3;
		IntPtr_t L_10 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		*(L_9) = L_10;
		int32_t* L_11 = ___cols1;
		IntPtr_t L_12 = ___pStmt0;
		int32_t L_13 = Sqlite_sqlite3_column_count_m1643060835(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		*((int32_t*)(L_11)) = (int32_t)L_13;
		goto IL_005f;
	}

IL_0047:
	{
		IntPtr_t L_14 = ___pStmt0;
		int32_t* L_15 = ___cols1;
		IntPtr_t* L_16 = ___pazValue2;
		IntPtr_t* L_17 = ___pazColName3;
		int32_t L_18 = Sqlite_sqlite_step_m3605130666(NULL /*static, unused*/, L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		int32_t L_19 = V_0;
		if ((!(((uint32_t)L_19) == ((uint32_t)1))))
		{
			goto IL_005f;
		}
	}
	{
		SqliteExecutionException_t175718547 * L_20 = (SqliteExecutionException_t175718547 *)il2cpp_codegen_object_new(SqliteExecutionException_t175718547_il2cpp_TypeInfo_var);
		SqliteExecutionException__ctor_m3454219336(L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}

IL_005f:
	{
		int32_t L_21 = V_0;
		if ((!(((uint32_t)L_21) == ((uint32_t)5))))
		{
			goto IL_006c;
		}
	}
	{
		SqliteBusyException_t975985850 * L_22 = (SqliteBusyException_t975985850 *)il2cpp_codegen_object_new(SqliteBusyException_t975985850_il2cpp_TypeInfo_var);
		SqliteBusyException__ctor_m2561528689(L_22, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_006c:
	{
		int32_t L_23 = V_0;
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)21)))))
		{
			goto IL_007a;
		}
	}
	{
		SqliteExecutionException_t175718547 * L_24 = (SqliteExecutionException_t175718547 *)il2cpp_codegen_object_new(SqliteExecutionException_t175718547_il2cpp_TypeInfo_var);
		SqliteExecutionException__ctor_m3454219336(L_24, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}

IL_007a:
	{
		int32_t L_25 = V_0;
		return (bool)((((int32_t)L_25) == ((int32_t)((int32_t)100)))? 1 : 0);
	}
}
// System.String Mono.Data.SqliteClient.SqliteCommand::BindParameters2()
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* SqliteParameter_t2628453389_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral39;
extern Il2CppCodeGenString* _stringLiteral1248;
extern const uint32_t SqliteCommand_BindParameters2_m3266475330_MetadataUsageId;
extern "C"  String_t* SqliteCommand_BindParameters2_m3266475330 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteCommand_BindParameters2_m3266475330_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	uint16_t V_1 = 0x0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	uint16_t V_4 = 0x0;
	int32_t V_5 = 0;
	String_t* V_6 = NULL;
	SqliteParameter_t2628453389 * V_7 = NULL;
	String_t* V_8 = NULL;
	{
		String_t* L_0 = __this->get_sql_6();
		V_0 = L_0;
		V_1 = 0;
		V_2 = 0;
		V_3 = 0;
		goto IL_0133;
	}

IL_0012:
	{
		String_t* L_1 = V_0;
		int32_t L_2 = V_3;
		NullCheck(L_1);
		uint16_t L_3 = String_get_Chars_m3015341861(L_1, L_2, /*hidden argument*/NULL);
		V_4 = L_3;
		uint16_t L_4 = V_4;
		uint16_t L_5 = V_1;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_002a;
		}
	}
	{
		V_1 = 0;
		goto IL_012f;
	}

IL_002a:
	{
		uint16_t L_6 = V_1;
		if (L_6)
		{
			goto IL_004a;
		}
	}
	{
		uint16_t L_7 = V_4;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)39))))
		{
			goto IL_0042;
		}
	}
	{
		uint16_t L_8 = V_4;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_004a;
		}
	}

IL_0042:
	{
		uint16_t L_9 = V_4;
		V_1 = L_9;
		goto IL_012f;
	}

IL_004a:
	{
		uint16_t L_10 = V_1;
		if (L_10)
		{
			goto IL_012f;
		}
	}
	{
		uint16_t L_11 = V_4;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)58))))
		{
			goto IL_0062;
		}
	}
	{
		uint16_t L_12 = V_4;
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)63)))))
		{
			goto IL_012f;
		}
	}

IL_0062:
	{
		int32_t L_13 = V_3;
		V_5 = L_13;
		goto IL_006a;
	}

IL_006a:
	{
		int32_t L_14 = V_3;
		int32_t L_15 = ((int32_t)((int32_t)L_14+(int32_t)1));
		V_3 = L_15;
		String_t* L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = String_get_Length_m2979997331(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_15) >= ((int32_t)L_17)))
		{
			goto IL_008b;
		}
	}
	{
		String_t* L_18 = V_0;
		int32_t L_19 = V_3;
		NullCheck(L_18);
		uint16_t L_20 = String_get_Chars_m3015341861(L_18, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2778706699_il2cpp_TypeInfo_var);
		bool L_21 = Char_IsLetterOrDigit_m2290383044(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_006a;
		}
	}

IL_008b:
	{
		String_t* L_22 = V_0;
		int32_t L_23 = V_5;
		int32_t L_24 = V_3;
		int32_t L_25 = V_5;
		NullCheck(L_22);
		String_t* L_26 = String_Substring_m675079568(L_22, L_23, ((int32_t)((int32_t)L_24-(int32_t)L_25)), /*hidden argument*/NULL);
		V_6 = L_26;
		String_t* L_27 = V_6;
		NullCheck(L_27);
		int32_t L_28 = String_get_Length_m2979997331(L_27, /*hidden argument*/NULL);
		if ((((int32_t)L_28) <= ((int32_t)1)))
		{
			goto IL_00bf;
		}
	}
	{
		SqliteParameterCollection_t3807043211 * L_29 = SqliteCommand_get_Parameters_m3305558975(__this, /*hidden argument*/NULL);
		String_t* L_30 = V_6;
		NullCheck(L_29);
		DbParameter_t3306161371 * L_31 = DbParameterCollection_get_Item_m3865959847(L_29, L_30, /*hidden argument*/NULL);
		V_7 = ((SqliteParameter_t2628453389 *)IsInstClass(L_31, SqliteParameter_t2628453389_il2cpp_TypeInfo_var));
		goto IL_00d2;
	}

IL_00bf:
	{
		SqliteParameterCollection_t3807043211 * L_32 = SqliteCommand_get_Parameters_m3305558975(__this, /*hidden argument*/NULL);
		int32_t L_33 = V_2;
		NullCheck(L_32);
		DbParameter_t3306161371 * L_34 = DbParameterCollection_get_Item_m1079807500(L_32, L_33, /*hidden argument*/NULL);
		V_7 = ((SqliteParameter_t2628453389 *)IsInstClass(L_34, SqliteParameter_t2628453389_il2cpp_TypeInfo_var));
	}

IL_00d2:
	{
		SqliteParameter_t2628453389 * L_35 = V_7;
		NullCheck(L_35);
		Il2CppObject * L_36 = VirtFuncInvoker0< Il2CppObject * >::Invoke(15 /* System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value() */, L_35);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		String_t* L_37 = Convert_ToString_m427788191(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		String_t* L_38 = String_Replace_m2915759397(L_37, _stringLiteral39, _stringLiteral1248, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral39, L_38, _stringLiteral39, /*hidden argument*/NULL);
		V_8 = L_39;
		String_t* L_40 = V_0;
		int32_t L_41 = V_5;
		String_t* L_42 = V_6;
		NullCheck(L_42);
		int32_t L_43 = String_get_Length_m2979997331(L_42, /*hidden argument*/NULL);
		NullCheck(L_40);
		String_t* L_44 = String_Remove_m242090629(L_40, L_41, L_43, /*hidden argument*/NULL);
		int32_t L_45 = V_5;
		String_t* L_46 = V_8;
		NullCheck(L_44);
		String_t* L_47 = String_Insert_m3926397187(L_44, L_45, L_46, /*hidden argument*/NULL);
		V_0 = L_47;
		int32_t L_48 = V_3;
		String_t* L_49 = V_8;
		NullCheck(L_49);
		int32_t L_50 = String_get_Length_m2979997331(L_49, /*hidden argument*/NULL);
		String_t* L_51 = V_6;
		NullCheck(L_51);
		int32_t L_52 = String_get_Length_m2979997331(L_51, /*hidden argument*/NULL);
		V_3 = ((int32_t)((int32_t)L_48+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_50-(int32_t)L_52))-(int32_t)1))));
		int32_t L_53 = V_2;
		V_2 = ((int32_t)((int32_t)L_53+(int32_t)1));
	}

IL_012f:
	{
		int32_t L_54 = V_3;
		V_3 = ((int32_t)((int32_t)L_54+(int32_t)1));
	}

IL_0133:
	{
		int32_t L_55 = V_3;
		String_t* L_56 = V_0;
		NullCheck(L_56);
		int32_t L_57 = String_get_Length_m2979997331(L_56, /*hidden argument*/NULL);
		if ((((int32_t)L_55) < ((int32_t)L_57)))
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_58 = V_0;
		return L_58;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteCommand::Prepare()
extern "C"  void SqliteCommand_Prepare_m1771868993 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_prepared_11();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		SqliteParameterCollection_t3807043211 * L_1 = SqliteCommand_get_Parameters_m3305558975(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(22 /* System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::get_Count() */, L_1);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		SqliteConnection_t2830494554 * L_3 = __this->get_parent_conn_4();
		NullCheck(L_3);
		int32_t L_4 = SqliteConnection_get_Version_m2626162688(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_003a;
		}
	}
	{
		String_t* L_5 = SqliteCommand_BindParameters2_m3266475330(__this, /*hidden argument*/NULL);
		__this->set_sql_6(L_5);
	}

IL_003a:
	{
		__this->set_prepared_11((bool)1);
		return;
	}
}
// System.Data.Common.DbParameter Mono.Data.SqliteClient.SqliteCommand::CreateDbParameter()
extern Il2CppClass* SqliteParameter_t2628453389_il2cpp_TypeInfo_var;
extern const uint32_t SqliteCommand_CreateDbParameter_m525919893_MetadataUsageId;
extern "C"  DbParameter_t3306161371 * SqliteCommand_CreateDbParameter_m525919893 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteCommand_CreateDbParameter_m525919893_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SqliteParameter_t2628453389 * L_0 = (SqliteParameter_t2628453389 *)il2cpp_codegen_object_new(SqliteParameter_t2628453389_il2cpp_TypeInfo_var);
		SqliteParameter__ctor_m1693193598(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Mono.Data.SqliteClient.SqliteCommand::ExecuteNonQuery()
extern "C"  int32_t SqliteCommand_ExecuteNonQuery_m1920021240 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SqliteCommand_ExecuteReader_m1629572846(__this, 0, (bool)0, (&V_0), /*hidden argument*/NULL);
		int32_t L_0 = V_0;
		return L_0;
	}
}
// Mono.Data.SqliteClient.SqliteDataReader Mono.Data.SqliteClient.SqliteCommand::ExecuteReader(System.Data.CommandBehavior)
extern "C"  SqliteDataReader_t545175945 * SqliteCommand_ExecuteReader_m1310918180 (SqliteCommand_t3207195823 * __this, int32_t ___behavior0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___behavior0;
		SqliteDataReader_t545175945 * L_1 = SqliteCommand_ExecuteReader_m1629572846(__this, L_0, (bool)1, (&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Data.Common.DbDataReader Mono.Data.SqliteClient.SqliteCommand::ExecuteDbDataReader(System.Data.CommandBehavior)
extern "C"  DbDataReader_t2472406139 * SqliteCommand_ExecuteDbDataReader_m3231897610 (SqliteCommand_t3207195823 * __this, int32_t ___behavior0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___behavior0;
		SqliteDataReader_t545175945 * L_1 = SqliteCommand_ExecuteReader_m1310918180(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Mono.Data.SqliteClient.SqliteDataReader Mono.Data.SqliteClient.SqliteCommand::ExecuteReader(System.Data.CommandBehavior,System.Boolean,System.Int32&)
extern Il2CppClass* Marshal_t3977632096_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* SqliteDataReader_t545175945_il2cpp_TypeInfo_var;
extern const uint32_t SqliteCommand_ExecuteReader_m1629572846_MetadataUsageId;
extern "C"  SqliteDataReader_t545175945 * SqliteCommand_ExecuteReader_m1629572846 (SqliteCommand_t3207195823 * __this, int32_t ___behavior0, bool ___want_results1, int32_t* ___rows_affected2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteCommand_ExecuteReader_m1629572846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	IntPtr_t V_3;
	memset(&V_3, 0, sizeof(V_3));
	bool V_4 = false;
	SqliteDataReader_t545175945 * V_5 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		VirtActionInvoker0::Invoke(34 /* System.Void Mono.Data.SqliteClient.SqliteCommand::Prepare() */, __this);
		SqliteConnection_t2830494554 * L_0 = __this->get_parent_conn_4();
		NullCheck(L_0);
		int32_t L_1 = SqliteConnection_get_Version_m2626162688(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_2 = __this->get_sql_6();
		NullCheck(L_2);
		String_t* L_3 = String_Trim_m1030489823(L_2, /*hidden argument*/NULL);
		SqliteConnection_t2830494554 * L_4 = __this->get_parent_conn_4();
		NullCheck(L_4);
		Encoding_t180559927 * L_5 = SqliteConnection_get_Encoding_m1059375315(L_4, /*hidden argument*/NULL);
		IntPtr_t L_6 = Sqlite_StringToHeap_m3048525239(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0049;
	}

IL_0038:
	{
		String_t* L_7 = __this->get_sql_6();
		NullCheck(L_7);
		String_t* L_8 = String_Trim_m1030489823(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		IntPtr_t L_9 = Marshal_StringToHGlobalUni_m2198927700(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
	}

IL_0049:
	{
		IntPtr_t L_10 = V_0;
		V_1 = L_10;
		SqliteConnection_t2830494554 * L_11 = __this->get_parent_conn_4();
		NullCheck(L_11);
		SqliteConnection_StartExec_m3816599250(L_11, /*hidden argument*/NULL);
		int32_t* L_12 = ___rows_affected2;
		*((int32_t*)(L_12)) = (int32_t)0;
	}

IL_0059:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_13 = V_1;
			SqliteCommand_GetNextStatement_m3351897126(__this, L_13, (&V_1), (&V_3), /*hidden argument*/NULL);
			IntPtr_t L_14 = V_3;
			IntPtr_t L_15 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_16 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
			if (!L_16)
			{
				goto IL_007a;
			}
		}

IL_0074:
		{
			Exception_t1967233988 * L_17 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
			Exception__ctor_m3223090658(L_17, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
		}

IL_007a:
		{
			IntPtr_t L_18 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
			uint8_t L_19 = Marshal_ReadByte_m238111202(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
			V_4 = (bool)((((int32_t)L_19) == ((int32_t)0))? 1 : 0);
		}

IL_0085:
		try
		{ // begin try (depth: 2)
			{
				SqliteConnection_t2830494554 * L_20 = __this->get_parent_conn_4();
				NullCheck(L_20);
				int32_t L_21 = SqliteConnection_get_Version_m2626162688(L_20, /*hidden argument*/NULL);
				if ((!(((uint32_t)L_21) == ((uint32_t)3))))
				{
					goto IL_009d;
				}
			}

IL_0096:
			{
				IntPtr_t L_22 = V_3;
				SqliteCommand_BindParameters3_m193939214(__this, L_22, /*hidden argument*/NULL);
			}

IL_009d:
			{
				bool L_23 = V_4;
				if (!L_23)
				{
					goto IL_00c3;
				}
			}

IL_00a4:
			{
				bool L_24 = ___want_results1;
				if (!L_24)
				{
					goto IL_00c3;
				}
			}

IL_00aa:
			{
				IntPtr_t L_25 = V_3;
				SqliteConnection_t2830494554 * L_26 = __this->get_parent_conn_4();
				NullCheck(L_26);
				int32_t L_27 = SqliteConnection_get_Version_m2626162688(L_26, /*hidden argument*/NULL);
				SqliteDataReader_t545175945 * L_28 = (SqliteDataReader_t545175945 *)il2cpp_codegen_object_new(SqliteDataReader_t545175945_il2cpp_TypeInfo_var);
				SqliteDataReader__ctor_m3186976856(L_28, __this, L_25, L_27, /*hidden argument*/NULL);
				V_5 = L_28;
				IL2CPP_LEAVE(0x135, FINALLY_00de);
			}

IL_00c3:
			{
				IntPtr_t L_29 = V_3;
				SqliteCommand_ExecuteStatement_m3514726002(__this, L_29, /*hidden argument*/NULL);
				bool L_30 = V_4;
				if (!L_30)
				{
					goto IL_00d9;
				}
			}

IL_00d1:
			{
				int32_t* L_31 = ___rows_affected2;
				int32_t L_32 = SqliteCommand_NumChanges_m4267508823(__this, /*hidden argument*/NULL);
				*((int32_t*)(L_31)) = (int32_t)L_32;
			}

IL_00d9:
			{
				IL2CPP_LEAVE(0x105, FINALLY_00de);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_00de;
		}

FINALLY_00de:
		{ // begin finally (depth: 2)
			{
				SqliteConnection_t2830494554 * L_33 = __this->get_parent_conn_4();
				NullCheck(L_33);
				int32_t L_34 = SqliteConnection_get_Version_m2626162688(L_33, /*hidden argument*/NULL);
				if ((!(((uint32_t)L_34) == ((uint32_t)3))))
				{
					goto IL_00fb;
				}
			}

IL_00ef:
			{
				IntPtr_t L_35 = V_3;
				Sqlite_sqlite3_finalize_m4286290236(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
				goto IL_0104;
			}

IL_00fb:
			{
				IntPtr_t L_36 = V_3;
				Sqlite_sqlite_finalize_m2315478619(NULL /*static, unused*/, L_36, (&V_2), /*hidden argument*/NULL);
			}

IL_0104:
			{
				IL2CPP_END_FINALLY(222)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(222)
		{
			IL2CPP_END_CLEANUP(0x135, FINALLY_0123);
			IL2CPP_JUMP_TBL(0x105, IL_0105)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0105:
		{
			bool L_37 = V_4;
			if (!L_37)
			{
				goto IL_0111;
			}
		}

IL_010c:
		{
			goto IL_0116;
		}

IL_0111:
		{
			goto IL_0059;
		}

IL_0116:
		{
			V_5 = (SqliteDataReader_t545175945 *)NULL;
			IL2CPP_LEAVE(0x135, FINALLY_0123);
		}

IL_011e:
		{
			; // IL_011e: leave IL_0135
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0123;
	}

FINALLY_0123:
	{ // begin finally (depth: 1)
		SqliteConnection_t2830494554 * L_38 = __this->get_parent_conn_4();
		NullCheck(L_38);
		SqliteConnection_EndExec_m1387854859(L_38, /*hidden argument*/NULL);
		IntPtr_t L_39 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m1353395547(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(291)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(291)
	{
		IL2CPP_JUMP_TBL(0x135, IL_0135)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0135:
	{
		SqliteDataReader_t545175945 * L_40 = V_5;
		return L_40;
	}
}
// System.String Mono.Data.SqliteClient.SqliteCommand::GetError3()
extern Il2CppClass* Marshal_t3977632096_il2cpp_TypeInfo_var;
extern const uint32_t SqliteCommand_GetError3_m1160471384_MetadataUsageId;
extern "C"  String_t* SqliteCommand_GetError3_m1160471384 (SqliteCommand_t3207195823 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteCommand_GetError3_m1160471384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SqliteConnection_t2830494554 * L_0 = __this->get_parent_conn_4();
		NullCheck(L_0);
		IntPtr_t L_1 = SqliteConnection_get_Handle_m3026649183(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = Sqlite_sqlite3_errmsg16_m2962662443(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		String_t* L_3 = Marshal_PtrToStringUni_m1806311251(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteConnection::.ctor()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t SqliteConnection__ctor_m4080992609_MetadataUsageId;
extern "C"  void SqliteConnection__ctor_m4080992609 (SqliteConnection_t2830494554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteConnection__ctor_m4080992609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DbConnection__ctor_m1396822102(__this, /*hidden argument*/NULL);
		__this->set_db_file_7((String_t*)NULL);
		__this->set_db_mode_8(((int32_t)644));
		__this->set_db_version_9(2);
		__this->set_state_11(0);
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_sqlite_handle_10(L_0);
		__this->set_encoding_12((Encoding_t180559927 *)NULL);
		__this->set_busy_timeout_13(0);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteConnection::.ctor(System.String)
extern "C"  void SqliteConnection__ctor_m3562382145 (SqliteConnection_t2830494554 * __this, String_t* ___connstring0, const MethodInfo* method)
{
	{
		SqliteConnection__ctor_m4080992609(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___connstring0;
		VirtActionInvoker1< String_t* >::Invoke(17 /* System.Void Mono.Data.SqliteClient.SqliteConnection::set_ConnectionString(System.String) */, __this, L_0);
		return;
	}
}
// System.Object Mono.Data.SqliteClient.SqliteConnection::System.ICloneable.Clone()
extern Il2CppClass* SqliteConnection_t2830494554_il2cpp_TypeInfo_var;
extern const uint32_t SqliteConnection_System_ICloneable_Clone_m1703326480_MetadataUsageId;
extern "C"  Il2CppObject * SqliteConnection_System_ICloneable_Clone_m1703326480 (SqliteConnection_t2830494554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteConnection_System_ICloneable_Clone_m1703326480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(16 /* System.String Mono.Data.SqliteClient.SqliteConnection::get_ConnectionString() */, __this);
		SqliteConnection_t2830494554 * L_1 = (SqliteConnection_t2830494554 *)il2cpp_codegen_object_new(SqliteConnection_t2830494554_il2cpp_TypeInfo_var);
		SqliteConnection__ctor_m3562382145(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteConnection::Dispose(System.Boolean)
extern "C"  void SqliteConnection_Dispose_m3972797013 (SqliteConnection_t2830494554 * __this, bool ___disposing0, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = ___disposing0;
			if (!L_0)
			{
				goto IL_001e;
			}
		}

IL_0006:
		{
			bool L_1 = __this->get_disposed_14();
			if (L_1)
			{
				goto IL_001e;
			}
		}

IL_0011:
		{
			VirtActionInvoker0::Invoke(20 /* System.Void Mono.Data.SqliteClient.SqliteConnection::Close() */, __this);
			__this->set_conn_str_6((String_t*)NULL);
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x32, FINALLY_0023);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		__this->set_disposed_14((bool)1);
		bool L_2 = ___disposing0;
		Component_Dispose_m2643979012(__this, L_2, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0032:
	{
		return;
	}
}
// System.String Mono.Data.SqliteClient.SqliteConnection::get_ConnectionString()
extern "C"  String_t* SqliteConnection_get_ConnectionString_m3335658776 (SqliteConnection_t2830494554 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_conn_str_6();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteConnection::set_ConnectionString(System.String)
extern "C"  void SqliteConnection_set_ConnectionString_m2467624915 (SqliteConnection_t2830494554 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		SqliteConnection_SetConnectionString_m3238925874(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Data.ConnectionState Mono.Data.SqliteClient.SqliteConnection::get_State()
extern "C"  int32_t SqliteConnection_get_State_m2282736236 (SqliteConnection_t2830494554 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_state_11();
		return L_0;
	}
}
// System.Text.Encoding Mono.Data.SqliteClient.SqliteConnection::get_Encoding()
extern "C"  Encoding_t180559927 * SqliteConnection_get_Encoding_m1059375315 (SqliteConnection_t2830494554 * __this, const MethodInfo* method)
{
	{
		Encoding_t180559927 * L_0 = __this->get_encoding_12();
		return L_0;
	}
}
// System.Int32 Mono.Data.SqliteClient.SqliteConnection::get_Version()
extern "C"  int32_t SqliteConnection_get_Version_m2626162688 (SqliteConnection_t2830494554 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_db_version_9();
		return L_0;
	}
}
// System.IntPtr Mono.Data.SqliteClient.SqliteConnection::get_Handle()
extern "C"  IntPtr_t SqliteConnection_get_Handle_m3026649183 (SqliteConnection_t2830494554 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_sqlite_handle_10();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteConnection::SetConnectionString(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* SqliteConnection_t2830494554_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t2029632748_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t180559927_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral459282826;
extern Il2CppCodeGenString* _stringLiteral3265935493;
extern Il2CppCodeGenString* _stringLiteral116076;
extern Il2CppCodeGenString* _stringLiteral3357091;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern Il2CppCodeGenString* _stringLiteral1711222099;
extern Il2CppCodeGenString* _stringLiteral236731675;
extern Il2CppCodeGenString* _stringLiteral3439929502;
extern Il2CppCodeGenString* _stringLiteral97434174;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral1345499797;
extern Il2CppCodeGenString* _stringLiteral1420935271;
extern Il2CppCodeGenString* _stringLiteral2447069043;
extern Il2CppCodeGenString* _stringLiteral1053269149;
extern const uint32_t SqliteConnection_SetConnectionString_m3238925874_MetadataUsageId;
extern "C"  void SqliteConnection_SetConnectionString_m3238925874 (SqliteConnection_t2830494554 * __this, String_t* ___connstring0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteConnection_SetConnectionString_m3238925874_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	StringU5BU5D_t2956870243* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	AppDomainSetup_t516720365 * V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	Dictionary_2_t190145395 * V_10 = NULL;
	int32_t V_11 = 0;
	{
		String_t* L_0 = ___connstring0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		VirtActionInvoker0::Invoke(20 /* System.Void Mono.Data.SqliteClient.SqliteConnection::Close() */, __this);
		__this->set_conn_str_6((String_t*)NULL);
		return;
	}

IL_0014:
	{
		String_t* L_1 = ___connstring0;
		String_t* L_2 = __this->get_conn_str_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_02aa;
		}
	}
	{
		VirtActionInvoker0::Invoke(20 /* System.Void Mono.Data.SqliteClient.SqliteConnection::Close() */, __this);
		String_t* L_4 = ___connstring0;
		__this->set_conn_str_6(L_4);
		__this->set_db_file_7((String_t*)NULL);
		__this->set_db_mode_8(((int32_t)644));
		String_t* L_5 = ___connstring0;
		CharU5BU5D_t3416858730* L_6 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		NullCheck(L_5);
		StringU5BU5D_t2956870243* L_7 = String_Split_m290179486(L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		V_1 = 0;
		goto IL_028b;
	}

IL_005d:
	{
		StringU5BU5D_t2956870243* L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		NullCheck(((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10))));
		String_t* L_11 = String_Trim_m1030489823(((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), /*hidden argument*/NULL);
		V_2 = L_11;
		String_t* L_12 = V_2;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m2979997331(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0076;
		}
	}
	{
		goto IL_0287;
	}

IL_0076:
	{
		String_t* L_14 = V_2;
		CharU5BU5D_t3416858730* L_15 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)61));
		NullCheck(L_14);
		StringU5BU5D_t2956870243* L_16 = String_Split_m290179486(L_14, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		StringU5BU5D_t2956870243* L_17 = V_3;
		NullCheck(L_17);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length))))) == ((int32_t)2)))
		{
			goto IL_009c;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_18 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_18, _stringLiteral459282826, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}

IL_009c:
	{
		StringU5BU5D_t2956870243* L_19 = V_3;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 0);
		int32_t L_20 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_21 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(((L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20))));
		String_t* L_22 = String_ToLower_m2140020155(((L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20))), L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = String_Trim_m1030489823(L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		StringU5BU5D_t2956870243* L_24 = V_3;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 1);
		int32_t L_25 = 1;
		NullCheck(((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25))));
		String_t* L_26 = String_Trim_m1030489823(((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25))), /*hidden argument*/NULL);
		V_5 = L_26;
		StringU5BU5D_t2956870243* L_27 = V_3;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 1);
		int32_t L_28 = 1;
		CultureInfo_t3603717042 * L_29 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(((L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28))));
		String_t* L_30 = String_ToLower_m2140020155(((L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28))), L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		String_t* L_31 = String_Trim_m1030489823(L_30, /*hidden argument*/NULL);
		V_6 = L_31;
		String_t* L_32 = V_4;
		V_9 = L_32;
		String_t* L_33 = V_9;
		if (!L_33)
		{
			goto IL_0287;
		}
	}
	{
		Dictionary_2_t190145395 * L_34 = ((SqliteConnection_t2830494554_StaticFields*)SqliteConnection_t2830494554_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_15();
		if (L_34)
		{
			goto IL_0140;
		}
	}
	{
		Dictionary_2_t190145395 * L_35 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_35, 6, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_10 = L_35;
		Dictionary_2_t190145395 * L_36 = V_10;
		NullCheck(L_36);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_36, _stringLiteral3265935493, 0);
		Dictionary_2_t190145395 * L_37 = V_10;
		NullCheck(L_37);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_37, _stringLiteral116076, 0);
		Dictionary_2_t190145395 * L_38 = V_10;
		NullCheck(L_38);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_38, _stringLiteral3357091, 1);
		Dictionary_2_t190145395 * L_39 = V_10;
		NullCheck(L_39);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_39, _stringLiteral351608024, 2);
		Dictionary_2_t190145395 * L_40 = V_10;
		NullCheck(L_40);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_40, _stringLiteral1711222099, 3);
		Dictionary_2_t190145395 * L_41 = V_10;
		NullCheck(L_41);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(30 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_41, _stringLiteral236731675, 4);
		Dictionary_2_t190145395 * L_42 = V_10;
		((SqliteConnection_t2830494554_StaticFields*)SqliteConnection_t2830494554_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map0_15(L_42);
	}

IL_0140:
	{
		Dictionary_2_t190145395 * L_43 = ((SqliteConnection_t2830494554_StaticFields*)SqliteConnection_t2830494554_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_15();
		String_t* L_44 = V_9;
		NullCheck(L_43);
		bool L_45 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(35 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_43, L_44, (&V_11));
		if (!L_45)
		{
			goto IL_0287;
		}
	}
	{
		int32_t L_46 = V_11;
		if (L_46 == 0)
		{
			goto IL_0173;
		}
		if (L_46 == 1)
		{
			goto IL_023f;
		}
		if (L_46 == 2)
		{
			goto IL_0251;
		}
		if (L_46 == 3)
		{
			goto IL_0263;
		}
		if (L_46 == 4)
		{
			goto IL_0275;
		}
	}
	{
		goto IL_0287;
	}

IL_0173:
	{
		String_t* L_47 = V_6;
		NullCheck(L_47);
		bool L_48 = String_StartsWith_m1500793453(L_47, _stringLiteral3439929502, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0197;
		}
	}
	{
		String_t* L_49 = V_5;
		NullCheck(L_49);
		String_t* L_50 = String_Substring_m2809233063(L_49, 7, /*hidden argument*/NULL);
		__this->set_db_file_7(L_50);
		goto IL_023a;
	}

IL_0197:
	{
		String_t* L_51 = V_6;
		NullCheck(L_51);
		bool L_52 = String_StartsWith_m1500793453(L_51, _stringLiteral97434174, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_01bb;
		}
	}
	{
		String_t* L_53 = V_5;
		NullCheck(L_53);
		String_t* L_54 = String_Substring_m2809233063(L_53, 5, /*hidden argument*/NULL);
		__this->set_db_file_7(L_54);
		goto IL_023a;
	}

IL_01bb:
	{
		String_t* L_55 = V_6;
		NullCheck(L_55);
		bool L_56 = String_StartsWith_m1500793453(L_55, _stringLiteral47, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_01d9;
		}
	}
	{
		String_t* L_57 = V_5;
		__this->set_db_file_7(L_57);
		goto IL_023a;
	}

IL_01d9:
	{
		String_t* L_58 = V_6;
		NullCheck(L_58);
		bool L_59 = String_StartsWith_m495172832(L_58, _stringLiteral1345499797, 3, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_022f;
		}
	}
	{
		AppDomain_t1551247802 * L_60 = AppDomain_get_CurrentDomain_m3448347417(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_60);
		AppDomainSetup_t516720365 * L_61 = AppDomain_get_SetupInformation_m4007885804(L_60, /*hidden argument*/NULL);
		V_7 = L_61;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t2029632748_il2cpp_TypeInfo_var);
		uint16_t L_62 = ((Path_t2029632748_StaticFields*)Path_t2029632748_il2cpp_TypeInfo_var->static_fields)->get_DirectorySeparatorChar_2();
		uint16_t L_63 = L_62;
		Il2CppObject * L_64 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_63);
		String_t* L_65 = V_6;
		NullCheck(L_65);
		String_t* L_66 = String_Substring_m2809233063(L_65, ((int32_t)15), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_67 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral1420935271, L_64, L_66, /*hidden argument*/NULL);
		V_8 = L_67;
		AppDomainSetup_t516720365 * L_68 = V_7;
		NullCheck(L_68);
		String_t* L_69 = AppDomainSetup_get_ApplicationBase_m2453508758(L_68, /*hidden argument*/NULL);
		String_t* L_70 = V_8;
		String_t* L_71 = Path_Combine_m4122812896(NULL /*static, unused*/, L_69, L_70, /*hidden argument*/NULL);
		__this->set_db_file_7(L_71);
		goto IL_023a;
	}

IL_022f:
	{
		InvalidOperationException_t2420574324 * L_72 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_72, _stringLiteral2447069043, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_72);
	}

IL_023a:
	{
		goto IL_0287;
	}

IL_023f:
	{
		String_t* L_73 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int32_t L_74 = Convert_ToInt32_m4085381007(NULL /*static, unused*/, L_73, /*hidden argument*/NULL);
		__this->set_db_mode_8(L_74);
		goto IL_0287;
	}

IL_0251:
	{
		String_t* L_75 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int32_t L_76 = Convert_ToInt32_m4085381007(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		__this->set_db_version_9(L_76);
		goto IL_0287;
	}

IL_0263:
	{
		String_t* L_77 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_78 = Encoding_GetEncoding_m4050696948(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		__this->set_encoding_12(L_78);
		goto IL_0287;
	}

IL_0275:
	{
		String_t* L_79 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int32_t L_80 = Convert_ToInt32_m4085381007(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		__this->set_busy_timeout_13(L_80);
		goto IL_0287;
	}

IL_0287:
	{
		int32_t L_81 = V_1;
		V_1 = ((int32_t)((int32_t)L_81+(int32_t)1));
	}

IL_028b:
	{
		int32_t L_82 = V_1;
		StringU5BU5D_t2956870243* L_83 = V_0;
		NullCheck(L_83);
		if ((((int32_t)L_82) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_83)->max_length)))))))
		{
			goto IL_005d;
		}
	}
	{
		String_t* L_84 = __this->get_db_file_7();
		if (L_84)
		{
			goto IL_02aa;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_85 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_85, _stringLiteral1053269149, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_85);
	}

IL_02aa:
	{
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteConnection::StartExec()
extern "C"  void SqliteConnection_StartExec_m3816599250 (SqliteConnection_t2830494554 * __this, const MethodInfo* method)
{
	{
		__this->set_state_11(4);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteConnection::EndExec()
extern "C"  void SqliteConnection_EndExec_m1387854859 (SqliteConnection_t2830494554 * __this, const MethodInfo* method)
{
	{
		__this->set_state_11(1);
		return;
	}
}
// System.Data.Common.DbTransaction Mono.Data.SqliteClient.SqliteConnection::BeginDbTransaction(System.Data.IsolationLevel)
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* SqliteTransaction_t2154866242_il2cpp_TypeInfo_var;
extern Il2CppClass* SqliteCommand_t3207195823_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4250996947;
extern Il2CppCodeGenString* _stringLiteral63078537;
extern const uint32_t SqliteConnection_BeginDbTransaction_m673728864_MetadataUsageId;
extern "C"  DbTransaction_t4162579344 * SqliteConnection_BeginDbTransaction_m673728864 (SqliteConnection_t2830494554 * __this, int32_t ___il0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteConnection_BeginDbTransaction_m673728864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SqliteTransaction_t2154866242 * V_0 = NULL;
	SqliteCommand_t3207195823 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_state_11();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0017;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral4250996947, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		SqliteTransaction_t2154866242 * L_2 = (SqliteTransaction_t2154866242 *)il2cpp_codegen_object_new(SqliteTransaction_t2154866242_il2cpp_TypeInfo_var);
		SqliteTransaction__ctor_m4193169641(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		SqliteTransaction_t2154866242 * L_3 = V_0;
		NullCheck(L_3);
		SqliteTransaction_SetConnection_m1734324017(L_3, __this, /*hidden argument*/NULL);
		DbCommand_t2323745021 * L_4 = DbConnection_CreateCommand_m1718958439(__this, /*hidden argument*/NULL);
		V_1 = ((SqliteCommand_t3207195823 *)CastclassClass(L_4, SqliteCommand_t3207195823_il2cpp_TypeInfo_var));
		SqliteCommand_t3207195823 * L_5 = V_1;
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(18 /* System.Void Mono.Data.SqliteClient.SqliteCommand::set_CommandText(System.String) */, L_5, _stringLiteral63078537);
		SqliteCommand_t3207195823 * L_6 = V_1;
		NullCheck(L_6);
		VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 Mono.Data.SqliteClient.SqliteCommand::ExecuteNonQuery() */, L_6);
		SqliteTransaction_t2154866242 * L_7 = V_0;
		return L_7;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteConnection::Close()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t SqliteConnection_Close_m1496884855_MetadataUsageId;
extern "C"  void SqliteConnection_Close_m1496884855 (SqliteConnection_t2830494554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteConnection_Close_m1496884855_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_state_11();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		__this->set_state_11(0);
		int32_t L_1 = SqliteConnection_get_Version_m2626162688(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0030;
		}
	}
	{
		IntPtr_t L_2 = __this->get_sqlite_handle_10();
		Sqlite_sqlite3_close_m3896572727(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_003b;
	}

IL_0030:
	{
		IntPtr_t L_3 = __this->get_sqlite_handle_10();
		Sqlite_sqlite_close_m1774242346(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_003b:
	{
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_sqlite_handle_10(L_4);
		return;
	}
}
// System.Data.Common.DbCommand Mono.Data.SqliteClient.SqliteConnection::CreateDbCommand()
extern Il2CppClass* SqliteCommand_t3207195823_il2cpp_TypeInfo_var;
extern const uint32_t SqliteConnection_CreateDbCommand_m2954565122_MetadataUsageId;
extern "C"  DbCommand_t2323745021 * SqliteConnection_CreateDbCommand_m2954565122 (SqliteConnection_t2830494554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteConnection_CreateDbCommand_m2954565122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SqliteCommand_t3207195823 * L_0 = (SqliteCommand_t3207195823 *)il2cpp_codegen_object_new(SqliteCommand_t3207195823_il2cpp_TypeInfo_var);
		SqliteCommand__ctor_m4105589194(L_0, (String_t*)NULL, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteConnection::Open()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Marshal_t3977632096_il2cpp_TypeInfo_var;
extern Il2CppClass* ApplicationException_t3809199252_il2cpp_TypeInfo_var;
extern Il2CppClass* DllNotFoundException_t3753030609_il2cpp_TypeInfo_var;
extern Il2CppClass* EntryPointNotFoundException_t140015479_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3295219178;
extern const uint32_t SqliteConnection_Open_m3720361869_MetadataUsageId;
extern "C"  void SqliteConnection_Open_m3720361869 (SqliteConnection_t2830494554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteConnection_Open_m3720361869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = __this->get_conn_str_6();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral3295219178, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = __this->get_state_11();
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		return;
	}

IL_0022:
	{
		IntPtr_t L_3 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		V_0 = L_3;
		int32_t L_4 = SqliteConnection_get_Version_m2626162688(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_00ac;
		}
	}

IL_0034:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_5 = __this->get_db_file_7();
			int32_t L_6 = __this->get_db_mode_8();
			IntPtr_t L_7 = Sqlite_sqlite_open_m1087827322(NULL /*static, unused*/, L_5, L_6, (&V_0), /*hidden argument*/NULL);
			__this->set_sqlite_handle_10(L_7);
			IntPtr_t L_8 = V_0;
			IntPtr_t L_9 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_10 = IntPtr_op_Inequality_m10053967(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
			if (!L_10)
			{
				goto IL_0071;
			}
		}

IL_005d:
		{
			IntPtr_t L_11 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
			String_t* L_12 = Marshal_PtrToStringAnsi_m1193920512(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
			V_1 = L_12;
			IntPtr_t L_13 = V_0;
			Sqlite_sqliteFree_m3453836343(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
			String_t* L_14 = V_1;
			ApplicationException_t3809199252 * L_15 = (ApplicationException_t3809199252 *)il2cpp_codegen_object_new(ApplicationException_t3809199252_il2cpp_TypeInfo_var);
			ApplicationException__ctor_m1727689164(L_15, L_14, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
		}

IL_0071:
		{
			goto IL_0090;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (DllNotFoundException_t3753030609_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0076;
		if(il2cpp_codegen_class_is_assignable_from (EntryPointNotFoundException_t140015479_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0083;
		throw e;
	}

CATCH_0076:
	{ // begin catch(System.DllNotFoundException)
		__this->set_db_version_9(3);
		goto IL_0090;
	} // end catch (depth: 1)

CATCH_0083:
	{ // begin catch(System.EntryPointNotFoundException)
		__this->set_db_version_9(3);
		goto IL_0090;
	} // end catch (depth: 1)

IL_0090:
	{
		int32_t L_16 = __this->get_busy_timeout_13();
		if (!L_16)
		{
			goto IL_00ac;
		}
	}
	{
		IntPtr_t L_17 = __this->get_sqlite_handle_10();
		int32_t L_18 = __this->get_busy_timeout_13();
		Sqlite_sqlite_busy_timeout_m3809979918(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
	}

IL_00ac:
	{
		int32_t L_19 = SqliteConnection_get_Version_m2626162688(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_19) == ((uint32_t)3))))
		{
			goto IL_0108;
		}
	}
	{
		String_t* L_20 = __this->get_db_file_7();
		IntPtr_t* L_21 = __this->get_address_of_sqlite_handle_10();
		int32_t L_22 = Sqlite_sqlite3_open16_m3110095766(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		int32_t L_23 = V_2;
		if ((!(((uint32_t)L_23) == ((uint32_t)1))))
		{
			goto IL_00e7;
		}
	}
	{
		IntPtr_t L_24 = __this->get_sqlite_handle_10();
		IntPtr_t L_25 = Sqlite_sqlite3_errmsg16_m2962662443(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		String_t* L_26 = Marshal_PtrToStringUni_m1806311251(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		ApplicationException_t3809199252 * L_27 = (ApplicationException_t3809199252 *)il2cpp_codegen_object_new(ApplicationException_t3809199252_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m1727689164(L_27, L_26, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_27);
	}

IL_00e7:
	{
		int32_t L_28 = __this->get_busy_timeout_13();
		if (!L_28)
		{
			goto IL_0103;
		}
	}
	{
		IntPtr_t L_29 = __this->get_sqlite_handle_10();
		int32_t L_30 = __this->get_busy_timeout_13();
		Sqlite_sqlite3_busy_timeout_m207214235(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
	}

IL_0103:
	{
		goto IL_0108;
	}

IL_0108:
	{
		__this->set_state_11(1);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteDataReader::.ctor(Mono.Data.SqliteClient.SqliteCommand,System.IntPtr,System.Int32)
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern Il2CppClass* StringComparer_t4058118931_il2cpp_TypeInfo_var;
extern const uint32_t SqliteDataReader__ctor_m3186976856_MetadataUsageId;
extern "C"  void SqliteDataReader__ctor_m3186976856 (SqliteDataReader_t545175945 * __this, SqliteCommand_t3207195823 * ___cmd0, IntPtr_t ___pVm1, int32_t ___version2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteDataReader__ctor_m3186976856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DbDataReader__ctor_m251430663(__this, /*hidden argument*/NULL);
		SqliteCommand_t3207195823 * L_0 = ___cmd0;
		__this->set_command_1(L_0);
		ArrayList_t2121638921 * L_1 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_1, /*hidden argument*/NULL);
		__this->set_rows_2(L_1);
		Hashtable_t3875263730 * L_2 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_2, /*hidden argument*/NULL);
		__this->set_column_names_sens_4(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t4058118931_il2cpp_TypeInfo_var);
		StringComparer_t4058118931 * L_3 = StringComparer_get_InvariantCultureIgnoreCase_m3518508912(NULL /*static, unused*/, /*hidden argument*/NULL);
		Hashtable_t3875263730 * L_4 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m2297924178(L_4, L_3, /*hidden argument*/NULL);
		__this->set_column_names_insens_5(L_4);
		__this->set_closed_7((bool)0);
		__this->set_current_row_6((-1));
		__this->set_reading_8((bool)1);
		IntPtr_t L_5 = ___pVm1;
		int32_t L_6 = ___version2;
		SqliteCommand_t3207195823 * L_7 = ___cmd0;
		SqliteDataReader_ReadpVm_m1436201761(__this, L_5, L_6, L_7, /*hidden argument*/NULL);
		SqliteDataReader_ReadingDone_m2348083870(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Mono.Data.SqliteClient.SqliteDataReader::get_FieldCount()
extern "C"  int32_t SqliteDataReader_get_FieldCount_m4201696990 (SqliteDataReader_t545175945 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t2956870243* L_0 = __this->get_columns_3();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Object Mono.Data.SqliteClient.SqliteDataReader::get_Item(System.Int32)
extern "C"  Il2CppObject * SqliteDataReader_get_Item_m1103853130 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i0;
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(40 /* System.Object Mono.Data.SqliteClient.SqliteDataReader::GetValue(System.Int32) */, __this, L_0);
		return L_1;
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteDataReader::get_IsClosed()
extern "C"  bool SqliteDataReader_get_IsClosed_m3649268921 (SqliteDataReader_t545175945 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_closed_7();
		return L_0;
	}
}
// System.Int32 Mono.Data.SqliteClient.SqliteDataReader::get_RecordsAffected()
extern "C"  int32_t SqliteDataReader_get_RecordsAffected_m2985366639 (SqliteDataReader_t545175945 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_records_affected_9();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteDataReader::ReadpVm(System.IntPtr,System.Int32,Mono.Data.SqliteClient.SqliteCommand)
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t1809983122_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Marshal_t3977632096_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern Il2CppClass* ApplicationException_t3809199252_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104431;
extern Il2CppCodeGenString* _stringLiteral1958052158;
extern Il2CppCodeGenString* _stringLiteral3076014;
extern Il2CppCodeGenString* _stringLiteral1793702779;
extern Il2CppCodeGenString* _stringLiteral1984560555;
extern const uint32_t SqliteDataReader_ReadpVm_m1436201761_MetadataUsageId;
extern "C"  void SqliteDataReader_ReadpVm_m1436201761 (SqliteDataReader_t545175945 * __this, IntPtr_t ___pVm0, int32_t ___version1, SqliteCommand_t3207195823 * ___cmd2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteDataReader_ReadpVm_m1436201761_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	bool V_3 = false;
	Int32U5BU5D_t1809983122* V_4 = NULL;
	bool V_5 = false;
	int32_t V_6 = 0;
	IntPtr_t V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t V_8 = 0;
	String_t* V_9 = NULL;
	IntPtr_t V_10;
	memset(&V_10, 0, sizeof(V_10));
	ObjectU5BU5D_t11523773* V_11 = NULL;
	int32_t V_12 = 0;
	IntPtr_t V_13;
	memset(&V_13, 0, sizeof(V_13));
	int64_t V_14 = 0;
	int32_t V_15 = 0;
	IntPtr_t V_16;
	memset(&V_16, 0, sizeof(V_16));
	ByteU5BU5D_t58506160* V_17 = NULL;
	int32_t V_18 = 0;
	{
		V_3 = (bool)1;
		V_4 = (Int32U5BU5D_t1809983122*)NULL;
	}

IL_0005:
	{
		SqliteCommand_t3207195823 * L_0 = ___cmd2;
		IntPtr_t L_1 = ___pVm0;
		NullCheck(L_0);
		bool L_2 = SqliteCommand_ExecuteStatement_m2763413839(L_0, L_1, (&V_0), (&V_1), (&V_2), /*hidden argument*/NULL);
		V_5 = L_2;
		bool L_3 = V_3;
		if (!L_3)
		{
			goto IL_0187;
		}
	}
	{
		V_3 = (bool)0;
		int32_t L_4 = ___version1;
		if ((!(((uint32_t)L_4) == ((uint32_t)3))))
		{
			goto IL_00f3;
		}
	}
	{
		int32_t L_5 = V_0;
		__this->set_decltypes_10(((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)L_5)));
		int32_t L_6 = V_0;
		V_4 = ((Int32U5BU5D_t1809983122*)SZArrayNew(Int32U5BU5D_t1809983122_il2cpp_TypeInfo_var, (uint32_t)L_6));
		V_6 = 0;
		goto IL_00eb;
	}

IL_003f:
	{
		IntPtr_t L_7 = ___pVm0;
		int32_t L_8 = V_6;
		IntPtr_t L_9 = Sqlite_sqlite3_column_decltype16_m3515264015(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_7 = L_9;
		IntPtr_t L_10 = V_7;
		IntPtr_t L_11 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_12 = IntPtr_op_Inequality_m10053967(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00e5;
		}
	}
	{
		StringU5BU5D_t2956870243* L_13 = __this->get_decltypes_10();
		int32_t L_14 = V_6;
		IntPtr_t L_15 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		String_t* L_16 = Marshal_PtrToStringUni_m1806311251(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_17 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		String_t* L_18 = String_ToLower_m2140020155(L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		ArrayElementTypeCheck (L_13, L_18);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (String_t*)L_18);
		StringU5BU5D_t2956870243* L_19 = __this->get_decltypes_10();
		int32_t L_20 = V_6;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m1260523650(NULL /*static, unused*/, ((L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21))), _stringLiteral104431, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00a4;
		}
	}
	{
		StringU5BU5D_t2956870243* L_23 = __this->get_decltypes_10();
		int32_t L_24 = V_6;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_26 = String_op_Equality_m1260523650(NULL /*static, unused*/, ((L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25))), _stringLiteral1958052158, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00af;
		}
	}

IL_00a4:
	{
		Int32U5BU5D_t1809983122* L_27 = V_4;
		int32_t L_28 = V_6;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, L_28);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(L_28), (int32_t)1);
		goto IL_00e5;
	}

IL_00af:
	{
		StringU5BU5D_t2956870243* L_29 = __this->get_decltypes_10();
		int32_t L_30 = V_6;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = L_30;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_32 = String_op_Equality_m1260523650(NULL /*static, unused*/, ((L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31))), _stringLiteral3076014, /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_00df;
		}
	}
	{
		StringU5BU5D_t2956870243* L_33 = __this->get_decltypes_10();
		int32_t L_34 = V_6;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = L_34;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_36 = String_op_Equality_m1260523650(NULL /*static, unused*/, ((L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35))), _stringLiteral1793702779, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00e5;
		}
	}

IL_00df:
	{
		Int32U5BU5D_t1809983122* L_37 = V_4;
		int32_t L_38 = V_6;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(L_38), (int32_t)2);
	}

IL_00e5:
	{
		int32_t L_39 = V_6;
		V_6 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_00eb:
	{
		int32_t L_40 = V_6;
		int32_t L_41 = V_0;
		if ((((int32_t)L_40) < ((int32_t)L_41)))
		{
			goto IL_003f;
		}
	}

IL_00f3:
	{
		int32_t L_42 = V_0;
		__this->set_columns_3(((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)L_42)));
		V_8 = 0;
		goto IL_017f;
	}

IL_0107:
	{
		int32_t L_43 = ___version1;
		if ((!(((uint32_t)L_43) == ((uint32_t)2))))
		{
			goto IL_0137;
		}
	}
	{
		IntPtr_t L_44 = V_2;
		int32_t L_45 = V_8;
		int32_t L_46 = IntPtr_get_Size_m2848612188(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		IntPtr_t L_47 = Marshal_ReadIntPtr_m600185173(NULL /*static, unused*/, L_44, ((int32_t)((int32_t)L_45*(int32_t)L_46)), /*hidden argument*/NULL);
		V_10 = L_47;
		IntPtr_t L_48 = V_10;
		SqliteCommand_t3207195823 * L_49 = ___cmd2;
		NullCheck(L_49);
		SqliteConnection_t2830494554 * L_50 = SqliteCommand_get_Connection_m1408967322(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		Encoding_t180559927 * L_51 = SqliteConnection_get_Encoding_m1059375315(L_50, /*hidden argument*/NULL);
		String_t* L_52 = Sqlite_HeapToString_m2398139703(NULL /*static, unused*/, L_48, L_51, /*hidden argument*/NULL);
		V_9 = L_52;
		goto IL_0146;
	}

IL_0137:
	{
		IntPtr_t L_53 = ___pVm0;
		int32_t L_54 = V_8;
		IntPtr_t L_55 = Sqlite_sqlite3_column_name16_m3464827510(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		String_t* L_56 = Marshal_PtrToStringUni_m1806311251(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		V_9 = L_56;
	}

IL_0146:
	{
		StringU5BU5D_t2956870243* L_57 = __this->get_columns_3();
		int32_t L_58 = V_8;
		String_t* L_59 = V_9;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, L_58);
		ArrayElementTypeCheck (L_57, L_59);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(L_58), (String_t*)L_59);
		Hashtable_t3875263730 * L_60 = __this->get_column_names_sens_4();
		String_t* L_61 = V_9;
		int32_t L_62 = V_8;
		int32_t L_63 = L_62;
		Il2CppObject * L_64 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_63);
		NullCheck(L_60);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(27 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_60, L_61, L_64);
		Hashtable_t3875263730 * L_65 = __this->get_column_names_insens_5();
		String_t* L_66 = V_9;
		int32_t L_67 = V_8;
		int32_t L_68 = L_67;
		Il2CppObject * L_69 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_68);
		NullCheck(L_65);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(27 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_65, L_66, L_69);
		int32_t L_70 = V_8;
		V_8 = ((int32_t)((int32_t)L_70+(int32_t)1));
	}

IL_017f:
	{
		int32_t L_71 = V_8;
		int32_t L_72 = V_0;
		if ((((int32_t)L_71) < ((int32_t)L_72)))
		{
			goto IL_0107;
		}
	}

IL_0187:
	{
		bool L_73 = V_5;
		if (L_73)
		{
			goto IL_0193;
		}
	}
	{
		goto IL_0333;
	}

IL_0193:
	{
		int32_t L_74 = V_0;
		V_11 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)L_74));
		V_12 = 0;
		goto IL_0318;
	}

IL_01a3:
	{
		int32_t L_75 = ___version1;
		if ((!(((uint32_t)L_75) == ((uint32_t)2))))
		{
			goto IL_01d6;
		}
	}
	{
		IntPtr_t L_76 = V_1;
		int32_t L_77 = V_12;
		int32_t L_78 = IntPtr_get_Size_m2848612188(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		IntPtr_t L_79 = Marshal_ReadIntPtr_m600185173(NULL /*static, unused*/, L_76, ((int32_t)((int32_t)L_77*(int32_t)L_78)), /*hidden argument*/NULL);
		V_13 = L_79;
		ObjectU5BU5D_t11523773* L_80 = V_11;
		int32_t L_81 = V_12;
		IntPtr_t L_82 = V_13;
		SqliteCommand_t3207195823 * L_83 = ___cmd2;
		NullCheck(L_83);
		SqliteConnection_t2830494554 * L_84 = SqliteCommand_get_Connection_m1408967322(L_83, /*hidden argument*/NULL);
		NullCheck(L_84);
		Encoding_t180559927 * L_85 = SqliteConnection_get_Encoding_m1059375315(L_84, /*hidden argument*/NULL);
		String_t* L_86 = Sqlite_HeapToString_m2398139703(NULL /*static, unused*/, L_82, L_85, /*hidden argument*/NULL);
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, L_81);
		ArrayElementTypeCheck (L_80, L_86);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(L_81), (Il2CppObject *)L_86);
		goto IL_0312;
	}

IL_01d6:
	{
		IntPtr_t L_87 = ___pVm0;
		int32_t L_88 = V_12;
		int32_t L_89 = Sqlite_sqlite3_column_type_m3242015453(NULL /*static, unused*/, L_87, L_88, /*hidden argument*/NULL);
		V_18 = L_89;
		int32_t L_90 = V_18;
		if (((int32_t)((int32_t)L_90-(int32_t)1)) == 0)
		{
			goto IL_0202;
		}
		if (((int32_t)((int32_t)L_90-(int32_t)1)) == 1)
		{
			goto IL_0275;
		}
		if (((int32_t)((int32_t)L_90-(int32_t)1)) == 2)
		{
			goto IL_028c;
		}
		if (((int32_t)((int32_t)L_90-(int32_t)1)) == 3)
		{
			goto IL_02c7;
		}
		if (((int32_t)((int32_t)L_90-(int32_t)1)) == 4)
		{
			goto IL_02fc;
		}
	}
	{
		goto IL_0307;
	}

IL_0202:
	{
		IntPtr_t L_91 = ___pVm0;
		int32_t L_92 = V_12;
		int64_t L_93 = Sqlite_sqlite3_column_int64_m3912717969(NULL /*static, unused*/, L_91, L_92, /*hidden argument*/NULL);
		V_14 = L_93;
		Int32U5BU5D_t1809983122* L_94 = V_4;
		int32_t L_95 = V_12;
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, L_95);
		int32_t L_96 = L_95;
		if ((!(((uint32_t)((L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_96)))) == ((uint32_t)1))))
		{
			goto IL_0243;
		}
	}
	{
		int64_t L_97 = V_14;
		if ((((int64_t)L_97) < ((int64_t)(((int64_t)((int64_t)((int32_t)-2147483648LL)))))))
		{
			goto IL_0243;
		}
	}
	{
		int64_t L_98 = V_14;
		if ((((int64_t)L_98) > ((int64_t)(((int64_t)((int64_t)((int32_t)2147483647LL)))))))
		{
			goto IL_0243;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_99 = V_11;
		int32_t L_100 = V_12;
		int64_t L_101 = V_14;
		int32_t L_102 = (((int32_t)((int32_t)L_101)));
		Il2CppObject * L_103 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_102);
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, L_100);
		ArrayElementTypeCheck (L_99, L_103);
		(L_99)->SetAt(static_cast<il2cpp_array_size_t>(L_100), (Il2CppObject *)L_103);
		goto IL_0270;
	}

IL_0243:
	{
		Int32U5BU5D_t1809983122* L_104 = V_4;
		int32_t L_105 = V_12;
		NullCheck(L_104);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_104, L_105);
		int32_t L_106 = L_105;
		if ((!(((uint32_t)((L_104)->GetAt(static_cast<il2cpp_array_size_t>(L_106)))) == ((uint32_t)2))))
		{
			goto IL_0264;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_107 = V_11;
		int32_t L_108 = V_12;
		int64_t L_109 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_110 = DateTime_FromFileTime_m4245871382(NULL /*static, unused*/, L_109, /*hidden argument*/NULL);
		DateTime_t339033936  L_111 = L_110;
		Il2CppObject * L_112 = Box(DateTime_t339033936_il2cpp_TypeInfo_var, &L_111);
		NullCheck(L_107);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_107, L_108);
		ArrayElementTypeCheck (L_107, L_112);
		(L_107)->SetAt(static_cast<il2cpp_array_size_t>(L_108), (Il2CppObject *)L_112);
		goto IL_0270;
	}

IL_0264:
	{
		ObjectU5BU5D_t11523773* L_113 = V_11;
		int32_t L_114 = V_12;
		int64_t L_115 = V_14;
		int64_t L_116 = L_115;
		Il2CppObject * L_117 = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &L_116);
		NullCheck(L_113);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_113, L_114);
		ArrayElementTypeCheck (L_113, L_117);
		(L_113)->SetAt(static_cast<il2cpp_array_size_t>(L_114), (Il2CppObject *)L_117);
	}

IL_0270:
	{
		goto IL_0312;
	}

IL_0275:
	{
		ObjectU5BU5D_t11523773* L_118 = V_11;
		int32_t L_119 = V_12;
		IntPtr_t L_120 = ___pVm0;
		int32_t L_121 = V_12;
		double L_122 = Sqlite_sqlite3_column_double_m1464837061(NULL /*static, unused*/, L_120, L_121, /*hidden argument*/NULL);
		double L_123 = L_122;
		Il2CppObject * L_124 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_123);
		NullCheck(L_118);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_118, L_119);
		ArrayElementTypeCheck (L_118, L_124);
		(L_118)->SetAt(static_cast<il2cpp_array_size_t>(L_119), (Il2CppObject *)L_124);
		goto IL_0312;
	}

IL_028c:
	{
		ObjectU5BU5D_t11523773* L_125 = V_11;
		int32_t L_126 = V_12;
		IntPtr_t L_127 = ___pVm0;
		int32_t L_128 = V_12;
		IntPtr_t L_129 = Sqlite_sqlite3_column_text16_m1213797784(NULL /*static, unused*/, L_127, L_128, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		String_t* L_130 = Marshal_PtrToStringUni_m1806311251(NULL /*static, unused*/, L_129, /*hidden argument*/NULL);
		NullCheck(L_125);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_125, L_126);
		ArrayElementTypeCheck (L_125, L_130);
		(L_125)->SetAt(static_cast<il2cpp_array_size_t>(L_126), (Il2CppObject *)L_130);
		Int32U5BU5D_t1809983122* L_131 = V_4;
		int32_t L_132 = V_12;
		NullCheck(L_131);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_131, L_132);
		int32_t L_133 = L_132;
		if ((!(((uint32_t)((L_131)->GetAt(static_cast<il2cpp_array_size_t>(L_133)))) == ((uint32_t)2))))
		{
			goto IL_02c2;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_134 = V_11;
		int32_t L_135 = V_12;
		ObjectU5BU5D_t11523773* L_136 = V_11;
		int32_t L_137 = V_12;
		NullCheck(L_136);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_136, L_137);
		int32_t L_138 = L_137;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_139 = DateTime_Parse_m4106681438(NULL /*static, unused*/, ((String_t*)CastclassSealed(((L_136)->GetAt(static_cast<il2cpp_array_size_t>(L_138))), String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		DateTime_t339033936  L_140 = L_139;
		Il2CppObject * L_141 = Box(DateTime_t339033936_il2cpp_TypeInfo_var, &L_140);
		NullCheck(L_134);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_134, L_135);
		ArrayElementTypeCheck (L_134, L_141);
		(L_134)->SetAt(static_cast<il2cpp_array_size_t>(L_135), (Il2CppObject *)L_141);
	}

IL_02c2:
	{
		goto IL_0312;
	}

IL_02c7:
	{
		IntPtr_t L_142 = ___pVm0;
		int32_t L_143 = V_12;
		int32_t L_144 = Sqlite_sqlite3_column_bytes16_m516228693(NULL /*static, unused*/, L_142, L_143, /*hidden argument*/NULL);
		V_15 = L_144;
		IntPtr_t L_145 = ___pVm0;
		int32_t L_146 = V_12;
		IntPtr_t L_147 = Sqlite_sqlite3_column_blob_m667396899(NULL /*static, unused*/, L_145, L_146, /*hidden argument*/NULL);
		V_16 = L_147;
		int32_t L_148 = V_15;
		V_17 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)L_148));
		IntPtr_t L_149 = V_16;
		ByteU5BU5D_t58506160* L_150 = V_17;
		int32_t L_151 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		Marshal_Copy_m1690250234(NULL /*static, unused*/, L_149, L_150, 0, L_151, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_152 = V_11;
		int32_t L_153 = V_12;
		ByteU5BU5D_t58506160* L_154 = V_17;
		NullCheck(L_152);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_152, L_153);
		ArrayElementTypeCheck (L_152, L_154);
		(L_152)->SetAt(static_cast<il2cpp_array_size_t>(L_153), (Il2CppObject *)L_154);
		goto IL_0312;
	}

IL_02fc:
	{
		ObjectU5BU5D_t11523773* L_155 = V_11;
		int32_t L_156 = V_12;
		NullCheck(L_155);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_155, L_156);
		ArrayElementTypeCheck (L_155, NULL);
		(L_155)->SetAt(static_cast<il2cpp_array_size_t>(L_156), (Il2CppObject *)NULL);
		goto IL_0312;
	}

IL_0307:
	{
		ApplicationException_t3809199252 * L_157 = (ApplicationException_t3809199252 *)il2cpp_codegen_object_new(ApplicationException_t3809199252_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m1727689164(L_157, _stringLiteral1984560555, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_157);
	}

IL_0312:
	{
		int32_t L_158 = V_12;
		V_12 = ((int32_t)((int32_t)L_158+(int32_t)1));
	}

IL_0318:
	{
		int32_t L_159 = V_12;
		int32_t L_160 = V_0;
		if ((((int32_t)L_159) < ((int32_t)L_160)))
		{
			goto IL_01a3;
		}
	}
	{
		ArrayList_t2121638921 * L_161 = __this->get_rows_2();
		ObjectU5BU5D_t11523773* L_162 = V_11;
		NullCheck(L_161);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_161, (Il2CppObject *)(Il2CppObject *)L_162);
		goto IL_0005;
	}

IL_0333:
	{
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteDataReader::ReadingDone()
extern "C"  void SqliteDataReader_ReadingDone_m2348083870 (SqliteDataReader_t545175945 * __this, const MethodInfo* method)
{
	{
		SqliteCommand_t3207195823 * L_0 = __this->get_command_1();
		NullCheck(L_0);
		int32_t L_1 = SqliteCommand_NumChanges_m4267508823(L_0, /*hidden argument*/NULL);
		__this->set_records_affected_9(L_1);
		__this->set_reading_8((bool)0);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteDataReader::Close()
extern "C"  void SqliteDataReader_Close_m351493416 (SqliteDataReader_t545175945 * __this, const MethodInfo* method)
{
	{
		__this->set_closed_7((bool)1);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteDataReader::Dispose(System.Boolean)
extern "C"  void SqliteDataReader_Dispose_m2110605062 (SqliteDataReader_t545175945 * __this, bool ___disposing0, const MethodInfo* method)
{
	{
		bool L_0 = ___disposing0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		VirtActionInvoker0::Invoke(28 /* System.Void Mono.Data.SqliteClient.SqliteDataReader::Close() */, __this);
	}

IL_000c:
	{
		return;
	}
}
// System.Collections.IEnumerator Mono.Data.SqliteClient.SqliteDataReader::GetEnumerator()
extern Il2CppClass* DbEnumerator_t317749042_il2cpp_TypeInfo_var;
extern const uint32_t SqliteDataReader_GetEnumerator_m1957115766_MetadataUsageId;
extern "C"  Il2CppObject * SqliteDataReader_GetEnumerator_m1957115766 (SqliteDataReader_t545175945 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteDataReader_GetEnumerator_m1957115766_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DbEnumerator_t317749042 * L_0 = (DbEnumerator_t317749042 *)il2cpp_codegen_object_new(DbEnumerator_t317749042_il2cpp_TypeInfo_var);
		DbEnumerator__ctor_m3796638865(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Data.DataTable Mono.Data.SqliteClient.SqliteDataReader::GetSchemaTable()
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Int32_t2847414787_0_0_0_var;
extern const Il2CppType* Boolean_t211005341_0_0_0_var;
extern const Il2CppType* Type_t_0_0_0_var;
extern Il2CppClass* DataTable_t2176726999_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4182770465;
extern Il2CppCodeGenString* _stringLiteral394879515;
extern Il2CppCodeGenString* _stringLiteral4182927511;
extern Il2CppCodeGenString* _stringLiteral764714001;
extern Il2CppCodeGenString* _stringLiteral2303668829;
extern Il2CppCodeGenString* _stringLiteral4193116603;
extern Il2CppCodeGenString* _stringLiteral70918325;
extern Il2CppCodeGenString* _stringLiteral350959251;
extern Il2CppCodeGenString* _stringLiteral3341501650;
extern Il2CppCodeGenString* _stringLiteral703433117;
extern Il2CppCodeGenString* _stringLiteral2897606536;
extern Il2CppCodeGenString* _stringLiteral1853714980;
extern Il2CppCodeGenString* _stringLiteral2204418958;
extern Il2CppCodeGenString* _stringLiteral4040228075;
extern Il2CppCodeGenString* _stringLiteral509654597;
extern Il2CppCodeGenString* _stringLiteral540140418;
extern Il2CppCodeGenString* _stringLiteral1064432680;
extern Il2CppCodeGenString* _stringLiteral3032076630;
extern Il2CppCodeGenString* _stringLiteral1601200776;
extern Il2CppCodeGenString* _stringLiteral3816158100;
extern Il2CppCodeGenString* _stringLiteral2198507238;
extern Il2CppCodeGenString* _stringLiteral332510668;
extern const uint32_t SqliteDataReader_GetSchemaTable_m1411085877_MetadataUsageId;
extern "C"  DataTable_t2176726999 * SqliteDataReader_GetSchemaTable_m1411085877 (SqliteDataReader_t545175945 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteDataReader_GetSchemaTable_m1411085877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataTable_t2176726999 * V_0 = NULL;
	int32_t V_1 = 0;
	DataRow_t3654701923 * V_2 = NULL;
	{
		DataTable_t2176726999 * L_0 = (DataTable_t2176726999 *)il2cpp_codegen_object_new(DataTable_t2176726999_il2cpp_TypeInfo_var);
		DataTable__ctor_m798602097(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		DataTable_t2176726999 * L_1 = V_0;
		NullCheck(L_1);
		DataColumnCollection_t3528392753 * L_2 = DataTable_get_Columns_m220042291(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		DataColumnCollection_Add_m2541835761(L_2, _stringLiteral4182770465, L_3, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_4 = V_0;
		NullCheck(L_4);
		DataColumnCollection_t3528392753 * L_5 = DataTable_get_Columns_m220042291(L_4, /*hidden argument*/NULL);
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t2847414787_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_5);
		DataColumnCollection_Add_m2541835761(L_5, _stringLiteral394879515, L_6, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_7 = V_0;
		NullCheck(L_7);
		DataColumnCollection_t3528392753 * L_8 = DataTable_get_Columns_m220042291(L_7, /*hidden argument*/NULL);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t2847414787_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_8);
		DataColumnCollection_Add_m2541835761(L_8, _stringLiteral4182927511, L_9, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_10 = V_0;
		NullCheck(L_10);
		DataColumnCollection_t3528392753 * L_11 = DataTable_get_Columns_m220042291(L_10, /*hidden argument*/NULL);
		Type_t * L_12 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t2847414787_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_11);
		DataColumnCollection_Add_m2541835761(L_11, _stringLiteral764714001, L_12, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_13 = V_0;
		NullCheck(L_13);
		DataColumnCollection_t3528392753 * L_14 = DataTable_get_Columns_m220042291(L_13, /*hidden argument*/NULL);
		Type_t * L_15 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t2847414787_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_14);
		DataColumnCollection_Add_m2541835761(L_14, _stringLiteral2303668829, L_15, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_16 = V_0;
		NullCheck(L_16);
		DataColumnCollection_t3528392753 * L_17 = DataTable_get_Columns_m220042291(L_16, /*hidden argument*/NULL);
		Type_t * L_18 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t211005341_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_17);
		DataColumnCollection_Add_m2541835761(L_17, _stringLiteral4193116603, L_18, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_19 = V_0;
		NullCheck(L_19);
		DataColumnCollection_t3528392753 * L_20 = DataTable_get_Columns_m220042291(L_19, /*hidden argument*/NULL);
		Type_t * L_21 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t211005341_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_20);
		DataColumnCollection_Add_m2541835761(L_20, _stringLiteral70918325, L_21, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_22 = V_0;
		NullCheck(L_22);
		DataColumnCollection_t3528392753 * L_23 = DataTable_get_Columns_m220042291(L_22, /*hidden argument*/NULL);
		Type_t * L_24 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_23);
		DataColumnCollection_Add_m2541835761(L_23, _stringLiteral350959251, L_24, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_25 = V_0;
		NullCheck(L_25);
		DataColumnCollection_t3528392753 * L_26 = DataTable_get_Columns_m220042291(L_25, /*hidden argument*/NULL);
		Type_t * L_27 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_26);
		DataColumnCollection_Add_m2541835761(L_26, _stringLiteral3341501650, L_27, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_28 = V_0;
		NullCheck(L_28);
		DataColumnCollection_t3528392753 * L_29 = DataTable_get_Columns_m220042291(L_28, /*hidden argument*/NULL);
		Type_t * L_30 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_29);
		DataColumnCollection_Add_m2541835761(L_29, _stringLiteral703433117, L_30, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_31 = V_0;
		NullCheck(L_31);
		DataColumnCollection_t3528392753 * L_32 = DataTable_get_Columns_m220042291(L_31, /*hidden argument*/NULL);
		Type_t * L_33 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_32);
		DataColumnCollection_Add_m2541835761(L_32, _stringLiteral2897606536, L_33, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_34 = V_0;
		NullCheck(L_34);
		DataColumnCollection_t3528392753 * L_35 = DataTable_get_Columns_m220042291(L_34, /*hidden argument*/NULL);
		Type_t * L_36 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Type_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_35);
		DataColumnCollection_Add_m2541835761(L_35, _stringLiteral1853714980, L_36, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_37 = V_0;
		NullCheck(L_37);
		DataColumnCollection_t3528392753 * L_38 = DataTable_get_Columns_m220042291(L_37, /*hidden argument*/NULL);
		Type_t * L_39 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t211005341_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_38);
		DataColumnCollection_Add_m2541835761(L_38, _stringLiteral2204418958, L_39, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_40 = V_0;
		NullCheck(L_40);
		DataColumnCollection_t3528392753 * L_41 = DataTable_get_Columns_m220042291(L_40, /*hidden argument*/NULL);
		Type_t * L_42 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t2847414787_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_41);
		DataColumnCollection_Add_m2541835761(L_41, _stringLiteral4040228075, L_42, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_43 = V_0;
		NullCheck(L_43);
		DataColumnCollection_t3528392753 * L_44 = DataTable_get_Columns_m220042291(L_43, /*hidden argument*/NULL);
		Type_t * L_45 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t211005341_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_44);
		DataColumnCollection_Add_m2541835761(L_44, _stringLiteral509654597, L_45, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_46 = V_0;
		NullCheck(L_46);
		DataColumnCollection_t3528392753 * L_47 = DataTable_get_Columns_m220042291(L_46, /*hidden argument*/NULL);
		Type_t * L_48 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t211005341_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_47);
		DataColumnCollection_Add_m2541835761(L_47, _stringLiteral540140418, L_48, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_49 = V_0;
		NullCheck(L_49);
		DataColumnCollection_t3528392753 * L_50 = DataTable_get_Columns_m220042291(L_49, /*hidden argument*/NULL);
		Type_t * L_51 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t211005341_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_50);
		DataColumnCollection_Add_m2541835761(L_50, _stringLiteral1064432680, L_51, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_52 = V_0;
		NullCheck(L_52);
		DataColumnCollection_t3528392753 * L_53 = DataTable_get_Columns_m220042291(L_52, /*hidden argument*/NULL);
		Type_t * L_54 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t211005341_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_53);
		DataColumnCollection_Add_m2541835761(L_53, _stringLiteral3032076630, L_54, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_55 = V_0;
		NullCheck(L_55);
		DataColumnCollection_t3528392753 * L_56 = DataTable_get_Columns_m220042291(L_55, /*hidden argument*/NULL);
		Type_t * L_57 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t211005341_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_56);
		DataColumnCollection_Add_m2541835761(L_56, _stringLiteral1601200776, L_57, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_58 = V_0;
		NullCheck(L_58);
		DataColumnCollection_t3528392753 * L_59 = DataTable_get_Columns_m220042291(L_58, /*hidden argument*/NULL);
		Type_t * L_60 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t211005341_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_59);
		DataColumnCollection_Add_m2541835761(L_59, _stringLiteral3816158100, L_60, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_61 = V_0;
		NullCheck(L_61);
		DataColumnCollection_t3528392753 * L_62 = DataTable_get_Columns_m220042291(L_61, /*hidden argument*/NULL);
		Type_t * L_63 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t211005341_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_62);
		DataColumnCollection_Add_m2541835761(L_62, _stringLiteral2198507238, L_63, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_64 = V_0;
		NullCheck(L_64);
		DataColumnCollection_t3528392753 * L_65 = DataTable_get_Columns_m220042291(L_64, /*hidden argument*/NULL);
		Type_t * L_66 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t211005341_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_65);
		DataColumnCollection_Add_m2541835761(L_65, _stringLiteral332510668, L_66, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_67 = V_0;
		NullCheck(L_67);
		DataTable_BeginLoadData_m1640602056(L_67, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_03fd;
	}

IL_0265:
	{
		DataTable_t2176726999 * L_68 = V_0;
		NullCheck(L_68);
		DataRow_t3654701923 * L_69 = DataTable_NewRow_m3256434597(L_68, /*hidden argument*/NULL);
		V_2 = L_69;
		DataRow_t3654701923 * L_70 = V_2;
		StringU5BU5D_t2956870243* L_71 = __this->get_columns_3();
		int32_t L_72 = V_1;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, L_72);
		int32_t L_73 = L_72;
		NullCheck(L_70);
		DataRow_set_Item_m1424269409(L_70, _stringLiteral4182770465, ((L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_73))), /*hidden argument*/NULL);
		DataRow_t3654701923 * L_74 = V_2;
		int32_t L_75 = V_1;
		int32_t L_76 = L_75;
		Il2CppObject * L_77 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_76);
		NullCheck(L_74);
		DataRow_set_Item_m1424269409(L_74, _stringLiteral394879515, L_77, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_78 = V_2;
		int32_t L_79 = 0;
		Il2CppObject * L_80 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_79);
		NullCheck(L_78);
		DataRow_set_Item_m1424269409(L_78, _stringLiteral4182927511, L_80, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_81 = V_2;
		int32_t L_82 = 0;
		Il2CppObject * L_83 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_82);
		NullCheck(L_81);
		DataRow_set_Item_m1424269409(L_81, _stringLiteral764714001, L_83, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_84 = V_2;
		int32_t L_85 = 0;
		Il2CppObject * L_86 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_85);
		NullCheck(L_84);
		DataRow_set_Item_m1424269409(L_84, _stringLiteral2303668829, L_86, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_87 = V_2;
		bool L_88 = ((bool)0);
		Il2CppObject * L_89 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_88);
		NullCheck(L_87);
		DataRow_set_Item_m1424269409(L_87, _stringLiteral4193116603, L_89, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_90 = V_2;
		bool L_91 = ((bool)0);
		Il2CppObject * L_92 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_91);
		NullCheck(L_90);
		DataRow_set_Item_m1424269409(L_90, _stringLiteral70918325, L_92, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_93 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_94 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_93);
		DataRow_set_Item_m1424269409(L_93, _stringLiteral350959251, L_94, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_95 = V_2;
		StringU5BU5D_t2956870243* L_96 = __this->get_columns_3();
		int32_t L_97 = V_1;
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, L_97);
		int32_t L_98 = L_97;
		NullCheck(L_95);
		DataRow_set_Item_m1424269409(L_95, _stringLiteral3341501650, ((L_96)->GetAt(static_cast<il2cpp_array_size_t>(L_98))), /*hidden argument*/NULL);
		DataRow_t3654701923 * L_99 = V_2;
		String_t* L_100 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_99);
		DataRow_set_Item_m1424269409(L_99, _stringLiteral703433117, L_100, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_101 = V_2;
		String_t* L_102 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_101);
		DataRow_set_Item_m1424269409(L_101, _stringLiteral2897606536, L_102, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_103 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_104 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_103);
		DataRow_set_Item_m1424269409(L_103, _stringLiteral1853714980, L_104, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_105 = V_2;
		bool L_106 = ((bool)1);
		Il2CppObject * L_107 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_106);
		NullCheck(L_105);
		DataRow_set_Item_m1424269409(L_105, _stringLiteral2204418958, L_107, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_108 = V_2;
		int32_t L_109 = 0;
		Il2CppObject * L_110 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_109);
		NullCheck(L_108);
		DataRow_set_Item_m1424269409(L_108, _stringLiteral4040228075, L_110, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_111 = V_2;
		bool L_112 = ((bool)0);
		Il2CppObject * L_113 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_112);
		NullCheck(L_111);
		DataRow_set_Item_m1424269409(L_111, _stringLiteral509654597, L_113, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_114 = V_2;
		bool L_115 = ((bool)0);
		Il2CppObject * L_116 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_115);
		NullCheck(L_114);
		DataRow_set_Item_m1424269409(L_114, _stringLiteral540140418, L_116, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_117 = V_2;
		bool L_118 = ((bool)0);
		Il2CppObject * L_119 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_118);
		NullCheck(L_117);
		DataRow_set_Item_m1424269409(L_117, _stringLiteral1064432680, L_119, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_120 = V_2;
		bool L_121 = ((bool)0);
		Il2CppObject * L_122 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_121);
		NullCheck(L_120);
		DataRow_set_Item_m1424269409(L_120, _stringLiteral3032076630, L_122, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_123 = V_2;
		bool L_124 = ((bool)0);
		Il2CppObject * L_125 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_124);
		NullCheck(L_123);
		DataRow_set_Item_m1424269409(L_123, _stringLiteral1601200776, L_125, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_126 = V_2;
		bool L_127 = ((bool)0);
		Il2CppObject * L_128 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_127);
		NullCheck(L_126);
		DataRow_set_Item_m1424269409(L_126, _stringLiteral3816158100, L_128, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_129 = V_2;
		bool L_130 = ((bool)0);
		Il2CppObject * L_131 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_130);
		NullCheck(L_129);
		DataRow_set_Item_m1424269409(L_129, _stringLiteral2198507238, L_131, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_132 = V_2;
		bool L_133 = ((bool)0);
		Il2CppObject * L_134 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_133);
		NullCheck(L_132);
		DataRow_set_Item_m1424269409(L_132, _stringLiteral332510668, L_134, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_135 = V_0;
		NullCheck(L_135);
		DataRowCollection_t1405583905 * L_136 = DataTable_get_Rows_m954608043(L_135, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_137 = V_2;
		NullCheck(L_136);
		DataRowCollection_Add_m2125891809(L_136, L_137, /*hidden argument*/NULL);
		DataRow_t3654701923 * L_138 = V_2;
		NullCheck(L_138);
		DataRow_AcceptChanges_m818702046(L_138, /*hidden argument*/NULL);
		int32_t L_139 = V_1;
		V_1 = ((int32_t)((int32_t)L_139+(int32_t)1));
	}

IL_03fd:
	{
		int32_t L_140 = V_1;
		int32_t L_141 = VirtFuncInvoker0< int32_t >::Invoke(22 /* System.Int32 Mono.Data.SqliteClient.SqliteDataReader::get_FieldCount() */, __this);
		if ((((int32_t)L_140) < ((int32_t)L_141)))
		{
			goto IL_0265;
		}
	}
	{
		DataTable_t2176726999 * L_142 = V_0;
		NullCheck(L_142);
		DataTable_EndLoadData_m3405094650(L_142, /*hidden argument*/NULL);
		DataTable_t2176726999 * L_143 = V_0;
		return L_143;
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteDataReader::NextResult()
extern "C"  bool SqliteDataReader_NextResult_m2337084842 (SqliteDataReader_t545175945 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_current_row_6();
		__this->set_current_row_6(((int32_t)((int32_t)L_0+(int32_t)1)));
		int32_t L_1 = __this->get_current_row_6();
		ArrayList_t2121638921 * L_2 = __this->get_rows_2();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_2);
		return (bool)((((int32_t)L_1) < ((int32_t)L_3))? 1 : 0);
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteDataReader::Read()
extern "C"  bool SqliteDataReader_Read_m1001217936 (SqliteDataReader_t545175945 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(43 /* System.Boolean Mono.Data.SqliteClient.SqliteDataReader::NextResult() */, __this);
		return L_0;
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteDataReader::GetBoolean(System.Int32)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t SqliteDataReader_GetBoolean_m3554466685_MetadataUsageId;
extern "C"  bool SqliteDataReader_GetBoolean_m3554466685 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteDataReader_GetBoolean_m3554466685_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = __this->get_rows_2();
		int32_t L_1 = __this->get_current_row_6();
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		int32_t L_3 = ___i0;
		NullCheck(((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)));
		IL2CPP_ARRAY_BOUNDS_CHECK(((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)), L_3);
		int32_t L_4 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		bool L_5 = Convert_ToBoolean_m531114797(NULL /*static, unused*/, ((((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)))->GetAt(static_cast<il2cpp_array_size_t>(L_4))), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.String Mono.Data.SqliteClient.SqliteDataReader::GetDataTypeName(System.Int32)
extern Il2CppCodeGenString* _stringLiteral3556653;
extern const uint32_t SqliteDataReader_GetDataTypeName_m1418635401_MetadataUsageId;
extern "C"  String_t* SqliteDataReader_GetDataTypeName_m1418635401 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteDataReader_GetDataTypeName_m1418635401_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = __this->get_decltypes_10();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		StringU5BU5D_t2956870243* L_1 = __this->get_decltypes_10();
		int32_t L_2 = ___i0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		if (!((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3))))
		{
			goto IL_0021;
		}
	}
	{
		StringU5BU5D_t2956870243* L_4 = __this->get_decltypes_10();
		int32_t L_5 = ___i0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		return ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
	}

IL_0021:
	{
		return _stringLiteral3556653;
	}
}
// System.Type Mono.Data.SqliteClient.SqliteDataReader::GetFieldType(System.Int32)
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t SqliteDataReader_GetFieldType_m4068207367_MetadataUsageId;
extern "C"  Type_t * SqliteDataReader_GetFieldType_m4068207367 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteDataReader_GetFieldType_m4068207367_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		int32_t L_0 = __this->get_current_row_6();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_0029;
		}
	}
	{
		ArrayList_t2121638921 * L_2 = __this->get_rows_2();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_2);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		return L_4;
	}

IL_0029:
	{
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)(-1)))))
		{
			goto IL_0032;
		}
	}
	{
		V_0 = 0;
	}

IL_0032:
	{
		ArrayList_t2121638921 * L_6 = __this->get_rows_2();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Il2CppObject * L_8 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_6, L_7);
		int32_t L_9 = ___i0;
		NullCheck(((ObjectU5BU5D_t11523773*)Castclass(L_8, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)));
		IL2CPP_ARRAY_BOUNDS_CHECK(((ObjectU5BU5D_t11523773*)Castclass(L_8, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)), L_9);
		int32_t L_10 = L_9;
		V_1 = ((((ObjectU5BU5D_t11523773*)Castclass(L_8, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)))->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
		Il2CppObject * L_11 = V_1;
		if (!L_11)
		{
			goto IL_0053;
		}
	}
	{
		Il2CppObject * L_12 = V_1;
		NullCheck(L_12);
		Type_t * L_13 = Object_GetType_m2022236990(L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_0053:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Int16 Mono.Data.SqliteClient.SqliteDataReader::GetInt16(System.Int32)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t SqliteDataReader_GetInt16_m2270111253_MetadataUsageId;
extern "C"  int16_t SqliteDataReader_GetInt16_m2270111253 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteDataReader_GetInt16_m2270111253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = __this->get_rows_2();
		int32_t L_1 = __this->get_current_row_6();
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		int32_t L_3 = ___i0;
		NullCheck(((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)));
		IL2CPP_ARRAY_BOUNDS_CHECK(((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)), L_3);
		int32_t L_4 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int16_t L_5 = Convert_ToInt16_m686441237(NULL /*static, unused*/, ((((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)))->GetAt(static_cast<il2cpp_array_size_t>(L_4))), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Int32 Mono.Data.SqliteClient.SqliteDataReader::GetInt32(System.Int32)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t SqliteDataReader_GetInt32_m2154195209_MetadataUsageId;
extern "C"  int32_t SqliteDataReader_GetInt32_m2154195209 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteDataReader_GetInt32_m2154195209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = __this->get_rows_2();
		int32_t L_1 = __this->get_current_row_6();
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		int32_t L_3 = ___i0;
		NullCheck(((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)));
		IL2CPP_ARRAY_BOUNDS_CHECK(((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)), L_3);
		int32_t L_4 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int32_t L_5 = Convert_ToInt32_m12524065(NULL /*static, unused*/, ((((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)))->GetAt(static_cast<il2cpp_array_size_t>(L_4))), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Int64 Mono.Data.SqliteClient.SqliteDataReader::GetInt64(System.Int32)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t SqliteDataReader_GetInt64_m3001048967_MetadataUsageId;
extern "C"  int64_t SqliteDataReader_GetInt64_m3001048967 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteDataReader_GetInt64_m3001048967_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = __this->get_rows_2();
		int32_t L_1 = __this->get_current_row_6();
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		int32_t L_3 = ___i0;
		NullCheck(((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)));
		IL2CPP_ARRAY_BOUNDS_CHECK(((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)), L_3);
		int32_t L_4 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int64_t L_5 = Convert_ToInt64_m167563939(NULL /*static, unused*/, ((((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)))->GetAt(static_cast<il2cpp_array_size_t>(L_4))), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.String Mono.Data.SqliteClient.SqliteDataReader::GetName(System.Int32)
extern "C"  String_t* SqliteDataReader_GetName_m956931941 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		StringU5BU5D_t2956870243* L_0 = __this->get_columns_3();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		return ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
	}
}
// System.String Mono.Data.SqliteClient.SqliteDataReader::GetString(System.Int32)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t SqliteDataReader_GetString_m1538306155_MetadataUsageId;
extern "C"  String_t* SqliteDataReader_GetString_m1538306155 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteDataReader_GetString_m1538306155_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = __this->get_rows_2();
		int32_t L_1 = __this->get_current_row_6();
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		int32_t L_3 = ___i0;
		NullCheck(((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)));
		IL2CPP_ARRAY_BOUNDS_CHECK(((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)), L_3);
		int32_t L_4 = L_3;
		NullCheck(((((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)))->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, ((((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)))->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		return L_5;
	}
}
// System.Object Mono.Data.SqliteClient.SqliteDataReader::GetValue(System.Int32)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t SqliteDataReader_GetValue_m1756856681_MetadataUsageId;
extern "C"  Il2CppObject * SqliteDataReader_GetValue_m1756856681 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteDataReader_GetValue_m1756856681_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = __this->get_rows_2();
		int32_t L_1 = __this->get_current_row_6();
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		int32_t L_3 = ___i0;
		NullCheck(((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)));
		IL2CPP_ARRAY_BOUNDS_CHECK(((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)), L_3);
		int32_t L_4 = L_3;
		return ((((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)))->GetAt(static_cast<il2cpp_array_size_t>(L_4)));
	}
}
// System.Int32 Mono.Data.SqliteClient.SqliteDataReader::GetValues(System.Object[])
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* DBNull_t491814586_il2cpp_TypeInfo_var;
extern const uint32_t SqliteDataReader_GetValues_m3144340824_MetadataUsageId;
extern "C"  int32_t SqliteDataReader_GetValues_m3144340824 (SqliteDataReader_t545175945 * __this, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteDataReader_GetValues_m3144340824_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___values0;
		NullCheck(L_0);
		StringU5BU5D_t2956870243* L_1 = __this->get_columns_3();
		NullCheck(L_1);
		int32_t L_2 = Math_Min_m811624909(NULL /*static, unused*/, (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))), (((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = 0;
		goto IL_0061;
	}

IL_0018:
	{
		ArrayList_t2121638921 * L_3 = __this->get_rows_2();
		int32_t L_4 = __this->get_current_row_6();
		NullCheck(L_3);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_3, L_4);
		int32_t L_6 = V_1;
		NullCheck(((ObjectU5BU5D_t11523773*)Castclass(L_5, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)));
		IL2CPP_ARRAY_BOUNDS_CHECK(((ObjectU5BU5D_t11523773*)Castclass(L_5, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)), L_6);
		int32_t L_7 = L_6;
		if (!((((ObjectU5BU5D_t11523773*)Castclass(L_5, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)))->GetAt(static_cast<il2cpp_array_size_t>(L_7))))
		{
			goto IL_0055;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_8 = ___values0;
		int32_t L_9 = V_1;
		ArrayList_t2121638921 * L_10 = __this->get_rows_2();
		int32_t L_11 = __this->get_current_row_6();
		NullCheck(L_10);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_10, L_11);
		int32_t L_13 = V_1;
		NullCheck(((ObjectU5BU5D_t11523773*)Castclass(L_12, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)));
		IL2CPP_ARRAY_BOUNDS_CHECK(((ObjectU5BU5D_t11523773*)Castclass(L_12, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)), L_13);
		int32_t L_14 = L_13;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, ((((ObjectU5BU5D_t11523773*)Castclass(L_12, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)))->GetAt(static_cast<il2cpp_array_size_t>(L_14))));
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (Il2CppObject *)((((ObjectU5BU5D_t11523773*)Castclass(L_12, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)))->GetAt(static_cast<il2cpp_array_size_t>(L_14))));
		goto IL_005d;
	}

IL_0055:
	{
		ObjectU5BU5D_t11523773* L_15 = ___values0;
		int32_t L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DBNull_t491814586_il2cpp_TypeInfo_var);
		DBNull_t491814586 * L_17 = ((DBNull_t491814586_StaticFields*)DBNull_t491814586_il2cpp_TypeInfo_var->static_fields)->get_Value_0();
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (Il2CppObject *)L_17);
	}

IL_005d:
	{
		int32_t L_18 = V_1;
		V_1 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_1;
		int32_t L_20 = V_0;
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_21 = V_0;
		return L_21;
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteDataReader::IsDBNull(System.Int32)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t SqliteDataReader_IsDBNull_m3635478490_MetadataUsageId;
extern "C"  bool SqliteDataReader_IsDBNull_m3635478490 (SqliteDataReader_t545175945 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteDataReader_IsDBNull_m3635478490_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = __this->get_rows_2();
		int32_t L_1 = __this->get_current_row_6();
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		int32_t L_3 = ___i0;
		NullCheck(((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)));
		IL2CPP_ARRAY_BOUNDS_CHECK(((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)), L_3);
		int32_t L_4 = L_3;
		return (bool)((((Il2CppObject*)(Il2CppObject *)((((ObjectU5BU5D_t11523773*)Castclass(L_2, ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var)))->GetAt(static_cast<il2cpp_array_size_t>(L_4)))) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteDataReader::get_HasRows()
extern "C"  bool SqliteDataReader_get_HasRows_m634013074 (SqliteDataReader_t545175945 * __this, const MethodInfo* method)
{
	{
		ArrayList_t2121638921 * L_0 = __this->get_rows_2();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		return (bool)((((int32_t)L_1) > ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 Mono.Data.SqliteClient.SqliteDataReader::get_VisibleFieldCount()
extern "C"  int32_t SqliteDataReader_get_VisibleFieldCount_m3269162400 (SqliteDataReader_t545175945 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(22 /* System.Int32 Mono.Data.SqliteClient.SqliteDataReader::get_FieldCount() */, __this);
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteExecutionException::.ctor()
extern Il2CppCodeGenString* _stringLiteral1373783082;
extern const uint32_t SqliteExecutionException__ctor_m3454219336_MetadataUsageId;
extern "C"  void SqliteExecutionException__ctor_m3454219336 (SqliteExecutionException_t175718547 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteExecutionException__ctor_m3454219336_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SqliteExecutionException__ctor_m2226364346(__this, _stringLiteral1373783082, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteExecutionException::.ctor(System.String)
extern "C"  void SqliteExecutionException__ctor_m2226364346 (SqliteExecutionException_t175718547 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		ApplicationException__ctor_m1727689164(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameter::.ctor()
extern "C"  void SqliteParameter__ctor_m1693193598 (SqliteParameter_t2628453389 * __this, const MethodInfo* method)
{
	{
		DbParameter__ctor_m2022249449(__this, /*hidden argument*/NULL);
		__this->set_type_3(((int32_t)16));
		__this->set_direction_7(1);
		__this->set_isNullable_11((bool)1);
		return;
	}
}
// System.Data.DbType Mono.Data.SqliteClient.SqliteParameter::get_DbType()
extern "C"  int32_t SqliteParameter_get_DbType_m194818123 (SqliteParameter_t2628453389 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_3();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_DbType(System.Data.DbType)
extern "C"  void SqliteParameter_set_DbType_m734555902 (SqliteParameter_t2628453389 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_typeSet_5();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = __this->get_type_3();
		__this->set_originalType_4(L_1);
		__this->set_typeSet_5((bool)1);
	}

IL_001e:
	{
		int32_t L_2 = ___value0;
		__this->set_type_3(L_2);
		bool L_3 = __this->get_typeSet_5();
		if (L_3)
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_4 = __this->get_type_3();
		__this->set_originalType_4(L_4);
	}

IL_003c:
	{
		return;
	}
}
// System.Data.ParameterDirection Mono.Data.SqliteClient.SqliteParameter::get_Direction()
extern "C"  int32_t SqliteParameter_get_Direction_m1968364812 (SqliteParameter_t2628453389 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_direction_7();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_Direction(System.Data.ParameterDirection)
extern "C"  void SqliteParameter_set_Direction_m3141793699 (SqliteParameter_t2628453389 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_direction_7(L_0);
		return;
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteParameter::get_IsNullable()
extern "C"  bool SqliteParameter_get_IsNullable_m1709788274 (SqliteParameter_t2628453389 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isNullable_11();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_IsNullable(System.Boolean)
extern "C"  void SqliteParameter_set_IsNullable_m2697584485 (SqliteParameter_t2628453389 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_isNullable_11(L_0);
		return;
	}
}
// System.String Mono.Data.SqliteClient.SqliteParameter::get_ParameterName()
extern "C"  String_t* SqliteParameter_get_ParameterName_m3143765892 (SqliteParameter_t2628453389 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_2();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_ParameterName(System.String)
extern "C"  void SqliteParameter_set_ParameterName_m1446956239 (SqliteParameter_t2628453389 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_name_2(L_0);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_Size(System.Int32)
extern "C"  void SqliteParameter_set_Size_m931788565 (SqliteParameter_t2628453389 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_size_10(L_0);
		return;
	}
}
// System.String Mono.Data.SqliteClient.SqliteParameter::get_SourceColumn()
extern "C"  String_t* SqliteParameter_get_SourceColumn_m599646339 (SqliteParameter_t2628453389 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_source_column_6();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_SourceColumn(System.String)
extern "C"  void SqliteParameter_set_SourceColumn_m1936352878 (SqliteParameter_t2628453389 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_source_column_6(L_0);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_SourceColumnNullMapping(System.Boolean)
extern "C"  void SqliteParameter_set_SourceColumnNullMapping_m4236372780 (SqliteParameter_t2628453389 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_sourceColumnNullMapping_12(L_0);
		return;
	}
}
// System.Data.DataRowVersion Mono.Data.SqliteClient.SqliteParameter::get_SourceVersion()
extern "C"  int32_t SqliteParameter_get_SourceVersion_m1707108188 (SqliteParameter_t2628453389 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_row_version_8();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_SourceVersion(System.Data.DataRowVersion)
extern "C"  void SqliteParameter_set_SourceVersion_m3470416783 (SqliteParameter_t2628453389 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_row_version_8(L_0);
		return;
	}
}
// System.Object Mono.Data.SqliteClient.SqliteParameter::get_Value()
extern "C"  Il2CppObject * SqliteParameter_get_Value_m4012789519 (SqliteParameter_t2628453389 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_param_value_9();
		return L_0;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameter::set_Value(System.Object)
extern "C"  void SqliteParameter_set_Value_m3711129636 (SqliteParameter_t2628453389 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_param_value_9(L_0);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::.ctor()
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern const uint32_t SqliteParameterCollection__ctor_m2482897600_MetadataUsageId;
extern "C"  void SqliteParameterCollection__ctor_m2482897600 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteParameterCollection__ctor_m2482897600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		__this->set_numeric_param_list_1(L_0);
		Hashtable_t3875263730 * L_1 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_1, /*hidden argument*/NULL);
		__this->set_named_param_hash_2(L_1);
		DbParameterCollection__ctor_m4186716011(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::CheckSqliteParam(System.Object)
extern Il2CppClass* SqliteParameter_t2628453389_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3639814452;
extern const uint32_t SqliteParameterCollection_CheckSqliteParam_m2862447415_MetadataUsageId;
extern "C"  void SqliteParameterCollection_CheckSqliteParam_m2862447415 (SqliteParameterCollection_t3807043211 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteParameterCollection_CheckSqliteParam_m2862447415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SqliteParameter_t2628453389 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		if (((SqliteParameter_t2628453389 *)IsInstClass(L_0, SqliteParameter_t2628453389_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		InvalidCastException_t922874574 * L_1 = (InvalidCastException_t922874574 *)il2cpp_codegen_object_new(InvalidCastException_t922874574_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m2000083078(L_1, _stringLiteral3639814452, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___value0;
		V_0 = ((SqliteParameter_t2628453389 *)IsInstClass(L_2, SqliteParameter_t2628453389_il2cpp_TypeInfo_var));
		SqliteParameter_t2628453389 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String Mono.Data.SqliteClient.SqliteParameter::get_ParameterName() */, L_3);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		SqliteParameter_t2628453389 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String Mono.Data.SqliteClient.SqliteParameter::get_ParameterName() */, L_5);
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m2979997331(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0044;
		}
	}

IL_0038:
	{
		SqliteParameter_t2628453389 * L_8 = V_0;
		String_t* L_9 = SqliteParameterCollection_GenerateParameterName_m2602227002(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void Mono.Data.SqliteClient.SqliteParameter::set_ParameterName(System.String) */, L_8, L_9);
	}

IL_0044:
	{
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::RecreateNamedHash()
extern Il2CppClass* SqliteParameter_t2628453389_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t SqliteParameterCollection_RecreateNamedHash_m107150870_MetadataUsageId;
extern "C"  void SqliteParameterCollection_RecreateNamedHash_m107150870 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteParameterCollection_RecreateNamedHash_m107150870_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0007:
	{
		Hashtable_t3875263730 * L_0 = __this->get_named_param_hash_2();
		ArrayList_t2121638921 * L_1 = __this->get_numeric_param_list_1();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_1, L_2);
		NullCheck(((SqliteParameter_t2628453389 *)CastclassClass(L_3, SqliteParameter_t2628453389_il2cpp_TypeInfo_var)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String Mono.Data.SqliteClient.SqliteParameter::get_ParameterName() */, ((SqliteParameter_t2628453389 *)CastclassClass(L_3, SqliteParameter_t2628453389_il2cpp_TypeInfo_var)));
		int32_t L_5 = V_0;
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_0);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(27 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_0, L_4, L_7);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_9 = V_0;
		ArrayList_t2121638921 * L_10 = __this->get_numeric_param_list_1();
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_10);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.String Mono.Data.SqliteClient.SqliteParameterCollection::GenerateParameterName()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral58;
extern const uint32_t SqliteParameterCollection_GenerateParameterName_m2602227002_MetadataUsageId;
extern "C"  String_t* SqliteParameterCollection_GenerateParameterName_m2602227002 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteParameterCollection_GenerateParameterName_m2602227002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(22 /* System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::get_Count() */, __this);
		V_0 = ((int32_t)((int32_t)L_0+(int32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_1 = L_1;
		goto IL_003e;
	}

IL_0014:
	{
		String_t* L_2 = Int32_ToString_m1286526384((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral58, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		int32_t L_5 = VirtFuncInvoker1< int32_t, String_t* >::Invoke(36 /* System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::IndexOf(System.String) */, __this, L_4);
		if ((!(((uint32_t)L_5) == ((uint32_t)(-1)))))
		{
			goto IL_003a;
		}
	}
	{
		V_0 = (-1);
		goto IL_003e;
	}

IL_003a:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) > ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		String_t* L_8 = V_1;
		return L_8;
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::isPrefixed(System.String)
extern "C"  bool SqliteParameterCollection_isPrefixed_m3917059723 (SqliteParameterCollection_t3807043211 * __this, String_t* ___parameterName0, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		String_t* L_0 = ___parameterName0;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_2 = ___parameterName0;
		NullCheck(L_2);
		uint16_t L_3 = String_get_Chars_m3015341861(L_2, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)((int32_t)58))))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_4 = ___parameterName0;
		NullCheck(L_4);
		uint16_t L_5 = String_get_Chars_m3015341861(L_4, 0, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_5) == ((int32_t)((int32_t)36)))? 1 : 0);
		goto IL_0028;
	}

IL_0027:
	{
		G_B4_0 = 1;
	}

IL_0028:
	{
		G_B6_0 = G_B4_0;
		goto IL_002b;
	}

IL_002a:
	{
		G_B6_0 = 0;
	}

IL_002b:
	{
		return (bool)G_B6_0;
	}
}
// System.Data.Common.DbParameter Mono.Data.SqliteClient.SqliteParameterCollection::GetParameter(System.Int32)
extern Il2CppClass* SqliteParameter_t2628453389_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IndexOutOfRangeException_t3760259642_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1616400493;
extern const uint32_t SqliteParameterCollection_GetParameter_m4088518556_MetadataUsageId;
extern "C"  DbParameter_t3306161371 * SqliteParameterCollection_GetParameter_m4088518556 (SqliteParameterCollection_t3807043211 * __this, int32_t ___parameterIndex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteParameterCollection_GetParameter_m4088518556_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(22 /* System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::get_Count() */, __this);
		int32_t L_1 = ___parameterIndex0;
		if ((((int32_t)L_0) < ((int32_t)((int32_t)((int32_t)L_1+(int32_t)1)))))
		{
			goto IL_0020;
		}
	}
	{
		ArrayList_t2121638921 * L_2 = __this->get_numeric_param_list_1();
		int32_t L_3 = ___parameterIndex0;
		NullCheck(L_2);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_2, L_3);
		return ((SqliteParameter_t2628453389 *)CastclassClass(L_4, SqliteParameter_t2628453389_il2cpp_TypeInfo_var));
	}

IL_0020:
	{
		String_t* L_5 = Int32_ToString_m1286526384((&___parameterIndex0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1616400493, L_5, /*hidden argument*/NULL);
		IndexOutOfRangeException_t3760259642 * L_7 = (IndexOutOfRangeException_t3760259642 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3760259642_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::SetParameter(System.Int32,System.Data.Common.DbParameter)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IndexOutOfRangeException_t3760259642_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1616400493;
extern const uint32_t SqliteParameterCollection_SetParameter_m2753962653_MetadataUsageId;
extern "C"  void SqliteParameterCollection_SetParameter_m2753962653 (SqliteParameterCollection_t3807043211 * __this, int32_t ___parameterIndex0, DbParameter_t3306161371 * ___parameter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteParameterCollection_SetParameter_m2753962653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(22 /* System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::get_Count() */, __this);
		int32_t L_1 = ___parameterIndex0;
		if ((((int32_t)L_0) < ((int32_t)((int32_t)((int32_t)L_1+(int32_t)1)))))
		{
			goto IL_0020;
		}
	}
	{
		ArrayList_t2121638921 * L_2 = __this->get_numeric_param_list_1();
		int32_t L_3 = ___parameterIndex0;
		DbParameter_t3306161371 * L_4 = ___parameter1;
		NullCheck(L_2);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(22 /* System.Void System.Collections.ArrayList::set_Item(System.Int32,System.Object) */, L_2, L_3, L_4);
		goto IL_0037;
	}

IL_0020:
	{
		String_t* L_5 = Int32_ToString_m1286526384((&___parameterIndex0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1616400493, L_5, /*hidden argument*/NULL);
		IndexOutOfRangeException_t3760259642 * L_7 = (IndexOutOfRangeException_t3760259642 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3760259642_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0037:
	{
		return;
	}
}
// System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::get_Count()
extern "C"  int32_t SqliteParameterCollection_get_Count_m3376269234 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method)
{
	{
		ArrayList_t2121638921 * L_0 = __this->get_numeric_param_list_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		return L_1;
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::get_IsFixedSize()
extern "C"  bool SqliteParameterCollection_get_IsFixedSize_m1792392136 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method)
{
	{
		ArrayList_t2121638921 * L_0 = __this->get_numeric_param_list_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(26 /* System.Boolean System.Collections.ArrayList::get_IsFixedSize() */, L_0);
		return L_1;
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::get_IsReadOnly()
extern "C"  bool SqliteParameterCollection_get_IsReadOnly_m3282774577 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method)
{
	{
		ArrayList_t2121638921 * L_0 = __this->get_numeric_param_list_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(27 /* System.Boolean System.Collections.ArrayList::get_IsReadOnly() */, L_0);
		return L_1;
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::get_IsSynchronized()
extern "C"  bool SqliteParameterCollection_get_IsSynchronized_m763380067 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method)
{
	{
		ArrayList_t2121638921 * L_0 = __this->get_numeric_param_list_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Collections.ArrayList::get_IsSynchronized() */, L_0);
		return L_1;
	}
}
// System.Object Mono.Data.SqliteClient.SqliteParameterCollection::get_SyncRoot()
extern "C"  Il2CppObject * SqliteParameterCollection_get_SyncRoot_m2711312415 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method)
{
	{
		ArrayList_t2121638921 * L_0 = __this->get_numeric_param_list_1();
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(29 /* System.Object System.Collections.ArrayList::get_SyncRoot() */, L_0);
		return L_1;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::AddRange(System.Array)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SqliteParameterCollection_AddRange_m1609136646_MetadataUsageId;
extern "C"  void SqliteParameterCollection_AddRange_m1609136646 (SqliteParameterCollection_t3807043211 * __this, Il2CppArray * ___values0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteParameterCollection_AddRange_m1609136646_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppArray * L_0 = ___values0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Il2CppArray * L_1 = ___values0;
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m1203127607(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0012;
		}
	}

IL_0011:
	{
		return;
	}

IL_0012:
	{
		Il2CppArray * L_3 = ___values0;
		NullCheck(L_3);
		Il2CppObject * L_4 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* System.Collections.IEnumerator System.Array::GetEnumerator() */, L_3);
		V_1 = L_4;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_001e:
		{
			Il2CppObject * L_5 = V_1;
			NullCheck(L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_5);
			V_0 = L_6;
			Il2CppObject * L_7 = V_0;
			VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(27 /* System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::Add(System.Object) */, __this, L_7);
		}

IL_002d:
		{
			Il2CppObject * L_8 = V_1;
			NullCheck(L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_001e;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x4F, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_10 = V_1;
			V_2 = ((Il2CppObject *)IsInst(L_10, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_11 = V_2;
			if (L_11)
			{
				goto IL_0048;
			}
		}

IL_0047:
		{
			IL2CPP_END_FINALLY(61)
		}

IL_0048:
		{
			Il2CppObject * L_12 = V_2;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_12);
			IL2CPP_END_FINALLY(61)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004f:
	{
		return;
	}
}
// System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::Add(System.Object)
extern Il2CppClass* SqliteParameter_t2628453389_il2cpp_TypeInfo_var;
extern Il2CppClass* DuplicateNameException_t46464140_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1526489127;
extern const uint32_t SqliteParameterCollection_Add_m2982982599_MetadataUsageId;
extern "C"  int32_t SqliteParameterCollection_Add_m2982982599 (SqliteParameterCollection_t3807043211 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteParameterCollection_Add_m2982982599_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SqliteParameter_t2628453389 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		SqliteParameterCollection_CheckSqliteParam_m2862447415(__this, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___value0;
		V_0 = ((SqliteParameter_t2628453389 *)IsInstClass(L_1, SqliteParameter_t2628453389_il2cpp_TypeInfo_var));
		Hashtable_t3875263730 * L_2 = __this->get_named_param_hash_2();
		SqliteParameter_t2628453389 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String Mono.Data.SqliteClient.SqliteParameter::get_ParameterName() */, L_3);
		NullCheck(L_2);
		bool L_5 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(31 /* System.Boolean System.Collections.Hashtable::Contains(System.Object) */, L_2, L_4);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		DuplicateNameException_t46464140 * L_6 = (DuplicateNameException_t46464140 *)il2cpp_codegen_object_new(DuplicateNameException_t46464140_il2cpp_TypeInfo_var);
		DuplicateNameException__ctor_m3773557642(L_6, _stringLiteral1526489127, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		Hashtable_t3875263730 * L_7 = __this->get_named_param_hash_2();
		SqliteParameter_t2628453389 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String Mono.Data.SqliteClient.SqliteParameter::get_ParameterName() */, L_8);
		ArrayList_t2121638921 * L_10 = __this->get_numeric_param_list_1();
		Il2CppObject * L_11 = ___value0;
		NullCheck(L_10);
		int32_t L_12 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_10, L_11);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_7);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(27 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_7, L_9, L_14);
		Hashtable_t3875263730 * L_15 = __this->get_named_param_hash_2();
		SqliteParameter_t2628453389 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String Mono.Data.SqliteClient.SqliteParameter::get_ParameterName() */, L_16);
		NullCheck(L_15);
		Il2CppObject * L_18 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_15, L_17);
		return ((*(int32_t*)((int32_t*)UnBox (L_18, Int32_t2847414787_il2cpp_TypeInfo_var))));
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::Clear()
extern "C"  void SqliteParameterCollection_Clear_m4183998187 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method)
{
	{
		ArrayList_t2121638921 * L_0 = __this->get_numeric_param_list_1();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(31 /* System.Void System.Collections.ArrayList::Clear() */, L_0);
		Hashtable_t3875263730 * L_1 = __this->get_named_param_hash_2();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(30 /* System.Void System.Collections.Hashtable::Clear() */, L_1);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::CopyTo(System.Array,System.Int32)
extern "C"  void SqliteParameterCollection_CopyTo_m3803708317 (SqliteParameterCollection_t3807043211 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ArrayList_t2121638921 * L_0 = __this->get_numeric_param_list_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck(L_0);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(41 /* System.Void System.Collections.ArrayList::CopyTo(System.Array,System.Int32) */, L_0, L_1, L_2);
		return;
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::Contains(System.Object)
extern Il2CppClass* SqliteParameter_t2628453389_il2cpp_TypeInfo_var;
extern const uint32_t SqliteParameterCollection_Contains_m3374294425_MetadataUsageId;
extern "C"  bool SqliteParameterCollection_Contains_m3374294425 (SqliteParameterCollection_t3807043211 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteParameterCollection_Contains_m3374294425_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = SqliteParameterCollection_Contains_m2896464572(__this, ((SqliteParameter_t2628453389 *)CastclassClass(L_0, SqliteParameter_t2628453389_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::Contains(System.String)
extern "C"  bool SqliteParameterCollection_Contains_m3152184071 (SqliteParameterCollection_t3807043211 * __this, String_t* ___parameterName0, const MethodInfo* method)
{
	{
		Hashtable_t3875263730 * L_0 = __this->get_named_param_hash_2();
		String_t* L_1 = ___parameterName0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(31 /* System.Boolean System.Collections.Hashtable::Contains(System.Object) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::Contains(Mono.Data.SqliteClient.SqliteParameter)
extern "C"  bool SqliteParameterCollection_Contains_m2896464572 (SqliteParameterCollection_t3807043211 * __this, SqliteParameter_t2628453389 * ___param0, const MethodInfo* method)
{
	{
		SqliteParameter_t2628453389 * L_0 = ___param0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String Mono.Data.SqliteClient.SqliteParameter::get_ParameterName() */, L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(31 /* System.Boolean Mono.Data.SqliteClient.SqliteParameterCollection::Contains(System.String) */, __this, L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator Mono.Data.SqliteClient.SqliteParameterCollection::GetEnumerator()
extern "C"  Il2CppObject * SqliteParameterCollection_GetEnumerator_m2279596428 (SqliteParameterCollection_t3807043211 * __this, const MethodInfo* method)
{
	{
		ArrayList_t2121638921 * L_0 = __this->get_numeric_param_list_1();
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_0);
		return L_1;
	}
}
// System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::IndexOf(System.Object)
extern Il2CppClass* SqliteParameter_t2628453389_il2cpp_TypeInfo_var;
extern const uint32_t SqliteParameterCollection_IndexOf_m2154802847_MetadataUsageId;
extern "C"  int32_t SqliteParameterCollection_IndexOf_m2154802847 (SqliteParameterCollection_t3807043211 * __this, Il2CppObject * ___param0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteParameterCollection_IndexOf_m2154802847_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___param0;
		int32_t L_1 = SqliteParameterCollection_IndexOf_m2119734390(__this, ((SqliteParameter_t2628453389 *)CastclassClass(L_0, SqliteParameter_t2628453389_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::IndexOf(System.String)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t SqliteParameterCollection_IndexOf_m1932692493_MetadataUsageId;
extern "C"  int32_t SqliteParameterCollection_IndexOf_m1932692493 (SqliteParameterCollection_t3807043211 * __this, String_t* ___parameterName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteParameterCollection_IndexOf_m1932692493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___parameterName0;
		bool L_1 = SqliteParameterCollection_isPrefixed_m3917059723(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0037;
		}
	}
	{
		String_t* L_2 = ___parameterName0;
		NullCheck(L_2);
		String_t* L_3 = String_Substring_m2809233063(L_2, 1, /*hidden argument*/NULL);
		V_0 = L_3;
		Hashtable_t3875263730 * L_4 = __this->get_named_param_hash_2();
		String_t* L_5 = V_0;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(31 /* System.Boolean System.Collections.Hashtable::Contains(System.Object) */, L_4, L_5);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Hashtable_t3875263730 * L_7 = __this->get_named_param_hash_2();
		String_t* L_8 = V_0;
		NullCheck(L_7);
		Il2CppObject * L_9 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_7, L_8);
		return ((*(int32_t*)((int32_t*)UnBox (L_9, Int32_t2847414787_il2cpp_TypeInfo_var))));
	}

IL_0037:
	{
		Hashtable_t3875263730 * L_10 = __this->get_named_param_hash_2();
		String_t* L_11 = ___parameterName0;
		NullCheck(L_10);
		bool L_12 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(31 /* System.Boolean System.Collections.Hashtable::Contains(System.Object) */, L_10, L_11);
		if (!L_12)
		{
			goto IL_005a;
		}
	}
	{
		Hashtable_t3875263730 * L_13 = __this->get_named_param_hash_2();
		String_t* L_14 = ___parameterName0;
		NullCheck(L_13);
		Il2CppObject * L_15 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_13, L_14);
		return ((*(int32_t*)((int32_t*)UnBox (L_15, Int32_t2847414787_il2cpp_TypeInfo_var))));
	}

IL_005a:
	{
		return (-1);
	}
}
// System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::IndexOf(Mono.Data.SqliteClient.SqliteParameter)
extern "C"  int32_t SqliteParameterCollection_IndexOf_m2119734390 (SqliteParameterCollection_t3807043211 * __this, SqliteParameter_t2628453389 * ___param0, const MethodInfo* method)
{
	{
		SqliteParameter_t2628453389 * L_0 = ___param0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String Mono.Data.SqliteClient.SqliteParameter::get_ParameterName() */, L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, String_t* >::Invoke(36 /* System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::IndexOf(System.String) */, __this, L_1);
		return L_2;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::Insert(System.Int32,System.Object)
extern "C"  void SqliteParameterCollection_Insert_m3898561116 (SqliteParameterCollection_t3807043211 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value1;
		SqliteParameterCollection_CheckSqliteParam_m2862447415(__this, L_0, /*hidden argument*/NULL);
		ArrayList_t2121638921 * L_1 = __this->get_numeric_param_list_1();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		int32_t L_3 = ___index0;
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0021;
		}
	}
	{
		Il2CppObject * L_4 = ___value1;
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(27 /* System.Int32 Mono.Data.SqliteClient.SqliteParameterCollection::Add(System.Object) */, __this, L_4);
		return;
	}

IL_0021:
	{
		ArrayList_t2121638921 * L_5 = __this->get_numeric_param_list_1();
		int32_t L_6 = ___index0;
		Il2CppObject * L_7 = ___value1;
		NullCheck(L_5);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(36 /* System.Void System.Collections.ArrayList::Insert(System.Int32,System.Object) */, L_5, L_6, L_7);
		SqliteParameterCollection_RecreateNamedHash_m107150870(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::Remove(System.Object)
extern Il2CppClass* SqliteParameter_t2628453389_il2cpp_TypeInfo_var;
extern const uint32_t SqliteParameterCollection_Remove_m2293346636_MetadataUsageId;
extern "C"  void SqliteParameterCollection_Remove_m2293346636 (SqliteParameterCollection_t3807043211 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteParameterCollection_Remove_m2293346636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		SqliteParameterCollection_CheckSqliteParam_m2862447415(__this, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___value0;
		SqliteParameterCollection_RemoveAt_m2152681980(__this, ((SqliteParameter_t2628453389 *)CastclassClass(L_1, SqliteParameter_t2628453389_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::RemoveAt(System.Int32)
extern Il2CppClass* SqliteParameter_t2628453389_il2cpp_TypeInfo_var;
extern const uint32_t SqliteParameterCollection_RemoveAt_m3959624684_MetadataUsageId;
extern "C"  void SqliteParameterCollection_RemoveAt_m3959624684 (SqliteParameterCollection_t3807043211 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteParameterCollection_RemoveAt_m3959624684_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = __this->get_numeric_param_list_1();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		NullCheck(((SqliteParameter_t2628453389 *)CastclassClass(L_2, SqliteParameter_t2628453389_il2cpp_TypeInfo_var)));
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String Mono.Data.SqliteClient.SqliteParameter::get_ParameterName() */, ((SqliteParameter_t2628453389 *)CastclassClass(L_2, SqliteParameter_t2628453389_il2cpp_TypeInfo_var)));
		VirtActionInvoker1< String_t* >::Invoke(40 /* System.Void Mono.Data.SqliteClient.SqliteParameterCollection::RemoveAt(System.String) */, __this, L_3);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::RemoveAt(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ApplicationException_t3809199252_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2452200887;
extern Il2CppCodeGenString* _stringLiteral2113392597;
extern const uint32_t SqliteParameterCollection_RemoveAt_m2945979335_MetadataUsageId;
extern "C"  void SqliteParameterCollection_RemoveAt_m2945979335 (SqliteParameterCollection_t3807043211 * __this, String_t* ___parameterName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteParameterCollection_RemoveAt_m2945979335_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t3875263730 * L_0 = __this->get_named_param_hash_2();
		String_t* L_1 = ___parameterName0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(31 /* System.Boolean System.Collections.Hashtable::Contains(System.Object) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_3 = ___parameterName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2452200887, L_3, _stringLiteral2113392597, /*hidden argument*/NULL);
		ApplicationException_t3809199252 * L_5 = (ApplicationException_t3809199252 *)il2cpp_codegen_object_new(ApplicationException_t3809199252_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m1727689164(L_5, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0027:
	{
		ArrayList_t2121638921 * L_6 = __this->get_numeric_param_list_1();
		Hashtable_t3875263730 * L_7 = __this->get_named_param_hash_2();
		String_t* L_8 = ___parameterName0;
		NullCheck(L_7);
		Il2CppObject * L_9 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_7, L_8);
		NullCheck(L_6);
		VirtActionInvoker1< int32_t >::Invoke(39 /* System.Void System.Collections.ArrayList::RemoveAt(System.Int32) */, L_6, ((*(int32_t*)((int32_t*)UnBox (L_9, Int32_t2847414787_il2cpp_TypeInfo_var)))));
		Hashtable_t3875263730 * L_10 = __this->get_named_param_hash_2();
		String_t* L_11 = ___parameterName0;
		NullCheck(L_10);
		VirtActionInvoker1< Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Remove(System.Object) */, L_10, L_11);
		SqliteParameterCollection_RecreateNamedHash_m107150870(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteParameterCollection::RemoveAt(Mono.Data.SqliteClient.SqliteParameter)
extern "C"  void SqliteParameterCollection_RemoveAt_m2152681980 (SqliteParameterCollection_t3807043211 * __this, SqliteParameter_t2628453389 * ___param0, const MethodInfo* method)
{
	{
		SqliteParameter_t2628453389 * L_0 = ___param0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String Mono.Data.SqliteClient.SqliteParameter::get_ParameterName() */, L_0);
		VirtActionInvoker1< String_t* >::Invoke(40 /* System.Void Mono.Data.SqliteClient.SqliteParameterCollection::RemoveAt(System.String) */, __this, L_1);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteSyntaxException::.ctor(System.String)
extern "C"  void SqliteSyntaxException__ctor_m294290695 (SqliteSyntaxException_t416288016 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		ApplicationException__ctor_m1727689164(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteTransaction::.ctor()
extern "C"  void SqliteTransaction__ctor_m4193169641 (SqliteTransaction_t2154866242 * __this, const MethodInfo* method)
{
	{
		DbTransaction__ctor_m2588262548(__this, /*hidden argument*/NULL);
		__this->set__open_2((bool)1);
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteTransaction::SetConnection(System.Data.Common.DbConnection)
extern Il2CppClass* SqliteConnection_t2830494554_il2cpp_TypeInfo_var;
extern const uint32_t SqliteTransaction_SetConnection_m1734324017_MetadataUsageId;
extern "C"  void SqliteTransaction_SetConnection_m1734324017 (SqliteTransaction_t2154866242 * __this, DbConnection_t462757452 * ___conn0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteTransaction_SetConnection_m1734324017_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DbConnection_t462757452 * L_0 = ___conn0;
		__this->set__connection_1(((SqliteConnection_t2830494554 *)CastclassClass(L_0, SqliteConnection_t2830494554_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteTransaction::Commit()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* SqliteCommand_t3207195823_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2854646521;
extern Il2CppCodeGenString* _stringLiteral3983702345;
extern Il2CppCodeGenString* _stringLiteral1993481527;
extern const uint32_t SqliteTransaction_Commit_m941214770_MetadataUsageId;
extern "C"  void SqliteTransaction_Commit_m941214770 (SqliteTransaction_t2154866242 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteTransaction_Commit_m941214770_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SqliteCommand_t3207195823 * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SqliteConnection_t2830494554 * L_0 = __this->get__connection_1();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		SqliteConnection_t2830494554 * L_1 = __this->get__connection_1();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Data.ConnectionState Mono.Data.SqliteClient.SqliteConnection::get_State() */, L_1);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0027;
		}
	}

IL_001c:
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, _stringLiteral2854646521, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0027:
	{
		bool L_4 = __this->get__open_2();
		if (L_4)
		{
			goto IL_003d;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_5 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, _stringLiteral3983702345, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_003d:
	try
	{ // begin try (depth: 1)
		SqliteConnection_t2830494554 * L_6 = __this->get__connection_1();
		NullCheck(L_6);
		DbCommand_t2323745021 * L_7 = DbConnection_CreateCommand_m1718958439(L_6, /*hidden argument*/NULL);
		V_0 = ((SqliteCommand_t3207195823 *)CastclassClass(L_7, SqliteCommand_t3207195823_il2cpp_TypeInfo_var));
		SqliteCommand_t3207195823 * L_8 = V_0;
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(18 /* System.Void Mono.Data.SqliteClient.SqliteCommand::set_CommandText(System.String) */, L_8, _stringLiteral1993481527);
		SqliteCommand_t3207195823 * L_9 = V_0;
		NullCheck(L_9);
		VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 Mono.Data.SqliteClient.SqliteCommand::ExecuteNonQuery() */, L_9);
		__this->set__open_2((bool)0);
		goto IL_0074;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_006c;
		throw e;
	}

CATCH_006c:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_10 = V_1;
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
		}

IL_006f:
		{
			goto IL_0074;
		}
	} // end catch (depth: 1)

IL_0074:
	{
		return;
	}
}
// System.Void Mono.Data.SqliteClient.SqliteTransaction::Rollback()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* SqliteCommand_t3207195823_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2854646521;
extern Il2CppCodeGenString* _stringLiteral512627128;
extern Il2CppCodeGenString* _stringLiteral522907364;
extern const uint32_t SqliteTransaction_Rollback_m1065083935_MetadataUsageId;
extern "C"  void SqliteTransaction_Rollback_m1065083935 (SqliteTransaction_t2154866242 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SqliteTransaction_Rollback_m1065083935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SqliteCommand_t3207195823 * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SqliteConnection_t2830494554 * L_0 = __this->get__connection_1();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		SqliteConnection_t2830494554 * L_1 = __this->get__connection_1();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Data.ConnectionState Mono.Data.SqliteClient.SqliteConnection::get_State() */, L_1);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0027;
		}
	}

IL_001c:
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, _stringLiteral2854646521, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0027:
	{
		bool L_4 = __this->get__open_2();
		if (L_4)
		{
			goto IL_003d;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_5 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, _stringLiteral512627128, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_003d:
	try
	{ // begin try (depth: 1)
		SqliteConnection_t2830494554 * L_6 = __this->get__connection_1();
		NullCheck(L_6);
		DbCommand_t2323745021 * L_7 = DbConnection_CreateCommand_m1718958439(L_6, /*hidden argument*/NULL);
		V_0 = ((SqliteCommand_t3207195823 *)CastclassClass(L_7, SqliteCommand_t3207195823_il2cpp_TypeInfo_var));
		SqliteCommand_t3207195823 * L_8 = V_0;
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(18 /* System.Void Mono.Data.SqliteClient.SqliteCommand::set_CommandText(System.String) */, L_8, _stringLiteral522907364);
		SqliteCommand_t3207195823 * L_9 = V_0;
		NullCheck(L_9);
		VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 Mono.Data.SqliteClient.SqliteCommand::ExecuteNonQuery() */, L_9);
		__this->set__open_2((bool)0);
		goto IL_0074;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_006c;
		throw e;
	}

CATCH_006c:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_10 = V_1;
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
		}

IL_006f:
		{
			goto IL_0074;
		}
	} // end catch (depth: 1)

IL_0074:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
