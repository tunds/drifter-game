﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Data_Mono_Data_SqlExpressions_yyParser_yyEx2360046412.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.SqlExpressions.yyParser.yyUnexpectedEof
struct  yyUnexpectedEof_t4221696488  : public yyException_t2360046412
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
