﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteDataReader
struct SqliteDataReader_t1567858368;
// Mono.Data.Sqlite.SqliteCommand
struct SqliteCommand_t4229878246;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Data.DataTable
struct DataTable_t2176726999;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// Mono.Data.Sqlite.SQLiteType
struct SQLiteType_t3947843053;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_SqliteCommand4229878246.h"
#include "System_Data_System_Data_CommandBehavior326612048.h"
#include "Mono_Data_Sqlite_Mono_Data_Sqlite_TypeAffinity3864856329.h"
#include "System_Data_System_Data_DbType2586775211.h"

// System.Void Mono.Data.Sqlite.SqliteDataReader::.ctor(Mono.Data.Sqlite.SqliteCommand,System.Data.CommandBehavior)
extern "C"  void SqliteDataReader__ctor_m1807980656 (SqliteDataReader_t1567858368 * __this, SqliteCommand_t4229878246 * ___cmd0, int32_t ___behave1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteDataReader::Close()
extern "C"  void SqliteDataReader_Close_m3213039688 (SqliteDataReader_t1567858368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteDataReader::CheckClosed()
extern "C"  void SqliteDataReader_CheckClosed_m500095140 (SqliteDataReader_t1567858368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteDataReader::CheckValidRow()
extern "C"  void SqliteDataReader_CheckValidRow_m1556805942 (SqliteDataReader_t1567858368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Mono.Data.Sqlite.SqliteDataReader::GetEnumerator()
extern "C"  Il2CppObject * SqliteDataReader_GetEnumerator_m629197216 (SqliteDataReader_t1567858368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteDataReader::get_FieldCount()
extern "C"  int32_t SqliteDataReader_get_FieldCount_m3703833140 (SqliteDataReader_t1567858368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteDataReader::get_VisibleFieldCount()
extern "C"  int32_t SqliteDataReader_get_VisibleFieldCount_m4005426570 (SqliteDataReader_t1567858368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.TypeAffinity Mono.Data.Sqlite.SqliteDataReader::VerifyType(System.Int32,System.Data.DbType)
extern "C"  int32_t SqliteDataReader_VerifyType_m1309576603 (SqliteDataReader_t1567858368 * __this, int32_t ___i0, int32_t ___typ1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteDataReader::GetBoolean(System.Int32)
extern "C"  bool SqliteDataReader_GetBoolean_m1668541843 (SqliteDataReader_t1567858368 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteDataReader::GetDataTypeName(System.Int32)
extern "C"  String_t* SqliteDataReader_GetDataTypeName_m1740311337 (SqliteDataReader_t1567858368 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Mono.Data.Sqlite.SqliteDataReader::GetFieldType(System.Int32)
extern "C"  Type_t * SqliteDataReader_GetFieldType_m907907175 (SqliteDataReader_t1567858368 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 Mono.Data.Sqlite.SqliteDataReader::GetInt16(System.Int32)
extern "C"  int16_t SqliteDataReader_GetInt16_m2528693611 (SqliteDataReader_t1567858368 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteDataReader::GetInt32(System.Int32)
extern "C"  int32_t SqliteDataReader_GetInt32_m3147776991 (SqliteDataReader_t1567858368 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.Data.Sqlite.SqliteDataReader::GetInt64(System.Int32)
extern "C"  int64_t SqliteDataReader_GetInt64_m1199746461 (SqliteDataReader_t1567858368 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteDataReader::GetName(System.Int32)
extern "C"  String_t* SqliteDataReader_GetName_m2979176965 (SqliteDataReader_t1567858368 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteDataReader::GetSchemaTable()
extern "C"  DataTable_t2176726999 * SqliteDataReader_GetSchemaTable_m3549290965 (SqliteDataReader_t1567858368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.DataTable Mono.Data.Sqlite.SqliteDataReader::GetSchemaTable(System.Boolean,System.Boolean)
extern "C"  DataTable_t2176726999 * SqliteDataReader_GetSchemaTable_m316386161 (SqliteDataReader_t1567858368 * __this, bool ___wantUniqueInfo0, bool ___wantDefaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Data.Sqlite.SqliteDataReader::GetString(System.Int32)
extern "C"  String_t* SqliteDataReader_GetString_m3590556427 (SqliteDataReader_t1567858368 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteDataReader::GetValue(System.Int32)
extern "C"  Il2CppObject * SqliteDataReader_GetValue_m3187690825 (SqliteDataReader_t1567858368 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteDataReader::GetValues(System.Object[])
extern "C"  int32_t SqliteDataReader_GetValues_m2792248622 (SqliteDataReader_t1567858368 * __this, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteDataReader::get_HasRows()
extern "C"  bool SqliteDataReader_get_HasRows_m2636752188 (SqliteDataReader_t1567858368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteDataReader::get_IsClosed()
extern "C"  bool SqliteDataReader_get_IsClosed_m1309672015 (SqliteDataReader_t1567858368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteDataReader::IsDBNull(System.Int32)
extern "C"  bool SqliteDataReader_IsDBNull_m3052511088 (SqliteDataReader_t1567858368 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteDataReader::NextResult()
extern "C"  bool SqliteDataReader_NextResult_m739121344 (SqliteDataReader_t1567858368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Data.Sqlite.SQLiteType Mono.Data.Sqlite.SqliteDataReader::GetSQLiteType(System.Int32)
extern "C"  SQLiteType_t3947843053 * SqliteDataReader_GetSQLiteType_m1806016193 (SqliteDataReader_t1567858368 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Data.Sqlite.SqliteDataReader::Read()
extern "C"  bool SqliteDataReader_Read_m3051088678 (SqliteDataReader_t1567858368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Data.Sqlite.SqliteDataReader::get_RecordsAffected()
extern "C"  int32_t SqliteDataReader_get_RecordsAffected_m3848701657 (SqliteDataReader_t1567858368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteDataReader::get_Item(System.Int32)
extern "C"  Il2CppObject * SqliteDataReader_get_Item_m2534687274 (SqliteDataReader_t1567858368 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteDataReader::LoadKeyInfo()
extern "C"  void SqliteDataReader_LoadKeyInfo_m149509943 (SqliteDataReader_t1567858368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
