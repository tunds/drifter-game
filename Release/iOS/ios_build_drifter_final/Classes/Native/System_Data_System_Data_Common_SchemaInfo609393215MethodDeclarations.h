﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Data.Common.SchemaInfo
struct SchemaInfo_t609393215;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void System.Data.Common.SchemaInfo::.ctor()
extern "C"  void SchemaInfo__ctor_m3311920515 (SchemaInfo_t609393215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.SchemaInfo::get_ColumnName()
extern "C"  String_t* SchemaInfo_get_ColumnName_m457791784 (SchemaInfo_t609393215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.SchemaInfo::set_ColumnName(System.String)
extern "C"  void SchemaInfo_set_ColumnName_m3720976003 (SchemaInfo_t609393215 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.SchemaInfo::set_ColumnOrdinal(System.Int32)
extern "C"  void SchemaInfo_set_ColumnOrdinal_m4127700752 (SchemaInfo_t609393215 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Data.Common.SchemaInfo::get_DataTypeName()
extern "C"  String_t* SchemaInfo_get_DataTypeName_m3472988118 (SchemaInfo_t609393215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.SchemaInfo::set_DataTypeName(System.String)
extern "C"  void SchemaInfo_set_DataTypeName_m3776437333 (SchemaInfo_t609393215 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Data.Common.SchemaInfo::get_FieldType()
extern "C"  Type_t * SchemaInfo_get_FieldType_m835674918 (SchemaInfo_t609393215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Data.Common.SchemaInfo::set_FieldType(System.Type)
extern "C"  void SchemaInfo_set_FieldType_m3722079201 (SchemaInfo_t609393215 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
