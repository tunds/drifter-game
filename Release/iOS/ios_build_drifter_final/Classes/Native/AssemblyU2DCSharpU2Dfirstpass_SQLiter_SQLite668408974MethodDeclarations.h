﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLiter.SQLite
struct SQLite_t668408974;
// System.String
struct String_t;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4157799059;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SQLiter.SQLite::.ctor()
extern "C"  void SQLite__ctor_m2304424315 (SQLite_t668408974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::.cctor()
extern "C"  void SQLite__cctor_m2235580818 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::Awake()
extern "C"  void SQLite_Awake_m2542029534 (SQLite_t668408974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::Start()
extern "C"  void SQLite_Start_m1251562107 (SQLite_t668408974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::Test()
extern "C"  void SQLite_Test_m194232443 (SQLite_t668408974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::OnDestroy()
extern "C"  void SQLite_OnDestroy_m300767476 (SQLite_t668408974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::RunAsyncInit()
extern "C"  void SQLite_RunAsyncInit_m782527370 (SQLite_t668408974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::SQLiteInit()
extern "C"  void SQLite_SQLiteInit_m1750213285 (SQLite_t668408974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::InsertPlayer(System.String,System.Single)
extern "C"  void SQLite_InsertPlayer_m3837957028 (SQLite_t668408974 * __this, String_t* ___name0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::GetAllPlayers()
extern "C"  void SQLite_GetAllPlayers_m398303648 (SQLite_t668408974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::GetAllScores(UnityEngine.UI.Text[],UnityEngine.UI.Text[])
extern "C"  void SQLite_GetAllScores_m182890629 (SQLite_t668408974 * __this, TextU5BU5D_t4157799059* ___names0, TextU5BU5D_t4157799059* ___scores1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLiter.SQLite::QueryString(System.String,System.String)
extern "C"  String_t* SQLite_QueryString_m2260085071 (SQLite_t668408974 * __this, String_t* ___column0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLiter.SQLite::QueryInt(System.String,System.String)
extern "C"  int32_t SQLite_QueryInt_m843545816 (SQLite_t668408974 * __this, String_t* ___column0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 SQLiter.SQLite::QueryShort(System.String,System.String)
extern "C"  int16_t SQLite_QueryShort_m4232946353 (SQLite_t668408974 * __this, String_t* ___column0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::SetValue(System.String,System.Int32,System.String)
extern "C"  void SQLite_SetValue_m1031651177 (SQLite_t668408974 * __this, String_t* ___column0, int32_t ___value1, String_t* ___name2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::DeletePlayer(System.String)
extern "C"  void SQLite_DeletePlayer_m1440389965 (SQLite_t668408974 * __this, String_t* ___nameKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::ExecuteNonQuery(System.String)
extern "C"  void SQLite_ExecuteNonQuery_m2279874681 (SQLite_t668408974 * __this, String_t* ___commandText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::SQLiteClose()
extern "C"  void SQLite_SQLiteClose_m1635261157 (SQLite_t668408974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::<Test>m__2()
extern "C"  void SQLite_U3CTestU3Em__2_m1231375582 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLiter.SQLite::<RunAsyncInit>m__3()
extern "C"  void SQLite_U3CRunAsyncInitU3Em__3_m1263477488 (SQLite_t668408974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
