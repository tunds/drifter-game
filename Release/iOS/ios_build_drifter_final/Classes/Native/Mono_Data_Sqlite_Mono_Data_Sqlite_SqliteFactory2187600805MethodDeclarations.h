﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Data.Sqlite.SqliteFactory
struct SqliteFactory_t2187600805;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Data.Common.DbCommand
struct DbCommand_t2323745021;
// System.Data.Common.DbConnection
struct DbConnection_t462757452;
// System.Data.Common.DbDataAdapter
struct DbDataAdapter_t3684585719;
// System.Data.Common.DbParameter
struct DbParameter_t3306161371;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void Mono.Data.Sqlite.SqliteFactory::.ctor()
extern "C"  void SqliteFactory__ctor_m1608646429 (SqliteFactory_t2187600805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Data.Sqlite.SqliteFactory::.cctor()
extern "C"  void SqliteFactory__cctor_m2141302832 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteFactory::System.IServiceProvider.GetService(System.Type)
extern "C"  Il2CppObject * SqliteFactory_System_IServiceProvider_GetService_m2087608300 (SqliteFactory_t2187600805 * __this, Type_t * ___serviceType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbCommand Mono.Data.Sqlite.SqliteFactory::CreateCommand()
extern "C"  DbCommand_t2323745021 * SqliteFactory_CreateCommand_m2154662766 (SqliteFactory_t2187600805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbConnection Mono.Data.Sqlite.SqliteFactory::CreateConnection()
extern "C"  DbConnection_t462757452 * SqliteFactory_CreateConnection_m314598618 (SqliteFactory_t2187600805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbDataAdapter Mono.Data.Sqlite.SqliteFactory::CreateDataAdapter()
extern "C"  DbDataAdapter_t3684585719 * SqliteFactory_CreateDataAdapter_m2708142306 (SqliteFactory_t2187600805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Data.Common.DbParameter Mono.Data.Sqlite.SqliteFactory::CreateParameter()
extern "C"  DbParameter_t3306161371 * SqliteFactory_CreateParameter_m675385002 (SqliteFactory_t2187600805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Data.Sqlite.SqliteFactory::GetSQLiteProviderServicesInstance()
extern "C"  Il2CppObject * SqliteFactory_GetSQLiteProviderServicesInstance_m2990997868 (SqliteFactory_t2187600805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
