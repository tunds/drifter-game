﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct List_1_t3271763293;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLiteDatabase.DBReader
struct  DBReader_t4220400158  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>> SQLiteDatabase.DBReader::dataTable
	List_1_t3271763293 * ___dataTable_0;
	// System.Int16 SQLiteDatabase.DBReader::rowCounter
	int16_t ___rowCounter_1;

public:
	inline static int32_t get_offset_of_dataTable_0() { return static_cast<int32_t>(offsetof(DBReader_t4220400158, ___dataTable_0)); }
	inline List_1_t3271763293 * get_dataTable_0() const { return ___dataTable_0; }
	inline List_1_t3271763293 ** get_address_of_dataTable_0() { return &___dataTable_0; }
	inline void set_dataTable_0(List_1_t3271763293 * value)
	{
		___dataTable_0 = value;
		Il2CppCodeGenWriteBarrier(&___dataTable_0, value);
	}

	inline static int32_t get_offset_of_rowCounter_1() { return static_cast<int32_t>(offsetof(DBReader_t4220400158, ___rowCounter_1)); }
	inline int16_t get_rowCounter_1() const { return ___rowCounter_1; }
	inline int16_t* get_address_of_rowCounter_1() { return &___rowCounter_1; }
	inline void set_rowCounter_1(int16_t value)
	{
		___rowCounter_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
