﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mono.Data.Sqlite.SQLiteEnlistment
struct SQLiteEnlistment_t56717400;
// Mono.Data.Sqlite.SQLiteBase
struct SQLiteBase_t3947283844;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// Mono.Data.Sqlite.SQLiteUpdateCallback
struct SQLiteUpdateCallback_t2915972065;
// Mono.Data.Sqlite.SQLiteCommitCallback
struct SQLiteCommitCallback_t1317328463;
// Mono.Data.Sqlite.SQLiteRollbackCallback
struct SQLiteRollbackCallback_t4255779836;
// Mono.Data.Sqlite.SQLiteUpdateEventHandler
struct SQLiteUpdateEventHandler_t1851894892;
// Mono.Data.Sqlite.SQLiteCommitHandler
struct SQLiteCommitHandler_t1041285550;
// System.EventHandler
struct EventHandler_t247020293;
// System.Data.StateChangeEventHandler
struct StateChangeEventHandler_t2980893476;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "System_Data_System_Data_Common_DbConnection462757452.h"
#include "System_Data_System_Data_ConnectionState270608870.h"
#include "System_Data_System_Data_IsolationLevel3729656617.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Data.Sqlite.SqliteConnection
struct  SqliteConnection_t3853176977  : public DbConnection_t462757452
{
public:
	// System.Data.ConnectionState Mono.Data.Sqlite.SqliteConnection::_connectionState
	int32_t ____connectionState_9;
	// System.String Mono.Data.Sqlite.SqliteConnection::_connectionString
	String_t* ____connectionString_10;
	// System.Int32 Mono.Data.Sqlite.SqliteConnection::_transactionLevel
	int32_t ____transactionLevel_11;
	// System.Data.IsolationLevel Mono.Data.Sqlite.SqliteConnection::_defaultIsolation
	int32_t ____defaultIsolation_12;
	// Mono.Data.Sqlite.SQLiteEnlistment Mono.Data.Sqlite.SqliteConnection::_enlistment
	SQLiteEnlistment_t56717400 * ____enlistment_13;
	// Mono.Data.Sqlite.SQLiteBase Mono.Data.Sqlite.SqliteConnection::_sql
	SQLiteBase_t3947283844 * ____sql_14;
	// System.String Mono.Data.Sqlite.SqliteConnection::_dataSource
	String_t* ____dataSource_15;
	// System.Byte[] Mono.Data.Sqlite.SqliteConnection::_password
	ByteU5BU5D_t58506160* ____password_16;
	// System.Int32 Mono.Data.Sqlite.SqliteConnection::_defaultTimeout
	int32_t ____defaultTimeout_17;
	// System.Boolean Mono.Data.Sqlite.SqliteConnection::_binaryGuid
	bool ____binaryGuid_18;
	// System.Int64 Mono.Data.Sqlite.SqliteConnection::_version
	int64_t ____version_19;
	// Mono.Data.Sqlite.SQLiteUpdateCallback Mono.Data.Sqlite.SqliteConnection::_updateCallback
	SQLiteUpdateCallback_t2915972065 * ____updateCallback_20;
	// Mono.Data.Sqlite.SQLiteCommitCallback Mono.Data.Sqlite.SqliteConnection::_commitCallback
	SQLiteCommitCallback_t1317328463 * ____commitCallback_21;
	// Mono.Data.Sqlite.SQLiteRollbackCallback Mono.Data.Sqlite.SqliteConnection::_rollbackCallback
	SQLiteRollbackCallback_t4255779836 * ____rollbackCallback_22;
	// Mono.Data.Sqlite.SQLiteUpdateEventHandler Mono.Data.Sqlite.SqliteConnection::_updateHandler
	SQLiteUpdateEventHandler_t1851894892 * ____updateHandler_23;
	// Mono.Data.Sqlite.SQLiteCommitHandler Mono.Data.Sqlite.SqliteConnection::_commitHandler
	SQLiteCommitHandler_t1041285550 * ____commitHandler_24;
	// System.EventHandler Mono.Data.Sqlite.SqliteConnection::_rollbackHandler
	EventHandler_t247020293 * ____rollbackHandler_25;
	// System.Data.StateChangeEventHandler Mono.Data.Sqlite.SqliteConnection::StateChange
	StateChangeEventHandler_t2980893476 * ___StateChange_26;

public:
	inline static int32_t get_offset_of__connectionState_9() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____connectionState_9)); }
	inline int32_t get__connectionState_9() const { return ____connectionState_9; }
	inline int32_t* get_address_of__connectionState_9() { return &____connectionState_9; }
	inline void set__connectionState_9(int32_t value)
	{
		____connectionState_9 = value;
	}

	inline static int32_t get_offset_of__connectionString_10() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____connectionString_10)); }
	inline String_t* get__connectionString_10() const { return ____connectionString_10; }
	inline String_t** get_address_of__connectionString_10() { return &____connectionString_10; }
	inline void set__connectionString_10(String_t* value)
	{
		____connectionString_10 = value;
		Il2CppCodeGenWriteBarrier(&____connectionString_10, value);
	}

	inline static int32_t get_offset_of__transactionLevel_11() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____transactionLevel_11)); }
	inline int32_t get__transactionLevel_11() const { return ____transactionLevel_11; }
	inline int32_t* get_address_of__transactionLevel_11() { return &____transactionLevel_11; }
	inline void set__transactionLevel_11(int32_t value)
	{
		____transactionLevel_11 = value;
	}

	inline static int32_t get_offset_of__defaultIsolation_12() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____defaultIsolation_12)); }
	inline int32_t get__defaultIsolation_12() const { return ____defaultIsolation_12; }
	inline int32_t* get_address_of__defaultIsolation_12() { return &____defaultIsolation_12; }
	inline void set__defaultIsolation_12(int32_t value)
	{
		____defaultIsolation_12 = value;
	}

	inline static int32_t get_offset_of__enlistment_13() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____enlistment_13)); }
	inline SQLiteEnlistment_t56717400 * get__enlistment_13() const { return ____enlistment_13; }
	inline SQLiteEnlistment_t56717400 ** get_address_of__enlistment_13() { return &____enlistment_13; }
	inline void set__enlistment_13(SQLiteEnlistment_t56717400 * value)
	{
		____enlistment_13 = value;
		Il2CppCodeGenWriteBarrier(&____enlistment_13, value);
	}

	inline static int32_t get_offset_of__sql_14() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____sql_14)); }
	inline SQLiteBase_t3947283844 * get__sql_14() const { return ____sql_14; }
	inline SQLiteBase_t3947283844 ** get_address_of__sql_14() { return &____sql_14; }
	inline void set__sql_14(SQLiteBase_t3947283844 * value)
	{
		____sql_14 = value;
		Il2CppCodeGenWriteBarrier(&____sql_14, value);
	}

	inline static int32_t get_offset_of__dataSource_15() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____dataSource_15)); }
	inline String_t* get__dataSource_15() const { return ____dataSource_15; }
	inline String_t** get_address_of__dataSource_15() { return &____dataSource_15; }
	inline void set__dataSource_15(String_t* value)
	{
		____dataSource_15 = value;
		Il2CppCodeGenWriteBarrier(&____dataSource_15, value);
	}

	inline static int32_t get_offset_of__password_16() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____password_16)); }
	inline ByteU5BU5D_t58506160* get__password_16() const { return ____password_16; }
	inline ByteU5BU5D_t58506160** get_address_of__password_16() { return &____password_16; }
	inline void set__password_16(ByteU5BU5D_t58506160* value)
	{
		____password_16 = value;
		Il2CppCodeGenWriteBarrier(&____password_16, value);
	}

	inline static int32_t get_offset_of__defaultTimeout_17() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____defaultTimeout_17)); }
	inline int32_t get__defaultTimeout_17() const { return ____defaultTimeout_17; }
	inline int32_t* get_address_of__defaultTimeout_17() { return &____defaultTimeout_17; }
	inline void set__defaultTimeout_17(int32_t value)
	{
		____defaultTimeout_17 = value;
	}

	inline static int32_t get_offset_of__binaryGuid_18() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____binaryGuid_18)); }
	inline bool get__binaryGuid_18() const { return ____binaryGuid_18; }
	inline bool* get_address_of__binaryGuid_18() { return &____binaryGuid_18; }
	inline void set__binaryGuid_18(bool value)
	{
		____binaryGuid_18 = value;
	}

	inline static int32_t get_offset_of__version_19() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____version_19)); }
	inline int64_t get__version_19() const { return ____version_19; }
	inline int64_t* get_address_of__version_19() { return &____version_19; }
	inline void set__version_19(int64_t value)
	{
		____version_19 = value;
	}

	inline static int32_t get_offset_of__updateCallback_20() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____updateCallback_20)); }
	inline SQLiteUpdateCallback_t2915972065 * get__updateCallback_20() const { return ____updateCallback_20; }
	inline SQLiteUpdateCallback_t2915972065 ** get_address_of__updateCallback_20() { return &____updateCallback_20; }
	inline void set__updateCallback_20(SQLiteUpdateCallback_t2915972065 * value)
	{
		____updateCallback_20 = value;
		Il2CppCodeGenWriteBarrier(&____updateCallback_20, value);
	}

	inline static int32_t get_offset_of__commitCallback_21() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____commitCallback_21)); }
	inline SQLiteCommitCallback_t1317328463 * get__commitCallback_21() const { return ____commitCallback_21; }
	inline SQLiteCommitCallback_t1317328463 ** get_address_of__commitCallback_21() { return &____commitCallback_21; }
	inline void set__commitCallback_21(SQLiteCommitCallback_t1317328463 * value)
	{
		____commitCallback_21 = value;
		Il2CppCodeGenWriteBarrier(&____commitCallback_21, value);
	}

	inline static int32_t get_offset_of__rollbackCallback_22() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____rollbackCallback_22)); }
	inline SQLiteRollbackCallback_t4255779836 * get__rollbackCallback_22() const { return ____rollbackCallback_22; }
	inline SQLiteRollbackCallback_t4255779836 ** get_address_of__rollbackCallback_22() { return &____rollbackCallback_22; }
	inline void set__rollbackCallback_22(SQLiteRollbackCallback_t4255779836 * value)
	{
		____rollbackCallback_22 = value;
		Il2CppCodeGenWriteBarrier(&____rollbackCallback_22, value);
	}

	inline static int32_t get_offset_of__updateHandler_23() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____updateHandler_23)); }
	inline SQLiteUpdateEventHandler_t1851894892 * get__updateHandler_23() const { return ____updateHandler_23; }
	inline SQLiteUpdateEventHandler_t1851894892 ** get_address_of__updateHandler_23() { return &____updateHandler_23; }
	inline void set__updateHandler_23(SQLiteUpdateEventHandler_t1851894892 * value)
	{
		____updateHandler_23 = value;
		Il2CppCodeGenWriteBarrier(&____updateHandler_23, value);
	}

	inline static int32_t get_offset_of__commitHandler_24() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____commitHandler_24)); }
	inline SQLiteCommitHandler_t1041285550 * get__commitHandler_24() const { return ____commitHandler_24; }
	inline SQLiteCommitHandler_t1041285550 ** get_address_of__commitHandler_24() { return &____commitHandler_24; }
	inline void set__commitHandler_24(SQLiteCommitHandler_t1041285550 * value)
	{
		____commitHandler_24 = value;
		Il2CppCodeGenWriteBarrier(&____commitHandler_24, value);
	}

	inline static int32_t get_offset_of__rollbackHandler_25() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ____rollbackHandler_25)); }
	inline EventHandler_t247020293 * get__rollbackHandler_25() const { return ____rollbackHandler_25; }
	inline EventHandler_t247020293 ** get_address_of__rollbackHandler_25() { return &____rollbackHandler_25; }
	inline void set__rollbackHandler_25(EventHandler_t247020293 * value)
	{
		____rollbackHandler_25 = value;
		Il2CppCodeGenWriteBarrier(&____rollbackHandler_25, value);
	}

	inline static int32_t get_offset_of_StateChange_26() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977, ___StateChange_26)); }
	inline StateChangeEventHandler_t2980893476 * get_StateChange_26() const { return ___StateChange_26; }
	inline StateChangeEventHandler_t2980893476 ** get_address_of_StateChange_26() { return &___StateChange_26; }
	inline void set_StateChange_26(StateChangeEventHandler_t2980893476 * value)
	{
		___StateChange_26 = value;
		Il2CppCodeGenWriteBarrier(&___StateChange_26, value);
	}
};

struct SqliteConnection_t3853176977_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Data.Sqlite.SqliteConnection::<>f__switch$map1
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map1_27;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Data.Sqlite.SqliteConnection::<>f__switch$map2
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map2_28;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_27() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977_StaticFields, ___U3CU3Ef__switchU24map1_27)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map1_27() const { return ___U3CU3Ef__switchU24map1_27; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map1_27() { return &___U3CU3Ef__switchU24map1_27; }
	inline void set_U3CU3Ef__switchU24map1_27(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map1_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_27, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_28() { return static_cast<int32_t>(offsetof(SqliteConnection_t3853176977_StaticFields, ___U3CU3Ef__switchU24map2_28)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map2_28() const { return ___U3CU3Ef__switchU24map2_28; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map2_28() { return &___U3CU3Ef__switchU24map2_28; }
	inline void set_U3CU3Ef__switchU24map2_28(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map2_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
