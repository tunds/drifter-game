﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType4014882752.h"
#include "LocalDB_SQLiteDatabase_SQLiteDB_DB_DataType4220602565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLiteDatabase.SQLiteDB/DB_Field
struct  DB_Field_t3780330969 
{
public:
	// System.String SQLiteDatabase.SQLiteDB/DB_Field::name
	String_t* ___name_0;
	// SQLiteDatabase.SQLiteDB/DB_DataType SQLiteDatabase.SQLiteDB/DB_Field::type
	int32_t ___type_1;
	// System.Int32 SQLiteDatabase.SQLiteDB/DB_Field::size
	int32_t ___size_2;
	// System.Boolean SQLiteDatabase.SQLiteDB/DB_Field::isPrimaryKey
	bool ___isPrimaryKey_3;
	// System.Boolean SQLiteDatabase.SQLiteDB/DB_Field::isNotNull
	bool ___isNotNull_4;
	// System.Boolean SQLiteDatabase.SQLiteDB/DB_Field::isUnique
	bool ___isUnique_5;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DB_Field_t3780330969, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(DB_Field_t3780330969, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(DB_Field_t3780330969, ___size_2)); }
	inline int32_t get_size_2() const { return ___size_2; }
	inline int32_t* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(int32_t value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of_isPrimaryKey_3() { return static_cast<int32_t>(offsetof(DB_Field_t3780330969, ___isPrimaryKey_3)); }
	inline bool get_isPrimaryKey_3() const { return ___isPrimaryKey_3; }
	inline bool* get_address_of_isPrimaryKey_3() { return &___isPrimaryKey_3; }
	inline void set_isPrimaryKey_3(bool value)
	{
		___isPrimaryKey_3 = value;
	}

	inline static int32_t get_offset_of_isNotNull_4() { return static_cast<int32_t>(offsetof(DB_Field_t3780330969, ___isNotNull_4)); }
	inline bool get_isNotNull_4() const { return ___isNotNull_4; }
	inline bool* get_address_of_isNotNull_4() { return &___isNotNull_4; }
	inline void set_isNotNull_4(bool value)
	{
		___isNotNull_4 = value;
	}

	inline static int32_t get_offset_of_isUnique_5() { return static_cast<int32_t>(offsetof(DB_Field_t3780330969, ___isUnique_5)); }
	inline bool get_isUnique_5() const { return ___isUnique_5; }
	inline bool* get_address_of_isUnique_5() { return &___isUnique_5; }
	inline void set_isUnique_5(bool value)
	{
		___isUnique_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: SQLiteDatabase.SQLiteDB/DB_Field
struct DB_Field_t3780330969_marshaled_pinvoke
{
	char* ___name_0;
	int32_t ___type_1;
	int32_t ___size_2;
	int32_t ___isPrimaryKey_3;
	int32_t ___isNotNull_4;
	int32_t ___isUnique_5;
};
// Native definition for marshalling of: SQLiteDatabase.SQLiteDB/DB_Field
struct DB_Field_t3780330969_marshaled_com
{
	uint16_t* ___name_0;
	int32_t ___type_1;
	int32_t ___size_2;
	int32_t ___isPrimaryKey_3;
	int32_t ___isNotNull_4;
	int32_t ___isUnique_5;
};
