﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Xml.XmlUrlResolver
struct XmlUrlResolver_t215921638;
// System.Object
struct Il2CppObject;
// System.Uri
struct Uri_t2776692961;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Xml.XmlValidatingReader
struct XmlValidatingReader_t2921250517;
// System.Xml.XmlReader
struct XmlReader_t4229084514;
// System.Xml.XmlParserContext
struct XmlParserContext_t3629084577;
// System.Xml.XmlNameTable
struct XmlNameTable_t3232213908;
// System.Xml.XmlResolver
struct XmlResolver_t2502213349;
// System.Xml.Schema.XmlSchemaCollection
struct XmlSchemaCollection_t1190777955;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t3693321125;
// System.Xml.Schema.ValidationEventArgs
struct ValidationEventArgs_t1700465963;
// System.Xml.XmlWhitespace
struct XmlWhitespace_t2187161500;
// System.Xml.XmlDocument
struct XmlDocument_t3705263098;
// System.Xml.XmlNode
struct XmlNode_t3592213601;
// System.Xml.XmlWriter
struct XmlWriter_t89522450;
// System.Xml.XmlWriterSettings
struct XmlWriterSettings_t3992467285;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Xml_System_Xml_XmlTokenizedType1796599028.h"
#include "System_Xml_System_Xml_XmlTokenizedType1796599028MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlUrlResolver215921638.h"
#include "System_Xml_System_Xml_XmlUrlResolver215921638MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "System_Xml_System_Xml_XmlResolver2502213349MethodDeclarations.h"
#include "System_System_Uri2776692961.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlException3490696160MethodDeclarations.h"
#include "System_System_Uri2776692961MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_IO_FileStream1527309539MethodDeclarations.h"
#include "System_System_Net_WebRequest3488810021MethodDeclarations.h"
#include "System_System_Net_WebRequest3488810021.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "System_Xml_System_Xml_XmlException3490696160.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_ArgumentException124305799.h"
#include "mscorlib_System_IO_FileStream1527309539.h"
#include "mscorlib_System_IO_FileMode1356058118.h"
#include "mscorlib_System_IO_FileAccess995838663.h"
#include "mscorlib_System_IO_FileShare2703391818.h"
#include "System_System_Net_WebResponse2411292415.h"
#include "System_System_Net_WebResponse2411292415MethodDeclarations.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "System_Xml_System_Xml_XmlResolver2502213349.h"
#include "System_Xml_System_Xml_XmlValidatingReader2921250517.h"
#include "System_Xml_System_Xml_XmlValidatingReader2921250517MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlReader4229084514.h"
#include "System_Xml_System_Xml_XmlReader4229084514MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlTextReader3719122287.h"
#include "System_Xml_System_Xml_EntityHandling2614841296.h"
#include "System_Xml_System_Xml_ValidationType2591842619.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "System_Xml_System_Xml_XmlParserContext3629084577.h"
#include "System_Xml_Mono_Xml_DTDValidatingReader2954095246.h"
#include "System_Xml_Mono_Xml_DTDValidatingReader2954095246MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "System_Xml_System_Xml_XmlTextReader3719122287MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNameTable3232213908.h"
#include "System_Xml_System_Xml_XmlNodeType3966624571.h"
#include "System_Xml_System_Xml_ReadState457987651.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCollection1190777955.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCollection1190777955MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlReaderSettings3693321125.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "System_Xml_System_Xml_XmlSpace3747690775.h"
#include "mscorlib_System_IndexOutOfRangeException3760259642MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3760259642.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidatingReader1549156720MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_ValidationEventHandle2777264566MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidatingReader1549156720.h"
#include "System_Xml_System_Xml_Schema_ValidationEventHandle2777264566.h"
#include "System_Xml_System_Xml_Schema_ValidationEventArgs1700465963.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSet3827173367.h"
#include "System_Xml_System_Xml_Schema_ValidationEventArgs1700465963MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSeverityType564452507.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaException2660249060.h"
#include "System_Xml_System_Xml_XmlWhitespace2187161500.h"
#include "System_Xml_System_Xml_XmlWhitespace2187161500MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlDocument3705263098.h"
#include "System_Xml_System_Xml_XmlCharacterData3253358980MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlCharacterData3253358980.h"
#include "System_Xml_System_Xml_XmlChar3591879093MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNode3592213601.h"
#include "System_Xml_System_Xml_XmlNode3592213601MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlWriter89522450.h"
#include "System_Xml_System_Xml_XmlWriter89522450MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlWriterSettings3992467285.h"
#include "System_Xml_System_Xml_XmlWriterSettings3992467285MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlConvert1882388356MethodDeclarations.h"
#include "System_Xml_System_Xml_ConformanceLevel2571527895.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Text_Encoding180559927MethodDeclarations.h"
#include "mscorlib_System_Environment63604104MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding180559927.h"
#include "System_Xml_System_Xml_NewLineHandling406254689.h"
#include "System_Xml_System_Xml_XmlOutputMethod2348413089.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlUrlResolver::.ctor()
extern "C"  void XmlUrlResolver__ctor_m2727795693 (XmlUrlResolver_t215921638 * __this, const MethodInfo* method)
{
	{
		XmlResolver__ctor_m2007000382(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XmlUrlResolver::GetEntity(System.Uri,System.String,System.Type)
extern const Il2CppType* Stream_t219029575_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FileStream_t1527309539_il2cpp_TypeInfo_var;
extern Il2CppClass* WebRequest_t3488810021_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2985384732;
extern Il2CppCodeGenString* _stringLiteral2115073277;
extern Il2CppCodeGenString* _stringLiteral3011459189;
extern Il2CppCodeGenString* _stringLiteral3143036;
extern const uint32_t XmlUrlResolver_GetEntity_m203780859_MetadataUsageId;
extern "C"  Il2CppObject * XmlUrlResolver_GetEntity_m203780859 (XmlUrlResolver_t215921638 * __this, Uri_t2776692961 * ___absoluteUri0, String_t* ___role1, Type_t * ___ofObjectToReturn2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlUrlResolver_GetEntity_m203780859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WebRequest_t3488810021 * V_0 = NULL;
	{
		Type_t * L_0 = ___ofObjectToReturn2;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Stream_t219029575_0_0_0_var), /*hidden argument*/NULL);
		___ofObjectToReturn2 = L_1;
	}

IL_0012:
	{
		Type_t * L_2 = ___ofObjectToReturn2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Stream_t219029575_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_2) == ((Il2CppObject*)(Type_t *)L_3)))
		{
			goto IL_002d;
		}
	}
	{
		XmlException_t3490696160 * L_4 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m3226581679(L_4, _stringLiteral2985384732, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_002d:
	{
		Uri_t2776692961 * L_5 = ___absoluteUri0;
		NullCheck(L_5);
		bool L_6 = Uri_get_IsAbsoluteUri_m2228437936(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0048;
		}
	}
	{
		ArgumentException_t124305799 * L_7 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_7, _stringLiteral2115073277, _stringLiteral3011459189, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0048:
	{
		Uri_t2776692961 * L_8 = ___absoluteUri0;
		NullCheck(L_8);
		String_t* L_9 = Uri_get_Scheme_m1248371453(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_9, _stringLiteral3143036, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0097;
		}
	}
	{
		Uri_t2776692961 * L_11 = ___absoluteUri0;
		NullCheck(L_11);
		String_t* L_12 = Uri_get_AbsolutePath_m1170417812(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_14 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		ArgumentException_t124305799 * L_15 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_15, _stringLiteral2115073277, _stringLiteral3011459189, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0082:
	{
		Uri_t2776692961 * L_16 = ___absoluteUri0;
		NullCheck(L_16);
		String_t* L_17 = Uri_get_LocalPath_m2374994490(L_16, /*hidden argument*/NULL);
		String_t* L_18 = XmlUrlResolver_UnescapeRelativeUriBody_m764219404(__this, L_17, /*hidden argument*/NULL);
		FileStream_t1527309539 * L_19 = (FileStream_t1527309539 *)il2cpp_codegen_object_new(FileStream_t1527309539_il2cpp_TypeInfo_var);
		FileStream__ctor_m3657053030(L_19, L_18, 3, 1, 1, /*hidden argument*/NULL);
		return L_19;
	}

IL_0097:
	{
		Uri_t2776692961 * L_20 = ___absoluteUri0;
		IL2CPP_RUNTIME_CLASS_INIT(WebRequest_t3488810021_il2cpp_TypeInfo_var);
		WebRequest_t3488810021 * L_21 = WebRequest_Create_m1770286229(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		Il2CppObject * L_22 = __this->get_credential_0();
		if (!L_22)
		{
			goto IL_00b5;
		}
	}
	{
		WebRequest_t3488810021 * L_23 = V_0;
		Il2CppObject * L_24 = __this->get_credential_0();
		NullCheck(L_23);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void System.Net.WebRequest::set_Credentials(System.Net.ICredentials) */, L_23, L_24);
	}

IL_00b5:
	{
		WebRequest_t3488810021 * L_25 = V_0;
		NullCheck(L_25);
		WebResponse_t2411292415 * L_26 = VirtFuncInvoker0< WebResponse_t2411292415 * >::Invoke(17 /* System.Net.WebResponse System.Net.WebRequest::GetResponse() */, L_25);
		NullCheck(L_26);
		Stream_t219029575 * L_27 = VirtFuncInvoker0< Stream_t219029575 * >::Invoke(10 /* System.IO.Stream System.Net.WebResponse::GetResponseStream() */, L_26);
		return L_27;
	}
}
// System.Uri System.Xml.XmlUrlResolver::ResolveUri(System.Uri,System.String)
extern "C"  Uri_t2776692961 * XmlUrlResolver_ResolveUri_m569585666 (XmlUrlResolver_t215921638 * __this, Uri_t2776692961 * ___baseUri0, String_t* ___relativeUri1, const MethodInfo* method)
{
	{
		Uri_t2776692961 * L_0 = ___baseUri0;
		String_t* L_1 = ___relativeUri1;
		Uri_t2776692961 * L_2 = XmlResolver_ResolveUri_m1762389849(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String System.Xml.XmlUrlResolver::UnescapeRelativeUriBody(System.String)
extern Il2CppCodeGenString* _stringLiteral37205;
extern Il2CppCodeGenString* _stringLiteral60;
extern Il2CppCodeGenString* _stringLiteral37207;
extern Il2CppCodeGenString* _stringLiteral62;
extern Il2CppCodeGenString* _stringLiteral37158;
extern Il2CppCodeGenString* _stringLiteral35;
extern Il2CppCodeGenString* _stringLiteral37157;
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral37155;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral37160;
extern Il2CppCodeGenString* _stringLiteral37;
extern const uint32_t XmlUrlResolver_UnescapeRelativeUriBody_m764219404_MetadataUsageId;
extern "C"  String_t* XmlUrlResolver_UnescapeRelativeUriBody_m764219404 (XmlUrlResolver_t215921638 * __this, String_t* ___src0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlUrlResolver_UnescapeRelativeUriBody_m764219404_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___src0;
		NullCheck(L_0);
		String_t* L_1 = String_Replace_m2915759397(L_0, _stringLiteral37205, _stringLiteral60, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = String_Replace_m2915759397(L_1, _stringLiteral37207, _stringLiteral62, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = String_Replace_m2915759397(L_2, _stringLiteral37158, _stringLiteral35, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = String_Replace_m2915759397(L_3, _stringLiteral37157, _stringLiteral34, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = String_Replace_m2915759397(L_4, _stringLiteral37155, _stringLiteral32, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = String_Replace_m2915759397(L_5, _stringLiteral37160, _stringLiteral37, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void System.Xml.XmlValidatingReader::.ctor(System.Xml.XmlReader)
extern Il2CppClass* XmlTextReader_t3719122287_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlUrlResolver_t215921638_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern const uint32_t XmlValidatingReader__ctor_m3012717130_MetadataUsageId;
extern "C"  void XmlValidatingReader__ctor_m3012717130 (XmlValidatingReader_t2921250517 * __this, XmlReader_t4229084514 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader__ctor_m3012717130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlReader__ctor_m2228312417(__this, /*hidden argument*/NULL);
		XmlReader_t4229084514 * L_0 = ___reader0;
		__this->set_sourceReader_4(L_0);
		XmlReader_t4229084514 * L_1 = ___reader0;
		__this->set_xmlTextReader_5(((XmlTextReader_t3719122287 *)IsInstClass(L_1, XmlTextReader_t3719122287_il2cpp_TypeInfo_var)));
		XmlTextReader_t3719122287 * L_2 = __this->get_xmlTextReader_5();
		if (L_2)
		{
			goto IL_002f;
		}
	}
	{
		XmlUrlResolver_t215921638 * L_3 = (XmlUrlResolver_t215921638 *)il2cpp_codegen_object_new(XmlUrlResolver_t215921638_il2cpp_TypeInfo_var);
		XmlUrlResolver__ctor_m2727795693(L_3, /*hidden argument*/NULL);
		__this->set_resolver_7(L_3);
	}

IL_002f:
	{
		__this->set_entityHandling_3(1);
		__this->set_validationType_9(1);
		StringBuilder_t3822575854 * L_4 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_4, /*hidden argument*/NULL);
		__this->set_storedCharacters_13(L_4);
		return;
	}
}
// System.Xml.XmlParserContext System.Xml.XmlValidatingReader::Mono.Xml.IHasXmlParserContext.get_ParserContext()
extern Il2CppClass* IHasXmlParserContext_t1426393902_il2cpp_TypeInfo_var;
extern const uint32_t XmlValidatingReader_Mono_Xml_IHasXmlParserContext_get_ParserContext_m2323898669_MetadataUsageId;
extern "C"  XmlParserContext_t3629084577 * XmlValidatingReader_Mono_Xml_IHasXmlParserContext_get_ParserContext_m2323898669 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_Mono_Xml_IHasXmlParserContext_get_ParserContext_m2323898669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	XmlParserContext_t3629084577 * G_B5_0 = NULL;
	{
		DTDValidatingReader_t2954095246 * L_0 = __this->get_dtdReader_11();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		DTDValidatingReader_t2954095246 * L_1 = __this->get_dtdReader_11();
		NullCheck(L_1);
		XmlParserContext_t3629084577 * L_2 = VirtFuncInvoker0< XmlParserContext_t3629084577 * >::Invoke(53 /* System.Xml.XmlParserContext Mono.Xml.DTDValidatingReader::get_ParserContext() */, L_1);
		return L_2;
	}

IL_0017:
	{
		XmlReader_t4229084514 * L_3 = __this->get_sourceReader_4();
		V_0 = ((Il2CppObject *)IsInst(L_3, IHasXmlParserContext_t1426393902_il2cpp_TypeInfo_var));
		Il2CppObject * L_4 = V_0;
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Il2CppObject * L_5 = V_0;
		NullCheck(L_5);
		XmlParserContext_t3629084577 * L_6 = InterfaceFuncInvoker0< XmlParserContext_t3629084577 * >::Invoke(0 /* System.Xml.XmlParserContext Mono.Xml.IHasXmlParserContext::get_ParserContext() */, IHasXmlParserContext_t1426393902_il2cpp_TypeInfo_var, L_5);
		G_B5_0 = L_6;
		goto IL_0035;
	}

IL_0034:
	{
		G_B5_0 = ((XmlParserContext_t3629084577 *)(NULL));
	}

IL_0035:
	{
		return G_B5_0;
	}
}
// System.Int32 System.Xml.XmlValidatingReader::get_AttributeCount()
extern "C"  int32_t XmlValidatingReader_get_AttributeCount_m3477386036 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001c;
	}

IL_0011:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Xml.XmlReader::get_AttributeCount() */, L_1);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.String System.Xml.XmlValidatingReader::get_BaseURI()
extern "C"  String_t* XmlValidatingReader_get_BaseURI_m1328757979 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	String_t* G_B3_0 = NULL;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		XmlReader_t4229084514 * L_1 = __this->get_sourceReader_4();
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Xml.XmlReader::get_BaseURI() */, L_1);
		G_B3_0 = L_2;
		goto IL_0026;
	}

IL_001b:
	{
		XmlReader_t4229084514 * L_3 = __this->get_validatingReader_6();
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Xml.XmlReader::get_BaseURI() */, L_3);
		G_B3_0 = L_4;
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XmlValidatingReader::get_CanResolveEntity()
extern "C"  bool XmlValidatingReader_get_CanResolveEntity_m975828230 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Int32 System.Xml.XmlValidatingReader::get_Depth()
extern "C"  int32_t XmlValidatingReader_get_Depth_m1005799396 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001c;
	}

IL_0011:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XmlReader::get_Depth() */, L_1);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Xml.EntityHandling System.Xml.XmlValidatingReader::get_EntityHandling()
extern "C"  int32_t XmlValidatingReader_get_EntityHandling_m1995197276 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_entityHandling_3();
		return L_0;
	}
}
// System.Void System.Xml.XmlValidatingReader::set_EntityHandling(System.Xml.EntityHandling)
extern "C"  void XmlValidatingReader_set_EntityHandling_m2196103129 (XmlValidatingReader_t2921250517 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_entityHandling_3(L_0);
		DTDValidatingReader_t2954095246 * L_1 = __this->get_dtdReader_11();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		DTDValidatingReader_t2954095246 * L_2 = __this->get_dtdReader_11();
		int32_t L_3 = ___value0;
		NullCheck(L_2);
		DTDValidatingReader_set_EntityHandling_m346929096(L_2, L_3, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Boolean System.Xml.XmlValidatingReader::get_EOF()
extern "C"  bool XmlValidatingReader_get_EOF_m1415237751 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001c;
	}

IL_0011:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean System.Xml.XmlReader::get_EOF() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XmlValidatingReader::get_HasValue()
extern "C"  bool XmlValidatingReader_get_HasValue_m2186250494 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001c;
	}

IL_0011:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XmlReader::get_HasValue() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XmlValidatingReader::get_IsDefault()
extern "C"  bool XmlValidatingReader_get_IsDefault_m1561305842 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001c;
	}

IL_0011:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(13 /* System.Boolean System.Xml.XmlReader::get_IsDefault() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XmlValidatingReader::get_IsEmptyElement()
extern "C"  bool XmlValidatingReader_get_IsEmptyElement_m1116878912 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001c;
	}

IL_0011:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XmlReader::get_IsEmptyElement() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Int32 System.Xml.XmlValidatingReader::get_LineNumber()
extern Il2CppClass* IXmlLineInfo_t570005944_il2cpp_TypeInfo_var;
extern const uint32_t XmlValidatingReader_get_LineNumber_m3214672190_MetadataUsageId;
extern "C"  int32_t XmlValidatingReader_get_LineNumber_m3214672190 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_get_LineNumber_m3214672190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t G_B5_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(13 /* System.Boolean System.Xml.XmlValidatingReader::get_IsDefault() */, __this);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		V_0 = ((Il2CppObject *)IsInst(L_1, IXmlLineInfo_t570005944_il2cpp_TypeInfo_var));
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Xml.IXmlLineInfo::get_LineNumber() */, IXmlLineInfo_t570005944_il2cpp_TypeInfo_var, L_3);
		G_B5_0 = L_4;
		goto IL_002b;
	}

IL_002a:
	{
		G_B5_0 = 0;
	}

IL_002b:
	{
		return G_B5_0;
	}
}
// System.Int32 System.Xml.XmlValidatingReader::get_LinePosition()
extern Il2CppClass* IXmlLineInfo_t570005944_il2cpp_TypeInfo_var;
extern const uint32_t XmlValidatingReader_get_LinePosition_m2993080862_MetadataUsageId;
extern "C"  int32_t XmlValidatingReader_get_LinePosition_m2993080862 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_get_LinePosition_m2993080862_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t G_B5_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(13 /* System.Boolean System.Xml.XmlValidatingReader::get_IsDefault() */, __this);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		V_0 = ((Il2CppObject *)IsInst(L_1, IXmlLineInfo_t570005944_il2cpp_TypeInfo_var));
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 System.Xml.IXmlLineInfo::get_LinePosition() */, IXmlLineInfo_t570005944_il2cpp_TypeInfo_var, L_3);
		G_B5_0 = L_4;
		goto IL_002b;
	}

IL_002a:
	{
		G_B5_0 = 0;
	}

IL_002b:
	{
		return G_B5_0;
	}
}
// System.String System.Xml.XmlValidatingReader::get_LocalName()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlValidatingReader_get_LocalName_m1405968918_MetadataUsageId;
extern "C"  String_t* XmlValidatingReader_get_LocalName_m1405968918 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_get_LocalName_m1405968918_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_0011:
	{
		bool L_2 = XmlValidatingReader_get_Namespaces_m1347603135(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		XmlReader_t4229084514 * L_3 = __this->get_validatingReader_6();
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_3);
		return L_4;
	}

IL_0028:
	{
		XmlReader_t4229084514 * L_5 = __this->get_validatingReader_6();
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Xml.XmlReader::get_Name() */, L_5);
		return L_6;
	}
}
// System.String System.Xml.XmlValidatingReader::get_Name()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlValidatingReader_get_Name_m3823532909_MetadataUsageId;
extern "C"  String_t* XmlValidatingReader_get_Name_m3823532909 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_get_Name_m3823532909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_1;
		goto IL_0020;
	}

IL_0015:
	{
		XmlReader_t4229084514 * L_2 = __this->get_validatingReader_6();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Xml.XmlReader::get_Name() */, L_2);
		G_B3_0 = L_3;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XmlValidatingReader::get_Namespaces()
extern "C"  bool XmlValidatingReader_get_Namespaces_m1347603135 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	{
		XmlTextReader_t3719122287 * L_0 = __this->get_xmlTextReader_5();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		XmlTextReader_t3719122287 * L_1 = __this->get_xmlTextReader_5();
		NullCheck(L_1);
		bool L_2 = XmlTextReader_get_Namespaces_m1718076121(L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0017:
	{
		return (bool)1;
	}
}
// System.String System.Xml.XmlValidatingReader::get_NamespaceURI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlValidatingReader_get_NamespaceURI_m3546691699_MetadataUsageId;
extern "C"  String_t* XmlValidatingReader_get_NamespaceURI_m3546691699 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_get_NamespaceURI_m3546691699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_0011:
	{
		bool L_2 = XmlValidatingReader_get_Namespaces_m1347603135(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		XmlReader_t4229084514 * L_3 = __this->get_validatingReader_6();
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_3);
		return L_4;
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_5;
	}
}
// System.Xml.XmlNameTable System.Xml.XmlValidatingReader::get_NameTable()
extern "C"  XmlNameTable_t3232213908 * XmlValidatingReader_get_NameTable_m1151460085 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	XmlNameTable_t3232213908 * G_B3_0 = NULL;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		XmlReader_t4229084514 * L_1 = __this->get_sourceReader_4();
		NullCheck(L_1);
		XmlNameTable_t3232213908 * L_2 = VirtFuncInvoker0< XmlNameTable_t3232213908 * >::Invoke(20 /* System.Xml.XmlNameTable System.Xml.XmlReader::get_NameTable() */, L_1);
		G_B3_0 = L_2;
		goto IL_0026;
	}

IL_001b:
	{
		XmlReader_t4229084514 * L_3 = __this->get_validatingReader_6();
		NullCheck(L_3);
		XmlNameTable_t3232213908 * L_4 = VirtFuncInvoker0< XmlNameTable_t3232213908 * >::Invoke(20 /* System.Xml.XmlNameTable System.Xml.XmlReader::get_NameTable() */, L_3);
		G_B3_0 = L_4;
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// System.Xml.XmlNodeType System.Xml.XmlValidatingReader::get_NodeType()
extern "C"  int32_t XmlValidatingReader_get_NodeType_m2679077551 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001c;
	}

IL_0011:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
	}

IL_001c:
	{
		return (int32_t)(G_B3_0);
	}
}
// System.String System.Xml.XmlValidatingReader::get_Prefix()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlValidatingReader_get_Prefix_m3385305460_MetadataUsageId;
extern "C"  String_t* XmlValidatingReader_get_Prefix_m3385305460 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_get_Prefix_m3385305460_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_1;
		goto IL_0020;
	}

IL_0015:
	{
		XmlReader_t4229084514 * L_2 = __this->get_validatingReader_6();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.Xml.XmlReader::get_Prefix() */, L_2);
		G_B3_0 = L_3;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Xml.ReadState System.Xml.XmlValidatingReader::get_ReadState()
extern "C"  int32_t XmlValidatingReader_get_ReadState_m2596423314 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (int32_t)(0);
	}

IL_000d:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Xml.ReadState System.Xml.XmlReader::get_ReadState() */, L_1);
		return L_2;
	}
}
// System.Xml.XmlResolver System.Xml.XmlValidatingReader::get_Resolver()
extern "C"  XmlResolver_t2502213349 * XmlValidatingReader_get_Resolver_m464606959 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	{
		XmlTextReader_t3719122287 * L_0 = __this->get_xmlTextReader_5();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		XmlTextReader_t3719122287 * L_1 = __this->get_xmlTextReader_5();
		NullCheck(L_1);
		XmlResolver_t2502213349 * L_2 = XmlTextReader_get_Resolver_m3306237705(L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0017:
	{
		bool L_3 = __this->get_resolverSpecified_8();
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		XmlResolver_t2502213349 * L_4 = __this->get_resolver_7();
		return L_4;
	}

IL_0029:
	{
		return (XmlResolver_t2502213349 *)NULL;
	}
}
// System.Xml.Schema.XmlSchemaCollection System.Xml.XmlValidatingReader::get_Schemas()
extern Il2CppClass* XmlSchemaCollection_t1190777955_il2cpp_TypeInfo_var;
extern const uint32_t XmlValidatingReader_get_Schemas_m3609787041_MetadataUsageId;
extern "C"  XmlSchemaCollection_t1190777955 * XmlValidatingReader_get_Schemas_m3609787041 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_get_Schemas_m3609787041_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlSchemaCollection_t1190777955 * L_0 = __this->get_schemas_10();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		XmlNameTable_t3232213908 * L_1 = VirtFuncInvoker0< XmlNameTable_t3232213908 * >::Invoke(20 /* System.Xml.XmlNameTable System.Xml.XmlValidatingReader::get_NameTable() */, __this);
		XmlSchemaCollection_t1190777955 * L_2 = (XmlSchemaCollection_t1190777955 *)il2cpp_codegen_object_new(XmlSchemaCollection_t1190777955_il2cpp_TypeInfo_var);
		XmlSchemaCollection__ctor_m1218368366(L_2, L_1, /*hidden argument*/NULL);
		__this->set_schemas_10(L_2);
	}

IL_001c:
	{
		XmlSchemaCollection_t1190777955 * L_3 = __this->get_schemas_10();
		return L_3;
	}
}
// System.Xml.XmlReaderSettings System.Xml.XmlValidatingReader::get_Settings()
extern "C"  XmlReaderSettings_t3693321125 * XmlValidatingReader_get_Settings_m4246462284 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	XmlReaderSettings_t3693321125 * G_B3_0 = NULL;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		XmlReader_t4229084514 * L_1 = __this->get_sourceReader_4();
		NullCheck(L_1);
		XmlReaderSettings_t3693321125 * L_2 = VirtFuncInvoker0< XmlReaderSettings_t3693321125 * >::Invoke(25 /* System.Xml.XmlReaderSettings System.Xml.XmlReader::get_Settings() */, L_1);
		G_B3_0 = L_2;
		goto IL_0026;
	}

IL_001b:
	{
		XmlReader_t4229084514 * L_3 = __this->get_validatingReader_6();
		NullCheck(L_3);
		XmlReaderSettings_t3693321125 * L_4 = VirtFuncInvoker0< XmlReaderSettings_t3693321125 * >::Invoke(25 /* System.Xml.XmlReaderSettings System.Xml.XmlReader::get_Settings() */, L_3);
		G_B3_0 = L_4;
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// System.Xml.ValidationType System.Xml.XmlValidatingReader::get_ValidationType()
extern "C"  int32_t XmlValidatingReader_get_ValidationType_m1920215420 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_validationType_9();
		return L_0;
	}
}
// System.Void System.Xml.XmlValidatingReader::set_ValidationType(System.Xml.ValidationType)
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3084989556;
extern const uint32_t XmlValidatingReader_set_ValidationType_m3321978755_MetadataUsageId;
extern "C"  void XmlValidatingReader_set_ValidationType_m3321978755 (XmlValidatingReader_t2921250517 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_set_ValidationType_m3321978755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Xml.ReadState System.Xml.XmlValidatingReader::get_ReadState() */, __this);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral3084989556, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = __this->get_validationType_9();
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (L_3 == 0)
		{
			goto IL_003c;
		}
		if (L_3 == 1)
		{
			goto IL_003c;
		}
		if (L_3 == 2)
		{
			goto IL_003c;
		}
		if (L_3 == 3)
		{
			goto IL_0048;
		}
		if (L_3 == 4)
		{
			goto IL_003c;
		}
	}
	{
		goto IL_004e;
	}

IL_003c:
	{
		int32_t L_4 = ___value0;
		__this->set_validationType_9(L_4);
		goto IL_004e;
	}

IL_0048:
	{
		NotSupportedException_t1374155497 * L_5 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_004e:
	{
		return;
	}
}
// System.String System.Xml.XmlValidatingReader::get_Value()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlValidatingReader_get_Value_m1075109809_MetadataUsageId;
extern "C"  String_t* XmlValidatingReader_get_Value_m1075109809 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_get_Value_m1075109809_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_1;
		goto IL_0020;
	}

IL_0015:
	{
		XmlReader_t4229084514 * L_2 = __this->get_validatingReader_6();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(26 /* System.String System.Xml.XmlReader::get_Value() */, L_2);
		G_B3_0 = L_3;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.String System.Xml.XmlValidatingReader::get_XmlLang()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlValidatingReader_get_XmlLang_m1331978277_MetadataUsageId;
extern "C"  String_t* XmlValidatingReader_get_XmlLang_m1331978277 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_get_XmlLang_m1331978277_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_1;
		goto IL_0020;
	}

IL_0015:
	{
		XmlReader_t4229084514 * L_2 = __this->get_validatingReader_6();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(27 /* System.String System.Xml.XmlReader::get_XmlLang() */, L_2);
		G_B3_0 = L_3;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Xml.XmlSpace System.Xml.XmlValidatingReader::get_XmlSpace()
extern "C"  int32_t XmlValidatingReader_get_XmlSpace_m1232010556 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001c;
	}

IL_0011:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(28 /* System.Xml.XmlSpace System.Xml.XmlReader::get_XmlSpace() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
	}

IL_001c:
	{
		return (int32_t)(G_B3_0);
	}
}
// System.Void System.Xml.XmlValidatingReader::Close()
extern "C"  void XmlValidatingReader_Close_m3732160612 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		XmlReader_t4229084514 * L_1 = __this->get_sourceReader_4();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(29 /* System.Void System.Xml.XmlReader::Close() */, L_1);
		goto IL_0026;
	}

IL_001b:
	{
		XmlReader_t4229084514 * L_2 = __this->get_validatingReader_6();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(29 /* System.Void System.Xml.XmlReader::Close() */, L_2);
	}

IL_0026:
	{
		return;
	}
}
// System.String System.Xml.XmlValidatingReader::GetAttribute(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3760259642_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1565480051;
extern const uint32_t XmlValidatingReader_GetAttribute_m3192267728_MetadataUsageId;
extern "C"  String_t* XmlValidatingReader_GetAttribute_m3192267728 (XmlValidatingReader_t2921250517 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_GetAttribute_m3192267728_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IndexOutOfRangeException_t3760259642 * L_1 = (IndexOutOfRangeException_t3760259642 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3760259642_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_1, _stringLiteral1565480051, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		XmlReader_t4229084514 * L_2 = __this->get_validatingReader_6();
		int32_t L_3 = ___i0;
		NullCheck(L_2);
		String_t* L_4 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(14 /* System.String System.Xml.XmlReader::get_Item(System.Int32) */, L_2, L_3);
		return L_4;
	}
}
// System.String System.Xml.XmlValidatingReader::GetAttribute(System.String)
extern "C"  String_t* XmlValidatingReader_GetAttribute_m632750179 (XmlValidatingReader_t2921250517 * __this, String_t* ___name0, const MethodInfo* method)
{
	String_t* G_B3_0 = NULL;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((String_t*)(NULL));
		goto IL_001d;
	}

IL_0011:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		String_t* L_2 = ___name0;
		NullCheck(L_1);
		String_t* L_3 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(15 /* System.String System.Xml.XmlReader::get_Item(System.String) */, L_1, L_2);
		G_B3_0 = L_3;
	}

IL_001d:
	{
		return G_B3_0;
	}
}
// System.String System.Xml.XmlValidatingReader::GetAttribute(System.String,System.String)
extern "C"  String_t* XmlValidatingReader_GetAttribute_m701120351 (XmlValidatingReader_t2921250517 * __this, String_t* ___localName0, String_t* ___namespaceName1, const MethodInfo* method)
{
	String_t* G_B3_0 = NULL;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((String_t*)(NULL));
		goto IL_001e;
	}

IL_0011:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		String_t* L_2 = ___localName0;
		String_t* L_3 = ___namespaceName1;
		NullCheck(L_1);
		String_t* L_4 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(16 /* System.String System.Xml.XmlReader::get_Item(System.String,System.String) */, L_1, L_2, L_3);
		G_B3_0 = L_4;
	}

IL_001e:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XmlValidatingReader::HasLineInfo()
extern Il2CppClass* IXmlLineInfo_t570005944_il2cpp_TypeInfo_var;
extern const uint32_t XmlValidatingReader_HasLineInfo_m1813300128_MetadataUsageId;
extern "C"  bool XmlValidatingReader_HasLineInfo_m1813300128 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_HasLineInfo_m1813300128_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t G_B3_0 = 0;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		V_0 = ((Il2CppObject *)IsInst(L_0, IXmlLineInfo_t570005944_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = InterfaceFuncInvoker0< bool >::Invoke(2 /* System.Boolean System.Xml.IXmlLineInfo::HasLineInfo() */, IXmlLineInfo_t570005944_il2cpp_TypeInfo_var, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.String System.Xml.XmlValidatingReader::LookupNamespace(System.String)
extern "C"  String_t* XmlValidatingReader_LookupNamespace_m2808869400 (XmlValidatingReader_t2921250517 * __this, String_t* ___prefix0, const MethodInfo* method)
{
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		String_t* L_2 = ___prefix0;
		NullCheck(L_1);
		String_t* L_3 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(34 /* System.String System.Xml.XmlReader::LookupNamespace(System.String) */, L_1, L_2);
		return L_3;
	}

IL_0018:
	{
		XmlReader_t4229084514 * L_4 = __this->get_sourceReader_4();
		String_t* L_5 = ___prefix0;
		NullCheck(L_4);
		String_t* L_6 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(34 /* System.String System.Xml.XmlReader::LookupNamespace(System.String) */, L_4, L_5);
		return L_6;
	}
}
// System.Void System.Xml.XmlValidatingReader::MoveToAttribute(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3760259642_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1565480051;
extern const uint32_t XmlValidatingReader_MoveToAttribute_m2954625997_MetadataUsageId;
extern "C"  void XmlValidatingReader_MoveToAttribute_m2954625997 (XmlValidatingReader_t2921250517 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_MoveToAttribute_m2954625997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IndexOutOfRangeException_t3760259642 * L_1 = (IndexOutOfRangeException_t3760259642 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3760259642_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1621772274(L_1, _stringLiteral1565480051, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		XmlReader_t4229084514 * L_2 = __this->get_validatingReader_6();
		int32_t L_3 = ___i0;
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(35 /* System.Void System.Xml.XmlReader::MoveToAttribute(System.Int32) */, L_2, L_3);
		return;
	}
}
// System.Boolean System.Xml.XmlValidatingReader::MoveToAttribute(System.String)
extern "C"  bool XmlValidatingReader_MoveToAttribute_m2861329422 (XmlValidatingReader_t2921250517 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		String_t* L_2 = ___name0;
		NullCheck(L_1);
		bool L_3 = VirtFuncInvoker1< bool, String_t* >::Invoke(36 /* System.Boolean System.Xml.XmlReader::MoveToAttribute(System.String) */, L_1, L_2);
		return L_3;
	}
}
// System.Boolean System.Xml.XmlValidatingReader::MoveToAttribute(System.String,System.String)
extern "C"  bool XmlValidatingReader_MoveToAttribute_m3503842250 (XmlValidatingReader_t2921250517 * __this, String_t* ___localName0, String_t* ___namespaceName1, const MethodInfo* method)
{
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		String_t* L_2 = ___localName0;
		String_t* L_3 = ___namespaceName1;
		NullCheck(L_1);
		bool L_4 = VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(37 /* System.Boolean System.Xml.XmlReader::MoveToAttribute(System.String,System.String) */, L_1, L_2, L_3);
		return L_4;
	}
}
// System.Boolean System.Xml.XmlValidatingReader::MoveToElement()
extern "C"  bool XmlValidatingReader_MoveToElement_m4184774036 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(39 /* System.Boolean System.Xml.XmlReader::MoveToElement() */, L_1);
		return L_2;
	}
}
// System.Boolean System.Xml.XmlValidatingReader::MoveToFirstAttribute()
extern "C"  bool XmlValidatingReader_MoveToFirstAttribute_m2055141206 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(40 /* System.Boolean System.Xml.XmlReader::MoveToFirstAttribute() */, L_1);
		return L_2;
	}
}
// System.Boolean System.Xml.XmlValidatingReader::MoveToNextAttribute()
extern "C"  bool XmlValidatingReader_MoveToNextAttribute_m3788997377 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(41 /* System.Boolean System.Xml.XmlReader::MoveToNextAttribute() */, L_1);
		return L_2;
	}
}
// System.Boolean System.Xml.XmlValidatingReader::Read()
extern Il2CppClass* DTDValidatingReader_t2954095246_il2cpp_TypeInfo_var;
extern Il2CppClass* XsdValidatingReader_t1549156720_il2cpp_TypeInfo_var;
extern Il2CppClass* ValidationEventHandler_t2777264566_il2cpp_TypeInfo_var;
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern Il2CppClass* IHasXmlSchemaInfo_t728010009_il2cpp_TypeInfo_var;
extern const MethodInfo* XmlValidatingReader_OnValidationEvent_m3153187659_MethodInfo_var;
extern const uint32_t XmlValidatingReader_Read_m1597874260_MetadataUsageId;
extern "C"  bool XmlValidatingReader_Read_m1597874260 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlValidatingReader_Read_m1597874260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XsdValidatingReader_t1549156720 * V_0 = NULL;
	int32_t V_1 = 0;
	DTDValidatingReader_t2954095246 * V_2 = NULL;
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_0104;
		}
	}
	{
		int32_t L_1 = XmlValidatingReader_get_ValidationType_m1920215420(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (L_2 == 0)
		{
			goto IL_0031;
		}
		if (L_2 == 1)
		{
			goto IL_0031;
		}
		if (L_2 == 2)
		{
			goto IL_0036;
		}
		if (L_2 == 3)
		{
			goto IL_00ed;
		}
		if (L_2 == 4)
		{
			goto IL_0067;
		}
	}
	{
		goto IL_00f3;
	}

IL_0031:
	{
		goto IL_0067;
	}

IL_0036:
	{
		XmlReader_t4229084514 * L_3 = __this->get_sourceReader_4();
		DTDValidatingReader_t2954095246 * L_4 = (DTDValidatingReader_t2954095246 *)il2cpp_codegen_object_new(DTDValidatingReader_t2954095246_il2cpp_TypeInfo_var);
		DTDValidatingReader__ctor_m2164651414(L_4, L_3, __this, /*hidden argument*/NULL);
		DTDValidatingReader_t2954095246 * L_5 = L_4;
		V_2 = L_5;
		__this->set_dtdReader_11(L_5);
		DTDValidatingReader_t2954095246 * L_6 = V_2;
		__this->set_validatingReader_6(L_6);
		DTDValidatingReader_t2954095246 * L_7 = __this->get_dtdReader_11();
		XmlResolver_t2502213349 * L_8 = XmlValidatingReader_get_Resolver_m464606959(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		DTDValidatingReader_set_XmlResolver_m4183902036(L_7, L_8, /*hidden argument*/NULL);
		goto IL_00f3;
	}

IL_0067:
	{
		XmlReader_t4229084514 * L_9 = __this->get_sourceReader_4();
		DTDValidatingReader_t2954095246 * L_10 = (DTDValidatingReader_t2954095246 *)il2cpp_codegen_object_new(DTDValidatingReader_t2954095246_il2cpp_TypeInfo_var);
		DTDValidatingReader__ctor_m2164651414(L_10, L_9, __this, /*hidden argument*/NULL);
		__this->set_dtdReader_11(L_10);
		DTDValidatingReader_t2954095246 * L_11 = __this->get_dtdReader_11();
		XsdValidatingReader_t1549156720 * L_12 = (XsdValidatingReader_t1549156720 *)il2cpp_codegen_object_new(XsdValidatingReader_t1549156720_il2cpp_TypeInfo_var);
		XsdValidatingReader__ctor_m3940422697(L_12, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		XsdValidatingReader_t1549156720 * L_13 = V_0;
		XsdValidatingReader_t1549156720 * L_14 = L_13;
		NullCheck(L_14);
		ValidationEventHandler_t2777264566 * L_15 = L_14->get_ValidationEventHandler_29();
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)XmlValidatingReader_OnValidationEvent_m3153187659_MethodInfo_var);
		ValidationEventHandler_t2777264566 * L_17 = (ValidationEventHandler_t2777264566 *)il2cpp_codegen_object_new(ValidationEventHandler_t2777264566_il2cpp_TypeInfo_var);
		ValidationEventHandler__ctor_m2060655069(L_17, __this, L_16, /*hidden argument*/NULL);
		Delegate_t3660574010 * L_18 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		NullCheck(L_14);
		L_14->set_ValidationEventHandler_29(((ValidationEventHandler_t2777264566 *)CastclassSealed(L_18, ValidationEventHandler_t2777264566_il2cpp_TypeInfo_var)));
		XsdValidatingReader_t1549156720 * L_19 = V_0;
		int32_t L_20 = XmlValidatingReader_get_ValidationType_m1920215420(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		XsdValidatingReader_set_ValidationType_m2189422498(L_19, L_20, /*hidden argument*/NULL);
		XsdValidatingReader_t1549156720 * L_21 = V_0;
		XmlSchemaCollection_t1190777955 * L_22 = XmlValidatingReader_get_Schemas_m3609787041(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		XmlSchemaSet_t3827173367 * L_23 = XmlSchemaCollection_get_SchemaSet_m3650114568(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		XsdValidatingReader_set_Schemas_m308878399(L_21, L_23, /*hidden argument*/NULL);
		XsdValidatingReader_t1549156720 * L_24 = V_0;
		XmlResolver_t2502213349 * L_25 = XmlValidatingReader_get_Resolver_m464606959(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		XsdValidatingReader_set_XmlResolver_m1087497092(L_24, L_25, /*hidden argument*/NULL);
		XsdValidatingReader_t1549156720 * L_26 = V_0;
		__this->set_validatingReader_6(L_26);
		DTDValidatingReader_t2954095246 * L_27 = __this->get_dtdReader_11();
		XmlResolver_t2502213349 * L_28 = XmlValidatingReader_get_Resolver_m464606959(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		DTDValidatingReader_set_XmlResolver_m4183902036(L_27, L_28, /*hidden argument*/NULL);
		goto IL_00f3;
	}

IL_00ed:
	{
		NotSupportedException_t1374155497 * L_29 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29);
	}

IL_00f3:
	{
		XmlReader_t4229084514 * L_30 = __this->get_validatingReader_6();
		__this->set_schemaInfo_12(((Il2CppObject *)IsInst(L_30, IHasXmlSchemaInfo_t728010009_il2cpp_TypeInfo_var)));
	}

IL_0104:
	{
		XmlReader_t4229084514 * L_31 = __this->get_validatingReader_6();
		NullCheck(L_31);
		bool L_32 = VirtFuncInvoker0< bool >::Invoke(42 /* System.Boolean System.Xml.XmlReader::Read() */, L_31);
		return L_32;
	}
}
// System.Boolean System.Xml.XmlValidatingReader::ReadAttributeValue()
extern "C"  bool XmlValidatingReader_ReadAttributeValue_m1629296169 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		XmlReader_t4229084514 * L_1 = __this->get_validatingReader_6();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(43 /* System.Boolean System.Xml.XmlReader::ReadAttributeValue() */, L_1);
		return L_2;
	}
}
// System.String System.Xml.XmlValidatingReader::ReadString()
extern "C"  String_t* XmlValidatingReader_ReadString_m2749573152 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = XmlReader_ReadString_m3848699309(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Xml.XmlValidatingReader::ResolveEntity()
extern "C"  void XmlValidatingReader_ResolveEntity_m2855933595 (XmlValidatingReader_t2921250517 * __this, const MethodInfo* method)
{
	{
		XmlReader_t4229084514 * L_0 = __this->get_validatingReader_6();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(51 /* System.Void System.Xml.XmlReader::ResolveEntity() */, L_0);
		return;
	}
}
// System.Void System.Xml.XmlValidatingReader::OnValidationEvent(System.Object,System.Xml.Schema.ValidationEventArgs)
extern "C"  void XmlValidatingReader_OnValidationEvent_m3153187659 (XmlValidatingReader_t2921250517 * __this, Il2CppObject * ___o0, ValidationEventArgs_t1700465963 * ___e1, const MethodInfo* method)
{
	{
		ValidationEventHandler_t2777264566 * L_0 = __this->get_ValidationEventHandler_14();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		ValidationEventHandler_t2777264566 * L_1 = __this->get_ValidationEventHandler_14();
		Il2CppObject * L_2 = ___o0;
		ValidationEventArgs_t1700465963 * L_3 = ___e1;
		NullCheck(L_1);
		ValidationEventHandler_Invoke_m3220362594(L_1, L_2, L_3, /*hidden argument*/NULL);
		goto IL_003a;
	}

IL_001d:
	{
		int32_t L_4 = XmlValidatingReader_get_ValidationType_m1920215420(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		ValidationEventArgs_t1700465963 * L_5 = ___e1;
		NullCheck(L_5);
		int32_t L_6 = ValidationEventArgs_get_Severity_m3103623188(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_003a;
		}
	}
	{
		ValidationEventArgs_t1700465963 * L_7 = ___e1;
		NullCheck(L_7);
		XmlSchemaException_t2660249060 * L_8 = ValidationEventArgs_get_Exception_m1711707009(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003a:
	{
		return;
	}
}
// System.Void System.Xml.XmlWhitespace::.ctor(System.String,System.Xml.XmlDocument)
extern "C"  void XmlWhitespace__ctor_m1787065039 (XmlWhitespace_t2187161500 * __this, String_t* ___strData0, XmlDocument_t3705263098 * ___doc1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___strData0;
		XmlDocument_t3705263098 * L_1 = ___doc1;
		XmlCharacterData__ctor_m2021947511(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XmlWhitespace::get_LocalName()
extern Il2CppCodeGenString* _stringLiteral1808123616;
extern const uint32_t XmlWhitespace_get_LocalName_m1385202671_MetadataUsageId;
extern "C"  String_t* XmlWhitespace_get_LocalName_m1385202671 (XmlWhitespace_t2187161500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlWhitespace_get_LocalName_m1385202671_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral1808123616;
	}
}
// System.String System.Xml.XmlWhitespace::get_Name()
extern Il2CppCodeGenString* _stringLiteral1808123616;
extern const uint32_t XmlWhitespace_get_Name_m3985233396_MetadataUsageId;
extern "C"  String_t* XmlWhitespace_get_Name_m3985233396 (XmlWhitespace_t2187161500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlWhitespace_get_Name_m3985233396_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral1808123616;
	}
}
// System.Xml.XmlNodeType System.Xml.XmlWhitespace::get_NodeType()
extern "C"  int32_t XmlWhitespace_get_NodeType_m3606704758 (XmlWhitespace_t2187161500 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(((int32_t)13));
	}
}
// System.String System.Xml.XmlWhitespace::get_Value()
extern "C"  String_t* XmlWhitespace_get_Value_m1792857610 (XmlWhitespace_t2187161500 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(38 /* System.String System.Xml.XmlCharacterData::get_Data() */, __this);
		return L_0;
	}
}
// System.Void System.Xml.XmlWhitespace::set_Value(System.String)
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral612912714;
extern const uint32_t XmlWhitespace_set_Value_m1818473929_MetadataUsageId;
extern "C"  void XmlWhitespace_set_Value_m1818473929 (XmlWhitespace_t2187161500 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlWhitespace_set_Value_m1818473929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_1 = XmlChar_IsWhitespace_m1770331005(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, _stringLiteral612912714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		String_t* L_3 = ___value0;
		VirtActionInvoker1< String_t* >::Invoke(39 /* System.Void System.Xml.XmlCharacterData::set_Data(System.String) */, __this, L_3);
		return;
	}
}
// System.Xml.XmlNode System.Xml.XmlWhitespace::get_ParentNode()
extern "C"  XmlNode_t3592213601 * XmlWhitespace_get_ParentNode_m423937856 (XmlWhitespace_t2187161500 * __this, const MethodInfo* method)
{
	{
		XmlNode_t3592213601 * L_0 = XmlNode_get_ParentNode_m33805637(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Xml.XmlNode System.Xml.XmlWhitespace::CloneNode(System.Boolean)
extern Il2CppClass* XmlWhitespace_t2187161500_il2cpp_TypeInfo_var;
extern const uint32_t XmlWhitespace_CloneNode_m1410926029_MetadataUsageId;
extern "C"  XmlNode_t3592213601 * XmlWhitespace_CloneNode_m1410926029 (XmlWhitespace_t2187161500 * __this, bool ___deep0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlWhitespace_CloneNode_m1410926029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(38 /* System.String System.Xml.XmlCharacterData::get_Data() */, __this);
		XmlDocument_t3705263098 * L_1 = VirtFuncInvoker0< XmlDocument_t3705263098 * >::Invoke(20 /* System.Xml.XmlDocument System.Xml.XmlNode::get_OwnerDocument() */, __this);
		XmlWhitespace_t2187161500 * L_2 = (XmlWhitespace_t2187161500 *)il2cpp_codegen_object_new(XmlWhitespace_t2187161500_il2cpp_TypeInfo_var);
		XmlWhitespace__ctor_m1787065039(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.XmlWriter::.ctor()
extern "C"  void XmlWriter__ctor_m2911908785 (XmlWriter_t89522450 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlWriter::System.IDisposable.Dispose()
extern "C"  void XmlWriter_System_IDisposable_Dispose_m3193799822 (XmlWriter_t89522450 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Xml.XmlWriter::Dispose(System.Boolean) */, __this, (bool)0);
		return;
	}
}
// System.Xml.XmlWriterSettings System.Xml.XmlWriter::get_Settings()
extern Il2CppClass* XmlWriterSettings_t3992467285_il2cpp_TypeInfo_var;
extern const uint32_t XmlWriter_get_Settings_m4235714649_MetadataUsageId;
extern "C"  XmlWriterSettings_t3992467285 * XmlWriter_get_Settings_m4235714649 (XmlWriter_t89522450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlWriter_get_Settings_m4235714649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlWriterSettings_t3992467285 * L_0 = __this->get_settings_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		XmlWriterSettings_t3992467285 * L_1 = (XmlWriterSettings_t3992467285 *)il2cpp_codegen_object_new(XmlWriterSettings_t3992467285_il2cpp_TypeInfo_var);
		XmlWriterSettings__ctor_m262185166(L_1, /*hidden argument*/NULL);
		__this->set_settings_0(L_1);
	}

IL_0016:
	{
		XmlWriterSettings_t3992467285 * L_2 = __this->get_settings_0();
		return L_2;
	}
}
// System.String System.Xml.XmlWriter::get_XmlLang()
extern "C"  String_t* XmlWriter_get_XmlLang_m4143028872 (XmlWriter_t89522450 * __this, const MethodInfo* method)
{
	{
		return (String_t*)NULL;
	}
}
// System.Xml.XmlSpace System.Xml.XmlWriter::get_XmlSpace()
extern "C"  int32_t XmlWriter_get_XmlSpace_m767449337 (XmlWriter_t89522450 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void System.Xml.XmlWriter::Dispose(System.Boolean)
extern "C"  void XmlWriter_Dispose_m1200530597 (XmlWriter_t89522450 * __this, bool ___disposing0, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(9 /* System.Void System.Xml.XmlWriter::Close() */, __this);
		return;
	}
}
// System.Void System.Xml.XmlWriter::WriteAttribute(System.Xml.XmlReader,System.Boolean)
extern "C"  void XmlWriter_WriteAttribute_m1597931729 (XmlWriter_t89522450 * __this, XmlReader_t4229084514 * ___reader0, bool ___defattr1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = ___defattr1;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		XmlReader_t4229084514 * L_1 = ___reader0;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(13 /* System.Boolean System.Xml.XmlReader::get_IsDefault() */, L_1);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		XmlReader_t4229084514 * L_3 = ___reader0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.Xml.XmlReader::get_Prefix() */, L_3);
		XmlReader_t4229084514 * L_5 = ___reader0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_5);
		XmlReader_t4229084514 * L_7 = ___reader0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_7);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(23 /* System.Void System.Xml.XmlWriter::WriteStartAttribute(System.String,System.String,System.String) */, __this, L_4, L_6, L_8);
		goto IL_0071;
	}

IL_002f:
	{
		XmlReader_t4229084514 * L_9 = ___reader0;
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_9);
		V_0 = L_10;
		int32_t L_11 = V_0;
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 0)
		{
			goto IL_004f;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 1)
		{
			goto IL_0071;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 2)
		{
			goto IL_0060;
		}
	}
	{
		goto IL_0071;
	}

IL_004f:
	{
		XmlReader_t4229084514 * L_12 = ___reader0;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(26 /* System.String System.Xml.XmlReader::get_Value() */, L_12);
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, __this, L_13);
		goto IL_0071;
	}

IL_0060:
	{
		XmlReader_t4229084514 * L_14 = ___reader0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Xml.XmlReader::get_Name() */, L_14);
		VirtActionInvoker1< String_t* >::Invoke(18 /* System.Void System.Xml.XmlWriter::WriteEntityRef(System.String) */, __this, L_15);
		goto IL_0071;
	}

IL_0071:
	{
		XmlReader_t4229084514 * L_16 = ___reader0;
		NullCheck(L_16);
		bool L_17 = VirtFuncInvoker0< bool >::Invoke(43 /* System.Boolean System.Xml.XmlReader::ReadAttributeValue() */, L_16);
		if (L_17)
		{
			goto IL_002f;
		}
	}
	{
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, __this);
		return;
	}
}
// System.Void System.Xml.XmlWriter::WriteAttributeString(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlWriter_WriteAttributeString_m1069599196_MetadataUsageId;
extern "C"  void XmlWriter_WriteAttributeString_m1069599196 (XmlWriter_t89522450 * __this, String_t* ___localName0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlWriter_WriteAttributeString_m1069599196_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_1 = ___localName0;
		String_t* L_2 = ___value1;
		XmlWriter_WriteAttributeString_m1630990645(__this, L_0, L_1, (String_t*)NULL, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlWriter::WriteAttributeString(System.String,System.String,System.String,System.String)
extern "C"  void XmlWriter_WriteAttributeString_m1630990645 (XmlWriter_t89522450 * __this, String_t* ___prefix0, String_t* ___localName1, String_t* ___ns2, String_t* ___value3, const MethodInfo* method)
{
	{
		String_t* L_0 = ___prefix0;
		String_t* L_1 = ___localName1;
		String_t* L_2 = ___ns2;
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(23 /* System.Void System.Xml.XmlWriter::WriteStartAttribute(System.String,System.String,System.String) */, __this, L_0, L_1, L_2);
		String_t* L_3 = ___value3;
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_4 = ___value3;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m2979997331(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_6 = ___value3;
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, __this, L_6);
	}

IL_0025:
	{
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndAttribute() */, __this);
		return;
	}
}
// System.Void System.Xml.XmlWriter::WriteElementString(System.String,System.String)
extern "C"  void XmlWriter_WriteElementString_m3759055996 (XmlWriter_t89522450 * __this, String_t* ___localName0, String_t* ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___localName0;
		XmlWriter_WriteStartElement_m2942452795(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___value1;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_2 = ___value1;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m2979997331(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_4 = ___value1;
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, __this, L_4);
	}

IL_0020:
	{
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, __this);
		return;
	}
}
// System.Void System.Xml.XmlWriter::WriteQualifiedName(System.String,System.String)
extern "C"  void XmlWriter_WriteQualifiedName_m2599870787 (XmlWriter_t89522450 * __this, String_t* ___localName0, String_t* ___ns1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___localName0;
		String_t* L_1 = ___ns1;
		XmlWriter_WriteQualifiedNameInternal_m2020252038(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlWriter::WriteQualifiedNameInternal(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlConvert_t1882388356_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1068873040;
extern Il2CppCodeGenString* _stringLiteral58;
extern const uint32_t XmlWriter_WriteQualifiedNameInternal_m2020252038_MetadataUsageId;
extern "C"  void XmlWriter_WriteQualifiedNameInternal_m2020252038 (XmlWriter_t89522450 * __this, String_t* ___localName0, String_t* ___ns1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlWriter_WriteQualifiedNameInternal_m2020252038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* G_B12_0 = NULL;
	{
		String_t* L_0 = ___localName0;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_1 = ___localName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}

IL_0016:
	{
		ArgumentException_t124305799 * L_4 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_001c:
	{
		String_t* L_5 = ___ns1;
		if (L_5)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		___ns1 = L_6;
	}

IL_0029:
	{
		XmlWriterSettings_t3992467285 * L_7 = VirtFuncInvoker0< XmlWriterSettings_t3992467285 * >::Invoke(5 /* System.Xml.XmlWriterSettings System.Xml.XmlWriter::get_Settings() */, __this);
		NullCheck(L_7);
		int32_t L_8 = XmlWriterSettings_get_ConformanceLevel_m1810449468(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) == ((int32_t)1)))
		{
			goto IL_0048;
		}
	}
	{
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)2)))
		{
			goto IL_0048;
		}
	}
	{
		goto IL_0054;
	}

IL_0048:
	{
		String_t* L_11 = ___localName0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlConvert_t1882388356_il2cpp_TypeInfo_var);
		XmlConvert_VerifyNCName_m2724758775(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		goto IL_0054;
	}

IL_0054:
	{
		String_t* L_12 = ___ns1;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m2979997331(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)0)))
		{
			goto IL_006c;
		}
	}
	{
		String_t* L_14 = ___ns1;
		String_t* L_15 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(12 /* System.String System.Xml.XmlWriter::LookupPrefix(System.String) */, __this, L_14);
		G_B12_0 = L_15;
		goto IL_0071;
	}

IL_006c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B12_0 = L_16;
	}

IL_0071:
	{
		V_0 = G_B12_0;
		String_t* L_17 = V_0;
		if (L_17)
		{
			goto IL_0089;
		}
	}
	{
		String_t* L_18 = ___ns1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1068873040, L_18, /*hidden argument*/NULL);
		ArgumentException_t124305799 * L_20 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_20, L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}

IL_0089:
	{
		String_t* L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_23 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00b7;
		}
	}
	{
		String_t* L_24 = V_0;
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, __this, L_24);
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, __this, _stringLiteral58);
		String_t* L_25 = ___localName0;
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, __this, L_25);
		goto IL_00be;
	}

IL_00b7:
	{
		String_t* L_26 = ___localName0;
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, __this, L_26);
	}

IL_00be:
	{
		return;
	}
}
// System.Void System.Xml.XmlWriter::WriteNode(System.Xml.XmlReader,System.Boolean)
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNodeType_t3966624571_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2370872937;
extern Il2CppCodeGenString* _stringLiteral2460968495;
extern Il2CppCodeGenString* _stringLiteral295753199;
extern Il2CppCodeGenString* _stringLiteral3827457213;
extern const uint32_t XmlWriter_WriteNode_m817874929_MetadataUsageId;
extern "C"  void XmlWriter_WriteNode_m817874929 (XmlWriter_t89522450 * __this, XmlReader_t4229084514 * ___reader0, bool ___defattr1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlWriter_WriteNode_m817874929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		XmlReader_t4229084514 * L_0 = ___reader0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentException_t124305799 * L_1 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		XmlReader_t4229084514 * L_2 = ___reader0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Xml.ReadState System.Xml.XmlReader::get_ReadState() */, L_2);
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		XmlReader_t4229084514 * L_4 = ___reader0;
		NullCheck(L_4);
		VirtFuncInvoker0< bool >::Invoke(42 /* System.Boolean System.Xml.XmlReader::Read() */, L_4);
	}

IL_001e:
	{
		XmlReader_t4229084514 * L_5 = ___reader0;
		bool L_6 = ___defattr1;
		VirtActionInvoker2< XmlReader_t4229084514 *, bool >::Invoke(21 /* System.Void System.Xml.XmlWriter::WriteNode(System.Xml.XmlReader,System.Boolean) */, __this, L_5, L_6);
		XmlReader_t4229084514 * L_7 = ___reader0;
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean System.Xml.XmlReader::get_EOF() */, L_7);
		if (!L_8)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_0032:
	{
		XmlReader_t4229084514 * L_9 = ___reader0;
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_9);
		V_2 = L_10;
		int32_t L_11 = V_2;
		if (L_11 == 0)
		{
			goto IL_01db;
		}
		if (L_11 == 1)
		{
			goto IL_008c;
		}
		if (L_11 == 2)
		{
			goto IL_012c;
		}
		if (L_11 == 3)
		{
			goto IL_012d;
		}
		if (L_11 == 4)
		{
			goto IL_013e;
		}
		if (L_11 == 5)
		{
			goto IL_014f;
		}
		if (L_11 == 6)
		{
			goto IL_01e0;
		}
		if (L_11 == 7)
		{
			goto IL_0160;
		}
		if (L_11 == 8)
		{
			goto IL_0177;
		}
		if (L_11 == 9)
		{
			goto IL_01e0;
		}
		if (L_11 == 10)
		{
			goto IL_0188;
		}
		if (L_11 == 11)
		{
			goto IL_01e0;
		}
		if (L_11 == 12)
		{
			goto IL_01e0;
		}
		if (L_11 == 13)
		{
			goto IL_01ba;
		}
		if (L_11 == 14)
		{
			goto IL_01b5;
		}
		if (L_11 == 15)
		{
			goto IL_01cb;
		}
		if (L_11 == 16)
		{
			goto IL_01d6;
		}
		if (L_11 == 17)
		{
			goto IL_0160;
		}
	}
	{
		goto IL_01e0;
	}

IL_008c:
	{
		XmlReader_t4229084514 * L_12 = ___reader0;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.Xml.XmlReader::get_Prefix() */, L_12);
		XmlReader_t4229084514 * L_14 = ___reader0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String System.Xml.XmlReader::get_LocalName() */, L_14);
		XmlReader_t4229084514 * L_16 = ___reader0;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XmlReader::get_NamespaceURI() */, L_16);
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, __this, L_13, L_15, L_17);
		XmlReader_t4229084514 * L_18 = ___reader0;
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean System.Xml.XmlReader::get_HasAttributes() */, L_18);
		if (!L_19)
		{
			goto IL_00dc;
		}
	}
	{
		V_0 = 0;
		goto IL_00c9;
	}

IL_00b6:
	{
		XmlReader_t4229084514 * L_20 = ___reader0;
		int32_t L_21 = V_0;
		NullCheck(L_20);
		VirtActionInvoker1< int32_t >::Invoke(35 /* System.Void System.Xml.XmlReader::MoveToAttribute(System.Int32) */, L_20, L_21);
		XmlReader_t4229084514 * L_22 = ___reader0;
		bool L_23 = ___defattr1;
		XmlWriter_WriteAttribute_m1597931729(__this, L_22, L_23, /*hidden argument*/NULL);
		int32_t L_24 = V_0;
		V_0 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00c9:
	{
		int32_t L_25 = V_0;
		XmlReader_t4229084514 * L_26 = ___reader0;
		NullCheck(L_26);
		int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Xml.XmlReader::get_AttributeCount() */, L_26);
		if ((((int32_t)L_25) < ((int32_t)L_27)))
		{
			goto IL_00b6;
		}
	}
	{
		XmlReader_t4229084514 * L_28 = ___reader0;
		NullCheck(L_28);
		VirtFuncInvoker0< bool >::Invoke(39 /* System.Boolean System.Xml.XmlReader::MoveToElement() */, L_28);
	}

IL_00dc:
	{
		XmlReader_t4229084514 * L_29 = ___reader0;
		NullCheck(L_29);
		bool L_30 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XmlReader::get_IsEmptyElement() */, L_29);
		if (!L_30)
		{
			goto IL_00f2;
		}
	}
	{
		VirtActionInvoker0::Invoke(17 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, __this);
		goto IL_0127;
	}

IL_00f2:
	{
		XmlReader_t4229084514 * L_31 = ___reader0;
		NullCheck(L_31);
		int32_t L_32 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XmlReader::get_Depth() */, L_31);
		V_1 = L_32;
		XmlReader_t4229084514 * L_33 = ___reader0;
		NullCheck(L_33);
		VirtFuncInvoker0< bool >::Invoke(42 /* System.Boolean System.Xml.XmlReader::Read() */, L_33);
		XmlReader_t4229084514 * L_34 = ___reader0;
		NullCheck(L_34);
		int32_t L_35 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_34);
		if ((((int32_t)L_35) == ((int32_t)((int32_t)15))))
		{
			goto IL_0121;
		}
	}

IL_010d:
	{
		XmlReader_t4229084514 * L_36 = ___reader0;
		bool L_37 = ___defattr1;
		VirtActionInvoker2< XmlReader_t4229084514 *, bool >::Invoke(21 /* System.Void System.Xml.XmlWriter::WriteNode(System.Xml.XmlReader,System.Boolean) */, __this, L_36, L_37);
		int32_t L_38 = V_1;
		XmlReader_t4229084514 * L_39 = ___reader0;
		NullCheck(L_39);
		int32_t L_40 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XmlReader::get_Depth() */, L_39);
		if ((((int32_t)L_38) < ((int32_t)L_40)))
		{
			goto IL_010d;
		}
	}

IL_0121:
	{
		VirtActionInvoker0::Invoke(19 /* System.Void System.Xml.XmlWriter::WriteFullEndElement() */, __this);
	}

IL_0127:
	{
		goto IL_0218;
	}

IL_012c:
	{
		return;
	}

IL_012d:
	{
		XmlReader_t4229084514 * L_41 = ___reader0;
		NullCheck(L_41);
		String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(26 /* System.String System.Xml.XmlReader::get_Value() */, L_41);
		VirtActionInvoker1< String_t* >::Invoke(26 /* System.Void System.Xml.XmlWriter::WriteString(System.String) */, __this, L_42);
		goto IL_0218;
	}

IL_013e:
	{
		XmlReader_t4229084514 * L_43 = ___reader0;
		NullCheck(L_43);
		String_t* L_44 = VirtFuncInvoker0< String_t* >::Invoke(26 /* System.String System.Xml.XmlReader::get_Value() */, L_43);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.Xml.XmlWriter::WriteCData(System.String) */, __this, L_44);
		goto IL_0218;
	}

IL_014f:
	{
		XmlReader_t4229084514 * L_45 = ___reader0;
		NullCheck(L_45);
		String_t* L_46 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Xml.XmlReader::get_Name() */, L_45);
		VirtActionInvoker1< String_t* >::Invoke(18 /* System.Void System.Xml.XmlWriter::WriteEntityRef(System.String) */, __this, L_46);
		goto IL_0218;
	}

IL_0160:
	{
		XmlReader_t4229084514 * L_47 = ___reader0;
		NullCheck(L_47);
		String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Xml.XmlReader::get_Name() */, L_47);
		XmlReader_t4229084514 * L_49 = ___reader0;
		NullCheck(L_49);
		String_t* L_50 = VirtFuncInvoker0< String_t* >::Invoke(26 /* System.String System.Xml.XmlReader::get_Value() */, L_49);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(22 /* System.Void System.Xml.XmlWriter::WriteProcessingInstruction(System.String,System.String) */, __this, L_48, L_50);
		goto IL_0218;
	}

IL_0177:
	{
		XmlReader_t4229084514 * L_51 = ___reader0;
		NullCheck(L_51);
		String_t* L_52 = VirtFuncInvoker0< String_t* >::Invoke(26 /* System.String System.Xml.XmlReader::get_Value() */, L_51);
		VirtActionInvoker1< String_t* >::Invoke(14 /* System.Void System.Xml.XmlWriter::WriteComment(System.String) */, __this, L_52);
		goto IL_0218;
	}

IL_0188:
	{
		XmlReader_t4229084514 * L_53 = ___reader0;
		NullCheck(L_53);
		String_t* L_54 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Xml.XmlReader::get_Name() */, L_53);
		XmlReader_t4229084514 * L_55 = ___reader0;
		NullCheck(L_55);
		String_t* L_56 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(15 /* System.String System.Xml.XmlReader::get_Item(System.String) */, L_55, _stringLiteral2370872937);
		XmlReader_t4229084514 * L_57 = ___reader0;
		NullCheck(L_57);
		String_t* L_58 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(15 /* System.String System.Xml.XmlReader::get_Item(System.String) */, L_57, _stringLiteral2460968495);
		XmlReader_t4229084514 * L_59 = ___reader0;
		NullCheck(L_59);
		String_t* L_60 = VirtFuncInvoker0< String_t* >::Invoke(26 /* System.String System.Xml.XmlReader::get_Value() */, L_59);
		VirtActionInvoker4< String_t*, String_t*, String_t*, String_t* >::Invoke(15 /* System.Void System.Xml.XmlWriter::WriteDocType(System.String,System.String,System.String,System.String) */, __this, L_54, L_56, L_58, L_60);
		goto IL_0218;
	}

IL_01b5:
	{
		goto IL_01ba;
	}

IL_01ba:
	{
		XmlReader_t4229084514 * L_61 = ___reader0;
		NullCheck(L_61);
		String_t* L_62 = VirtFuncInvoker0< String_t* >::Invoke(26 /* System.String System.Xml.XmlReader::get_Value() */, L_61);
		VirtActionInvoker1< String_t* >::Invoke(27 /* System.Void System.Xml.XmlWriter::WriteWhitespace(System.String) */, __this, L_62);
		goto IL_0218;
	}

IL_01cb:
	{
		VirtActionInvoker0::Invoke(19 /* System.Void System.Xml.XmlWriter::WriteFullEndElement() */, __this);
		goto IL_0218;
	}

IL_01d6:
	{
		goto IL_0218;
	}

IL_01db:
	{
		goto IL_0218;
	}

IL_01e0:
	{
		ObjectU5BU5D_t11523773* L_63 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, 0);
		ArrayElementTypeCheck (L_63, _stringLiteral295753199);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral295753199);
		ObjectU5BU5D_t11523773* L_64 = L_63;
		XmlReader_t4229084514 * L_65 = ___reader0;
		NullCheck(L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Xml.XmlReader::get_Name() */, L_65);
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, 1);
		ArrayElementTypeCheck (L_64, L_66);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_66);
		ObjectU5BU5D_t11523773* L_67 = L_64;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, 2);
		ArrayElementTypeCheck (L_67, _stringLiteral3827457213);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3827457213);
		ObjectU5BU5D_t11523773* L_68 = L_67;
		XmlReader_t4229084514 * L_69 = ___reader0;
		NullCheck(L_69);
		int32_t L_70 = VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Xml.XmlNodeType System.Xml.XmlReader::get_NodeType() */, L_69);
		int32_t L_71 = L_70;
		Il2CppObject * L_72 = Box(XmlNodeType_t3966624571_il2cpp_TypeInfo_var, &L_71);
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, 3);
		ArrayElementTypeCheck (L_68, L_72);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_72);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_73 = String_Concat_m3016520001(NULL /*static, unused*/, L_68, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_74 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m3226581679(L_74, L_73, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_74);
	}

IL_0218:
	{
		XmlReader_t4229084514 * L_75 = ___reader0;
		NullCheck(L_75);
		VirtFuncInvoker0< bool >::Invoke(42 /* System.Boolean System.Xml.XmlReader::Read() */, L_75);
		return;
	}
}
// System.Void System.Xml.XmlWriter::WriteStartAttribute(System.String,System.String)
extern "C"  void XmlWriter_WriteStartAttribute_m904857238 (XmlWriter_t89522450 * __this, String_t* ___localName0, String_t* ___ns1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___localName0;
		String_t* L_1 = ___ns1;
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(23 /* System.Void System.Xml.XmlWriter::WriteStartAttribute(System.String,System.String,System.String) */, __this, (String_t*)NULL, L_0, L_1);
		return;
	}
}
// System.Void System.Xml.XmlWriter::WriteStartElement(System.String)
extern "C"  void XmlWriter_WriteStartElement_m2942452795 (XmlWriter_t89522450 * __this, String_t* ___localName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___localName0;
		VirtActionInvoker3< String_t*, String_t*, String_t* >::Invoke(25 /* System.Void System.Xml.XmlWriter::WriteStartElement(System.String,System.String,System.String) */, __this, (String_t*)NULL, L_0, (String_t*)NULL);
		return;
	}
}
// System.Void System.Xml.XmlWriterSettings::.ctor()
extern "C"  void XmlWriterSettings__ctor_m262185166 (XmlWriterSettings_t3992467285 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		XmlWriterSettings_Reset_m2203585403(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XmlWriterSettings::Reset()
extern Il2CppClass* Encoding_t180559927_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024;
extern const uint32_t XmlWriterSettings_Reset_m2203585403_MetadataUsageId;
extern "C"  void XmlWriterSettings_Reset_m2203585403 (XmlWriterSettings_t3992467285 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlWriterSettings_Reset_m2203585403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_checkCharacters_0((bool)1);
		__this->set_closeOutput_1((bool)0);
		__this->set_conformance_2(2);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_0 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_encoding_3(L_0);
		__this->set_indent_4((bool)0);
		__this->set_indentChars_5(_stringLiteral1024);
		String_t* L_1 = Environment_get_NewLine_m1034655108(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_newLineChars_6(L_1);
		__this->set_newLineOnAttributes_7((bool)0);
		__this->set_newLineHandling_8(2);
		__this->set_omitXmlDeclaration_9((bool)0);
		__this->set_outputMethod_10(3);
		return;
	}
}
// System.Xml.ConformanceLevel System.Xml.XmlWriterSettings::get_ConformanceLevel()
extern "C"  int32_t XmlWriterSettings_get_ConformanceLevel_m1810449468 (XmlWriterSettings_t3992467285 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_conformance_2();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
