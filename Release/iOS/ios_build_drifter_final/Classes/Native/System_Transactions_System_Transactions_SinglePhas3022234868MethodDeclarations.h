﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Transactions.SinglePhaseEnlistment
struct SinglePhaseEnlistment_t3022234868;
// System.Transactions.Transaction
struct Transaction_t3175846586;
// System.Transactions.ISinglePhaseNotification
struct ISinglePhaseNotification_t2163935249;

#include "codegen/il2cpp-codegen.h"
#include "System_Transactions_System_Transactions_Transactio3175846586.h"

// System.Void System.Transactions.SinglePhaseEnlistment::.ctor(System.Transactions.Transaction,System.Transactions.ISinglePhaseNotification)
extern "C"  void SinglePhaseEnlistment__ctor_m44442160 (SinglePhaseEnlistment_t3022234868 * __this, Transaction_t3175846586 * ___tx0, Il2CppObject * ___enlisted1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
