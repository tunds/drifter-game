﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalObjectManager
struct GlobalObjectManager_t849077355;
// UnityEngine.AudioSource
struct AudioSource_t3628549054;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuManager
struct  MenuManager_t3994435886  : public MonoBehaviour_t3012272455
{
public:
	// GlobalObjectManager MenuManager::persistentData
	GlobalObjectManager_t849077355 * ___persistentData_2;
	// UnityEngine.AudioSource MenuManager::gameBGSound
	AudioSource_t3628549054 * ___gameBGSound_3;

public:
	inline static int32_t get_offset_of_persistentData_2() { return static_cast<int32_t>(offsetof(MenuManager_t3994435886, ___persistentData_2)); }
	inline GlobalObjectManager_t849077355 * get_persistentData_2() const { return ___persistentData_2; }
	inline GlobalObjectManager_t849077355 ** get_address_of_persistentData_2() { return &___persistentData_2; }
	inline void set_persistentData_2(GlobalObjectManager_t849077355 * value)
	{
		___persistentData_2 = value;
		Il2CppCodeGenWriteBarrier(&___persistentData_2, value);
	}

	inline static int32_t get_offset_of_gameBGSound_3() { return static_cast<int32_t>(offsetof(MenuManager_t3994435886, ___gameBGSound_3)); }
	inline AudioSource_t3628549054 * get_gameBGSound_3() const { return ___gameBGSound_3; }
	inline AudioSource_t3628549054 ** get_address_of_gameBGSound_3() { return &___gameBGSound_3; }
	inline void set_gameBGSound_3(AudioSource_t3628549054 * value)
	{
		___gameBGSound_3 = value;
		Il2CppCodeGenWriteBarrier(&___gameBGSound_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
