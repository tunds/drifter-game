﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct Collection_1_t2785436231;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t3778309272;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t2299554949;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IList_1_t2982940815;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_816448501.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor()
extern "C"  void Collection_1__ctor_m1608445137_gshared (Collection_1_t2785436231 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1608445137(__this, method) ((  void (*) (Collection_1_t2785436231 *, const MethodInfo*))Collection_1__ctor_m1608445137_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2914797450_gshared (Collection_1_t2785436231 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2914797450(__this, method) ((  bool (*) (Collection_1_t2785436231 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2914797450_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m4161769875_gshared (Collection_1_t2785436231 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m4161769875(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2785436231 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m4161769875_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3791828942_gshared (Collection_1_t2785436231 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3791828942(__this, method) ((  Il2CppObject * (*) (Collection_1_t2785436231 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3791828942_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3662085475_gshared (Collection_1_t2785436231 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3662085475(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2785436231 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3662085475_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m147160777_gshared (Collection_1_t2785436231 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m147160777(__this, ___value0, method) ((  bool (*) (Collection_1_t2785436231 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m147160777_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3912485435_gshared (Collection_1_t2785436231 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3912485435(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2785436231 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3912485435_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2673967846_gshared (Collection_1_t2785436231 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2673967846(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2785436231 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2673967846_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1828284546_gshared (Collection_1_t2785436231 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1828284546(__this, ___value0, method) ((  void (*) (Collection_1_t2785436231 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1828284546_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3195041779_gshared (Collection_1_t2785436231 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3195041779(__this, method) ((  bool (*) (Collection_1_t2785436231 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3195041779_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2263933151_gshared (Collection_1_t2785436231 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2263933151(__this, method) ((  Il2CppObject * (*) (Collection_1_t2785436231 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2263933151_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1835153656_gshared (Collection_1_t2785436231 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1835153656(__this, method) ((  bool (*) (Collection_1_t2785436231 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1835153656_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3422701313_gshared (Collection_1_t2785436231 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3422701313(__this, method) ((  bool (*) (Collection_1_t2785436231 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3422701313_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3451917990_gshared (Collection_1_t2785436231 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3451917990(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2785436231 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3451917990_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1906609469_gshared (Collection_1_t2785436231 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1906609469(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2785436231 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1906609469_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Add(T)
extern "C"  void Collection_1_Add_m3090068878_gshared (Collection_1_t2785436231 * __this, KeyValuePair_2_t816448501  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3090068878(__this, ___item0, method) ((  void (*) (Collection_1_t2785436231 *, KeyValuePair_2_t816448501 , const MethodInfo*))Collection_1_Add_m3090068878_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Clear()
extern "C"  void Collection_1_Clear_m3309545724_gshared (Collection_1_t2785436231 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3309545724(__this, method) ((  void (*) (Collection_1_t2785436231 *, const MethodInfo*))Collection_1_Clear_m3309545724_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3224778118_gshared (Collection_1_t2785436231 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3224778118(__this, method) ((  void (*) (Collection_1_t2785436231 *, const MethodInfo*))Collection_1_ClearItems_m3224778118_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Contains(T)
extern "C"  bool Collection_1_Contains_m2719252650_gshared (Collection_1_t2785436231 * __this, KeyValuePair_2_t816448501  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2719252650(__this, ___item0, method) ((  bool (*) (Collection_1_t2785436231 *, KeyValuePair_2_t816448501 , const MethodInfo*))Collection_1_Contains_m2719252650_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2708706814_gshared (Collection_1_t2785436231 * __this, KeyValuePair_2U5BU5D_t3778309272* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2708706814(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2785436231 *, KeyValuePair_2U5BU5D_t3778309272*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2708706814_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2579273869_gshared (Collection_1_t2785436231 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2579273869(__this, method) ((  Il2CppObject* (*) (Collection_1_t2785436231 *, const MethodInfo*))Collection_1_GetEnumerator_m2579273869_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m352674050_gshared (Collection_1_t2785436231 * __this, KeyValuePair_2_t816448501  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m352674050(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2785436231 *, KeyValuePair_2_t816448501 , const MethodInfo*))Collection_1_IndexOf_m352674050_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2992313333_gshared (Collection_1_t2785436231 * __this, int32_t ___index0, KeyValuePair_2_t816448501  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2992313333(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2785436231 *, int32_t, KeyValuePair_2_t816448501 , const MethodInfo*))Collection_1_Insert_m2992313333_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2278375336_gshared (Collection_1_t2785436231 * __this, int32_t ___index0, KeyValuePair_2_t816448501  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2278375336(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2785436231 *, int32_t, KeyValuePair_2_t816448501 , const MethodInfo*))Collection_1_InsertItem_m2278375336_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Remove(T)
extern "C"  bool Collection_1_Remove_m619012901_gshared (Collection_1_t2785436231 * __this, KeyValuePair_2_t816448501  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m619012901(__this, ___item0, method) ((  bool (*) (Collection_1_t2785436231 *, KeyValuePair_2_t816448501 , const MethodInfo*))Collection_1_Remove_m619012901_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m866166203_gshared (Collection_1_t2785436231 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m866166203(__this, ___index0, method) ((  void (*) (Collection_1_t2785436231 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m866166203_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2061690651_gshared (Collection_1_t2785436231 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2061690651(__this, ___index0, method) ((  void (*) (Collection_1_t2785436231 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2061690651_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m2160268473_gshared (Collection_1_t2785436231 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m2160268473(__this, method) ((  int32_t (*) (Collection_1_t2785436231 *, const MethodInfo*))Collection_1_get_Count_m2160268473_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t816448501  Collection_1_get_Item_m1909031807_gshared (Collection_1_t2785436231 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1909031807(__this, ___index0, method) ((  KeyValuePair_2_t816448501  (*) (Collection_1_t2785436231 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1909031807_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m4089588620_gshared (Collection_1_t2785436231 * __this, int32_t ___index0, KeyValuePair_2_t816448501  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m4089588620(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2785436231 *, int32_t, KeyValuePair_2_t816448501 , const MethodInfo*))Collection_1_set_Item_m4089588620_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3883989869_gshared (Collection_1_t2785436231 * __this, int32_t ___index0, KeyValuePair_2_t816448501  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3883989869(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2785436231 *, int32_t, KeyValuePair_2_t816448501 , const MethodInfo*))Collection_1_SetItem_m3883989869_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3766044738_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3766044738(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3766044738_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::ConvertItem(System.Object)
extern "C"  KeyValuePair_2_t816448501  Collection_1_ConvertItem_m4119288542_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m4119288542(__this /* static, unused */, ___item0, method) ((  KeyValuePair_2_t816448501  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m4119288542_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3755136702_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3755136702(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3755136702_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m50618690_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m50618690(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m50618690_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m4093459613_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m4093459613(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m4093459613_gshared)(__this /* static, unused */, ___list0, method)
