﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerAttack
struct PlayerAttack_t3691797673;
// UnityEngine.AudioClip
struct AudioClip_t3714538611;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AudioClip3714538611.h"

// System.Void PlayerAttack::.ctor()
extern "C"  void PlayerAttack__ctor_m576635922 (PlayerAttack_t3691797673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerAttack::Awake()
extern "C"  void PlayerAttack_Awake_m814241141 (PlayerAttack_t3691797673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerAttack::Update()
extern "C"  void PlayerAttack_Update_m2422706491 (PlayerAttack_t3691797673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerAttack::PlayAtkSound(UnityEngine.AudioClip)
extern "C"  void PlayerAttack_PlayAtkSound_m399909350 (PlayerAttack_t3691797673 * __this, AudioClip_t3714538611 * ___clip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerAttack::Attack()
extern "C"  void PlayerAttack_Attack_m1638851322 (PlayerAttack_t3691797673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
