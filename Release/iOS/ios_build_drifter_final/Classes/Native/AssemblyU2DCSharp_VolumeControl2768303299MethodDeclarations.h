﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VolumeControl
struct VolumeControl_t2768303299;

#include "codegen/il2cpp-codegen.h"

// System.Void VolumeControl::.ctor()
extern "C"  void VolumeControl__ctor_m4118616072 (VolumeControl_t2768303299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VolumeControl::Start()
extern "C"  void VolumeControl_Start_m3065753864 (VolumeControl_t2768303299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VolumeControl::changeVolume()
extern "C"  void VolumeControl_changeVolume_m3120926086 (VolumeControl_t2768303299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
